.class public Lcom/intsig/camscanner/db/DbTriggerCreator;
.super Ljava/lang/Object;
.source "DbTriggerCreator.java"


# direct methods
.method public static O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS doclike_cleanup DELETE ON documents BEGIN DELETE FROM documentlike WHERE co_token = old.co_token;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS tags_cleanup DELETE ON tags BEGIN UPDATE documents SET tag_id=\'\' WHERE tag_id = old._id;DELETE FROM mtags WHERE tag_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS graphics_cleanup DELETE ON images BEGIN DELETE FROM graphics WHERE image_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS page_mark_cleanup DELETE ON images BEGIN DELETE FROM page_mark WHERE page_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE INDEX IF NOT EXISTS same_document_index on images(document_id);"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS bank_card_journal_page_cleanup DELETE ON images BEGIN DELETE FROM bank_card_journal_page WHERE sync_image_id = old.sync_image_id and sync_account_id = old.sync_account_id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇80〇808〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS pdfsize_cleanup DELETE ON pdfsize BEGIN UPDATE documents SET page_size=0 WHERE page_size = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇8o8o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 11

    .line 1
    const-string v0, "images_cleanup"

    .line 2
    .line 3
    const-string v1, "documents_cleanup"

    .line 4
    .line 5
    const-string v2, "doclike_cleanup"

    .line 6
    .line 7
    const-string v3, "collaborators_cleanup"

    .line 8
    .line 9
    const-string v4, "comments_cleanup"

    .line 10
    .line 11
    const-string v5, "page_mark_cleanup"

    .line 12
    .line 13
    const-string v6, "pdfsize_cleanup"

    .line 14
    .line 15
    const-string v7, "graphics_cleanup"

    .line 16
    .line 17
    const-string v8, "notepath_cleanup"

    .line 18
    .line 19
    const-string v9, "tags_cleanup"

    .line 20
    .line 21
    const-string v10, "bank_card_journal_page_cleanup"

    .line 22
    .line 23
    filled-new-array/range {v0 .. v10}, [Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x0

    .line 28
    :goto_0
    const/16 v2, 0xb

    .line 29
    .line 30
    if-ge v1, v2, :cond_0

    .line 31
    .line 32
    aget-object v2, v0, v1

    .line 33
    .line 34
    new-instance v3, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v4, "DROP TRIGGER IF EXISTS "

    .line 40
    .line 41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v2, ";"

    .line 48
    .line 49
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    add-int/lit8 v1, v1, 0x1

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇O8o08O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->beginTransaction()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 5
    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇80〇808〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 17
    .line 18
    .line 19
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 20
    .line 21
    .line 22
    invoke-interface {p0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->setTransactionSuccessful()V

    .line 23
    .line 24
    .line 25
    invoke-interface {p0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->endTransaction()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS collaborators_cleanup DELETE ON documents BEGIN DELETE FROM collaborators WHERE document_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS comments_cleanup DELETE ON documents BEGIN DELETE FROM comments WHERE doc_co_token = old.co_token or doc_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "CREATE TRIGGER IF NOT EXISTS notepath_cleanup DELETE ON graphics BEGIN DELETE FROM notepath WHERE graphics_id = old._id;END"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

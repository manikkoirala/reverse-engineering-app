.class public final Lcom/intsig/camscanner/db/wrap/WCDBHelper;
.super Lcom/tencent/wcdb/database/SQLiteOpenHelper;
.source "WCDBHelper.kt"

# interfaces
.implements Lcom/intsig/camscanner/db/wrap/DBHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/db/wrap/WCDBHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final o〇00O:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇08O〇00〇o:Lcom/intsig/camscanner/db/wrap/WCDBHelper$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO:Lcom/tencent/wcdb/database/SQLiteDatabase;

.field private final o0:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/db/wrap/WCDBHelper$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/db/wrap/WCDBHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇08O〇00〇o:Lcom/intsig/camscanner/db/wrap/WCDBHelper$Companion;

    .line 8
    .line 9
    const/16 v0, 0x13

    .line 10
    .line 11
    new-array v0, v0, [B

    .line 12
    .line 13
    fill-array-data v0, :array_0

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o〇00O:[B

    .line 17
    .line 18
    return-void

    .line 19
    :array_0
    .array-data 1
        0x43t
        0x61t
        0x6dt
        0x53t
        0x63t
        0x61t
        0x6et
        0x6et
        0x65t
        0x72t
        0x40t
        0x32t
        0x30t
        0x32t
        0x33t
        0x30t
        0x32t
        0x31t
        0x30t
    .end array-data
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v3, "cipherdocuments.db"

    .line 7
    .line 8
    sget-object v4, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o〇00O:[B

    .line 9
    .line 10
    new-instance v0, Lcom/tencent/wcdb/database/SQLiteCipherSpec;

    .line 11
    .line 12
    invoke-direct {v0}, Lcom/tencent/wcdb/database/SQLiteCipherSpec;-><init>()V

    .line 13
    .line 14
    .line 15
    const/16 v1, 0x1000

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/tencent/wcdb/database/SQLiteCipherSpec;->setPageSize(I)Lcom/tencent/wcdb/database/SQLiteCipherSpec;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const v1, 0xfa00

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/tencent/wcdb/database/SQLiteCipherSpec;->setKDFIteration(I)Lcom/tencent/wcdb/database/SQLiteCipherSpec;

    .line 25
    .line 26
    .line 27
    move-result-object v5

    .line 28
    const/4 v6, 0x0

    .line 29
    const v7, 0xa0280

    .line 30
    .line 31
    .line 32
    new-instance v8, Lo08O/〇o00〇〇Oo;

    .line 33
    .line 34
    invoke-direct {v8}, Lo08O/〇o00〇〇Oo;-><init>()V

    .line 35
    .line 36
    .line 37
    move-object v1, p0

    .line 38
    move-object v2, p1

    .line 39
    invoke-direct/range {v1 .. v8}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;[BLcom/tencent/wcdb/database/SQLiteCipherSpec;Lcom/tencent/wcdb/database/SQLiteDatabase$CursorFactory;ILcom/tencent/wcdb/DatabaseErrorHandler;)V

    .line 40
    .line 41
    .line 42
    iput-object p1, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 43
    .line 44
    const/4 p1, -0x1

    .line 45
    iput p1, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static synthetic 〇080(Lcom/tencent/wcdb/database/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇o00〇〇Oo(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇o00〇〇Oo(Lcom/tencent/wcdb/database/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string p0, "WCDBHelper"

    .line 2
    .line 3
    const-string v0, "DatabaseErrorHandler onCorruption"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇o〇()Lcom/tencent/wcdb/database/SQLiteDatabase;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->OO:Lcom/tencent/wcdb/database/SQLiteDatabase;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;->getWritableDatabase()Lcom/tencent/wcdb/database/SQLiteDatabase;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->OO:Lcom/tencent/wcdb/database/SQLiteDatabase;

    .line 10
    .line 11
    :cond_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public Oo08()Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇o〇()Lcom/tencent/wcdb/database/SQLiteDatabase;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onConfigure(Lcom/tencent/wcdb/database/SQLiteDatabase;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;->onConfigure(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    invoke-virtual {p0, v0}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;->setWriteAheadLoggingEnabled(Z)V

    .line 6
    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1, v0}, Lcom/tencent/wcdb/database/SQLiteDatabase;->setAsyncCheckpointEnabled(Z)V

    .line 12
    .line 13
    .line 14
    :goto_0
    if-eqz p1, :cond_1

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const/4 p1, 0x0

    .line 22
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v1, "onConfigure db path: "

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const-string v0, "WCDBHelper"

    .line 40
    .line 41
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onCreate(Lcom/tencent/wcdb/database/SQLiteDatabase;)V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 2
    .line 3
    const-string v1, "documents.db"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const-string v2, " consume="

    .line 14
    .line 15
    const-string v3, "WCDBHelper"

    .line 16
    .line 17
    const/4 v4, 0x0

    .line 18
    if-eqz v1, :cond_a

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/io/File;->length()J

    .line 21
    .line 22
    .line 23
    move-result-wide v5

    .line 24
    sget-object v1, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->〇080:Lcom/intsig/camscanner/db/wrap/DBHelperFactory;

    .line 25
    .line 26
    iget-object v7, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/db/wrap/MigrateErrorType;->〇080()I

    .line 29
    .line 30
    .line 31
    move-result v8

    .line 32
    invoke-virtual {v1, v7, v8}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 36
    .line 37
    .line 38
    move-result-wide v7

    .line 39
    const-string v9, "Migrating plain-text database to encrypted one."

    .line 40
    .line 41
    invoke-static {v3, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    if-eqz p1, :cond_0

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->endTransaction()V

    .line 47
    .line 48
    .line 49
    :cond_0
    sget-object v9, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 50
    .line 51
    const/4 v9, 0x1

    .line 52
    new-array v10, v9, [Ljava/lang/Object;

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/tencent/wcdb/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    const/4 v11, 0x0

    .line 63
    aput-object v0, v10, v11

    .line 64
    .line 65
    invoke-static {v10, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const-string v9, "ATTACH DATABASE %s AS old KEY \'\';"

    .line 70
    .line 71
    invoke-static {v9, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const-string v9, "format(format, *args)"

    .line 76
    .line 77
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    if-eqz p1, :cond_1

    .line 81
    .line 82
    invoke-virtual {p1, v0}, Lcom/tencent/wcdb/database/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    :cond_1
    if-eqz p1, :cond_2

    .line 86
    .line 87
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->beginTransaction()V

    .line 88
    .line 89
    .line 90
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 91
    .line 92
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 93
    .line 94
    .line 95
    invoke-static {v0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇8o8o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 96
    .line 97
    .line 98
    const-string v0, "SELECT sqlcipher_export(\'main\', \'old\');"

    .line 99
    .line 100
    invoke-static {p1, v0, v4}, Lcom/tencent/wcdb/DatabaseUtils;->stringForQuery(Lcom/tencent/wcdb/database/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    if-eqz p1, :cond_3

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->setTransactionSuccessful()V

    .line 106
    .line 107
    .line 108
    :cond_3
    if-eqz p1, :cond_4

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->endTransaction()V

    .line 111
    .line 112
    .line 113
    :cond_4
    const-string v0, "PRAGMA old.user_version;"

    .line 114
    .line 115
    invoke-static {p1, v0, v4}, Lcom/tencent/wcdb/DatabaseUtils;->longForQuery(Lcom/tencent/wcdb/database/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    .line 116
    .line 117
    .line 118
    move-result-wide v9

    .line 119
    long-to-int v0, v9

    .line 120
    if-eqz p1, :cond_5

    .line 121
    .line 122
    const-string v9, "DETACH DATABASE old;"

    .line 123
    .line 124
    invoke-virtual {p1, v9}, Lcom/tencent/wcdb/database/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    :cond_5
    if-eqz p1, :cond_6

    .line 128
    .line 129
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->beginTransaction()V

    .line 130
    .line 131
    .line 132
    :cond_6
    const v9, 0xa0280

    .line 133
    .line 134
    .line 135
    if-le v0, v9, :cond_7

    .line 136
    .line 137
    invoke-virtual {p0, p1, v0, v9}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;->onDowngrade(Lcom/tencent/wcdb/database/SQLiteDatabase;II)V

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_7
    if-ge v0, v9, :cond_8

    .line 142
    .line 143
    invoke-virtual {p0, p1, v0, v9}, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->onUpgrade(Lcom/tencent/wcdb/database/SQLiteDatabase;II)V

    .line 144
    .line 145
    .line 146
    :cond_8
    :goto_0
    new-instance v0, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 147
    .line 148
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 149
    .line 150
    .line 151
    invoke-static {v0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇O8o08O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 152
    .line 153
    .line 154
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 155
    .line 156
    .line 157
    move-result-wide v9

    .line 158
    sub-long/2addr v9, v7

    .line 159
    if-eqz p1, :cond_9

    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v4

    .line 165
    :cond_9
    new-instance p1, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    const-string v0, "migrate db path "

    .line 171
    .line 172
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {p1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    iget-object p1, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 192
    .line 193
    invoke-static {}, Lcom/intsig/camscanner/db/wrap/MigrateErrorType;->〇o〇()I

    .line 194
    .line 195
    .line 196
    move-result v0

    .line 197
    invoke-virtual {v1, p1, v0}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v1, v9, v10}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->oO80(J)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {v1, v5, v6}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->〇80〇808〇O(J)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v1}, Lcom/intsig/camscanner/db/wrap/DBHelperFactory;->〇o〇()Lcom/intsig/camscanner/db/wrap/MigrateCallback;

    .line 207
    .line 208
    .line 209
    move-result-object p1

    .line 210
    if-eqz p1, :cond_c

    .line 211
    .line 212
    invoke-interface {p1}, Lcom/intsig/camscanner/db/wrap/MigrateCallback;->〇080()V

    .line 213
    .line 214
    .line 215
    goto :goto_1

    .line 216
    :cond_a
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 217
    .line 218
    .line 219
    move-result-wide v0

    .line 220
    sget-object v5, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇080:Lcom/intsig/camscanner/db/wrap/DBHelperUtil;

    .line 221
    .line 222
    iget-object v6, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 223
    .line 224
    new-instance v7, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 225
    .line 226
    invoke-direct {v7, p1}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v5, v6, v7}, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 230
    .line 231
    .line 232
    if-eqz p1, :cond_b

    .line 233
    .line 234
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object v4

    .line 238
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 239
    .line 240
    .line 241
    move-result-wide v5

    .line 242
    sub-long/2addr v5, v0

    .line 243
    new-instance p1, Ljava/lang/StringBuilder;

    .line 244
    .line 245
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .line 247
    .line 248
    const-string v0, "onCreate db path "

    .line 249
    .line 250
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 263
    .line 264
    .line 265
    move-result-object p1

    .line 266
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    :cond_c
    :goto_1
    return-void
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public onOpen(Lcom/tencent/wcdb/database/SQLiteDatabase;)V
    .locals 6
    .param p1    # Lcom/tencent/wcdb/database/SQLiteDatabase;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "db"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->isReadOnly()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const-string v1, "WCDBHelper"

    .line 11
    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iget v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I

    .line 15
    .line 16
    const/4 v2, -0x1

    .line 17
    if-eq v0, v2, :cond_0

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getVersion()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iget v3, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I

    .line 24
    .line 25
    if-eq v0, v3, :cond_0

    .line 26
    .line 27
    :try_start_0
    invoke-virtual {p1, v3}, Lcom/tencent/wcdb/database/SQLiteDatabase;->setVersion(I)V

    .line 28
    .line 29
    .line 30
    iget v0, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I

    .line 31
    .line 32
    new-instance v3, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v4, "onOpen mRestoreVersion: "

    .line 38
    .line 39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    iput v2, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    .line 58
    .line 59
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/tencent/wcdb/database/SQLiteOpenHelper;->onOpen(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 71
    .line 72
    .line 73
    move-result-wide v2

    .line 74
    const/16 p1, 0x400

    .line 75
    .line 76
    int-to-long v4, p1

    .line 77
    div-long/2addr v2, v4

    .line 78
    new-instance p1, Ljava/lang/StringBuilder;

    .line 79
    .line 80
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    const-string v4, "onOpen db path: "

    .line 84
    .line 85
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v0, " size:"

    .line 92
    .line 93
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string v0, " KB"

    .line 100
    .line 101
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public onUpgrade(Lcom/tencent/wcdb/database/SQLiteDatabase;II)V
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇080:Lcom/intsig/camscanner/db/wrap/DBHelperUtil;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 8
    .line 9
    new-instance v4, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 10
    .line 11
    invoke-direct {v4, p1}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v2, v3, v4, p2}, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇o〇(Landroid/content/Context;Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;I)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    iput v2, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇OOo8〇0:I

    .line 19
    .line 20
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 21
    .line 22
    .line 23
    move-result-wide v2

    .line 24
    sub-long/2addr v2, v0

    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->getPath()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 p1, 0x0

    .line 33
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v1, "onUpgrade consume: "

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v1, " path="

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string p1, " oldVersion:"

    .line 55
    .line 56
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string p1, " newVersion:"

    .line 63
    .line 64
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    const-string p2, "WCDBHelper"

    .line 75
    .line 76
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public 〇〇888(I)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->〇o〇()Lcom/tencent/wcdb/database/SQLiteDatabase;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇080:Lcom/intsig/camscanner/db/wrap/DBHelperUtil;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/db/wrap/WCDBHelper;->o0:Landroid/content/Context;

    .line 10
    .line 11
    new-instance v3, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;

    .line 12
    .line 13
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/db/wrap/WCDBDatabaseWrapper;-><init>(Lcom/tencent/wcdb/database/SQLiteDatabase;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v2, v3, p1}, Lcom/intsig/camscanner/db/wrap/DBHelperUtil;->〇o〇(Landroid/content/Context;Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;I)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    const/4 v1, -0x1

    .line 21
    if-ne p1, v1, :cond_0

    .line 22
    .line 23
    const p1, 0xa0280

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Lcom/tencent/wcdb/database/SQLiteDatabase;->setVersion(I)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class public Lcom/intsig/camscanner/db/DBCommit;
.super Ljava/lang/Object;
.source "DBCommit.java"


# direct methods
.method public static O000(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "dirs"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "share_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE dirs ADD share_id TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "share_status"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_1

    .line 27
    .line 28
    const-string v1, "ALTER TABLE dirs ADD share_status INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v1, "share_owner"

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-nez v2, :cond_2

    .line 40
    .line 41
    const-string v2, "ALTER TABLE dirs ADD share_owner INTEGER DEFAULT 0"

    .line 42
    .line 43
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    const-string v2, "is_share_entry"

    .line 47
    .line 48
    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-nez v0, :cond_3

    .line 53
    .line 54
    const-string v0, "ALTER TABLE dirs ADD is_share_entry INTEGER DEFAULT 0"

    .line 55
    .line 56
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_3
    const-string v0, "documents"

    .line 60
    .line 61
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-nez v0, :cond_4

    .line 70
    .line 71
    const-string v0, "ALTER TABLE documents ADD share_owner INTEGER DEFAULT 0"

    .line 72
    .line 73
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    :cond_4
    const-string v0, "images"

    .line 77
    .line 78
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    const-string v1, "status_legal"

    .line 83
    .line 84
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-nez v0, :cond_5

    .line 89
    .line 90
    const-string v0, "ALTER TABLE images ADD status_legal INTEGER DEFAULT 0"

    .line 91
    .line 92
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :cond_5
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇O〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 96
    .line 97
    .line 98
    const-string p0, "DBCommit"

    .line 99
    .line 100
    const-string v0, "upgradeDb616000to617000 done"

    .line 101
    .line 102
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static O08000(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "trimmed_image_data"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE images ADD trimmed_image_data TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "sync_trimmed_paper_jpg_state"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE images ADD sync_trimmed_paper_jpg_state INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string p0, "DBCommit"

    .line 34
    .line 35
    const-string v0, "upgradeDb55000toDb55100 done"

    .line 36
    .line 37
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static O0O8OO088(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE graphics ADD scale FLOAT DEFAULT 1.0"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "ALTER TABLE graphics ADD dpi FLOAT DEFAULT 264.0"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "UPDATE graphics set scale=1.0, dpi=264.0;"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static O0o〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "current_share_time"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD current_share_time INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDb626000toDb632000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static O8(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "UPDATE "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string p0, " set "

    .line 15
    .line 16
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p0, "="

    .line 23
    .line 24
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string p0, ";"

    .line 31
    .line 32
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static O8O〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;J)V
    .locals 5

    .line 1
    const-string v0, "ALTER TABLE documents ADD sync_account_id INTEGER DEFAULT -1"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "ALTER TABLE documents ADD sync_doc_id TEXT"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "ALTER TABLE documents ADD sync_timestamp INTEGER DEFAULT 0"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "ALTER TABLE documents ADD sync_state INTEGER DEFAULT 1"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "ALTER TABLE documents ADD sync_extra_state INTEGER DEFAULT 0"

    .line 22
    .line 23
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const-string v0, "ALTER TABLE documents ADD sync_version INTEGER DEFAULT 0"

    .line 27
    .line 28
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string v0, "ALTER TABLE documents ADD page_index_modified INTEGER DEFAULT 0"

    .line 32
    .line 33
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const-string v0, "ALTER TABLE documents ADD sync_ui_state INTEGER DEFAULT 0"

    .line 37
    .line 38
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const-string v0, "ALTER TABLE documents ADD sync_extra_data1 TEXT"

    .line 42
    .line 43
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    const-string v0, "ALTER TABLE documents ADD sync_extra_data2 TEXT"

    .line 47
    .line 48
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string v0, "documents"

    .line 52
    .line 53
    const-string v1, "sync_account_id"

    .line 54
    .line 55
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    const-string v2, "sync_state"

    .line 63
    .line 64
    const/4 v3, 0x1

    .line 65
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/DBCommit;->O8(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    const-string v0, "ALTER TABLE images ADD image_border TEXT"

    .line 73
    .line 74
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const-string v0, "ALTER TABLE images ADD ocr_result TEXT"

    .line 78
    .line 79
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    const-string v0, "ALTER TABLE images ADD ocr_border TEXT"

    .line 83
    .line 84
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    const-string v0, "ALTER TABLE images ADD enhance_mode INTEGER"

    .line 88
    .line 89
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    const-string v0, "ALTER TABLE images ADD image_titile TEXT"

    .line 93
    .line 94
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    const-string v0, "ALTER TABLE images ADD sync_account_id INTEGER DEFAULT -1"

    .line 98
    .line 99
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    const-string v0, "ALTER TABLE images ADD sync_timestamp INTEGER"

    .line 103
    .line 104
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    const-string v0, "ALTER TABLE images ADD sync_state INTEGER"

    .line 108
    .line 109
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    const-string v0, "ALTER TABLE images ADD sync_extra_state INTEGER"

    .line 113
    .line 114
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    const-string v0, "ALTER TABLE images ADD sync_version INTEGER DEFAULT 0"

    .line 118
    .line 119
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    const-string v0, "ALTER TABLE images ADD sync_image_id TEXT"

    .line 123
    .line 124
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    const-string v0, "ALTER TABLE images ADD created_time INTEGER"

    .line 128
    .line 129
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    const-string v0, "ALTER TABLE images ADD last_modified INTEGER"

    .line 133
    .line 134
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    const-string v0, "ALTER TABLE images ADD detail_index INTEGER DEFAULT -1"

    .line 138
    .line 139
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    const-string v0, "ALTER TABLE images ADD contrast_index INTEGER DEFAULT -1"

    .line 143
    .line 144
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    const-string v0, "ALTER TABLE images ADD bright_index INTEGER DEFAULT -1"

    .line 148
    .line 149
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    const-string v0, "ALTER TABLE images ADD image_rotation INTEGER DEFAULT 0"

    .line 153
    .line 154
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    const-string v0, "ALTER TABLE images ADD sync_jpage_timestamp INTEGER"

    .line 158
    .line 159
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    const-string v0, "ALTER TABLE images ADD sync_jpage_state INTEGER"

    .line 163
    .line 164
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    const-string v0, "ALTER TABLE images ADD sync_jpage_extra_state INTEGER"

    .line 168
    .line 169
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    const-string v0, "ALTER TABLE images ADD sync_jpage_version INTEGER"

    .line 173
    .line 174
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    const-string v0, "ALTER TABLE images ADD min_version FLOAT DEFAULT 0"

    .line 178
    .line 179
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    const-string v0, "ALTER TABLE images ADD max_version FLOAT DEFAULT 0"

    .line 183
    .line 184
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    const-string v0, "ALTER TABLE images ADD image_backup TEXT"

    .line 188
    .line 189
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    const-string v0, "ALTER TABLE images ADD sync_ui_state  DEFAULT 0"

    .line 193
    .line 194
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 195
    .line 196
    .line 197
    const-string v0, "ALTER TABLE images ADD sync_extra_data1 TEXT"

    .line 198
    .line 199
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    const-string v0, "ALTER TABLE images ADD sync_extra_data2 TEXT"

    .line 203
    .line 204
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    const-string v0, "images"

    .line 208
    .line 209
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v4

    .line 213
    invoke-interface {p0, v4}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/DBCommit;->O8(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v2

    .line 220
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    const-string v2, "sync_jpage_state"

    .line 224
    .line 225
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/DBCommit;->O8(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v0

    .line 229
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 230
    .line 231
    .line 232
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;J)V

    .line 233
    .line 234
    .line 235
    const-string v0, "ALTER TABLE accounts ADD sync_account_id INTEGER"

    .line 236
    .line 237
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    const-string v0, "accounts"

    .line 241
    .line 242
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 243
    .line 244
    .line 245
    move-result-object v0

    .line 246
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    const-string v0, "ALTER TABLE uploadstate ADD sync_account_id INTEGER"

    .line 250
    .line 251
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 252
    .line 253
    .line 254
    const-string v0, "uploadstate"

    .line 255
    .line 256
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 257
    .line 258
    .line 259
    move-result-object v0

    .line 260
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    const-string v0, "ALTER TABLE faxtask ADD sync_account_id INTEGER"

    .line 264
    .line 265
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    const-string v0, "faxtask"

    .line 269
    .line 270
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v0

    .line 274
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 275
    .line 276
    .line 277
    const-string v0, "ALTER TABLE printtask ADD sync_account_id INTEGER"

    .line 278
    .line 279
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    const-string v0, "printtask"

    .line 283
    .line 284
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/camscanner/db/DBCommit;->〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    .line 285
    .line 286
    .line 287
    move-result-object p1

    .line 288
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 289
    .line 290
    .line 291
    const-string p1, "ALTER TABLE sync_accounts ADD sync_local_version INTEGER"

    .line 292
    .line 293
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    new-instance p1, Ljava/lang/StringBuilder;

    .line 297
    .line 298
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 299
    .line 300
    .line 301
    const-string p2, "UPDATE images SET created_time="

    .line 302
    .line 303
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 307
    .line 308
    .line 309
    move-result-wide v0

    .line 310
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 311
    .line 312
    .line 313
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 314
    .line 315
    .line 316
    move-result-object p1

    .line 317
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 318
    .line 319
    .line 320
    new-instance p1, Ljava/lang/StringBuilder;

    .line 321
    .line 322
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 323
    .line 324
    .line 325
    const-string p2, "UPDATE images SET last_modified="

    .line 326
    .line 327
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    .line 329
    .line 330
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 331
    .line 332
    .line 333
    move-result-wide v0

    .line 334
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 335
    .line 336
    .line 337
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 338
    .line 339
    .line 340
    move-result-object p1

    .line 341
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 342
    .line 343
    .line 344
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->Oooo8o0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 345
    .line 346
    .line 347
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 348
    .line 349
    .line 350
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇〇8O0〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 351
    .line 352
    .line 353
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 354
    .line 355
    .line 356
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->OoO8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 357
    .line 358
    .line 359
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇80〇808〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 360
    .line 361
    .line 362
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 363
    .line 364
    .line 365
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 366
    .line 367
    .line 368
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->o〇O8〇〇o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 369
    .line 370
    .line 371
    const-string p1, "DELETE FROM documents where pages=0"

    .line 372
    .line 373
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 374
    .line 375
    .line 376
    return-void
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public static O8ooOoo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 5
    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇8o8o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static O8〇o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "faxtask"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "country_code"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE faxtask ADD country_code TEXT"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string p0, "DBCommit"

    .line 22
    .line 23
    const-string v0, "upgradeDb3800toDb4000 done"

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OO0o〇〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "office_first_page_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE documents ADD office_first_page_id TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "office_thumb_sync_state"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE documents ADD office_thumb_sync_state INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v0, "images"

    .line 34
    .line 35
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v1, "turned_to_pdf_page"

    .line 40
    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_2

    .line 46
    .line 47
    const-string v0, "ALTER TABLE images ADD turned_to_pdf_page INTEGER DEFAULT 0"

    .line 48
    .line 49
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 53
    .line 54
    .line 55
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 56
    .line 57
    .line 58
    const-string p0, "DBCommit"

    .line 59
    .line 60
    const-string v0, "upgradeDB641000to642000 done"

    .line 61
    .line 62
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "image_type"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE images ADD image_type INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v0, "sync_accounts"

    .line 21
    .line 22
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "account_pwd"

    .line 27
    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    const-string v0, "sync_accounts_new"

    .line 35
    .line 36
    const v1, 0xa0280

    .line 37
    .line 38
    .line 39
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/db/DBCommit;->〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;I)V

    .line 40
    .line 41
    .line 42
    const-string v0, "INSERT INTO sync_accounts_new SELECT _id,account_name,account_state,sync_doc_version,sync_tag_version,sync_image_version,sync_local_version,sync_time,message_num,account_uid,account_type,account_sns_token,account_sns_uid,display_name,col_update_time,col_num_limit,account_extra_data1,layer_num,single_layer_num,total_num,cur_total_num,upload_time FROM sync_accounts"

    .line 43
    .line 44
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const-string v0, "DROP TABLE IF EXISTS sync_accounts"

    .line 48
    .line 49
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const-string v0, "ALTER TABLE sync_accounts_new RENAME TO sync_accounts"

    .line 53
    .line 54
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_1
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static OO8oO0o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "func_tags"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE documents ADD func_tags INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "esign_info"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE documents ADD esign_info TEXT"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v0, "images"

    .line 34
    .line 35
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v1, "p_md5"

    .line 40
    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_2

    .line 46
    .line 47
    const-string v0, "ALTER TABLE images ADD p_md5 TEXT"

    .line 48
    .line 49
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static OOO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->OO0o〇〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OOO〇O0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "camcard_state"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE images ADD camcard_state INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v0, "documents"

    .line 22
    .line 23
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v2, "title_sort_index"

    .line 28
    .line 29
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-ne v0, v1, :cond_1

    .line 34
    .line 35
    const-string v0, "ALTER TABLE documents ADD title_sort_index TEXT"

    .line 36
    .line 37
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    const-string v0, "tags"

    .line 41
    .line 42
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const-string v2, "title_pinyin"

    .line 47
    .line 48
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-ne v0, v1, :cond_2

    .line 53
    .line 54
    const-string v0, "ALTER TABLE tags ADD title_pinyin TEXT"

    .line 55
    .line 56
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_2
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->oo88o8O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 60
    .line 61
    .line 62
    const-string p0, "DBCommit"

    .line 63
    .line 64
    const-string v0, "upgradeDb3600toDb3700 done"

    .line 65
    .line 66
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "INSERT INTO pdfsize values(1,\'A3\',842, 1190)"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "INSERT INTO pdfsize values(2,\'A4\',595, 842)"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "INSERT INTO pdfsize values(3,\'A5\',420, 595)"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "INSERT INTO pdfsize values(4,\'B4\',709, 1001)"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "INSERT INTO pdfsize values(5,\'B5\',499, 709)"

    .line 22
    .line 23
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const-string v0, "INSERT INTO pdfsize values(6,\'Letter\',612, 792)"

    .line 27
    .line 28
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-string v0, "INSERT INTO pdfsize values(7,\'Tabloid\',792, 1224)"

    .line 32
    .line 33
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const-string v0, "INSERT INTO pdfsize values(8,\'Legal\',612, 1008)"

    .line 37
    .line 38
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const-string v0, "INSERT INTO pdfsize values(9,\'Executive\',522, 756)"

    .line 42
    .line 43
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    const-string v0, "INSERT INTO pdfsize values(10,\'Postcard\',283, 416)"

    .line 47
    .line 48
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string v0, "INSERT INTO pdfsize values(11,\'American Foolscap\',612, 936)"

    .line 52
    .line 53
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    const-string v0, "INSERT INTO pdfsize values(12,\'Europe Foolscap\',648, 936)"

    .line 57
    .line 58
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static Oo8Oo00oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "page_water_maker_text"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE images ADD page_water_maker_text TEXT"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string p0, "DBCommit"

    .line 22
    .line 23
    const-string v0, "upgradeDb5000toDb5200 done"

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static OoO8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "ocr_type"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE images ADD ocr_type INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static Ooo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "dirs"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "template_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE dirs ADD template_id TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDb618000to622000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static Oooo8o0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "trim_switch_state"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE images ADD trim_switch_state INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "image_quality_status"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE images ADD image_quality_status INTEGER DEFAULT -1"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v0, "documents"

    .line 34
    .line 35
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v1, "title_source"

    .line 40
    .line 41
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-nez v1, :cond_2

    .line 46
    .line 47
    const-string v1, "ALTER TABLE documents ADD title_source INTEGER DEFAULT 0"

    .line 48
    .line 49
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    const-string v1, "ocr_title_local"

    .line 53
    .line 54
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    if-nez v1, :cond_3

    .line 59
    .line 60
    const-string v1, "ALTER TABLE documents ADD ocr_title_local TEXT"

    .line 61
    .line 62
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    const-string v1, "ocr_title_cloud"

    .line 66
    .line 67
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    if-nez v0, :cond_4

    .line 72
    .line 73
    const-string v0, "ALTER TABLE documents ADD ocr_title_cloud TEXT"

    .line 74
    .line 75
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    :cond_4
    const-string p0, "DBCommit"

    .line 79
    .line 80
    const-string v0, "upgradeDb642000to645000 done"

    .line 81
    .line 82
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static Oo〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "messagecenters"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "team_token"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, -0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    const-string v1, "ALTER TABLE messagecenters ADD team_token TEXT"

    .line 17
    .line 18
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v1, "team_area"

    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-ne v1, v2, :cond_1

    .line 28
    .line 29
    const-string v1, "ALTER TABLE messagecenters ADD team_area INTEGER"

    .line 30
    .line 31
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string v1, "team_title"

    .line 35
    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-ne v1, v2, :cond_2

    .line 41
    .line 42
    const-string v1, "ALTER TABLE messagecenters ADD team_title TEXT"

    .line 43
    .line 44
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    const-string v1, "op_account"

    .line 48
    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-ne v1, v2, :cond_3

    .line 54
    .line 55
    const-string v1, "ALTER TABLE messagecenters ADD op_account TEXT"

    .line 56
    .line 57
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :cond_3
    const-string v1, "op_nickname"

    .line 61
    .line 62
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-ne v1, v2, :cond_4

    .line 67
    .line 68
    const-string v1, "ALTER TABLE messagecenters ADD op_nickname TEXT"

    .line 69
    .line 70
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :cond_4
    const-string v1, "update_type"

    .line 74
    .line 75
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    if-ne v1, v2, :cond_5

    .line 80
    .line 81
    const-string v1, "ALTER TABLE messagecenters ADD update_type TEXT"

    .line 82
    .line 83
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    :cond_5
    const-string v1, "permission"

    .line 87
    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-ne v1, v2, :cond_6

    .line 93
    .line 94
    const-string v1, "ALTER TABLE messagecenters ADD permission INTEGER"

    .line 95
    .line 96
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    :cond_6
    const-string v1, "event"

    .line 100
    .line 101
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-ne v0, v2, :cond_7

    .line 106
    .line 107
    const-string v0, "ALTER TABLE messagecenters ADD event TEXT"

    .line 108
    .line 109
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    :cond_7
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static O〇8O8〇008(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE documents ADD pdf_state INTEGER DEFAULT 0"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p0, "DBCommit"

    .line 7
    .line 8
    const-string v0, "upgradeDb13ToDb14 done"

    .line 9
    .line 10
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static O〇O〇oO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "teaminfos"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "top_doc"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE teaminfos ADD top_doc INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v0, "images"

    .line 22
    .line 23
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v2, "sync_jpg_error_type"

    .line 28
    .line 29
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-ne v0, v1, :cond_1

    .line 34
    .line 35
    const-string v0, "ALTER TABLE images ADD sync_jpg_error_type INTEGER DEFAULT 0"

    .line 36
    .line 37
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    const-string p0, "DBCommit"

    .line 41
    .line 42
    const-string v0, "upgradeDb5700toDb5900 done"

    .line 43
    .line 44
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static o0O0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "auto_wrap"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE images ADD auto_wrap INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDb65000to615000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o0ooO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇00(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    const-string p0, "DBCommit"

    .line 5
    .line 6
    const-string v0, "upgradeDb4020toDb4650 done"

    .line 7
    .line 8
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE documents ADD password TEXT;"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "ALTER TABLE documents ADD page_margin INTEGER DEFAULT 0;"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "CREATE TABLE IF NOT EXISTS faxtask(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,filepath TEXT NOT NULL,pages INTEGER DEFAULT -1,faxnumber TEXT NOT NULL,state INTEGER DEFAULT 1,txn_id INTEGER DEFAULT -1,subject TEXT,created INTEGER DEFAULT 0,time TEXT);"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "CREATE TABLE IF NOT EXISTS printtask(_id INTEGER PRIMARY KEY AUTOINCREMENT,filename TEXT,filepath TEXT NOT NULL,pages INTEGER DEFAULT -1,state INTEGER DEFAULT 1,email TEXT,printer TEXT,printerId TEXT,authtoken TEXT,tag TEXT,created INTEGER DEFAULT 0,time TEXT);"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o800o8O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "relation_word_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD relation_word_id TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o88〇OO08〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "sync_accounts"

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/db/DBCommit;->〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o8oO〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->O〇8O8〇008(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    const-string v0, "messagecenters"

    .line 5
    .line 6
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "sub_type"

    .line 11
    .line 12
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, -0x1

    .line 17
    if-ne v0, v1, :cond_0

    .line 18
    .line 19
    const-string v0, "ALTER TABLE messagecenters ADD sub_type INTEGER DEFAULT -1"

    .line 20
    .line 21
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    const-string p0, "DBCommit"

    .line 25
    .line 26
    const-string v0, "upgradeDb5900toDb5950 done"

    .line 27
    .line 28
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
.end method

.method public static oO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "ocr_string"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE images ADD ocr_string TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "ocr_is_over"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE images ADD ocr_is_over INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string p0, "DBCommit"

    .line 34
    .line 35
    const-string v0, "upgradeDb53100toDb54901 done"

    .line 36
    .line 37
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static oO00OOO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "need_deblur_flag"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE images ADD need_deblur_flag INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "need_crop_dewarp_flag"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE images ADD need_crop_dewarp_flag INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string p0, "DBCommit"

    .line 34
    .line 35
    const-string v0, "upgradeDb615000to616000 done"

    .line 36
    .line 37
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 9

    .line 1
    const-string v1, "tags"

    .line 2
    .line 3
    const-string v0, "_id"

    .line 4
    .line 5
    filled-new-array {v0}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const/4 v3, 0x0

    .line 10
    const/4 v4, 0x0

    .line 11
    const/4 v5, 0x0

    .line 12
    const/4 v6, 0x0

    .line 13
    const/4 v7, 0x0

    .line 14
    move-object v0, p0

    .line 15
    invoke-interface/range {v0 .. v7}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    new-instance v2, Landroid/content/ContentValues;

    .line 26
    .line 27
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 28
    .line 29
    .line 30
    const/4 v3, 0x0

    .line 31
    const/4 v4, 0x0

    .line 32
    :goto_0
    if-ge v4, v1, :cond_0

    .line 33
    .line 34
    invoke-interface {v0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 35
    .line 36
    .line 37
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    .line 38
    .line 39
    .line 40
    move-result-wide v5

    .line 41
    invoke-virtual {v2}, Landroid/content/ContentValues;->clear()V

    .line 42
    .line 43
    .line 44
    add-int/lit8 v4, v4, 0x1

    .line 45
    .line 46
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v7

    .line 50
    const-string v8, "tag_num"

    .line 51
    .line 52
    invoke-virtual {v2, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 53
    .line 54
    .line 55
    const/4 v7, 0x1

    .line 56
    new-array v7, v7, [Ljava/lang/String;

    .line 57
    .line 58
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v5

    .line 62
    aput-object v5, v7, v3

    .line 63
    .line 64
    const-string v5, "tags"

    .line 65
    .line 66
    const-string v6, "_id=?"

    .line 67
    .line 68
    invoke-interface {p0, v5, v2, v6, v7}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 73
    .line 74
    .line 75
    :cond_1
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static oo88o8O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇0〇O0088o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static ooo〇8oO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇O888o0o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static oo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Landroid/content/Context;)V
    .locals 9

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "cache_state"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE images ADD cache_state INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v2, "sharedapps"

    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    const/4 v7, 0x0

    .line 28
    const/4 v8, 0x0

    .line 29
    move-object v1, p0

    .line 30
    invoke-interface/range {v1 .. v8}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    invoke-static {p1, p0}, Lcom/intsig/utils/provider/SharedApps;->〇080(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 35
    .line 36
    .line 37
    const-string p0, "DBCommit"

    .line 38
    .line 39
    const-string p1, "upgradeDb3700toDb3800 done"

    .line 40
    .line 41
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "UPDATE documents set page_size=page_size+1;"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇0OOo〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "PDF_PAGE_NUM_LOCATION"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE documents ADD PDF_PAGE_NUM_LOCATION INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string p0, "DBCommit"

    .line 22
    .line 23
    const-string v0, "upgradeDb51800toDb52300 done"

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->〇〇o8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->〇o0O0O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 5
    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->Oo〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇0000OOO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->o〇〇0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇oOO8O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 17
    .line 18
    .line 19
    const-string p0, "DBCommit"

    .line 20
    .line 21
    const-string v0, "upgradeDb4650toDb5000 done"

    .line 22
    .line 23
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇8oOO88(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "certificate_info"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE documents ADD certificate_info TEXT"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string p0, "DBCommit"

    .line 22
    .line 23
    const-string v0, "upgradeDb5950toDb51400 done"

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE documents ADD password_pdf TEXT;"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇O8〇〇o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE images ADD sync_raw_jpg_state INTEGER DEFAULT 0"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "UPDATE images set sync_raw_jpg_state = 1"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static o〇〇0〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "DELETE FROM accounts where type=2 AND ( EXISTS (SELECT _id FROM accounts WHERE type=3))"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "UPDATE accounts set type=2 where type=3;"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE tags ADD tag_num INTEGER NOT NULL DEFAULT 0;"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBCommit;->oO80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇00(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE images ADD ocr_result_user TEXT"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p0, "DBCommit"

    .line 7
    .line 8
    const-string v0, "upgradeDb13ToDb14 done"

    .line 9
    .line 10
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇0000OOO(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "mtags"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "tag_sync_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE mtags ADD tag_sync_id TEXT "

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string p0, "DBCommit"

    .line 22
    .line 23
    const-string v0, "upgradeDb17ToDb360 done"

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇00〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE images ADD status INTEGER DEFAULT 0;"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "ALTER TABLE images ADD raw_data TEXT;"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "ALTER TABLE images ADD note TEXT;"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "CREATE TABLE IF NOT EXISTS mtags (_id INTEGER PRIMARY KEY AUTOINCREMENT,document_id INTEGER DEFAULT 0,tag_id INTEGER DEFAULT 0);"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇080(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "CREATE TABLE IF NOT EXISTS "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string p1, " ("

    .line 20
    .line 21
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string p1, "_id"

    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string p1, " INTEGER PRIMARY KEY AUTOINCREMENT,"

    .line 30
    .line 31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string p1, "account_name"

    .line 35
    .line 36
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string p1, " TEXT,"

    .line 40
    .line 41
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string p1, "account_state"

    .line 45
    .line 46
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    const-string p1, " INTEGER,"

    .line 50
    .line 51
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v2, "sync_doc_version"

    .line 55
    .line 56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v2, "sync_tag_version"

    .line 63
    .line 64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string v2, "sync_image_version"

    .line 71
    .line 72
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    const/16 p1, 0x8

    .line 86
    .line 87
    if-le p2, p1, :cond_0

    .line 88
    .line 89
    const-string p1, "sync_local_version INTEGER,"

    .line 90
    .line 91
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    :cond_0
    const-string p1, "sync_time INTEGRE,message_num INTEGER,account_uid TEXT,account_type TEXT,account_sns_token TEXT,account_sns_uid TEXT,display_name TEXT,col_update_time INTEGER DEFAULT 0,col_num_limit INTEGER DEFAULT 0,account_extra_data1 TEXT,layer_num INTEGER DEFAULT 1,single_layer_num INTEGER DEFAULT 3,total_num INTEGER DEFAULT 0,cur_total_num INTEGER DEFAULT 0,upload_time LONG DEFAULT 0);"

    .line 95
    .line 96
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
.end method

.method public static 〇08O8o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "sign_border"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE images ADD sign_border TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "file_type"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "ALTER TABLE images ADD file_type INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string p0, "DBCommit"

    .line 34
    .line 35
    const-string v0, "upgradeDb51800toDb53100 done"

    .line 36
    .line 37
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇0〇O0088o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "office_file_upload_state"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD office_file_upload_state INTERGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->o〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    const-string v0, "documents"

    .line 5
    .line 6
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "property"

    .line 11
    .line 12
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const-string v0, "ALTER TABLE documents ADD property TEXT"

    .line 19
    .line 20
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    const-string p0, "DBCommit"

    .line 24
    .line 25
    const-string v0, "upgradeDb54901toDb55000 done"

    .line 26
    .line 27
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇80(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 5

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "scenario_doc_type"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE documents ADD scenario_doc_type INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "scenario_certificate_info"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_1

    .line 27
    .line 28
    const-string v1, "ALTER TABLE documents ADD scenario_certificate_info TEXT"

    .line 29
    .line 30
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v1, "scenario_recommend_status"

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-nez v1, :cond_2

    .line 40
    .line 41
    const-string v1, "ALTER TABLE documents ADD scenario_recommend_status INTEGER DEFAULT 0"

    .line 42
    .line 43
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    const-string v1, "certificate_state"

    .line 47
    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-nez v1, :cond_3

    .line 53
    .line 54
    const-string v1, "ALTER TABLE documents ADD certificate_state INTEGER DEFAULT 0"

    .line 55
    .line 56
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_3
    const-string v1, "scenario_rename_hint_shown"

    .line 60
    .line 61
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-nez v0, :cond_4

    .line 66
    .line 67
    const-string v0, "ALTER TABLE documents ADD scenario_rename_hint_shown INTEGER DEFAULT 0"

    .line 68
    .line 69
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :cond_4
    const-string v0, "dirs"

    .line 73
    .line 74
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    const-string v1, "scenario_dir_type"

    .line 79
    .line 80
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-nez v1, :cond_5

    .line 85
    .line 86
    const-string v1, "ALTER TABLE dirs ADD scenario_dir_type INTEGER DEFAULT 0"

    .line 87
    .line 88
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    :cond_5
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 92
    .line 93
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    const-string v2, "DBCommit"

    .line 98
    .line 99
    const/4 v3, 0x0

    .line 100
    if-eqz v1, :cond_6

    .line 101
    .line 102
    const-string v4, "DOC_VIEW_MODE"

    .line 103
    .line 104
    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    goto :goto_0

    .line 109
    :cond_6
    const-string v1, "upgradeDb617000to618000 sp is NULL"

    .line 110
    .line 111
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    :goto_0
    const-string v1, "dir_view_mode"

    .line 115
    .line 116
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    if-nez v0, :cond_7

    .line 121
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    .line 123
    .line 124
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .line 126
    .line 127
    const-string v1, "ALTER TABLE dirs ADD dir_view_mode INTEGER DEFAULT "

    .line 128
    .line 129
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    :cond_7
    const-string v0, "images"

    .line 143
    .line 144
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    const-string v1, "pay_for_export"

    .line 149
    .line 150
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 151
    .line 152
    .line 153
    move-result v0

    .line 154
    if-nez v0, :cond_8

    .line 155
    .line 156
    const-string v0, "ALTER TABLE images ADD pay_for_export TEXT"

    .line 157
    .line 158
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    :cond_8
    new-instance p0, Ljava/lang/StringBuilder;

    .line 162
    .line 163
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    .line 165
    .line 166
    const-string v0, "upgradeDb617000to618000 done, globalViewMode="

    .line 167
    .line 168
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object p0

    .line 178
    invoke-static {v2, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    return-void
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static 〇80〇808〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "file_type"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    const-string v1, "ALTER TABLE documents ADD file_type TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v1, "file_source"

    .line 21
    .line 22
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_1

    .line 27
    .line 28
    const-string v1, "ALTER TABLE documents ADD file_source INTERGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    const-string v1, "office_file_path"

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-nez v1, :cond_2

    .line 40
    .line 41
    const-string v1, "ALTER TABLE documents ADD office_file_path TEXT"

    .line 42
    .line 43
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    const-string v1, "office_file_sync_state"

    .line 47
    .line 48
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-nez v1, :cond_3

    .line 53
    .line 54
    const-string v1, "ALTER TABLE documents ADD office_file_sync_state INTERGER DEFAULT 0"

    .line 55
    .line 56
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_3
    const-string v1, "office_file_sync_state_backup"

    .line 60
    .line 61
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-nez v1, :cond_4

    .line 66
    .line 67
    const-string v1, "ALTER TABLE documents ADD office_file_sync_state_backup INTERHER DEFAULT 0"

    .line 68
    .line 69
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    :cond_4
    const-string v1, "office_file_sync_version"

    .line 73
    .line 74
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    if-nez v1, :cond_5

    .line 79
    .line 80
    const-string v1, "ALTER TABLE documents ADD office_file_sync_version INTERGER DEFAULT 0"

    .line 81
    .line 82
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    :cond_5
    const-string v1, "office_page_size"

    .line 86
    .line 87
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-nez v0, :cond_6

    .line 92
    .line 93
    const-string v0, "ALTER TABLE documents ADD office_page_size INTERGER DEFAULT 0"

    .line 94
    .line 95
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    :cond_6
    const-string p0, "DBCommit"

    .line 99
    .line 100
    const-string v0, "upgradeDB632000to633000 done"

    .line 101
    .line 102
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇8o8o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "office_file_upload_version"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD office_file_upload_version INTERGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDB635100to637000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇8〇0〇o〇O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "ocr_time"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE images ADD ocr_time INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDb55100to65000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O00(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "pay_extra"

    .line 8
    .line 9
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    const-string v2, "ALTER TABLE documents ADD pay_extra TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string v2, "pay_lock_file"

    .line 21
    .line 22
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_1

    .line 27
    .line 28
    const-string v1, "ALTER TABLE documents ADD pay_lock_file INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v1, "certificate_lock_file"

    .line 38
    .line 39
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_2

    .line 44
    .line 45
    const-string v0, "ALTER TABLE documents ADD certificate_lock_file INTEGER DEFAULT 0"

    .line 46
    .line 47
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    :cond_2
    const-string v0, "images"

    .line 51
    .line 52
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const-string v1, "parent_spell_page"

    .line 57
    .line 58
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-nez v1, :cond_3

    .line 63
    .line 64
    const-string v1, "ALTER TABLE images ADD parent_spell_page TEXT"

    .line 65
    .line 66
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    :cond_3
    const-string v1, "child_spell_pages"

    .line 70
    .line 71
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-nez v0, :cond_4

    .line 76
    .line 77
    const-string v0, "ALTER TABLE images ADD child_spell_pages TEXT"

    .line 78
    .line 79
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    :cond_4
    const-string p0, "DBCommit"

    .line 83
    .line 84
    const-string v0, "upgradeDB648000to650000 done"

    .line 85
    .line 86
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇O888o0o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "origin"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD origin INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDB655000to656000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O8o08O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "mtags"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "tag_source"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE mtags ADD tag_source INTERGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "is_electronic"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD is_electronic INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDB647000to648000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇O〇80o08O(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "signature"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "signature_color"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE signature ADD signature_color TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDb622000to624000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "sync_dir_id"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    const/4 v1, -0x1

    .line 14
    if-ne v0, v1, :cond_0

    .line 15
    .line 16
    const-string v0, "ALTER TABLE documents ADD sync_dir_id TEXT"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v0, "sync_accounts"

    .line 22
    .line 23
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v2, "layer_num"

    .line 28
    .line 29
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-ne v2, v1, :cond_1

    .line 34
    .line 35
    const-string v2, "ALTER TABLE sync_accounts ADD layer_num INTEGER DEFAULT 1"

    .line 36
    .line 37
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    const-string v2, "single_layer_num"

    .line 41
    .line 42
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    if-ne v2, v1, :cond_2

    .line 47
    .line 48
    const-string v2, "ALTER TABLE sync_accounts ADD single_layer_num INTEGER DEFAULT 3"

    .line 49
    .line 50
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    const-string v2, "total_num"

    .line 54
    .line 55
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-ne v2, v1, :cond_3

    .line 60
    .line 61
    const-string v2, "ALTER TABLE sync_accounts ADD total_num INTEGER DEFAULT 0"

    .line 62
    .line 63
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    :cond_3
    const-string v2, "cur_total_num"

    .line 67
    .line 68
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-ne v2, v1, :cond_4

    .line 73
    .line 74
    const-string v2, "ALTER TABLE sync_accounts ADD cur_total_num INTEGER DEFAULT 0"

    .line 75
    .line 76
    invoke-interface {p0, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :cond_4
    const-string v2, "upload_time"

    .line 80
    .line 81
    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    if-ne v0, v1, :cond_5

    .line 86
    .line 87
    const-string v0, "ALTER TABLE sync_accounts ADD upload_time INTEGER DEFAULT 0"

    .line 88
    .line 89
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    :cond_5
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 93
    .line 94
    .line 95
    const-string p0, "DBCommit"

    .line 96
    .line 97
    const-string v0, "upgradeDb4000toDb4020 done"

    .line 98
    .line 99
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "PRAGMA table_info("

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, ")"

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const/4 v2, 0x0

    .line 29
    invoke-interface {p0, v1, v2}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    if-eqz p0, :cond_2

    .line 34
    .line 35
    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    const-string v1, "name"

    .line 42
    .line 43
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-nez v2, :cond_0

    .line 56
    .line 57
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 62
    .line 63
    .line 64
    new-instance p0, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v1, "getColumnNames table"

    .line 70
    .line 71
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string p1, " columnNames="

    .line 78
    .line 79
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p0

    .line 89
    const-string p1, "DBCommit"

    .line 90
    .line 91
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_2
    return-object v0
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private static 〇o0O0O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "dirs"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "permission"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, -0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    const-string v1, "ALTER TABLE dirs ADD permission INTEGER DEFAULT 2"

    .line 17
    .line 18
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v1, "team_token"

    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-ne v1, v2, :cond_1

    .line 28
    .line 29
    const-string v1, "ALTER TABLE dirs ADD team_token TEXT"

    .line 30
    .line 31
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string v1, "team_layer"

    .line 35
    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-ne v1, v2, :cond_2

    .line 41
    .line 42
    const-string v1, "ALTER TABLE dirs ADD team_layer INTEGER DEFAULT 1"

    .line 43
    .line 44
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    const-string v1, "last_upload_time"

    .line 48
    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-ne v0, v2, :cond_3

    .line 54
    .line 55
    const-string v0, "ALTER TABLE dirs ADD last_upload_time LONG"

    .line 56
    .line 57
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :cond_3
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇oOO8O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    const-string v0, "ALTER TABLE documents ADD auto_upload_state INTEGER DEFAULT 0"

    .line 2
    .line 3
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "update documents set auto_upload_state = 0"

    .line 7
    .line 8
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "ALTER TABLE documents ADD need_auto_upload INTEGER DEFAULT 0"

    .line 12
    .line 13
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "ALTER TABLE images ADD image_confirm_state INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "update images set image_confirm_state = 0"

    .line 22
    .line 23
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const-string p0, "DBCommit"

    .line 27
    .line 28
    const-string v0, "upgradeDb16ToDb17 done"

    .line 29
    .line 30
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
.end method

.method public static 〇oo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->O8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 5
    .line 6
    .line 7
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->Oo08(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 8
    .line 9
    .line 10
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->〇o〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 11
    .line 12
    .line 13
    const-string v0, "ALTER TABLE documents ADD belong_state INTEGER DEFAULT 0"

    .line 14
    .line 15
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string v0, "ALTER TABLE documents ADD co_token TEXT"

    .line 19
    .line 20
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-string v0, "ALTER TABLE documents ADD co_state INTEGER DEFAULT 0"

    .line 24
    .line 25
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const-string v0, "ALTER TABLE documents ADD co_time INTEGER DEFAULT 0"

    .line 29
    .line 30
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const-string v0, "ALTER TABLE documents ADD co_tmp_time INTEGER DEFAULT 0"

    .line 34
    .line 35
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const-string v0, "ALTER TABLE documents ADD thumb_state INTEGER DEFAULT 0"

    .line 39
    .line 40
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const-string v0, "ALTER TABLE documents ADD co_num_limit INTEGER DEFAULT 0"

    .line 44
    .line 45
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const-string v0, "ALTER TABLE images ADD belong_state INTEGER DEFAULT 0"

    .line 49
    .line 50
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const-string v0, "ALTER TABLE sync_accounts ADD display_name TEXT"

    .line 54
    .line 55
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const-string v0, "ALTER TABLE sync_accounts ADD col_update_time INTEGER DEFAULT 0"

    .line 59
    .line 60
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const-string v0, "ALTER TABLE sync_accounts ADD col_num_limit INTEGER DEFAULT 0"

    .line 64
    .line 65
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const-string v0, "UPDATE documents set thumb_state = 1"

    .line 69
    .line 70
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private static 〇o〇(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "UPDATE "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string p0, " set "

    .line 15
    .line 16
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string p0, "="

    .line 23
    .line 24
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string p0, ";"

    .line 31
    .line 32
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    return-object p0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static 〇〇0o(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "type"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, -0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    const-string v1, "ALTER TABLE documents ADD type INTEGER DEFAULT 0"

    .line 17
    .line 18
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v1, "folder_type"

    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-ne v0, v2, :cond_1

    .line 28
    .line 29
    const-string v0, "ALTER TABLE documents ADD folder_type INTEGER DEFAULT 0"

    .line 30
    .line 31
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string v0, "images"

    .line 35
    .line 36
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-ne v0, v2, :cond_2

    .line 45
    .line 46
    const-string v0, "ALTER TABLE images ADD folder_type INTEGER DEFAULT 0"

    .line 47
    .line 48
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :cond_2
    const-string v0, "dirs"

    .line 52
    .line 53
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-ne v0, v2, :cond_3

    .line 62
    .line 63
    const-string v0, "ALTER TABLE dirs ADD folder_type INTEGER DEFAULT 0"

    .line 64
    .line 65
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    :cond_3
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->〇oo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 69
    .line 70
    .line 71
    const-string p0, "DBCommit"

    .line 72
    .line 73
    const-string v0, "upgradeDb5200toDb5700 done"

    .line 74
    .line 75
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇〇808〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "page_view_mode"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD page_view_mode INTEGER DEFAULT 0"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDB645000to647000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static 〇〇888(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;J)V
    .locals 15

    .line 1
    move-object v8, p0

    .line 2
    const-string v1, "tags"

    .line 3
    .line 4
    const-string v9, "_id"

    .line 5
    .line 6
    const-string v10, "title"

    .line 7
    .line 8
    const-string v11, "tag_num"

    .line 9
    .line 10
    filled-new-array {v9, v10, v11}, [Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x0

    .line 17
    const/4 v6, 0x0

    .line 18
    const/4 v7, 0x0

    .line 19
    move-object v0, p0

    .line 20
    invoke-interface/range {v0 .. v7}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v1, 0x0

    .line 25
    const/4 v2, 0x1

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-lez v4, :cond_0

    .line 33
    .line 34
    new-array v5, v4, [J

    .line 35
    .line 36
    new-array v6, v4, [Ljava/lang/String;

    .line 37
    .line 38
    new-array v4, v4, [I

    .line 39
    .line 40
    const/4 v7, 0x0

    .line 41
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 42
    .line 43
    .line 44
    move-result v12

    .line 45
    if-eqz v12, :cond_1

    .line 46
    .line 47
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 48
    .line 49
    .line 50
    move-result-wide v12

    .line 51
    aput-wide v12, v5, v7

    .line 52
    .line 53
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v12

    .line 57
    aput-object v12, v6, v7

    .line 58
    .line 59
    const/4 v12, 0x2

    .line 60
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    .line 61
    .line 62
    .line 63
    move-result v12

    .line 64
    aput v12, v4, v7

    .line 65
    .line 66
    add-int/2addr v7, v2

    .line 67
    goto :goto_0

    .line 68
    :cond_0
    move-object v4, v3

    .line 69
    move-object v5, v4

    .line 70
    move-object v6, v5

    .line 71
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 72
    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_2
    move-object v4, v3

    .line 76
    move-object v5, v4

    .line 77
    move-object v6, v5

    .line 78
    :goto_1
    const-string v0, "DROP TABLE tags;"

    .line 79
    .line 80
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-static {p0}, Lcom/intsig/camscanner/db/DBTableCreator;->O8ooOoo〇(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 84
    .line 85
    .line 86
    invoke-static {p0}, Lcom/intsig/camscanner/db/DbTriggerCreator;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V

    .line 87
    .line 88
    .line 89
    if-eqz v5, :cond_3

    .line 90
    .line 91
    new-instance v0, Landroid/content/ContentValues;

    .line 92
    .line 93
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 94
    .line 95
    .line 96
    :goto_2
    array-length v7, v5

    .line 97
    if-ge v1, v7, :cond_3

    .line 98
    .line 99
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 100
    .line 101
    .line 102
    aget-wide v12, v5, v1

    .line 103
    .line 104
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 105
    .line 106
    .line 107
    move-result-object v7

    .line 108
    invoke-virtual {v0, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 109
    .line 110
    .line 111
    aget-object v7, v6, v1

    .line 112
    .line 113
    invoke-virtual {v0, v10, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    aget v7, v4, v1

    .line 117
    .line 118
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 119
    .line 120
    .line 121
    move-result-object v7

    .line 122
    invoke-virtual {v0, v11, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 123
    .line 124
    .line 125
    const-string v7, "sync_tag_id"

    .line 126
    .line 127
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v12

    .line 131
    invoke-virtual {v0, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    const-string v7, "sync_account_id"

    .line 135
    .line 136
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 137
    .line 138
    .line 139
    move-result-object v12

    .line 140
    invoke-virtual {v0, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    .line 142
    .line 143
    const-string v7, "sync_state"

    .line 144
    .line 145
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 146
    .line 147
    .line 148
    move-result-object v12

    .line 149
    invoke-virtual {v0, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 150
    .line 151
    .line 152
    const-string v7, "tags"

    .line 153
    .line 154
    invoke-interface {p0, v7, v3, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 155
    .line 156
    .line 157
    move-result-wide v12

    .line 158
    new-instance v7, Ljava/lang/StringBuilder;

    .line 159
    .line 160
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    .line 162
    .line 163
    const-string v14, "updateSyncTagTable num = "

    .line 164
    .line 165
    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    aget v14, v4, v1

    .line 169
    .line 170
    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    const-string v14, " id = "

    .line 174
    .line 175
    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-virtual {v7, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v7

    .line 185
    const-string v12, "DBCommit"

    .line 186
    .line 187
    invoke-static {v12, v7}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    add-int/lit8 v1, v1, 0x1

    .line 191
    .line 192
    goto :goto_2

    .line 193
    :cond_3
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public static 〇〇8O0〇8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 2

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "image_json_param"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ALTER TABLE documents ADD image_json_param TEXT"

    .line 16
    .line 17
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const-string p0, "DBCommit"

    .line 21
    .line 22
    const-string v0, "upgradeDB650000to652000 done"

    .line 23
    .line 24
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static 〇〇o8(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "documents"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "permission"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, -0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    const-string v1, "ALTER TABLE documents ADD permission INTEGER DEFAULT 2"

    .line 17
    .line 18
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v1, "creator_user_id"

    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-ne v1, v2, :cond_1

    .line 28
    .line 29
    const-string v1, "ALTER TABLE documents ADD creator_user_id TEXT"

    .line 30
    .line 31
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string v1, "creator_account"

    .line 35
    .line 36
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-ne v1, v2, :cond_2

    .line 41
    .line 42
    const-string v1, "ALTER TABLE documents ADD creator_account TEXT"

    .line 43
    .line 44
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    const-string v1, "creator_nickname"

    .line 48
    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-ne v1, v2, :cond_3

    .line 54
    .line 55
    const-string v1, "ALTER TABLE documents ADD creator_nickname TEXT"

    .line 56
    .line 57
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :cond_3
    const-string v1, "upload_time"

    .line 61
    .line 62
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-ne v1, v2, :cond_4

    .line 67
    .line 68
    const-string v1, "ALTER TABLE documents ADD upload_time LONG"

    .line 69
    .line 70
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :cond_4
    const-string v1, "last_upload_time"

    .line 74
    .line 75
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    if-ne v1, v2, :cond_5

    .line 80
    .line 81
    const-string v1, "ALTER TABLE documents ADD last_upload_time LONG"

    .line 82
    .line 83
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    :cond_5
    const-string v1, "team_token"

    .line 87
    .line 88
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-ne v0, v2, :cond_6

    .line 93
    .line 94
    const-string v0, "ALTER TABLE documents ADD team_token TEXT"

    .line 95
    .line 96
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    :cond_6
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static 〇〇〇0〇〇0(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;)V
    .locals 3

    .line 1
    const-string v0, "images"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lcom/intsig/camscanner/db/DBCommit;->〇o00〇〇Oo(Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "ori_rotation"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, -0x1

    .line 14
    if-ne v1, v2, :cond_0

    .line 15
    .line 16
    const-string v1, "ALTER TABLE images ADD ori_rotation INTEGER DEFAULT -1"

    .line 17
    .line 18
    invoke-interface {p0, v1}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    const-string v1, "ocr_paragraph"

    .line 22
    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-ne v0, v2, :cond_1

    .line 28
    .line 29
    const-string v0, "ALTER TABLE images ADD ocr_paragraph TEXT"

    .line 30
    .line 31
    invoke-interface {p0, v0}, Lcom/intsig/camscanner/db/wrap/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string p0, "DBCommit"

    .line 35
    .line 36
    const-string v0, "upgradeDb51400toDb51800 done"

    .line 37
    .line 38
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.class public Lcom/intsig/camscanner/CsEventBusIndex;
.super Ljava/lang/Object;
.source "CsEventBusIndex.java"

# interfaces
.implements Lorg/greenrobot/eventbus/meta/SubscriberInfoIndex;


# static fields
.field private static final 〇080:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lorg/greenrobot/eventbus/meta/SubscriberInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/CsEventBusIndex;->〇080:Ljava/util/Map;

    .line 7
    .line 8
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    new-array v2, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 12
    .line 13
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 14
    .line 15
    sget-object v10, Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;

    .line 16
    .line 17
    const-string v4, "onSyncResult"

    .line 18
    .line 19
    const-class v5, Lcom/intsig/camscanner/eventbus/SyncEvent;

    .line 20
    .line 21
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 22
    .line 23
    .line 24
    const/4 v11, 0x0

    .line 25
    aput-object v3, v2, v11

    .line 26
    .line 27
    const-class v3, Lcom/intsig/camscanner/test/docjson/DocSyncTestFragment;

    .line 28
    .line 29
    invoke-direct {v0, v3, v1, v2}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 30
    .line 31
    .line 32
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 33
    .line 34
    .line 35
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 36
    .line 37
    new-array v2, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 38
    .line 39
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 40
    .line 41
    const-string v6, "onReceiveLoginOut"

    .line 42
    .line 43
    const-class v7, Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LoginOutEvent;

    .line 44
    .line 45
    invoke-direct {v3, v6, v7, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 46
    .line 47
    .line 48
    aput-object v3, v2, v11

    .line 49
    .line 50
    const-class v3, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 51
    .line 52
    invoke-direct {v0, v3, v1, v2}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 53
    .line 54
    .line 55
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 56
    .line 57
    .line 58
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 59
    .line 60
    const/16 v2, 0x9

    .line 61
    .line 62
    new-array v3, v2, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 63
    .line 64
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 65
    .line 66
    const-string v9, "onReceiveDirShareEventFromQr"

    .line 67
    .line 68
    const-class v12, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle$DuuidJson;

    .line 69
    .line 70
    invoke-direct {v8, v9, v12, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 71
    .line 72
    .line 73
    aput-object v8, v3, v11

    .line 74
    .line 75
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 76
    .line 77
    const-string v9, "onBackFromCardDetailMoveCopy"

    .line 78
    .line 79
    const-class v12, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;

    .line 80
    .line 81
    invoke-direct {v8, v9, v12, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 82
    .line 83
    .line 84
    aput-object v8, v3, v1

    .line 85
    .line 86
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 87
    .line 88
    const-string v9, "onReceiveLoginFinish"

    .line 89
    .line 90
    const-class v12, Lcom/intsig/tsapp/account/model/LoginFinishEvent;

    .line 91
    .line 92
    invoke-direct {v8, v9, v12, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 93
    .line 94
    .line 95
    const/4 v13, 0x2

    .line 96
    aput-object v8, v3, v13

    .line 97
    .line 98
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 99
    .line 100
    invoke-direct {v8, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 101
    .line 102
    .line 103
    const/4 v14, 0x3

    .line 104
    aput-object v8, v3, v14

    .line 105
    .line 106
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 107
    .line 108
    const-string v15, "remoteOpenFolder"

    .line 109
    .line 110
    const-class v2, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;

    .line 111
    .line 112
    invoke-direct {v8, v15, v2, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 113
    .line 114
    .line 115
    const/4 v2, 0x4

    .line 116
    aput-object v8, v3, v2

    .line 117
    .line 118
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 119
    .line 120
    const-string v15, "onSystemMsgReceived"

    .line 121
    .line 122
    const-class v2, Lcom/intsig/camscanner/mainmenu/mepage/entity/SystemMsgEvent;

    .line 123
    .line 124
    invoke-direct {v8, v15, v2, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 125
    .line 126
    .line 127
    const/4 v14, 0x5

    .line 128
    aput-object v8, v3, v14

    .line 129
    .line 130
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 131
    .line 132
    const-string v14, "onCaptureImportFileReceived"

    .line 133
    .line 134
    const-class v13, Lcom/intsig/camscanner/eventbus/ImportFileEvent;

    .line 135
    .line 136
    invoke-direct {v8, v14, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 137
    .line 138
    .line 139
    const/4 v13, 0x6

    .line 140
    aput-object v8, v3, v13

    .line 141
    .line 142
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 143
    .line 144
    const-string v14, "onVipIconShake"

    .line 145
    .line 146
    const-class v13, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$VipIconShaker;

    .line 147
    .line 148
    invoke-direct {v8, v14, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 149
    .line 150
    .line 151
    const/4 v13, 0x7

    .line 152
    aput-object v8, v3, v13

    .line 153
    .line 154
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 155
    .line 156
    const-string v14, "onReceiveUnsubscribeScaffoldMeRedDot"

    .line 157
    .line 158
    const-class v13, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$UnsubscribeScaffoldMeRedDot;

    .line 159
    .line 160
    invoke-direct {v8, v14, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 161
    .line 162
    .line 163
    const/16 v13, 0x8

    .line 164
    .line 165
    aput-object v8, v3, v13

    .line 166
    .line 167
    const-class v8, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 168
    .line 169
    invoke-direct {v0, v8, v1, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 170
    .line 171
    .line 172
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 173
    .line 174
    .line 175
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 176
    .line 177
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 178
    .line 179
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 180
    .line 181
    const-string v14, "onPageChange"

    .line 182
    .line 183
    const-class v13, Lcom/intsig/camscanner/eventbus/PageChangeEvent;

    .line 184
    .line 185
    invoke-direct {v8, v14, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 186
    .line 187
    .line 188
    aput-object v8, v3, v11

    .line 189
    .line 190
    const-class v8, Lcom/intsig/camscanner/fragment/ImagePageViewFragment;

    .line 191
    .line 192
    invoke-direct {v0, v8, v1, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 193
    .line 194
    .line 195
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 196
    .line 197
    .line 198
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 199
    .line 200
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 201
    .line 202
    new-instance v8, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 203
    .line 204
    const-string v1, "onReceiveProductResult"

    .line 205
    .line 206
    move-object/from16 v16, v13

    .line 207
    .line 208
    const-class v13, Lcom/intsig/camscanner/guide/guidevideo/entity/GuideVideoProductResult;

    .line 209
    .line 210
    invoke-direct {v8, v1, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 211
    .line 212
    .line 213
    aput-object v8, v3, v11

    .line 214
    .line 215
    const-class v1, Lcom/intsig/camscanner/guide/guidevideo/GuideVideoFragment;

    .line 216
    .line 217
    const/4 v8, 0x1

    .line 218
    invoke-direct {v0, v1, v8, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 219
    .line 220
    .line 221
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 222
    .line 223
    .line 224
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 225
    .line 226
    const/4 v1, 0x2

    .line 227
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 228
    .line 229
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 230
    .line 231
    const-string v8, "onToolTitleEvent"

    .line 232
    .line 233
    const-class v13, Lcom/intsig/camscanner/eventbus/ToolTitleEvent;

    .line 234
    .line 235
    invoke-direct {v1, v8, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 236
    .line 237
    .line 238
    aput-object v1, v3, v11

    .line 239
    .line 240
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 241
    .line 242
    const-string v8, "onSceneCardChange"

    .line 243
    .line 244
    const-class v13, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment$SceneCardChangeEvent;

    .line 245
    .line 246
    invoke-direct {v1, v8, v13, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 247
    .line 248
    .line 249
    const/4 v8, 0x1

    .line 250
    aput-object v1, v3, v8

    .line 251
    .line 252
    const-class v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment;

    .line 253
    .line 254
    invoke-direct {v0, v1, v8, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 255
    .line 256
    .line 257
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 258
    .line 259
    .line 260
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 261
    .line 262
    new-array v1, v8, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 263
    .line 264
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 265
    .line 266
    const-string v13, "finishCurActivity"

    .line 267
    .line 268
    const-class v8, Lcom/intsig/camscanner/newsign/done/SignDoneActivity$FinishImageScannerActivityEvent;

    .line 269
    .line 270
    invoke-direct {v3, v13, v8, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 271
    .line 272
    .line 273
    aput-object v3, v1, v11

    .line 274
    .line 275
    const-class v3, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 276
    .line 277
    const/4 v11, 0x1

    .line 278
    invoke-direct {v0, v3, v11, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 279
    .line 280
    .line 281
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 282
    .line 283
    .line 284
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 285
    .line 286
    new-array v1, v11, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 287
    .line 288
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 289
    .line 290
    const-string v11, "onInsertFavorableEvent"

    .line 291
    .line 292
    move-object/from16 v17, v8

    .line 293
    .line 294
    const-class v8, Lcom/intsig/camscanner/purchase/FavorableManager$FavorableEvent;

    .line 295
    .line 296
    invoke-direct {v3, v11, v8, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 297
    .line 298
    .line 299
    const/4 v8, 0x0

    .line 300
    aput-object v3, v1, v8

    .line 301
    .line 302
    const-class v3, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;

    .line 303
    .line 304
    const/4 v11, 0x1

    .line 305
    invoke-direct {v0, v3, v11, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 306
    .line 307
    .line 308
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 309
    .line 310
    .line 311
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 312
    .line 313
    const/4 v1, 0x3

    .line 314
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 315
    .line 316
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 317
    .line 318
    const-string v11, "notifyUserTransferResult"

    .line 319
    .line 320
    move-object/from16 v18, v14

    .line 321
    .line 322
    const-class v14, Lcom/intsig/camscanner/eventbus/TransferToOfficeEvent;

    .line 323
    .line 324
    invoke-direct {v1, v11, v14, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 325
    .line 326
    .line 327
    aput-object v1, v3, v8

    .line 328
    .line 329
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 330
    .line 331
    invoke-direct {v1, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 332
    .line 333
    .line 334
    const/4 v4, 0x1

    .line 335
    aput-object v1, v3, v4

    .line 336
    .line 337
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 338
    .line 339
    const-string v5, "onReceiveSyncWechatFile"

    .line 340
    .line 341
    const-class v8, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFile;

    .line 342
    .line 343
    invoke-direct {v1, v5, v8, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 344
    .line 345
    .line 346
    const/4 v5, 0x2

    .line 347
    aput-object v1, v3, v5

    .line 348
    .line 349
    const-class v1, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;

    .line 350
    .line 351
    invoke-direct {v0, v1, v4, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 352
    .line 353
    .line 354
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 355
    .line 356
    .line 357
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 358
    .line 359
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 360
    .line 361
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 362
    .line 363
    const-string v5, "onReceiveFinishCurrentActivityEvent"

    .line 364
    .line 365
    const-class v8, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity$PageFinishEvent;

    .line 366
    .line 367
    invoke-direct {v3, v5, v8, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 368
    .line 369
    .line 370
    const/4 v5, 0x0

    .line 371
    aput-object v3, v1, v5

    .line 372
    .line 373
    const-class v3, Lcom/intsig/camscanner/newsign/share/ShareByAppActivity;

    .line 374
    .line 375
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 376
    .line 377
    .line 378
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 379
    .line 380
    .line 381
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 382
    .line 383
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 384
    .line 385
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 386
    .line 387
    const-string v8, "onReceiveConvertResult"

    .line 388
    .line 389
    const-class v11, Lcom/intsig/webview/data/ConvertData;

    .line 390
    .line 391
    invoke-direct {v3, v8, v11, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 392
    .line 393
    .line 394
    aput-object v3, v1, v5

    .line 395
    .line 396
    const-class v3, Lcom/intsig/camscanner/pdf/external/PdfToCsBaseActivity;

    .line 397
    .line 398
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 399
    .line 400
    .line 401
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 402
    .line 403
    .line 404
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 405
    .line 406
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 407
    .line 408
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 409
    .line 410
    const-string v8, "updateInvoiceResult"

    .line 411
    .line 412
    const-class v11, Lcom/intsig/camscanner/capture/invoice/UpdateInvoiceResultEvent;

    .line 413
    .line 414
    invoke-direct {v3, v8, v11, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 415
    .line 416
    .line 417
    aput-object v3, v1, v5

    .line 418
    .line 419
    const-class v3, Lcom/intsig/camscanner/capture/invoice/fragment/InvoiceResultFragment;

    .line 420
    .line 421
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 422
    .line 423
    .line 424
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 425
    .line 426
    .line 427
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 428
    .line 429
    const/4 v1, 0x4

    .line 430
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 431
    .line 432
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 433
    .line 434
    invoke-direct {v1, v6, v7, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 435
    .line 436
    .line 437
    aput-object v1, v3, v5

    .line 438
    .line 439
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 440
    .line 441
    invoke-direct {v1, v9, v12, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 442
    .line 443
    .line 444
    aput-object v1, v3, v4

    .line 445
    .line 446
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 447
    .line 448
    invoke-direct {v1, v15, v2, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 449
    .line 450
    .line 451
    const/4 v5, 0x2

    .line 452
    aput-object v1, v3, v5

    .line 453
    .line 454
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 455
    .line 456
    const-string v5, "onInviteRewardGiftReceived"

    .line 457
    .line 458
    const-class v6, Lcom/intsig/camscanner/attention/AppUpdatePropertyNotice;

    .line 459
    .line 460
    invoke-direct {v1, v5, v6, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 461
    .line 462
    .line 463
    const/4 v5, 0x3

    .line 464
    aput-object v1, v3, v5

    .line 465
    .line 466
    const-class v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 467
    .line 468
    invoke-direct {v0, v1, v4, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 469
    .line 470
    .line 471
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 472
    .line 473
    .line 474
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 475
    .line 476
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 477
    .line 478
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 479
    .line 480
    const-string v5, "showGpRedeem"

    .line 481
    .line 482
    const-class v6, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity$StartESignActivityEvent;

    .line 483
    .line 484
    invoke-direct {v3, v5, v6, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 485
    .line 486
    .line 487
    const/4 v5, 0x0

    .line 488
    aput-object v3, v1, v5

    .line 489
    .line 490
    const-class v3, Lcom/intsig/camscanner/newsign/main/activity/ESignMainActivity;

    .line 491
    .line 492
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 493
    .line 494
    .line 495
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 496
    .line 497
    .line 498
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 499
    .line 500
    const/4 v1, 0x3

    .line 501
    new-array v3, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 502
    .line 503
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 504
    .line 505
    const-string v5, "copyOrMoveDoc"

    .line 506
    .line 507
    const-class v6, Lcom/intsig/camscanner/pagelist/newpagelist/data/CopyOrMoveDocEvent;

    .line 508
    .line 509
    const/4 v8, 0x0

    .line 510
    const/4 v9, 0x1

    .line 511
    move-object v4, v1

    .line 512
    move-object v7, v10

    .line 513
    move-object/from16 v11, v17

    .line 514
    .line 515
    invoke-direct/range {v4 .. v9}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;IZ)V

    .line 516
    .line 517
    .line 518
    const/4 v4, 0x0

    .line 519
    aput-object v1, v3, v4

    .line 520
    .line 521
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 522
    .line 523
    const-string v5, "fromGuideScanKit"

    .line 524
    .line 525
    const-class v6, Lcom/intsig/camscanner/eventbus/ScanKitEvent;

    .line 526
    .line 527
    move-object v4, v1

    .line 528
    invoke-direct/range {v4 .. v9}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;IZ)V

    .line 529
    .line 530
    .line 531
    const/4 v4, 0x1

    .line 532
    aput-object v1, v3, v4

    .line 533
    .line 534
    new-instance v1, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 535
    .line 536
    const-string v5, "onReceiveImage2Office"

    .line 537
    .line 538
    const-class v6, Lcom/intsig/camscanner/office_doc/data/ImageToOfficeEvent;

    .line 539
    .line 540
    move-object v4, v1

    .line 541
    invoke-direct/range {v4 .. v9}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;IZ)V

    .line 542
    .line 543
    .line 544
    const/4 v4, 0x2

    .line 545
    aput-object v1, v3, v4

    .line 546
    .line 547
    const-class v1, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 548
    .line 549
    const/4 v4, 0x1

    .line 550
    invoke-direct {v0, v1, v4, v3}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 551
    .line 552
    .line 553
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 554
    .line 555
    .line 556
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 557
    .line 558
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 559
    .line 560
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 561
    .line 562
    invoke-direct {v3, v13, v11, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 563
    .line 564
    .line 565
    const/4 v5, 0x0

    .line 566
    aput-object v3, v1, v5

    .line 567
    .line 568
    const-class v3, Lcom/intsig/camscanner/capture/CaptureRefactorActivity;

    .line 569
    .line 570
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 571
    .line 572
    .line 573
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 574
    .line 575
    .line 576
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 577
    .line 578
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 579
    .line 580
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 581
    .line 582
    const-string v6, "onCloseWebView"

    .line 583
    .line 584
    const-class v7, Lcom/intsig/camscanner/web/CloseWebView;

    .line 585
    .line 586
    invoke-direct {v3, v6, v7, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 587
    .line 588
    .line 589
    aput-object v3, v1, v5

    .line 590
    .line 591
    const-class v3, Lcom/intsig/camscanner/settings/FeedBackListFragment;

    .line 592
    .line 593
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 594
    .line 595
    .line 596
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 597
    .line 598
    .line 599
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 600
    .line 601
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 602
    .line 603
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 604
    .line 605
    move-object/from16 v7, v16

    .line 606
    .line 607
    move-object/from16 v6, v18

    .line 608
    .line 609
    invoke-direct {v3, v6, v7, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 610
    .line 611
    .line 612
    aput-object v3, v1, v5

    .line 613
    .line 614
    const-class v3, Lcom/intsig/camscanner/pagedetail/PageDetailFragment;

    .line 615
    .line 616
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 617
    .line 618
    .line 619
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 620
    .line 621
    .line 622
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 623
    .line 624
    const/16 v1, 0xa

    .line 625
    .line 626
    new-array v1, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 627
    .line 628
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 629
    .line 630
    const-string v4, "onReceiveCancelShareDirEvent"

    .line 631
    .line 632
    const-class v6, Lcom/intsig/camscanner/attention/CancelShareDirEvent;

    .line 633
    .line 634
    invoke-direct {v3, v4, v6, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 635
    .line 636
    .line 637
    aput-object v3, v1, v5

    .line 638
    .line 639
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 640
    .line 641
    const-string v4, "onReceiveExitShareDirEvent"

    .line 642
    .line 643
    const-class v5, Lcom/intsig/camscanner/attention/ExitShareDirEvent;

    .line 644
    .line 645
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 646
    .line 647
    .line 648
    const/4 v4, 0x1

    .line 649
    aput-object v3, v1, v4

    .line 650
    .line 651
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 652
    .line 653
    const-string v4, "onReceiveOffice2Pdf"

    .line 654
    .line 655
    const-class v5, Lcom/intsig/camscanner/office_doc/data/OfficeToPdfEvent;

    .line 656
    .line 657
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 658
    .line 659
    .line 660
    const/4 v4, 0x2

    .line 661
    aput-object v3, v1, v4

    .line 662
    .line 663
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 664
    .line 665
    const-string v4, "onReceivePdf2Office"

    .line 666
    .line 667
    const-class v5, Lcom/intsig/camscanner/office_doc/data/PdfToOfficeEvent;

    .line 668
    .line 669
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 670
    .line 671
    .line 672
    const/4 v4, 0x3

    .line 673
    aput-object v3, v1, v4

    .line 674
    .line 675
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 676
    .line 677
    const-string v4, "onReceiveImage2Office"

    .line 678
    .line 679
    const-class v5, Lcom/intsig/camscanner/office_doc/data/ImageToOfficeEvent;

    .line 680
    .line 681
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 682
    .line 683
    .line 684
    const/4 v4, 0x4

    .line 685
    aput-object v3, v1, v4

    .line 686
    .line 687
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 688
    .line 689
    const-string v4, "onReceivePdfImportSuccess"

    .line 690
    .line 691
    const-class v5, Lcom/intsig/camscanner/office_doc/PdfImportSuccessEvent;

    .line 692
    .line 693
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 694
    .line 695
    .line 696
    const/4 v4, 0x5

    .line 697
    aput-object v3, v1, v4

    .line 698
    .line 699
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 700
    .line 701
    const-string v4, "onReceiveAdapterUpdate"

    .line 702
    .line 703
    const-class v5, Lcom/intsig/camscanner/office_doc/UpdateMainAdapterEvent;

    .line 704
    .line 705
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 706
    .line 707
    .line 708
    const/4 v4, 0x6

    .line 709
    aput-object v3, v1, v4

    .line 710
    .line 711
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 712
    .line 713
    const-string v4, "onReceiveDocImportTip"

    .line 714
    .line 715
    const-class v5, Lcom/intsig/camscanner/office_doc/DocImportTipEvent;

    .line 716
    .line 717
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 718
    .line 719
    .line 720
    const/4 v4, 0x7

    .line 721
    aput-object v3, v1, v4

    .line 722
    .line 723
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 724
    .line 725
    const-string v4, "onMainHomePageChange"

    .line 726
    .line 727
    const-class v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$MainHomeBottomIndexChangeEvent;

    .line 728
    .line 729
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 730
    .line 731
    .line 732
    const/16 v4, 0x8

    .line 733
    .line 734
    aput-object v3, v1, v4

    .line 735
    .line 736
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 737
    .line 738
    const-string v4, "showCloudSpaceOverLimitForInvited"

    .line 739
    .line 740
    const-class v5, Lcom/intsig/camscanner/mainmenu/mainactivity/ShareDirOwnerCloudSpaceOverLimitEvent;

    .line 741
    .line 742
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 743
    .line 744
    .line 745
    const/16 v4, 0x9

    .line 746
    .line 747
    aput-object v3, v1, v4

    .line 748
    .line 749
    const-class v3, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 750
    .line 751
    const/4 v4, 0x1

    .line 752
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 753
    .line 754
    .line 755
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 756
    .line 757
    .line 758
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 759
    .line 760
    const/4 v1, 0x5

    .line 761
    new-array v1, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 762
    .line 763
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 764
    .line 765
    const-string v4, "onMainHomePageChange"

    .line 766
    .line 767
    const-class v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$MainHomeBottomIndexChangeEvent;

    .line 768
    .line 769
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 770
    .line 771
    .line 772
    const/4 v4, 0x0

    .line 773
    aput-object v3, v1, v4

    .line 774
    .line 775
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 776
    .line 777
    const-string v4, "onSyncSetting"

    .line 778
    .line 779
    const-class v5, Lcom/intsig/camscanner/eventbus/SyncSettingEvent;

    .line 780
    .line 781
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 782
    .line 783
    .line 784
    const/4 v4, 0x1

    .line 785
    aput-object v3, v1, v4

    .line 786
    .line 787
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 788
    .line 789
    const-string v4, "onReceiveAdapterUpdate"

    .line 790
    .line 791
    const-class v5, Lcom/intsig/camscanner/office_doc/UpdateMainAdapterEvent;

    .line 792
    .line 793
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 794
    .line 795
    .line 796
    const/4 v4, 0x2

    .line 797
    aput-object v3, v1, v4

    .line 798
    .line 799
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 800
    .line 801
    const-string v4, "onReceiveOffice2Pdf"

    .line 802
    .line 803
    const-class v5, Lcom/intsig/camscanner/office_doc/data/OfficeToPdfEvent;

    .line 804
    .line 805
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 806
    .line 807
    .line 808
    const/4 v4, 0x3

    .line 809
    aput-object v3, v1, v4

    .line 810
    .line 811
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 812
    .line 813
    const-string v4, "onImportWechatDoc"

    .line 814
    .line 815
    const-class v5, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/ImportWechatDoc;

    .line 816
    .line 817
    invoke-direct {v3, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 818
    .line 819
    .line 820
    const/4 v4, 0x4

    .line 821
    aput-object v3, v1, v4

    .line 822
    .line 823
    const-class v3, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 824
    .line 825
    const/4 v4, 0x1

    .line 826
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 827
    .line 828
    .line 829
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 830
    .line 831
    .line 832
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 833
    .line 834
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 835
    .line 836
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 837
    .line 838
    const-class v5, Lcom/intsig/camscanner/eventbus/ConnectReceiverEvent;

    .line 839
    .line 840
    sget-object v6, Lorg/greenrobot/eventbus/ThreadMode;->BACKGROUND:Lorg/greenrobot/eventbus/ThreadMode;

    .line 841
    .line 842
    const-string v7, "onConnectReceiver"

    .line 843
    .line 844
    invoke-direct {v3, v7, v5, v6}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 845
    .line 846
    .line 847
    const/4 v5, 0x0

    .line 848
    aput-object v3, v1, v5

    .line 849
    .line 850
    const-class v3, Lcom/intsig/camscanner/tsapp/sync/ConnectReceiverLifecycle;

    .line 851
    .line 852
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 853
    .line 854
    .line 855
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 856
    .line 857
    .line 858
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 859
    .line 860
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 861
    .line 862
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 863
    .line 864
    const-string v6, "commandMsg"

    .line 865
    .line 866
    const-class v7, Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;

    .line 867
    .line 868
    invoke-direct {v3, v6, v7, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 869
    .line 870
    .line 871
    aput-object v3, v1, v5

    .line 872
    .line 873
    const-class v3, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;

    .line 874
    .line 875
    invoke-direct {v0, v3, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 876
    .line 877
    .line 878
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 879
    .line 880
    .line 881
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 882
    .line 883
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 884
    .line 885
    new-instance v3, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 886
    .line 887
    invoke-direct {v3, v15, v2, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 888
    .line 889
    .line 890
    aput-object v3, v1, v5

    .line 891
    .line 892
    const-class v2, Lcom/intsig/camscanner/newsign/main/me/SignMeFragment;

    .line 893
    .line 894
    invoke-direct {v0, v2, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 895
    .line 896
    .line 897
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 898
    .line 899
    .line 900
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 901
    .line 902
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 903
    .line 904
    new-instance v2, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 905
    .line 906
    const-string v3, "onCheckEduAuthEvent"

    .line 907
    .line 908
    const-class v6, Lcom/intsig/camscanner/settings/newsettings/entity/EduAuthEvent;

    .line 909
    .line 910
    invoke-direct {v2, v3, v6, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 911
    .line 912
    .line 913
    aput-object v2, v1, v5

    .line 914
    .line 915
    const-class v2, Lcom/intsig/camscanner/settings/newsettings/fragment/MyAccountFragment;

    .line 916
    .line 917
    invoke-direct {v0, v2, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 918
    .line 919
    .line 920
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 921
    .line 922
    .line 923
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 924
    .line 925
    new-array v1, v4, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 926
    .line 927
    new-instance v2, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 928
    .line 929
    const-string v3, "receivePageFinishEvent"

    .line 930
    .line 931
    const-class v6, Lcom/intsig/camscanner/newsign/esign/ESignActivity$PageFinishEvent;

    .line 932
    .line 933
    invoke-direct {v2, v3, v6, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 934
    .line 935
    .line 936
    aput-object v2, v1, v5

    .line 937
    .line 938
    const-class v2, Lcom/intsig/camscanner/newsign/esign/ESignActivity;

    .line 939
    .line 940
    invoke-direct {v0, v2, v4, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 941
    .line 942
    .line 943
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 944
    .line 945
    .line 946
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 947
    .line 948
    const/4 v1, 0x2

    .line 949
    new-array v1, v1, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 950
    .line 951
    new-instance v2, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 952
    .line 953
    const-string v5, "fromGuideScanKit"

    .line 954
    .line 955
    const-class v6, Lcom/intsig/camscanner/eventbus/ScanKitEvent;

    .line 956
    .line 957
    move-object v4, v2

    .line 958
    move-object v7, v10

    .line 959
    invoke-direct/range {v4 .. v9}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;IZ)V

    .line 960
    .line 961
    .line 962
    const/4 v3, 0x0

    .line 963
    aput-object v2, v1, v3

    .line 964
    .line 965
    new-instance v2, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 966
    .line 967
    const-string v5, "copyOrMoveDoc"

    .line 968
    .line 969
    const-class v6, Lcom/intsig/camscanner/pagelist/newpagelist/data/CopyOrMoveDocEvent;

    .line 970
    .line 971
    move-object v4, v2

    .line 972
    invoke-direct/range {v4 .. v9}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;IZ)V

    .line 973
    .line 974
    .line 975
    const/4 v3, 0x1

    .line 976
    aput-object v2, v1, v3

    .line 977
    .line 978
    const-class v2, Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 979
    .line 980
    invoke-direct {v0, v2, v3, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 981
    .line 982
    .line 983
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 984
    .line 985
    .line 986
    new-instance v0, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;

    .line 987
    .line 988
    new-array v1, v3, [Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 989
    .line 990
    new-instance v2, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;

    .line 991
    .line 992
    const-string v4, "receivePageFinishEvent"

    .line 993
    .line 994
    const-class v5, Lcom/intsig/camscanner/newsign/esign/ESignActivity$PageFinishEvent;

    .line 995
    .line 996
    invoke-direct {v2, v4, v5, v10}, Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Lorg/greenrobot/eventbus/ThreadMode;)V

    .line 997
    .line 998
    .line 999
    const/4 v4, 0x0

    .line 1000
    aput-object v2, v1, v4

    .line 1001
    .line 1002
    const-class v2, Lcom/intsig/camscanner/newsign/detail/SignDetailActivity;

    .line 1003
    .line 1004
    invoke-direct {v0, v2, v3, v1}, Lorg/greenrobot/eventbus/meta/SimpleSubscriberInfo;-><init>(Ljava/lang/Class;Z[Lorg/greenrobot/eventbus/meta/SubscriberMethodInfo;)V

    .line 1005
    .line 1006
    .line 1007
    invoke-static {v0}, Lcom/intsig/camscanner/CsEventBusIndex;->〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V

    .line 1008
    .line 1009
    .line 1010
    return-void
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static 〇o00〇〇Oo(Lorg/greenrobot/eventbus/meta/SubscriberInfo;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/CsEventBusIndex;->〇080:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {p0}, Lorg/greenrobot/eventbus/meta/SubscriberInfo;->〇o00〇〇Oo()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public 〇080(Ljava/lang/Class;)Lorg/greenrobot/eventbus/meta/SubscriberInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lorg/greenrobot/eventbus/meta/SubscriberInfo;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/CsEventBusIndex;->〇080:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lorg/greenrobot/eventbus/meta/SubscriberInfo;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    return-object p1

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

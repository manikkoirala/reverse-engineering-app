.class public Lcom/intsig/camscanner/background_batch/client/BackScanClient;
.super Ljava/lang/Object;
.source "BackScanClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;,
        Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;,
        Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;,
        Lcom/intsig/camscanner/background_batch/client/BackScanClient$BackScanClientImpl;
    }
.end annotation


# instance fields
.field private O8:Z

.field private final OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

.field private OO0o〇〇〇〇0:Ljava/lang/Thread;

.field private Oo08:Ljava/util/concurrent/ThreadPoolExecutor;

.field private final OoO8:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

.field private final o800o8O:[B

.field private volatile oO80:Z

.field private o〇0:Ljava/util/concurrent/ThreadPoolExecutor;

.field private volatile 〇080:Landroid/os/HandlerThread;

.field private 〇0〇O0088o:Ljava/util/concurrent/Semaphore;

.field private final 〇80〇808〇O:[B

.field private final 〇8o8o〇:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O00:Z

.field private final 〇O888o0o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O〇:Lcom/intsig/callback/Callback0;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Landroid/os/Handler;

.field private 〇o〇:Z

.field private final 〇〇808〇:[B

.field private final 〇〇888:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo08:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 8
    .line 9
    new-instance v1, Landroid/util/LruCache;

    .line 10
    .line 11
    const/16 v2, 0x20

    .line 12
    .line 13
    invoke-direct {v1, v2}, Landroid/util/LruCache;-><init>(I)V

    .line 14
    .line 15
    .line 16
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oO80:Z

    .line 20
    .line 21
    new-array v2, v1, [B

    .line 22
    .line 23
    iput-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇80〇808〇O:[B

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 26
    .line 27
    new-instance v0, Loo/oo88o8O;

    .line 28
    .line 29
    invoke-direct {v0}, Loo/oo88o8O;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇8o8o〇:Ljava/util/Comparator;

    .line 33
    .line 34
    new-instance v2, Ljava/util/concurrent/PriorityBlockingQueue;

    .line 35
    .line 36
    const/16 v3, 0xa

    .line 37
    .line 38
    invoke-direct {v2, v3, v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    .line 39
    .line 40
    .line 41
    iput-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 44
    .line 45
    const-wide/16 v2, -0x1

    .line 46
    .line 47
    invoke-direct {v0, v2, v3}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;-><init>(J)V

    .line 48
    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 51
    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 53
    .line 54
    new-array v0, v1, [B

    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇808〇:[B

    .line 57
    .line 58
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00:Z

    .line 59
    .line 60
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 67
    .line 68
    new-instance v0, Ljava/util/concurrent/Semaphore;

    .line 69
    .line 70
    const/4 v2, 0x1

    .line 71
    invoke-direct {v0, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 72
    .line 73
    .line 74
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 75
    .line 76
    new-instance v0, Ljava/util/HashSet;

    .line 77
    .line 78
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 79
    .line 80
    .line 81
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 82
    .line 83
    new-array v0, v1, [B

    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 86
    .line 87
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 88
    .line 89
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 90
    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private O08000(ILcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            "Lcom/intsig/camscanner/service/BackScanImageData;",
            "Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;",
            ")",
            "Ljava/util/concurrent/Future<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            ">;"
        }
    .end annotation

    const-string v0, "BackScanClient"

    .line 1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startImageProgress pageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " backScanPageModel.imageRawPath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-static {v0}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o〇()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/ImageDaoUtil;->〇080(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 5
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->o〇0()I

    move-result v0

    iput v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    .line 6
    :goto_0
    new-instance v5, Lcom/intsig/camscanner/util/ImageProgressClient;

    invoke-direct {v5}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>()V

    .line 7
    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "BackScanClient"

    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startImageProgress  pageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " backScanPageModel.imagePath="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    iget-wide v1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->O0o〇〇Oo(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O000()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".jpg"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 11
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/camscanner/util/ImageProgressClient;->reset()Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object v0

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/util/ImageProgressClient;->setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 13
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇888:I

    .line 15
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setPageIndex(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇O〇:Ljava/lang/String;

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveOnlyTrimImage(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-boolean v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->O8:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v0, 0x1

    .line 17
    :goto_2
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 18
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageBorder([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 19
    invoke-static {v0, v1}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->Oo08:I

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇O8o08O:Ljava/lang/String;

    .line 22
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setTrimmedPaperPath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-boolean v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇:Z

    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedDeBlurImage(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    .line 24
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇080()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setBrightness(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    .line 25
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o00〇〇Oo()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/ImageProgressClient;->setContrast(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    .line 26
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇〇888()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->setDetail(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-boolean p3, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->Oooo8o0〇:Z

    .line 27
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    .line 28
    invoke-static {}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->getNeedTrimWhenNoBorder()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    move-result-object p1

    iget-boolean p3, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇808〇:Z

    .line 29
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedAutoDeMoire(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    invoke-virtual {p1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;

    move-result-object v3

    const/4 p1, 0x0

    if-eqz v3, :cond_6

    .line 31
    invoke-virtual {v3}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_6

    .line 32
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0()I

    move-result p3

    if-lez p3, :cond_6

    .line 33
    :goto_3
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_4

    const-wide/16 v0, 0x64

    .line 34
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p3

    const-string v0, "BackScanClient"

    .line 35
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3

    .line 37
    :cond_4
    sget-object p3, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-static {p3, v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00〇8(Landroid/content/Context;J)I

    move-result p3

    if-nez p3, :cond_5

    const-string p3, "BackScanClient"

    .line 38
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "startImageProgress pageId="

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-virtual {p4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, " ImageStatus  Image.STATUS_NORMAL"

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 39
    :cond_5
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    monitor-enter p3

    .line 40
    :try_start_1
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 41
    monitor-exit p3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    :try_start_2
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_1
    move-exception p1

    .line 43
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Thread;->interrupt()V

    const-string p3, "BackScanClient"

    .line 44
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p3, Lcom/intsig/camscanner/background_batch/client/〇080;

    move-object v1, p3

    move-object v2, p0

    move-object v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/background_batch/client/〇080;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/okbinder/ServerInfo;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)V

    invoke-interface {p1, p3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    .line 46
    :try_start_3
    monitor-exit p3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    .line 47
    :cond_6
    sget-object p3, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-static {p3, v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00〇8(Landroid/content/Context;J)I

    move-result p3

    if-nez p3, :cond_7

    const-string p3, "BackScanClient"

    .line 48
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "startImageProgress pageId="

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    invoke-virtual {p4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, " ImageStatus  Image.STATUS_NORMAL="

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 49
    :cond_7
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "BACK_PUSH_"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "BackScanClient"

    .line 50
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startImageProgress: executeProgress uuid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "batch_process_issue_count"

    .line 51
    new-instance v1, Loo/〇O00;

    invoke-direct {v1, v5, p3, p2}, Loo/〇O00;-><init>(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    invoke-static {v0, v1}, Lcom/intsig/camscanner/booksplitter/Util/NoResponseRecorder;->oO80(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    .line 52
    invoke-interface {p4, v5}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;->〇080(Lcom/intsig/camscanner/util/ImageProgressClient;)V

    return-object p1
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0000OOO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private synthetic O8ooOoo〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 18
    .line 19
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 20
    .line 21
    iget-wide v4, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 22
    .line 23
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;->〇o〇(JJ)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-boolean p1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->o〇0:Z

    .line 28
    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    iget-object p1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 32
    .line 33
    invoke-static {p1}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->〇0〇O0088o(Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static synthetic O8〇o(Lcom/intsig/okbinder/ServerInfo;Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    move-object v0, p0

    .line 6
    check-cast v0, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;

    .line 7
    .line 8
    iget-object v3, p3, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇8O0〇8:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 9
    .line 10
    iget-object v4, p3, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇O00:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    sget-object p0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 14
    .line 15
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    new-instance v6, Loo/〇〇808〇;

    .line 19
    .line 20
    invoke-direct {v6, p0}, Loo/〇〇808〇;-><init>(Lcom/intsig/camscanner/paper/PaperUtil;)V

    .line 21
    .line 22
    .line 23
    sget-object p0, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇080:Lcom/intsig/camscanner/demoire/DeMoireManager;

    .line 24
    .line 25
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    new-instance v7, Loo/OoO8;

    .line 29
    .line 30
    invoke-direct {v7, p0}, Loo/OoO8;-><init>(Lcom/intsig/camscanner/demoire/DeMoireManager;)V

    .line 31
    .line 32
    .line 33
    sget-object p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 34
    .line 35
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    new-instance v8, Loo/o800o8O;

    .line 39
    .line 40
    invoke-direct {v8, p0}, Loo/o800o8O;-><init>(Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;)V

    .line 41
    .line 42
    .line 43
    sget-object p0, Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;->〇080:Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;

    .line 44
    .line 45
    invoke-static {p0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    new-instance v9, Loo/〇O888o0o;

    .line 49
    .line 50
    invoke-direct {v9, p0}, Loo/〇O888o0o;-><init>(Lcom/intsig/camscanner/imagescanner/CloudEnhanceUtil;)V

    .line 51
    .line 52
    .line 53
    move-object v1, p1

    .line 54
    move-object v2, p2

    .line 55
    invoke-interface/range {v0 .. v9}, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    return-object p0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/okbinder/ServerInfo;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇00〇8(Lcom/intsig/okbinder/ServerInfo;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method private synthetic OOO〇O0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OOO〇O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private Oo8Oo00oo(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V
    .locals 6

    .line 1
    iget-wide v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 2
    .line 3
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00(J)Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-wide v1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 14
    .line 15
    iget-wide v3, v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 16
    .line 17
    cmp-long v5, v1, v3

    .line 18
    .line 19
    if-nez v5, :cond_0

    .line 20
    .line 21
    if-eq v0, p1, :cond_2

    .line 22
    .line 23
    new-instance v1, Loo/〇00;

    .line 24
    .line 25
    invoke-direct {v1, p0}, Loo/〇00;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇〇888(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/lang/Runnable;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    if-eq v0, p1, :cond_2

    .line 37
    .line 38
    new-instance v1, Loo/O〇8O8〇008;

    .line 39
    .line 40
    invoke-direct {v1, p0, v0}, Loo/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇〇888(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/lang/Runnable;)V

    .line 44
    .line 45
    .line 46
    :cond_2
    :goto_0
    iget-wide v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 47
    .line 48
    const-wide/16 v2, 0x0

    .line 49
    .line 50
    cmp-long v4, v0, v2

    .line 51
    .line 52
    if-lez v4, :cond_3

    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 55
    .line 56
    if-eqz v0, :cond_3

    .line 57
    .line 58
    iget-wide v0, v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 59
    .line 60
    cmp-long v4, v0, v2

    .line 61
    .line 62
    if-lez v4, :cond_3

    .line 63
    .line 64
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 65
    .line 66
    cmp-long p1, v0, v2

    .line 67
    .line 68
    if-eqz p1, :cond_3

    .line 69
    .line 70
    const-string p1, "BackScanClient"

    .line 71
    .line 72
    const-string v0, "reAddBackScanDocModel auto interruptHandleCurrentDoc"

    .line 73
    .line 74
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const/4 p1, 0x1

    .line 78
    iput-boolean p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00:Z

    .line 79
    .line 80
    :cond_3
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public static OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BackScanClientImpl;->〇080()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇8O8〇008(Landroid/os/Message;)Z
    .locals 9

    .line 1
    iget v0, p1, Landroid/os/Message;->what:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    packed-switch v0, :pswitch_data_0

    .line 5
    .line 6
    .line 7
    goto :goto_1

    .line 8
    :pswitch_0
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 11
    .line 12
    .line 13
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 17
    .line 18
    instance-of v0, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    check-cast p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o8oO〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 31
    .line 32
    instance-of v0, p1, Ljava/lang/Long;

    .line 33
    .line 34
    if-eqz v0, :cond_1

    .line 35
    .line 36
    new-instance v0, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 37
    .line 38
    check-cast p1, Ljava/lang/Long;

    .line 39
    .line 40
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 41
    .line 42
    .line 43
    move-result-wide v3

    .line 44
    const-wide/16 v5, -0x1

    .line 45
    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 47
    .line 48
    .line 49
    move-result-wide v7

    .line 50
    move-object v2, v0

    .line 51
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;-><init>(JJJ)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o8oO〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;)V

    .line 55
    .line 56
    .line 57
    :cond_1
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :pswitch_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 61
    .line 62
    instance-of v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 63
    .line 64
    if-eqz v0, :cond_4

    .line 65
    .line 66
    check-cast p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-nez v0, :cond_2

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 76
    .line 77
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 78
    .line 79
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-virtual {v0, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    check-cast v0, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 88
    .line 89
    if-eqz v0, :cond_3

    .line 90
    .line 91
    iget-wide v2, v0, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o00〇〇Oo:J

    .line 92
    .line 93
    iget-wide v4, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 94
    .line 95
    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->OO0o〇〇〇〇0(JJ)V

    .line 96
    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 99
    .line 100
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 101
    .line 102
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    invoke-virtual {v0, v2}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    .line 108
    .line 109
    :cond_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo8Oo00oo(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 110
    .line 111
    .line 112
    :cond_4
    iput-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 113
    .line 114
    :goto_0
    const/4 v1, 0x1

    .line 115
    :goto_1
    return v1

    .line 116
    nop

    .line 117
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private o0ooO(ILcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V
    .locals 16

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v8, p2

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "looperHandleImage docId="

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    iget-wide v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v9, "BackScanClient"

    .line 25
    .line 26
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_0

    .line 40
    .line 41
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    check-cast v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 46
    .line 47
    iget-wide v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 48
    .line 49
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;->O8(J)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    new-instance v1, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v2, "multiProcessImageType:"

    .line 63
    .line 64
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    if-lez v0, :cond_2

    .line 78
    .line 79
    iget-object v1, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 80
    .line 81
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->OO0o〇〇()V

    .line 82
    .line 83
    .line 84
    const/4 v1, 0x3

    .line 85
    if-lt v0, v1, :cond_1

    .line 86
    .line 87
    new-instance v0, Ljava/util/concurrent/Semaphore;

    .line 88
    .line 89
    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 90
    .line 91
    .line 92
    iput-object v0, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 93
    .line 94
    goto :goto_1

    .line 95
    :cond_1
    new-instance v1, Ljava/util/concurrent/Semaphore;

    .line 96
    .line 97
    invoke-direct {v1, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 98
    .line 99
    .line 100
    iput-object v1, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 101
    .line 102
    :cond_2
    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    .line 103
    .line 104
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .line 106
    .line 107
    new-instance v0, Ljava/util/ArrayList;

    .line 108
    .line 109
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .line 111
    .line 112
    new-instance v11, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 113
    .line 114
    iget-wide v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 115
    .line 116
    invoke-direct {v11, v1, v2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;-><init>(J)V

    .line 117
    .line 118
    .line 119
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 120
    .line 121
    .line 122
    move-result-wide v12

    .line 123
    const/4 v14, 0x0

    .line 124
    const/4 v15, 0x0

    .line 125
    :cond_3
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    if-lez v1, :cond_a

    .line 130
    .line 131
    iget-boolean v1, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00:Z

    .line 132
    .line 133
    if-eqz v1, :cond_4

    .line 134
    .line 135
    iput-boolean v14, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00:Z

    .line 136
    .line 137
    const-string v1, "looperHandleImage stop handle current doc"

    .line 138
    .line 139
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    goto/16 :goto_4

    .line 143
    .line 144
    :cond_4
    :goto_3
    iget-boolean v1, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 145
    .line 146
    if-eqz v1, :cond_5

    .line 147
    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    .line 149
    .line 150
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .line 152
    .line 153
    const-string v2, "pauseHandleImage="

    .line 154
    .line 155
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    iget-boolean v2, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 159
    .line 160
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    const-wide/16 v1, 0x1f4

    .line 171
    .line 172
    invoke-direct {v7, v1, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇8(J)V

    .line 173
    .line 174
    .line 175
    goto :goto_3

    .line 176
    :cond_5
    iget-boolean v1, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 177
    .line 178
    if-eqz v1, :cond_6

    .line 179
    .line 180
    const-string v1, "looperHandleImage stopHandleImage"

    .line 181
    .line 182
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    goto/16 :goto_4

    .line 186
    .line 187
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇80〇808〇O()Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 188
    .line 189
    .line 190
    move-result-object v4

    .line 191
    if-nez v4, :cond_7

    .line 192
    .line 193
    const-string v1, "looperHandleImage backScanPageModel == null"

    .line 194
    .line 195
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    goto :goto_2

    .line 199
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 200
    .line 201
    .line 202
    move-result-object v1

    .line 203
    iget-wide v2, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 204
    .line 205
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->oO80(Landroid/content/Context;J)Z

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    if-nez v1, :cond_8

    .line 210
    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    .line 212
    .line 213
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 214
    .line 215
    .line 216
    const-string v2, "looperHandleImage pageId="

    .line 217
    .line 218
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    iget-wide v2, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 222
    .line 223
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    const-string v2, " be deleted"

    .line 227
    .line 228
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    .line 230
    .line 231
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v1

    .line 235
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    goto :goto_2

    .line 239
    :cond_8
    iget-object v1, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 240
    .line 241
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 242
    .line 243
    .line 244
    move-result v1

    .line 245
    if-nez v1, :cond_9

    .line 246
    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    .line 248
    .line 249
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .line 251
    .line 252
    const-string v2, "looperHandleImage imageRawPath="

    .line 253
    .line 254
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    .line 256
    .line 257
    iget-object v2, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 258
    .line 259
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    const-string v2, " is not exist"

    .line 263
    .line 264
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    invoke-static {v9, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    .line 273
    .line 274
    goto/16 :goto_2

    .line 275
    .line 276
    :cond_9
    move-object/from16 v1, p0

    .line 277
    .line 278
    move/from16 v2, p1

    .line 279
    .line 280
    move-object/from16 v3, p2

    .line 281
    .line 282
    move-object v5, v10

    .line 283
    move-object v6, v11

    .line 284
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oo88o8O(ILcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)Ljava/util/concurrent/Future;

    .line 285
    .line 286
    .line 287
    move-result-object v1

    .line 288
    add-int/lit8 v15, v15, 0x1

    .line 289
    .line 290
    if-eqz v1, :cond_3

    .line 291
    .line 292
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    .line 294
    .line 295
    goto/16 :goto_2

    .line 296
    .line 297
    :cond_a
    :goto_4
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 298
    .line 299
    .line 300
    move-result-object v1

    .line 301
    :cond_b
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 302
    .line 303
    .line 304
    move-result v0

    .line 305
    if-eqz v0, :cond_c

    .line 306
    .line 307
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 308
    .line 309
    .line 310
    move-result-object v0

    .line 311
    check-cast v0, Ljava/util/concurrent/Future;

    .line 312
    .line 313
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    .line 314
    .line 315
    .line 316
    move-result-object v0

    .line 317
    check-cast v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 318
    .line 319
    if-eqz v0, :cond_b

    .line 320
    .line 321
    const-wide/16 v2, 0x0

    .line 322
    .line 323
    iput-wide v2, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->oO80:J

    .line 324
    .line 325
    invoke-virtual {v11, v0}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    .line 327
    .line 328
    goto :goto_5

    .line 329
    :catch_0
    move-exception v0

    .line 330
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object v0

    .line 334
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    .line 336
    .line 337
    goto :goto_5

    .line 338
    :catch_1
    move-exception v0

    .line 339
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 340
    .line 341
    .line 342
    move-result-object v0

    .line 343
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    .line 345
    .line 346
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 347
    .line 348
    .line 349
    move-result-object v0

    .line 350
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 351
    .line 352
    .line 353
    goto :goto_5

    .line 354
    :cond_c
    if-lez v15, :cond_d

    .line 355
    .line 356
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 357
    .line 358
    .line 359
    move-result-wide v0

    .line 360
    sub-long/2addr v0, v12

    .line 361
    invoke-direct {v7, v0, v1, v15}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇8〇0〇o〇O(JI)V

    .line 362
    .line 363
    .line 364
    :cond_d
    invoke-virtual {v11}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 365
    .line 366
    .line 367
    move-result v0

    .line 368
    if-lez v0, :cond_e

    .line 369
    .line 370
    const-string v0, "reAdd fail handle image"

    .line 371
    .line 372
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    .line 374
    .line 375
    const/4 v0, 0x0

    .line 376
    invoke-virtual {v8, v11, v0}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇〇888(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/lang/Runnable;)V

    .line 377
    .line 378
    .line 379
    :cond_e
    iget-object v0, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 380
    .line 381
    iput-object v0, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 382
    .line 383
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 384
    .line 385
    .line 386
    move-result-object v1

    .line 387
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 388
    .line 389
    .line 390
    move-result v0

    .line 391
    if-eqz v0, :cond_f

    .line 392
    .line 393
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 394
    .line 395
    .line 396
    move-result-object v0

    .line 397
    check-cast v0, Ljava/util/concurrent/Future;

    .line 398
    .line 399
    :try_start_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 400
    .line 401
    .line 402
    goto :goto_6

    .line 403
    :catch_2
    move-exception v0

    .line 404
    move-object v2, v0

    .line 405
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 406
    .line 407
    .line 408
    move-result-object v0

    .line 409
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    .line 411
    .line 412
    goto :goto_6

    .line 413
    :catch_3
    move-exception v0

    .line 414
    move-object v2, v0

    .line 415
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 416
    .line 417
    .line 418
    move-result-object v0

    .line 419
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    .line 421
    .line 422
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 423
    .line 424
    .line 425
    move-result-object v0

    .line 426
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 427
    .line 428
    .line 429
    goto :goto_6

    .line 430
    :cond_f
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    .line 431
    .line 432
    iget-wide v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 433
    .line 434
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->〇〇8O0〇8(J)V

    .line 435
    .line 436
    .line 437
    iget-object v0, v7, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 438
    .line 439
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 440
    .line 441
    .line 442
    move-result-object v0

    .line 443
    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 444
    .line 445
    .line 446
    move-result v1

    .line 447
    if-eqz v1, :cond_10

    .line 448
    .line 449
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 450
    .line 451
    .line 452
    move-result-object v1

    .line 453
    check-cast v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 454
    .line 455
    iget-wide v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 456
    .line 457
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;->〇080(J)V

    .line 458
    .line 459
    .line 460
    goto :goto_7

    .line 461
    :cond_10
    return-void
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
.end method

.method private o8()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oO80:Z

    .line 2
    .line 3
    const-string v1, "BackScanClient"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "pushUnprocessedPages last task not finish"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oO80:Z

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/background_batch/client/BackScanClient$1;

    .line 17
    .line 18
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$1;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o8oO〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-wide v1, v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 6
    .line 7
    iget-wide v3, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 8
    .line 9
    cmp-long v5, v1, v3

    .line 10
    .line 11
    if-nez v5, :cond_0

    .line 12
    .line 13
    iget-wide v1, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o〇:J

    .line 14
    .line 15
    iput-wide v1, v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 16
    .line 17
    iget-wide v3, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o00〇〇Oo:J

    .line 18
    .line 19
    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->OO0o〇〇〇〇0(JJ)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const-string v1, "BackScanClient"

    .line 30
    .line 31
    if-nez v0, :cond_1

    .line 32
    .line 33
    const-string v0, "updateDocAccessTimeImpl priorityQueue.size() == 0"

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 39
    .line 40
    iget-wide v1, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 41
    .line 42
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_1
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 51
    .line 52
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00(J)Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    if-nez v0, :cond_2

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 59
    .line 60
    iget-wide v1, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 61
    .line 62
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-virtual {v0, v1, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_2
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o〇:J

    .line 71
    .line 72
    iput-wide v2, v0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 73
    .line 74
    iget-wide v4, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o00〇〇Oo:J

    .line 75
    .line 76
    invoke-virtual {v0, v4, v5, v2, v3}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->OO0o〇〇〇〇0(JJ)V

    .line 77
    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 80
    .line 81
    invoke-virtual {v2, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->remove(Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 85
    .line 86
    invoke-virtual {v2, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 87
    .line 88
    .line 89
    const/4 v0, 0x1

    .line 90
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O00:Z

    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 93
    .line 94
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 95
    .line 96
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 97
    .line 98
    .line 99
    move-result-object v2

    .line 100
    invoke-virtual {v0, v2}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string v2, "updateDocAccessTimeImpl docId="

    .line 109
    .line 110
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇080:J

    .line 114
    .line 115
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oo〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oo88o8O(ILcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)Ljava/util/concurrent/Future;
    .locals 11
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            "Ljava/util/List<",
            "Ljava/util/concurrent/Future<",
            "*>;>;",
            "Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;",
            ")",
            "Ljava/util/concurrent/Future<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            ">;"
        }
    .end annotation

    .line 1
    move-object v8, p3

    .line 2
    iget-object v0, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 3
    .line 4
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x0

    .line 9
    const-string v2, "BackScanClient"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    iget-object v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 19
    .line 20
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v3, " is not exist "

    .line 24
    .line 25
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-object v1

    .line 36
    :cond_0
    new-instance v3, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 37
    .line 38
    iget-object v0, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 39
    .line 40
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    new-instance v9, Lcom/intsig/camscanner/service/BackScanImageData;

    .line 44
    .line 45
    invoke-direct {v9}, Lcom/intsig/camscanner/service/BackScanImageData;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    iget-wide v4, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 53
    .line 54
    invoke-static {v0, v4, v5, v9}, Lcom/intsig/camscanner/service/BackScanImageData;->〇80〇808〇O(Landroid/content/Context;JLcom/intsig/camscanner/service/BackScanImageData;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v9}, Lcom/intsig/camscanner/service/BackScanImageData;->oO80()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-nez v0, :cond_1

    .line 62
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string v3, "image pageId:"

    .line 69
    .line 70
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    iget-wide v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 74
    .line 75
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v3, " path:"

    .line 79
    .line 80
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    iget-object v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 84
    .line 85
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v3, " has handle"

    .line 89
    .line 90
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    return-object v1

    .line 101
    :cond_1
    new-instance v10, Lcom/intsig/camscanner/background_batch/client/〇o00〇〇Oo;

    .line 102
    .line 103
    move-object v0, v10

    .line 104
    move-object v1, p0

    .line 105
    move-object v2, p3

    .line 106
    move-object v4, v9

    .line 107
    move-object v5, p2

    .line 108
    move-object v6, p4

    .line 109
    move-object/from16 v7, p5

    .line 110
    .line 111
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/background_batch/client/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 112
    .line 113
    .line 114
    move-object v0, p0

    .line 115
    move v1, p1

    .line 116
    invoke-direct {p0, p1, p3, v9, v10}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O08000(ILcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)Ljava/util/concurrent/Future;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    return-object v1
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method private synthetic oo〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇oOO8O8(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/util/ImageProgressClient;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
.end method

.method private o〇O(Landroid/content/ContentValues;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 2
    .line 3
    iget-wide v1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v2, "(image_border =? "

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o〇()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    const-string v3, ""

    .line 28
    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    const-string v2, " or image_border IS NULL "

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    move-object v2, v3

    .line 35
    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v2, " or ("

    .line 39
    .line 40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v2, "image_border"

    .line 44
    .line 45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v4, " =? ) or ("

    .line 49
    .line 50
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v2, " =? )) and ("

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string v2, "image_rotation"

    .line 62
    .line 63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v4, " =? or "

    .line 67
    .line 68
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v2, " =? ) and ("

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string v5, "ori_rotation"

    .line 80
    .line 81
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v2, "enhance_mode"

    .line 94
    .line 95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    const-string v2, " =? "

    .line 99
    .line 100
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->O8()I

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    if-nez v2, :cond_1

    .line 108
    .line 109
    const-string v2, " or enhance_mode IS NULL "

    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_1
    move-object v2, v3

    .line 113
    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    const-string v2, ")"

    .line 117
    .line 118
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    const/16 v2, 0x8

    .line 126
    .line 127
    new-array v2, v2, [Ljava/lang/String;

    .line 128
    .line 129
    const/4 v4, 0x0

    .line 130
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o〇()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v5

    .line 134
    aput-object v5, v2, v4

    .line 135
    .line 136
    const/4 v4, 0x1

    .line 137
    aput-object p5, v2, v4

    .line 138
    .line 139
    const/4 p5, 0x2

    .line 140
    aput-object p4, v2, p5

    .line 141
    .line 142
    new-instance p4, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->o〇0()I

    .line 148
    .line 149
    .line 150
    move-result p5

    .line 151
    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object p4

    .line 161
    const/4 p5, 0x3

    .line 162
    aput-object p4, v2, p5

    .line 163
    .line 164
    new-instance p4, Ljava/lang/StringBuilder;

    .line 165
    .line 166
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .line 168
    .line 169
    iget p2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    .line 170
    .line 171
    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object p2

    .line 181
    const/4 p4, 0x4

    .line 182
    aput-object p2, v2, p4

    .line 183
    .line 184
    new-instance p2, Ljava/lang/StringBuilder;

    .line 185
    .line 186
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    .line 188
    .line 189
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->Oo08()I

    .line 190
    .line 191
    .line 192
    move-result p4

    .line 193
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p2

    .line 203
    const/4 p4, 0x5

    .line 204
    aput-object p2, v2, p4

    .line 205
    .line 206
    new-instance p2, Ljava/lang/StringBuilder;

    .line 207
    .line 208
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .line 210
    .line 211
    invoke-virtual {p2, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object p2

    .line 221
    const/4 p4, 0x6

    .line 222
    aput-object p2, v2, p4

    .line 223
    .line 224
    new-instance p2, Ljava/lang/StringBuilder;

    .line 225
    .line 226
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 227
    .line 228
    .line 229
    invoke-virtual {p3}, Lcom/intsig/camscanner/service/BackScanImageData;->O8()I

    .line 230
    .line 231
    .line 232
    move-result p3

    .line 233
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 234
    .line 235
    .line 236
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    .line 238
    .line 239
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 240
    .line 241
    .line 242
    move-result-object p2

    .line 243
    const/4 p3, 0x7

    .line 244
    aput-object p2, v2, p3

    .line 245
    .line 246
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 247
    .line 248
    .line 249
    move-result-object p2

    .line 250
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 251
    .line 252
    .line 253
    move-result-object p2

    .line 254
    invoke-virtual {p2, v0, p1, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 255
    .line 256
    .line 257
    move-result p1

    .line 258
    return p1
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
.end method

.method private o〇O8〇〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇80〇808〇O:[B

    .line 11
    .line 12
    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 14
    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    new-instance v1, Landroid/os/HandlerThread;

    .line 18
    .line 19
    const-string v2, "BackScanClient"

    .line 20
    .line 21
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇808〇()V

    .line 32
    .line 33
    .line 34
    :cond_1
    monitor-exit v0

    .line 35
    return-void

    .line 36
    :catchall_0
    move-exception v1

    .line 37
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    throw v1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static synthetic o〇〇0〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)I
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 2
    .line 3
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-lez v4, :cond_0

    .line 8
    .line 9
    const/4 p0, -0x1

    .line 10
    return p0

    .line 11
    :cond_0
    cmp-long v4, v0, v2

    .line 12
    .line 13
    if-gez v4, :cond_1

    .line 14
    .line 15
    const/4 p0, 0x1

    .line 16
    return p0

    .line 17
    :cond_1
    iget-wide v0, p0, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo:J

    .line 18
    .line 19
    iget-wide p0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo:J

    .line 20
    .line 21
    invoke-static {v0, v1, p0, p1}, Ljava/lang/Long;->compare(JJ)I

    .line 22
    .line 23
    .line 24
    move-result p0

    .line 25
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇00()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇oo〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇O8〇〇o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0000OOO()V
    .locals 11

    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    const/4 v1, 0x0

    .line 3
    :cond_0
    :goto_1
    iget-boolean v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 4
    .line 5
    const-string v3, "BackScanClient"

    .line 6
    .line 7
    if-nez v2, :cond_9

    .line 8
    .line 9
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 10
    .line 11
    invoke-virtual {v2}, Ljava/util/concurrent/PriorityBlockingQueue;->take()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    check-cast v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    goto :goto_3

    .line 18
    :catch_0
    move-exception v2

    .line 19
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    goto :goto_2

    .line 27
    :catch_1
    move-exception v2

    .line 28
    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 40
    .line 41
    .line 42
    :goto_2
    const/4 v2, 0x0

    .line 43
    :goto_3
    iget-boolean v4, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 44
    .line 45
    if-eqz v4, :cond_1

    .line 46
    .line 47
    goto/16 :goto_7

    .line 48
    .line 49
    :cond_1
    if-eqz v2, :cond_8

    .line 50
    .line 51
    invoke-virtual {v2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    if-nez v4, :cond_2

    .line 56
    .line 57
    goto/16 :goto_6

    .line 58
    .line 59
    :cond_2
    iput-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 60
    .line 61
    if-nez v1, :cond_3

    .line 62
    .line 63
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    :cond_3
    if-nez v1, :cond_4

    .line 68
    .line 69
    iget-object v4, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 70
    .line 71
    iput-object v4, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 72
    .line 73
    const-string v4, "engineInitContext == 0 error"

    .line 74
    .line 75
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo8Oo00oo(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 79
    .line 80
    .line 81
    const-wide/16 v2, 0x1388

    .line 82
    .line 83
    invoke-direct {p0, v2, v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇8(J)V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    iget-wide v5, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 92
    .line 93
    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    if-eqz v4, :cond_7

    .line 98
    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 100
    .line 101
    .line 102
    move-result-wide v4

    .line 103
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 104
    .line 105
    .line 106
    move-result-object v6

    .line 107
    const/4 v7, 0x1

    .line 108
    invoke-virtual {v6, v7}, Lcom/intsig/ocrapi/LocalOcrClient;->〇oOO8O8(Z)V

    .line 109
    .line 110
    .line 111
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o0ooO(ILcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 112
    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 115
    .line 116
    .line 117
    move-result-object v6

    .line 118
    invoke-virtual {v6, v0}, Lcom/intsig/ocrapi/LocalOcrClient;->〇oOO8O8(Z)V

    .line 119
    .line 120
    .line 121
    new-instance v6, Ljava/lang/StringBuilder;

    .line 122
    .line 123
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v7, "looperHandleImage costTime:"

    .line 127
    .line 128
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 132
    .line 133
    .line 134
    move-result-wide v7

    .line 135
    sub-long/2addr v7, v4

    .line 136
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v4

    .line 143
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {v2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 147
    .line 148
    .line 149
    move-result v4

    .line 150
    if-lez v4, :cond_5

    .line 151
    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 153
    .line 154
    .line 155
    move-result-wide v4

    .line 156
    iput-wide v4, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo:J

    .line 157
    .line 158
    const-wide/16 v4, 0x0

    .line 159
    .line 160
    iput-wide v4, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 161
    .line 162
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo8Oo00oo(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V

    .line 163
    .line 164
    .line 165
    new-instance v4, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    const-string v5, "looperHandleImage backScanDocModel.getPageNumber() == "

    .line 171
    .line 172
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v2}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->Oo08()I

    .line 176
    .line 177
    .line 178
    move-result v5

    .line 179
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v4

    .line 186
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    goto :goto_4

    .line 190
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    .line 191
    .line 192
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 193
    .line 194
    .line 195
    const-string v5, "looperHandleImage backScanDocModel.getPageNumber() == 0 docId="

    .line 196
    .line 197
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    iget-wide v5, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 201
    .line 202
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v4

    .line 209
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 213
    .line 214
    .line 215
    move-result-object v5

    .line 216
    iget-wide v6, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 217
    .line 218
    const/4 v8, 0x3

    .line 219
    const/4 v9, 0x1

    .line 220
    const/4 v10, 0x0

    .line 221
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 222
    .line 223
    .line 224
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 225
    .line 226
    .line 227
    move-result-object v4

    .line 228
    iget-wide v5, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 229
    .line 230
    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 231
    .line 232
    .line 233
    iget-object v4, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O〇:Lcom/intsig/callback/Callback0;

    .line 234
    .line 235
    if-eqz v4, :cond_6

    .line 236
    .line 237
    invoke-interface {v4}, Lcom/intsig/callback/Callback0;->call()V

    .line 238
    .line 239
    .line 240
    :cond_6
    :goto_4
    sget-object v4, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    .line 241
    .line 242
    invoke-virtual {v4}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    .line 243
    .line 244
    .line 245
    move-result-object v4

    .line 246
    iget-wide v5, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 247
    .line 248
    const-wide/16 v7, -0x1

    .line 249
    .line 250
    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇00(JJ)V

    .line 251
    .line 252
    .line 253
    goto :goto_5

    .line 254
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    .line 255
    .line 256
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    .line 258
    .line 259
    const-string v5, "DocumentDao.checkDocById false, backScanDocModel.docId:"

    .line 260
    .line 261
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    iget-wide v5, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 265
    .line 266
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 267
    .line 268
    .line 269
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v2

    .line 273
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    :goto_5
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 277
    .line 278
    iput-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 279
    .line 280
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 281
    .line 282
    invoke-virtual {v2}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 283
    .line 284
    .line 285
    move-result v2

    .line 286
    if-nez v2, :cond_0

    .line 287
    .line 288
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8(I)V

    .line 289
    .line 290
    .line 291
    const-string v1, "priorityQueue.size() == 0"

    .line 292
    .line 293
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    .line 295
    .line 296
    goto/16 :goto_0

    .line 297
    .line 298
    :cond_8
    :goto_6
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 299
    .line 300
    iput-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 301
    .line 302
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8(I)V

    .line 303
    .line 304
    .line 305
    const-string v1, "backScanDocModel == null || backScanDocModel.getPageNumber() == 0"

    .line 306
    .line 307
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    .line 309
    .line 310
    goto/16 :goto_0

    .line 311
    .line 312
    :cond_9
    :goto_7
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 313
    .line 314
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 315
    .line 316
    invoke-static {v1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 317
    .line 318
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    .line 320
    .line 321
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 322
    .line 323
    .line 324
    const-string v1, "stopHandleImage HandleImageThread stopHandleImage="

    .line 325
    .line 326
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    .line 328
    .line 329
    iget-boolean v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 330
    .line 331
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 332
    .line 333
    .line 334
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v0

    .line 338
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .line 340
    .line 341
    return-void
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
.end method

.method private synthetic 〇00〇8(Lcom/intsig/okbinder/ServerInfo;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    const/4 v2, 0x0

    .line 4
    :try_start_0
    iget-object v3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 5
    .line 6
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 7
    .line 8
    .line 9
    iget-boolean v3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 10
    .line 11
    if-nez v3, :cond_1

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    if-nez v3, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v4, "BACK_PUSH_MULTI_PROCESS_"

    .line 26
    .line 27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    const-string v4, "BackScanClient"

    .line 42
    .line 43
    new-instance v5, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v6, "startImageProgress before executeProgress UUID="

    .line 49
    .line 50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const-string v4, "batch_process_issue_count"

    .line 64
    .line 65
    new-instance v5, Loo/〇0〇O0088o;

    .line 66
    .line 67
    invoke-direct {v5, p1, v3, p3, p2}, Loo/〇0〇O0088o;-><init>(Lcom/intsig/okbinder/ServerInfo;Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 68
    .line 69
    .line 70
    invoke-static {v4, v5}, Lcom/intsig/camscanner/booksplitter/Util/NoResponseRecorder;->oO80(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    check-cast p1, Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 75
    .line 76
    const-string p3, "BackScanClient"

    .line 77
    .line 78
    const-string v3, "startImageProgress after executeProgress"

    .line 79
    .line 80
    invoke-static {p3, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-interface {p4, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$HandleImageCallback;->〇080(Lcom/intsig/camscanner/util/ImageProgressClient;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 87
    .line 88
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 89
    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 92
    .line 93
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 94
    .line 95
    .line 96
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 97
    .line 98
    monitor-enter p1

    .line 99
    :try_start_1
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 100
    .line 101
    iget-wide v1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 102
    .line 103
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    invoke-virtual {p3, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    monitor-exit p1

    .line 111
    return-object v0

    .line 112
    :catchall_0
    move-exception p2

    .line 113
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    throw p2

    .line 115
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 116
    .line 117
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 121
    .line 122
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 123
    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 126
    .line 127
    monitor-enter p1

    .line 128
    :try_start_2
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 129
    .line 130
    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 131
    .line 132
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 133
    .line 134
    .line 135
    move-result-object p4

    .line 136
    invoke-virtual {p3, p4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 137
    .line 138
    .line 139
    monitor-exit p1

    .line 140
    return-object p2

    .line 141
    :catchall_1
    move-exception p2

    .line 142
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 143
    throw p2

    .line 144
    :catchall_2
    move-exception p1

    .line 145
    :try_start_3
    const-string p3, "BackScanClient"

    .line 146
    .line 147
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 148
    .line 149
    .line 150
    sget-object p1, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 151
    .line 152
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 153
    .line 154
    .line 155
    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇O〇()Z

    .line 157
    .line 158
    .line 159
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888()V

    .line 162
    .line 163
    .line 164
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->android_multi_process_no_retry:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_5

    .line 169
    .line 170
    if-ne p1, v1, :cond_2

    .line 171
    .line 172
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 173
    .line 174
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 175
    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 178
    .line 179
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 180
    .line 181
    .line 182
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 183
    .line 184
    monitor-enter p1

    .line 185
    :try_start_4
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 186
    .line 187
    iget-wide v1, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 188
    .line 189
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 190
    .line 191
    .line 192
    move-result-object p2

    .line 193
    invoke-virtual {p3, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    monitor-exit p1

    .line 197
    return-object v0

    .line 198
    :catchall_3
    move-exception p2

    .line 199
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 200
    throw p2

    .line 201
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 202
    .line 203
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 204
    .line 205
    .line 206
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 207
    .line 208
    invoke-virtual {p1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 209
    .line 210
    .line 211
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 212
    .line 213
    monitor-enter p1

    .line 214
    :try_start_5
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 215
    .line 216
    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 217
    .line 218
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 219
    .line 220
    .line 221
    move-result-object p4

    .line 222
    invoke-virtual {p3, p4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 223
    .line 224
    .line 225
    monitor-exit p1

    .line 226
    return-object p2

    .line 227
    :catchall_4
    move-exception p2

    .line 228
    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 229
    throw p2

    .line 230
    :catchall_5
    move-exception p1

    .line 231
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 232
    .line 233
    invoke-virtual {p3, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oooo8o0〇(Z)V

    .line 234
    .line 235
    .line 236
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇0〇O0088o:Ljava/util/concurrent/Semaphore;

    .line 237
    .line 238
    invoke-virtual {p3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 239
    .line 240
    .line 241
    iget-object p3, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o800o8O:[B

    .line 242
    .line 243
    monitor-enter p3

    .line 244
    :try_start_6
    iget-object p4, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8:Ljava/util/HashSet;

    .line 245
    .line 246
    iget-wide v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 247
    .line 248
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 249
    .line 250
    .line 251
    move-result-object p2

    .line 252
    invoke-virtual {p4, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 253
    .line 254
    .line 255
    monitor-exit p3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 256
    throw p1

    .line 257
    :catchall_6
    move-exception p1

    .line 258
    :try_start_7
    monitor-exit p3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 259
    throw p1
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8ooOoo〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇0〇O0088o()Ljava/util/concurrent/ExecutorService;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x3

    .line 6
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 11
    .line 12
    const-wide/16 v1, 0x3c

    .line 13
    .line 14
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/ThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 17
    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 24
    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8(J)V
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception p1

    .line 6
    const-string p2, "BackScanClient"

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p1}, Ljava/lang/Thread;->interrupt()V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇〇0〇(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->oO80:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇8〇0〇o〇O(JI)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇8O0〇8:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    if-gtz v1, :cond_1

    .line 21
    .line 22
    :cond_0
    const/4 v1, 0x0

    .line 23
    :cond_1
    const/4 v0, 0x3

    .line 24
    new-array v0, v0, [Landroid/util/Pair;

    .line 25
    .line 26
    new-instance v3, Landroid/util/Pair;

    .line 27
    .line 28
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 29
    .line 30
    const/4 v5, 0x1

    .line 31
    new-array v6, v5, [Ljava/lang/Object;

    .line 32
    .line 33
    const/high16 v7, 0x3f800000    # 1.0f

    .line 34
    .line 35
    long-to-float p1, p1

    .line 36
    mul-float p1, p1, v7

    .line 37
    .line 38
    int-to-float p2, p3

    .line 39
    div-float/2addr p1, p2

    .line 40
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    aput-object p1, v6, v2

    .line 45
    .line 46
    const-string p1, "%.2f"

    .line 47
    .line 48
    invoke-static {v4, p1, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    const-string p2, "cost_time"

    .line 53
    .line 54
    invoke-direct {v3, p2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 55
    .line 56
    .line 57
    aput-object v3, v0, v2

    .line 58
    .line 59
    new-instance p1, Landroid/util/Pair;

    .line 60
    .line 61
    new-instance p2, Ljava/lang/StringBuilder;

    .line 62
    .line 63
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v1, ""

    .line 70
    .line 71
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    const-string v2, "multi_process_type"

    .line 79
    .line 80
    invoke-direct {p1, v2, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 81
    .line 82
    .line 83
    aput-object p1, v0, v5

    .line 84
    .line 85
    new-instance p1, Landroid/util/Pair;

    .line 86
    .line 87
    new-instance p2, Ljava/lang/StringBuilder;

    .line 88
    .line 89
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object p2

    .line 102
    const-string p3, "total_num"

    .line 103
    .line 104
    invoke-direct {p1, p3, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    const/4 p2, 0x2

    .line 108
    aput-object p1, v0, p2

    .line 109
    .line 110
    const-string p1, "CSDevelopmentTool"

    .line 111
    .line 112
    const-string p2, "batch_handle_image_time"

    .line 113
    .line 114
    invoke-static {p1, p2, v0}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 115
    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private 〇O00(J)Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 3
    .line 4
    invoke-virtual {v1}, Ljava/util/concurrent/PriorityBlockingQueue;->iterator()Ljava/util/Iterator;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_2

    .line 13
    .line 14
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 19
    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget-wide v3, v2, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    cmp-long v5, v3, p1

    .line 26
    .line 27
    if-nez v5, :cond_0

    .line 28
    .line 29
    move-object v0, v2

    .line 30
    goto :goto_1

    .line 31
    :catch_0
    move-exception p1

    .line 32
    const-string p2, "BackScanClient"

    .line 33
    .line 34
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 35
    .line 36
    .line 37
    :cond_2
    :goto_1
    return-object v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇O888o0o()Ljava/util/concurrent/ExecutorService;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo08:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    check-cast v1, Ljava/util/concurrent/ThreadPoolExecutor;

    .line 11
    .line 12
    const-wide/16 v2, 0x3c

    .line 13
    .line 14
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 15
    .line 16
    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/ThreadPoolExecutor;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1, v0}, Ljava/util/concurrent/ThreadPoolExecutor;->allowCoreThreadTimeOut(Z)V

    .line 20
    .line 21
    .line 22
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo08:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oo08:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O8o08O(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;)V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo:J

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static synthetic 〇o(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Ljava/lang/Void;
    .locals 2

    .line 1
    iget-object v0, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇8O0〇8:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 2
    .line 3
    iget-object p2, p2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇O00:Lcom/intsig/camscanner/image_progress/ImageProcessCallback;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;)V

    .line 7
    .line 8
    .line 9
    return-object v1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇8O8〇008(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇oOO8O8(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Lcom/intsig/camscanner/service/BackScanImageData;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Ljava/util/List;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/util/ImageProgressClient;)V
    .locals 4

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->getBorder()[I

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->getBorder()[I

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iput-object v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 16
    .line 17
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 18
    .line 19
    iget-object v1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 20
    .line 21
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    .line 25
    .line 26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v2, "before isDecodeOk:"

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v2, ",isEncodeOk:"

    .line 42
    .line 43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk()Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const-string v2, "BackScanClient"

    .line 58
    .line 59
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk()Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    const/4 v3, 0x0

    .line 67
    if-eqz v1, :cond_6

    .line 68
    .line 69
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk()Z

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    if-eqz v1, :cond_6

    .line 74
    .line 75
    iget-object v1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 76
    .line 77
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_6

    .line 82
    .line 83
    const-string p7, "before saveInfoToDB"

    .line 84
    .line 85
    invoke-static {v2, p7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-static {p2, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result p2

    .line 92
    const/4 p7, 0x1

    .line 93
    if-eqz p2, :cond_5

    .line 94
    .line 95
    const-string p2, "saveInfoToDB"

    .line 96
    .line 97
    invoke-static {v2, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇0o(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;)Z

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    if-eqz p2, :cond_4

    .line 105
    .line 106
    iget-object p2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 109
    .line 110
    .line 111
    move-result-object p2

    .line 112
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 113
    .line 114
    .line 115
    move-result p3

    .line 116
    if-eqz p3, :cond_1

    .line 117
    .line 118
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object p3

    .line 122
    check-cast p3, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 123
    .line 124
    invoke-interface {p3, p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 125
    .line 126
    .line 127
    goto :goto_0

    .line 128
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->Oooo8o0〇:Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 129
    .line 130
    if-eqz p2, :cond_2

    .line 131
    .line 132
    iget-wide v0, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 133
    .line 134
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->oO80(J)V

    .line 135
    .line 136
    .line 137
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o()Ljava/util/concurrent/ExecutorService;

    .line 138
    .line 139
    .line 140
    move-result-object p2

    .line 141
    new-instance p3, Loo/〇〇8O0〇8;

    .line 142
    .line 143
    invoke-direct {p3, p0, p4, p1}, Loo/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 144
    .line 145
    .line 146
    invoke-interface {p2, p3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    if-nez p2, :cond_3

    .line 151
    .line 152
    const-string p2, "future == null"

    .line 153
    .line 154
    invoke-static {v2, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    goto :goto_1

    .line 158
    :cond_3
    invoke-interface {p5, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    goto :goto_1

    .line 162
    :cond_4
    const/4 v3, 0x1

    .line 163
    :goto_1
    move p7, v3

    .line 164
    goto :goto_2

    .line 165
    :cond_5
    const-string p2, "raw image change"

    .line 166
    .line 167
    invoke-static {v2, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    :goto_2
    if-eqz p7, :cond_7

    .line 171
    .line 172
    const-wide/16 p2, 0x0

    .line 173
    .line 174
    iput-wide p2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->oO80:J

    .line 175
    .line 176
    invoke-virtual {p6, p1}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 177
    .line 178
    .line 179
    goto :goto_3

    .line 180
    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string p3, "imageProgressClient isDecodeOk()="

    .line 186
    .line 187
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isDecodeOk()Z

    .line 191
    .line 192
    .line 193
    move-result p3

    .line 194
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    const-string p3, " isEncodeOk="

    .line 198
    .line 199
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {p7}, Lcom/intsig/camscanner/util/ImageProgressClient;->isEncodeOk()Z

    .line 203
    .line 204
    .line 205
    move-result p3

    .line 206
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    const-string p3, ", imagePath="

    .line 210
    .line 211
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    iget-object p3, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 215
    .line 216
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    const-string p3, " isExists="

    .line 220
    .line 221
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    iget-object p1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 225
    .line 226
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 227
    .line 228
    .line 229
    move-result p1

    .line 230
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    const-string p1, "\uff0c Please provide the source image to the developer, recordError="

    .line 234
    .line 235
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 239
    .line 240
    .line 241
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object p1

    .line 245
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    .line 247
    .line 248
    :cond_7
    :goto_3
    return-void
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method

.method private 〇oo〇()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CsAvoidUseApiDetector"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇808〇:[B

    .line 7
    .line 8
    monitor-enter v0

    .line 9
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 10
    .line 11
    if-nez v1, :cond_1

    .line 12
    .line 13
    new-instance v1, Ljava/lang/Thread;

    .line 14
    .line 15
    new-instance v2, Loo/〇oo〇;

    .line 16
    .line 17
    invoke-direct {v2, p0}, Loo/〇oo〇;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 21
    .line 22
    .line 23
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 24
    .line 25
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 26
    .line 27
    .line 28
    :cond_1
    monitor-exit v0

    .line 29
    return-void

    .line 30
    :catchall_0
    move-exception v1

    .line 31
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    throw v1
    .line 33
.end method

.method public static synthetic 〇o〇(Lcom/intsig/okbinder/ServerInfo;Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8〇o(Lcom/intsig/okbinder/ServerInfo;Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private 〇〇0o(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;)Z
    .locals 14

    .line 1
    move-object v8, p1

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 3
    .line 4
    .line 5
    move-result-wide v9

    .line 6
    iget-object v0, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v11, 0x1

    .line 9
    invoke-static {v0, v11}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇o(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    new-instance v2, Landroid/content/ContentValues;

    .line 20
    .line 21
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 22
    .line 23
    .line 24
    iget v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->Oo08:I

    .line 25
    .line 26
    invoke-static {v3}, Lcom/intsig/camscanner/app/DBUtil;->oo〇(I)I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    const-string v4, "enhance_mode"

    .line 35
    .line 36
    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 37
    .line 38
    .line 39
    iget-object v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 40
    .line 41
    invoke-static {v3}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    const-string v12, "BackScanClient"

    .line 46
    .line 47
    const/4 v13, 0x0

    .line 48
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    if-eqz v0, :cond_1

    .line 53
    .line 54
    if-eqz v3, :cond_1

    .line 55
    .line 56
    iget-object v5, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 57
    .line 58
    if-nez v5, :cond_0

    .line 59
    .line 60
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->〇08O8o〇0([I)[I

    .line 61
    .line 62
    .line 63
    move-result-object v5

    .line 64
    invoke-static {v0, v3, v5, v13}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->〇08O8o〇0([I)[I

    .line 69
    .line 70
    .line 71
    move-result-object v5

    .line 72
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/service/BackScanImageData;->o〇0()I

    .line 73
    .line 74
    .line 75
    move-result v6

    .line 76
    invoke-static {v0, v0, v5, v6}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    goto :goto_0

    .line 81
    :cond_0
    invoke-static {v0, v3, v5, v13}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    iget-object v5, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 86
    .line 87
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/service/BackScanImageData;->o〇0()I

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    invoke-static {v0, v0, v5, v6}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    .line 96
    .line 97
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .line 99
    .line 100
    const-string v6, "borderStr = "

    .line 101
    .line 102
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    const-string v6, " rawBorderStr="

    .line 109
    .line 110
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v5

    .line 120
    invoke-static {v12, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    const-string v5, "image_border"

    .line 124
    .line 125
    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    move-object v6, v0

    .line 129
    move-object v5, v3

    .line 130
    goto :goto_1

    .line 131
    :cond_1
    const/4 v0, 0x0

    .line 132
    move-object v5, v0

    .line 133
    move-object v6, v5

    .line 134
    :goto_1
    const-string v0, "_data"

    .line 135
    .line 136
    iget-object v3, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 137
    .line 138
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/service/BackScanImageData;->〇〇888()I

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    const-string v3, "detail_index"

    .line 150
    .line 151
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    .line 153
    .line 154
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/service/BackScanImageData;->〇o00〇〇Oo()I

    .line 155
    .line 156
    .line 157
    move-result v0

    .line 158
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    const-string v3, "contrast_index"

    .line 163
    .line 164
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 165
    .line 166
    .line 167
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/service/BackScanImageData;->〇080()I

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    const-string v3, "bright_index"

    .line 176
    .line 177
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 178
    .line 179
    .line 180
    const-string v0, "thumb_data"

    .line 181
    .line 182
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    const-string v0, "image_rotation"

    .line 186
    .line 187
    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 188
    .line 189
    .line 190
    iget v0, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇8o8o〇:I

    .line 191
    .line 192
    add-int/lit16 v0, v0, 0x168

    .line 193
    .line 194
    iget-object v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o00〇〇Oo:Ljava/lang/String;

    .line 195
    .line 196
    invoke-static {v1}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 197
    .line 198
    .line 199
    move-result v1

    .line 200
    sub-int/2addr v0, v1

    .line 201
    rem-int/lit16 v7, v0, 0x168

    .line 202
    .line 203
    const-string v0, "ori_rotation"

    .line 204
    .line 205
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210
    .line 211
    .line 212
    const-string v0, "status"

    .line 213
    .line 214
    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 215
    .line 216
    .line 217
    move-object v1, p0

    .line 218
    move-object v3, p1

    .line 219
    move-object/from16 v4, p2

    .line 220
    .line 221
    :try_start_0
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇O(Landroid/content/ContentValues;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;Lcom/intsig/camscanner/service/BackScanImageData;Ljava/lang/String;Ljava/lang/String;I)I

    .line 222
    .line 223
    .line 224
    move-result v0

    .line 225
    iget v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇80〇808〇O:I

    .line 226
    .line 227
    const/4 v2, 0x5

    .line 228
    if-ge v1, v2, :cond_2

    .line 229
    .line 230
    if-nez v0, :cond_2

    .line 231
    .line 232
    add-int/2addr v1, v11

    .line 233
    iput v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇80〇808〇O:I

    .line 234
    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    .line 236
    .line 237
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    .line 239
    .line 240
    const-string v2, "backScanPageModel.tryTime="

    .line 241
    .line 242
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    iget v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇80〇808〇O:I

    .line 246
    .line 247
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v1

    .line 254
    invoke-static {v12, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    const/4 v11, 0x0

    .line 258
    goto :goto_2

    .line 259
    :cond_2
    if-lez v0, :cond_3

    .line 260
    .line 261
    sget-object v1, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    .line 262
    .line 263
    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    .line 264
    .line 265
    .line 266
    move-result-object v1

    .line 267
    iget-wide v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 268
    .line 269
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->O8ooOoo〇(J)V

    .line 270
    .line 271
    .line 272
    :cond_3
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 273
    .line 274
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    .line 276
    .line 277
    const-string v2, "saveInfoToDB IAMGE ID:"

    .line 278
    .line 279
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    iget-wide v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 283
    .line 284
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 285
    .line 286
    .line 287
    const-string v2, " path:"

    .line 288
    .line 289
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    .line 291
    .line 292
    iget-object v2, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇o〇:Ljava/lang/String;

    .line 293
    .line 294
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    const-string v2, " num = "

    .line 298
    .line 299
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    .line 301
    .line 302
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 303
    .line 304
    .line 305
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 306
    .line 307
    .line 308
    move-result-object v0

    .line 309
    invoke-static {v12, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    .line 311
    .line 312
    move v13, v11

    .line 313
    goto :goto_3

    .line 314
    :catch_0
    move-exception v0

    .line 315
    const-string v1, "RuntimeException in backgroud service Image.THUMB_DATA"

    .line 316
    .line 317
    invoke-static {v12, v1, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 318
    .line 319
    .line 320
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 321
    .line 322
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 323
    .line 324
    .line 325
    const-string v1, "saveInfoToDB costTime="

    .line 326
    .line 327
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    .line 329
    .line 330
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 331
    .line 332
    .line 333
    move-result-wide v1

    .line 334
    sub-long/2addr v1, v9

    .line 335
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 336
    .line 337
    .line 338
    const-string v1, " saveInfoToDB="

    .line 339
    .line 340
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    .line 342
    .line 343
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 344
    .line 345
    .line 346
    const-string v1, " pageId:"

    .line 347
    .line 348
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    iget-wide v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇080:J

    .line 352
    .line 353
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 354
    .line 355
    .line 356
    const-string v1, " index:"

    .line 357
    .line 358
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    .line 360
    .line 361
    iget v1, v8, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇888:I

    .line 362
    .line 363
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 364
    .line 365
    .line 366
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 367
    .line 368
    .line 369
    move-result-object v0

    .line 370
    invoke-static {v12, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    return v13
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private 〇〇808〇()V
    .locals 3

    .line 1
    new-instance v0, Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Loo/o〇O8〇〇o;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Loo/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/background_batch/client/BackScanClient;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Ljava/lang/Void;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o(Lcom/intsig/camscanner/util/ImageProgressClient;Ljava/lang/String;Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)Ljava/lang/Void;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇〇8O0〇8(I)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->OOO8o〇〇(Landroid/content/Context;)I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 19
    .line 20
    .line 21
    const-string p1, "BackScanClient"

    .line 22
    .line 23
    const-string v0, "UnconfirmedImageNum = 0, requestSync"

    .line 24
    .line 25
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    :cond_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public OO0o〇〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Oooo8o0〇()V
    .locals 2

    .line 1
    const-string v0, "clearAllBackScanDoc"

    .line 2
    .line 3
    const-string v1, "BackScanClient"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇O8〇〇o()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string v0, "clearAllBackScanDoc workHandler == null"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    const/16 v1, 0x3ec

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "updateDocAccessTime docId="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "BackScanClient"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-wide/16 v2, 0x0

    .line 24
    .line 25
    cmp-long v0, p1, v2

    .line 26
    .line 27
    if-gtz v0, :cond_0

    .line 28
    .line 29
    return-object p0

    .line 30
    :cond_0
    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇00()V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 37
    .line 38
    if-nez v0, :cond_1

    .line 39
    .line 40
    const-string p1, "updateDocAccessTime workHandler == null"

    .line 41
    .line 42
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-object p0

    .line 46
    :cond_1
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const/16 v1, 0x3ea

    .line 51
    .line 52
    iput v1, v0, Landroid/os/Message;->what:I

    .line 53
    .line 54
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 61
    .line 62
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 63
    .line 64
    .line 65
    return-object p0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public o800o8O()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0OOo〇0()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o0O0()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/db/DBUpgradeUtil;->〇o〇(Landroid/content/Context;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o8()V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O8o08O:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 25
    .line 26
    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-lez v0, :cond_1

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇00()V

    .line 33
    .line 34
    .line 35
    :cond_1
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o〇8(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;J)V
    .locals 4

    .line 1
    const-string v0, "BackScanClient"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇888:Landroid/util/LruCache;

    .line 6
    .line 7
    iget-wide v2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 8
    .line 9
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    iget-wide v1, v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;->〇o〇:J

    .line 22
    .line 23
    invoke-static {v1, v2, p2, p3}, Ljava/lang/Math;->max(JJ)J

    .line 24
    .line 25
    .line 26
    move-result-wide p2

    .line 27
    iput-wide p2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    iput-wide p2, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o〇:J

    .line 31
    .line 32
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string p3, "pushBackScanDocToQueue docId="

    .line 38
    .line 39
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    iget-wide v1, p1, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇080:J

    .line 43
    .line 44
    invoke-virtual {p2, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    const/4 p2, 0x0

    .line 55
    iput-boolean p2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇00()V

    .line 58
    .line 59
    .line 60
    iget-object p2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 61
    .line 62
    if-nez p2, :cond_2

    .line 63
    .line 64
    const-string p1, "pushBackScanDocToQueue workHandler == null"

    .line 65
    .line 66
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void

    .line 70
    :cond_2
    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    const/16 p3, 0x3e9

    .line 75
    .line 76
    iput p3, p2, Landroid/os/Message;->what:I

    .line 77
    .line 78
    iput-object p1, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 81
    .line 82
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public o〇8oOO88(JJ)Lcom/intsig/camscanner/background_batch/client/BackScanClient;
    .locals 9

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "updatePageAccessTime docId="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, " pageId="

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "BackScanClient"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const-wide/16 v2, 0x0

    .line 32
    .line 33
    cmp-long v0, p1, v2

    .line 34
    .line 35
    if-lez v0, :cond_2

    .line 36
    .line 37
    cmp-long v0, p3, v2

    .line 38
    .line 39
    if-gtz v0, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v0, 0x0

    .line 43
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇00()V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 49
    .line 50
    if-nez v0, :cond_1

    .line 51
    .line 52
    const-string p1, "updatePageAccessTime workHandler == null"

    .line 53
    .line 54
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-object p0

    .line 58
    :cond_1
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    const/16 v1, 0x3eb

    .line 63
    .line 64
    iput v1, v0, Landroid/os/Message;->what:I

    .line 65
    .line 66
    new-instance v1, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;

    .line 67
    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 69
    .line 70
    .line 71
    move-result-wide v7

    .line 72
    move-object v2, v1

    .line 73
    move-wide v3, p1

    .line 74
    move-wide v5, p3

    .line 75
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/background_batch/client/BackScanClient$AccessTimeCacheModel;-><init>(JJJ)V

    .line 76
    .line 77
    .line 78
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 81
    .line 82
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 83
    .line 84
    .line 85
    :cond_2
    :goto_0
    return-object p0
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method public 〇08O8o〇0(Lcom/intsig/callback/Callback0;)V
    .locals 0
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O〇:Lcom/intsig/callback/Callback0;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇O〇()V
    .locals 3

    .line 1
    const-string v0, "BackScanClient"

    .line 2
    .line 3
    const-string v1, "exitBackScan"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O8:Z

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o〇:Z

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇808〇:[B

    .line 20
    .line 21
    monitor-enter v0

    .line 22
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇〇〇0:Ljava/lang/Thread;

    .line 30
    .line 31
    :cond_0
    monitor-exit v0

    .line 32
    goto :goto_0

    .line 33
    :catchall_0
    move-exception v1

    .line 34
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    throw v1

    .line 36
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 37
    .line 38
    if-eqz v0, :cond_4

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇80〇808〇O:[B

    .line 41
    .line 42
    monitor-enter v0

    .line 43
    :try_start_1
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 44
    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 48
    .line 49
    invoke-virtual {v2}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 50
    .line 51
    .line 52
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇080:Landroid/os/HandlerThread;

    .line 53
    .line 54
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 55
    .line 56
    if-eqz v2, :cond_3

    .line 57
    .line 58
    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 59
    .line 60
    .line 61
    :cond_3
    monitor-exit v0

    .line 62
    goto :goto_1

    .line 63
    :catchall_1
    move-exception v1

    .line 64
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 65
    throw v1

    .line 66
    :cond_4
    :goto_1
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇〇〇0〇〇0(Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O888o0o:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

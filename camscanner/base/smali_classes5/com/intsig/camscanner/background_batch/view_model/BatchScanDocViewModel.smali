.class public Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "BatchScanDocViewModel.java"


# instance fields
.field private OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final o0:Lcom/intsig/camscanner/background_batch/client/BackScanClient;

.field private final o〇00O:Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

.field private 〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel$1;-><init>(Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->o〇00O:Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iput-object v1, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->o0:Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OO0o〇〇(Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onCleared()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->o0:Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->o〇00O:Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇〇〇0〇〇0(Lcom/intsig/camscanner/background_batch/client/BackScanClient$BatchScanDocListener;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "BatchScanDocViewModel"

    .line 12
    .line 13
    const-string v1, "onCleared"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇08O〇00〇o:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8o8o〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

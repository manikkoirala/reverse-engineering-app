.class public Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;
.super Ljava/lang/Object;
.source "BatchImageReeditPresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$Presenter;


# static fields
.field private static final O〇8O8〇008:[Ljava/lang/String;

.field private static 〇00:Ljava/lang/String; = "BatchImageReeditPresenter"


# instance fields
.field private final O8:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇〇〇0:I

.field private Oo08:Z

.field private OoO8:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private Oooo8o0〇:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private o800o8O:Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;

.field private oO80:Z

.field private oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

.field private o〇0:Z

.field private o〇O8〇〇o:Z

.field private final 〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

.field private 〇0〇O0088o:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private 〇80〇808〇O:I

.field private 〇8o8o〇:I

.field private 〇O00:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/adapter/AbsRecyclerViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O888o0o:Z

.field private 〇O8o08O:Landroid/widget/TextView;

.field private 〇O〇:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "[I>;"
        }
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

.field private 〇oo〇:Lcom/intsig/camscanner/batch/contract/ActionTips;

.field private 〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

.field private 〇〇808〇:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇〇888:Ljava/lang/String;

.field private 〇〇8O0〇8:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 1
    const-string v0, "_id"

    .line 2
    .line 3
    const-string v1, "sync_image_id"

    .line 4
    .line 5
    const-string v2, "image_border"

    .line 6
    .line 7
    const-string v3, "thumb_data"

    .line 8
    .line 9
    const-string v4, "raw_data"

    .line 10
    .line 11
    const-string v5, "_data"

    .line 12
    .line 13
    const-string v6, "image_backup"

    .line 14
    .line 15
    const-string v7, "ori_rotation"

    .line 16
    .line 17
    const-string v8, "image_rotation"

    .line 18
    .line 19
    const-string v9, "enhance_mode"

    .line 20
    .line 21
    const-string v10, "sync_state"

    .line 22
    .line 23
    filled-new-array/range {v0 .. v10}, [Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    sput-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O〇8O8〇008:[Ljava/lang/String;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 10
    .line 11
    new-instance v0, Ljava/util/HashSet;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O8:Ljava/util/HashSet;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oo08:Z

    .line 20
    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇0:Z

    .line 22
    .line 23
    const/4 v1, 0x6

    .line 24
    iput v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80〇808〇O:I

    .line 25
    .line 26
    const/16 v1, 0xd2

    .line 27
    .line 28
    iput v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇〇〇0:I

    .line 29
    .line 30
    const/16 v1, 0x129

    .line 31
    .line 32
    iput v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8o8o〇:I

    .line 33
    .line 34
    new-instance v1, Landroid/util/LongSparseArray;

    .line 35
    .line 36
    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 40
    .line 41
    new-instance v1, Landroid/util/LongSparseArray;

    .line 42
    .line 43
    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    .line 44
    .line 45
    .line 46
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 47
    .line 48
    new-instance v1, Landroid/util/LongSparseArray;

    .line 49
    .line 50
    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    .line 51
    .line 52
    .line 53
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇808〇:Landroid/util/LongSparseArray;

    .line 54
    .line 55
    new-instance v1, Landroid/util/LongSparseArray;

    .line 56
    .line 57
    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    .line 58
    .line 59
    .line 60
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 61
    .line 62
    const/4 v1, 0x0

    .line 63
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 64
    .line 65
    new-instance v2, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$1;

    .line 66
    .line 67
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$1;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)V

    .line 68
    .line 69
    .line 70
    iput-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇8O0〇8:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 71
    .line 72
    new-instance v2, Ljava/util/HashMap;

    .line 73
    .line 74
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 75
    .line 76
    .line 77
    iput-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OoO8:Ljava/util/Map;

    .line 78
    .line 79
    new-instance v2, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$3;

    .line 80
    .line 81
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$3;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)V

    .line 82
    .line 83
    .line 84
    iput-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o800o8O:Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;

    .line 85
    .line 86
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O888o0o:Z

    .line 87
    .line 88
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇oo〇:Lcom/intsig/camscanner/batch/contract/ActionTips;

    .line 89
    .line 90
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇O8〇〇o:Z

    .line 91
    .line 92
    iput-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 93
    .line 94
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    iput-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇888:Ljava/lang/String;

    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private O000()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 9
    .line 10
    invoke-interface {v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->oO〇()Landroidx/fragment/app/Fragment;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    new-instance v2, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$4;

    .line 15
    .line 16
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$4;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O08000()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_3

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 23
    .line 24
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 25
    .line 26
    if-eqz v3, :cond_0

    .line 27
    .line 28
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 29
    .line 30
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    iget-object v3, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 35
    .line 36
    iget-object v4, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 37
    .line 38
    invoke-virtual {v4}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    if-eqz v4, :cond_2

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080()Z

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    if-nez v4, :cond_1

    .line 53
    .line 54
    invoke-virtual {v3}, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o00〇〇Oo()Z

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    if-eqz v4, :cond_2

    .line 59
    .line 60
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    iget v3, v3, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 65
    .line 66
    if-eqz v3, :cond_0

    .line 67
    .line 68
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    return-object v0
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private O0o〇〇Oo(Landroid/content/Context;Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;Ljava/util/ArrayList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p2

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 4
    .line 5
    iget-object v2, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->O8:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 6
    .line 7
    iget-object v3, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->o〇0:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 8
    .line 9
    iget-object v4, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇〇888:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 10
    .line 11
    sget-object v5, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 12
    .line 13
    new-instance v6, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v7, "onlyRotatePageInfo imageDBInfo.modifyRotation\uff1d"

    .line 19
    .line 20
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    iget v7, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 24
    .line 25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v6

    .line 32
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v5

    .line 39
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    const/16 v6, 0x64

    .line 44
    .line 45
    const/high16 v7, 0x3f800000    # 1.0f

    .line 46
    .line 47
    const/4 v8, 0x0

    .line 48
    const/4 v9, 0x0

    .line 49
    if-eqz v5, :cond_0

    .line 50
    .line 51
    invoke-virtual {v4}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v4

    .line 55
    iget v5, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 56
    .line 57
    invoke-static {v4, v5, v7, v6, v9}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 58
    .line 59
    .line 60
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    invoke-static {v4, v8}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    goto :goto_0

    .line 69
    :cond_0
    move-object v4, v9

    .line 70
    :goto_0
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    if-eqz v5, :cond_1

    .line 79
    .line 80
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    iget v5, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 85
    .line 86
    invoke-static {v2, v5, v7, v6, v9}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 87
    .line 88
    .line 89
    :cond_1
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    if-eqz v2, :cond_2

    .line 98
    .line 99
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    iget v5, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 104
    .line 105
    invoke-static {v2, v5, v7, v6, v9}, Lcom/intsig/scanner/ScannerEngine;->scaleImage(Ljava/lang/String;IFILjava/lang/String;)I

    .line 106
    .line 107
    .line 108
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    invoke-static {v2, v8}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 113
    .line 114
    .line 115
    move-result-object v9

    .line 116
    :cond_2
    new-instance v2, Landroid/content/ContentValues;

    .line 117
    .line 118
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v3, "ocr_border"

    .line 122
    .line 123
    const-string v5, ""

    .line 124
    .line 125
    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    const-string v3, "ocr_result"

    .line 129
    .line 130
    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    const-string v3, "ocr_result_user"

    .line 134
    .line 135
    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    const-string v3, "ocr_paragraph"

    .line 139
    .line 140
    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    iget v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇8o8o〇:I

    .line 144
    .line 145
    const-string v5, "image_rotation"

    .line 146
    .line 147
    const/4 v6, 0x1

    .line 148
    if-eq v3, v6, :cond_4

    .line 149
    .line 150
    const/4 v7, 0x3

    .line 151
    if-ne v3, v7, :cond_3

    .line 152
    .line 153
    goto :goto_1

    .line 154
    :cond_3
    iget v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o00〇〇Oo:I

    .line 155
    .line 156
    iget v7, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 157
    .line 158
    add-int/2addr v3, v7

    .line 159
    rem-int/lit16 v3, v3, 0x168

    .line 160
    .line 161
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 162
    .line 163
    .line 164
    move-result-object v3

    .line 165
    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 166
    .line 167
    .line 168
    goto :goto_2

    .line 169
    :cond_4
    :goto_1
    sget-object v3, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 170
    .line 171
    new-instance v7, Ljava/lang/StringBuilder;

    .line 172
    .line 173
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 174
    .line 175
    .line 176
    const-string v10, "the jpg is not uploaded, no need to change rotation "

    .line 177
    .line 178
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    iget v10, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 182
    .line 183
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object v7

    .line 190
    invoke-static {v3, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 194
    .line 195
    .line 196
    move-result-object v3

    .line 197
    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 198
    .line 199
    .line 200
    iget v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 201
    .line 202
    iget v5, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 203
    .line 204
    add-int/2addr v3, v5

    .line 205
    iget v5, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080:I

    .line 206
    .line 207
    sub-int/2addr v3, v5

    .line 208
    add-int/lit16 v3, v3, 0x168

    .line 209
    .line 210
    rem-int/lit16 v3, v3, 0x168

    .line 211
    .line 212
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 213
    .line 214
    .line 215
    move-result-object v3

    .line 216
    const-string v5, "ori_rotation"

    .line 217
    .line 218
    invoke-virtual {v2, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    .line 220
    .line 221
    :goto_2
    iget-wide v10, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 222
    .line 223
    iget v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 224
    .line 225
    move-object/from16 v5, p1

    .line 226
    .line 227
    invoke-static {v5, v10, v11, v3}, Lcom/intsig/camscanner/inkcore/InkUtils;->o800o8O(Landroid/content/Context;JI)V

    .line 228
    .line 229
    .line 230
    if-eqz v4, :cond_7

    .line 231
    .line 232
    if-nez v9, :cond_7

    .line 233
    .line 234
    iget-object v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->Oo08:Ljava/lang/String;

    .line 235
    .line 236
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 237
    .line 238
    .line 239
    move-result v3

    .line 240
    if-nez v3, :cond_7

    .line 241
    .line 242
    iget-object v3, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->Oo08:Ljava/lang/String;

    .line 243
    .line 244
    invoke-static {v3}, Lcom/intsig/camscanner/app/DBUtil;->〇80〇808〇O(Ljava/lang/String;)[I

    .line 245
    .line 246
    .line 247
    move-result-object v3

    .line 248
    if-eqz v3, :cond_7

    .line 249
    .line 250
    const/4 v7, 0x2

    .line 251
    new-array v9, v7, [I

    .line 252
    .line 253
    aget v7, v4, v8

    .line 254
    .line 255
    aget v4, v4, v6

    .line 256
    .line 257
    if-le v7, v4, :cond_5

    .line 258
    .line 259
    aget v10, v3, v8

    .line 260
    .line 261
    aget v11, v3, v6

    .line 262
    .line 263
    if-lt v10, v11, :cond_6

    .line 264
    .line 265
    :cond_5
    if-ge v7, v4, :cond_7

    .line 266
    .line 267
    aget v4, v3, v8

    .line 268
    .line 269
    aget v7, v3, v6

    .line 270
    .line 271
    if-le v4, v7, :cond_7

    .line 272
    .line 273
    :cond_6
    aget v4, v3, v6

    .line 274
    .line 275
    aput v4, v9, v8

    .line 276
    .line 277
    aget v3, v3, v8

    .line 278
    .line 279
    aput v3, v9, v6

    .line 280
    .line 281
    :cond_7
    if-eqz v9, :cond_8

    .line 282
    .line 283
    iget-wide v13, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 284
    .line 285
    iget v15, v1, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 286
    .line 287
    aget v16, v9, v6

    .line 288
    .line 289
    aget v17, v9, v8

    .line 290
    .line 291
    move-object/from16 v12, p1

    .line 292
    .line 293
    invoke-static/range {v12 .. v17}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->OoO8(Landroid/content/Context;JIII)V

    .line 294
    .line 295
    .line 296
    :cond_8
    const-string v1, "2"

    .line 297
    .line 298
    const-string v3, "5"

    .line 299
    .line 300
    filled-new-array {v1, v3}, [Ljava/lang/String;

    .line 301
    .line 302
    .line 303
    move-result-object v1

    .line 304
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 305
    .line 306
    iget-wide v4, v0, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 307
    .line 308
    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 309
    .line 310
    .line 311
    move-result-object v0

    .line 312
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 313
    .line 314
    .line 315
    move-result-object v0

    .line 316
    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 317
    .line 318
    .line 319
    move-result-object v0

    .line 320
    const-string v2, "sync_jpage_state != ?  AND sync_jpage_state != ?"

    .line 321
    .line 322
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 323
    .line 324
    .line 325
    move-result-object v0

    .line 326
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 327
    .line 328
    .line 329
    move-result-object v0

    .line 330
    move-object/from16 v1, p3

    .line 331
    .line 332
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    .line 334
    .line 335
    return-void
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Landroid/util/LongSparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇〇〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private OO8oO0o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O888o0o:Z

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O000()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OOO(Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 2
    .line 3
    iget-object v1, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 4
    .line 5
    new-instance v2, Landroid/content/ContentValues;

    .line 6
    .line 7
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 8
    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v4

    .line 15
    const-string v5, "cache_state"

    .line 16
    .line 17
    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 18
    .line 19
    .line 20
    const/4 v5, 0x1

    .line 21
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v6

    .line 25
    const-string v7, "status"

    .line 26
    .line 27
    invoke-virtual {v2, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 28
    .line 29
    .line 30
    iget v6, v0, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 31
    .line 32
    iget v7, v0, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 33
    .line 34
    add-int/2addr v6, v7

    .line 35
    const-string v7, "image_rotation"

    .line 36
    .line 37
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v8

    .line 41
    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 42
    .line 43
    .line 44
    iget-object v7, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->o〇0:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 45
    .line 46
    invoke-virtual {v7}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v7

    .line 50
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v7

    .line 54
    if-eqz v7, :cond_0

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    invoke-static {v7}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v7

    .line 64
    const-string v8, "_data"

    .line 65
    .line 66
    invoke-virtual {v2, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    iget-object v7, v0, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇〇888:[I

    .line 78
    .line 79
    if-nez v7, :cond_1

    .line 80
    .line 81
    invoke-static {v1}, Lcom/intsig/camscanner/app/DBUtil;->〇08O8o〇0([I)[I

    .line 82
    .line 83
    .line 84
    move-result-object v7

    .line 85
    :cond_1
    invoke-static {v1, v1, v7, v6}, Lcom/intsig/camscanner/app/DBUtil;->oO80([I[I[II)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    const-string v6, "image_border"

    .line 90
    .line 91
    invoke-virtual {v2, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    iget v0, v0, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->OO0o〇〇〇〇0:I

    .line 95
    .line 96
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    const-string v1, "enhance_mode"

    .line 101
    .line 102
    invoke-virtual {v2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 103
    .line 104
    .line 105
    const-string v0, "ocr_border"

    .line 106
    .line 107
    const-string v1, ""

    .line 108
    .line 109
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    const-string v0, "ocr_result"

    .line 113
    .line 114
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    const-string v0, "ocr_result_user"

    .line 118
    .line 119
    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    const-string v0, "ocr_paragraph"

    .line 123
    .line 124
    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    const-string v0, "sync_extra_data1"

    .line 128
    .line 129
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    const-string v0, "sync_extra_data2"

    .line 133
    .line 134
    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    const/16 v0, 0x64

    .line 138
    .line 139
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    const-string v6, "detail_index"

    .line 144
    .line 145
    invoke-virtual {v2, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    .line 147
    .line 148
    const-string v0, "contrast_index"

    .line 149
    .line 150
    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    .line 152
    .line 153
    const-string v0, "bright_index"

    .line 154
    .line 155
    invoke-virtual {v2, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    .line 157
    .line 158
    const-string v0, "image_backup"

    .line 159
    .line 160
    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    const-string v0, "2"

    .line 164
    .line 165
    const-string v4, "5"

    .line 166
    .line 167
    filled-new-array {v0, v4}, [Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    sget-object v4, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 172
    .line 173
    iget-wide v6, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 174
    .line 175
    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 176
    .line 177
    .line 178
    move-result-object v4

    .line 179
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 180
    .line 181
    .line 182
    move-result-object v4

    .line 183
    invoke-virtual {v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 184
    .line 185
    .line 186
    move-result-object v2

    .line 187
    const-string v4, "sync_jpage_state != ?  AND sync_jpage_state != ?"

    .line 188
    .line 189
    invoke-virtual {v2, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    .line 199
    .line 200
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Graphics;->〇080:Landroid/net/Uri;

    .line 201
    .line 202
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    new-array v2, v5, [Ljava/lang/String;

    .line 207
    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    iget-wide v6, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 214
    .line 215
    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v4

    .line 225
    aput-object v4, v2, v3

    .line 226
    .line 227
    const-string v4, "image_id=?"

    .line 228
    .line 229
    invoke-virtual {v0, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    .line 239
    .line 240
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$PageMark;->〇o00〇〇Oo:Landroid/net/Uri;

    .line 241
    .line 242
    iget-wide v6, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 243
    .line 244
    invoke-static {v0, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 245
    .line 246
    .line 247
    move-result-object v0

    .line 248
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 253
    .line 254
    .line 255
    move-result-object v0

    .line 256
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    .line 258
    .line 259
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Signature;->〇080:Landroid/net/Uri;

    .line 260
    .line 261
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    new-array v2, v5, [Ljava/lang/String;

    .line 266
    .line 267
    new-instance v4, Ljava/lang/StringBuilder;

    .line 268
    .line 269
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    .line 271
    .line 272
    iget-wide v5, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 273
    .line 274
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 275
    .line 276
    .line 277
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    aput-object p1, v2, v3

    .line 285
    .line 286
    const-string p1, "image_id = ? "

    .line 287
    .line 288
    invoke-virtual {v0, p1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 289
    .line 290
    .line 291
    move-result-object p1

    .line 292
    invoke-virtual {p1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 293
    .line 294
    .line 295
    move-result-object p1

    .line 296
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    .line 298
    .line 299
    return-void
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method static bridge synthetic OOO〇O0(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OoO8:Ljava/util/Map;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic Oo8Oo00oo(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Landroid/util/LruCache;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8()Landroid/util/LruCache;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private synthetic Ooo(Ljava/util/List;Landroid/content/Context;)V
    .locals 11

    .line 1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    if-eqz v3, :cond_5

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    check-cast v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 22
    .line 23
    iget-boolean v4, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O888o0o:Z

    .line 24
    .line 25
    if-eqz v4, :cond_1

    .line 26
    .line 27
    goto/16 :goto_3

    .line 28
    .line 29
    :cond_1
    const/4 v4, 0x1

    .line 30
    add-int/2addr v2, v4

    .line 31
    iget-object v5, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 32
    .line 33
    if-eqz v5, :cond_2

    .line 34
    .line 35
    const/high16 v6, 0x42c60000    # 99.0f

    .line 36
    .line 37
    int-to-float v7, v2

    .line 38
    mul-float v7, v7, v6

    .line 39
    .line 40
    int-to-float v6, v0

    .line 41
    div-float/2addr v7, v6

    .line 42
    float-to-int v6, v7

    .line 43
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 44
    .line 45
    .line 46
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    iget-object v6, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 52
    .line 53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v6, ".jpg"

    .line 57
    .line 58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    invoke-static {v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇8oOO88(Ljava/lang/String;)Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    new-instance v7, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v8

    .line 78
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    iget-object v8, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 82
    .line 83
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const-string v8, "temp"

    .line 87
    .line 88
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v6

    .line 98
    iget-object v7, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 99
    .line 100
    iget-object v7, v7, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 101
    .line 102
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 103
    .line 104
    .line 105
    move-result v7

    .line 106
    if-eqz v7, :cond_3

    .line 107
    .line 108
    iget-object v4, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 109
    .line 110
    iget-wide v7, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 111
    .line 112
    invoke-static {v4, v7, v8, v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇〇〇0〇〇0(Ljava/lang/String;JLjava/lang/String;)I

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    goto :goto_2

    .line 117
    :cond_3
    iget-object v7, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 118
    .line 119
    iget-object v7, v7, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 120
    .line 121
    invoke-static {p2, v7}, Lcom/intsig/camscanner/db/dao/TeamInfoDao;->〇080(Landroid/content/Context;Ljava/lang/String;)I

    .line 122
    .line 123
    .line 124
    move-result v7

    .line 125
    :try_start_0
    iget-object v8, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 126
    .line 127
    iget-object v8, v8, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 128
    .line 129
    iget-object v9, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o00〇〇Oo:Ljava/lang/String;

    .line 130
    .line 131
    const/4 v10, 0x2

    .line 132
    if-ne v7, v10, :cond_4

    .line 133
    .line 134
    const/4 v7, 0x1

    .line 135
    goto :goto_1

    .line 136
    :cond_4
    const/4 v7, 0x0

    .line 137
    :goto_1
    iget-object v10, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o800o8O:Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;

    .line 138
    .line 139
    invoke-static {v8, v9, v7, v6, v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/intsig/tianshu/TianShuAPI$OnProgressListener;)V
    :try_end_0
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .line 141
    .line 142
    goto :goto_2

    .line 143
    :catch_0
    move-exception v4

    .line 144
    sget-object v7, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 145
    .line 146
    invoke-static {v7, v4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 147
    .line 148
    .line 149
    const/4 v4, 0x0

    .line 150
    :goto_2
    if-lez v4, :cond_0

    .line 151
    .line 152
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 153
    .line 154
    .line 155
    move-result v4

    .line 156
    if-eqz v4, :cond_0

    .line 157
    .line 158
    invoke-static {v6, v5}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 159
    .line 160
    .line 161
    move-result v4

    .line 162
    if-eqz v4, :cond_0

    .line 163
    .line 164
    iget-object v4, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 165
    .line 166
    sget-object v6, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 167
    .line 168
    new-instance v7, Ljava/lang/StringBuilder;

    .line 169
    .line 170
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    .line 172
    .line 173
    const-string v8, "before exifRotation="

    .line 174
    .line 175
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    iget v8, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080:I

    .line 179
    .line 180
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    const-string v8, " lastMergeRotation="

    .line 184
    .line 185
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    iget v9, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 189
    .line 190
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v7

    .line 197
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    invoke-static {v5}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 201
    .line 202
    .line 203
    move-result v6

    .line 204
    iput v6, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080:I

    .line 205
    .line 206
    iget v7, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 207
    .line 208
    add-int/2addr v7, v6

    .line 209
    rem-int/lit16 v7, v7, 0x168

    .line 210
    .line 211
    iput v7, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 212
    .line 213
    sget-object v6, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 214
    .line 215
    new-instance v7, Ljava/lang/StringBuilder;

    .line 216
    .line 217
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 218
    .line 219
    .line 220
    const-string v9, "after exifRotation="

    .line 221
    .line 222
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    iget v9, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080:I

    .line 226
    .line 227
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    iget v4, v4, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 234
    .line 235
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v4

    .line 242
    invoke-static {v6, v4}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    iget-object v4, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 246
    .line 247
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->o〇0(Ljava/lang/String;)V

    .line 248
    .line 249
    .line 250
    new-instance v4, Landroid/content/ContentValues;

    .line 251
    .line 252
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 253
    .line 254
    .line 255
    const-string v6, "raw_data"

    .line 256
    .line 257
    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    const-string v5, "sync_raw_jpg_state"

    .line 261
    .line 262
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 263
    .line 264
    .line 265
    move-result-object v6

    .line 266
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 267
    .line 268
    .line 269
    sget-object v5, Lcom/intsig/camscanner/provider/Documents$Image;->Oo08:Landroid/net/Uri;

    .line 270
    .line 271
    iget-wide v6, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 272
    .line 273
    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 274
    .line 275
    .line 276
    move-result-object v3

    .line 277
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 278
    .line 279
    .line 280
    move-result-object v5

    .line 281
    const/4 v6, 0x0

    .line 282
    invoke-virtual {v5, v3, v4, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 283
    .line 284
    .line 285
    goto/16 :goto_0

    .line 286
    .line 287
    :cond_5
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 288
    .line 289
    invoke-virtual {p1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 290
    .line 291
    .line 292
    return-void
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Landroid/util/LongSparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private O〇O〇oO()Landroid/util/LongSparseArray;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Landroid/util/LongSparseArray;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 7
    .line 8
    if-eqz v1, :cond_3

    .line 9
    .line 10
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_3

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 34
    .line 35
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 36
    .line 37
    if-nez v3, :cond_1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    iget-object v3, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 47
    .line 48
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    if-nez v3, :cond_2

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    iget-wide v3, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 60
    .line 61
    iget-object v2, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 62
    .line 63
    iget v2, v2, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 64
    .line 65
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {v0, v3, v4, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_3
    :goto_1
    return-object v0
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static bridge synthetic o0ooO(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic o8(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private o8oO〇()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 7
    .line 8
    if-eqz v1, :cond_1

    .line 9
    .line 10
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_1

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-eqz v2, :cond_1

    .line 27
    .line 28
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 33
    .line 34
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 35
    .line 36
    if-eqz v3, :cond_0

    .line 37
    .line 38
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    iget-object v3, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 45
    .line 46
    invoke-virtual {v3}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 51
    .line 52
    .line 53
    move-result v3

    .line 54
    if-nez v3, :cond_0

    .line 55
    .line 56
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private oO(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 14
    .line 15
    invoke-interface {p1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->o8oO〇()V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 20
    .line 21
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_1

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 32
    .line 33
    invoke-interface {p1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->oo8ooo8O()V

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO8oO0o〇()V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 46
    .line 47
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    new-instance v2, LO0OO8〇0/〇o00〇〇Oo;

    .line 60
    .line 61
    invoke-direct {v2, p0, p1, v0}, LO0OO8〇0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;Ljava/util/List;Landroid/content/Context;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Ljava/util/HashSet;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O8:Ljava/util/HashSet;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private ooo〇8oO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O8o08O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80〇808〇O()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    const v1, 0x7f1301b4

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const v1, 0x7f1300f8

    .line 16
    .line 17
    .line 18
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 19
    .line 20
    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oo〇(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8o8o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic o〇0OOo〇0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇8(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;Ljava/util/List;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o〇8oOO88()I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-lez v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 29
    .line 30
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 31
    .line 32
    if-eqz v3, :cond_0

    .line 33
    .line 34
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 35
    .line 36
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    iget-object v2, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    if-eqz v2, :cond_0

    .line 51
    .line 52
    add-int/lit8 v1, v1, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    return v1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private o〇O()Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->oO〇()Landroidx/fragment/app/Fragment;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$5;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$5;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/control/ProgressAnimHandler;-><init>(Ljava/lang/Object;Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇O8〇〇o(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic o〇〇0〇(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO80:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇0000OOO(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Landroid/util/LongSparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;Ljava/util/List;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Ooo(Ljava/util/List;Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇8()Landroid/util/LruCache;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇0〇O0088o:Landroid/util/LruCache;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$2;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {v1}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o〇(Landroid/content/Context;)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter$2;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;I)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇0〇O0088o:Landroid/util/LruCache;

    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇0〇O0088o:Landroid/util/LruCache;

    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇80()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_2

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 30
    .line 31
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 32
    .line 33
    if-eqz v3, :cond_1

    .line 34
    .line 35
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    iget-object v2, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o00〇〇Oo()Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-eqz v2, :cond_1

    .line 48
    .line 49
    const/4 v1, 0x1

    .line 50
    :cond_2
    :goto_0
    return v1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇8〇0〇o〇O(Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 7
    .line 8
    if-eqz v1, :cond_3

    .line 9
    .line 10
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-nez v1, :cond_0

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_3

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Lcom/intsig/adapter/AbsRecyclerViewItem;

    .line 34
    .line 35
    instance-of v3, v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 36
    .line 37
    if-eqz v3, :cond_1

    .line 38
    .line 39
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->OO0o〇〇()Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-eqz v3, :cond_2

    .line 46
    .line 47
    if-eqz p1, :cond_2

    .line 48
    .line 49
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    iget-boolean v3, v3, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o〇:Z

    .line 54
    .line 55
    if-eqz v3, :cond_1

    .line 56
    .line 57
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_3
    :goto_1
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private synthetic 〇O〇80o08O(Ljava/util/List;Landroid/content/Context;Lcom/intsig/camscanner/control/ProgressAnimHandler;)V
    .locals 10

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const/4 v4, 0x0

    .line 25
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v5

    .line 29
    if-eqz v5, :cond_4

    .line 30
    .line 31
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    check-cast v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 36
    .line 37
    iget-object v6, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 38
    .line 39
    iget-object v7, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 40
    .line 41
    iget-wide v8, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 42
    .line 43
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 44
    .line 45
    .line 46
    move-result-object v8

    .line 47
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    invoke-virtual {v7}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v7

    .line 54
    invoke-static {v7}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 55
    .line 56
    .line 57
    move-result v7

    .line 58
    if-eqz v7, :cond_2

    .line 59
    .line 60
    invoke-virtual {v6}, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇080()Z

    .line 61
    .line 62
    .line 63
    move-result v7

    .line 64
    if-nez v7, :cond_0

    .line 65
    .line 66
    invoke-virtual {v6}, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o00〇〇Oo()Z

    .line 67
    .line 68
    .line 69
    move-result v7

    .line 70
    if-eqz v7, :cond_2

    .line 71
    .line 72
    :cond_0
    invoke-direct {p0, v5, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OOO(Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;Ljava/util/ArrayList;)V

    .line 73
    .line 74
    .line 75
    iget-object v6, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->O8:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 76
    .line 77
    invoke-virtual {v6}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v6

    .line 81
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 82
    .line 83
    .line 84
    move-result v6

    .line 85
    if-eqz v6, :cond_1

    .line 86
    .line 87
    iget-object v6, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->O8:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 88
    .line 89
    invoke-virtual {v6}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v6

    .line 93
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 94
    .line 95
    .line 96
    :cond_1
    iget-wide v6, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 97
    .line 98
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 99
    .line 100
    .line 101
    move-result-object v6

    .line 102
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    sget-object v6, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 106
    .line 107
    const-string v7, "loadModifyPageInfo"

    .line 108
    .line 109
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    goto :goto_1

    .line 113
    :cond_2
    iget v6, v6, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 114
    .line 115
    if-eqz v6, :cond_3

    .line 116
    .line 117
    invoke-direct {p0, p2, v5, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O0o〇〇Oo(Landroid/content/Context;Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;Ljava/util/ArrayList;)V

    .line 118
    .line 119
    .line 120
    sget-object v6, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 121
    .line 122
    const-string v7, "onlyRotatePageInfo"

    .line 123
    .line 124
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    :cond_3
    :goto_1
    iget-wide v5, v5, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 128
    .line 129
    invoke-static {v5, v6}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇〇888(J)V

    .line 130
    .line 131
    .line 132
    add-int/lit8 v4, v4, 0x1

    .line 133
    .line 134
    const/high16 v5, 0x42c60000    # 99.0f

    .line 135
    .line 136
    int-to-float v6, v4

    .line 137
    mul-float v6, v6, v5

    .line 138
    .line 139
    int-to-float v5, v3

    .line 140
    div-float/2addr v6, v5

    .line 141
    float-to-int v5, v6

    .line 142
    invoke-virtual {p3, v5}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->oo〇(I)V

    .line 143
    .line 144
    .line 145
    goto :goto_0

    .line 146
    :cond_4
    invoke-static {p2, v0}, Lcom/intsig/camscanner/db/dao/DBDaoUtil;->O8(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 151
    .line 152
    .line 153
    move-result v0

    .line 154
    if-lez v0, :cond_5

    .line 155
    .line 156
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    sget-object v3, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 161
    .line 162
    invoke-virtual {v0, v3, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .line 164
    .line 165
    goto :goto_2

    .line 166
    :catch_0
    move-exception p1

    .line 167
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 168
    .line 169
    const-string v3, "Exception "

    .line 170
    .line 171
    invoke-static {v0, v3, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 172
    .line 173
    .line 174
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 175
    .line 176
    .line 177
    move-result-wide v3

    .line 178
    const/4 p1, 0x3

    .line 179
    invoke-static {p2, v1, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇00〇8oO(Landroid/content/Context;Ljava/util/List;I)V

    .line 180
    .line 181
    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 183
    .line 184
    .line 185
    move-result-wide v5

    .line 186
    invoke-static {p2, v2, p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O8o08O8O(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 187
    .line 188
    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 190
    .line 191
    .line 192
    move-result-wide v7

    .line 193
    sget-object p1, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 194
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    .line 196
    .line 197
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .line 199
    .line 200
    const-string v9, " saveChange SyncUtil.updatePageSyncStat time="

    .line 201
    .line 202
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    sub-long v3, v5, v3

    .line 206
    .line 207
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    const-string v3, " SyncUtil.updateJpagePageSyncStat time="

    .line 211
    .line 212
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    sub-long/2addr v7, v5

    .line 216
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 217
    .line 218
    .line 219
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .line 225
    .line 226
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 227
    .line 228
    iget-wide v3, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 229
    .line 230
    invoke-static {p2, v3, v4}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 231
    .line 232
    .line 233
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 234
    .line 235
    iget-wide v4, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 236
    .line 237
    const/4 v6, 0x3

    .line 238
    const/4 v7, 0x1

    .line 239
    const/4 v8, 0x0

    .line 240
    move-object v3, p2

    .line 241
    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 245
    .line 246
    .line 247
    move-result p1

    .line 248
    if-lez p1, :cond_5

    .line 249
    .line 250
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 251
    .line 252
    iget-wide v0, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 253
    .line 254
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 255
    .line 256
    .line 257
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 258
    .line 259
    .line 260
    move-result-object p1

    .line 261
    iget-object p2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 262
    .line 263
    iget-wide v0, p2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 264
    .line 265
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 266
    .line 267
    .line 268
    move-result-object p1

    .line 269
    invoke-virtual {p1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0OOo〇0()V

    .line 270
    .line 271
    .line 272
    :cond_5
    invoke-virtual {p3}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o800o8O()V

    .line 273
    .line 274
    .line 275
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 276
    .line 277
    iget-object p2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 278
    .line 279
    iget-wide p2, p2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 280
    .line 281
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇〇0〇(Landroid/content/Context;J)I

    .line 282
    .line 283
    .line 284
    move-result p1

    .line 285
    const/16 p2, 0x8b

    .line 286
    .line 287
    if-ne p1, p2, :cond_6

    .line 288
    .line 289
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 290
    .line 291
    invoke-static {p1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->o0O0(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 292
    .line 293
    .line 294
    move-result-object p1

    .line 295
    iget-object p2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 296
    .line 297
    iget-wide p2, p2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 298
    .line 299
    invoke-static {p2, p3, p1}, Lcom/intsig/camscanner/bankcardjournal/api/BankCardJournalApi;->o〇0(JLjava/util/List;)I

    .line 300
    .line 301
    .line 302
    :cond_6
    return-void
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method static bridge synthetic 〇o(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Landroid/util/LongSparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇808〇:Landroid/util/LongSparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇oo〇(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O888o0o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;Ljava/util/List;Landroid/content/Context;Lcom/intsig/camscanner/control/ProgressAnimHandler;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇80o08O(Ljava/util/List;Landroid/content/Context;Lcom/intsig/camscanner/control/ProgressAnimHandler;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private 〇〇0o()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO00OOO()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇8oOO88()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇oo〇:Lcom/intsig/camscanner/batch/contract/ActionTips;

    .line 10
    .line 11
    if-nez v2, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    const/4 v3, 0x1

    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    iput-boolean v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇O8〇〇o:Z

    .line 18
    .line 19
    invoke-interface {v2}, Lcom/intsig/camscanner/batch/contract/ActionTips;->〇o00〇〇Oo()V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    if-ge v1, v0, :cond_2

    .line 24
    .line 25
    iput-boolean v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇O8〇〇o:Z

    .line 26
    .line 27
    invoke-interface {v2}, Lcom/intsig/camscanner/batch/contract/ActionTips;->〇080()V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_2
    invoke-interface {v2}, Lcom/intsig/camscanner/batch/contract/ActionTips;->〇o〇()V

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method static bridge synthetic 〇〇〇0〇〇0()[Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O〇8O8〇008:[Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8()Z
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v2, 0x0

    .line 16
    if-lez v0, :cond_2

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const/4 v3, 0x0

    .line 25
    :goto_0
    if-ge v3, v0, :cond_2

    .line 26
    .line 27
    iget-object v4, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 28
    .line 29
    invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    check-cast v4, Ljava/lang/Integer;

    .line 34
    .line 35
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    if-eqz v4, :cond_1

    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    goto :goto_1

    .line 43
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 49
    .line 50
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-lez v0, :cond_3

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_3
    move v1, v2

    .line 58
    :goto_2
    return v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public OO0o〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇oo〇:Lcom/intsig/camscanner/batch/contract/ActionTips;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o8oO〇()Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_1

    .line 15
    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO(Ljava/util/List;)V

    .line 17
    .line 18
    .line 19
    :cond_1
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80〇808〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OoO8(Lcom/intsig/camscanner/batch/contract/ActionTips;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇oo〇:Lcom/intsig/camscanner/batch/contract/ActionTips;

    .line 2
    .line 3
    iget-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇O8〇〇o:Z

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o8oO〇()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_0

    .line 16
    .line 17
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO(Ljava/util/List;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/batch/contract/ActionTips;->〇o〇()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public Oooo8o0〇(Ljava/lang/String;)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "isChangeAccount lastAccountSyncUID="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇888:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, " newSyncAccountUID="

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇888:Ljava/lang/String;

    .line 38
    .line 39
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    const/4 p1, 0x0

    .line 46
    return p1

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇888:Ljava/lang/String;

    .line 48
    .line 49
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    xor-int/lit8 p1, p1, 0x1

    .line 54
    .line 55
    return p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public o800o8O()Landroid/content/Intent;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8〇0〇o〇O(Z)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/4 v3, 0x0

    .line 16
    if-lez v2, :cond_2

    .line 17
    .line 18
    new-instance v2, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;

    .line 19
    .line 20
    invoke-direct {v2}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    if-eqz v4, :cond_3

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    check-cast v4, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 38
    .line 39
    if-eqz v4, :cond_0

    .line 40
    .line 41
    invoke-virtual {v4}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    iget-object v5, v4, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->Oo08:Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 46
    .line 47
    invoke-virtual {v5}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v6

    .line 51
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 52
    .line 53
    .line 54
    move-result v6

    .line 55
    if-nez v6, :cond_1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    iget-wide v6, v4, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 59
    .line 60
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    iget-object v6, v4, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->oO80:Lcom/intsig/camscanner/batch/contract/ImageDBInfo;

    .line 68
    .line 69
    invoke-virtual {v5}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v7

    .line 73
    iget v8, v6, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->〇o〇:I

    .line 74
    .line 75
    iget v6, v6, Lcom/intsig/camscanner/batch/contract/ImageDBInfo;->O8:I

    .line 76
    .line 77
    add-int/2addr v8, v6

    .line 78
    rem-int/lit16 v8, v8, 0x168

    .line 79
    .line 80
    invoke-virtual {v2, v7, v8}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->Oooo8o0〇(Ljava/lang/String;I)V

    .line 81
    .line 82
    .line 83
    iget-object v6, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 84
    .line 85
    iget-wide v7, v4, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 86
    .line 87
    invoke-virtual {v6, v7, v8}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    if-ltz v6, :cond_0

    .line 92
    .line 93
    invoke-virtual {v5}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;->〇o〇()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    iget-object v6, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 98
    .line 99
    iget-wide v7, v4, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 100
    .line 101
    invoke-virtual {v6, v7, v8}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object v4

    .line 105
    check-cast v4, [I

    .line 106
    .line 107
    invoke-virtual {v2, v5, v4}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->OO0o〇〇〇〇0(Ljava/lang/String;[I)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_2
    move-object v2, v3

    .line 112
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 113
    .line 114
    .line 115
    move-result v1

    .line 116
    if-nez v1, :cond_4

    .line 117
    .line 118
    return-object v3

    .line 119
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 120
    .line 121
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->clone()Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    check-cast v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .line 127
    goto :goto_1

    .line 128
    :catch_0
    move-exception v1

    .line 129
    sget-object v4, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 130
    .line 131
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    .line 133
    .line 134
    move-object v1, v3

    .line 135
    :goto_1
    if-nez v1, :cond_5

    .line 136
    .line 137
    return-object v3

    .line 138
    :cond_5
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O00(Ljava/util/List;)[J

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    iput-object v0, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 145
    .line 146
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    const/4 v4, 0x2

    .line 151
    invoke-static {v0, v1, v2, v4, v3}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;->O0〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;ILjava/util/List;)Landroid/content/Intent;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    return-object v0
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public oO00OOO()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0

    .line 19
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 20
    return v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80(Landroid/widget/TextView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O8o08O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇80(Landroid/app/Activity;)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "saveChange"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 20
    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O08000()Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    if-nez v1, :cond_1

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 41
    .line 42
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 47
    .line 48
    .line 49
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 50
    .line 51
    const-string v1, "saveChange not changce"

    .line 52
    .line 53
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return-void

    .line 57
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇O()Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 62
    .line 63
    invoke-interface {v2}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-virtual {v1}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇〇0〇()V

    .line 72
    .line 73
    .line 74
    const/4 v3, 0x1

    .line 75
    iput-boolean v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->o〇0:Z

    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    new-instance v4, LO0OO8〇0/〇080;

    .line 82
    .line 83
    invoke-direct {v4, p0, v0, v2, v1}, LO0OO8〇0/〇080;-><init>(Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;Ljava/util/List;Landroid/content/Context;Lcom/intsig/camscanner/control/ProgressAnimHandler;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3, v4}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 87
    .line 88
    .line 89
    return-void

    .line 90
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 91
    .line 92
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 97
    .line 98
    .line 99
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 100
    .line 101
    const-string v1, "saveChange error"

    .line 102
    .line 103
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇08O8o〇0(I)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8〇0〇o〇O(Z)Ljava/util/List;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-nez v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-gt v1, p1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 30
    .line 31
    iget-wide v1, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 32
    .line 33
    iget-boolean p1, p1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇o〇:Z

    .line 34
    .line 35
    xor-int/lit8 p1, p1, 0x1

    .line 36
    .line 37
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {v0, v1, v2, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->ooo〇8oO()V

    .line 50
    .line 51
    .line 52
    :cond_1
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇0〇O0088o(Landroidx/loader/app/LoaderManager;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇8O0〇8:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;

    .line 4
    .line 5
    const/16 v2, 0x1774

    .line 6
    .line 7
    invoke-direct {v0, p1, v1, v2}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;-><init>(Landroidx/loader/app/LoaderManager;Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager$LoaderManagerListener;I)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇80〇808〇O()Z
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "isAllSelected: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O00:Ljava/util/List;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    if-eqz v0, :cond_3

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    const/4 v2, 0x0

    .line 27
    const/4 v3, 0x0

    .line 28
    :goto_0
    if-ge v3, v0, :cond_2

    .line 29
    .line 30
    iget-object v4, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 31
    .line 32
    invoke-virtual {v4, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    check-cast v4, Ljava/lang/Boolean;

    .line 37
    .line 38
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    if-nez v4, :cond_1

    .line 43
    .line 44
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 45
    .line 46
    const-string v1, "isAllSelected: FALSE!"

    .line 47
    .line 48
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    return v2

    .line 52
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 56
    .line 57
    const-string v2, "isAllSelected: TRUE!"

    .line 58
    .line 59
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return v1

    .line 63
    :cond_3
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 64
    .line 65
    const-string v2, "isAllSelected: EXIT EMPTY LIST!"

    .line 66
    .line 67
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return v1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇8o8o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O8:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o00〇〇Oo(Ljava/util/HashSet;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇0〇O0088o:Landroid/util/LruCache;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O00(I)V
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8〇0〇o〇O(Z)Ljava/util/List;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    iget-wide v1, v1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 34
    .line 35
    iget-object v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 36
    .line 37
    invoke-virtual {v3, v1, v2}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    if-ltz v3, :cond_1

    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 44
    .line 45
    invoke-virtual {v3, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    check-cast v3, Ljava/lang/Integer;

    .line 50
    .line 51
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    add-int/2addr v3, p1

    .line 56
    rem-int/lit16 v3, v3, 0x168

    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 59
    .line 60
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-virtual {v4, v1, v2, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 69
    .line 70
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    invoke-virtual {v3, v1, v2, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇O888o0o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oo88o8O:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x1

    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O888o0o:Z

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇〇8O0〇8()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O8o08O(Landroid/app/Activity;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "intent == null"

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    const-string v1, "extra_doc_info"

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const-string v2, "extra_can_check"

    .line 34
    .line 35
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO80:Z

    .line 40
    .line 41
    instance-of v0, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 42
    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    check-cast v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 46
    .line 47
    iput-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 48
    .line 49
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 50
    .line 51
    if-eqz v0, :cond_3

    .line 52
    .line 53
    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 54
    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    array-length v0, v0

    .line 58
    if-nez v0, :cond_4

    .line 59
    .line 60
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 61
    .line 62
    const-string v1, "illegal data"

    .line 63
    .line 64
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 68
    .line 69
    .line 70
    :cond_4
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇O〇(Z)V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "checkAllItem: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8〇0〇o〇O(Z)Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    sget-object p1, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 20
    .line 21
    const-string v0, "checkAllItem: EXIT LIST EMPTY!"

    .line 22
    .line 23
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_2

    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    check-cast v1, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 42
    .line 43
    if-eqz v1, :cond_1

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    iget-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oooo8o0〇:Landroid/util/LongSparseArray;

    .line 52
    .line 53
    iget-wide v3, v1, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 54
    .line 55
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-virtual {v2, v3, v4, v1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 60
    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 63
    .line 64
    invoke-virtual {v1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->ooo〇8oO()V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇o00〇〇Oo(Landroid/content/Intent;)V
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    const-string v0, "extra_multi_capture_status"

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    instance-of v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    sget-object p1, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 15
    .line 16
    const-string v0, "parcelable is not MultiCaptureResultStatus"

    .line 17
    .line 18
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->〇80〇808〇O()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_4

    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->O〇O〇oO()Landroid/util/LongSparseArray;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->o〇0()Ljava/util/List;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-eqz v1, :cond_3

    .line 47
    .line 48
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    check-cast v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 53
    .line 54
    iget-wide v2, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 55
    .line 56
    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    if-ltz v2, :cond_2

    .line 61
    .line 62
    iget-wide v2, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 63
    .line 64
    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    check-cast v2, Ljava/lang/Integer;

    .line 69
    .line 70
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 71
    .line 72
    .line 73
    move-result v2

    .line 74
    iget-object v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇:Landroid/util/LongSparseArray;

    .line 75
    .line 76
    iget-wide v4, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 77
    .line 78
    iget v6, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 79
    .line 80
    sub-int/2addr v6, v2

    .line 81
    add-int/lit16 v6, v6, 0x2d0

    .line 82
    .line 83
    rem-int/lit16 v6, v6, 0x168

    .line 84
    .line 85
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-virtual {v3, v4, v5, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 90
    .line 91
    .line 92
    :cond_2
    iget-object v2, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇O〇:Landroid/util/LongSparseArray;

    .line 93
    .line 94
    iget-wide v3, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 95
    .line 96
    iget-object v5, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 97
    .line 98
    invoke-virtual {v2, v3, v4, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 99
    .line 100
    .line 101
    sget-object v2, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 102
    .line 103
    new-instance v3, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string v4, "imageChange.imageId="

    .line 109
    .line 110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    iget-wide v4, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 114
    .line 115
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oo08()V

    .line 127
    .line 128
    .line 129
    :cond_4
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public 〇〇808〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->oO80:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇080:Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditContract$View;->getCurActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/16 v2, 0xa4

    .line 12
    .line 13
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    const/4 v3, 0x4

    .line 18
    invoke-static {v0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80〇808〇O:I

    .line 23
    .line 24
    const/4 v3, 0x2

    .line 25
    if-lez v1, :cond_2

    .line 26
    .line 27
    if-lez v2, :cond_2

    .line 28
    .line 29
    sub-int v4, v1, v0

    .line 30
    .line 31
    add-int v5, v2, v0

    .line 32
    .line 33
    div-int/2addr v4, v5

    .line 34
    if-le v4, v3, :cond_1

    .line 35
    .line 36
    add-int v3, v2, v0

    .line 37
    .line 38
    mul-int v3, v3, v4

    .line 39
    .line 40
    add-int/2addr v3, v0

    .line 41
    if-le v3, v1, :cond_0

    .line 42
    .line 43
    const/4 v4, -0x1

    .line 44
    :cond_0
    move v3, v4

    .line 45
    :cond_1
    add-int/lit8 v4, v3, 0x1

    .line 46
    .line 47
    mul-int v4, v4, v0

    .line 48
    .line 49
    sub-int v0, v1, v4

    .line 50
    .line 51
    div-int/2addr v0, v3

    .line 52
    iput v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇〇〇0:I

    .line 53
    .line 54
    int-to-float v0, v0

    .line 55
    const v4, 0x43948000    # 297.0f

    .line 56
    .line 57
    .line 58
    mul-float v0, v0, v4

    .line 59
    .line 60
    const/high16 v4, 0x43520000    # 210.0f

    .line 61
    .line 62
    div-float/2addr v0, v4

    .line 63
    float-to-int v0, v0

    .line 64
    iput v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8o8o〇:I

    .line 65
    .line 66
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 67
    .line 68
    new-instance v4, Ljava/lang/StringBuilder;

    .line 69
    .line 70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string v5, "pageItemWidth="

    .line 74
    .line 75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    iget v5, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->OO0o〇〇〇〇0:I

    .line 79
    .line 80
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string v5, " pageItemHeight="

    .line 84
    .line 85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    iget v5, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8o8o〇:I

    .line 89
    .line 90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v5, " numColumn="

    .line 94
    .line 95
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string v5, " viewWidth="

    .line 102
    .line 103
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    const-string v1, " pageItemImgWidthOri="

    .line 110
    .line 111
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v1, " mItemMargin="

    .line 118
    .line 119
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget v1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇80〇808〇O:I

    .line 123
    .line 124
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    return v3
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇〇8O0〇8(I)V
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇00:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "enhanceModeChange enhanceMode="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇8〇0〇o〇O(Z)Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_1

    .line 44
    .line 45
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    check-cast v2, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;

    .line 50
    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇〇808〇:Landroid/util/LongSparseArray;

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/intsig/camscanner/recycler_adapter/item/ReeditPageItem;->〇O00()Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    iget-wide v4, v2, Lcom/intsig/camscanner/batch/contract/ReeditPageItemData;->〇080:J

    .line 58
    .line 59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v3, v4, v5, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    iput-boolean v0, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->Oo08:Z

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/batch/contract/BatchImageReeditPresenter;->〇o〇:Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/certificate_package/manager/LoaderCallbackManager;->o〇0()V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.class public final Lcom/intsig/camscanner/R$xml;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final activity_message_setting:I = 0x7f160000

.field public static final activity_revert_privacy_setting:I = 0x7f160001

.field public static final analytics_gobal_config:I = 0x7f160002

.field public static final appsflyer_backup_rules:I = 0x7f160003

.field public static final bd_file_paths:I = 0x7f160004

.field public static final capture_appwidget_info:I = 0x7f160005

.field public static final certificate_more_search:I = 0x7f160006

.field public static final edu_upgrade_android:I = 0x7f160007

.field public static final file_paths:I = 0x7f160008

.field public static final fragment_main_header_view_scene:I = 0x7f160009

.field public static final fragment_office_doc_preview_scene:I = 0x7f16000a

.field public static final fragment_pagelist_container_scene:I = 0x7f16000b

.field public static final fragment_pagelist_container_scene_default:I = 0x7f16000c

.field public static final fragment_pdf_edit_scene:I = 0x7f16000d

.field public static final gma_ad_services_config:I = 0x7f16000e

.field public static final image_share_filepaths:I = 0x7f16000f

.field public static final layout_vip_month_right_scene:I = 0x7f160010

.field public static final motion_layout_scene_function_foreign_recomend:I = 0x7f160011

.field public static final motion_layout_scene_function_recomend:I = 0x7f160012

.field public static final motion_pagelist_coordination_header_scene:I = 0x7f160013

.field public static final mtg_provider_paths:I = 0x7f160014

.field public static final network_security_config:I = 0x7f160015

.field public static final settings_document_manager:I = 0x7f160016

.field public static final settings_excel:I = 0x7f160017

.field public static final settings_export_document:I = 0x7f160018

.field public static final settings_help:I = 0x7f160019

.field public static final settings_image_scan:I = 0x7f16001a

.field public static final settings_ocr:I = 0x7f16001b

.field public static final settings_ocr_full:I = 0x7f16001c

.field public static final settings_pdf:I = 0x7f16001d

.field public static final settings_security:I = 0x7f16001e

.field public static final sync_state_setting:I = 0x7f16001f

.field public static final tencent_file_paths:I = 0x7f160020

.field public static final toutiao_file_paths:I = 0x7f160021

.field public static final widget_provider_main_search:I = 0x7f160022

.field public static final widget_provider_multi_mode_1_1:I = 0x7f160023

.field public static final widget_provider_multi_mode_2_2:I = 0x7f160024

.field public static final widget_provider_qrcode_mode_1_1:I = 0x7f160025

.field public static final widget_provider_qrcode_mode_2_2:I = 0x7f160026

.field public static final widget_provider_recent_doc_4_2:I = 0x7f160027

.field public static final widget_provider_single_mode_1_1:I = 0x7f160028

.field public static final widget_provider_single_mode_2_2:I = 0x7f160029


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

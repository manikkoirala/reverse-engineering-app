.class public Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;
.super Landroid/widget/CursorAdapter;
.source "MultiChoiceCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;,
        Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;
    }
.end annotation


# static fields
.field public static O〇o88o08〇:I


# instance fields
.field protected O0O:Z

.field private O88O:I

.field protected O8o08O8O:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private OO:I

.field private OO〇00〇8oO:I

.field private Oo80:Landroid/view/animation/RotateAnimation;

.field protected final o0:I

.field private o8o:Z

.field private o8oOOo:I

.field private o8〇OO0〇0o:Z

.field private oOO〇〇:Landroid/content/Context;

.field private oOo0:Ljava/lang/String;

.field private oOo〇8o008:Landroid/view/View$OnClickListener;

.field private oo8ooo8O:Z

.field private ooo0〇〇O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field protected o〇00O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation
.end field

.field private o〇oO:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;",
            ">;"
        }
    .end annotation
.end field

.field protected 〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

.field protected 〇08O〇00〇o:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private 〇08〇o0O:Ljava/text/SimpleDateFormat;

.field private 〇0O:Landroid/view/animation/Animation;

.field private 〇8〇oO〇〇8o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:I

.field private 〇O〇〇O8:I

.field private 〇o0O:Landroid/view/View;

.field private 〇〇08O:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇o〇:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/intsig/camscanner/adapter/QueryInterface;I)V
    .locals 3

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const/4 p2, 0x1

    .line 19
    iput p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o0:I

    const/4 v1, 0x0

    .line 20
    iput-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0O:Landroid/view/animation/Animation;

    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    .line 22
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇08O:Ljava/util/HashMap;

    .line 23
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O0O:Z

    .line 24
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    .line 25
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    const/4 v2, -0x1

    .line 26
    iput v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O88O:I

    .line 27
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    .line 28
    iput-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 29
    new-instance p2, Ljava/text/SimpleDateFormat;

    invoke-direct {p2}, Ljava/text/SimpleDateFormat;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08〇o0O:Ljava/text/SimpleDateFormat;

    .line 30
    iput-object p3, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO:I

    .line 32
    iput p4, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 33
    sput p4, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O〇o88o08〇:I

    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0000OOO(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    const/4 p2, 0x1

    .line 2
    iput p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o0:I

    const/4 v1, 0x0

    .line 3
    iput-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0O:Landroid/view/animation/Animation;

    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    .line 5
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇08O:Ljava/util/HashMap;

    .line 6
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O0O:Z

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    .line 8
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    const/4 v0, -0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O88O:I

    .line 10
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    .line 11
    iput-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 12
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08〇o0O:Ljava/text/SimpleDateFormat;

    .line 13
    iput p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO:I

    .line 14
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    .line 15
    iput-object p3, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 16
    iget p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    sput p2, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O〇o88o08〇:I

    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0000OOO(Landroid/content/Context;)V

    return-void
.end method

.method private O8〇o(Landroid/widget/ImageView;Landroid/widget/ImageView;J)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x8

    .line 5
    .line 6
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1, p3, p4}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O00(Landroid/widget/ImageView;J)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private OOO〇O0()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/view/animation/RotateAnimation;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    const v3, 0x43b38000    # 359.0f

    .line 9
    .line 10
    .line 11
    const/4 v4, 0x1

    .line 12
    const/high16 v5, 0x3f000000    # 0.5f

    .line 13
    .line 14
    const/4 v6, 0x1

    .line 15
    const/high16 v7, 0x3f000000    # 0.5f

    .line 16
    .line 17
    move-object v1, v0

    .line 18
    invoke-direct/range {v1 .. v7}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    .line 22
    .line 23
    const-wide/16 v1, 0x384

    .line 24
    .line 25
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    .line 29
    .line 30
    const/4 v1, -0x1

    .line 31
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    .line 35
    .line 36
    const-wide/16 v1, -0x1

    .line 37
    .line 38
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartTime(J)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    .line 42
    .line 43
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    .line 44
    .line 45
    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 49
    .line 50
    .line 51
    :cond_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private o800o8O(J)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇00〇8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08〇o0O:Ljava/text/SimpleDateFormat;

    .line 5
    .line 6
    new-instance v1, Ljava/util/Date;

    .line 7
    .line 8
    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private oo88o8O(Landroid/content/Context;Ljava/lang/String;J[Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 1
    if-eqz p5, :cond_3

    .line 2
    .line 3
    array-length p5, p5

    .line 4
    if-nez p5, :cond_0

    .line 5
    .line 6
    goto :goto_1

    .line 7
    :cond_0
    new-instance p5, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-static {p3, p4}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v2, "_id"

    .line 24
    .line 25
    const-string v3, "image_titile"

    .line 26
    .line 27
    const-string v4, "ocr_result"

    .line 28
    .line 29
    const-string v5, "ocr_result_user"

    .line 30
    .line 31
    const-string v6, "note"

    .line 32
    .line 33
    const-string v7, "ocr_string"

    .line 34
    .line 35
    filled-new-array/range {v2 .. v7}, [Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    const/4 v3, 0x0

    .line 40
    const/4 v4, 0x0

    .line 41
    const/4 v5, 0x0

    .line 42
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    if-eqz p1, :cond_2

    .line 47
    .line 48
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    if-eqz p2, :cond_1

    .line 53
    .line 54
    const/4 p2, 0x1

    .line 55
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const/4 p2, 0x2

    .line 63
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const/4 p2, 0x3

    .line 71
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const/4 p2, 0x4

    .line 79
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    const/4 p2, 0x5

    .line 87
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 96
    .line 97
    .line 98
    :cond_2
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    const-string p2, "\\s+"

    .line 103
    .line 104
    const-string p3, ""

    .line 105
    .line 106
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    const-string p2, "\r|\n"

    .line 111
    .line 112
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    return-object p1

    .line 117
    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 118
    return-object p1
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private oo〇(JLandroid/widget/TextView;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 2
    .line 3
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 14
    .line 15
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;->〇080()J

    .line 26
    .line 27
    .line 28
    move-result-wide p1

    .line 29
    invoke-static {p1, p2}, Lcom/intsig/utils/MemoryUtils;->〇o〇(J)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string p1, ""

    .line 38
    .line 39
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o〇〇0〇(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;

    .line 6
    .line 7
    if-nez v0, :cond_3

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;-><init>()V

    .line 12
    .line 13
    .line 14
    const v1, 0x7f0a0562

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Landroid/widget/TextView;

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o0ooO(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 24
    .line 25
    .line 26
    const v1, 0x7f0a0506

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Landroid/widget/CheckBox;

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇0000OOO(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/CheckBox;)V

    .line 36
    .line 37
    .line 38
    const v1, 0x7f0a053e

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    check-cast v1, Landroid/widget/ImageView;

    .line 46
    .line 47
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 48
    .line 49
    .line 50
    const v1, 0x7f0a1970

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    check-cast v1, Landroid/widget/ImageView;

    .line 58
    .line 59
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O08000(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 60
    .line 61
    .line 62
    const v1, 0x7f0a0552

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    check-cast v1, Landroid/widget/TextView;

    .line 70
    .line 71
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 72
    .line 73
    .line 74
    const v1, 0x7f0a1172

    .line 75
    .line 76
    .line 77
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    check-cast v1, Landroid/widget/ImageView;

    .line 82
    .line 83
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo8Oo00oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 84
    .line 85
    .line 86
    const v1, 0x7f0a1973

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    check-cast v1, Landroid/widget/TextView;

    .line 94
    .line 95
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8〇0〇o〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 96
    .line 97
    .line 98
    const v1, 0x7f0a1192

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    check-cast v1, Landroid/widget/TextView;

    .line 106
    .line 107
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇〇0〇〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 108
    .line 109
    .line 110
    const v1, 0x7f0a0529

    .line 111
    .line 112
    .line 113
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    check-cast v1, Landroid/widget/ImageView;

    .line 118
    .line 119
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0OOo〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 120
    .line 121
    .line 122
    const v1, 0x7f0a0478

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    check-cast v1, Landroid/widget/ImageView;

    .line 130
    .line 131
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OOO〇O0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 132
    .line 133
    .line 134
    const v1, 0x7f0a0479

    .line 135
    .line 136
    .line 137
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v1

    .line 141
    check-cast v1, Landroid/widget/ImageView;

    .line 142
    .line 143
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇〇0〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 144
    .line 145
    .line 146
    const v1, 0x7f0a075f

    .line 147
    .line 148
    .line 149
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    check-cast v1, Landroid/widget/ImageView;

    .line 154
    .line 155
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/ImageView;)V

    .line 156
    .line 157
    .line 158
    const v1, 0x7f0a0bdd

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    .line 166
    .line 167
    .line 168
    const v1, 0x7f0a13c1

    .line 169
    .line 170
    .line 171
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 172
    .line 173
    .line 174
    move-result-object v1

    .line 175
    check-cast v1, Landroid/widget/TextView;

    .line 176
    .line 177
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oO(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 178
    .line 179
    .line 180
    const v1, 0x7f0a13c3

    .line 181
    .line 182
    .line 183
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 184
    .line 185
    .line 186
    move-result-object v1

    .line 187
    check-cast v1, Landroid/widget/TextView;

    .line 188
    .line 189
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 190
    .line 191
    .line 192
    const v1, 0x7f0a0bdc

    .line 193
    .line 194
    .line 195
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 196
    .line 197
    .line 198
    move-result-object v1

    .line 199
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    .line 200
    .line 201
    .line 202
    const v1, 0x7f0a13c2

    .line 203
    .line 204
    .line 205
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    check-cast v1, Landroid/widget/TextView;

    .line 210
    .line 211
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇08O8o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 212
    .line 213
    .line 214
    const v1, 0x7f0a1995

    .line 215
    .line 216
    .line 217
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 218
    .line 219
    .line 220
    move-result-object v1

    .line 221
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇O8〇〇o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    .line 222
    .line 223
    .line 224
    iget-boolean v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    .line 225
    .line 226
    if-eqz v1, :cond_0

    .line 227
    .line 228
    const v1, 0x7f0a13bd

    .line 229
    .line 230
    .line 231
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 232
    .line 233
    .line 234
    move-result-object v1

    .line 235
    check-cast v1, Landroid/widget/TextView;

    .line 236
    .line 237
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇0o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/TextView;)V

    .line 238
    .line 239
    .line 240
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 241
    .line 242
    const/4 v2, 0x1

    .line 243
    if-eq v1, v2, :cond_1

    .line 244
    .line 245
    if-nez v1, :cond_2

    .line 246
    .line 247
    :cond_1
    const v1, 0x7f0a1a29

    .line 248
    .line 249
    .line 250
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 251
    .line 252
    .line 253
    move-result-object v1

    .line 254
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8〇o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    .line 255
    .line 256
    .line 257
    const v1, 0x7f0a0f85

    .line 258
    .line 259
    .line 260
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 261
    .line 262
    .line 263
    move-result-object v1

    .line 264
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇00〇8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    .line 265
    .line 266
    .line 267
    const v1, 0x7f0a00bc

    .line 268
    .line 269
    .line 270
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 271
    .line 272
    .line 273
    move-result-object v1

    .line 274
    check-cast v1, Landroidx/appcompat/widget/AppCompatImageView;

    .line 275
    .line 276
    invoke-static {v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroidx/appcompat/widget/AppCompatImageView;)V

    .line 277
    .line 278
    .line 279
    invoke-static {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    .line 280
    .line 281
    .line 282
    move-result-object v1

    .line 283
    if-eqz v1, :cond_2

    .line 284
    .line 285
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOo〇8o008:Landroid/view/View$OnClickListener;

    .line 286
    .line 287
    if-eqz v1, :cond_2

    .line 288
    .line 289
    invoke-static {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    .line 290
    .line 291
    .line 292
    move-result-object v1

    .line 293
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOo〇8o008:Landroid/view/View$OnClickListener;

    .line 294
    .line 295
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    .line 297
    .line 298
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 299
    .line 300
    .line 301
    :cond_3
    return-void
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private 〇0000OOO(Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 2
    .line 3
    new-instance p1, Ljava/util/HashSet;

    .line 4
    .line 5
    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 6
    .line 7
    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 9
    .line 10
    new-instance p1, Ljava/util/HashSet;

    .line 11
    .line 12
    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08O〇00〇o:Ljava/util/HashSet;

    .line 16
    .line 17
    new-instance p1, Ljava/util/HashSet;

    .line 18
    .line 19
    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->ooo0〇〇O:Ljava/util/HashSet;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇00〇8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 14
    .line 15
    if-nez v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string v1, "yyyy"

    .line 19
    .line 20
    const-string v2, "yy"

    .line 21
    .line 22
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v0, " HH:mm"

    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇o〇:Ljava/lang/String;

    .line 44
    .line 45
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08〇o0O:Ljava/text/SimpleDateFormat;

    .line 53
    .line 54
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇o〇:Ljava/lang/String;

    .line 58
    .line 59
    :goto_1
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇08O8o〇0(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇O00(Landroid/widget/ImageView;J)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 9
    .line 10
    if-eq v0, v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 13
    .line 14
    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 16
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v2, ""

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p2

    .line 34
    invoke-static {v0, p2}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    iget-object p3, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 39
    .line 40
    invoke-static {p3}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 41
    .line 42
    .line 43
    move-result-object p3

    .line 44
    invoke-virtual {p3, p2}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 45
    .line 46
    .line 47
    move-result-object p3

    .line 48
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O888o0o(Ljava/lang/String;)Lcom/bumptech/glide/request/RequestOptions;

    .line 49
    .line 50
    .line 51
    move-result-object p2

    .line 52
    invoke-virtual {p3, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    const p3, 0x3f19999a    # 0.6f

    .line 57
    .line 58
    .line 59
    invoke-virtual {p2, p3}, Lcom/bumptech/glide/RequestBuilder;->O00(F)Lcom/bumptech/glide/RequestBuilder;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    invoke-virtual {p2, p1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
.end method

.method private 〇O888o0o(Ljava/lang/String;)Lcom/bumptech/glide/request/RequestOptions;
    .locals 4

    .line 1
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 13
    .line 14
    const v1, 0x7f080249

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇8o8o〇(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇o〇()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 34
    .line 35
    new-instance v1, Lcom/intsig/camscanner/util/GlideRoundTransform;

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 38
    .line 39
    const/4 v3, 0x2

    .line 40
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/util/GlideRoundTransform;-><init>(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80〇808〇O()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 58
    .line 59
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;

    .line 60
    .line 61
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;-><init>(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    check-cast p1, Lcom/bumptech/glide/request/RequestOptions;

    .line 69
    .line 70
    return-object p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇oOO8O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oO80(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oO80(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇888(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-static {p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇888(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇oo〇(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 14
    .line 15
    if-nez v4, :cond_0

    .line 16
    .line 17
    new-instance v4, Ljava/util/HashMap;

    .line 18
    .line 19
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 20
    .line 21
    .line 22
    iput-object v4, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 26
    .line 27
    .line 28
    :goto_0
    array-length v4, p2

    .line 29
    const/4 v5, 0x0

    .line 30
    const/4 v6, 0x0

    .line 31
    :goto_1
    if-ge v6, v4, :cond_2

    .line 32
    .line 33
    aget-object v7, p2, v6

    .line 34
    .line 35
    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v7

    .line 39
    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 40
    .line 41
    .line 42
    move-result v8

    .line 43
    const/4 v9, -0x1

    .line 44
    if-le v8, v9, :cond_1

    .line 45
    .line 46
    if-ge v8, v3, :cond_1

    .line 47
    .line 48
    iget-object v9, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 49
    .line 50
    new-instance v10, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;

    .line 51
    .line 52
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    .line 53
    .line 54
    .line 55
    move-result v11

    .line 56
    invoke-direct {v10, p0, v7, v8, v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;-><init>(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;Ljava/lang/String;II)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v9, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 66
    .line 67
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    .line 68
    .line 69
    .line 70
    move-result p2

    .line 71
    const/4 v2, 0x0

    .line 72
    if-lez p2, :cond_c

    .line 73
    .line 74
    new-instance p2, Ljava/util/ArrayList;

    .line 75
    .line 76
    iget-object v3, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇oO:Ljava/util/HashMap;

    .line 77
    .line 78
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-direct {p2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 83
    .line 84
    .line 85
    new-instance v3, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$1;

    .line 86
    .line 87
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$1;-><init>(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;)V

    .line 88
    .line 89
    .line 90
    invoke-static {p2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 91
    .line 92
    .line 93
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    const/4 v4, 0x0

    .line 98
    const/4 v6, 0x0

    .line 99
    :goto_2
    if-ge v4, v3, :cond_c

    .line 100
    .line 101
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    move-result-object v7

    .line 105
    check-cast v7, Ljava/util/Map$Entry;

    .line 106
    .line 107
    if-eqz v7, :cond_b

    .line 108
    .line 109
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 110
    .line 111
    .line 112
    move-result-object v7

    .line 113
    check-cast v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;

    .line 114
    .line 115
    if-eqz v7, :cond_b

    .line 116
    .line 117
    const-string v8, "..."

    .line 118
    .line 119
    if-nez v4, :cond_4

    .line 120
    .line 121
    iget v2, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 122
    .line 123
    iget v6, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o〇:I

    .line 124
    .line 125
    add-int/2addr v6, v2

    .line 126
    const/4 v9, 0x2

    .line 127
    if-lt v2, v9, :cond_3

    .line 128
    .line 129
    new-instance v2, Ljava/lang/StringBuilder;

    .line 130
    .line 131
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    iget v7, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 138
    .line 139
    add-int/lit8 v7, v7, -0x1

    .line 140
    .line 141
    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v7

    .line 145
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v2

    .line 152
    goto/16 :goto_4

    .line 153
    .line 154
    :cond_3
    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v2

    .line 158
    goto/16 :goto_4

    .line 159
    .line 160
    :cond_4
    iget v9, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 161
    .line 162
    sub-int v10, v9, v6

    .line 163
    .line 164
    const/4 v11, 0x3

    .line 165
    if-gt v10, v11, :cond_7

    .line 166
    .line 167
    iget v10, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o〇:I

    .line 168
    .line 169
    add-int/2addr v9, v10

    .line 170
    add-int/lit8 v10, v3, -0x1

    .line 171
    .line 172
    if-ne v4, v10, :cond_6

    .line 173
    .line 174
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    add-int/lit8 v10, v10, -0x1

    .line 179
    .line 180
    if-lt v9, v10, :cond_5

    .line 181
    .line 182
    new-instance v8, Ljava/lang/StringBuilder;

    .line 183
    .line 184
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    .line 186
    .line 187
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object v2

    .line 194
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    goto/16 :goto_3

    .line 202
    .line 203
    :cond_5
    sub-int v10, v9, v6

    .line 204
    .line 205
    if-lez v10, :cond_a

    .line 206
    .line 207
    new-instance v10, Ljava/lang/StringBuilder;

    .line 208
    .line 209
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    add-int/lit8 v9, v9, 0x1

    .line 216
    .line 217
    invoke-virtual {p1, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object v2

    .line 221
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    .line 226
    .line 227
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object v2

    .line 231
    goto/16 :goto_3

    .line 232
    .line 233
    :cond_6
    if-le v9, v6, :cond_a

    .line 234
    .line 235
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 236
    .line 237
    .line 238
    move-result v8

    .line 239
    if-gt v9, v8, :cond_a

    .line 240
    .line 241
    new-instance v8, Ljava/lang/StringBuilder;

    .line 242
    .line 243
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .line 245
    .line 246
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    .line 248
    .line 249
    invoke-virtual {p1, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 250
    .line 251
    .line 252
    move-result-object v2

    .line 253
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 257
    .line 258
    .line 259
    move-result-object v2

    .line 260
    goto/16 :goto_3

    .line 261
    .line 262
    :cond_7
    new-instance v9, Ljava/lang/StringBuilder;

    .line 263
    .line 264
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    .line 266
    .line 267
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    add-int/lit8 v2, v6, 0x1

    .line 271
    .line 272
    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    .line 278
    .line 279
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 283
    .line 284
    .line 285
    move-result-object v2

    .line 286
    iget v6, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 287
    .line 288
    iget v9, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o〇:I

    .line 289
    .line 290
    add-int/2addr v9, v6

    .line 291
    add-int/lit8 v10, v3, -0x1

    .line 292
    .line 293
    if-ne v4, v10, :cond_9

    .line 294
    .line 295
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 296
    .line 297
    .line 298
    move-result v6

    .line 299
    add-int/lit8 v6, v6, -0x1

    .line 300
    .line 301
    if-lt v9, v6, :cond_8

    .line 302
    .line 303
    new-instance v6, Ljava/lang/StringBuilder;

    .line 304
    .line 305
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    .line 307
    .line 308
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    iget v2, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 312
    .line 313
    add-int/lit8 v2, v2, -0x1

    .line 314
    .line 315
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 316
    .line 317
    .line 318
    move-result-object v2

    .line 319
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    .line 321
    .line 322
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 323
    .line 324
    .line 325
    move-result-object v2

    .line 326
    goto :goto_3

    .line 327
    :cond_8
    new-instance v6, Ljava/lang/StringBuilder;

    .line 328
    .line 329
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    .line 331
    .line 332
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    .line 334
    .line 335
    iget v2, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 336
    .line 337
    add-int/lit8 v2, v2, -0x1

    .line 338
    .line 339
    add-int/lit8 v9, v9, 0x1

    .line 340
    .line 341
    invoke-virtual {p1, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 342
    .line 343
    .line 344
    move-result-object v2

    .line 345
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 352
    .line 353
    .line 354
    move-result-object v2

    .line 355
    goto :goto_3

    .line 356
    :cond_9
    add-int/lit8 v6, v6, -0x1

    .line 357
    .line 358
    if-le v9, v6, :cond_a

    .line 359
    .line 360
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 361
    .line 362
    .line 363
    move-result v6

    .line 364
    if-gt v9, v6, :cond_a

    .line 365
    .line 366
    new-instance v6, Ljava/lang/StringBuilder;

    .line 367
    .line 368
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 369
    .line 370
    .line 371
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    .line 373
    .line 374
    iget v2, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 375
    .line 376
    add-int/lit8 v2, v2, -0x1

    .line 377
    .line 378
    invoke-virtual {p1, v2, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v2

    .line 382
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    .line 384
    .line 385
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 386
    .line 387
    .line 388
    move-result-object v2

    .line 389
    :cond_a
    :goto_3
    iget v6, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o00〇〇Oo:I

    .line 390
    .line 391
    iget v7, v7, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$SearchFilter;->〇o〇:I

    .line 392
    .line 393
    add-int/2addr v6, v7

    .line 394
    :cond_b
    :goto_4
    add-int/lit8 v4, v4, 0x1

    .line 395
    .line 396
    goto/16 :goto_2

    .line 397
    .line 398
    :cond_c
    new-instance p1, Ljava/lang/StringBuilder;

    .line 399
    .line 400
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 401
    .line 402
    .line 403
    const-string p2, "getSearchFilterContent time="

    .line 404
    .line 405
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 409
    .line 410
    .line 411
    move-result-wide v3

    .line 412
    sub-long/2addr v3, v0

    .line 413
    invoke-virtual {p1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 414
    .line 415
    .line 416
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 417
    .line 418
    .line 419
    move-result-object p1

    .line 420
    const-string p2, "MultiChoiceCursorAdapter"

    .line 421
    .line 422
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    .line 424
    .line 425
    return-object v2
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
.end method

.method private 〇〇0o(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8ooOoo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo8Oo00oo(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public OoO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇8O8〇008(Landroid/content/Context;ZZ)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "ZZ)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const-string p2, "belong_state>= -1"

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 p2, 0x0

    .line 7
    :goto_0
    const-string v0, "_id"

    .line 8
    .line 9
    if-eqz p3, :cond_2

    .line 10
    .line 11
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result p3

    .line 15
    if-eqz p3, :cond_1

    .line 16
    .line 17
    const-string p2, "_id > 0 "

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    new-instance p3, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string p2, " and "

    .line 29
    .line 30
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string p2, " >0 "

    .line 37
    .line 38
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    :cond_2
    :goto_1
    move-object v4, p2

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 51
    .line 52
    filled-new-array {v0}, [Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    const/4 v5, 0x0

    .line 57
    const/4 v6, 0x0

    .line 58
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    new-instance p2, Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .line 66
    .line 67
    if-eqz p1, :cond_6

    .line 68
    .line 69
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 70
    .line 71
    .line 72
    move-result p3

    .line 73
    if-eqz p3, :cond_5

    .line 74
    .line 75
    const/4 p3, 0x0

    .line 76
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getLong(I)J

    .line 77
    .line 78
    .line 79
    move-result-wide v0

    .line 80
    iget-object p3, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 81
    .line 82
    invoke-virtual {p3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 83
    .line 84
    .line 85
    move-result-object p3

    .line 86
    :cond_4
    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    .line 87
    .line 88
    .line 89
    move-result v2

    .line 90
    if-eqz v2, :cond_3

    .line 91
    .line 92
    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 97
    .line 98
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 99
    .line 100
    .line 101
    move-result-wide v2

    .line 102
    cmp-long v4, v2, v0

    .line 103
    .line 104
    if-nez v4, :cond_4

    .line 105
    .line 106
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 115
    .line 116
    .line 117
    :cond_6
    return-object p2
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 16

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    const-string v10, "MultiChoiceCursorAdapter"

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2
    instance-of v1, v0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;

    if-nez v1, :cond_0

    return-void

    .line 3
    :cond_0
    move-object v11, v0

    check-cast v11, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;

    :try_start_0
    const-string v0, "_id"

    .line 4
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 5
    new-instance v0, Lcom/intsig/camscanner/datastruct/DocItem;

    invoke-direct {v0, v9}, Lcom/intsig/camscanner/datastruct/DocItem;-><init>(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "team_token"

    .line 6
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 7
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v13, 0x8

    const/4 v14, 0x0

    if-ne v2, v13, :cond_1

    .line 8
    invoke-virtual {v7, v14}, Landroid/view/View;->setVisibility(I)V

    .line 9
    :cond_1
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OO0o〇〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    const-string v5, "title"

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v9, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇8O0〇8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇〇808〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v15

    invoke-direct {v6, v2, v15, v3, v4}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O8〇o(Landroid/widget/ImageView;Landroid/widget/ImageView;J)V

    .line 11
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oooo8o0〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/view/View;->setVisibility(I)V

    const-string v2, "pages"

    .line 12
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/16 v15, 0x3e7

    if-le v2, v15, :cond_2

    .line 13
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oooo8o0〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    const-string v15, "999+"

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 14
    :cond_2
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oooo8o0〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v15

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    :goto_0
    iget-object v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O8o08O8O:Ljava/util/HashMap;

    const/4 v13, 0x1

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_3
    const-string v2, "password"

    .line 16
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 17
    iget-object v14, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O8o08O8O:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 18
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v13, :cond_4

    goto :goto_1

    .line 19
    :cond_4
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const v13, 0x7f080fb5

    const-string v15, "ACCESS_BY_PASSWORD"

    if-eqz v2, :cond_5

    .line 20
    iget-object v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O8o08O8O:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v2, v14, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 22
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    const v13, -0xa0a0b

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundColor(I)V

    .line 23
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo88o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    const-string v13, "ACCESS_DIRECTLY"

    .line 24
    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 25
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo88o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, 0x7f0812d3

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 26
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 27
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    .line 28
    :cond_6
    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 29
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, 0x7f080fb5

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 30
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, -0xa0a0b

    invoke-virtual {v13, v14}, Landroid/view/View;->setBackgroundColor(I)V

    .line 31
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo88o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_7
    :goto_1
    const/4 v2, 0x0

    .line 32
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 33
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setBackgroundColor(I)V

    .line 34
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo88o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_8
    :goto_2
    const/4 v2, 0x0

    .line 35
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 36
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v14}, Landroid/view/View;->setBackgroundColor(I)V

    .line 37
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo88o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 38
    :cond_9
    :goto_3
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 39
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 40
    :cond_a
    iget-boolean v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    const/4 v15, 0x2

    if-eqz v2, :cond_c

    .line 41
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇0〇O0088o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {v6, v3, v4, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo〇(JLandroid/widget/TextView;)V

    .line 42
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 43
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 44
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    const/16 v13, 0x8

    invoke-virtual {v1, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_b
    const/16 v13, 0x8

    .line 45
    :goto_4
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 46
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v0

    .line 47
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 48
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_c
    const/16 v13, 0x8

    .line 49
    iget v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO:I

    if-nez v2, :cond_e

    .line 50
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 51
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 52
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 53
    :cond_d
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 54
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 55
    :cond_e
    iget-object v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08O〇00〇o:Ljava/util/HashSet;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v2, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 56
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 57
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 58
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 59
    :cond_f
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 60
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 61
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doc id = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, " isDocImageJpgComplete false"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 62
    :cond_11
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 63
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 65
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    :cond_12
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 67
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_13
    const/16 v2, 0x8

    .line 68
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 69
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v13

    if-eqz v13, :cond_14

    .line 70
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/view/View;->setVisibility(I)V

    .line 71
    :cond_14
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v14}, Landroid/view/View;->setFocusable(Z)V

    .line 72
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇8O0〇8(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v0

    .line 73
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 74
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 75
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇080(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v14}, Landroid/view/View;->setVisibility(I)V

    .line 76
    :cond_15
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 77
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->Oo08(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    :goto_5
    const-string v0, "sync_ui_state"

    .line 78
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 79
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    const-string v13, "sync_state"

    .line 81
    invoke-interface {v9, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v9, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "folder_type"

    .line 82
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v9, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 83
    invoke-static {v14}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->OO0o〇〇(I)Z

    move-result v14

    if-eqz v14, :cond_18

    if-eqz v2, :cond_17

    .line 84
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 85
    :cond_17
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_7

    :cond_18
    const/4 v14, 0x1

    if-eq v0, v14, :cond_1f

    if-ne v0, v15, :cond_19

    goto :goto_6

    :cond_19
    if-eqz v2, :cond_1a

    .line 86
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    :cond_1a
    if-nez v13, :cond_1b

    if-nez v0, :cond_1b

    .line 87
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f081204

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :cond_1b
    const/4 v2, 0x4

    if-ne v0, v2, :cond_1c

    .line 88
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f081203

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_7

    :cond_1c
    const/4 v2, 0x1

    if-ne v13, v2, :cond_1e

    .line 89
    iget-object v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1e

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1e

    .line 90
    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 91
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_7

    .line 92
    :cond_1d
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f080dda

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_7

    .line 93
    :cond_1e
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_7

    .line 94
    :cond_1f
    :goto_6
    invoke-static/range {p2 .. p2}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooo〇8oO()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 95
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OOO〇O0()V

    .line 96
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f081205

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    if-nez v2, :cond_22

    .line 97
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->Oo80:Landroid/view/animation/RotateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_7

    :cond_20
    if-eqz v2, :cond_21

    .line 98
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 99
    :cond_21
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 100
    :cond_22
    :goto_7
    invoke-static/range {p2 .. p2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0OO8(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_23

    const-string v0, "modified"

    .line 101
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_8

    :cond_23
    const-string v1, "created"

    const/4 v2, 0x3

    if-gt v0, v2, :cond_24

    .line 102
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_8

    .line 103
    :cond_24
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 104
    :goto_8
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇oo〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-direct {v6, v0, v1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o800o8O(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-direct {v6, v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇oOO8O8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)V

    .line 106
    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

    const-string v13, ""

    if-eqz v0, :cond_31

    invoke-interface {v0}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇o00〇〇Oo()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_31

    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 107
    invoke-interface {v0}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_31

    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

    invoke-interface {v0}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_31

    .line 108
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_25

    const/4 v1, 0x0

    .line 109
    aget-object v0, v0, v1

    if-eqz v0, :cond_25

    .line 110
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 111
    :cond_25
    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/QueryInterface;

    invoke-interface {v0}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    move-result-object v14

    .line 112
    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object v5, v14

    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo88o8O(Landroid/content/Context;Ljava/lang/String;J[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    array-length v1, v14

    const/4 v2, 0x1

    if-le v1, v2, :cond_28

    .line 114
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_26

    goto :goto_9

    .line 115
    :cond_26
    invoke-direct {v6, v0, v14}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇oo〇(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_27

    goto :goto_9

    .line 117
    :cond_27
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v14}, Lcom/intsig/camscanner/util/StringUtil;->〇O8o08O([Ljava/lang/String;)[Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-static {v0, v2, v8}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-direct {v6, v1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇0o(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;)V

    goto/16 :goto_a

    .line 118
    :cond_28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2a

    :cond_29
    :goto_9
    const/4 v0, 0x1

    goto :goto_b

    :cond_2a
    const-string v1, "<p>"

    .line 119
    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "</p>"

    invoke-virtual {v0, v1, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 120
    aget-object v2, v14, v1

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 122
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v3, "..."

    if-lt v2, v15, :cond_2b

    .line 123
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x1

    sub-int/2addr v2, v5

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    :cond_2b
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v2, v1

    .line 125
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v15

    if-gt v2, v1, :cond_2c

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x1

    add-int/2addr v2, v4

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_2c
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v14}, Lcom/intsig/camscanner/util/StringUtil;->〇O8o08O([Ljava/lang/String;)[Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-static {v0, v2, v8}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-direct {v6, v1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇〇0o(Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;)V

    :goto_a
    const/4 v0, 0x0

    :goto_b
    if-eqz v0, :cond_2d

    .line 128
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_2d
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 131
    iget-object v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOo0:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 132
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    const v1, 0x7f130192

    if-nez v0, :cond_2f

    .line 133
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO〇00〇8oO:I

    if-nez v0, :cond_2e

    .line 134
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OoO8(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v3, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOo0:Ljava/lang/String;

    aput-object v3, v4, v2

    const v3, 0x7f13012a

    invoke-virtual {v8, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 137
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o800o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 138
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O888o0o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 139
    :cond_2e
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 141
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o800o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOo0:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O888o0o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO〇00〇8oO:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 143
    :cond_2f
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget v2, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO〇00〇8oO:I

    if-ne v0, v2, :cond_30

    .line 144
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 145
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 146
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->o800o8O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O888o0o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    iget v3, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO〇00〇8oO:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_c
    const/4 v0, 0x1

    goto :goto_d

    :cond_30
    const/4 v0, 0x0

    :goto_d
    if-nez v0, :cond_35

    .line 148
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_f

    .line 150
    :cond_31
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_32

    .line 151
    aget-object v0, v0, v1

    if-nez v0, :cond_33

    .line 152
    :cond_32
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080376

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 154
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_33
    const-string v0, "dd"

    .line 155
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 157
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    .line 159
    :cond_34
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O00(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v1

    invoke-direct {v6, v1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08O8o〇0(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 160
    :goto_e
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 161
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 163
    :cond_35
    :goto_f
    iget-boolean v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    if-nez v0, :cond_38

    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    if-nez v0, :cond_36

    sget-boolean v1, Lcom/intsig/camscanner/app/AppConfig;->〇o00〇〇Oo:Z

    if-eqz v1, :cond_38

    :cond_36
    if-ne v0, v15, :cond_37

    goto :goto_10

    :cond_37
    const/16 v2, 0x8

    goto/16 :goto_14

    :cond_38
    :goto_10
    const v0, 0x7f0a1a13

    .line 164
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->oo〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/view/View;)V

    const v0, 0x7f0a0f87

    .line 165
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-static {v11, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇o(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;Landroid/widget/RelativeLayout;)V

    .line 166
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇80〇808〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3c

    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O8o08O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-nez v0, :cond_39

    goto :goto_11

    .line 167
    :cond_39
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3b

    .line 168
    iget-boolean v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O0O:Z

    if-eqz v0, :cond_3a

    .line 169
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇80〇808〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 170
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O8o08O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    goto :goto_14

    :cond_3a
    const/16 v2, 0x8

    .line 171
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇80〇808〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 172
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O8o08O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    goto :goto_14

    :cond_3b
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 173
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O8o08O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    .line 174
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇80〇808〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_14

    :cond_3c
    :goto_11
    const/16 v2, 0x8

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "vh.mDivider == null:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇80〇808〇O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_3d

    const/4 v1, 0x1

    goto :goto_12

    :cond_3d
    const/4 v1, 0x0

    :goto_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "vh.mLlDocListItem == null:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇O8o08O(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v1

    if-nez v1, :cond_3e

    const/4 v1, 0x1

    goto :goto_13

    :cond_3e
    const/4 v1, 0x0

    :goto_13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :goto_14
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 177
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3f

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 178
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 180
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 181
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    if-ne v0, v1, :cond_42

    .line 182
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_16

    .line 183
    :cond_3f
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    if-eqz v1, :cond_40

    const/16 v13, 0x8

    goto :goto_15

    :cond_40
    const/4 v13, 0x0

    :goto_15
    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 184
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 185
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 186
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_42

    .line 187
    iget-boolean v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    if-eqz v0, :cond_41

    .line 188
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080fac

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_16

    .line 189
    :cond_41
    invoke-static {v11}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;->〇8o8o〇(Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 190
    :cond_42
    :goto_16
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_44

    .line 191
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    if-gtz v0, :cond_43

    const v0, 0x7f0a0fd5

    .line 192
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    .line 194
    :cond_43
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    if-lez v0, :cond_46

    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8O0O808〇()I

    move-result v0

    if-nez v0, :cond_46

    .line 195
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8oOOo:I

    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8Oo08O(I)V

    goto :goto_17

    :cond_44
    if-nez v0, :cond_46

    .line 196
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    if-nez v0, :cond_45

    const v0, 0x7f0a0f85

    .line 197
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    .line 199
    :cond_45
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    if-lez v0, :cond_46

    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO80OOO〇()I

    move-result v0

    if-nez v0, :cond_46

    .line 200
    iget v0, v6, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇〇O8:I

    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o08〇OO0(I)V

    :cond_46
    :goto_17
    return-void

    :catch_0
    const-string v0, "bindView error"

    .line 201
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public getCount()I
    .locals 2

    .line 1
    :try_start_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    goto :goto_0

    .line 6
    :catch_0
    move-exception v0

    .line 7
    const-string v1, "MultiChoiceCursorAdapter"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    :goto_0
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    new-instance p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 14
    .line 15
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/datastruct/DocItem;-><init>(Landroid/database/Cursor;)V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getItemId(I)J
    .locals 2

    .line 1
    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->getItemId(I)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5
    goto :goto_0

    .line 6
    :catch_0
    const-string p1, "MultiChoiceCursorAdapter"

    .line 7
    .line 8
    const-string v0, "getItemId error"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-wide/16 v0, 0x0

    .line 14
    .line 15
    :goto_0
    return-wide v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .line 1
    const-string v0, "getView error"

    .line 2
    .line 3
    const-string v1, "MultiChoiceCursorAdapter"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    goto :goto_0

    .line 11
    :catch_0
    move-exception p1

    .line 12
    invoke-static {v1, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :catch_1
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇o0O:Landroid/view/View;

    .line 17
    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    iget p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O88O:I

    .line 21
    .line 22
    const/4 p2, -0x1

    .line 23
    if-le p1, p2, :cond_0

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oOO〇〇:Landroid/content/Context;

    .line 26
    .line 27
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iget p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O88O:I

    .line 32
    .line 33
    invoke-virtual {p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇o0O:Landroid/view/View;

    .line 38
    .line 39
    const/16 p2, 0x8

    .line 40
    .line 41
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇o0O:Landroid/view/View;

    .line 45
    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇〇0〇(Landroid/view/View;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇o0O:Landroid/view/View;

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-object v2
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 1
    iget-boolean p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    const p2, 0x7f0d03d8

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    iget p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 11
    .line 12
    if-nez p2, :cond_1

    .line 13
    .line 14
    const p2, 0x7f0d0267

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v1, 0x1

    .line 19
    if-ne p2, v1, :cond_2

    .line 20
    .line 21
    const p2, 0x7f0d0265

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_2
    const/4 v1, 0x2

    .line 26
    if-ne p2, v1, :cond_3

    .line 27
    .line 28
    const p2, 0x7f0d0269

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_3
    const/4 p2, 0x0

    .line 33
    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput p2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O88O:I

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇〇0〇(Landroid/view/View;)V

    .line 44
    .line 45
    .line 46
    return-object p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .line 1
    :try_start_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "MultiChoiceCursorAdapter"

    .line 7
    .line 8
    const-string v2, "notifyDataSetChanged"

    .line 9
    .line 10
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o0ooO()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "MultiChoiceCursorAdapter"

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "cursor == null"

    .line 10
    .line 11
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-eqz v2, :cond_1

    .line 20
    .line 21
    const-string v0, "cursor isClosed"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    const/4 v3, 0x1

    .line 32
    if-ge v2, v3, :cond_2

    .line 33
    .line 34
    const-string v0, "cursor is empty"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-eqz v2, :cond_3

    .line 49
    .line 50
    new-instance v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 51
    .line 52
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/datastruct/DocItem;-><init>(Landroid/database/Cursor;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 56
    .line 57
    .line 58
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    if-eqz v2, :cond_3

    .line 63
    .line 64
    new-instance v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 65
    .line 66
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/datastruct/DocItem;-><init>(Landroid/database/Cursor;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇O〇(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_3
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public o8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->oo8ooo8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public oO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇OOo8〇0:I

    .line 2
    .line 3
    sput p1, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O〇o88o08〇:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08O〇00〇o:Ljava/util/HashSet;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇08O〇00〇o:Ljava/util/HashSet;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0OOo〇0(I)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OO:I

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    if-ne p1, v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OoO8()V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇8(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 24
    .line 25
    .line 26
    move-result-wide v4

    .line 27
    cmp-long v6, v2, v4

    .line 28
    .line 29
    if-nez v6, :cond_0

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const/4 v0, 0x0

    .line 39
    :goto_0
    if-nez v0, :cond_2

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    :cond_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public o〇O8〇〇o()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇00()J
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-wide/16 v1, 0x0

    .line 8
    .line 9
    if-lez v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    if-eqz v3, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    check-cast v3, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 28
    .line 29
    if-eqz v3, :cond_0

    .line 30
    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 32
    .line 33
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 34
    .line 35
    .line 36
    move-result-wide v5

    .line 37
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    if-eqz v4, :cond_0

    .line 46
    .line 47
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇8〇oO〇〇8o:Ljava/util/Map;

    .line 48
    .line 49
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 50
    .line 51
    .line 52
    move-result-wide v5

    .line 53
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v3

    .line 61
    check-cast v3, Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;

    .line 62
    .line 63
    if-eqz v3, :cond_0

    .line 64
    .line 65
    invoke-virtual {v3}, Lcom/intsig/camscanner/settings/DocsSizeManager$DocSize;->〇080()J

    .line 66
    .line 67
    .line 68
    move-result-wide v3

    .line 69
    add-long/2addr v1, v3

    .line 70
    goto :goto_0

    .line 71
    :cond_1
    return-wide v1
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇080(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->O8o08O8O:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇0〇O0088o()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇8(Landroid/database/Cursor;)V
    .locals 8

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 4
    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_3

    .line 12
    .line 13
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 16
    .line 17
    .line 18
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 25
    .line 26
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/datastruct/DocItem;-><init>(Landroid/database/Cursor;)V

    .line 27
    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-eqz v3, :cond_0

    .line 40
    .line 41
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    check-cast v3, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 48
    .line 49
    .line 50
    move-result-wide v3

    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 52
    .line 53
    .line 54
    move-result-wide v5

    .line 55
    cmp-long v7, v3, v5

    .line 56
    .line 57
    if-nez v7, :cond_1

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 64
    .line 65
    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :catch_0
    move-exception p1

    .line 72
    const-string v0, "MultiChoiceCursorAdapter"

    .line 73
    .line 74
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    .line 76
    .line 77
    :cond_3
    :goto_1
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇O〇(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 24
    .line 25
    .line 26
    move-result-wide v3

    .line 27
    cmp-long v5, v1, v3

    .line 28
    .line 29
    if-nez v5, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 v0, 0x0

    .line 34
    :goto_0
    if-nez v0, :cond_2

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 37
    .line 38
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    :cond_2
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇o(J)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    cmp-long v3, v1, p1

    .line 24
    .line 25
    if-nez v3, :cond_0

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇8O0〇8(Lcom/intsig/camscanner/datastruct/DocItem;)Z
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-string p1, "MultiChoiceCursorAdapter"

    .line 4
    .line 5
    const-string v0, "checkSelect docItem == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇00O:Ljava/util/HashSet;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 30
    .line 31
    .line 32
    move-result-wide v1

    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 34
    .line 35
    .line 36
    move-result-wide v3

    .line 37
    cmp-long v5, v1, v3

    .line 38
    .line 39
    if-nez v5, :cond_1

    .line 40
    .line 41
    const/4 p1, 0x1

    .line 42
    return p1

    .line 43
    :cond_2
    :goto_0
    const/4 p1, 0x0

    .line 44
    return p1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇〇〇0〇〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

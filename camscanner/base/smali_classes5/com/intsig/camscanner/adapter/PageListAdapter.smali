.class public Lcom/intsig/camscanner/adapter/PageListAdapter;
.super Landroid/widget/CursorAdapter;
.source "PageListAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final O0O:I

.field private O88O:Z

.field private O8o08O8O:J

.field private final OO:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

.field private OO〇00〇8oO:Z

.field private Oo0〇Ooo:Ljava/text/SimpleDateFormat;

.field private Oo80:I

.field private Ooo08:Landroid/view/View$OnClickListener;

.field private O〇08oOOO0:Landroid/view/View;

.field private O〇o88o08〇:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final o0:I

.field private o8o:Z

.field private o8oOOo:I

.field private final o8〇OO:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

.field private final o8〇OO0〇0o:I

.field private oOO〇〇:Z

.field private oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

.field private oOo〇8o008:Landroid/app/Activity;

.field private oo8ooo8O:[Ljava/lang/String;

.field ooO:I

.field private ooo0〇〇O:I

.field private o〇00O:I

.field private o〇oO:[Ljava/util/regex/Pattern;

.field private 〇00O0:Z

.field private 〇080OO8〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

.field private 〇08O〇00〇o:I

.field private 〇08〇o0O:[Ljava/lang/String;

.field private 〇0O:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇8〇oO〇〇8o:I

.field private 〇OO8ooO8〇:Lcom/intsig/camscanner/loadimage/PageImage;

.field private final 〇OOo8〇0:I

.field private 〇OO〇00〇0O:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

.field private 〇O〇〇O8:I

.field private 〇o0O:Z

.field private 〇〇08O:I

.field private 〇〇o〇:Z

.field private 〇〇〇0o〇〇0:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;Landroid/database/Cursor;Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p3, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 3
    .line 4
    .line 5
    new-instance p3, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {p3}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 11
    .line 12
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8〇OO0〇0o:I

    .line 13
    .line 14
    const/4 p3, 0x1

    .line 15
    iput p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇8〇oO〇〇8o:I

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooo0〇〇O:I

    .line 18
    .line 19
    const/4 v1, 0x2

    .line 20
    iput v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O0O:I

    .line 21
    .line 22
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8oOOo:I

    .line 23
    .line 24
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O〇〇O8:I

    .line 25
    .line 26
    iput-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇o0O:Z

    .line 27
    .line 28
    iput-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O88O:Z

    .line 29
    .line 30
    iput-boolean p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇o〇:Z

    .line 31
    .line 32
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 33
    .line 34
    iput-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00O0:Z

    .line 35
    .line 36
    new-instance v1, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallbackImpl;

    .line 37
    .line 38
    invoke-direct {v1}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallbackImpl;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8〇OO:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 42
    .line 43
    new-instance v2, Lcom/intsig/camscanner/adapter/PageListAdapter$1;

    .line 44
    .line 45
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/adapter/PageListAdapter$1;-><init>(Lcom/intsig/camscanner/adapter/PageListAdapter;)V

    .line 46
    .line 47
    .line 48
    iput-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Ooo08:Landroid/view/View$OnClickListener;

    .line 49
    .line 50
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooO:I

    .line 51
    .line 52
    new-instance v2, Ljava/text/SimpleDateFormat;

    .line 53
    .line 54
    invoke-direct {v2}, Ljava/text/SimpleDateFormat;-><init>()V

    .line 55
    .line 56
    .line 57
    iput-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo0〇Ooo:Ljava/text/SimpleDateFormat;

    .line 58
    .line 59
    new-instance v2, Ljava/util/Hashtable;

    .line 60
    .line 61
    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 62
    .line 63
    .line 64
    iput-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 65
    .line 66
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 67
    .line 68
    iput-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 69
    .line 70
    invoke-virtual {p2}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->〇O8oOo0()[Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    iput-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 75
    .line 76
    invoke-static {p2}, Lcom/intsig/camscanner/util/StringUtil;->〇O8o08O([Ljava/lang/String;)[Ljava/util/regex/Pattern;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    iput-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇oO:[Ljava/util/regex/Pattern;

    .line 81
    .line 82
    sget-boolean p2, Lcom/intsig/camscanner/app/AppConfig;->〇o00〇〇Oo:Z

    .line 83
    .line 84
    if-nez p2, :cond_0

    .line 85
    .line 86
    sget-boolean p2, Lcom/intsig/camscanner/app/AppConfig;->O8:Z

    .line 87
    .line 88
    if-nez p2, :cond_0

    .line 89
    .line 90
    const/4 p2, 0x1

    .line 91
    goto :goto_0

    .line 92
    :cond_0
    const/4 p2, 0x0

    .line 93
    :goto_0
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO〇00〇8oO:Z

    .line 94
    .line 95
    iput-boolean p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOO〇〇:Z

    .line 96
    .line 97
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 98
    .line 99
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 100
    .line 101
    .line 102
    move-result-object p2

    .line 103
    iget-object p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 104
    .line 105
    const v2, 0x7f131d77

    .line 106
    .line 107
    .line 108
    invoke-virtual {p4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p4

    .line 112
    invoke-interface {p2, p4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 113
    .line 114
    .line 115
    move-result p2

    .line 116
    iput-boolean p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8o:Z

    .line 117
    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    const p4, 0x7f070223

    .line 123
    .line 124
    .line 125
    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 126
    .line 127
    .line 128
    move-result p2

    .line 129
    iput p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o0:I

    .line 130
    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 132
    .line 133
    .line 134
    move-result-object p4

    .line 135
    const v0, 0x7f070222

    .line 136
    .line 137
    .line 138
    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 139
    .line 140
    .line 141
    move-result p4

    .line 142
    iput p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OOo8〇0:I

    .line 143
    .line 144
    iput p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 145
    .line 146
    iput p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 147
    .line 148
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 149
    .line 150
    .line 151
    move-result-object p1

    .line 152
    const p2, 0x7f070183

    .line 153
    .line 154
    .line 155
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 156
    .line 157
    .line 158
    move-result p1

    .line 159
    iput p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 160
    .line 161
    iput-boolean p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇o〇:Z

    .line 162
    .line 163
    new-instance p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 164
    .line 165
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 166
    .line 167
    invoke-direct {p1, p2, v1}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;)V

    .line 168
    .line 169
    .line 170
    iput-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method public static synthetic O8(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O8ooOoo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v0, " HH:mm"

    .line 22
    .line 23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo0〇Ooo:Ljava/text/SimpleDateFormat;

    .line 40
    .line 41
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 45
    .line 46
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private OO0o〇〇(J)Lcom/bumptech/glide/request/RequestOptions;
    .locals 4

    .line 1
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 13
    .line 14
    const v1, 0x7f0802b5

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇8o8o〇(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇o〇()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 34
    .line 35
    new-instance v1, Lcom/intsig/camscanner/util/GlideRoundTransform;

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 38
    .line 39
    const/4 v3, 0x2

    .line 40
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/util/GlideRoundTransform;-><init>(I)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80〇808〇O()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 58
    .line 59
    new-instance v1, Lcom/intsig/utils/image/GlideImageExtKey;

    .line 60
    .line 61
    invoke-direct {v1, p1, p2}, Lcom/intsig/utils/image/GlideImageExtKey;-><init>(J)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    check-cast p1, Lcom/bumptech/glide/request/RequestOptions;

    .line 69
    .line 70
    return-object p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private OOO〇O0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇o0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00O0:Z

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-lez v0, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/business/operation/OperateEngine;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/adapter/PageListAdapter;)Landroid/app/Activity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private OoO8(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;-><init>(LO0/〇8o8o〇;)V

    .line 13
    .line 14
    .line 15
    const v1, 0x7f0a0fcd

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 23
    .line 24
    const v1, 0x7f0a11ab

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Landroid/widget/TextView;

    .line 32
    .line 33
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 34
    .line 35
    const v1, 0x7f0a1966

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    check-cast v1, Landroid/widget/TextView;

    .line 43
    .line 44
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o〇:Landroid/widget/TextView;

    .line 45
    .line 46
    const v1, 0x7f0a0e43

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    check-cast v1, Landroid/widget/ImageView;

    .line 54
    .line 55
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇888:Landroid/widget/ImageView;

    .line 56
    .line 57
    const v1, 0x7f0a11aa

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Landroid/widget/TextView;

    .line 65
    .line 66
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 67
    .line 68
    const v1, 0x7f0a1129

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 76
    .line 77
    const v1, 0x7f0a1128

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    check-cast v1, Landroid/widget/ImageView;

    .line 85
    .line 86
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->O8:Landroid/widget/ImageView;

    .line 87
    .line 88
    const v1, 0x7f0a1127

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 96
    .line 97
    const v1, 0x7f0a1172

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    check-cast v1, Landroid/widget/ImageView;

    .line 105
    .line 106
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 107
    .line 108
    const v1, 0x7f0a0733

    .line 109
    .line 110
    .line 111
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    check-cast v1, Landroid/widget/ImageView;

    .line 116
    .line 117
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 118
    .line 119
    const v1, 0x7f0a0c7b

    .line 120
    .line 121
    .line 122
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇8o8o〇:Landroid/view/View;

    .line 127
    .line 128
    const v1, 0x7f0a1964

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    check-cast v1, Landroid/widget/TextView;

    .line 136
    .line 137
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O8o08O:Landroid/widget/TextView;

    .line 138
    .line 139
    const v1, 0x7f0a1965

    .line 140
    .line 141
    .line 142
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 143
    .line 144
    .line 145
    move-result-object v1

    .line 146
    check-cast v1, Landroid/widget/TextView;

    .line 147
    .line 148
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇:Landroid/widget/TextView;

    .line 149
    .line 150
    const v1, 0x7f0a0336

    .line 151
    .line 152
    .line 153
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    check-cast v1, Landroid/widget/CheckBox;

    .line 158
    .line 159
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oooo8o0〇:Landroid/widget/CheckBox;

    .line 160
    .line 161
    const v1, 0x7f0a1995

    .line 162
    .line 163
    .line 164
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 165
    .line 166
    .line 167
    move-result-object v1

    .line 168
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇808〇:Landroid/view/View;

    .line 169
    .line 170
    const v1, 0x7f0a0f0e

    .line 171
    .line 172
    .line 173
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    check-cast v1, Landroid/widget/ImageView;

    .line 178
    .line 179
    iput-object v1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 180
    .line 181
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 182
    .line 183
    .line 184
    iget-object p1, v0, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 185
    .line 186
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Ooo08:Landroid/view/View$OnClickListener;

    .line 187
    .line 188
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    .line 190
    .line 191
    :cond_0
    return-void
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method private Oooo8o0〇()Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OO〇00〇0O:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OO〇00〇0O:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 11
    .line 12
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooO:I

    .line 13
    .line 14
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇080:I

    .line 15
    .line 16
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇08O:I

    .line 17
    .line 18
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o00〇〇Oo:I

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 21
    .line 22
    iput-object v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o〇:Landroid/app/Activity;

    .line 23
    .line 24
    iget-boolean v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O88O:Z

    .line 25
    .line 26
    iput-boolean v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->O8:Z

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇080OO8〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 29
    .line 30
    iput-object v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->oO80:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 31
    .line 32
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 33
    .line 34
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->Oo08:I

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 37
    .line 38
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->o〇0:I

    .line 39
    .line 40
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 41
    .line 42
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇〇888:I

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooO:I

    .line 46
    .line 47
    iput v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇080:I

    .line 48
    .line 49
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OO〇00〇0O:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 50
    .line 51
    iget-wide v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O8o08O8O:J

    .line 52
    .line 53
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-static {v1}, Lcom/intsig/camscanner/paper/CamExamGuideManager;->〇O8o08O(Ljava/lang/Long;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    iput-boolean v1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇80〇808〇O:Z

    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OO〇00〇0O:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 64
    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private static synthetic O〇8O8〇008(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 0

    .line 1
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    check-cast p0, Ljava/lang/Integer;

    .line 6
    .line 7
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Ljava/lang/Integer;

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    sub-int/2addr p0, p1

    .line 22
    return p0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO80(Landroid/widget/ImageView;ILcom/intsig/camscanner/loadimage/BitmapPara;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ImageView;->getScaleType()Landroid/widget/ImageView$ScaleType;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 9
    .line 10
    if-eq v0, v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 13
    .line 14
    .line 15
    :cond_1
    invoke-virtual {p3, p2}, Lcom/intsig/camscanner/loadimage/BitmapPara;->〇080(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    iget-object p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 20
    .line 21
    invoke-static {p3}, Lcom/bumptech/glide/Glide;->〇0〇O0088o(Landroid/app/Activity;)Lcom/bumptech/glide/RequestManager;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-virtual {p3, p2}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 26
    .line 27
    .line 28
    move-result-object p3

    .line 29
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇O00(Ljava/lang/String;)J

    .line 30
    .line 31
    .line 32
    move-result-wide v0

    .line 33
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO0o〇〇(J)Lcom/bumptech/glide/request/RequestOptions;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {p3, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    const p3, 0x3f19999a    # 0.6f

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2, p3}, Lcom/bumptech/glide/RequestBuilder;->O00(F)Lcom/bumptech/glide/RequestBuilder;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    invoke-virtual {p2, p1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private oo88o8O(Landroid/database/Cursor;)Z
    .locals 2

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x9

    .line 8
    .line 9
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-nez p1, :cond_0

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    return p1

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    return p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/adapter/PageListAdapter;)Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private synthetic o〇O8〇〇o(JLjava/lang/String;Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    invoke-virtual {p4}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result p4

    .line 9
    if-nez p4, :cond_0

    .line 10
    .line 11
    iget-object p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 12
    .line 13
    if-eqz p4, :cond_0

    .line 14
    .line 15
    invoke-virtual {p4}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->oO〇oo()Z

    .line 16
    .line 17
    .line 18
    move-result p4

    .line 19
    if-nez p4, :cond_0

    .line 20
    .line 21
    new-instance p4, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 34
    .line 35
    new-instance v1, LO0/OO0o〇〇〇〇0;

    .line 36
    .line 37
    invoke-direct {v1, p0, p1, p2, p3}, LO0/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/adapter/PageListAdapter;JLjava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const/4 p1, 0x0

    .line 41
    invoke-static {v0, p4, p1, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private static synthetic 〇00(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 0

    .line 1
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    check-cast p0, Ljava/lang/Integer;

    .line 6
    .line 7
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    .line 8
    .line 9
    .line 10
    move-result p0

    .line 11
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Ljava/lang/Integer;

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    sub-int/2addr p0, p1

    .line 22
    return p0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/adapter/PageListAdapter;JLjava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇O8〇〇o(JLjava/lang/String;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private 〇0〇O0088o(I)[Ljava/lang/String;
    .locals 5

    .line 1
    if-lez p1, :cond_1

    .line 2
    .line 3
    new-array v0, p1, [Ljava/lang/String;

    .line 4
    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇o〇:Z

    .line 6
    .line 7
    const-string v2, ""

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    :goto_0
    if-ge v3, p1, :cond_2

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    add-int/lit8 v4, v3, 0x1

    .line 20
    .line 21
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    aput-object v1, v0, v3

    .line 32
    .line 33
    move v3, v4

    .line 34
    goto :goto_0

    .line 35
    :cond_0
    :goto_1
    if-ge v3, p1, :cond_2

    .line 36
    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    sub-int v4, p1, v3

    .line 43
    .line 44
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    aput-object v1, v0, v3

    .line 55
    .line 56
    add-int/lit8 v3, v3, 0x1

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v1, "initePageNumIndex count="

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    const-string v0, "PageListAdapter"

    .line 77
    .line 78
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    const/4 v0, 0x0

    .line 82
    :cond_2
    return-object v0
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇8o8o〇(J)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->O8ooOoo〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo0〇Ooo:Ljava/text/SimpleDateFormat;

    .line 5
    .line 6
    new-instance v1, Ljava/util/Date;

    .line 7
    .line 8
    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇O8o08O(I)I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    sub-int/2addr v0, v1

    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    iget-boolean p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇o0O:Z

    .line 10
    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    iget-boolean p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00O0:Z

    .line 14
    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    iget p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O〇〇O8:I

    .line 18
    .line 19
    if-lez p1, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v1, 0x0

    .line 23
    :goto_0
    return v1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/adapter/PageListAdapter;JLjava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇oo〇(JLjava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private synthetic 〇oo〇(JLjava/lang/String;I)V
    .locals 7

    .line 1
    new-instance p4, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 2
    .line 3
    invoke-direct {p4, p1, p2, p3}, Lcom/intsig/camscanner/loadimage/PageImage;-><init>(JLjava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OO8ooO8〇:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 7
    .line 8
    new-instance p1, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 14
    .line 15
    invoke-static {p2, p3}, Lcom/intsig/camscanner/mode_ocr/OCRClient;->o〇8(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/OCRData;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    new-instance v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 25
    .line 26
    invoke-direct {v3}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 27
    .line 28
    .line 29
    iget-wide p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O8o08O8O:J

    .line 30
    .line 31
    iput-wide p2, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 32
    .line 33
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;->〇080:Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 36
    .line 37
    new-instance v2, Ljava/util/ArrayList;

    .line 38
    .line 39
    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 40
    .line 41
    .line 42
    sget-object v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_PAGE_LIST_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 43
    .line 44
    const/4 v5, -0x1

    .line 45
    const/4 v6, 0x0

    .line 46
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;->〇o00〇〇Oo(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mode_ocr/PageFromType;IZ)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 51
    .line 52
    invoke-virtual {p2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static synthetic 〇o〇(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇8O8〇008(Ljava/util/Map$Entry;Ljava/util/Map$Entry;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/adapter/PageListAdapter;)Ljava/util/HashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public O8〇o(Landroid/database/Cursor;)V
    .locals 7

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 4
    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_2

    .line 12
    .line 13
    :try_start_0
    new-instance v0, Ljava/util/Hashtable;

    .line 14
    .line 15
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 26
    .line 27
    .line 28
    move-result-wide v2

    .line 29
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 30
    .line 31
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    const/4 v5, 0x3

    .line 40
    if-eqz v4, :cond_0

    .line 41
    .line 42
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    invoke-virtual {v0, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    if-eqz v2, :cond_1

    .line 62
    .line 63
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 64
    .line 65
    .line 66
    move-result-wide v2

    .line 67
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 68
    .line 69
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 70
    .line 71
    .line 72
    move-result-object v6

    .line 73
    invoke-virtual {v4, v6}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    if-eqz v4, :cond_0

    .line 78
    .line 79
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-virtual {v0, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 96
    .line 97
    invoke-virtual {p1}, Ljava/util/Hashtable;->clear()V

    .line 98
    .line 99
    .line 100
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .line 102
    goto :goto_1

    .line 103
    :catch_0
    move-exception p1

    .line 104
    const-string v0, "PageListAdapter"

    .line 105
    .line 106
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    .line 108
    .line 109
    :cond_2
    :goto_1
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-lez v0, :cond_3

    .line 9
    .line 10
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 11
    .line 12
    .line 13
    move-result-wide v2

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    iget-object v4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 20
    .line 21
    invoke-virtual {v4}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    if-eqz v4, :cond_1

    .line 26
    .line 27
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    if-eqz v5, :cond_1

    .line 32
    .line 33
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    check-cast v5, Ljava/lang/Long;

    .line 38
    .line 39
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    .line 40
    .line 41
    .line 42
    move-result-wide v5

    .line 43
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 44
    .line 45
    .line 46
    move-result v7

    .line 47
    if-lez v7, :cond_0

    .line 48
    .line 49
    new-instance v7, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v8, ", "

    .line 55
    .line 56
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v8, ""

    .line 76
    .line 77
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v5

    .line 87
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    if-lez v4, :cond_2

    .line 96
    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    const-string v4, "( "

    .line 103
    .line 104
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    const-string v0, " )"

    .line 115
    .line 116
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    move-object v1, v0

    .line 124
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 125
    .line 126
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .line 128
    .line 129
    const-string v4, "getFilterPageIdString cost time="

    .line 130
    .line 131
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 135
    .line 136
    .line 137
    move-result-wide v4

    .line 138
    sub-long/2addr v4, v2

    .line 139
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    const-string v2, "PageListAdapter"

    .line 147
    .line 148
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    :cond_3
    return-object v1
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    const-string v2, "bindView error"

    .line 6
    .line 7
    const-string v3, "PageListAdapter"

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    :try_start_0
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    .line 11
    .line 12
    .line 13
    move-result-wide v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v7

    .line 18
    instance-of v8, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;

    .line 19
    .line 20
    if-nez v8, :cond_0

    .line 21
    .line 22
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    check-cast v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;

    .line 27
    .line 28
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getVisibility()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    const/16 v8, 0x8

    .line 33
    .line 34
    if-ne v2, v8, :cond_1

    .line 35
    .line 36
    move-object/from16 v2, p1

    .line 37
    .line 38
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    :cond_1
    const/4 v2, 0x2

    .line 42
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v9

    .line 46
    const/4 v10, 0x3

    .line 47
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getInt(I)I

    .line 48
    .line 49
    .line 50
    move-result v11

    .line 51
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 52
    .line 53
    const/4 v13, 0x1

    .line 54
    new-array v14, v13, [Ljava/lang/Object;

    .line 55
    .line 56
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 57
    .line 58
    .line 59
    move-result-object v11

    .line 60
    aput-object v11, v14, v4

    .line 61
    .line 62
    const-string v11, "%1$02d"

    .line 63
    .line 64
    invoke-static {v11, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v11

    .line 68
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    .line 70
    .line 71
    const/4 v11, 0x7

    .line 72
    invoke-interface {v1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v11

    .line 76
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 77
    .line 78
    if-eqz v12, :cond_3

    .line 79
    .line 80
    array-length v12, v12

    .line 81
    if-lez v12, :cond_3

    .line 82
    .line 83
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 84
    .line 85
    .line 86
    move-result v12

    .line 87
    if-nez v12, :cond_3

    .line 88
    .line 89
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {v11, v12}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result v12

    .line 95
    if-eqz v12, :cond_2

    .line 96
    .line 97
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇oO:[Ljava/util/regex/Pattern;

    .line 98
    .line 99
    if-eqz v12, :cond_2

    .line 100
    .line 101
    array-length v14, v12

    .line 102
    if-lez v14, :cond_2

    .line 103
    .line 104
    iget-object v14, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o〇:Landroid/widget/TextView;

    .line 105
    .line 106
    iget-object v15, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 107
    .line 108
    invoke-static {v11, v12, v15}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    .line 109
    .line 110
    .line 111
    move-result-object v11

    .line 112
    invoke-virtual {v14, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    .line 114
    .line 115
    const/4 v11, 0x1

    .line 116
    goto :goto_1

    .line 117
    :cond_2
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o〇:Landroid/widget/TextView;

    .line 118
    .line 119
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_3
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇o〇:Landroid/widget/TextView;

    .line 124
    .line 125
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    .line 127
    .line 128
    :goto_0
    const/4 v11, 0x0

    .line 129
    :goto_1
    iget-boolean v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8o:Z

    .line 130
    .line 131
    const/16 v14, 0xb

    .line 132
    .line 133
    const/4 v15, 0x4

    .line 134
    if-eqz v12, :cond_6

    .line 135
    .line 136
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    .line 137
    .line 138
    .line 139
    move-result v12

    .line 140
    if-nez v12, :cond_6

    .line 141
    .line 142
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v12

    .line 146
    invoke-static {v12}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 147
    .line 148
    .line 149
    move-result v16

    .line 150
    if-nez v16, :cond_4

    .line 151
    .line 152
    move-object v12, v9

    .line 153
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->OOO〇O0()I

    .line 154
    .line 155
    .line 156
    move-result v16

    .line 157
    if-nez v16, :cond_5

    .line 158
    .line 159
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O8o08O:Landroid/widget/TextView;

    .line 160
    .line 161
    new-instance v10, Ljava/lang/StringBuilder;

    .line 162
    .line 163
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    .line 165
    .line 166
    invoke-static {v12}, Lcom/intsig/camscanner/util/StringUtil;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object v12

    .line 170
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    const-string v12, ","

    .line 174
    .line 175
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 179
    .line 180
    .line 181
    move-result-object v12

    .line 182
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v10

    .line 189
    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    .line 191
    .line 192
    iget-object v10, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O8o08O:Landroid/widget/TextView;

    .line 193
    .line 194
    const/high16 v12, 0x41200000    # 10.0f

    .line 195
    .line 196
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setTextSize(F)V

    .line 197
    .line 198
    .line 199
    goto :goto_2

    .line 200
    :cond_5
    iget-object v10, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O8o08O:Landroid/widget/TextView;

    .line 201
    .line 202
    invoke-static {v12}, Lcom/intsig/camscanner/util/StringUtil;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v12

    .line 206
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    .line 208
    .line 209
    :goto_2
    iget-object v10, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇:Landroid/widget/TextView;

    .line 210
    .line 211
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    .line 212
    .line 213
    .line 214
    move-result-wide v12

    .line 215
    invoke-direct {v0, v12, v13}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇8o8o〇(J)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v12

    .line 219
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    .line 221
    .line 222
    iget-object v10, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇8o8o〇:Landroid/view/View;

    .line 223
    .line 224
    invoke-virtual {v10, v4}, Landroid/view/View;->setVisibility(I)V

    .line 225
    .line 226
    .line 227
    goto :goto_3

    .line 228
    :cond_6
    iget-object v10, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇8o8o〇:Landroid/view/View;

    .line 229
    .line 230
    invoke-virtual {v10, v8}, Landroid/view/View;->setVisibility(I)V

    .line 231
    .line 232
    .line 233
    :goto_3
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    .line 234
    .line 235
    .line 236
    move-result v10

    .line 237
    const/4 v12, 0x0

    .line 238
    if-nez v10, :cond_7

    .line 239
    .line 240
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->O8:Landroid/widget/ImageView;

    .line 241
    .line 242
    invoke-virtual {v13, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    .line 244
    .line 245
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->O8:Landroid/widget/ImageView;

    .line 246
    .line 247
    invoke-virtual {v13, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    .line 249
    .line 250
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 251
    .line 252
    invoke-virtual {v13, v8}, Landroid/view/View;->setVisibility(I)V

    .line 253
    .line 254
    .line 255
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 256
    .line 257
    invoke-virtual {v13, v8}, Landroid/view/View;->setVisibility(I)V

    .line 258
    .line 259
    .line 260
    goto :goto_4

    .line 261
    :cond_7
    if-eq v10, v2, :cond_8

    .line 262
    .line 263
    const/4 v13, 0x3

    .line 264
    if-eq v10, v13, :cond_8

    .line 265
    .line 266
    const/4 v13, 0x1

    .line 267
    if-ne v10, v13, :cond_9

    .line 268
    .line 269
    :cond_8
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->O8:Landroid/widget/ImageView;

    .line 270
    .line 271
    const v15, 0x7f08103e

    .line 272
    .line 273
    .line 274
    invoke-virtual {v13, v15}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 275
    .line 276
    .line 277
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->O8:Landroid/widget/ImageView;

    .line 278
    .line 279
    invoke-virtual {v13, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 280
    .line 281
    .line 282
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oo08:Landroid/view/View;

    .line 283
    .line 284
    invoke-virtual {v13, v4}, Landroid/view/View;->setVisibility(I)V

    .line 285
    .line 286
    .line 287
    iget-object v13, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->o〇0:Landroid/view/View;

    .line 288
    .line 289
    invoke-virtual {v13, v4}, Landroid/view/View;->setVisibility(I)V

    .line 290
    .line 291
    .line 292
    :cond_9
    :goto_4
    const/16 v13, 0x9

    .line 293
    .line 294
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getInt(I)I

    .line 295
    .line 296
    .line 297
    move-result v13

    .line 298
    const/16 v15, 0xd

    .line 299
    .line 300
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    .line 301
    .line 302
    .line 303
    move-result v15

    .line 304
    const/16 v14, 0xe

    .line 305
    .line 306
    invoke-interface {v1, v14}, Landroid/database/Cursor;->getInt(I)I

    .line 307
    .line 308
    .line 309
    move-result v14

    .line 310
    invoke-static {v14}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->OO0o〇〇(I)Z

    .line 311
    .line 312
    .line 313
    move-result v14

    .line 314
    if-nez v14, :cond_e

    .line 315
    .line 316
    sget-object v14, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 317
    .line 318
    invoke-static {v14}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 319
    .line 320
    .line 321
    move-result v14

    .line 322
    if-eqz v14, :cond_a

    .line 323
    .line 324
    goto :goto_5

    .line 325
    :cond_a
    const v14, 0x7f081202

    .line 326
    .line 327
    .line 328
    const/4 v12, 0x1

    .line 329
    if-ne v13, v12, :cond_b

    .line 330
    .line 331
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 332
    .line 333
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 334
    .line 335
    .line 336
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 337
    .line 338
    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 339
    .line 340
    .line 341
    goto :goto_6

    .line 342
    :cond_b
    if-ne v13, v2, :cond_c

    .line 343
    .line 344
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 345
    .line 346
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 347
    .line 348
    .line 349
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 350
    .line 351
    const v12, 0x7f081206

    .line 352
    .line 353
    .line 354
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 355
    .line 356
    .line 357
    goto :goto_6

    .line 358
    :cond_c
    const/4 v2, -0x1

    .line 359
    if-ne v15, v2, :cond_d

    .line 360
    .line 361
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 362
    .line 363
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    .line 365
    .line 366
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 367
    .line 368
    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 369
    .line 370
    .line 371
    goto :goto_6

    .line 372
    :cond_d
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 373
    .line 374
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 375
    .line 376
    .line 377
    goto :goto_6

    .line 378
    :cond_e
    :goto_5
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->oO80:Landroid/widget/ImageView;

    .line 379
    .line 380
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 381
    .line 382
    .line 383
    :goto_6
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇888:Landroid/widget/ImageView;

    .line 384
    .line 385
    new-instance v12, Lcom/intsig/camscanner/loadimage/BitmapPara;

    .line 386
    .line 387
    const/4 v13, 0x1

    .line 388
    invoke-interface {v1, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 389
    .line 390
    .line 391
    move-result-object v14

    .line 392
    const/4 v15, 0x5

    .line 393
    invoke-interface {v1, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 394
    .line 395
    .line 396
    move-result-object v15

    .line 397
    invoke-direct {v12, v9, v14, v15}, Lcom/intsig/camscanner/loadimage/BitmapPara;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    .line 399
    .line 400
    invoke-direct {v0, v2, v10, v12}, Lcom/intsig/camscanner/adapter/PageListAdapter;->oO80(Landroid/widget/ImageView;ILcom/intsig/camscanner/loadimage/BitmapPara;)V

    .line 401
    .line 402
    .line 403
    iget-boolean v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOO〇〇:Z

    .line 404
    .line 405
    const/16 v9, 0xc

    .line 406
    .line 407
    const/16 v10, 0xa

    .line 408
    .line 409
    if-nez v2, :cond_1b

    .line 410
    .line 411
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 412
    .line 413
    invoke-virtual {v2}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->oO〇oo()Z

    .line 414
    .line 415
    .line 416
    move-result v2

    .line 417
    if-eqz v2, :cond_f

    .line 418
    .line 419
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oooo8o0〇:Landroid/widget/CheckBox;

    .line 420
    .line 421
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 422
    .line 423
    .line 424
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 425
    .line 426
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 427
    .line 428
    .line 429
    move-result-object v12

    .line 430
    invoke-virtual {v2, v12}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    .line 431
    .line 432
    .line 433
    move-result v2

    .line 434
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oooo8o0〇:Landroid/widget/CheckBox;

    .line 435
    .line 436
    invoke-virtual {v12, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 437
    .line 438
    .line 439
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇808〇:Landroid/view/View;

    .line 440
    .line 441
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 442
    .line 443
    .line 444
    goto :goto_7

    .line 445
    :cond_f
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇808〇:Landroid/view/View;

    .line 446
    .line 447
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 448
    .line 449
    .line 450
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->Oooo8o0〇:Landroid/widget/CheckBox;

    .line 451
    .line 452
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 453
    .line 454
    .line 455
    :goto_7
    const/4 v2, 0x6

    .line 456
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 457
    .line 458
    .line 459
    move-result-object v2

    .line 460
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 461
    .line 462
    .line 463
    move-result v12

    .line 464
    if-nez v12, :cond_14

    .line 465
    .line 466
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 467
    .line 468
    invoke-virtual {v12}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->oO〇oo()Z

    .line 469
    .line 470
    .line 471
    move-result v12

    .line 472
    if-nez v12, :cond_14

    .line 473
    .line 474
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 475
    .line 476
    invoke-virtual {v12, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    .line 478
    .line 479
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 480
    .line 481
    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    .line 483
    .line 484
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 485
    .line 486
    iget-object v14, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 487
    .line 488
    invoke-virtual {v12, v14}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 489
    .line 490
    .line 491
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 492
    .line 493
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 494
    .line 495
    .line 496
    move-result-object v14

    .line 497
    invoke-virtual {v12, v14}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 498
    .line 499
    .line 500
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 501
    .line 502
    if-eqz v12, :cond_11

    .line 503
    .line 504
    array-length v14, v12

    .line 505
    if-lez v14, :cond_11

    .line 506
    .line 507
    invoke-static {v2, v12}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 508
    .line 509
    .line 510
    move-result v12

    .line 511
    if-eqz v12, :cond_10

    .line 512
    .line 513
    iget-object v14, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇oO:[Ljava/util/regex/Pattern;

    .line 514
    .line 515
    if-eqz v14, :cond_10

    .line 516
    .line 517
    array-length v15, v14

    .line 518
    if-lez v15, :cond_10

    .line 519
    .line 520
    iget-object v11, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 521
    .line 522
    iget-object v15, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 523
    .line 524
    invoke-static {v2, v14, v15}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    .line 525
    .line 526
    .line 527
    move-result-object v2

    .line 528
    invoke-virtual {v11, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    .line 530
    .line 531
    const/4 v11, 0x1

    .line 532
    :cond_10
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 533
    .line 534
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 535
    .line 536
    .line 537
    move-result-object v14

    .line 538
    invoke-virtual {v2, v14}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 539
    .line 540
    .line 541
    move-result v2

    .line 542
    if-nez v2, :cond_11

    .line 543
    .line 544
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 545
    .line 546
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 547
    .line 548
    .line 549
    move-result-object v14

    .line 550
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 551
    .line 552
    .line 553
    move-result-object v12

    .line 554
    invoke-virtual {v2, v14, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    .line 556
    .line 557
    :cond_11
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 558
    .line 559
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 560
    .line 561
    .line 562
    move-result-object v12

    .line 563
    invoke-virtual {v2, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    .line 565
    .line 566
    move-result-object v2

    .line 567
    if-eqz v2, :cond_12

    .line 568
    .line 569
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 570
    .line 571
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 572
    .line 573
    .line 574
    move-result-object v12

    .line 575
    invoke-virtual {v2, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    .line 577
    .line 578
    move-result-object v2

    .line 579
    check-cast v2, Ljava/lang/Boolean;

    .line 580
    .line 581
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 582
    .line 583
    .line 584
    move-result v2

    .line 585
    if-eqz v2, :cond_12

    .line 586
    .line 587
    const/4 v2, 0x1

    .line 588
    goto :goto_8

    .line 589
    :cond_12
    const/4 v2, 0x0

    .line 590
    :goto_8
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 591
    .line 592
    if-eqz v2, :cond_13

    .line 593
    .line 594
    const/4 v14, 0x0

    .line 595
    goto :goto_9

    .line 596
    :cond_13
    const/16 v14, 0x8

    .line 597
    .line 598
    :goto_9
    invoke-virtual {v12, v14}, Landroid/view/View;->setVisibility(I)V

    .line 599
    .line 600
    .line 601
    iget-object v12, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 602
    .line 603
    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 604
    .line 605
    .line 606
    new-instance v2, Ljava/lang/StringBuilder;

    .line 607
    .line 608
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 609
    .line 610
    .line 611
    const-string v12, "vh.noteBtn.setSelected = "

    .line 612
    .line 613
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 614
    .line 615
    .line 616
    iget-object v12, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 617
    .line 618
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 619
    .line 620
    .line 621
    move-result-object v14

    .line 622
    invoke-virtual {v12, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    .line 624
    .line 625
    move-result-object v12

    .line 626
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 627
    .line 628
    .line 629
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 630
    .line 631
    .line 632
    move-result-object v2

    .line 633
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    .line 635
    .line 636
    goto :goto_a

    .line 637
    :cond_14
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 638
    .line 639
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    .line 641
    .line 642
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 643
    .line 644
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 645
    .line 646
    .line 647
    :goto_a
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 648
    .line 649
    invoke-virtual {v2}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->oO〇oo()Z

    .line 650
    .line 651
    .line 652
    move-result v2

    .line 653
    const v3, 0x7f080fac

    .line 654
    .line 655
    .line 656
    if-nez v2, :cond_1a

    .line 657
    .line 658
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 659
    .line 660
    if-eqz v2, :cond_1a

    .line 661
    .line 662
    array-length v2, v2

    .line 663
    if-lez v2, :cond_1a

    .line 664
    .line 665
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 666
    .line 667
    .line 668
    move-result-object v2

    .line 669
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 670
    .line 671
    .line 672
    move-result-object v12

    .line 673
    if-nez v11, :cond_18

    .line 674
    .line 675
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 676
    .line 677
    .line 678
    move-result v14

    .line 679
    if-nez v14, :cond_15

    .line 680
    .line 681
    iget-object v14, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 682
    .line 683
    invoke-static {v2, v14}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 684
    .line 685
    .line 686
    move-result v2

    .line 687
    if-nez v2, :cond_17

    .line 688
    .line 689
    :cond_15
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 690
    .line 691
    .line 692
    move-result v2

    .line 693
    if-nez v2, :cond_16

    .line 694
    .line 695
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo8ooo8O:[Ljava/lang/String;

    .line 696
    .line 697
    invoke-static {v12, v2}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 698
    .line 699
    .line 700
    move-result v2

    .line 701
    if-eqz v2, :cond_16

    .line 702
    .line 703
    goto :goto_b

    .line 704
    :cond_16
    const/4 v13, 0x0

    .line 705
    :cond_17
    :goto_b
    or-int/2addr v11, v13

    .line 706
    :cond_18
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 707
    .line 708
    if-eqz v11, :cond_19

    .line 709
    .line 710
    const v3, 0x7f080fae

    .line 711
    .line 712
    .line 713
    :cond_19
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 714
    .line 715
    .line 716
    goto :goto_c

    .line 717
    :cond_1a
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 718
    .line 719
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 720
    .line 721
    .line 722
    goto :goto_c

    .line 723
    :cond_1b
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->OO0o〇〇〇〇0:Landroid/widget/ImageView;

    .line 724
    .line 725
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 726
    .line 727
    .line 728
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇80〇808〇O:Landroid/widget/TextView;

    .line 729
    .line 730
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 731
    .line 732
    .line 733
    :goto_c
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇888:Landroid/widget/ImageView;

    .line 734
    .line 735
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 736
    .line 737
    .line 738
    move-result-object v2

    .line 739
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 740
    .line 741
    iget v3, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 742
    .line 743
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 744
    .line 745
    iget v3, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 746
    .line 747
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 748
    .line 749
    iget-object v3, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇〇888:Landroid/widget/ImageView;

    .line 750
    .line 751
    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 752
    .line 753
    .line 754
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇080:Landroid/view/View;

    .line 755
    .line 756
    iget v3, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 757
    .line 758
    invoke-virtual {v2, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 759
    .line 760
    .line 761
    iget-object v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo0:Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;

    .line 762
    .line 763
    invoke-virtual {v2}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;->oO〇oo()Z

    .line 764
    .line 765
    .line 766
    move-result v2

    .line 767
    if-nez v2, :cond_1d

    .line 768
    .line 769
    iget-boolean v2, v0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOO〇〇:Z

    .line 770
    .line 771
    if-nez v2, :cond_1d

    .line 772
    .line 773
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 774
    .line 775
    .line 776
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 777
    .line 778
    .line 779
    move-result-object v2

    .line 780
    iget-object v3, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 781
    .line 782
    const/4 v9, 0x0

    .line 783
    invoke-virtual {v3, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 784
    .line 785
    .line 786
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 787
    .line 788
    .line 789
    move-result v2

    .line 790
    if-eqz v2, :cond_1c

    .line 791
    .line 792
    iget-object v1, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 793
    .line 794
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 795
    .line 796
    .line 797
    goto :goto_d

    .line 798
    :cond_1c
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 799
    .line 800
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 801
    .line 802
    .line 803
    const/16 v2, 0xb

    .line 804
    .line 805
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 806
    .line 807
    .line 808
    move-result-object v1

    .line 809
    iget-object v2, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 810
    .line 811
    new-instance v3, LO0/〇80〇808〇O;

    .line 812
    .line 813
    invoke-direct {v3, v0, v5, v6, v1}, LO0/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/adapter/PageListAdapter;JLjava/lang/String;)V

    .line 814
    .line 815
    .line 816
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 817
    .line 818
    .line 819
    goto :goto_d

    .line 820
    :cond_1d
    iget-object v1, v7, Lcom/intsig/camscanner/adapter/PageListAdapter$ViewHolder;->〇O〇:Landroid/widget/ImageView;

    .line 821
    .line 822
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 823
    .line 824
    .line 825
    :goto_d
    return-void

    .line 826
    :catch_0
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    .line 828
    .line 829
    return-void
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public getCount()I
    .locals 2

    .line 1
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->OOO〇O0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8oOOo:I

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O〇〇O8:I

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O〇〇O8:I

    .line 19
    .line 20
    :goto_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->getCount()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O〇〇O8:I

    .line 25
    .line 26
    add-int/2addr v0, v1

    .line 27
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooO:I

    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oooo8o0〇()Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;->〇O8o08O(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    .line 37
    .line 38
    goto :goto_1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    const-string v1, "PageListAdapter"

    .line 41
    .line 42
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .line 44
    .line 45
    :goto_1
    iget v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooO:I

    .line 46
    .line 47
    return v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 3
    .line 4
    .line 5
    move-result-object v0

    .line 6
    :try_start_0
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8oOOo:I

    .line 7
    .line 8
    if-ne p1, v1, :cond_0

    .line 9
    .line 10
    iget-boolean v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇o0O:Z

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    iget-boolean v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00O0:Z

    .line 15
    .line 16
    if-nez v1, :cond_0

    .line 17
    .line 18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    return-object p1

    .line 23
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    goto :goto_0

    .line 28
    :catch_0
    const-string p1, "PageListAdapter"

    .line 29
    .line 30
    const-string v1, "getItemId error"

    .line 31
    .line 32
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public getItemId(I)J
    .locals 2

    .line 1
    :try_start_0
    iget v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o8oOOo:I

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇o0O:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇00O0:Z

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    int-to-long v0, p1

    .line 14
    return-wide v0

    .line 15
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/CursorAdapter;->getItemId(I)J

    .line 16
    .line 17
    .line 18
    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    goto :goto_0

    .line 20
    :catch_0
    const-string p1, "PageListAdapter"

    .line 21
    .line 22
    const-string v0, "getItemId error"

    .line 23
    .line 24
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    const-wide/16 v0, 0x0

    .line 28
    .line 29
    :goto_0
    return-wide v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇O8o08O(I)I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooo0〇〇O:I

    .line 6
    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getPositionForSection(I)I
    .locals 0

    .line 1
    return p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getSectionForPosition(I)I
    .locals 0

    .line 1
    return p1
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->getCount()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08〇o0O:[Ljava/lang/String;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0〇O0088o(I)[Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08〇o0O:[Ljava/lang/String;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    array-length v1, v1

    .line 17
    if-eq v1, v0, :cond_1

    .line 18
    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "getSections change mPageNumIndex count="

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v2, " mPageNumIndex.length="

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08〇o0O:[Ljava/lang/String;

    .line 38
    .line 39
    array-length v2, v2

    .line 40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const-string v2, "PageListAdapter"

    .line 48
    .line 49
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0〇O0088o(I)[Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08〇o0O:[Ljava/lang/String;

    .line 57
    .line 58
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08〇o0O:[Ljava/lang/String;

    .line 59
    .line 60
    return-object v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 1
    :try_start_0
    iget v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->ooo0〇〇O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->OO:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const p1, 0x7f0d0633

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p2, p3, p1}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;->〇8o8o〇(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1

    .line 18
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    goto :goto_0

    .line 23
    :catch_0
    move-exception p1

    .line 24
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇08oOOO0:Landroid/view/View;

    .line 25
    .line 26
    if-nez p2, :cond_1

    .line 27
    .line 28
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 29
    .line 30
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 31
    .line 32
    .line 33
    move-result-object p2

    .line 34
    const p3, 0x7f0d046e

    .line 35
    .line 36
    .line 37
    const/4 v0, 0x0

    .line 38
    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    iput-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇08oOOO0:Landroid/view/View;

    .line 43
    .line 44
    const/16 p3, 0x8

    .line 45
    .line 46
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 47
    .line 48
    .line 49
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇08oOOO0:Landroid/view/View;

    .line 50
    .line 51
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/adapter/PageListAdapter;->OoO8(Landroid/view/View;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇08oOOO0:Landroid/view/View;

    .line 55
    .line 56
    new-instance p3, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v0, "getView error"

    .line 62
    .line 63
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const-string p3, "PageListAdapter"

    .line 74
    .line 75
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    move-object p1, p2

    .line 79
    :goto_0
    return-object p1
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public getViewTypeCount()I
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->oOo〇8o008:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const p2, 0x7f0d046e

    .line 8
    .line 9
    .line 10
    const/4 p3, 0x0

    .line 11
    invoke-virtual {p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->OoO8(Landroid/view/View;)V

    .line 16
    .line 17
    .line 18
    return-object p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .line 1
    :try_start_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "PageListAdapter"

    .line 7
    .line 8
    const-string v2, "notifyDataSetChanged"

    .line 9
    .line 10
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    .line 12
    .line 13
    :goto_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo88o8O(Landroid/database/Cursor;)Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-nez v1, :cond_0

    .line 19
    .line 20
    return v2

    .line 21
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->oo88o8O(Landroid/database/Cursor;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-nez v1, :cond_0

    .line 32
    .line 33
    return v2

    .line 34
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v1, "isAllPageUploaded result:"

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const/4 v1, 0x1

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    const-string v2, "PageListAdapter"

    .line 53
    .line 54
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return v1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected onContentChanged()V
    .locals 2

    .line 1
    :try_start_0
    invoke-super {p0}, Landroid/widget/CursorAdapter;->onContentChanged()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "PageListAdapter"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo〇(I)I
    .locals 7

    .line 1
    const/4 v0, 0x2

    .line 2
    if-lez p1, :cond_2

    .line 3
    .line 4
    iget v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o0:I

    .line 5
    .line 6
    if-lez v1, :cond_2

    .line 7
    .line 8
    iget v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 9
    .line 10
    mul-int/lit8 v3, v2, 0x2

    .line 11
    .line 12
    add-int/2addr v3, v1

    .line 13
    div-int v3, p1, v3

    .line 14
    .line 15
    const/high16 v4, 0x43520000    # 210.0f

    .line 16
    .line 17
    const v5, 0x43948000    # 297.0f

    .line 18
    .line 19
    .line 20
    if-le v3, v0, :cond_1

    .line 21
    .line 22
    iput v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 23
    .line 24
    iget v6, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇OOo8〇0:I

    .line 25
    .line 26
    iput v6, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 27
    .line 28
    mul-int/lit8 v2, v2, 0x2

    .line 29
    .line 30
    add-int/2addr v2, v1

    .line 31
    mul-int v2, v2, v3

    .line 32
    .line 33
    sub-int v0, p1, v2

    .line 34
    .line 35
    if-lez v0, :cond_0

    .line 36
    .line 37
    div-int/2addr v0, v3

    .line 38
    if-lez v0, :cond_0

    .line 39
    .line 40
    add-int/2addr v1, v0

    .line 41
    iput v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 42
    .line 43
    int-to-float v0, v1

    .line 44
    mul-float v0, v0, v5

    .line 45
    .line 46
    div-float/2addr v0, v4

    .line 47
    float-to-int v0, v0

    .line 48
    iput v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 49
    .line 50
    :cond_0
    move v0, v3

    .line 51
    goto :goto_0

    .line 52
    :cond_1
    div-int/lit8 v1, p1, 0x2

    .line 53
    .line 54
    mul-int/lit8 v2, v2, 0x2

    .line 55
    .line 56
    sub-int/2addr v1, v2

    .line 57
    iput v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 58
    .line 59
    int-to-float v1, v1

    .line 60
    mul-float v1, v1, v5

    .line 61
    .line 62
    div-float/2addr v1, v4

    .line 63
    float-to-int v1, v1

    .line 64
    iput v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 65
    .line 66
    :cond_2
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v2, "PAGE_ITEM_IMG_WIDTH="

    .line 72
    .line 73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    iget v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇08O〇00〇o:I

    .line 77
    .line 78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v2, " PAGE_ITEM_IMG_HEIGHT="

    .line 82
    .line 83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    iget v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o〇00O:I

    .line 87
    .line 88
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string v2, " numColumn="

    .line 92
    .line 93
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string v2, " viewWidth="

    .line 100
    .line 101
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    const-string p1, " PAGE_ITEM_IMG_WIDTH_ORI="

    .line 108
    .line 109
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    iget p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->o0:I

    .line 113
    .line 114
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string p1, " mItemMargin="

    .line 118
    .line 119
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->Oo80:I

    .line 123
    .line 124
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    const-string v1, "PageListAdapter"

    .line 132
    .line 133
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    return v0
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public o〇〇0〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇0000OOO(JIZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    if-eqz p4, :cond_1

    .line 14
    .line 15
    iget-object p3, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 16
    .line 17
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-virtual {p3, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object p4, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 26
    .line 27
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {p4, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    :cond_1
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public 〇80〇808〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O00()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_0

    .line 15
    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 19
    .line 20
    invoke-virtual {v2}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 25
    .line 26
    .line 27
    new-instance v2, LO0/oO80;

    .line 28
    .line 29
    invoke-direct {v2}, LO0/oO80;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 33
    .line 34
    .line 35
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    check-cast v2, Ljava/util/Map$Entry;

    .line 50
    .line 51
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    check-cast v2, Ljava/lang/Long;

    .line 56
    .line 57
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇O888o0o(I)Z
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/adapter/PageListAdapter;->getItemId(I)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0O:Ljava/util/HashMap;

    .line 18
    .line 19
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    check-cast p1, Ljava/lang/Boolean;

    .line 28
    .line 29
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    return p1

    .line 34
    :cond_0
    const/4 p1, 0x0

    .line 35
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇O〇()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashSet;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 7
    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Ljava/util/Hashtable;->size()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-lez v1, :cond_0

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-eqz v2, :cond_0

    .line 27
    .line 28
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    check-cast v2, Ljava/lang/Long;

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇oOO8O8(JI)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/adapter/PageListAdapter;->〇0000OOO(JIZ)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇808〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇8O0〇8(Z)[I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_1

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/adapter/PageListAdapter;->O〇o88o08〇:Ljava/util/Hashtable;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/util/Hashtable;->entrySet()Ljava/util/Set;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 20
    .line 21
    .line 22
    new-instance v1, LO0/〇〇888;

    .line 23
    .line 24
    invoke-direct {v1}, LO0/〇〇888;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 28
    .line 29
    .line 30
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    new-array v1, v1, [I

    .line 35
    .line 36
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    const/4 v2, 0x0

    .line 41
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    if-eqz v3, :cond_2

    .line 46
    .line 47
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    check-cast v3, Ljava/util/Map$Entry;

    .line 52
    .line 53
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    check-cast v3, Ljava/lang/Integer;

    .line 58
    .line 59
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    if-eqz p1, :cond_0

    .line 64
    .line 65
    add-int/lit8 v3, v3, -0x1

    .line 66
    .line 67
    :cond_0
    aput v3, v1, v2

    .line 68
    .line 69
    add-int/lit8 v2, v2, 0x1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_1
    const/4 v1, 0x0

    .line 73
    :cond_2
    return-object v1
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

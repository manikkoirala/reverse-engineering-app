.class public final Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "DocOpticalRecognizeRetainDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;,
        Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private o〇00O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;

.field private 〇08O〇00〇o:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->O8o08O8O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$Companion;

    .line 8
    .line 9
    const-string v0, "cloud_disk_type"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇080OO8〇0:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇08O〇00〇o:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o880()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    const-string v0, "CSStopAutoReadFilePop"

    .line 14
    .line 15
    const-string v1, "DocOpticalRecognizeRetainDialog"

    .line 16
    .line 17
    if-nez p1, :cond_1

    .line 18
    .line 19
    goto :goto_2

    .line 20
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const v3, 0x7f0a138a

    .line 25
    .line 26
    .line 27
    if-ne v2, v3, :cond_4

    .line 28
    .line 29
    iget p1, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇08O〇00〇o:I

    .line 30
    .line 31
    const/4 v2, -0x1

    .line 32
    const/4 v3, 0x1

    .line 33
    if-eq p1, v2, :cond_2

    .line 34
    .line 35
    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    .line 36
    .line 37
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->Oooo8o0〇(Z)V

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    .line 42
    .line 43
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇O〇(Z)V

    .line 44
    .line 45
    .line 46
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->o〇00O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;

    .line 47
    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    invoke-interface {p1}, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;->〇o00〇〇Oo()V

    .line 51
    .line 52
    .line 53
    :cond_3
    const-string p1, "common_continue"

    .line 54
    .line 55
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const-string p1, "mistake"

    .line 59
    .line 60
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 64
    .line 65
    .line 66
    goto :goto_3

    .line 67
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 68
    .line 69
    goto :goto_3

    .line 70
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    const v2, 0x7f0a138b

    .line 75
    .line 76
    .line 77
    if-ne p1, v2, :cond_7

    .line 78
    .line 79
    const-string p1, "common_leave"

    .line 80
    .line 81
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    const-string p1, "stop"

    .line 85
    .line 86
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->o〇00O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;

    .line 90
    .line 91
    if-eqz p1, :cond_6

    .line 92
    .line 93
    invoke-interface {p1}, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;->〇080()V

    .line 94
    .line 95
    .line 96
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 97
    .line 98
    .line 99
    :cond_7
    :goto_3
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->〇O8oOo0()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->o00〇88〇08()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final o00〇88〇08()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a138a

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 11
    .line 12
    const v2, 0x7f0a138b

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v2, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 20
    .line 21
    const v3, 0x7f0a0945

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Landroidx/appcompat/widget/AppCompatImageView;

    .line 29
    .line 30
    const/4 v3, 0x3

    .line 31
    new-array v3, v3, [Landroid/view/View;

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    aput-object v0, v3, v4

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    aput-object v1, v3, v0

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 40
    .line 41
    const v1, 0x7f0a0b20

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const/4 v1, 0x2

    .line 49
    aput-object v0, v3, v1

    .line 50
    .line 51
    invoke-virtual {p0, v3}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    .line 60
    const v0, 0x7f080f17

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    const v0, 0x7f080f18

    .line 68
    .line 69
    .line 70
    invoke-virtual {v2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 71
    .line 72
    .line 73
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    if-eqz v0, :cond_1

    .line 78
    .line 79
    sget-object v2, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇080OO8〇0:Ljava/lang/String;

    .line 80
    .line 81
    invoke-virtual {v0, v2}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    goto :goto_1

    .line 86
    :cond_1
    const/4 v0, -0x1

    .line 87
    :goto_1
    iput v0, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇08O〇00〇o:I

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 90
    .line 91
    const v2, 0x7f0a138d

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    check-cast v0, Landroid/widget/TextView;

    .line 99
    .line 100
    iget v2, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->〇08O〇00〇o:I

    .line 101
    .line 102
    if-eqz v2, :cond_4

    .line 103
    .line 104
    if-eq v2, v1, :cond_3

    .line 105
    .line 106
    const/4 v1, 0x4

    .line 107
    if-eq v2, v1, :cond_2

    .line 108
    .line 109
    const v1, 0x7f130f90

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    goto :goto_2

    .line 117
    :cond_2
    const v1, 0x7f1315ff

    .line 118
    .line 119
    .line 120
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    goto :goto_2

    .line 125
    :cond_3
    const v1, 0x7f1315fd

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    goto :goto_2

    .line 133
    :cond_4
    const v1, 0x7f1315fe

    .line 134
    .line 135
    .line 136
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSStopAutoReadFilePop"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d01aa

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇o0〇8(Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog;->o〇00O:Lcom/intsig/camscanner/dialog/DocOpticalRecognizeRetainDialog$DialogClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class public final Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "ExternalMemberRetainedDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FunctionViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/camscanner/data/GiftTaskJson;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Landroidx/appcompat/widget/AppCompatImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Landroidx/constraintlayout/widget/Group;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Landroidx/appcompat/widget/AppCompatTextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final ooo0〇〇O:I

.field private final o〇00O:Landroidx/appcompat/widget/AppCompatImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Landroidx/appcompat/widget/AppCompatTextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8〇oO〇〇8o:I

.field final synthetic 〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Landroid/view/View;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 7
    .line 8
    invoke-direct {p0, p2}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    const v0, 0x7f0a0954

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "itemView.findViewById(R.id.iv_item_icon_bg_shadow)"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 26
    .line 27
    const v0, 0x7f0a0953

    .line 28
    .line 29
    .line 30
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "itemView.findViewById(R.id.iv_item_icon_bg_no)"

    .line 35
    .line 36
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 42
    .line 43
    const v0, 0x7f0a094e

    .line 44
    .line 45
    .line 46
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "itemView.findViewById(R.id.iv_item_icon)"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 58
    .line 59
    const v0, 0x7f0a1537

    .line 60
    .line 61
    .line 62
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const-string v1, "itemView.findViewById(R.id.tv_item_title)"

    .line 67
    .line 68
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 72
    .line 73
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 74
    .line 75
    const v0, 0x7f0a151c

    .line 76
    .line 77
    .line 78
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    const-string v1, "itemView.findViewById(R.id.tv_item_btn)"

    .line 83
    .line 84
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 90
    .line 91
    const v0, 0x7f0a152e

    .line 92
    .line 93
    .line 94
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    const-string v1, "itemView.findViewById(R.id.tv_item_no_time)"

    .line 99
    .line 100
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 104
    .line 105
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 106
    .line 107
    const v0, 0x7f0a0955

    .line 108
    .line 109
    .line 110
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    const-string v1, "itemView.findViewById(R.id.iv_item_icon_no)"

    .line 115
    .line 116
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 120
    .line 121
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 122
    .line 123
    const v0, 0x7f0a153a

    .line 124
    .line 125
    .line 126
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    const-string v1, "itemView.findViewById(R.id.tv_item_title_no)"

    .line 131
    .line 132
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 136
    .line 137
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 138
    .line 139
    const v0, 0x7f0a06bf

    .line 140
    .line 141
    .line 142
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    const-string v1, "itemView.findViewById(R.id.group)"

    .line 147
    .line 148
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    check-cast v0, Landroidx/constraintlayout/widget/Group;

    .line 152
    .line 153
    iput-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;

    .line 154
    .line 155
    const v0, 0x7f0a06d8

    .line 156
    .line 157
    .line 158
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 159
    .line 160
    .line 161
    move-result-object p2

    .line 162
    const-string v0, "itemView.findViewById(R.id.group_no)"

    .line 163
    .line 164
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    check-cast p2, Landroidx/constraintlayout/widget/Group;

    .line 168
    .line 169
    iput-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o8〇OO0〇0o:Landroidx/constraintlayout/widget/Group;

    .line 170
    .line 171
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    const/16 v0, 0x8

    .line 176
    .line 177
    invoke-static {p2, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 178
    .line 179
    .line 180
    move-result p2

    .line 181
    iput p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇8〇oO〇〇8o:I

    .line 182
    .line 183
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    const/16 p2, 0x16

    .line 188
    .line 189
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 190
    .line 191
    .line 192
    move-result p1

    .line 193
    iput p1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->ooo0〇〇O:I

    .line 194
    .line 195
    return-void
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private static final OOO〇O0(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Lcom/intsig/camscanner/data/GiftTaskJson;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumMarketing:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 7
    .line 8
    invoke-virtual {p2}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p2

    .line 12
    const-string v0, "accept_reward"

    .line 13
    .line 14
    invoke-static {p0}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇〇o0〇8(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;)Lorg/json/JSONObject;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 19
    .line 20
    .line 21
    const/4 p2, 0x0

    .line 22
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->o880(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Lcom/intsig/camscanner/data/GiftTaskJson;Z)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Lcom/intsig/camscanner/data/GiftTaskJson;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OOO〇O0(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Lcom/intsig/camscanner/data/GiftTaskJson;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method


# virtual methods
.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/data/GiftTaskJson;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o〇〇0〇(Lcom/intsig/camscanner/data/GiftTaskJson;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇〇0〇(Lcom/intsig/camscanner/data/GiftTaskJson;I)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->status:Ljava/lang/Integer;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-string v2, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    .line 13
    .line 14
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 20
    .line 21
    invoke-static {v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;)I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    const/4 v3, 0x3

    .line 26
    const/4 v4, 0x2

    .line 27
    if-le v2, v3, :cond_3

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 30
    .line 31
    invoke-static {v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;)I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    const/4 v3, 0x7

    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    if-gt p2, v4, :cond_2

    .line 40
    .line 41
    iget p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇8〇oO〇〇8o:I

    .line 42
    .line 43
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    iget p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->ooo0〇〇O:I

    .line 47
    .line 48
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_3
    :goto_0
    if-gt p2, v4, :cond_4

    .line 52
    .line 53
    iget p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->ooo0〇〇O:I

    .line 54
    .line 55
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 56
    .line 57
    goto :goto_1

    .line 58
    :cond_4
    iget p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇8〇oO〇〇8o:I

    .line 59
    .line 60
    iput p2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 61
    .line 62
    :goto_1
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 63
    .line 64
    invoke-virtual {p2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    .line 66
    .line 67
    const/4 p2, -0x1

    .line 68
    const/4 v1, 0x0

    .line 69
    const/16 v2, 0x8

    .line 70
    .line 71
    if-nez v0, :cond_5

    .line 72
    .line 73
    goto :goto_2

    .line 74
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 75
    .line 76
    .line 77
    move-result v3

    .line 78
    if-eq v3, p2, :cond_b

    .line 79
    .line 80
    :goto_2
    if-nez v0, :cond_6

    .line 81
    .line 82
    goto :goto_3

    .line 83
    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    const/4 v4, 0x1

    .line 88
    if-ne v3, v4, :cond_7

    .line 89
    .line 90
    goto/16 :goto_6

    .line 91
    .line 92
    :cond_7
    :goto_3
    if-nez v0, :cond_8

    .line 93
    .line 94
    goto :goto_4

    .line 95
    :cond_8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 96
    .line 97
    .line 98
    move-result p2

    .line 99
    if-nez p2, :cond_9

    .line 100
    .line 101
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 102
    .line 103
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 104
    .line 105
    .line 106
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;

    .line 107
    .line 108
    invoke-virtual {p2, v1}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 109
    .line 110
    .line 111
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o8〇OO0〇0o:Landroidx/constraintlayout/widget/Group;

    .line 112
    .line 113
    invoke-virtual {p2, v2}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 114
    .line 115
    .line 116
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 117
    .line 118
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 119
    .line 120
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 121
    .line 122
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 123
    .line 124
    invoke-static {p2, v0, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇8〇80o(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 125
    .line 126
    .line 127
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 128
    .line 129
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 130
    .line 131
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 132
    .line 133
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 134
    .line 135
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 136
    .line 137
    invoke-static {p2, v0, v1, v2, v3}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇o〇88〇8(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/Boolean;)V

    .line 138
    .line 139
    .line 140
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 141
    .line 142
    iget-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 143
    .line 144
    new-instance v1, L〇080O0/o〇0;

    .line 145
    .line 146
    invoke-direct {v1, v0, p1}, L〇080O0/o〇0;-><init>(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Lcom/intsig/camscanner/data/GiftTaskJson;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    .line 151
    .line 152
    iget-object p1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 153
    .line 154
    const p2, 0x7f130d2e

    .line 155
    .line 156
    .line 157
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 158
    .line 159
    .line 160
    goto/16 :goto_9

    .line 161
    .line 162
    :cond_9
    :goto_4
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 163
    .line 164
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 165
    .line 166
    .line 167
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;

    .line 168
    .line 169
    invoke-virtual {p2, v2}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 170
    .line 171
    .line 172
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o8〇OO0〇0o:Landroidx/constraintlayout/widget/Group;

    .line 173
    .line 174
    invoke-virtual {p2, v1}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 175
    .line 176
    .line 177
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 178
    .line 179
    iget-object v0, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 180
    .line 181
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->create_time:Ljava/lang/String;

    .line 182
    .line 183
    if-eqz v1, :cond_a

    .line 184
    .line 185
    invoke-static {v1}, Lkotlin/text/StringsKt;->Oooo8o0〇(Ljava/lang/String;)Ljava/lang/Long;

    .line 186
    .line 187
    .line 188
    move-result-object v1

    .line 189
    if-eqz v1, :cond_a

    .line 190
    .line 191
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 192
    .line 193
    .line 194
    move-result-wide v1

    .line 195
    const/16 v3, 0x3e8

    .line 196
    .line 197
    int-to-long v3, v3

    .line 198
    mul-long v1, v1, v3

    .line 199
    .line 200
    goto :goto_5

    .line 201
    :cond_a
    const-wide/16 v1, 0x0

    .line 202
    .line 203
    :goto_5
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->o〇0〇o(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;J)Ljava/lang/String;

    .line 204
    .line 205
    .line 206
    move-result-object v0

    .line 207
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    .line 209
    .line 210
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 211
    .line 212
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 213
    .line 214
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 215
    .line 216
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 217
    .line 218
    invoke-static {p2, v0, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇8〇80o(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 219
    .line 220
    .line 221
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 222
    .line 223
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 224
    .line 225
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 226
    .line 227
    iget-object v1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 228
    .line 229
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 230
    .line 231
    invoke-static {p2, v0, p1, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇o〇88〇8(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/Boolean;)V

    .line 232
    .line 233
    .line 234
    goto :goto_9

    .line 235
    :cond_b
    :goto_6
    iget-object v3, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;

    .line 236
    .line 237
    invoke-virtual {v3, v2}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 238
    .line 239
    .line 240
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->o8〇OO0〇0o:Landroidx/constraintlayout/widget/Group;

    .line 241
    .line 242
    invoke-virtual {v2, v1}, Landroidx/constraintlayout/widget/Group;->setVisibility(I)V

    .line 243
    .line 244
    .line 245
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 246
    .line 247
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 248
    .line 249
    .line 250
    if-nez v0, :cond_c

    .line 251
    .line 252
    goto :goto_7

    .line 253
    :cond_c
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 254
    .line 255
    .line 256
    move-result v0

    .line 257
    if-ne v0, p2, :cond_d

    .line 258
    .line 259
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 260
    .line 261
    const v0, 0x7f130d3d

    .line 262
    .line 263
    .line 264
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 265
    .line 266
    .line 267
    goto :goto_8

    .line 268
    :cond_d
    :goto_7
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 269
    .line 270
    const-string v0, "\u5df2\u9886\u53d6"

    .line 271
    .line 272
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    .line 274
    .line 275
    :goto_8
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 276
    .line 277
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 278
    .line 279
    iget-object v1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 280
    .line 281
    iget-object v2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 282
    .line 283
    invoke-static {p2, v0, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇8〇80o(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatTextView;)V

    .line 284
    .line 285
    .line 286
    iget-object p2, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->〇〇08O:Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;

    .line 287
    .line 288
    iget-object v0, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->type:Ljava/lang/String;

    .line 289
    .line 290
    iget-object p1, p1, Lcom/intsig/camscanner/data/GiftTaskJson;->num:Ljava/lang/Integer;

    .line 291
    .line 292
    iget-object v1, p0, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog$FunctionViewHolder;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 293
    .line 294
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 295
    .line 296
    invoke-static {p2, v0, p1, v1, v2}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->〇o〇88〇8(Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;Ljava/lang/String;Ljava/lang/Integer;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/Boolean;)V

    .line 297
    .line 298
    .line 299
    :goto_9
    return-void
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.class public final Lcom/intsig/camscanner/ads/operation/OperationStateRecord;
.super Ljava/lang/Object;
.source "OperationStateRecord.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "position"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "data"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇080:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇080:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {p2}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    invoke-virtual {p1, v0, p2}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇00(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string p2, "getInstance().getOperati\u2026rd(mPlacementId, data.id)"

    .line 35
    .line 36
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public 〇080(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getClickCount()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 13
    .line 14
    add-int/lit8 p1, p1, 0x1

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setClickCount(I)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->getClickCount()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    add-int/lit8 v0, v0, 0x1

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->setClickCount(I)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 45
    .line 46
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setLastUpdateTime(J)V

    .line 47
    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇8〇0〇o〇O(J)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getCloseCount()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 13
    .line 14
    add-int/lit8 p1, p1, 0x1

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setCloseCount(I)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->getCloseCount()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    add-int/lit8 v0, v0, 0x1

    .line 36
    .line 37
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->setCloseCount(I)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 45
    .line 46
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setLastUpdateTime(J)V

    .line 47
    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇8〇0〇o〇O(J)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public 〇o〇(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getShowCount()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 13
    .line 14
    add-int/lit8 p1, p1, 0x1

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setShowCount(I)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {p1}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->getShowCount()I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    add-int/lit8 p1, p1, 0x1

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->setShowCount(I)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/ads/operation/OperationStateRecord;->〇o00〇〇Oo:Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 45
    .line 46
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/operation/AdIdRecord;->setLastUpdateTime(J)V

    .line 47
    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇8〇0〇o〇O(J)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

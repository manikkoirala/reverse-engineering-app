.class public final Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog$Companion;
.super Ljava/lang/Object;
.source "FreeAdsTipsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog$Companion$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/advertisement/enums/PositionType;)Z
    .locals 10
    .param p1    # Lcom/intsig/advertisement/enums/PositionType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "positionType"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/advertisement/control/AdConfigManager;->〇〇808〇()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    const-string v2, "FreeAdsTipsDialog"

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const-string p1, "free ad  is not open"

    .line 16
    .line 17
    invoke-static {v2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return v1

    .line 21
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->O〇8O8〇008()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    const-string p1, "current is not ad user"

    .line 28
    .line 29
    invoke-static {v2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return v1

    .line 33
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog$Companion$WhenMappings;->〇080:[I

    .line 34
    .line 35
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    aget v0, v0, v3

    .line 40
    .line 41
    const/4 v3, 0x1

    .line 42
    if-eq v0, v3, :cond_4

    .line 43
    .line 44
    const/4 v4, 0x2

    .line 45
    if-eq v0, v4, :cond_3

    .line 46
    .line 47
    const/4 v4, 0x3

    .line 48
    if-eq v0, v4, :cond_2

    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    goto :goto_0

    .line 55
    :cond_2
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    goto :goto_0

    .line 60
    :cond_3
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;->o8oO〇()Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    goto :goto_0

    .line 65
    :cond_4
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇00()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 74
    .line 75
    .line 76
    move-result-object v5

    .line 77
    invoke-virtual {v5, p1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇0〇O0088o(Lcom/intsig/advertisement/enums/PositionType;)J

    .line 78
    .line 79
    .line 80
    move-result-wide v5

    .line 81
    const-wide/16 v7, 0x0

    .line 82
    .line 83
    cmp-long v9, v5, v7

    .line 84
    .line 85
    if-lez v9, :cond_5

    .line 86
    .line 87
    invoke-static {v5, v6}, Lcom/intsig/camscanner/tsapp/coupon/TimeUtil;->〇080(J)J

    .line 88
    .line 89
    .line 90
    move-result-wide v5

    .line 91
    goto :goto_1

    .line 92
    :cond_5
    const-wide/16 v5, 0x3e8

    .line 93
    .line 94
    :goto_1
    int-to-long v7, v4

    .line 95
    cmp-long v9, v5, v7

    .line 96
    .line 97
    if-gez v9, :cond_6

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string p1, ":dayGap = "

    .line 112
    .line 113
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    const-string p1, ",cfgIntervalGap = "

    .line 120
    .line 121
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    invoke-static {v2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    return v1

    .line 135
    :cond_6
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 136
    .line 137
    .line 138
    move-result-object v4

    .line 139
    invoke-virtual {v4, p1}, Lcom/intsig/advertisement/record/AdRecordHelper;->o800o8O(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 140
    .line 141
    .line 142
    move-result v4

    .line 143
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->O〇8O8〇008()I

    .line 144
    .line 145
    .line 146
    move-result v5

    .line 147
    if-lez v5, :cond_7

    .line 148
    .line 149
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->O〇8O8〇008()I

    .line 150
    .line 151
    .line 152
    move-result v5

    .line 153
    if-ge v4, v5, :cond_7

    .line 154
    .line 155
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 156
    .line 157
    .line 158
    move-result-object v3

    .line 159
    invoke-virtual {v3, p1}, Lcom/intsig/advertisement/record/AdRecordHelper;->oO(Lcom/intsig/advertisement/enums/PositionType;)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {p1}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->O〇8O8〇008()I

    .line 167
    .line 168
    .line 169
    move-result v0

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    .line 171
    .line 172
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .line 174
    .line 175
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    const-string p1, ":hasSkip = "

    .line 179
    .line 180
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    const-string p1, ",cfgSkip = "

    .line 187
    .line 188
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object p1

    .line 198
    invoke-static {v2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    return v1

    .line 202
    :cond_7
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 203
    .line 204
    .line 205
    move-result-object v4

    .line 206
    invoke-virtual {v4, p1}, Lcom/intsig/advertisement/record/AdRecordHelper;->OoO8(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 207
    .line 208
    .line 209
    move-result v4

    .line 210
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->O8ooOoo〇()I

    .line 211
    .line 212
    .line 213
    move-result v5

    .line 214
    if-lt v4, v5, :cond_8

    .line 215
    .line 216
    invoke-virtual {p1}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object p1

    .line 220
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->O8ooOoo〇()I

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    new-instance v3, Ljava/lang/StringBuilder;

    .line 225
    .line 226
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 227
    .line 228
    .line 229
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    const-string p1, ":showCount = "

    .line 233
    .line 234
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    const-string p1, ",maxCount = "

    .line 241
    .line 242
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object p1

    .line 252
    invoke-static {v2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    return v1

    .line 256
    :cond_8
    return v3
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public final 〇o00〇〇Oo(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/advertisement/params/RequestParam;)V
    .locals 2
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "fgActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p2}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇8(Lcom/intsig/advertisement/enums/PositionType;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;

    .line 20
    .line 21
    invoke-direct {v0}, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;->〇o08(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object v1, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog$Companion;

    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, p1, v1}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-static {p1}, Lcom/intsig/advertisement/logagent/AdTrackUtils;->o〇0(Lcom/intsig/advertisement/enums/PositionType;)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    const-string p2, "CSPurePop"

    .line 53
    .line 54
    const-string v0, "from_ad"

    .line 55
    .line 56
    invoke-static {p2, v0, p1}, Lcom/intsig/log/LogAgentHelper;->o〇〇0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

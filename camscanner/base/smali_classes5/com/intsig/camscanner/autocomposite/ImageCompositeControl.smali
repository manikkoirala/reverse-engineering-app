.class public Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;
.super Ljava/lang/Object;
.source "ImageCompositeControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$PersonalMold;,
        Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$TeamMold;,
        Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;
    }
.end annotation


# static fields
.field public static OOO〇O0:I = 0xdb4

.field private static final oo〇:[I

.field public static o〇〇0〇:I = 0x9b0

.field private static 〇0000OOO:Landroid/graphics/Bitmap$Config;


# instance fields
.field private O8:F

.field private final O8ooOoo〇:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

.field private OO0o〇〇〇〇0:Ljava/lang/String;

.field private Oo08:I

.field private OoO8:Landroid/graphics/drawable/ShapeDrawable;

.field private Oooo8o0〇:Z

.field private O〇8O8〇008:Z

.field private o800o8O:Landroid/graphics/BitmapShader;

.field private oO80:Z

.field private oo88o8O:Ljava/lang/String;

.field private o〇0:I

.field private o〇O8〇〇o:Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;

.field private 〇00:Z

.field private 〇080:F

.field private 〇0〇O0088o:Landroid/graphics/drawable/shapes/RoundRectShape;

.field private 〇80〇808〇O:Landroid/net/Uri;

.field private 〇8o8o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O00:F

.field private 〇O888o0o:Z

.field private 〇O8o08O:Ljava/lang/String;

.field private 〇O〇:F

.field private 〇o00〇〇Oo:F

.field private 〇oOO8O8:Landroid/net/Uri;

.field private 〇oo〇:Ljava/lang/String;

.field private 〇o〇:F

.field private 〇〇808〇:Z

.field private 〇〇888:Landroid/content/Context;

.field private 〇〇8O0〇8:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo〇:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0x1
        0x2
        0x4
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(ZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Landroid/graphics/RectF;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, "page_num DESC"

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O8o08O:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oooo8o0〇:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇808〇:Z

    .line 12
    .line 13
    const/high16 v1, 0x42480000    # 50.0f

    .line 14
    .line 15
    iput v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O〇:F

    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O00:F

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    iput-boolean v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O888o0o:Z

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    iput-object v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo88o8O:Ljava/lang/String;

    .line 24
    .line 25
    iput-object v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oo〇:Ljava/lang/String;

    .line 26
    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇00:Z

    .line 28
    .line 29
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O〇8O8〇008:Z

    .line 30
    .line 31
    new-instance v0, Ljava/util/HashMap;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8ooOoo〇:Ljava/util/Map;

    .line 37
    .line 38
    iput-object p2, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888:Landroid/content/Context;

    .line 39
    .line 40
    iput-object p5, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇80〇808〇O:Landroid/net/Uri;

    .line 41
    .line 42
    iput-boolean p1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oO80:Z

    .line 43
    .line 44
    iput-object p6, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 45
    .line 46
    iput-object p7, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇8o8o〇:Ljava/util/ArrayList;

    .line 47
    .line 48
    iput-object p8, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O8o08O:Ljava/lang/String;

    .line 49
    .line 50
    iput-object p9, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

    .line 51
    .line 52
    const-string p1, "page_num ASC"

    .line 53
    .line 54
    invoke-virtual {p1, p8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    iput-boolean p1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O888o0o:Z

    .line 59
    .line 60
    invoke-static {p2, p4}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 61
    .line 62
    .line 63
    new-instance p1, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;

    .line 64
    .line 65
    invoke-direct {p1, p4}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;-><init>(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-static {p1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 69
    .line 70
    .line 71
    iput-object p4, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo88o8O:Ljava/lang/String;

    .line 72
    .line 73
    iput-object p3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oo〇:Ljava/lang/String;

    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇()V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
.end method

.method private O8(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    .line 7
    .line 8
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    .line 9
    .line 10
    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 17
    .line 18
    .line 19
    const/4 v0, -0x1

    .line 20
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private OO0o〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oo〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$PersonalMold;

    .line 11
    .line 12
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$PersonalMold;-><init>(Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;Lcom/intsig/camscanner/autocomposite/o〇O;)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇O8〇〇o:Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$TeamMold;

    .line 19
    .line 20
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$TeamMold;-><init>(Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;Lcom/intsig/camscanner/autocomposite/oO00OOO;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇O8〇〇o:Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;

    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static OoO8(JJ)V
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "availMemory = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, "  runtimeMaxMemory = "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "ImageCompositeControl"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 32
    .line 33
    sput-object v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0000OOO:Landroid/graphics/Bitmap$Config;

    .line 34
    .line 35
    const-wide/32 v2, 0xec01570

    .line 36
    .line 37
    .line 38
    cmp-long v0, p0, v2

    .line 39
    .line 40
    if-lez v0, :cond_0

    .line 41
    .line 42
    cmp-long v0, p2, v2

    .line 43
    .line 44
    if-lez v0, :cond_0

    .line 45
    .line 46
    const/16 p0, 0xceb

    .line 47
    .line 48
    const/16 p1, 0x1245

    .line 49
    .line 50
    invoke-static {p0, p1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 51
    .line 52
    .line 53
    const-string p0, "ImageComposite, Density = 400DPI"

    .line 54
    .line 55
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_0
    const-wide/32 v2, 0x84bfc00

    .line 61
    .line 62
    .line 63
    cmp-long v0, p0, v2

    .line 64
    .line 65
    if-lez v0, :cond_1

    .line 66
    .line 67
    cmp-long v0, p2, v2

    .line 68
    .line 69
    if-lez v0, :cond_1

    .line 70
    .line 71
    const/16 p0, 0x9b0

    .line 72
    .line 73
    const/16 p1, 0xdb4

    .line 74
    .line 75
    invoke-static {p0, p1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 76
    .line 77
    .line 78
    const-string p0, "ImageComposite, Density = 300DPI"

    .line 79
    .line 80
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_1
    const-wide/32 v2, 0x54f2800

    .line 85
    .line 86
    .line 87
    cmp-long v0, p0, v2

    .line 88
    .line 89
    if-lez v0, :cond_2

    .line 90
    .line 91
    cmp-long v0, p2, v2

    .line 92
    .line 93
    if-lez v0, :cond_2

    .line 94
    .line 95
    const/16 p0, 0x7c0

    .line 96
    .line 97
    const/16 p1, 0xaf6

    .line 98
    .line 99
    invoke-static {p0, p1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 100
    .line 101
    .line 102
    const-string p0, "ImageComposite, Density = 240DPI"

    .line 103
    .line 104
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_2
    const-wide/32 v2, 0x3b08220

    .line 109
    .line 110
    .line 111
    cmp-long v0, p0, v2

    .line 112
    .line 113
    if-lez v0, :cond_3

    .line 114
    .line 115
    cmp-long v0, p2, v2

    .line 116
    .line 117
    if-lez v0, :cond_3

    .line 118
    .line 119
    const/16 p0, 0x676

    .line 120
    .line 121
    const/16 p1, 0x923

    .line 122
    .line 123
    invoke-static {p0, p1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 124
    .line 125
    .line 126
    const-string p0, "ImageComposite, Density = 200DPI"

    .line 127
    .line 128
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    goto :goto_0

    .line 132
    :cond_3
    const/16 v0, 0x839

    .line 133
    .line 134
    const/16 v2, 0x5cf

    .line 135
    .line 136
    const-wide/32 v3, 0x2fc3170

    .line 137
    .line 138
    .line 139
    cmp-long v5, p0, v3

    .line 140
    .line 141
    if-lez v5, :cond_4

    .line 142
    .line 143
    cmp-long p0, p2, v3

    .line 144
    .line 145
    if-lez p0, :cond_4

    .line 146
    .line 147
    invoke-static {v2, v0}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 148
    .line 149
    .line 150
    const-string p0, "ImageComposite, Density = 180DPI"

    .line 151
    .line 152
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    goto :goto_0

    .line 156
    :cond_4
    invoke-static {v2, v0}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o(II)V

    .line 157
    .line 158
    .line 159
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->O8〇o()Landroid/graphics/Bitmap$Config;

    .line 160
    .line 161
    .line 162
    move-result-object p0

    .line 163
    sput-object p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0000OOO:Landroid/graphics/Bitmap$Config;

    .line 164
    .line 165
    const-string p0, "ImageComposite, Default,Density = 180DPI"

    .line 166
    .line 167
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    :goto_0
    return-void
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private Oooo8o0〇(Landroid/database/Cursor;Ljava/lang/String;ZZ)Z
    .locals 19

    move-object/from16 v10, p0

    move-object/from16 v1, p1

    const-string v11, "ImageCompositeControl"

    const/4 v2, 0x1

    .line 1
    :try_start_0
    sget v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇〇0〇:I

    sget v4, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OOO〇O0:I

    sget-object v5, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0000OOO:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 2
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    if-nez v0, :cond_0

    if-eqz v4, :cond_13

    .line 3
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_10

    :cond_0
    if-eqz v4, :cond_13

    .line 4
    :try_start_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 5
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 6
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 7
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 8
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v7, -0x1

    .line 9
    invoke-virtual {v5, v7}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 10
    iget-object v7, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇8o8o〇:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 11
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v8

    .line 12
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v2, 0x0

    const/16 v16, 0x0

    :goto_1
    if-ge v2, v8, :cond_c

    .line 13
    invoke-direct {v10, v5}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8(Landroid/graphics/Canvas;)V

    .line 14
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v7, :cond_4

    .line 15
    iget-object v0, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇8o8o〇:Ljava/util/ArrayList;

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    move/from16 v17, v7

    move/from16 v18, v8

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v5, v0, v8}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oO80(Landroid/graphics/Canvas;Landroid/graphics/RectF;Ljava/lang/String;)Z

    move-result v0

    const/4 v7, 0x2

    if-nez v12, :cond_1

    .line 16
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_1
    const-string v8, ","

    .line 17
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    if-nez v0, :cond_2

    move v7, v2

    move/from16 v8, v18

    goto :goto_4

    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 18
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_3

    move v7, v2

    const/4 v8, 0x0

    goto :goto_4

    :cond_3
    add-int/lit8 v12, v12, 0x1

    move/from16 v7, v17

    move/from16 v8, v18

    goto :goto_2

    :cond_4
    move/from16 v17, v7

    move/from16 v18, v8

    move v7, v2

    :goto_4
    move v2, v0

    if-eqz v2, :cond_b

    .line 19
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    if-nez v0, :cond_7

    .line 20
    :try_start_2
    iget-object v0, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888:Landroid/content/Context;

    sget v12, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇〇0〇:I

    sget v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OOO〇O0:I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move/from16 v18, v8

    const/4 v8, 0x0

    :try_start_3
    invoke-static {v0, v15, v12, v1, v8}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_5

    .line 21
    :try_start_4
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 22
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-virtual {v5, v8, v8, v0, v12}, Landroid/graphics/Canvas;->clipRect(IIII)Z
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/4 v0, 0x0

    const/4 v12, 0x0

    .line 23
    :try_start_5
    invoke-virtual {v5, v1, v0, v0, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 24
    invoke-virtual {v5}, Landroid/graphics/Canvas;->restore()V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    const/4 v12, 0x0

    goto :goto_9

    :cond_5
    const/4 v12, 0x0

    :goto_5
    if-eqz v1, :cond_8

    .line 25
    :try_start_6
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_8

    .line 26
    :goto_6
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_b

    :catchall_0
    move-exception v0

    goto :goto_7

    :catch_3
    move-exception v0

    goto :goto_8

    :catchall_1
    move-exception v0

    const/4 v8, 0x0

    :goto_7
    const/4 v12, 0x0

    move-object v3, v12

    goto :goto_a

    :catch_4
    move-exception v0

    move/from16 v18, v8

    const/4 v8, 0x0

    :goto_8
    const/4 v12, 0x0

    move-object v1, v12

    .line 27
    :goto_9
    :try_start_7
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v1, :cond_8

    .line 28
    :try_start_8
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_6

    :catchall_2
    move-exception v0

    move-object v3, v1

    :goto_a
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_6

    .line 29
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 30
    :cond_6
    throw v0

    :cond_7
    move/from16 v18, v8

    const/4 v8, 0x0

    const/4 v12, 0x0

    .line 31
    :cond_8
    :goto_b
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 32
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v0

    .line 33
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".jpg"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 34
    iget-object v8, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888:Landroid/content/Context;

    invoke-virtual {v10, v8, v4, v1}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O〇(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_a

    .line 35
    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    invoke-static {v8, v1}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 37
    invoke-virtual {v14, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v0, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

    move/from16 v8, v16

    if-eqz v0, :cond_9

    .line 41
    invoke-interface {v0, v8}, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;->〇080(I)V

    add-int/lit8 v16, v8, 0x1

    :cond_9
    move-object/from16 v1, p1

    move v0, v2

    move v2, v7

    move/from16 v7, v17

    move/from16 v8, v18

    goto/16 :goto_1

    :cond_a
    move/from16 v8, v16

    const-string v0, "Fail to save the path of image !"

    .line 42
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_c

    :cond_b
    move/from16 v8, v16

    const-string v0, "Fail to composte image!"

    .line 43
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_c

    :cond_c
    move/from16 v8, v16

    .line 44
    :goto_c
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    if-eqz v0, :cond_10

    .line 45
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    if-eqz p4, :cond_e

    .line 46
    iget-object v0, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

    if-eqz v0, :cond_d

    .line 47
    invoke-interface {v0, v13}, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;->〇o〇(Ljava/util/ArrayList;)V

    :cond_d
    const/4 v1, 0x1

    return v1

    .line 48
    :cond_e
    iget-object v2, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888:Landroid/content/Context;

    iget-object v3, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇80〇808〇O:Landroid/net/Uri;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    move-object/from16 v1, p0

    move-object v4, v13

    move-object v5, v14

    move-object/from16 v7, p2

    move v12, v8

    const/4 v15, 0x0

    move/from16 v8, p3

    :try_start_9
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZLjava/util/ArrayList;)Z

    move-result v7

    .line 49
    iget-object v0, v10, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

    if-eqz v0, :cond_11

    .line 50
    invoke-interface {v0, v12}, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;->〇080(I)V

    goto :goto_d

    :cond_f
    const/4 v15, 0x0

    const-string v0, "Fail to save createNewCompositeDoc!"

    .line 51
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    goto :goto_d

    :cond_10
    const/4 v15, 0x0

    move v7, v0

    :cond_11
    :goto_d
    if-nez v7, :cond_12

    .line 52
    invoke-direct {v10, v13, v14}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_e

    :catch_5
    move-exception v0

    goto :goto_f

    :cond_12
    :goto_e
    move v12, v7

    goto :goto_11

    :catch_6
    move-exception v0

    const/4 v15, 0x0

    .line 53
    :goto_f
    invoke-static {v11, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v12, 0x0

    goto :goto_11

    :cond_13
    :goto_10
    move v12, v0

    :goto_11
    return v12
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oO80:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇0〇O0088o(II)V
    .locals 0

    .line 1
    sput p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇〇0〇:I

    .line 2
    .line 3
    sput p1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OOO〇O0:I

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇80〇808〇O(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇8O0〇8:[F

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o:Landroid/graphics/drawable/shapes/RoundRectShape;

    .line 10
    .line 11
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇0〇O0088o:Landroid/graphics/drawable/shapes/RoundRectShape;

    .line 14
    .line 15
    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OoO8:Landroid/graphics/drawable/ShapeDrawable;

    .line 19
    .line 20
    invoke-virtual {v0, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 21
    .line 22
    .line 23
    new-instance p3, Landroid/graphics/BitmapShader;

    .line 24
    .line 25
    sget-object v0, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    .line 26
    .line 27
    invoke-direct {p3, p2, v0, v0}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 28
    .line 29
    .line 30
    iput-object p3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o800o8O:Landroid/graphics/BitmapShader;

    .line 31
    .line 32
    iget-object p2, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OoO8:Landroid/graphics/drawable/ShapeDrawable;

    .line 33
    .line 34
    invoke-virtual {p2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    iget-object p3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o800o8O:Landroid/graphics/BitmapShader;

    .line 39
    .line 40
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 41
    .line 42
    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OoO8:Landroid/graphics/drawable/ShapeDrawable;

    .line 44
    .line 45
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo88o8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇〇808〇(Landroid/content/Context;J)V
    .locals 4

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const-string v3, "image_confirm_state"

    .line 12
    .line 13
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x2

    .line 17
    new-array v2, v2, [Ljava/lang/String;

    .line 18
    .line 19
    const-string v3, "2"

    .line 20
    .line 21
    aput-object v3, v2, v1

    .line 22
    .line 23
    new-instance v1, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string p2, ""

    .line 32
    .line 33
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    const/4 p3, 0x1

    .line 41
    aput-object p2, v2, p3

    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 48
    .line 49
    const-string p3, "image_confirm_state =? AND document_id =? "

    .line 50
    .line 51
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    new-instance p2, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string p3, "refreshImageConfirmState row="

    .line 61
    .line 62
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    const-string p2, "ImageCompositeControl"

    .line 73
    .line 74
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private 〇〇888(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    check-cast v1, Ljava/lang/String;

    .line 24
    .line 25
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 30
    .line 31
    .line 32
    :cond_1
    if-eqz p2, :cond_3

    .line 33
    .line 34
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-nez p1, :cond_3

    .line 39
    .line 40
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    check-cast v0, Ljava/lang/String;

    .line 55
    .line 56
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 61
    .line 62
    .line 63
    :cond_3
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public OO0o〇〇〇〇0()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇808〇:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ZLjava/util/ArrayList;)Z
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v2, p1

    .line 4
    .line 5
    move-object/from16 v3, p2

    .line 6
    .line 7
    move-object/from16 v4, p5

    .line 8
    .line 9
    move-object/from16 v5, p6

    .line 10
    .line 11
    move/from16 v6, p7

    .line 12
    .line 13
    move-object/from16 v7, p8

    .line 14
    .line 15
    const-string v0, "page_num"

    .line 16
    .line 17
    new-instance v8, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v9, "createNewCompositeDoc appendPage:"

    .line 23
    .line 24
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v8

    .line 34
    const-string v9, "ImageCompositeControl"

    .line 35
    .line 36
    invoke-static {v9, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result v8

    .line 43
    if-lez v8, :cond_e

    .line 44
    .line 45
    const/4 v14, 0x0

    .line 46
    if-eqz v6, :cond_0

    .line 47
    .line 48
    :try_start_0
    iput-object v3, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    iget-object v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇O8〇〇o:Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;

    .line 52
    .line 53
    invoke-interface {v10, v2, v5}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl$MoldInterface;->〇080(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    .line 54
    .line 55
    .line 56
    move-result-object v10

    .line 57
    iput-object v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 58
    .line 59
    :goto_0
    iget-object v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 60
    .line 61
    if-eqz v10, :cond_d

    .line 62
    .line 63
    new-instance v13, Ljava/util/ArrayList;

    .line 64
    .line 65
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .line 67
    .line 68
    iget-object v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 69
    .line 70
    invoke-static {v10}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 71
    .line 72
    .line 73
    move-result-wide v11

    .line 74
    iget-boolean v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇00:Z

    .line 75
    .line 76
    if-eqz v10, :cond_8

    .line 77
    .line 78
    iget-object v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 79
    .line 80
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 81
    .line 82
    .line 83
    move-result v10

    .line 84
    if-eqz v10, :cond_1

    .line 85
    .line 86
    const/4 v10, 0x0

    .line 87
    :goto_1
    move-object/from16 v20, v10

    .line 88
    .line 89
    goto :goto_2

    .line 90
    :cond_1
    new-instance v10, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string v15, "_id in "

    .line 96
    .line 97
    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    iget-object v15, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 101
    .line 102
    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v10

    .line 109
    goto :goto_1

    .line 110
    :goto_2
    invoke-static {v11, v12}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 111
    .line 112
    .line 113
    move-result-object v10

    .line 114
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 115
    .line 116
    .line 117
    move-result-object v17

    .line 118
    filled-new-array {v0}, [Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v19

    .line 122
    const/16 v21, 0x0

    .line 123
    .line 124
    const-string v22, "page_num ASC"

    .line 125
    .line 126
    move-object/from16 v18, v10

    .line 127
    .line 128
    invoke-virtual/range {v17 .. v22}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 129
    .line 130
    .line 131
    move-result-object v15

    .line 132
    if-eqz v15, :cond_3

    .line 133
    .line 134
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 135
    .line 136
    .line 137
    move-result v17

    .line 138
    if-eqz v17, :cond_2

    .line 139
    .line 140
    invoke-interface {v15, v14}, Landroid/database/Cursor;->getInt(I)I

    .line 141
    .line 142
    .line 143
    move-result v17

    .line 144
    goto :goto_3

    .line 145
    :cond_2
    const/16 v17, 0x1

    .line 146
    .line 147
    :goto_3
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 148
    .line 149
    .line 150
    move/from16 v15, v17

    .line 151
    .line 152
    goto :goto_4

    .line 153
    :cond_3
    const/4 v15, 0x1

    .line 154
    :goto_4
    new-instance v14, Ljava/util/ArrayList;

    .line 155
    .line 156
    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 157
    .line 158
    .line 159
    const-string v24, "page_num >= ?"

    .line 160
    .line 161
    move-object/from16 v18, v13

    .line 162
    .line 163
    const/4 v13, 0x1

    .line 164
    new-array v5, v13, [Ljava/lang/String;

    .line 165
    .line 166
    new-instance v13, Ljava/lang/StringBuilder;

    .line 167
    .line 168
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    const-string v3, ""

    .line 175
    .line 176
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v3

    .line 183
    const/4 v13, 0x0

    .line 184
    aput-object v3, v5, v13

    .line 185
    .line 186
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 187
    .line 188
    .line 189
    move-result-object v21

    .line 190
    const-string v3, "_id"

    .line 191
    .line 192
    filled-new-array {v3, v0}, [Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object v23

    .line 196
    const-string v26, "page_num ASC"

    .line 197
    .line 198
    move-object/from16 v22, v10

    .line 199
    .line 200
    move-object/from16 v25, v5

    .line 201
    .line 202
    invoke-virtual/range {v21 .. v26}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 203
    .line 204
    .line 205
    move-result-object v3

    .line 206
    if-eqz v3, :cond_5

    .line 207
    .line 208
    :goto_5
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 209
    .line 210
    .line 211
    move-result v5

    .line 212
    if-eqz v5, :cond_4

    .line 213
    .line 214
    new-instance v5, Landroid/content/ContentValues;

    .line 215
    .line 216
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 217
    .line 218
    .line 219
    const/4 v10, 0x1

    .line 220
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getInt(I)I

    .line 221
    .line 222
    .line 223
    move-result v13

    .line 224
    add-int/2addr v13, v8

    .line 225
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 226
    .line 227
    .line 228
    move-result-object v10

    .line 229
    invoke-virtual {v5, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    .line 231
    .line 232
    sget-object v10, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 233
    .line 234
    const/4 v13, 0x0

    .line 235
    invoke-interface {v3, v13}, Landroid/database/Cursor;->getLong(I)J

    .line 236
    .line 237
    .line 238
    move-result-wide v6

    .line 239
    invoke-static {v10, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 240
    .line 241
    .line 242
    move-result-object v6

    .line 243
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 244
    .line 245
    .line 246
    move-result-object v6

    .line 247
    invoke-virtual {v6, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 248
    .line 249
    .line 250
    move-result-object v5

    .line 251
    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 252
    .line 253
    .line 254
    move-result-object v5

    .line 255
    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    .line 257
    .line 258
    move/from16 v6, p7

    .line 259
    .line 260
    move-object/from16 v7, p8

    .line 261
    .line 262
    goto :goto_5

    .line 263
    :cond_4
    const/4 v13, 0x0

    .line 264
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 265
    .line 266
    .line 267
    goto :goto_6

    .line 268
    :cond_5
    const/4 v13, 0x0

    .line 269
    :goto_6
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    .line 270
    .line 271
    .line 272
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 273
    if-lez v0, :cond_6

    .line 274
    .line 275
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    sget-object v3, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 280
    .line 281
    invoke-virtual {v0, v3, v14}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 282
    .line 283
    .line 284
    goto :goto_7

    .line 285
    :catch_0
    move-exception v0

    .line 286
    :try_start_2
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    .line 288
    .line 289
    goto :goto_7

    .line 290
    :catch_1
    move-exception v0

    .line 291
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 292
    .line 293
    .line 294
    :cond_6
    :goto_7
    const/4 v3, 0x1

    .line 295
    if-lez v15, :cond_7

    .line 296
    .line 297
    add-int/lit8 v0, v15, -0x1

    .line 298
    .line 299
    goto :goto_8

    .line 300
    :cond_7
    const/4 v0, 0x0

    .line 301
    goto :goto_8

    .line 302
    :cond_8
    move-object/from16 v18, v13

    .line 303
    .line 304
    const/4 v3, 0x1

    .line 305
    const/4 v13, 0x0

    .line 306
    invoke-static {v2, v11, v12}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 307
    .line 308
    .line 309
    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 310
    :goto_8
    const/4 v5, 0x0

    .line 311
    :goto_9
    if-ge v5, v8, :cond_a

    .line 312
    .line 313
    :try_start_3
    iget-boolean v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O888o0o:Z

    .line 314
    .line 315
    if-eqz v6, :cond_9

    .line 316
    .line 317
    add-int/lit8 v6, v5, 0x1

    .line 318
    .line 319
    goto :goto_a

    .line 320
    :cond_9
    sub-int v6, v8, v5

    .line 321
    .line 322
    :goto_a
    add-int v16, v0, v6

    .line 323
    .line 324
    sget-object v10, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 325
    .line 326
    move-object/from16 v6, p3

    .line 327
    .line 328
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 329
    .line 330
    .line 331
    move-result-object v7

    .line 332
    check-cast v7, Ljava/lang/String;

    .line 333
    .line 334
    move-object/from16 v15, p4

    .line 335
    .line 336
    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 337
    .line 338
    .line 339
    move-result-object v14

    .line 340
    check-cast v14, Ljava/lang/String;

    .line 341
    .line 342
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 343
    .line 344
    .line 345
    move-result-object v17

    .line 346
    check-cast v17, Ljava/lang/String;

    .line 347
    .line 348
    iget-boolean v3, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oO80:Z

    .line 349
    .line 350
    move-object/from16 v6, p8

    .line 351
    .line 352
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 353
    .line 354
    .line 355
    move-result-object v20

    .line 356
    check-cast v20, Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 357
    .line 358
    move-wide/from16 v21, v11

    .line 359
    .line 360
    move-object/from16 v1, v18

    .line 361
    .line 362
    const/16 v18, 0x0

    .line 363
    .line 364
    move-object v13, v7

    .line 365
    const/4 v7, 0x0

    .line 366
    const/16 v23, 0x1

    .line 367
    .line 368
    move-object/from16 v15, v17

    .line 369
    .line 370
    move/from16 v17, v3

    .line 371
    .line 372
    move/from16 v18, v5

    .line 373
    .line 374
    move/from16 v19, v8

    .line 375
    .line 376
    :try_start_4
    invoke-virtual/range {v10 .. v20}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇o〇(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)Landroid/content/ContentProviderOperation;

    .line 377
    .line 378
    .line 379
    move-result-object v3

    .line 380
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    .line 382
    .line 383
    sget-object v3, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 384
    .line 385
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 386
    .line 387
    .line 388
    move-result-object v10

    .line 389
    check-cast v10, Ljava/lang/String;

    .line 390
    .line 391
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 392
    .line 393
    .line 394
    move-result-object v11

    .line 395
    check-cast v11, Ljava/lang/String;

    .line 396
    .line 397
    invoke-virtual {v3, v2, v10, v11}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->ooo〇8oO(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    .line 399
    .line 400
    add-int/lit8 v5, v5, 0x1

    .line 401
    .line 402
    move-object/from16 v18, v1

    .line 403
    .line 404
    move-wide/from16 v11, v21

    .line 405
    .line 406
    const/4 v3, 0x1

    .line 407
    const/4 v13, 0x0

    .line 408
    move-object/from16 v1, p0

    .line 409
    .line 410
    goto :goto_9

    .line 411
    :catch_2
    const/4 v7, 0x0

    .line 412
    goto :goto_d

    .line 413
    :cond_a
    move-wide/from16 v21, v11

    .line 414
    .line 415
    move-object/from16 v1, v18

    .line 416
    .line 417
    const/4 v7, 0x0

    .line 418
    const/16 v23, 0x1

    .line 419
    .line 420
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 421
    .line 422
    .line 423
    move-result v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 424
    if-lez v0, :cond_b

    .line 425
    .line 426
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 427
    .line 428
    .line 429
    move-result-object v0

    .line 430
    sget-object v3, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 431
    .line 432
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 433
    .line 434
    .line 435
    goto :goto_b

    .line 436
    :catchall_0
    move-exception v0

    .line 437
    :try_start_6
    new-instance v3, Ljava/lang/StringBuilder;

    .line 438
    .line 439
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 440
    .line 441
    .line 442
    const-string v4, "createNewCompositeDoc error= "

    .line 443
    .line 444
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 448
    .line 449
    .line 450
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v0

    .line 454
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    :cond_b
    :goto_b
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 458
    .line 459
    .line 460
    if-eqz p7, :cond_c

    .line 461
    .line 462
    move-object/from16 v1, p0

    .line 463
    .line 464
    move-wide/from16 v3, v21

    .line 465
    .line 466
    :try_start_7
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇808〇(Landroid/content/Context;J)V

    .line 467
    .line 468
    .line 469
    goto :goto_c

    .line 470
    :cond_c
    move-object/from16 v1, p0

    .line 471
    .line 472
    move-wide/from16 v3, v21

    .line 473
    .line 474
    iget-object v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 475
    .line 476
    move-object/from16 v5, p2

    .line 477
    .line 478
    invoke-static {v2, v5, v0}, Lcom/intsig/camscanner/app/DBUtil;->〇O〇(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 479
    .line 480
    .line 481
    :goto_c
    move-object/from16 v5, p6

    .line 482
    .line 483
    invoke-static {v2, v3, v4, v5}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o0O0O8(Landroid/content/Context;JLjava/lang/String;)V

    .line 484
    .line 485
    .line 486
    new-instance v0, Ljava/lang/StringBuilder;

    .line 487
    .line 488
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 489
    .line 490
    .line 491
    const-string v2, "mNewDocUri = "

    .line 492
    .line 493
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    .line 495
    .line 496
    iget-object v2, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 497
    .line 498
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 499
    .line 500
    .line 501
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 502
    .line 503
    .line 504
    move-result-object v0

    .line 505
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 506
    .line 507
    .line 508
    goto :goto_f

    .line 509
    :catch_3
    :goto_d
    move-object/from16 v1, p0

    .line 510
    .line 511
    goto :goto_e

    .line 512
    :cond_d
    const/16 v23, 0x1

    .line 513
    .line 514
    goto :goto_f

    .line 515
    :catch_4
    const/4 v7, 0x0

    .line 516
    :catch_5
    :goto_e
    const-string v0, "CreateNewCompositeDoc fail!"

    .line 517
    .line 518
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    .line 520
    .line 521
    const/4 v15, 0x0

    .line 522
    goto :goto_10

    .line 523
    :cond_e
    const/16 v23, 0x1

    .line 524
    .line 525
    const-string v0, "The path of image is empty"

    .line 526
    .line 527
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    .line 529
    .line 530
    :goto_f
    const/4 v15, 0x1

    .line 531
    :goto_10
    return v15
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
.end method

.method public o800o8O(FF)V
    .locals 2

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O〇:F

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O00:F

    .line 4
    .line 5
    const/16 v0, 0x8

    .line 6
    .line 7
    new-array v0, v0, [F

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    aput p1, v0, v1

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    aput p1, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    aput p2, v0, v1

    .line 17
    .line 18
    const/4 v1, 0x3

    .line 19
    aput p2, v0, v1

    .line 20
    .line 21
    const/4 v1, 0x4

    .line 22
    aput p1, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x5

    .line 25
    aput p1, v0, v1

    .line 26
    .line 27
    const/4 p1, 0x6

    .line 28
    aput p2, v0, p1

    .line 29
    .line 30
    const/4 p1, 0x7

    .line 31
    aput p2, v0, p1

    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇8O0〇8:[F

    .line 34
    .line 35
    new-instance p1, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string p2, "mXRadius="

    .line 41
    .line 42
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    iget p2, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O〇:F

    .line 46
    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    const-string p2, "ImageCompositeControl"

    .line 55
    .line 56
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80(Landroid/graphics/Canvas;Landroid/graphics/RectF;Ljava/lang/String;)Z
    .locals 23

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v2, p1

    .line 4
    .line 5
    move-object/from16 v0, p2

    .line 6
    .line 7
    move-object/from16 v3, p3

    .line 8
    .line 9
    const-string v4, "ImageCompositeControl"

    .line 10
    .line 11
    const/4 v5, 0x0

    .line 12
    if-eqz v2, :cond_f

    .line 13
    .line 14
    if-eqz v0, :cond_f

    .line 15
    .line 16
    if-eqz v3, :cond_f

    .line 17
    .line 18
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    .line 19
    .line 20
    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 21
    .line 22
    .line 23
    const/4 v7, 0x1

    .line 24
    iput v7, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 25
    .line 26
    iput-boolean v7, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 27
    .line 28
    invoke-static {v3, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 29
    .line 30
    .line 31
    iget-object v8, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8ooOoo〇:Ljava/util/Map;

    .line 32
    .line 33
    invoke-interface {v8, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    move-result v8

    .line 37
    if-eqz v8, :cond_0

    .line 38
    .line 39
    iget-object v8, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8ooOoo〇:Ljava/util/Map;

    .line 40
    .line 41
    invoke-interface {v8, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v8

    .line 45
    check-cast v8, Ljava/lang/Integer;

    .line 46
    .line 47
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v8

    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 v8, 0x0

    .line 53
    :goto_0
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 54
    .line 55
    if-lez v9, :cond_10

    .line 56
    .line 57
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 58
    .line 59
    if-lez v9, :cond_10

    .line 60
    .line 61
    iget v9, v0, Landroid/graphics/RectF;->left:F

    .line 62
    .line 63
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 64
    .line 65
    iget v9, v0, Landroid/graphics/RectF;->top:F

    .line 66
    .line 67
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 68
    .line 69
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    .line 70
    .line 71
    .line 72
    move-result v9

    .line 73
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 74
    .line 75
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    iput v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8:F

    .line 80
    .line 81
    rem-int/lit16 v9, v8, 0xb4

    .line 82
    .line 83
    if-nez v9, :cond_1

    .line 84
    .line 85
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 86
    .line 87
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 88
    .line 89
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 90
    .line 91
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_1
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 95
    .line 96
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 97
    .line 98
    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 99
    .line 100
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 101
    .line 102
    :goto_1
    iget-boolean v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇808〇:Z

    .line 103
    .line 104
    const/high16 v10, 0x40000000    # 2.0f

    .line 105
    .line 106
    const/4 v11, 0x0

    .line 107
    if-nez v9, :cond_4

    .line 108
    .line 109
    iget v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 110
    .line 111
    int-to-float v12, v9

    .line 112
    iget v13, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 113
    .line 114
    cmpg-float v12, v12, v13

    .line 115
    .line 116
    if-gez v12, :cond_4

    .line 117
    .line 118
    iget v12, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 119
    .line 120
    int-to-float v14, v12

    .line 121
    cmpg-float v14, v14, v0

    .line 122
    .line 123
    if-gez v14, :cond_4

    .line 124
    .line 125
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 126
    .line 127
    int-to-float v9, v9

    .line 128
    sub-float/2addr v13, v9

    .line 129
    div-float/2addr v13, v10

    .line 130
    add-float/2addr v6, v13

    .line 131
    iput v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 132
    .line 133
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 134
    .line 135
    int-to-float v9, v12

    .line 136
    sub-float/2addr v0, v9

    .line 137
    div-float/2addr v0, v10

    .line 138
    add-float/2addr v6, v0

    .line 139
    iput v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 140
    .line 141
    :try_start_0
    invoke-static/range {p3 .. p3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 142
    .line 143
    .line 144
    move-result-object v3
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 145
    if-lez v8, :cond_2

    .line 146
    .line 147
    :try_start_1
    invoke-static {v3, v8}, Lcom/intsig/utils/ImageUtil;->〇oOO8O8(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    .line 148
    .line 149
    .line 150
    move-result-object v3

    .line 151
    :cond_2
    iget-boolean v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oooo8o0〇:Z

    .line 152
    .line 153
    if-eqz v0, :cond_3

    .line 154
    .line 155
    new-instance v0, Landroid/graphics/Rect;

    .line 156
    .line 157
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 158
    .line 159
    float-to-int v8, v6

    .line 160
    iget v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 161
    .line 162
    float-to-int v9, v9

    .line 163
    float-to-int v6, v6

    .line 164
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 165
    .line 166
    .line 167
    move-result v10

    .line 168
    add-int/2addr v6, v10

    .line 169
    iget v10, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 170
    .line 171
    float-to-int v10, v10

    .line 172
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 173
    .line 174
    .line 175
    move-result v11

    .line 176
    add-int/2addr v10, v11

    .line 177
    invoke-direct {v0, v8, v9, v6, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 178
    .line 179
    .line 180
    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇80〇808〇O(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 181
    .line 182
    .line 183
    goto :goto_2

    .line 184
    :cond_3
    iget v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 185
    .line 186
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 187
    .line 188
    invoke-virtual {v2, v3, v0, v6, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 189
    .line 190
    .line 191
    :goto_2
    const/4 v5, 0x1

    .line 192
    goto :goto_4

    .line 193
    :catch_0
    move-exception v0

    .line 194
    move-object v11, v3

    .line 195
    goto :goto_3

    .line 196
    :catch_1
    move-exception v0

    .line 197
    :goto_3
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 198
    .line 199
    .line 200
    move-object v3, v11

    .line 201
    :goto_4
    if-eqz v3, :cond_11

    .line 202
    .line 203
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 204
    .line 205
    .line 206
    goto/16 :goto_f

    .line 207
    .line 208
    :cond_4
    iget v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 209
    .line 210
    int-to-float v9, v9

    .line 211
    const/high16 v12, 0x3f800000    # 1.0f

    .line 212
    .line 213
    mul-float v9, v9, v12

    .line 214
    .line 215
    iget v13, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 216
    .line 217
    int-to-float v13, v13

    .line 218
    div-float/2addr v9, v13

    .line 219
    mul-float v13, v0, v9

    .line 220
    .line 221
    iget v14, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 222
    .line 223
    cmpg-float v15, v13, v14

    .line 224
    .line 225
    if-gtz v15, :cond_5

    .line 226
    .line 227
    iget v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 228
    .line 229
    sub-float/2addr v14, v13

    .line 230
    div-float/2addr v14, v10

    .line 231
    float-to-int v9, v14

    .line 232
    int-to-float v9, v9

    .line 233
    add-float/2addr v0, v9

    .line 234
    iput v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 235
    .line 236
    float-to-int v0, v13

    .line 237
    int-to-float v0, v0

    .line 238
    iput v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 239
    .line 240
    goto :goto_5

    .line 241
    :cond_5
    div-float/2addr v14, v9

    .line 242
    iget v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 243
    .line 244
    sub-float/2addr v0, v14

    .line 245
    div-float/2addr v0, v10

    .line 246
    float-to-int v0, v0

    .line 247
    int-to-float v0, v0

    .line 248
    add-float/2addr v9, v0

    .line 249
    iput v9, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 250
    .line 251
    :goto_5
    iput-boolean v5, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 252
    .line 253
    sget-object v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo〇:[I

    .line 254
    .line 255
    array-length v9, v0

    .line 256
    move-object v13, v11

    .line 257
    move-object v14, v13

    .line 258
    const/4 v0, 0x1

    .line 259
    const/4 v10, 0x0

    .line 260
    :goto_6
    if-ge v10, v9, :cond_a

    .line 261
    .line 262
    if-eqz v13, :cond_6

    .line 263
    .line 264
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 265
    .line 266
    .line 267
    move-result v0

    .line 268
    if-nez v0, :cond_6

    .line 269
    .line 270
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 271
    .line 272
    .line 273
    :cond_6
    :try_start_2
    sget-object v0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->oo〇:[I

    .line 274
    .line 275
    aget v0, v0, v10

    .line 276
    .line 277
    iget v15, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 278
    .line 279
    mul-int v0, v0, v15

    .line 280
    .line 281
    int-to-float v0, v0

    .line 282
    iget v15, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 283
    .line 284
    div-float/2addr v0, v15

    .line 285
    float-to-int v0, v0

    .line 286
    iput v0, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 287
    .line 288
    invoke-static {v3, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 289
    .line 290
    .line 291
    move-result-object v13

    .line 292
    if-lez v8, :cond_7

    .line 293
    .line 294
    invoke-static {v13, v8}, Lcom/intsig/utils/ImageUtil;->〇oOO8O8(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    .line 295
    .line 296
    .line 297
    move-result-object v0

    .line 298
    move-object v13, v0

    .line 299
    :cond_7
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    .line 300
    .line 301
    .line 302
    move-result v0

    .line 303
    iput v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 304
    .line 305
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    .line 306
    .line 307
    .line 308
    move-result v0

    .line 309
    iput v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 310
    .line 311
    iget v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o〇:F

    .line 312
    .line 313
    mul-float v0, v0, v12

    .line 314
    .line 315
    iget v15, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 316
    .line 317
    int-to-float v15, v15

    .line 318
    div-float/2addr v0, v15

    .line 319
    move-object/from16 v22, v6

    .line 320
    .line 321
    float-to-double v5, v0

    .line 322
    const-wide v15, 0x3fefae147ae147aeL    # 0.99

    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    cmpl-double v17, v5, v15

    .line 328
    .line 329
    if-lez v17, :cond_8

    .line 330
    .line 331
    const-wide v15, 0x3ff028f5c28f5c29L    # 1.01

    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    cmpg-double v17, v5, v15

    .line 337
    .line 338
    if-gez v17, :cond_8

    .line 339
    .line 340
    move-object v0, v13

    .line 341
    goto :goto_7

    .line 342
    :cond_8
    :try_start_3
    new-instance v5, Landroid/graphics/Matrix;

    .line 343
    .line 344
    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 348
    .line 349
    .line 350
    const/16 v16, 0x0

    .line 351
    .line 352
    const/16 v17, 0x0

    .line 353
    .line 354
    iget v0, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 355
    .line 356
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 357
    .line 358
    const/16 v21, 0x1

    .line 359
    .line 360
    move-object v15, v13

    .line 361
    move/from16 v18, v0

    .line 362
    .line 363
    move/from16 v19, v6

    .line 364
    .line 365
    move-object/from16 v20, v5

    .line 366
    .line 367
    invoke-static/range {v15 .. v21}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    .line 368
    .line 369
    .line 370
    move-result-object v0
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 371
    :goto_7
    move-object v14, v0

    .line 372
    const/4 v0, 0x1

    .line 373
    goto :goto_a

    .line 374
    :catch_2
    move-exception v0

    .line 375
    goto :goto_9

    .line 376
    :catch_3
    move-exception v0

    .line 377
    goto :goto_9

    .line 378
    :catch_4
    move-exception v0

    .line 379
    goto :goto_8

    .line 380
    :catch_5
    move-exception v0

    .line 381
    :goto_8
    move-object/from16 v22, v6

    .line 382
    .line 383
    :goto_9
    new-instance v5, Ljava/lang/StringBuilder;

    .line 384
    .line 385
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 386
    .line 387
    .line 388
    const-string v6, "mBitmapWidth="

    .line 389
    .line 390
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    .line 392
    .line 393
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oo08:I

    .line 394
    .line 395
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 396
    .line 397
    .line 398
    const-string v6, "  mBitmapHeight="

    .line 399
    .line 400
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    .line 402
    .line 403
    iget v6, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->o〇0:I

    .line 404
    .line 405
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    const-string v6, " errorMsg="

    .line 409
    .line 410
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    .line 412
    .line 413
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 414
    .line 415
    .line 416
    move-result-object v0

    .line 417
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    .line 419
    .line 420
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 421
    .line 422
    .line 423
    move-result-object v0

    .line 424
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    .line 426
    .line 427
    const/4 v0, 0x0

    .line 428
    :goto_a
    if-eqz v0, :cond_9

    .line 429
    .line 430
    goto :goto_b

    .line 431
    :cond_9
    const-string v5, "try again"

    .line 432
    .line 433
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .line 435
    .line 436
    add-int/lit8 v10, v10, 0x1

    .line 437
    .line 438
    move-object/from16 v6, v22

    .line 439
    .line 440
    const/4 v5, 0x0

    .line 441
    goto/16 :goto_6

    .line 442
    .line 443
    :cond_a
    :goto_b
    if-eqz v13, :cond_b

    .line 444
    .line 445
    invoke-virtual {v13, v14}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 446
    .line 447
    .line 448
    move-result v3

    .line 449
    if-nez v3, :cond_b

    .line 450
    .line 451
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 452
    .line 453
    .line 454
    :cond_b
    if-eqz v0, :cond_e

    .line 455
    .line 456
    :try_start_4
    iget-boolean v3, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oooo8o0〇:Z

    .line 457
    .line 458
    if-eqz v3, :cond_c

    .line 459
    .line 460
    new-instance v3, Landroid/graphics/Rect;

    .line 461
    .line 462
    iget v5, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 463
    .line 464
    float-to-int v6, v5

    .line 465
    iget v7, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 466
    .line 467
    float-to-int v7, v7

    .line 468
    float-to-int v5, v5

    .line 469
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    .line 470
    .line 471
    .line 472
    move-result v8

    .line 473
    add-int/2addr v5, v8

    .line 474
    iget v8, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 475
    .line 476
    float-to-int v8, v8

    .line 477
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    .line 478
    .line 479
    .line 480
    move-result v9

    .line 481
    add-int/2addr v8, v9

    .line 482
    invoke-direct {v3, v6, v7, v5, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 483
    .line 484
    .line 485
    invoke-direct {v1, v2, v14, v3}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇80〇808〇O(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 486
    .line 487
    .line 488
    goto :goto_c

    .line 489
    :cond_c
    iget v3, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇080:F

    .line 490
    .line 491
    iget v5, v1, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇o00〇〇Oo:F

    .line 492
    .line 493
    invoke-virtual {v2, v14, v3, v5, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    .line 494
    .line 495
    .line 496
    :goto_c
    move v5, v0

    .line 497
    goto :goto_e

    .line 498
    :catch_6
    move-exception v0

    .line 499
    goto :goto_d

    .line 500
    :catch_7
    move-exception v0

    .line 501
    :goto_d
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 502
    .line 503
    .line 504
    const/4 v5, 0x0

    .line 505
    :goto_e
    if-eqz v14, :cond_d

    .line 506
    .line 507
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 508
    .line 509
    .line 510
    move-result v0

    .line 511
    if-nez v0, :cond_d

    .line 512
    .line 513
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 514
    .line 515
    .line 516
    :cond_d
    move v0, v5

    .line 517
    :cond_e
    move v5, v0

    .line 518
    goto :goto_f

    .line 519
    :cond_f
    const-string v0, "drawBitmapOnCanvas, canvas==null, or areaInfo==null, or bitmap==null !"

    .line 520
    .line 521
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    .line 523
    .line 524
    :cond_10
    const/4 v5, 0x0

    .line 525
    :cond_11
    :goto_f
    return v5
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public o〇0(Ljava/lang/String;ZZ)Z
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇:Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/autocomposite/AutoCompositePreViewActivity$CompositeProgressListener;->onStart()V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    if-eqz p2, :cond_6

    .line 16
    .line 17
    :cond_1
    const-string v0, "createNewDoc"

    .line 18
    .line 19
    const-string v2, "ImageCompositeControl"

    .line 20
    .line 21
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    goto :goto_0

    .line 34
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "_id in "

    .line 40
    .line 41
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 45
    .line 46
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    :goto_0
    move-object v6, v0

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v3, "before composite\uff1a "

    .line 60
    .line 61
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget-object v3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 65
    .line 66
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇〇888:Landroid/content/Context;

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇80〇808〇O:Landroid/net/Uri;

    .line 83
    .line 84
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 85
    .line 86
    .line 87
    move-result-wide v4

    .line 88
    invoke-static {v4, v5}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 89
    .line 90
    .line 91
    move-result-object v4

    .line 92
    const-string v0, "page_water_maker_text"

    .line 93
    .line 94
    const-string v5, "sync_image_id"

    .line 95
    .line 96
    const-string v7, "_data"

    .line 97
    .line 98
    filled-new-array {v7, v0, v5}, [Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v5

    .line 102
    const/4 v7, 0x0

    .line 103
    iget-object v8, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇O8o08O:Ljava/lang/String;

    .line 104
    .line 105
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    if-eqz v0, :cond_5

    .line 110
    .line 111
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 112
    .line 113
    .line 114
    move-result v3

    .line 115
    if-eqz v3, :cond_4

    .line 116
    .line 117
    iget-object v3, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇8o8o〇:Ljava/util/ArrayList;

    .line 118
    .line 119
    if-eqz v3, :cond_3

    .line 120
    .line 121
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oooo8o0〇(Landroid/database/Cursor;Ljava/lang/String;ZZ)Z

    .line 122
    .line 123
    .line 124
    move-result v1

    .line 125
    goto :goto_1

    .line 126
    :cond_3
    const-string p1, "Template infomation is null!"

    .line 127
    .line 128
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    goto :goto_1

    .line 132
    :cond_4
    const-string p1, "cursor != null, but cann\'t find any information about the source document!"

    .line 133
    .line 134
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 138
    .line 139
    .line 140
    goto :goto_2

    .line 141
    :cond_5
    const-string p1, "cursor == null, cann\'t find any information about the source document!"

    .line 142
    .line 143
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    :cond_6
    :goto_2
    return v1
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method public 〇8o8o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->Oooo8o0〇:Z

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O00(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O〇8O8〇008:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇O888o0o(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8ooOoo〇:Ljava/util/Map;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/Map;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-lez v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->O8ooOoo〇:Ljava/util/Map;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇O8o08O()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇oOO8O8:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O〇(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1
    const-string p1, "ImageCompositeControl"

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    invoke-static {p3}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p3

    .line 10
    new-instance v1, Ljava/io/File;

    .line 11
    .line 12
    invoke-direct {v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    .line 16
    .line 17
    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 18
    .line 19
    .line 20
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    .line 21
    .line 22
    const/16 v3, 0x50

    .line 23
    .line 24
    invoke-virtual {p2, v1, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    .line 29
    .line 30
    move-object v0, p3

    .line 31
    goto :goto_0

    .line 32
    :catch_0
    move-exception p2

    .line 33
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catch_1
    move-exception p2

    .line 38
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const-string p2, "saveCompositeBitmap, bitmap is null"

    .line 43
    .line 44
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public 〇〇8O0〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/autocomposite/ImageCompositeControl;->〇00:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;
.super Lcom/intsig/camscanner/tools/ProgressAsyncTask;
.source "BookEditFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BookSplitterTrimTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/tools/ProgressAsyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V
    .locals 4

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tools/ProgressAsyncTask;-><init>(Landroid/content/Context;)V

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 4
    invoke-virtual {p2}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    move-result-object p1

    if-eqz p1, :cond_0

    .line 5
    array-length p2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-object v1, p1, v0

    .line 6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BookSplitterTrimTask mBorders="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " roation="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    invoke-virtual {v1}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇80〇808〇O()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BookEditFragment"

    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;LOo〇O8o〇8/〇o00〇〇Oo;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->oO80([Ljava/lang/Void;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected varargs oO80([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-static {p1, v0}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->oO80(Ljava/lang/String;[[I)[I

    .line 14
    .line 15
    .line 16
    move-result-object v7

    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    const/4 p1, 0x0

    .line 24
    invoke-static {v3, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;Z)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    invoke-static {p1}, Lcom/intsig/scanner/ScannerEngine;->getImageStructPointer(I)J

    .line 29
    .line 30
    .line 31
    move-result-wide v1

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇o〇()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇80〇808〇O()I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->〇o00〇〇Oo(JLjava/lang/String;[[III[I)Ljava/util/List;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-static {p1}, Lcom/intsig/scanner/ScannerEngine;->releaseImageS(I)I

    .line 55
    .line 56
    .line 57
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->Oooo8o0〇()Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 62
    .line 63
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇80〇808〇O(Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;Ljava/util/List;)V

    .line 64
    .line 65
    .line 66
    const/4 p1, 0x0

    .line 67
    return-object p1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->〇80〇808〇O(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected 〇80〇808〇O(Ljava/lang/Void;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/tools/ProgressAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    const-string p1, "BookEditFragment"

    .line 5
    .line 6
    const-string v0, "onPostExecute"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance p1, Landroid/content/Intent;

    .line 12
    .line 13
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v0, Landroid/os/Bundle;

    .line 17
    .line 18
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, "extra_booksplittermodel"

    .line 22
    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/fragment/BookEditFragment$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 24
    .line 25
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/tools/ProgressAsyncTask;->〇o〇:Landroid/content/Context;

    .line 32
    .line 33
    check-cast v0, Landroid/app/Activity;

    .line 34
    .line 35
    const/4 v1, -0x1

    .line 36
    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

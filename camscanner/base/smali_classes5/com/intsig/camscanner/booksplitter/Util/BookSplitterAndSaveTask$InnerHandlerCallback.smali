.class Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;
.super Ljava/lang/Object;
.source "BookSplitterAndSaveTask.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InnerHandlerCallback"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;LO0〇OO8/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;-><init>(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "handleMessage what: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p1, Landroid/os/Message;->what:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v1, " , callback: "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 22
    .line 23
    invoke-static {v1}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇o〇(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "BookSplitterAndSaveTask"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 40
    .line 41
    invoke-static {v0}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇o〇(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const/4 v1, 0x0

    .line 46
    if-nez v0, :cond_0

    .line 47
    .line 48
    return v1

    .line 49
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    if-eq v0, v2, :cond_4

    .line 53
    .line 54
    const/4 v2, 0x2

    .line 55
    if-eq v0, v2, :cond_2

    .line 56
    .line 57
    const/4 p1, 0x3

    .line 58
    if-eq v0, p1, :cond_1

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 62
    .line 63
    invoke-static {p1}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇o〇(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;->Oo08()V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 72
    .line 73
    instance-of v0, p1, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 74
    .line 75
    if-eqz v0, :cond_3

    .line 76
    .line 77
    check-cast p1, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_3
    const/4 p1, 0x0

    .line 81
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 82
    .line 83
    invoke-static {v0}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇o〇(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 88
    .line 89
    invoke-static {v2}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇80〇808〇O(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Landroid/graphics/Bitmap;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 94
    .line 95
    invoke-static {v3}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇080(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    invoke-virtual {v3}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇〇808〇()I

    .line 100
    .line 101
    .line 102
    move-result v3

    .line 103
    invoke-interface {v0, v2, v3, p1}, Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;->oO80(Landroid/graphics/Bitmap;ILcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask$InnerHandlerCallback;->o0:Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;

    .line 108
    .line 109
    invoke-static {p1}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;->〇o〇(Lcom/intsig/camscanner/booksplitter/Util/BookSplitterAndSaveTask;)Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-interface {p1}, Lcom/intsig/camscanner/capture/book/IBookHandleCallBack;->〇80〇808〇O()V

    .line 114
    .line 115
    .line 116
    :goto_1
    return v1
    .line 117
.end method

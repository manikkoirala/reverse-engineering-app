.class public Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;
.super Ljava/lang/Object;
.source "BookSplitterRegion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SeparationLine"
.end annotation


# instance fields
.field private O8:Landroid/graphics/Bitmap;

.field private Oo08:F

.field private o〇0:Landroid/graphics/Paint;

.field final 〇080:I

.field private 〇o00〇〇Oo:Landroid/graphics/PointF;

.field private 〇o〇:Landroid/graphics/PointF;

.field private 〇〇888:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Paint;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x64

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇080:I

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/RectF;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888:Landroid/graphics/RectF;

    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 16
    .line 17
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->o〇0:Landroid/graphics/Paint;

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private Oo08(II)Landroid/graphics/PointF;
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888:Landroid/graphics/RectF;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 4
    .line 5
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 6
    .line 7
    const/high16 v3, 0x42c80000    # 100.0f

    .line 8
    .line 9
    sub-float v4, v2, v3

    .line 10
    .line 11
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 12
    .line 13
    sub-float v5, v1, v3

    .line 14
    .line 15
    add-float/2addr v2, v3

    .line 16
    add-float/2addr v1, v3

    .line 17
    invoke-virtual {v0, v4, v5, v2, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888:Landroid/graphics/RectF;

    .line 21
    .line 22
    int-to-float p1, p1

    .line 23
    int-to-float p2, p2

    .line 24
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 31
    .line 32
    return-object p1

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888:Landroid/graphics/RectF;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 36
    .line 37
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 38
    .line 39
    sub-float v4, v2, v3

    .line 40
    .line 41
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 42
    .line 43
    sub-float v5, v1, v3

    .line 44
    .line 45
    add-float/2addr v2, v3

    .line 46
    add-float/2addr v1, v3

    .line 47
    invoke-virtual {v0, v4, v5, v2, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888:Landroid/graphics/RectF;

    .line 51
    .line 52
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 59
    .line 60
    return-object p1

    .line 61
    :cond_1
    const/4 p1, 0x0

    .line 62
    return-object p1
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;II)Landroid/graphics/PointF;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->Oo08(II)Landroid/graphics/PointF;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇80〇808〇O(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 2
    .line 3
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888(Landroid/graphics/PointF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 7
    .line 8
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇〇888(Landroid/graphics/PointF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇80〇808〇O(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇〇888(Landroid/graphics/PointF;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 4

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [F

    .line 3
    .line 4
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    aput v1, v0, v2

    .line 8
    .line 9
    iget v1, p1, Landroid/graphics/PointF;->y:F

    .line 10
    .line 11
    const/4 v3, 0x1

    .line 12
    aput v1, v0, v3

    .line 13
    .line 14
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p3, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 18
    .line 19
    .line 20
    aget p2, v0, v2

    .line 21
    .line 22
    iput p2, p1, Landroid/graphics/PointF;->x:F

    .line 23
    .line 24
    aget p2, v0, v3

    .line 25
    .line 26
    iput p2, p1, Landroid/graphics/PointF;->y:F

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method


# virtual methods
.method public O8()Landroid/graphics/PointF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->O8:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    div-int/lit8 p1, p1, 0x2

    .line 8
    .line 9
    int-to-float p1, p1

    .line 10
    iput p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->Oo08:F

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public o〇0()Landroid/graphics/PointF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇(Landroid/graphics/Canvas;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 2
    .line 3
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 4
    .line 5
    iget v3, v0, Landroid/graphics/PointF;->y:F

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 8
    .line 9
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 10
    .line 11
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 12
    .line 13
    iget-object v6, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->o〇0:Landroid/graphics/Paint;

    .line 14
    .line 15
    move-object v1, p1

    .line 16
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->O8:Landroid/graphics/Bitmap;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo:Landroid/graphics/PointF;

    .line 22
    .line 23
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 24
    .line 25
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->Oo08:F

    .line 26
    .line 27
    sub-float/2addr v2, v3

    .line 28
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 29
    .line 30
    sub-float/2addr v1, v3

    .line 31
    const/4 v3, 0x0

    .line 32
    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->O8:Landroid/graphics/Bitmap;

    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇:Landroid/graphics/PointF;

    .line 38
    .line 39
    iget v2, v1, Landroid/graphics/PointF;->x:F

    .line 40
    .line 41
    iget v4, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->Oo08:F

    .line 42
    .line 43
    sub-float/2addr v2, v4

    .line 44
    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 45
    .line 46
    sub-float/2addr v1, v4

    .line 47
    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.class public Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;
.super Ljava/lang/Object;
.source "BookSplitterRegion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;
    }
.end annotation


# static fields
.field public static O8:I = 0xa


# instance fields
.field private 〇080:Landroid/graphics/Paint;

.field private 〇o00〇〇Oo:Landroid/graphics/Bitmap;

.field private 〇o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/graphics/Paint;

    .line 5
    .line 6
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 19
    .line 20
    const v1, -0xe64364

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 27
    .line 28
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 34
    .line 35
    const/4 v1, 0x1

    .line 36
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 40
    .line 41
    const/4 v1, 0x2

    .line 42
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    int-to-float v2, v2

    .line 47
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 48
    .line 49
    .line 50
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    const v0, 0x7f08044e

    .line 55
    .line 56
    .line 57
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o00〇〇Oo:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :catch_0
    move-exception p1

    .line 65
    const-string v0, "BookSplitterRegion"

    .line 66
    .line 67
    const-string v2, "HightlightRegion OutOfMemoryError"

    .line 68
    .line 69
    invoke-static {v0, v2, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 70
    .line 71
    .line 72
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    div-int/2addr p1, v1

    .line 79
    sput p1, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->O8:I

    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private Oo08(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Landroid/graphics/Matrix;)[F
    .locals 5

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    new-array v0, v0, [F

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->o〇0()Landroid/graphics/PointF;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-direct {p0, v1, p3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇80〇808〇O(Landroid/graphics/PointF;Landroid/graphics/Matrix;)[F

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    aget v3, v1, v2

    .line 15
    .line 16
    aput v3, v0, v2

    .line 17
    .line 18
    const/4 v3, 0x1

    .line 19
    aget v1, v1, v3

    .line 20
    .line 21
    aput v1, v0, v3

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->O8()Landroid/graphics/PointF;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇80〇808〇O(Landroid/graphics/PointF;Landroid/graphics/Matrix;)[F

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    aget v1, p1, v2

    .line 32
    .line 33
    const/4 v4, 0x6

    .line 34
    aput v1, v0, v4

    .line 35
    .line 36
    const/4 v1, 0x7

    .line 37
    aget p1, p1, v3

    .line 38
    .line 39
    aput p1, v0, v1

    .line 40
    .line 41
    invoke-virtual {p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->o〇0()Landroid/graphics/PointF;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇80〇808〇O(Landroid/graphics/PointF;Landroid/graphics/Matrix;)[F

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    aget v1, p1, v2

    .line 50
    .line 51
    const/4 v4, 0x2

    .line 52
    aput v1, v0, v4

    .line 53
    .line 54
    const/4 v1, 0x3

    .line 55
    aget p1, p1, v3

    .line 56
    .line 57
    aput p1, v0, v1

    .line 58
    .line 59
    invoke-virtual {p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->O8()Landroid/graphics/PointF;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇80〇808〇O(Landroid/graphics/PointF;Landroid/graphics/Matrix;)[F

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    aget p2, p1, v2

    .line 68
    .line 69
    const/4 p3, 0x4

    .line 70
    aput p2, v0, p3

    .line 71
    .line 72
    const/4 p2, 0x5

    .line 73
    aget p1, p1, v3

    .line 74
    .line 75
    aput p1, v0, p2

    .line 76
    .line 77
    new-instance p1, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string p2, "getBorderImpl border="

    .line 83
    .line 84
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-static {v0}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    const-string p2, "BookSplitterRegion"

    .line 99
    .line 100
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    return-object v0
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method private 〇80〇808〇O(Landroid/graphics/PointF;Landroid/graphics/Matrix;)[F
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [F

    .line 3
    .line 4
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    aput v1, v0, v2

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 11
    .line 12
    aput p1, v0, v1

    .line 13
    .line 14
    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8(Landroid/graphics/Matrix;)[[F
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x2

    .line 8
    if-ge v0, v1, :cond_0

    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    return-object p1

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v2, 0x1

    .line 19
    sub-int/2addr v0, v2

    .line 20
    new-array v1, v1, [I

    .line 21
    .line 22
    const/16 v3, 0x8

    .line 23
    .line 24
    aput v3, v1, v2

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    aput v0, v1, v2

    .line 28
    .line 29
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 30
    .line 31
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, [[F

    .line 36
    .line 37
    :goto_0
    array-length v1, v0

    .line 38
    if-ge v2, v1, :cond_1

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 41
    .line 42
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    check-cast v1, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 49
    .line 50
    add-int/lit8 v4, v2, 0x1

    .line 51
    .line 52
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    check-cast v3, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 57
    .line 58
    invoke-direct {p0, v1, v3, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->Oo08(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Landroid/graphics/Matrix;)[F

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    aput-object v1, v0, v2

    .line 63
    .line 64
    move v2, v4

    .line 65
    goto :goto_0

    .line 66
    :cond_1
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public OO0o〇〇〇〇0(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 18
    .line 19
    invoke-static {v1, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o00〇〇Oo(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO80()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080:Landroid/graphics/Paint;

    .line 4
    .line 5
    invoke-direct {v0, p1, p2, v1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/Paint;)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->oO80(Landroid/graphics/Bitmap;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o〇(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 18
    .line 19
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇o〇(Landroid/graphics/Canvas;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇〇888(II)Landroid/graphics/PointF;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_1

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;

    .line 19
    .line 20
    invoke-static {v1, p1, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;->〇080(Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;II)Landroid/graphics/PointF;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    if-eqz v1, :cond_0

    .line 25
    .line 26
    :cond_1
    return-object v1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

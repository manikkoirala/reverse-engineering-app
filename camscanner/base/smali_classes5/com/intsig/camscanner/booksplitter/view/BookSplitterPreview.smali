.class public Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;
.super Landroid/view/View;
.source "BookSplitterPreview.java"


# instance fields
.field private O8o08O8O:I

.field private OO:I

.field private OO〇00〇8oO:Z

.field private o0:Landroid/graphics/Paint;

.field private oOo0:Z

.field private oOo〇8o008:I

.field private o〇00O:Landroid/graphics/Path;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private 〇080OO8〇0:Landroid/graphics/Paint;

.field private final 〇08O〇00〇o:Landroid/graphics/Path;

.field private 〇0O:I

.field private 〇OOo8〇0:Landroid/graphics/Paint;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    new-instance p2, Landroid/graphics/Path;

    .line 5
    .line 6
    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 10
    .line 11
    const/4 p2, 0x0

    .line 12
    iput p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 13
    .line 14
    iput p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo〇8o008:I

    .line 15
    .line 16
    const/4 p2, 0x1

    .line 17
    iput-boolean p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo0:Z

    .line 18
    .line 19
    iput-boolean p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO〇00〇8oO:Z

    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080(Landroid/content/Context;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇080(Landroid/content/Context;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const v1, 0x7f060527

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO:I

    .line 20
    .line 21
    const/16 v0, 0xa

    .line 22
    .line 23
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    new-instance v1, Landroid/graphics/DashPathEffect;

    .line 28
    .line 29
    const/4 v2, 0x2

    .line 30
    new-array v2, v2, [F

    .line 31
    .line 32
    int-to-float v0, v0

    .line 33
    const/4 v3, 0x0

    .line 34
    aput v0, v2, v3

    .line 35
    .line 36
    const/4 v3, 0x1

    .line 37
    aput v0, v2, v3

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-direct {v1, v2, v0}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 44
    .line 45
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO:I

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 51
    .line 52
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 53
    .line 54
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 58
    .line 59
    const/4 v2, 0x6

    .line 60
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    int-to-float v2, v2

    .line 65
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 71
    .line 72
    .line 73
    const/16 v0, 0x14

    .line 74
    .line 75
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇0O:I

    .line 80
    .line 81
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo〇8o008:I

    .line 82
    .line 83
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇o00〇〇Oo(Landroid/content/Context;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇o00〇〇Oo(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const v1, 0x7f060060

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 23
    .line 24
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇0O:I

    .line 32
    .line 33
    mul-int/lit8 v0, v0, 0x2

    .line 34
    .line 35
    int-to-float v0, v0

    .line 36
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    iget-boolean v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo0:Z

    .line 13
    .line 14
    if-eqz v2, :cond_0

    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    const/4 v5, 0x0

    .line 18
    int-to-float v6, v0

    .line 19
    int-to-float v7, v1

    .line 20
    iget-object v8, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 21
    .line 22
    move-object v3, p1

    .line 23
    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    if-eqz v0, :cond_4

    .line 27
    .line 28
    if-nez v1, :cond_1

    .line 29
    .line 30
    goto/16 :goto_1

    .line 31
    .line 32
    :cond_1
    const/4 v2, 0x0

    .line 33
    if-le v1, v0, :cond_2

    .line 34
    .line 35
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo〇8o008:I

    .line 36
    .line 37
    int-to-float v4, v3

    .line 38
    add-float/2addr v4, v2

    .line 39
    div-int/lit8 v2, v1, 0x2

    .line 40
    .line 41
    int-to-float v2, v2

    .line 42
    sub-int v3, v0, v3

    .line 43
    .line 44
    int-to-float v3, v3

    .line 45
    move v5, v4

    .line 46
    move v4, v3

    .line 47
    move v3, v2

    .line 48
    goto :goto_0

    .line 49
    :cond_2
    div-int/lit8 v3, v0, 0x2

    .line 50
    .line 51
    int-to-float v4, v3

    .line 52
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo〇8o008:I

    .line 53
    .line 54
    int-to-float v5, v3

    .line 55
    add-float/2addr v2, v5

    .line 56
    sub-int v3, v1, v3

    .line 57
    .line 58
    int-to-float v3, v3

    .line 59
    move v5, v4

    .line 60
    :goto_0
    iget-object v6, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 61
    .line 62
    invoke-virtual {v6}, Landroid/graphics/Path;->reset()V

    .line 63
    .line 64
    .line 65
    iget-object v6, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 66
    .line 67
    invoke-virtual {v6, v5, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 68
    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 71
    .line 72
    invoke-virtual {v2, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 73
    .line 74
    .line 75
    iget-boolean v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO〇00〇8oO:Z

    .line 76
    .line 77
    if-eqz v2, :cond_3

    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇08O〇00〇o:Landroid/graphics/Path;

    .line 80
    .line 81
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 82
    .line 83
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 84
    .line 85
    .line 86
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 87
    .line 88
    if-eqz v2, :cond_4

    .line 89
    .line 90
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 91
    .line 92
    if-lez v2, :cond_4

    .line 93
    .line 94
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 95
    .line 96
    if-eqz v2, :cond_4

    .line 97
    .line 98
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇0O:I

    .line 99
    .line 100
    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    .line 101
    .line 102
    .line 103
    move-result v2

    .line 104
    float-to-int v2, v2

    .line 105
    div-int/lit8 v2, v2, 0x2

    .line 106
    .line 107
    add-int/2addr v3, v2

    .line 108
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 109
    .line 110
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 111
    .line 112
    .line 113
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 114
    .line 115
    int-to-float v4, v3

    .line 116
    iget v5, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 117
    .line 118
    add-int/2addr v5, v3

    .line 119
    int-to-float v5, v5

    .line 120
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 121
    .line 122
    .line 123
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 124
    .line 125
    invoke-virtual {v2, v4, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    .line 127
    .line 128
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 129
    .line 130
    iget v5, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 131
    .line 132
    add-int/2addr v5, v3

    .line 133
    int-to-float v5, v5

    .line 134
    invoke-virtual {v2, v5, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    .line 136
    .line 137
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 138
    .line 139
    iget v5, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 140
    .line 141
    sub-int v5, v1, v5

    .line 142
    .line 143
    sub-int/2addr v5, v3

    .line 144
    int-to-float v5, v5

    .line 145
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 146
    .line 147
    .line 148
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 149
    .line 150
    sub-int v5, v1, v3

    .line 151
    .line 152
    int-to-float v5, v5

    .line 153
    invoke-virtual {v2, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    .line 155
    .line 156
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 157
    .line 158
    iget v6, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 159
    .line 160
    add-int/2addr v6, v3

    .line 161
    int-to-float v6, v6

    .line 162
    invoke-virtual {v2, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    .line 164
    .line 165
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 166
    .line 167
    sub-int v6, v0, v3

    .line 168
    .line 169
    int-to-float v6, v6

    .line 170
    iget v7, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 171
    .line 172
    sub-int/2addr v1, v7

    .line 173
    sub-int/2addr v1, v3

    .line 174
    int-to-float v1, v1

    .line 175
    invoke-virtual {v2, v6, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 176
    .line 177
    .line 178
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 179
    .line 180
    invoke-virtual {v1, v6, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    .line 182
    .line 183
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 184
    .line 185
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 186
    .line 187
    sub-int v2, v0, v2

    .line 188
    .line 189
    sub-int/2addr v2, v3

    .line 190
    int-to-float v2, v2

    .line 191
    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    .line 193
    .line 194
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 195
    .line 196
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 197
    .line 198
    add-int/2addr v2, v3

    .line 199
    int-to-float v2, v2

    .line 200
    invoke-virtual {v1, v6, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 201
    .line 202
    .line 203
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 204
    .line 205
    invoke-virtual {v1, v6, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .line 207
    .line 208
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 209
    .line 210
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 211
    .line 212
    sub-int/2addr v0, v2

    .line 213
    sub-int/2addr v0, v3

    .line 214
    int-to-float v0, v0

    .line 215
    invoke-virtual {v1, v0, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    .line 217
    .line 218
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 219
    .line 220
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 221
    .line 222
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    .line 224
    .line 225
    :cond_4
    :goto_1
    return-void
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public setMiddleLineMargin(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo〇8o008:I

    .line 2
    .line 3
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public setMiddleLineShown(Z)V
    .locals 1
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO〇00〇8oO:Z

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO〇00〇8oO:Z

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public setShowMask(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o〇()V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 7
    .line 8
    new-instance v0, Landroid/graphics/Path;

    .line 9
    .line 10
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o〇00O:Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const/16 v1, 0x19

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->O8o08O8O:I

    .line 26
    .line 27
    const-string v0, "#FFFFFF"

    .line 28
    .line 29
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO:I

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 36
    .line 37
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 41
    .line 42
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 48
    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const/4 v2, 0x3

    .line 54
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    int-to-float v1, v1

    .line 59
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 63
    .line 64
    iget v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->OO:I

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->o0:Landroid/graphics/Paint;

    .line 70
    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    int-to-float v1, v1

    .line 80
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterPreview;->〇080OO8〇0:Landroid/graphics/Paint;

    .line 84
    .line 85
    const-string v1, "#4D000000"

    .line 86
    .line 87
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.class public Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;
.super Lcom/intsig/view/SafeImageView;
.source "BookSpliiterImageView.java"


# instance fields
.field private O0O:F

.field private O8o08O8O:Z

.field private OO〇00〇8oO:F

.field private o8oOOo:F

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

.field private oOo0:I

.field private oOo〇8o008:[[I

.field private ooo0〇〇O:Landroid/graphics/PointF;

.field private o〇00O:Landroid/graphics/Matrix;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

.field private 〇0O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

.field private 〇8〇oO〇〇8o:F

.field private 〇O〇〇O8:F

.field private 〇o0O:Landroid/graphics/RectF;

.field private 〇〇08O:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2
    .line 3
    .line 4
    new-instance p2, Landroid/graphics/Matrix;

    .line 5
    .line 6
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 10
    .line 11
    const/4 p2, 0x0

    .line 12
    iput p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO〇00〇8oO:F

    .line 13
    .line 14
    new-instance p2, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    invoke-direct {p2, v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    .line 18
    .line 19
    .line 20
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 21
    .line 22
    const/high16 p2, 0x3f800000    # 1.0f

    .line 23
    .line 24
    iput p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8〇oO〇〇8o:F

    .line 25
    .line 26
    new-instance p2, Landroid/graphics/RectF;

    .line 27
    .line 28
    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    .line 29
    .line 30
    .line 31
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇o0O:Landroid/graphics/RectF;

    .line 32
    .line 33
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇〇888(Landroid/content/Context;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OO0o〇〇〇〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "BookSpliiterImageView"

    .line 7
    .line 8
    const-string v1, "resetDisplayBorder"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Landroid/graphics/Matrix;

    .line 14
    .line 15
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 16
    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 21
    .line 22
    .line 23
    new-instance v1, Landroid/graphics/Matrix;

    .line 24
    .line 25
    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 26
    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 29
    .line 30
    invoke-virtual {p0, v2, v1}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇0(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 31
    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 34
    .line 35
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 36
    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 39
    .line 40
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8〇oO〇〇8o:F

    .line 41
    .line 42
    invoke-virtual {v2, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 43
    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 46
    .line 47
    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 48
    .line 49
    .line 50
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 51
    .line 52
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 53
    .line 54
    invoke-virtual {v2, v0, v3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->OO0o〇〇〇〇0(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private Oo08([F[F)V
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "statrPoint="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-static {p1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, " endPoint="

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-static {p2}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "BookSpliiterImageView"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 40
    .line 41
    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 45
    .line 46
    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 50
    .line 51
    new-instance v1, Landroid/graphics/PointF;

    .line 52
    .line 53
    const/4 v2, 0x0

    .line 54
    aget v3, p1, v2

    .line 55
    .line 56
    const/4 v4, 0x1

    .line 57
    aget p1, p1, v4

    .line 58
    .line 59
    invoke-direct {v1, v3, p1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 60
    .line 61
    .line 62
    new-instance p1, Landroid/graphics/PointF;

    .line 63
    .line 64
    aget v2, p2, v2

    .line 65
    .line 66
    aget p2, p2, v4

    .line 67
    .line 68
    invoke-direct {p1, v2, p2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇080(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private setBookSplitterBorderImpl([[I)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8〇oO〇〇8o:F

    .line 11
    .line 12
    invoke-virtual {v0, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->setRestBookSplitterBorder([[I)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    iput-boolean p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O8o08O8O:Z

    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
.end method

.method private setRestBookSplitterBorder([[I)V
    .locals 11

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    array-length v0, p1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    goto :goto_1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o00〇〇Oo()V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    aget-object v1, p1, v0

    .line 14
    .line 15
    const/4 v2, 0x2

    .line 16
    new-array v3, v2, [F

    .line 17
    .line 18
    aget v4, v1, v0

    .line 19
    .line 20
    int-to-float v4, v4

    .line 21
    aput v4, v3, v0

    .line 22
    .line 23
    const/4 v4, 0x1

    .line 24
    aget v5, v1, v4

    .line 25
    .line 26
    int-to-float v5, v5

    .line 27
    aput v5, v3, v4

    .line 28
    .line 29
    new-array v5, v2, [F

    .line 30
    .line 31
    const/4 v6, 0x6

    .line 32
    aget v6, v1, v6

    .line 33
    .line 34
    int-to-float v6, v6

    .line 35
    aput v6, v5, v0

    .line 36
    .line 37
    const/4 v6, 0x7

    .line 38
    aget v6, v1, v6

    .line 39
    .line 40
    int-to-float v6, v6

    .line 41
    aput v6, v5, v4

    .line 42
    .line 43
    invoke-direct {p0, v3, v5}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->Oo08([F[F)V

    .line 44
    .line 45
    .line 46
    new-array v3, v2, [F

    .line 47
    .line 48
    aget v5, v1, v2

    .line 49
    .line 50
    int-to-float v5, v5

    .line 51
    aput v5, v3, v0

    .line 52
    .line 53
    const/4 v5, 0x3

    .line 54
    aget v6, v1, v5

    .line 55
    .line 56
    int-to-float v6, v6

    .line 57
    aput v6, v3, v4

    .line 58
    .line 59
    new-array v6, v2, [F

    .line 60
    .line 61
    const/4 v7, 0x4

    .line 62
    aget v8, v1, v7

    .line 63
    .line 64
    int-to-float v8, v8

    .line 65
    aput v8, v6, v0

    .line 66
    .line 67
    const/4 v8, 0x5

    .line 68
    aget v1, v1, v8

    .line 69
    .line 70
    int-to-float v1, v1

    .line 71
    aput v1, v6, v4

    .line 72
    .line 73
    invoke-direct {p0, v3, v6}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->Oo08([F[F)V

    .line 74
    .line 75
    .line 76
    const/4 v1, 0x1

    .line 77
    :goto_0
    array-length v3, p1

    .line 78
    if-ge v1, v3, :cond_1

    .line 79
    .line 80
    aget-object v3, p1, v1

    .line 81
    .line 82
    new-array v6, v2, [F

    .line 83
    .line 84
    aget v9, v3, v2

    .line 85
    .line 86
    int-to-float v9, v9

    .line 87
    aput v9, v6, v0

    .line 88
    .line 89
    aget v9, v3, v5

    .line 90
    .line 91
    int-to-float v9, v9

    .line 92
    aput v9, v6, v4

    .line 93
    .line 94
    new-array v9, v2, [F

    .line 95
    .line 96
    aget v10, v3, v7

    .line 97
    .line 98
    int-to-float v10, v10

    .line 99
    aput v10, v9, v0

    .line 100
    .line 101
    aget v3, v3, v8

    .line 102
    .line 103
    int-to-float v3, v3

    .line 104
    aput v3, v9, v4

    .line 105
    .line 106
    invoke-direct {p0, v6, v9}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->Oo08([F[F)V

    .line 107
    .line 108
    .line 109
    add-int/lit8 v1, v1, 0x1

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_1
    :goto_1
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private 〇8o8o〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇o0O:Landroid/graphics/RectF;

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    int-to-float v2, v2

    .line 17
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    int-to-float v0, v0

    .line 22
    const/4 v3, 0x0

    .line 23
    invoke-virtual {v1, v3, v3, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇o0O:Landroid/graphics/RectF;

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private 〇〇888(Landroid/content/Context;)V
    .locals 2

    .line 1
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;-><init>(Landroid/content/Context;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->o〇0()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    int-to-float p1, p1

    .line 23
    iput p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO〇00〇8oO:F

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public OO0o〇〇(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    rem-int/lit16 v0, p1, 0x168

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    new-instance v0, Landroid/graphics/Matrix;

    .line 11
    .line 12
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 16
    .line 17
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    add-int/2addr v1, p1

    .line 27
    add-int/lit16 v1, v1, 0x168

    .line 28
    .line 29
    rem-int/lit16 v1, v1, 0x168

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 32
    .line 33
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 34
    .line 35
    .line 36
    new-instance p1, Landroid/graphics/Matrix;

    .line 37
    .line 38
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 39
    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 42
    .line 43
    invoke-virtual {p0, v2, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇0(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 44
    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 47
    .line 48
    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    .line 49
    .line 50
    .line 51
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 52
    .line 53
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8〇oO〇〇8o:F

    .line 54
    .line 55
    invoke-virtual {v2, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 56
    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 59
    .line 60
    invoke-virtual {v2, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 61
    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 66
    .line 67
    invoke-virtual {v2, v0, v3}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->OO0o〇〇〇〇0(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 71
    .line 72
    .line 73
    new-instance p1, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v0, "rotationImage newRation="

    .line 79
    .line 80
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    const-string v0, "BookSpliiterImageView"

    .line 91
    .line 92
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :cond_1
    :goto_0
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public Oooo8o0〇([[ILcom/intsig/camscanner/loadimage/RotateBitmap;F)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/view/SafeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 6
    .line 7
    .line 8
    iput-object p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->getRotaion()I

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    iput p2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo0:I

    .line 15
    .line 16
    iput p3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8〇oO〇〇8o:F

    .line 17
    .line 18
    new-instance p2, Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-direct {p2}, Landroid/graphics/Matrix;-><init>()V

    .line 21
    .line 22
    .line 23
    iget-object p3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 24
    .line 25
    invoke-virtual {p0, p3, p2}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇0(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, p2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->setBookSplitterBorderImpl([[I)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method

.method public getBookSplitterBorder()[[I
    .locals 8

    .line 1
    new-instance v0, Landroid/graphics/Matrix;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o〇00O:Landroid/graphics/Matrix;

    .line 7
    .line 8
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->O8(Landroid/graphics/Matrix;)[[F

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    return-object v0

    .line 21
    :cond_0
    array-length v1, v0

    .line 22
    const/4 v2, 0x2

    .line 23
    new-array v2, v2, [I

    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    const/16 v4, 0x8

    .line 27
    .line 28
    aput v4, v2, v3

    .line 29
    .line 30
    const/4 v3, 0x0

    .line 31
    aput v1, v2, v3

    .line 32
    .line 33
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 34
    .line 35
    invoke-static {v1, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    check-cast v1, [[I

    .line 40
    .line 41
    const/4 v2, 0x0

    .line 42
    :goto_0
    array-length v4, v0

    .line 43
    if-ge v2, v4, :cond_2

    .line 44
    .line 45
    aget-object v4, v0, v2

    .line 46
    .line 47
    const/4 v5, 0x0

    .line 48
    :goto_1
    array-length v6, v4

    .line 49
    if-ge v5, v6, :cond_1

    .line 50
    .line 51
    aget-object v6, v1, v2

    .line 52
    .line 53
    aget v7, v4, v5

    .line 54
    .line 55
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    .line 56
    .line 57
    .line 58
    move-result v7

    .line 59
    aput v7, v6, v5

    .line 60
    .line 61
    add-int/lit8 v5, v5, 0x1

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    return-object v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public getRotaion()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8〇OO0〇0o:Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->O8()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getSeparationLineList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion$SeparationLine;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->oO80()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()Z
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo0:I

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->getRotaion()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->getBookSplitterBorder()[[I

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 19
    .line 20
    if-nez v3, :cond_1

    .line 21
    .line 22
    return v1

    .line 23
    :cond_1
    if-eqz v0, :cond_7

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 26
    .line 27
    if-nez v3, :cond_2

    .line 28
    .line 29
    goto/16 :goto_2

    .line 30
    .line 31
    :cond_2
    array-length v4, v0

    .line 32
    array-length v3, v3

    .line 33
    const-string v5, "BookSpliiterImageView"

    .line 34
    .line 35
    if-eq v4, v3, :cond_3

    .line 36
    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v3, "currentBorder.length="

    .line 43
    .line 44
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    array-length v0, v0

    .line 48
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    const-string v0, " mBackupBookBorder.length="

    .line 52
    .line 53
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 57
    .line 58
    array-length v0, v0

    .line 59
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return v2

    .line 70
    :cond_3
    const/4 v3, 0x0

    .line 71
    :goto_0
    array-length v4, v0

    .line 72
    if-ge v3, v4, :cond_6

    .line 73
    .line 74
    const/4 v4, 0x0

    .line 75
    :goto_1
    aget-object v6, v0, v3

    .line 76
    .line 77
    array-length v7, v6

    .line 78
    if-ge v4, v7, :cond_5

    .line 79
    .line 80
    aget v6, v6, v4

    .line 81
    .line 82
    iget-object v7, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 83
    .line 84
    aget-object v7, v7, v3

    .line 85
    .line 86
    aget v7, v7, v4

    .line 87
    .line 88
    if-eq v6, v7, :cond_4

    .line 89
    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    .line 91
    .line 92
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    .line 94
    .line 95
    const-string v6, "currentBorder[firstIndex][secondIndex]="

    .line 96
    .line 97
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    aget-object v0, v0, v3

    .line 101
    .line 102
    aget v0, v0, v4

    .line 103
    .line 104
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    .line 115
    .line 116
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .line 118
    .line 119
    const-string v1, "mBackupBookBorder[firstIndex][secondIndex]="

    .line 120
    .line 121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    iget-object v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 125
    .line 126
    aget-object v1, v1, v3

    .line 127
    .line 128
    aget v1, v1, v4

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    return v2

    .line 141
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_5
    add-int/lit8 v3, v3, 0x1

    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_6
    return v1

    .line 148
    :cond_7
    :goto_2
    return v2
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O8o08O8O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇o〇(Landroid/graphics/Canvas;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O8o08O8O:Z

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO0o〇〇〇〇0()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O8o08O8O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    return p1

    .line 7
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    const/4 v2, 0x1

    .line 20
    if-eqz p1, :cond_4

    .line 21
    .line 22
    if-eq p1, v2, :cond_3

    .line 23
    .line 24
    const/4 v3, 0x2

    .line 25
    if-eq p1, v3, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->ooo0〇〇O:Landroid/graphics/PointF;

    .line 29
    .line 30
    if-eqz p1, :cond_5

    .line 31
    .line 32
    iget v3, p1, Landroid/graphics/PointF;->x:F

    .line 33
    .line 34
    add-float/2addr v3, v0

    .line 35
    iget v4, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇〇08O:F

    .line 36
    .line 37
    sub-float/2addr v3, v4

    .line 38
    iput v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8oOOo:F

    .line 39
    .line 40
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 41
    .line 42
    add-float/2addr p1, v1

    .line 43
    iget v4, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O0O:F

    .line 44
    .line 45
    sub-float/2addr p1, v4

    .line 46
    iput p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇O〇〇O8:F

    .line 47
    .line 48
    iget-object v4, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇o0O:Landroid/graphics/RectF;

    .line 49
    .line 50
    invoke-virtual {v4, v3, p1}, Landroid/graphics/RectF;->contains(FF)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_2

    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->ooo0〇〇O:Landroid/graphics/PointF;

    .line 57
    .line 58
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->o8oOOo:F

    .line 59
    .line 60
    iput v3, p1, Landroid/graphics/PointF;->x:F

    .line 61
    .line 62
    iget v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇O〇〇O8:F

    .line 63
    .line 64
    iput v3, p1, Landroid/graphics/PointF;->y:F

    .line 65
    .line 66
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 67
    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇0O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 70
    .line 71
    if-eqz p1, :cond_2

    .line 72
    .line 73
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->ooo0〇〇O:Landroid/graphics/PointF;

    .line 74
    .line 75
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 76
    .line 77
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 78
    .line 79
    invoke-interface {p1, v4, v3}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇Oo(FF)V

    .line 80
    .line 81
    .line 82
    :cond_2
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇〇08O:F

    .line 83
    .line 84
    iput v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O0O:F

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇0O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 88
    .line 89
    if-eqz p1, :cond_5

    .line 90
    .line 91
    invoke-interface {p1}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇08O〇00〇o()V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇080OO8〇0:Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;

    .line 96
    .line 97
    float-to-int v3, v0

    .line 98
    float-to-int v4, v1

    .line 99
    invoke-virtual {p1, v3, v4}, Lcom/intsig/camscanner/booksplitter/view/BookSplitterRegion;->〇〇888(II)Landroid/graphics/PointF;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->ooo0〇〇O:Landroid/graphics/PointF;

    .line 104
    .line 105
    if-eqz p1, :cond_5

    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇8o8o〇()V

    .line 108
    .line 109
    .line 110
    iget-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇0O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 111
    .line 112
    if-eqz p1, :cond_5

    .line 113
    .line 114
    iget-object v3, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->ooo0〇〇O:Landroid/graphics/PointF;

    .line 115
    .line 116
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 117
    .line 118
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 119
    .line 120
    invoke-interface {p1, v4, v3}, Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;->〇Oo(FF)V

    .line 121
    .line 122
    .line 123
    iput v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇〇08O:F

    .line 124
    .line 125
    iput v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O0O:F

    .line 126
    .line 127
    :cond_5
    :goto_0
    return v2
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method protected o〇0(Lcom/intsig/camscanner/loadimage/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    int-to-float v0, v0

    .line 6
    iget v1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO〇00〇8oO:F

    .line 7
    .line 8
    sub-float/2addr v0, v1

    .line 9
    sub-float/2addr v0, v1

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    int-to-float v1, v1

    .line 15
    iget v2, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO〇00〇8oO:F

    .line 16
    .line 17
    sub-float/2addr v1, v2

    .line 18
    sub-float/2addr v1, v2

    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->Oo08()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    int-to-float v2, v2

    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o00〇〇Oo()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    int-to-float v3, v3

    .line 29
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 30
    .line 31
    .line 32
    div-float v4, v0, v2

    .line 33
    .line 34
    div-float v5, v1, v3

    .line 35
    .line 36
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇o〇()Landroid/graphics/Matrix;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p2, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 48
    .line 49
    .line 50
    iget p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->OO〇00〇8oO:F

    .line 51
    .line 52
    mul-float v2, v2, v4

    .line 53
    .line 54
    sub-float/2addr v0, v2

    .line 55
    const/high16 v2, 0x40000000    # 2.0f

    .line 56
    .line 57
    div-float/2addr v0, v2

    .line 58
    add-float/2addr v0, p1

    .line 59
    mul-float v3, v3, v4

    .line 60
    .line 61
    sub-float/2addr v1, v3

    .line 62
    div-float/2addr v1, v2

    .line 63
    add-float/2addr p1, v1

    .line 64
    invoke-virtual {p2, v0, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
.end method

.method public setOnCornorChangeListener(Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->〇0O:Lcom/intsig/camscanner/view/ImageEditView$OnCornorChangeListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇80〇808〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->oOo〇8o008:[[I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->setRestBookSplitterBorder([[I)V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/booksplitter/view/BookSpliiterImageView;->O8o08O8O:Z

    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/view/View;->postInvalidate()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public final Lcom/intsig/camscanner/demoire/ImageQualityReeditUtil$Companion;
.super Ljava/lang/Object;
.source "ImageQualityReeditUtil.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/demoire/ImageQualityReeditUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/demoire/ImageQualityReeditUtil$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/util/Pair;
    .locals 16
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    const-string v2, "context"

    .line 6
    .line 7
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const-string v2, "pageIdArray"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    iget-object v2, v2, Lcom/intsig/tsapp/sync/AppConfigJson;->experience_optimization:Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    const/4 v4, 0x0

    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    iget v2, v2, Lcom/intsig/tsapp/sync/AppConfigJson$ExperienceOptimization;->silent_ocr_optimize:I

    .line 26
    .line 27
    if-nez v2, :cond_0

    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v2, 0x0

    .line 32
    :goto_0
    const v5, 0x7f130810

    .line 33
    .line 34
    .line 35
    if-eqz v2, :cond_1

    .line 36
    .line 37
    new-instance v1, Landroid/util/Pair;

    .line 38
    .line 39
    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 48
    .line 49
    .line 50
    return-object v1

    .line 51
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/db/dao/InkDao;->〇080:Lcom/intsig/camscanner/db/dao/InkDao;

    .line 52
    .line 53
    sget-object v6, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 54
    .line 55
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 56
    .line 57
    .line 58
    move-result-object v7

    .line 59
    invoke-virtual {v2, v7, v1}, Lcom/intsig/camscanner/db/dao/InkDao;->〇080(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    sget-object v7, Lcom/intsig/camscanner/db/dao/WaterMarkDao;->〇080:Lcom/intsig/camscanner/db/dao/WaterMarkDao;

    .line 64
    .line 65
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 66
    .line 67
    .line 68
    move-result-object v8

    .line 69
    invoke-virtual {v7, v8, v1}, Lcom/intsig/camscanner/db/dao/WaterMarkDao;->〇080(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 70
    .line 71
    .line 72
    move-result v7

    .line 73
    sget-object v8, Lcom/intsig/camscanner/db/dao/SignatureDao;->〇080:Lcom/intsig/camscanner/db/dao/SignatureDao;

    .line 74
    .line 75
    invoke-virtual {v6}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    invoke-virtual {v8, v6, v1}, Lcom/intsig/camscanner/db/dao/SignatureDao;->〇080(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 80
    .line 81
    .line 82
    move-result v6

    .line 83
    new-instance v8, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v9, "hasInk\uff1a"

    .line 89
    .line 90
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    const-string v9, "  hasWaterMark: "

    .line 97
    .line 98
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    const-string v9, "  hasSignature: "

    .line 105
    .line 106
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v8

    .line 116
    const-string v9, "ImageQualityReeditUtil"

    .line 117
    .line 118
    invoke-static {v9, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    sget-object v8, Lcom/intsig/camscanner/db/dao/ImageDao;->〇080:Lcom/intsig/camscanner/db/dao/ImageDao;

    .line 122
    .line 123
    invoke-static/range {p2 .. p2}, Lkotlin/collections/CollectionsKt;->OOO8o〇〇(Ljava/util/Collection;)[J

    .line 124
    .line 125
    .line 126
    move-result-object v10

    .line 127
    invoke-virtual {v8, v10}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00O0O0([J)Z

    .line 128
    .line 129
    .line 130
    move-result v8

    .line 131
    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v5

    .line 135
    const-string v10, "context.getString(R.stri\u20268c_batch_process_warning)"

    .line 136
    .line 137
    invoke-static {v5, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 145
    .line 146
    .line 147
    move-result v10

    .line 148
    const/4 v11, 0x2

    .line 149
    if-eqz v10, :cond_3

    .line 150
    .line 151
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 152
    .line 153
    .line 154
    move-result-object v10

    .line 155
    check-cast v10, Ljava/lang/Long;

    .line 156
    .line 157
    sget-object v12, Lcom/intsig/camscanner/db/dao/ImageDao;->〇080:Lcom/intsig/camscanner/db/dao/ImageDao;

    .line 158
    .line 159
    sget-object v13, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 160
    .line 161
    invoke-virtual {v13}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 162
    .line 163
    .line 164
    move-result-object v13

    .line 165
    const-string v14, "pageId"

    .line 166
    .line 167
    invoke-static {v10, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    .line 171
    .line 172
    .line 173
    move-result-wide v14

    .line 174
    invoke-virtual {v12, v13, v14, v15}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇〇0o(Landroid/content/Context;J)I

    .line 175
    .line 176
    .line 177
    move-result v10

    .line 178
    if-eq v10, v11, :cond_2

    .line 179
    .line 180
    const/4 v1, 0x1

    .line 181
    goto :goto_2

    .line 182
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    .line 183
    .line 184
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    .line 186
    .line 187
    const-string v12, "ocrType : "

    .line 188
    .line 189
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v10

    .line 199
    invoke-static {v9, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    goto :goto_1

    .line 203
    :cond_3
    const/4 v1, 0x0

    .line 204
    :goto_2
    new-instance v10, Ljava/lang/StringBuilder;

    .line 205
    .line 206
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .line 208
    .line 209
    const-string v12, "isManualOcr : "

    .line 210
    .line 211
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object v10

    .line 221
    invoke-static {v9, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    if-eqz v8, :cond_b

    .line 225
    .line 226
    if-eqz v1, :cond_b

    .line 227
    .line 228
    if-eqz v2, :cond_4

    .line 229
    .line 230
    if-eqz v7, :cond_4

    .line 231
    .line 232
    if-eqz v6, :cond_4

    .line 233
    .line 234
    const v1, 0x7f131aa5

    .line 235
    .line 236
    .line 237
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object v5

    .line 241
    const-string v1, "context.getString(R.string.cs_655_add_json15)"

    .line 242
    .line 243
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    :cond_4
    if-nez v2, :cond_5

    .line 247
    .line 248
    if-nez v7, :cond_5

    .line 249
    .line 250
    if-nez v6, :cond_5

    .line 251
    .line 252
    const v1, 0x7f131a97

    .line 253
    .line 254
    .line 255
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 256
    .line 257
    .line 258
    move-result-object v5

    .line 259
    const-string v1, "context.getString(R.string.cs_655_add_json01)"

    .line 260
    .line 261
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    .line 263
    .line 264
    const/4 v3, 0x2

    .line 265
    :cond_5
    if-eqz v2, :cond_6

    .line 266
    .line 267
    if-nez v7, :cond_6

    .line 268
    .line 269
    if-nez v6, :cond_6

    .line 270
    .line 271
    const v1, 0x7f131a98

    .line 272
    .line 273
    .line 274
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 275
    .line 276
    .line 277
    move-result-object v5

    .line 278
    const-string v1, "context.getString(R.string.cs_655_add_json02)"

    .line 279
    .line 280
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    :cond_6
    if-nez v2, :cond_7

    .line 284
    .line 285
    if-eqz v7, :cond_7

    .line 286
    .line 287
    if-nez v6, :cond_7

    .line 288
    .line 289
    const v1, 0x7f131a99

    .line 290
    .line 291
    .line 292
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 293
    .line 294
    .line 295
    move-result-object v5

    .line 296
    const-string v1, "context.getString(R.string.cs_655_add_json03)"

    .line 297
    .line 298
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    .line 300
    .line 301
    :cond_7
    if-nez v2, :cond_8

    .line 302
    .line 303
    if-nez v7, :cond_8

    .line 304
    .line 305
    if-eqz v6, :cond_8

    .line 306
    .line 307
    const v1, 0x7f131a9a

    .line 308
    .line 309
    .line 310
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 311
    .line 312
    .line 313
    move-result-object v5

    .line 314
    const-string v1, "context.getString(R.string.cs_655_add_json04)"

    .line 315
    .line 316
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    .line 318
    .line 319
    :cond_8
    if-eqz v2, :cond_9

    .line 320
    .line 321
    if-eqz v7, :cond_9

    .line 322
    .line 323
    if-nez v6, :cond_9

    .line 324
    .line 325
    const v1, 0x7f131a9b

    .line 326
    .line 327
    .line 328
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 329
    .line 330
    .line 331
    move-result-object v5

    .line 332
    const-string v1, "context.getString(R.string.cs_655_add_json05)"

    .line 333
    .line 334
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 335
    .line 336
    .line 337
    :cond_9
    if-nez v2, :cond_a

    .line 338
    .line 339
    if-eqz v7, :cond_a

    .line 340
    .line 341
    if-eqz v6, :cond_a

    .line 342
    .line 343
    const v1, 0x7f131aa3

    .line 344
    .line 345
    .line 346
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 347
    .line 348
    .line 349
    move-result-object v5

    .line 350
    const-string v1, "context.getString(R.string.cs_655_add_json13)"

    .line 351
    .line 352
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    .line 354
    .line 355
    :cond_a
    if-eqz v2, :cond_15

    .line 356
    .line 357
    if-nez v7, :cond_15

    .line 358
    .line 359
    if-eqz v6, :cond_15

    .line 360
    .line 361
    const v1, 0x7f131a9c

    .line 362
    .line 363
    .line 364
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 365
    .line 366
    .line 367
    move-result-object v5

    .line 368
    const-string v0, "context.getString(R.string.cs_655_add_json06)"

    .line 369
    .line 370
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    goto/16 :goto_4

    .line 374
    .line 375
    :cond_b
    if-eqz v2, :cond_c

    .line 376
    .line 377
    if-eqz v7, :cond_c

    .line 378
    .line 379
    if-eqz v6, :cond_c

    .line 380
    .line 381
    const v1, 0x7f131aa4

    .line 382
    .line 383
    .line 384
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 385
    .line 386
    .line 387
    move-result-object v5

    .line 388
    const-string v1, "context.getString(R.string.cs_655_add_json14)"

    .line 389
    .line 390
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    :cond_c
    if-nez v2, :cond_d

    .line 394
    .line 395
    if-nez v7, :cond_d

    .line 396
    .line 397
    if-nez v6, :cond_d

    .line 398
    .line 399
    const-string v5, ""

    .line 400
    .line 401
    goto :goto_3

    .line 402
    :cond_d
    const/4 v4, 0x3

    .line 403
    :goto_3
    if-eqz v2, :cond_e

    .line 404
    .line 405
    if-nez v7, :cond_e

    .line 406
    .line 407
    if-nez v6, :cond_e

    .line 408
    .line 409
    const v1, 0x7f131a9d

    .line 410
    .line 411
    .line 412
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 413
    .line 414
    .line 415
    move-result-object v5

    .line 416
    const-string v1, "context.getString(R.string.cs_655_add_json07)"

    .line 417
    .line 418
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 419
    .line 420
    .line 421
    :cond_e
    if-nez v2, :cond_f

    .line 422
    .line 423
    if-eqz v7, :cond_f

    .line 424
    .line 425
    if-nez v6, :cond_f

    .line 426
    .line 427
    const v1, 0x7f131a9e

    .line 428
    .line 429
    .line 430
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 431
    .line 432
    .line 433
    move-result-object v5

    .line 434
    const-string v1, "context.getString(R.string.cs_655_add_json08)"

    .line 435
    .line 436
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    .line 438
    .line 439
    :cond_f
    if-nez v2, :cond_10

    .line 440
    .line 441
    if-nez v7, :cond_10

    .line 442
    .line 443
    if-eqz v6, :cond_10

    .line 444
    .line 445
    const v1, 0x7f131a9f    # 1.9553474E38f

    .line 446
    .line 447
    .line 448
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 449
    .line 450
    .line 451
    move-result-object v5

    .line 452
    const-string v1, "context.getString(R.string.cs_655_add_json09)"

    .line 453
    .line 454
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    :cond_10
    if-eqz v2, :cond_11

    .line 458
    .line 459
    if-eqz v7, :cond_11

    .line 460
    .line 461
    if-nez v6, :cond_11

    .line 462
    .line 463
    const v1, 0x7f131aa0

    .line 464
    .line 465
    .line 466
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 467
    .line 468
    .line 469
    move-result-object v5

    .line 470
    const-string v1, "context.getString(R.string.cs_655_add_json10)"

    .line 471
    .line 472
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 473
    .line 474
    .line 475
    :cond_11
    if-eqz v2, :cond_12

    .line 476
    .line 477
    if-nez v7, :cond_12

    .line 478
    .line 479
    if-eqz v6, :cond_12

    .line 480
    .line 481
    const v1, 0x7f131aa1

    .line 482
    .line 483
    .line 484
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 485
    .line 486
    .line 487
    move-result-object v5

    .line 488
    const-string v1, "context.getString(R.string.cs_655_add_json11)"

    .line 489
    .line 490
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 491
    .line 492
    .line 493
    :cond_12
    const-string v1, "context.getString(R.string.cs_655_add_json12)"

    .line 494
    .line 495
    const v3, 0x7f131aa2

    .line 496
    .line 497
    .line 498
    if-nez v2, :cond_13

    .line 499
    .line 500
    if-eqz v7, :cond_13

    .line 501
    .line 502
    if-eqz v6, :cond_13

    .line 503
    .line 504
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 505
    .line 506
    .line 507
    move-result-object v5

    .line 508
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 509
    .line 510
    .line 511
    :cond_13
    if-nez v2, :cond_14

    .line 512
    .line 513
    if-eqz v7, :cond_14

    .line 514
    .line 515
    if-eqz v6, :cond_14

    .line 516
    .line 517
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 518
    .line 519
    .line 520
    move-result-object v5

    .line 521
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 522
    .line 523
    .line 524
    :cond_14
    move v3, v4

    .line 525
    :cond_15
    :goto_4
    new-instance v0, Landroid/util/Pair;

    .line 526
    .line 527
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 528
    .line 529
    .line 530
    move-result-object v1

    .line 531
    invoke-direct {v0, v5, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 532
    .line 533
    .line 534
    return-object v0
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
.end method

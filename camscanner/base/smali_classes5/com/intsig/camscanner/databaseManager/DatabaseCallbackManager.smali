.class public Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;
.super Ljava/lang/Object;
.source "DatabaseCallbackManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;,
        Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseCallbackManagerImpl;
    }
.end annotation


# instance fields
.field private final 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(LO88〇〇o0O/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;-><init>()V

    return-void
.end method

.method public static 〇o00〇〇Oo()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseCallbackManagerImpl;->〇080()Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public delete(Landroid/net/Uri;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;

    .line 27
    .line 28
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;->〇080(Landroid/net/Uri;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
.end method

.method public insert(Landroid/net/Uri;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;

    .line 27
    .line 28
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;->〇o00〇〇Oo(Landroid/net/Uri;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
.end method

.method public update(Landroid/net/Uri;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;

    .line 27
    .line 28
    invoke-interface {v1, p1}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;->〇o〇(Landroid/net/Uri;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
.end method

.method public 〇080(Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public 〇o〇(Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager$DatabaseChangeListener;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackManager;->〇080:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class public Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;
.super Ljava/lang/Object;
.source "LongImageStitchClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/Client/LongImageStitchClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LongImageWaterMark"
.end annotation


# instance fields
.field private final O8:F

.field private OO0o〇〇〇〇0:I

.field private Oo08:I

.field private oO80:Landroid/graphics/Paint;

.field private o〇0:I

.field private final 〇080:I

.field private 〇80〇808〇O:Landroid/graphics/Paint;

.field private 〇8o8o〇:Ljava/lang/String;

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:F

.field private final 〇〇888:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const v0, -0xdededf

    .line 5
    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇080:I

    .line 8
    .line 9
    const v1, -0x232324

    .line 10
    .line 11
    .line 12
    iput v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇o00〇〇Oo:I

    .line 13
    .line 14
    const/high16 v2, 0x420c0000    # 35.0f

    .line 15
    .line 16
    iput v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇o〇:F

    .line 17
    .line 18
    const v3, 0x3e6a0ea1

    .line 19
    .line 20
    .line 21
    iput v3, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->O8:F

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageInfo;->〇〇888()I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    iput v4, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 28
    .line 29
    const/16 v4, 0x8

    .line 30
    .line 31
    iput v4, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->OO0o〇〇〇〇0:I

    .line 32
    .line 33
    const-string v4, "CamScanner"

    .line 34
    .line 35
    iput-object v4, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇8o8o〇:Ljava/lang/String;

    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇〇888:Landroid/content/Context;

    .line 38
    .line 39
    new-instance p1, Landroid/graphics/Paint;

    .line 40
    .line 41
    const/4 v4, 0x1

    .line 42
    invoke-direct {p1, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 46
    .line 47
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    .line 49
    .line 50
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 51
    .line 52
    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 56
    .line 57
    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setDither(Z)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 61
    .line 62
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 63
    .line 64
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 68
    .line 69
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    .line 70
    .line 71
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 72
    .line 73
    .line 74
    iget p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 75
    .line 76
    int-to-float p1, p1

    .line 77
    mul-float p1, p1, v3

    .line 78
    .line 79
    float-to-int p1, p1

    .line 80
    iput p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->o〇0:I

    .line 81
    .line 82
    new-instance p1, Landroid/graphics/Paint;

    .line 83
    .line 84
    invoke-direct {p1, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 85
    .line 86
    .line 87
    iput-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 88
    .line 89
    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 93
    .line 94
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 95
    .line 96
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 100
    .line 101
    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 102
    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 105
    .line 106
    const/high16 v0, 0x3f800000    # 1.0f

    .line 107
    .line 108
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method private O8()Landroid/graphics/Bitmap;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇〇888:Landroid/content/Context;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->o〇0:I

    .line 6
    .line 7
    const/4 v3, -0x1

    .line 8
    const v4, 0x7f080561

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v4, v1, v2, v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇〇8O0〇8(Landroid/content/Context;IIII)Landroid/graphics/Bitmap;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-lez v1, :cond_2

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-gtz v1, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    iget v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 35
    .line 36
    if-eq v1, v2, :cond_1

    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    :cond_1
    new-instance v1, Landroid/graphics/Canvas;

    .line 43
    .line 44
    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 48
    .line 49
    .line 50
    move-result v2

    .line 51
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇o〇(Landroid/graphics/Canvas;I)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 55
    .line 56
    .line 57
    return-object v0

    .line 58
    :cond_2
    :goto_0
    const-string v0, "LongImageStitchClient"

    .line 59
    .line 60
    const-string v1, "waterMarkBitmap == null"

    .line 61
    .line 62
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    const/4 v0, 0x0

    .line 66
    return-object v0
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method private Oo08(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 9

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 2
    .line 3
    int-to-float v0, v0

    .line 4
    const/high16 v1, 0x3f800000    # 1.0f

    .line 5
    .line 6
    mul-float v0, v0, v1

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    int-to-float v2, v2

    .line 13
    mul-float v0, v0, v2

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    int-to-float v2, v2

    .line 20
    div-float/2addr v0, v2

    .line 21
    float-to-int v6, v0

    .line 22
    new-instance v7, Landroid/graphics/Matrix;

    .line 23
    .line 24
    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 25
    .line 26
    .line 27
    iget v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 28
    .line 29
    int-to-float v0, v0

    .line 30
    mul-float v0, v0, v1

    .line 31
    .line 32
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    int-to-float v1, v1

    .line 37
    div-float/2addr v0, v1

    .line 38
    invoke-virtual {v7, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 39
    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    const/4 v4, 0x0

    .line 43
    iget v5, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 44
    .line 45
    const/4 v8, 0x1

    .line 46
    move-object v2, p1

    .line 47
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    .line 53
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    if-nez v1, :cond_0

    .line 58
    .line 59
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 60
    .line 61
    .line 62
    move-object p1, v0

    .line 63
    :cond_0
    return-object p1
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->o〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇o〇(Landroid/graphics/Canvas;I)V
    .locals 8

    .line 1
    new-instance v0, Landroid/graphics/Paint$FontMetrics;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint$FontMetrics;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->〇O8o08O(Landroid/graphics/Paint$FontMetrics;Landroid/graphics/Paint;)F

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇8o8o〇:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇8o8o〇:Ljava/lang/String;

    .line 20
    .line 21
    iget v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 22
    .line 23
    int-to-float v3, v2

    .line 24
    cmpl-float v0, v0, v3

    .line 25
    .line 26
    if-lez v0, :cond_0

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 29
    .line 30
    int-to-float v2, v2

    .line 31
    const-string v3, "..."

    .line 32
    .line 33
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 34
    .line 35
    .line 36
    move-result v3

    .line 37
    sub-float/2addr v2, v3

    .line 38
    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇〇888(Ljava/lang/String;Landroid/graphics/Paint;F)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    .line 43
    .line 44
    iget v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 45
    .line 46
    int-to-float v2, v2

    .line 47
    int-to-float p2, p2

    .line 48
    const/4 v3, 0x0

    .line 49
    invoke-direct {v0, v3, v3, v2, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 50
    .line 51
    .line 52
    iget-object p2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 53
    .line 54
    invoke-static {v1, v0, p1, p2}, Lcom/intsig/camscanner/watermark/WaterMarkUtil;->〇8o8o〇(Ljava/lang/String;Landroid/graphics/RectF;Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 55
    .line 56
    .line 57
    new-instance p2, Landroid/graphics/Rect;

    .line 58
    .line 59
    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->oO80:Landroid/graphics/Paint;

    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 65
    .line 66
    .line 67
    move-result v2

    .line 68
    const/4 v3, 0x0

    .line 69
    invoke-virtual {v0, v1, v3, v2, p2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 70
    .line 71
    .line 72
    iget v0, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->o〇0:I

    .line 73
    .line 74
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    add-int/2addr v0, v1

    .line 79
    div-int/lit8 v0, v0, 0x2

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇〇888:Landroid/content/Context;

    .line 82
    .line 83
    const/4 v2, 0x4

    .line 84
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    add-int/2addr v0, v1

    .line 89
    iget v1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->Oo08:I

    .line 90
    .line 91
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    .line 92
    .line 93
    .line 94
    move-result v2

    .line 95
    sub-int/2addr v1, v2

    .line 96
    div-int/lit8 v1, v1, 0x2

    .line 97
    .line 98
    iget v2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->OO0o〇〇〇〇0:I

    .line 99
    .line 100
    sub-int/2addr v1, v2

    .line 101
    int-to-float v3, v1

    .line 102
    int-to-float v6, v0

    .line 103
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    .line 104
    .line 105
    .line 106
    move-result p2

    .line 107
    add-int/2addr v1, p2

    .line 108
    iget p2, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->OO0o〇〇〇〇0:I

    .line 109
    .line 110
    mul-int/lit8 p2, p2, 0x2

    .line 111
    .line 112
    add-int/2addr v1, p2

    .line 113
    int-to-float v5, v1

    .line 114
    iget-object v7, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇80〇808〇O:Landroid/graphics/Paint;

    .line 115
    .line 116
    move-object v2, p1

    .line 117
    move v4, v6

    .line 118
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

.method private 〇〇888(Ljava/lang/String;Landroid/graphics/Paint;F)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    :goto_0
    if-lez v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {p2, p1, v1, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    cmpg-float v2, v2, p3

    .line 13
    .line 14
    if-gez v2, :cond_0

    .line 15
    .line 16
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    goto :goto_1

    .line 21
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    :goto_1
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method


# virtual methods
.method public o〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method 〇o00〇〇Oo(JI)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "addBottomWaterMark topPosition="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "LongImageStitchClient"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/Client/LongImageStitchClient$LongImageWaterMark;->O8()Landroid/graphics/Bitmap;

    .line 24
    .line 25
    .line 26
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    goto :goto_0

    .line 28
    :catch_0
    move-exception v0

    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    :goto_0
    if-nez v0, :cond_0

    .line 34
    .line 35
    const-string p1, "bitmap == null"

    .line 36
    .line 37
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    const/4 v1, 0x0

    .line 42
    invoke-static {p1, p2, v0, v1, p3}, Lcom/intsig/nativelib/LongImageStitch;->AddBitmap(JLandroid/graphics/Bitmap;II)I

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

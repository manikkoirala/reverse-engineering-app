.class public final Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy$Companion;
.super Ljava/lang/Object;
.source "PerformanceMonitorStrategy.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy;->〇080()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o00〇〇Oo()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy$Companion;->〇080()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_6

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/util/ProcessUtil;->〇080:Lcom/intsig/camscanner/util/ProcessUtil;

    .line 8
    .line 9
    invoke-static {}, Landroid/os/Process;->myPid()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/util/ProcessUtil;->o〇0(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const/4 v1, 0x0

    .line 18
    const/4 v2, 0x2

    .line 19
    const/4 v3, 0x1

    .line 20
    const/4 v4, 0x0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const-string v5, ":batch_engine"

    .line 24
    .line 25
    invoke-static {v0, v5, v4, v2, v1}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v5

    .line 29
    if-ne v5, v3, :cond_0

    .line 30
    .line 31
    const/4 v5, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v5, 0x0

    .line 34
    :goto_0
    if-eqz v5, :cond_1

    .line 35
    .line 36
    const-string v0, "batch_engine"

    .line 37
    .line 38
    goto :goto_3

    .line 39
    :cond_1
    if-eqz v0, :cond_2

    .line 40
    .line 41
    const-string v5, ":engine"

    .line 42
    .line 43
    invoke-static {v0, v5, v4, v2, v1}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v5

    .line 47
    if-ne v5, v3, :cond_2

    .line 48
    .line 49
    const/4 v5, 0x1

    .line 50
    goto :goto_1

    .line 51
    :cond_2
    const/4 v5, 0x0

    .line 52
    :goto_1
    if-eqz v5, :cond_3

    .line 53
    .line 54
    const-string v0, "engine"

    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_3
    if-eqz v0, :cond_4

    .line 58
    .line 59
    const-string v5, ":ocr_engine"

    .line 60
    .line 61
    invoke-static {v0, v5, v4, v2, v1}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-ne v0, v3, :cond_4

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_4
    const/4 v3, 0x0

    .line 69
    :goto_2
    if-eqz v3, :cond_5

    .line 70
    .line 71
    const-string v0, "ocr_engine"

    .line 72
    .line 73
    goto :goto_3

    .line 74
    :cond_5
    const-string v0, ""

    .line 75
    .line 76
    :goto_3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy$Companion;->〇o〇(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :cond_6
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public final 〇o〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/debug/strategy/PerformanceMonitorStrategy;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

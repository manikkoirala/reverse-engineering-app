.class public Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
.super Ljava/lang/Object;
.source "ParcelDocInfo.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/datastruct/ParcelDocInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public O8o08O8O:Ljava/lang/String;

.field public OO:Ljava/lang/String;

.field public OO〇00〇8oO:Z

.field public o0:J

.field public o8〇OO0〇0o:[J

.field public oOo0:I

.field public oOo〇8o008:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public o〇00O:Z

.field public 〇080OO8〇0:Ljava/lang/String;

.field public 〇08O〇00〇o:Z

.field public 〇0O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

.field public 〇8〇oO〇〇8o:I

.field public 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 2
    iput-wide v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    const/4 v0, 0x0

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 5
    iput-wide v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    const/4 v0, 0x0

    .line 6
    iput v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 7
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 8
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 9
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 10
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 11
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o〇00O:Z

    .line 12
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇080OO8〇0:Ljava/lang/String;

    .line 14
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 15
    const-class v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    iput-boolean v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇8〇oO〇〇8o:I

    .line 20
    const-class v0, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    iput-object p1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇0O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 5
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 1
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-lez v1, :cond_0

    .line 17
    .line 18
    new-instance v1, Ljava/util/ArrayList;

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 21
    .line 22
    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    move-object v1, v2

    .line 27
    :goto_0
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    array-length v3, v1

    .line 34
    if-lez v3, :cond_1

    .line 35
    .line 36
    array-length v2, v1

    .line 37
    new-array v2, v2, [J

    .line 38
    .line 39
    array-length v3, v1

    .line 40
    const/4 v4, 0x0

    .line 41
    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42
    .line 43
    .line 44
    :cond_1
    iput-object v2, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 45
    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 2
    .line 3
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 19
    .line 20
    .line 21
    iget-boolean v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o〇00O:Z

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇080OO8〇0:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 39
    .line 40
    .line 41
    iget v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    .line 45
    .line 46
    iget-boolean v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 54
    .line 55
    .line 56
    iget v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇8〇oO〇〇8o:I

    .line 57
    .line 58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇0O:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 62
    .line 63
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
.end method

.method public 〇080(Landroid/content/Context;)V
    .locals 13

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-ltz v4, :cond_0

    .line 8
    .line 9
    cmp-long v4, v0, v2

    .line 10
    .line 11
    if-lez v4, :cond_4

    .line 12
    .line 13
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_4

    .line 18
    .line 19
    :cond_0
    const-string v0, "insertImage mDocId "

    .line 20
    .line 21
    const-string v1, "ParcelDocInfo"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 37
    .line 38
    const/4 v3, 0x1

    .line 39
    const/4 v4, 0x0

    .line 40
    invoke-static {v0, v2, v3, v4}, Lcom/intsig/camscanner/util/Util;->O8ooOoo〇(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 45
    .line 46
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 47
    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    .line 54
    new-instance v0, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 55
    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 57
    .line 58
    iget-object v4, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 59
    .line 60
    const/4 v5, 0x0

    .line 61
    const/4 v6, 0x0

    .line 62
    iget v7, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 63
    .line 64
    iget-boolean v8, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 65
    .line 66
    move-object v2, v0

    .line 67
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)V

    .line 68
    .line 69
    .line 70
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    goto :goto_0

    .line 75
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 76
    .line 77
    iget-object v3, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 78
    .line 79
    iget-object v4, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 80
    .line 81
    iget-object v5, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 82
    .line 83
    const/4 v6, 0x0

    .line 84
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v7

    .line 88
    const/4 v8, 0x0

    .line 89
    const/4 v9, 0x0

    .line 90
    iget v10, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 91
    .line 92
    const/4 v11, 0x0

    .line 93
    sget-object v12, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 94
    .line 95
    move-object v2, v0

    .line 96
    invoke-direct/range {v2 .. v12}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 97
    .line 98
    .line 99
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    :goto_0
    if-nez p1, :cond_3

    .line 104
    .line 105
    const-string p1, "url == null"

    .line 106
    .line 107
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    return-void

    .line 111
    :cond_3
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 112
    .line 113
    .line 114
    move-result-wide v0

    .line 115
    iput-wide v0, p0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 116
    .line 117
    :cond_4
    return-void
.end method

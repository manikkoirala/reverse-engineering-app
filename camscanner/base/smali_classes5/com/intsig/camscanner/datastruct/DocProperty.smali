.class public Lcom/intsig/camscanner/datastruct/DocProperty;
.super Ljava/lang/Object;
.source "DocProperty.java"


# instance fields
.field public O8:Ljava/lang/String;

.field public O8ooOoo〇:Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field public OO0o〇〇:Ljava/lang/String;

.field public OO0o〇〇〇〇0:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

.field public Oo08:I

.field public OoO8:Ljava/lang/String;

.field public Oooo8o0〇:I

.field public O〇8O8〇008:I

.field public o800o8O:Ljava/lang/String;

.field public oO80:Z

.field public oo88o8O:J

.field public o〇0:Ljava/lang/String;

.field public o〇O8〇〇o:Lcom/intsig/camscanner/card_photo/PayExtraInfo;

.field public 〇00:I

.field public 〇080:Ljava/lang/String;

.field public 〇0〇O0088o:I

.field public 〇80〇808〇O:Z

.field public 〇8o8o〇:I

.field private 〇O00:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

.field public 〇O888o0o:I

.field public 〇O8o08O:I

.field public 〇O〇:Ljava/lang/String;

.field public 〇o00〇〇Oo:I

.field public 〇oOO8O8:Z

.field public 〇oo〇:Z

.field public 〇o〇:Ljava/lang/String;

.field public 〇〇808〇:Ljava/lang/String;

.field public 〇〇888:Ljava/lang/String;

.field public 〇〇8O0〇8:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V
    .locals 12

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    .line 5
    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o00〇〇Oo:I

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o〇:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->O8:Ljava/lang/String;

    const/4 v1, 0x0

    .line 10
    iput v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->Oo08:I

    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->o〇0:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇888:Ljava/lang/String;

    .line 13
    iput-boolean v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->oO80:Z

    .line 14
    iput-boolean v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇80〇808〇O:Z

    .line 15
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 16
    iput v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O8o08O:I

    .line 17
    iput v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->Oooo8o0〇:I

    const-string v0, "non_preset"

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇808〇:Ljava/lang/String;

    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇oo〇:Z

    .line 20
    iput-boolean v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇oOO8O8:Z

    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇080:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o〇:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->O8:Ljava/lang/String;

    .line 24
    iput p4, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->Oo08:I

    .line 25
    iput-object p5, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->o〇0:Ljava/lang/String;

    .line 26
    iput-object p6, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇888:Ljava/lang/String;

    .line 27
    iput-boolean p7, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->oO80:Z

    .line 28
    iput p8, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇8o8o〇:I

    .line 29
    iput-boolean p9, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇80〇808〇O:Z

    .line 30
    iput-object p10, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 31
    iput-object p11, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->OO0o〇〇:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 12

    const/4 v8, 0x0

    .line 4
    sget-object v10, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 12

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1
    sget-object v10, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    move/from16 v7, p4

    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)V
    .locals 12

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    .line 2
    sget-object v10, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;)V
    .locals 12

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    .line 3
    sget-object v10, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "DocProperty{title=\'"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇080:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x27

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v2, ", teamToken=\'"

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o〇:Ljava/lang/String;

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v2, ", parentSyncId=\'"

    .line 35
    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->O8:Ljava/lang/String;

    .line 40
    .line 41
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v2, ", docPermi="

    .line 48
    .line 49
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    iget v2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->Oo08:I

    .line 53
    .line 54
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    const-string v2, ", createrUid=\'"

    .line 58
    .line 59
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    iget-object v2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->o〇0:Ljava/lang/String;

    .line 63
    .line 64
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string v2, ", pdfPath=\'"

    .line 71
    .line 72
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇888:Ljava/lang/String;

    .line 76
    .line 77
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string v1, ", isNameCardDoc="

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    iget-boolean v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->oO80:Z

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v1, ", isOfflineFolder="

    .line 94
    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    iget-boolean v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇80〇808〇O:Z

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    const-string v1, ", mOp="

    .line 104
    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    iget-object v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 109
    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    const-string v1, ", type="

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    iget v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇8o8o〇:I

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    const-string v1, ", property="

    .line 124
    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->OO0o〇〇:Ljava/lang/String;

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const/16 v1, 0x7d

    .line 134
    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    return-object v0
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇080()Lcom/intsig/camscanner/capture/scene/CaptureSceneData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O00:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O00:Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.class public Lcom/intsig/camscanner/datastruct/TeamDocInfo;
.super Ljava/lang/Object;
.source "TeamDocInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/datastruct/TeamDocInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public O8o08O8O:I

.field public OO:Ljava/lang/String;

.field public o0:Ljava/lang/String;

.field public o〇00O:I

.field public 〇080OO8〇0:Z

.field public 〇08O〇00〇o:I

.field public 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/TeamDocInfo$1;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/TeamDocInfo$1;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇08O〇00〇o:I

    const/4 v1, 0x0

    .line 11
    iput v1, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o〇00O:I

    .line 12
    iput v1, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->O8o08O8O:I

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o0:Ljava/lang/String;

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 15
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->OO:Ljava/lang/String;

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇08O〇00〇o:I

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o〇00O:I

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->O8o08O8O:I

    new-array v0, v0, [Z

    .line 19
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    aget-boolean p1, v0, v1

    .line 20
    iput-boolean p1, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇080OO8〇0:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o0:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->OO:Ljava/lang/String;

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇08O〇00〇o:I

    .line 6
    iput p5, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o〇00O:I

    .line 7
    iput p6, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->O8o08O8O:I

    .line 8
    iput-boolean p7, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇080OO8〇0:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->OO:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇08O〇00〇o:I

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    iget p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->o〇00O:I

    .line 22
    .line 23
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    .line 25
    .line 26
    iget p2, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->O8o08O8O:I

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    const/4 p2, 0x1

    .line 32
    new-array p2, p2, [Z

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    iget-boolean v1, p0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;->〇080OO8〇0:Z

    .line 36
    .line 37
    aput-boolean v1, p2, v0

    .line 38
    .line 39
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

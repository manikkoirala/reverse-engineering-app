.class public final Lcom/intsig/camscanner/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f0a0000

.field public static final BOTTOM:I = 0x7f0a0001

.field public static final BOTTOM_END:I = 0x7f0a0002

.field public static final BOTTOM_START:I = 0x7f0a0003

.field public static final BaseQuickAdapter_databinding_support:I = 0x7f0a0004

.field public static final BaseQuickAdapter_dragging_support:I = 0x7f0a0005

.field public static final BaseQuickAdapter_swiping_support:I = 0x7f0a0006

.field public static final BaseQuickAdapter_viewholder_support:I = 0x7f0a0007

.field public static final CTRL:I = 0x7f0a0008

.field public static final FUNCTION:I = 0x7f0a0009

.field public static final LEFT:I = 0x7f0a000a

.field public static final LinearLayout01:I = 0x7f0a000b

.field public static final LinearLayout1:I = 0x7f0a000c

.field public static final META:I = 0x7f0a000d

.field public static final NO_DEBUG:I = 0x7f0a000e

.field public static final RIGHT:I = 0x7f0a000f

.field public static final RelativeLayout01:I = 0x7f0a0010

.field public static final SHIFT:I = 0x7f0a0011

.field public static final SHOW_ALL:I = 0x7f0a0012

.field public static final SHOW_PATH:I = 0x7f0a0013

.field public static final SHOW_PROGRESS:I = 0x7f0a0014

.field public static final SYM:I = 0x7f0a0015

.field public static final TOP:I = 0x7f0a0016

.field public static final TOP_END:I = 0x7f0a0017

.field public static final TOP_START:I = 0x7f0a0018

.field public static final acb_compress_rate:I = 0x7f0a0019

.field public static final acb_java_crash:I = 0x7f0a001a

.field public static final acb_native_crash:I = 0x7f0a001b

.field public static final accelerate:I = 0x7f0a001c

.field public static final accessibility_action_clickable_span:I = 0x7f0a001d

.field public static final accessibility_custom_action_0:I = 0x7f0a001e

.field public static final accessibility_custom_action_1:I = 0x7f0a001f

.field public static final accessibility_custom_action_10:I = 0x7f0a0020

.field public static final accessibility_custom_action_11:I = 0x7f0a0021

.field public static final accessibility_custom_action_12:I = 0x7f0a0022

.field public static final accessibility_custom_action_13:I = 0x7f0a0023

.field public static final accessibility_custom_action_14:I = 0x7f0a0024

.field public static final accessibility_custom_action_15:I = 0x7f0a0025

.field public static final accessibility_custom_action_16:I = 0x7f0a0026

.field public static final accessibility_custom_action_17:I = 0x7f0a0027

.field public static final accessibility_custom_action_18:I = 0x7f0a0028

.field public static final accessibility_custom_action_19:I = 0x7f0a0029

.field public static final accessibility_custom_action_2:I = 0x7f0a002a

.field public static final accessibility_custom_action_20:I = 0x7f0a002b

.field public static final accessibility_custom_action_21:I = 0x7f0a002c

.field public static final accessibility_custom_action_22:I = 0x7f0a002d

.field public static final accessibility_custom_action_23:I = 0x7f0a002e

.field public static final accessibility_custom_action_24:I = 0x7f0a002f

.field public static final accessibility_custom_action_25:I = 0x7f0a0030

.field public static final accessibility_custom_action_26:I = 0x7f0a0031

.field public static final accessibility_custom_action_27:I = 0x7f0a0032

.field public static final accessibility_custom_action_28:I = 0x7f0a0033

.field public static final accessibility_custom_action_29:I = 0x7f0a0034

.field public static final accessibility_custom_action_3:I = 0x7f0a0035

.field public static final accessibility_custom_action_30:I = 0x7f0a0036

.field public static final accessibility_custom_action_31:I = 0x7f0a0037

.field public static final accessibility_custom_action_4:I = 0x7f0a0038

.field public static final accessibility_custom_action_5:I = 0x7f0a0039

.field public static final accessibility_custom_action_6:I = 0x7f0a003a

.field public static final accessibility_custom_action_7:I = 0x7f0a003b

.field public static final accessibility_custom_action_8:I = 0x7f0a003c

.field public static final accessibility_custom_action_9:I = 0x7f0a003d

.field public static final account:I = 0x7f0a003e

.field public static final account_box_btn:I = 0x7f0a003f

.field public static final account_dropbox_btn:I = 0x7f0a0040

.field public static final account_evernote_btn:I = 0x7f0a0041

.field public static final account_gdoc_btn:I = 0x7f0a0042

.field public static final account_onedrive_btn:I = 0x7f0a0043

.field public static final account_scanner_btn:I = 0x7f0a0044

.field public static final aciv_close:I = 0x7f0a0045

.field public static final aciv_discount_purchase_v2_clock:I = 0x7f0a0046

.field public static final aciv_discount_purchase_v2_close:I = 0x7f0a0047

.field public static final aciv_discount_purchase_v2_top_logo:I = 0x7f0a0048

.field public static final aciv_guide_gp_purchase_page_more:I = 0x7f0a0049

.field public static final aciv_lasso_light:I = 0x7f0a004a

.field public static final aciv_lasso_thick:I = 0x7f0a004b

.field public static final aciv_pay_check_month:I = 0x7f0a004c

.field public static final aciv_pay_check_year:I = 0x7f0a004d

.field public static final aciv_recall_purchase_title:I = 0x7f0a004e

.field public static final aciv_recall_purchase_top:I = 0x7f0a004f

.field public static final act_vip_coupon:I = 0x7f0a0050

.field public static final action0:I = 0x7f0a0051

.field public static final actionDone:I = 0x7f0a0052

.field public static final actionDown:I = 0x7f0a0053

.field public static final actionDownUp:I = 0x7f0a0054

.field public static final actionGo:I = 0x7f0a0055

.field public static final actionNext:I = 0x7f0a0056

.field public static final actionSearch:I = 0x7f0a0057

.field public static final actionSend:I = 0x7f0a0058

.field public static final actionUp:I = 0x7f0a0059

.field public static final action_bar:I = 0x7f0a005a

.field public static final action_bar_activity_content:I = 0x7f0a005b

.field public static final action_bar_container:I = 0x7f0a005c

.field public static final action_bar_doc:I = 0x7f0a005d

.field public static final action_bar_doc_extract:I = 0x7f0a005e

.field public static final action_bar_doc_pdf_kit_move:I = 0x7f0a005f

.field public static final action_bar_root:I = 0x7f0a0060

.field public static final action_bar_spinner:I = 0x7f0a0061

.field public static final action_bar_subtitle:I = 0x7f0a0062

.field public static final action_bar_title:I = 0x7f0a0063

.field public static final action_btn:I = 0x7f0a0064

.field public static final action_certificate_more_search:I = 0x7f0a0065

.field public static final action_container:I = 0x7f0a0066

.field public static final action_context_bar:I = 0x7f0a0067

.field public static final action_divider:I = 0x7f0a0068

.field public static final action_image:I = 0x7f0a0069

.field public static final action_layout:I = 0x7f0a006a

.field public static final action_menu_divider:I = 0x7f0a006b

.field public static final action_menu_presenter:I = 0x7f0a006c

.field public static final action_mode_bar:I = 0x7f0a006d

.field public static final action_mode_bar_stub:I = 0x7f0a006e

.field public static final action_mode_close_button:I = 0x7f0a006f

.field public static final action_save:I = 0x7f0a0070

.field public static final action_share:I = 0x7f0a0071

.field public static final action_text:I = 0x7f0a0072

.field public static final actions:I = 0x7f0a0073

.field public static final activity_authentication:I = 0x7f0a0074

.field public static final activity_chooser_view_content:I = 0x7f0a0075

.field public static final actv_discount_purchase_v2_skip:I = 0x7f0a0076

.field public static final actv_discount_purchase_v2_start_trial:I = 0x7f0a0077

.field public static final actv_guide_cn_purchase_price_desc01:I = 0x7f0a0078

.field public static final actv_guide_cn_purchase_price_desc02:I = 0x7f0a0079

.field public static final actv_item_guide_cn_subtitle:I = 0x7f0a007a

.field public static final actv_item_negative_premium_subtitle:I = 0x7f0a007b

.field public static final ad_arrow:I = 0x7f0a007c

.field public static final ad_btn:I = 0x7f0a007d

.field public static final ad_choices_container:I = 0x7f0a007e

.field public static final ad_complain:I = 0x7f0a007f

.field public static final ad_container:I = 0x7f0a0080

.field public static final ad_container_launch:I = 0x7f0a0081

.field public static final ad_control_button:I = 0x7f0a0082

.field public static final ad_controls_view:I = 0x7f0a0083

.field public static final ad_des:I = 0x7f0a0084

.field public static final ad_edt:I = 0x7f0a0085

.field public static final ad_main_media:I = 0x7f0a0086

.field public static final ad_presenter_view:I = 0x7f0a0087

.field public static final ad_root:I = 0x7f0a0088

.field public static final ad_tag:I = 0x7f0a0089

.field public static final ad_tag_bottom:I = 0x7f0a008a

.field public static final ad_title:I = 0x7f0a008b

.field public static final ad_view_container:I = 0x7f0a008c

.field public static final adapt_grid_item_id:I = 0x7f0a008d

.field public static final add:I = 0x7f0a008e

.field public static final add_capture_root:I = 0x7f0a008f

.field public static final add_ink_btn:I = 0x7f0a0090

.field public static final add_new_pdf_size:I = 0x7f0a0091

.field public static final add_one_msg:I = 0x7f0a0092

.field public static final add_photo:I = 0x7f0a0093

.field public static final adjacent:I = 0x7f0a0094

.field public static final adjust_bg_menu:I = 0x7f0a0095

.field public static final adjust_bg_seekBar:I = 0x7f0a0096

.field public static final adjust_height:I = 0x7f0a0097

.field public static final adjust_iv_reset:I = 0x7f0a0098

.field public static final adjust_iv_save:I = 0x7f0a0099

.field public static final adjust_sb_brightness:I = 0x7f0a009a

.field public static final adjust_tv_brightness:I = 0x7f0a009b

.field public static final adjust_tv_brightness_value:I = 0x7f0a009c

.field public static final adjust_tv_contrast:I = 0x7f0a009d

.field public static final adjust_tv_detail:I = 0x7f0a009e

.field public static final adjust_width:I = 0x7f0a009f

.field public static final adtv_lable_vip_type:I = 0x7f0a00a0

.field public static final agv_grid_view:I = 0x7f0a00a1

.field public static final aiv_arrow:I = 0x7f0a00a2

.field public static final aiv_arrow_0:I = 0x7f0a00a3

.field public static final aiv_arrow_1:I = 0x7f0a00a4

.field public static final aiv_arrow_card_bag:I = 0x7f0a00a5

.field public static final aiv_arrow_right:I = 0x7f0a00a6

.field public static final aiv_arrowhead:I = 0x7f0a00a7

.field public static final aiv_award_preview:I = 0x7f0a00a8

.field public static final aiv_banner:I = 0x7f0a00a9

.field public static final aiv_benefit_gift:I = 0x7f0a00aa

.field public static final aiv_book_arrow:I = 0x7f0a00ab

.field public static final aiv_brief_case_bottom_icon:I = 0x7f0a00ac

.field public static final aiv_brief_case_icon:I = 0x7f0a00ad

.field public static final aiv_canvas:I = 0x7f0a00ae

.field public static final aiv_card_bag_bottom_icon:I = 0x7f0a00af

.field public static final aiv_card_bag_icon:I = 0x7f0a00b0

.field public static final aiv_change_image:I = 0x7f0a00b1

.field public static final aiv_close:I = 0x7f0a00b2

.field public static final aiv_close_camera:I = 0x7f0a00b3

.field public static final aiv_cloud_icon:I = 0x7f0a00b4

.field public static final aiv_content:I = 0x7f0a00b5

.field public static final aiv_content_ori:I = 0x7f0a00b6

.field public static final aiv_cur_model_close:I = 0x7f0a00b7

.field public static final aiv_data_security:I = 0x7f0a00b8

.field public static final aiv_data_show_ori_copy:I = 0x7f0a00b9

.field public static final aiv_data_show_trans_copy:I = 0x7f0a00ba

.field public static final aiv_dir_icon:I = 0x7f0a00bb

.field public static final aiv_direct_func:I = 0x7f0a00bc

.field public static final aiv_doc_icon:I = 0x7f0a00bd

.field public static final aiv_doc_pages:I = 0x7f0a00be

.field public static final aiv_done:I = 0x7f0a00bf

.field public static final aiv_dynamic_feature:I = 0x7f0a00c0

.field public static final aiv_gift_box:I = 0x7f0a00c1

.field public static final aiv_gp_guide_mark_bg:I = 0x7f0a00c2

.field public static final aiv_gp_guide_mark_new_bg:I = 0x7f0a00c3

.field public static final aiv_guide_gp_item_normal_middle_title:I = 0x7f0a00c4

.field public static final aiv_guide_gp_item_normal_right_title:I = 0x7f0a00c5

.field public static final aiv_hd_tag:I = 0x7f0a00c6

.field public static final aiv_header:I = 0x7f0a00c7

.field public static final aiv_header_arrow:I = 0x7f0a00c8

.field public static final aiv_header_option:I = 0x7f0a00c9

.field public static final aiv_icon:I = 0x7f0a00ca

.field public static final aiv_icon_novice:I = 0x7f0a00cb

.field public static final aiv_icon_tip:I = 0x7f0a00cc

.field public static final aiv_image_bg:I = 0x7f0a00cd

.field public static final aiv_index_bg:I = 0x7f0a00ce

.field public static final aiv_is_vip:I = 0x7f0a00cf

.field public static final aiv_item_0:I = 0x7f0a00d0

.field public static final aiv_item_1:I = 0x7f0a00d1

.field public static final aiv_item_2:I = 0x7f0a00d2

.field public static final aiv_item_3:I = 0x7f0a00d3

.field public static final aiv_item_dir_corner_icon:I = 0x7f0a00d4

.field public static final aiv_item_dir_icon:I = 0x7f0a00d5

.field public static final aiv_left:I = 0x7f0a00d6

.field public static final aiv_lock_icon:I = 0x7f0a00d7

.field public static final aiv_mask:I = 0x7f0a00d8

.field public static final aiv_middle_item_normal_basic:I = 0x7f0a00d9

.field public static final aiv_more:I = 0x7f0a00da

.field public static final aiv_more_fun_1:I = 0x7f0a00db

.field public static final aiv_more_fun_2:I = 0x7f0a00dc

.field public static final aiv_more_fun_bg:I = 0x7f0a00dd

.field public static final aiv_more_icon:I = 0x7f0a00de

.field public static final aiv_next:I = 0x7f0a00df

.field public static final aiv_normal_dir:I = 0x7f0a00e0

.field public static final aiv_novice_close:I = 0x7f0a00e1

.field public static final aiv_ocr_preview:I = 0x7f0a00e2

.field public static final aiv_office_type:I = 0x7f0a00e3

.field public static final aiv_ori_show_all:I = 0x7f0a00e4

.field public static final aiv_page_1_wheat_start:I = 0x7f0a00e5

.field public static final aiv_page_count_0:I = 0x7f0a00e6

.field public static final aiv_page_count_1:I = 0x7f0a00e7

.field public static final aiv_page_count_2:I = 0x7f0a00e8

.field public static final aiv_page_count_3:I = 0x7f0a00e9

.field public static final aiv_pdf_editing_cancel_cs_qr_code:I = 0x7f0a00ea

.field public static final aiv_pdf_icon:I = 0x7f0a00eb

.field public static final aiv_resort_merged_docs_drag:I = 0x7f0a00ec

.field public static final aiv_revert_book:I = 0x7f0a00ed

.field public static final aiv_right:I = 0x7f0a00ee

.field public static final aiv_right_item_normal_premium:I = 0x7f0a00ef

.field public static final aiv_search_hl_image_title:I = 0x7f0a00f0

.field public static final aiv_select_left_month:I = 0x7f0a00f1

.field public static final aiv_select_left_year:I = 0x7f0a00f2

.field public static final aiv_select_right_month:I = 0x7f0a00f3

.field public static final aiv_select_right_year:I = 0x7f0a00f4

.field public static final aiv_setting_filter:I = 0x7f0a00f5

.field public static final aiv_setting_flash:I = 0x7f0a00f6

.field public static final aiv_setting_guide:I = 0x7f0a00f7

.field public static final aiv_setting_more:I = 0x7f0a00f8

.field public static final aiv_setting_pixel:I = 0x7f0a00f9

.field public static final aiv_setting_pixel_select:I = 0x7f0a00fa

.field public static final aiv_share:I = 0x7f0a00fb

.field public static final aiv_share_dir:I = 0x7f0a00fc

.field public static final aiv_share_dir_info:I = 0x7f0a00fd

.field public static final aiv_share_icon:I = 0x7f0a00fe

.field public static final aiv_share_link_grid_image:I = 0x7f0a00ff

.field public static final aiv_share_link_hor_list_image:I = 0x7f0a0100

.field public static final aiv_share_link_list_image:I = 0x7f0a0101

.field public static final aiv_share_vip:I = 0x7f0a0102

.field public static final aiv_share_vip_icon:I = 0x7f0a0103

.field public static final aiv_translate_from_lang:I = 0x7f0a0104

.field public static final aiv_translate_to_lang:I = 0x7f0a0105

.field public static final aiv_vip:I = 0x7f0a0106

.field public static final aiv_vip_icon:I = 0x7f0a0107

.field public static final aiv_wechat:I = 0x7f0a0108

.field public static final al_exo_ad_overlay:I = 0x7f0a0109

.field public static final al_exo_artwork:I = 0x7f0a010a

.field public static final al_exo_audio_track:I = 0x7f0a010b

.field public static final al_exo_basic_controls:I = 0x7f0a010c

.field public static final al_exo_bottom_bar:I = 0x7f0a010d

.field public static final al_exo_buffering:I = 0x7f0a010e

.field public static final al_exo_center_controls:I = 0x7f0a010f

.field public static final al_exo_content_frame:I = 0x7f0a0110

.field public static final al_exo_controller:I = 0x7f0a0111

.field public static final al_exo_controller_placeholder:I = 0x7f0a0112

.field public static final al_exo_controls_background:I = 0x7f0a0113

.field public static final al_exo_duration:I = 0x7f0a0114

.field public static final al_exo_error_message:I = 0x7f0a0115

.field public static final al_exo_extra_controls:I = 0x7f0a0116

.field public static final al_exo_extra_controls_scroll_view:I = 0x7f0a0117

.field public static final al_exo_ffwd:I = 0x7f0a0118

.field public static final al_exo_ffwd_with_amount:I = 0x7f0a0119

.field public static final al_exo_fullscreen:I = 0x7f0a011a

.field public static final al_exo_minimal_controls:I = 0x7f0a011b

.field public static final al_exo_minimal_fullscreen:I = 0x7f0a011c

.field public static final al_exo_next:I = 0x7f0a011d

.field public static final al_exo_overflow_hide:I = 0x7f0a011e

.field public static final al_exo_overflow_show:I = 0x7f0a011f

.field public static final al_exo_overlay:I = 0x7f0a0120

.field public static final al_exo_pause:I = 0x7f0a0121

.field public static final al_exo_play:I = 0x7f0a0122

.field public static final al_exo_play_pause:I = 0x7f0a0123

.field public static final al_exo_playback_speed:I = 0x7f0a0124

.field public static final al_exo_position:I = 0x7f0a0125

.field public static final al_exo_prev:I = 0x7f0a0126

.field public static final al_exo_progress:I = 0x7f0a0127

.field public static final al_exo_progress_placeholder:I = 0x7f0a0128

.field public static final al_exo_repeat_toggle:I = 0x7f0a0129

.field public static final al_exo_rew:I = 0x7f0a012a

.field public static final al_exo_rew_with_amount:I = 0x7f0a012b

.field public static final al_exo_settings:I = 0x7f0a012c

.field public static final al_exo_shuffle:I = 0x7f0a012d

.field public static final al_exo_shutter:I = 0x7f0a012e

.field public static final al_exo_subtitle:I = 0x7f0a012f

.field public static final al_exo_subtitles:I = 0x7f0a0130

.field public static final al_exo_time:I = 0x7f0a0131

.field public static final al_exo_vr:I = 0x7f0a0132

.field public static final alertTitle:I = 0x7f0a0133

.field public static final aligned:I = 0x7f0a0134

.field public static final all:I = 0x7f0a0135

.field public static final allStates:I = 0x7f0a0136

.field public static final always:I = 0x7f0a0137

.field public static final amin_progress_bar:I = 0x7f0a0138

.field public static final amin_unsubscribe_recall_progress_bar:I = 0x7f0a0139

.field public static final analytics_purposes_switch:I = 0x7f0a013a

.field public static final analytics_purposes_switch_textview:I = 0x7f0a013b

.field public static final animateToEnd:I = 0x7f0a013c

.field public static final animateToStart:I = 0x7f0a013d

.field public static final antiClockwise:I = 0x7f0a013e

.field public static final anticipate:I = 0x7f0a013f

.field public static final appCompatTextView:I = 0x7f0a0140

.field public static final app_banner_image:I = 0x7f0a0141

.field public static final app_bar:I = 0x7f0a0142

.field public static final app_bar_layout:I = 0x7f0a0143

.field public static final app_copyright:I = 0x7f0a0144

.field public static final app_launch_ll_tips:I = 0x7f0a0145

.field public static final app_name:I = 0x7f0a0146

.field public static final app_open_ad_control_button:I = 0x7f0a0147

.field public static final app_open_ad_control_view:I = 0x7f0a0148

.field public static final app_version:I = 0x7f0a0149

.field public static final appbar_layout:I = 0x7f0a014a

.field public static final applovin_native_ad_badge_and_title_text_view:I = 0x7f0a014b

.field public static final applovin_native_ad_content_linear_layout:I = 0x7f0a014c

.field public static final applovin_native_ad_view_container:I = 0x7f0a014d

.field public static final applovin_native_advertiser_text_view:I = 0x7f0a014e

.field public static final applovin_native_badge_text_view:I = 0x7f0a014f

.field public static final applovin_native_body_text_view:I = 0x7f0a0150

.field public static final applovin_native_cta_button:I = 0x7f0a0151

.field public static final applovin_native_guideline:I = 0x7f0a0152

.field public static final applovin_native_icon_and_text_layout:I = 0x7f0a0153

.field public static final applovin_native_icon_image_view:I = 0x7f0a0154

.field public static final applovin_native_icon_view:I = 0x7f0a0155

.field public static final applovin_native_inner_linear_layout:I = 0x7f0a0156

.field public static final applovin_native_inner_parent_layout:I = 0x7f0a0157

.field public static final applovin_native_leader_icon_and_text_layout:I = 0x7f0a0158

.field public static final applovin_native_media_content_view:I = 0x7f0a0159

.field public static final applovin_native_options_view:I = 0x7f0a015a

.field public static final applovin_native_star_rating_view:I = 0x7f0a015b

.field public static final applovin_native_title_text_view:I = 0x7f0a015c

.field public static final arc:I = 0x7f0a015d

.field public static final arrow:I = 0x7f0a015e

.field public static final arrow_down_layout:I = 0x7f0a015f

.field public static final arrow_up_layout:I = 0x7f0a0160

.field public static final asConfigured:I = 0x7f0a0161

.field public static final assist_line:I = 0x7f0a0162

.field public static final async:I = 0x7f0a0163

.field public static final ativ_star:I = 0x7f0a0164

.field public static final atv_about_ocr:I = 0x7f0a0165

.field public static final atv_accept_award:I = 0x7f0a0166

.field public static final atv_back_to_accept_award:I = 0x7f0a0167

.field public static final atv_backup_dir_doc_sort:I = 0x7f0a0168

.field public static final atv_backup_dir_doc_type_filter:I = 0x7f0a0169

.field public static final atv_book_edit_tips:I = 0x7f0a016a

.field public static final atv_book_unlock_page:I = 0x7f0a016b

.field public static final atv_brief_case_name:I = 0x7f0a016c

.field public static final atv_camscanner_website_guide_title_refactor:I = 0x7f0a016d

.field public static final atv_card_bag_name:I = 0x7f0a016e

.field public static final atv_change_title:I = 0x7f0a016f

.field public static final atv_cloud_claim:I = 0x7f0a0170

.field public static final atv_confirm_give_up_award:I = 0x7f0a0171

.field public static final atv_count_down_title:I = 0x7f0a0172

.field public static final atv_cur_model_name:I = 0x7f0a0173

.field public static final atv_data_security:I = 0x7f0a0174

.field public static final atv_des:I = 0x7f0a0175

.field public static final atv_dir_gen:I = 0x7f0a0176

.field public static final atv_dir_type_name:I = 0x7f0a0177

.field public static final atv_disclaimer:I = 0x7f0a0178

.field public static final atv_edit:I = 0x7f0a0179

.field public static final atv_end_desc:I = 0x7f0a017a

.field public static final atv_flash_auto:I = 0x7f0a017b

.field public static final atv_flash_off:I = 0x7f0a017c

.field public static final atv_flash_on:I = 0x7f0a017d

.field public static final atv_flash_torch:I = 0x7f0a017e

.field public static final atv_give_up_award:I = 0x7f0a017f

.field public static final atv_header:I = 0x7f0a0180

.field public static final atv_hint_top:I = 0x7f0a0181

.field public static final atv_hint_topic_question_horizontal:I = 0x7f0a0182

.field public static final atv_member_benefits:I = 0x7f0a0183

.field public static final atv_normal_bottom_take_photo:I = 0x7f0a0184

.field public static final atv_normal_dir_desc:I = 0x7f0a0185

.field public static final atv_normal_dir_title:I = 0x7f0a0186

.field public static final atv_pdf_claim:I = 0x7f0a0187

.field public static final atv_pixel_all_name:I = 0x7f0a0188

.field public static final atv_pixel_name:I = 0x7f0a0189

.field public static final atv_pixel_number:I = 0x7f0a018a

.field public static final atv_recommend:I = 0x7f0a018b

.field public static final atv_scan_tips:I = 0x7f0a018c

.field public static final atv_setting_more_auto_capture_desc:I = 0x7f0a018d

.field public static final atv_setting_more_auto_capture_title:I = 0x7f0a018e

.field public static final atv_setting_more_auto_crop_desc:I = 0x7f0a018f

.field public static final atv_setting_more_auto_crop_title:I = 0x7f0a0190

.field public static final atv_setting_more_grid_desc:I = 0x7f0a0191

.field public static final atv_setting_more_grid_title:I = 0x7f0a0192

.field public static final atv_setting_more_jump_to_page:I = 0x7f0a0193

.field public static final atv_setting_more_orientation_desc:I = 0x7f0a0194

.field public static final atv_setting_more_orientation_title:I = 0x7f0a0195

.field public static final atv_setting_more_sensor_desc:I = 0x7f0a0196

.field public static final atv_setting_more_sensor_title:I = 0x7f0a0197

.field public static final atv_setting_more_sound_desc:I = 0x7f0a0198

.field public static final atv_setting_more_sound_title:I = 0x7f0a0199

.field public static final atv_share_dir_desc:I = 0x7f0a019a

.field public static final atv_share_dir_title:I = 0x7f0a019b

.field public static final atv_share_text:I = 0x7f0a019c

.field public static final atv_take_photo:I = 0x7f0a019d

.field public static final atv_text_cloud:I = 0x7f0a019e

.field public static final atv_text_cloud_date:I = 0x7f0a019f

.field public static final atv_text_cloud_tip:I = 0x7f0a01a0

.field public static final atv_text_pdf:I = 0x7f0a01a1

.field public static final atv_text_pdf_date:I = 0x7f0a01a2

.field public static final atv_text_pdf_tip:I = 0x7f0a01a3

.field public static final atv_text_vip:I = 0x7f0a01a4

.field public static final atv_text_vip_account:I = 0x7f0a01a5

.field public static final atv_text_vip_date:I = 0x7f0a01a6

.field public static final atv_time_line_year:I = 0x7f0a01a7

.field public static final atv_timeline_date:I = 0x7f0a01a8

.field public static final atv_timeline_month:I = 0x7f0a01a9

.field public static final atv_timeline_title:I = 0x7f0a01aa

.field public static final atv_tips:I = 0x7f0a01ab

.field public static final atv_title:I = 0x7f0a01ac

.field public static final atv_upgrade_btn:I = 0x7f0a01ad

.field public static final atv_vip_claim:I = 0x7f0a01ae

.field public static final atv_vip_expire_date:I = 0x7f0a01af

.field public static final authentication_activity_progressBar:I = 0x7f0a01b0

.field public static final authentication_activity_webView:I = 0x7f0a01b1

.field public static final auto:I = 0x7f0a01b2

.field public static final autoComplete:I = 0x7f0a01b3

.field public static final autoCompleteToEnd:I = 0x7f0a01b4

.field public static final autoCompleteToStart:I = 0x7f0a01b5

.field public static final auto_capture_progress:I = 0x7f0a01b6

.field public static final auto_capture_root:I = 0x7f0a01b7

.field public static final automatic:I = 0x7f0a01b8

.field public static final back:I = 0x7f0a01b9

.field public static final back_button:I = 0x7f0a01ba

.field public static final baiv_email:I = 0x7f0a01bb

.field public static final baiv_google:I = 0x7f0a01bc

.field public static final baiv_phone:I = 0x7f0a01bd

.field public static final baiv_wx:I = 0x7f0a01be

.field public static final bank_card_back:I = 0x7f0a01bf

.field public static final bank_card_import:I = 0x7f0a01c0

.field public static final bank_card_shutter_button:I = 0x7f0a01c1

.field public static final banner_ad_view_container:I = 0x7f0a01c2

.field public static final banner_control_button:I = 0x7f0a01c3

.field public static final banner_control_view:I = 0x7f0a01c4

.field public static final banner_label:I = 0x7f0a01c5

.field public static final banner_remove:I = 0x7f0a01c6

.field public static final barrier:I = 0x7f0a01c7

.field public static final barrier_bg:I = 0x7f0a01c8

.field public static final barrier_bottom:I = 0x7f0a01c9

.field public static final barrier_btn:I = 0x7f0a01ca

.field public static final barrier_end:I = 0x7f0a01cb

.field public static final barrier_line:I = 0x7f0a01cc

.field public static final barrier_pay_way:I = 0x7f0a01cd

.field public static final barrier_start:I = 0x7f0a01ce

.field public static final baseline:I = 0x7f0a01cf

.field public static final beginOnFirstDraw:I = 0x7f0a01d0

.field public static final beginning:I = 0x7f0a01d1

.field public static final bestChoice:I = 0x7f0a01d2

.field public static final bg_capture:I = 0x7f0a01d3

.field public static final bg_image:I = 0x7f0a01d4

.field public static final bg_iv_cs_icon:I = 0x7f0a01d5

.field public static final bg_iv_qr_code:I = 0x7f0a01d6

.field public static final bg_tv_bottom_des:I = 0x7f0a01d7

.field public static final bg_tv_title:I = 0x7f0a01d8

.field public static final bg_v:I = 0x7f0a01d9

.field public static final bl_content:I = 0x7f0a01da

.field public static final blocking:I = 0x7f0a01db

.field public static final book_back:I = 0x7f0a01dc

.field public static final book_edit:I = 0x7f0a01dd

.field public static final book_shutter_button:I = 0x7f0a01de

.field public static final book_splitter_num:I = 0x7f0a01df

.field public static final book_splitter_thumb:I = 0x7f0a01e0

.field public static final border_view:I = 0x7f0a01e1

.field public static final bottom:I = 0x7f0a01e2

.field public static final bottom_action_bar:I = 0x7f0a01e3

.field public static final bottom_action_bar_activiate:I = 0x7f0a01e4

.field public static final bottom_area:I = 0x7f0a01e5

.field public static final bottom_bar:I = 0x7f0a01e6

.field public static final bottom_barrier:I = 0x7f0a01e7

.field public static final bottom_bg:I = 0x7f0a01e8

.field public static final bottom_divider:I = 0x7f0a01e9

.field public static final bottom_left:I = 0x7f0a01ea

.field public static final bottom_line:I = 0x7f0a01eb

.field public static final bottom_navibar:I = 0x7f0a01ec

.field public static final bottom_panel:I = 0x7f0a01ed

.field public static final bottom_purchase_price_layout:I = 0x7f0a01ee

.field public static final bottom_right:I = 0x7f0a01ef

.field public static final bottom_space:I = 0x7f0a01f0

.field public static final bottom_space_tag_horizontal:I = 0x7f0a01f1

.field public static final bottom_space_tag_vertical:I = 0x7f0a01f2

.field public static final bottom_tag:I = 0x7f0a01f3

.field public static final bottombar_container:I = 0x7f0a01f4

.field public static final bottombar_container_for_book_mode:I = 0x7f0a01f5

.field public static final bottombar_container_for_ocr_multi:I = 0x7f0a01f6

.field public static final bottombar_container_for_topic_paper:I = 0x7f0a01f7

.field public static final bottombar_container_fun_recommend:I = 0x7f0a01f8

.field public static final bounce:I = 0x7f0a01f9

.field public static final bounceBoth:I = 0x7f0a01fa

.field public static final bounceEnd:I = 0x7f0a01fb

.field public static final bounceStart:I = 0x7f0a01fc

.field public static final box_count:I = 0x7f0a01fd

.field public static final box_new_pwd_logined:I = 0x7f0a01fe

.field public static final box_new_pwd_unlogined:I = 0x7f0a01ff

.field public static final box_old_pwd_logined:I = 0x7f0a0200

.field public static final box_old_pwd_unlogined:I = 0x7f0a0201

.field public static final box_username:I = 0x7f0a0202

.field public static final browser_actions_header_text:I = 0x7f0a0203

.field public static final browser_actions_menu_item_icon:I = 0x7f0a0204

.field public static final browser_actions_menu_item_text:I = 0x7f0a0205

.field public static final browser_actions_menu_items:I = 0x7f0a0206

.field public static final browser_actions_menu_view:I = 0x7f0a0207

.field public static final bt_favorable24:I = 0x7f0a0208

.field public static final bt_favorable48:I = 0x7f0a0209

.field public static final bt_favorable_end:I = 0x7f0a020a

.field public static final bt_query_vip:I = 0x7f0a020b

.field public static final btm_divider:I = 0x7f0a020c

.field public static final btnSheet:I = 0x7f0a020d

.field public static final btn_a_key_login_a_key_login:I = 0x7f0a020e

.field public static final btn_action:I = 0x7f0a020f

.field public static final btn_actionbar_done:I = 0x7f0a0210

.field public static final btn_actionbar_email_to_me:I = 0x7f0a0211

.field public static final btn_actionbar_import:I = 0x7f0a0212

.field public static final btn_actionbar_more:I = 0x7f0a0213

.field public static final btn_actionbar_reedit:I = 0x7f0a0214

.field public static final btn_actionbar_rename:I = 0x7f0a0215

.field public static final btn_actionbar_share:I = 0x7f0a0216

.field public static final btn_actionbar_turn_right:I = 0x7f0a0217

.field public static final btn_actionbar_view_pdf:I = 0x7f0a0218

.field public static final btn_ad:I = 0x7f0a0219

.field public static final btn_adb_move_image_files:I = 0x7f0a021a

.field public static final btn_adjust:I = 0x7f0a021b

.field public static final btn_annot_del:I = 0x7f0a021c

.field public static final btn_annot_style:I = 0x7f0a021d

.field public static final btn_apis_gid:I = 0x7f0a021e

.field public static final btn_app_launch:I = 0x7f0a021f

.field public static final btn_app_launch_active:I = 0x7f0a0220

.field public static final btn_app_launch_query_property:I = 0x7f0a0221

.field public static final btn_area_code_confirm_next:I = 0x7f0a0222

.field public static final btn_auto_report_log:I = 0x7f0a0223

.field public static final btn_baidu:I = 0x7f0a0224

.field public static final btn_bind_fail_ok:I = 0x7f0a0225

.field public static final btn_bind_phone_for_certain:I = 0x7f0a0226

.field public static final btn_bind_success_ok:I = 0x7f0a0227

.field public static final btn_bottom:I = 0x7f0a0228

.field public static final btn_bottom_more:I = 0x7f0a0229

.field public static final btn_buy_1:I = 0x7f0a022a

.field public static final btn_buy_10:I = 0x7f0a022b

.field public static final btn_buy_2:I = 0x7f0a022c

.field public static final btn_cancel:I = 0x7f0a022d

.field public static final btn_cancel_account:I = 0x7f0a022e

.field public static final btn_capture_retake:I = 0x7f0a022f

.field public static final btn_capture_test:I = 0x7f0a0230

.field public static final btn_certificate_engine:I = 0x7f0a0231

.field public static final btn_change:I = 0x7f0a0232

.field public static final btn_change_db:I = 0x7f0a0233

.field public static final btn_change_db_reverse:I = 0x7f0a0234

.field public static final btn_clean:I = 0x7f0a0235

.field public static final btn_clear:I = 0x7f0a0236

.field public static final btn_common:I = 0x7f0a0237

.field public static final btn_confirm:I = 0x7f0a0238

.field public static final btn_confirm_logined:I = 0x7f0a0239

.field public static final btn_confirm_unlogined:I = 0x7f0a023a

.field public static final btn_continue:I = 0x7f0a023b

.field public static final btn_continue_photo:I = 0x7f0a023c

.field public static final btn_copy:I = 0x7f0a023d

.field public static final btn_copy_image_files:I = 0x7f0a023e

.field public static final btn_create:I = 0x7f0a023f

.field public static final btn_create_account:I = 0x7f0a0240

.field public static final btn_db_test:I = 0x7f0a0241

.field public static final btn_de_moire:I = 0x7f0a0242

.field public static final btn_deeplink:I = 0x7f0a0243

.field public static final btn_delete:I = 0x7f0a0244

.field public static final btn_deleteline:I = 0x7f0a0245

.field public static final btn_desecret:I = 0x7f0a0246

.field public static final btn_device_id:I = 0x7f0a0247

.field public static final btn_dir:I = 0x7f0a0248

.field public static final btn_dirty_account_uid:I = 0x7f0a0249

.field public static final btn_dismss:I = 0x7f0a024a

.field public static final btn_doc_get_upgrade:I = 0x7f0a024b

.field public static final btn_done:I = 0x7f0a024c

.field public static final btn_draft_share:I = 0x7f0a024d

.field public static final btn_drag:I = 0x7f0a024e

.field public static final btn_drag_compare_image:I = 0x7f0a024f

.field public static final btn_edit:I = 0x7f0a0250

.field public static final btn_edit_image:I = 0x7f0a0251

.field public static final btn_email_login_next:I = 0x7f0a0252

.field public static final btn_email_login_sign_in:I = 0x7f0a0253

.field public static final btn_email_register_next:I = 0x7f0a0254

.field public static final btn_ensecret:I = 0x7f0a0255

.field public static final btn_exception_check:I = 0x7f0a0256

.field public static final btn_export:I = 0x7f0a0257

.field public static final btn_fax_send:I = 0x7f0a0258

.field public static final btn_feedback_submit:I = 0x7f0a0259

.field public static final btn_filter:I = 0x7f0a025a

.field public static final btn_fix_image:I = 0x7f0a025b

.field public static final btn_forget_pwd_get_verify_code:I = 0x7f0a025c

.field public static final btn_gen_large_files:I = 0x7f0a025d

.field public static final btn_get_cs_cfg:I = 0x7f0a025e

.field public static final btn_get_gaid:I = 0x7f0a025f

.field public static final btn_go_web:I = 0x7f0a0260

.field public static final btn_guide:I = 0x7f0a0261

.field public static final btn_halfpack_left:I = 0x7f0a0262

.field public static final btn_halfpack_right:I = 0x7f0a0263

.field public static final btn_header_1:I = 0x7f0a0264

.field public static final btn_header_2:I = 0x7f0a0265

.field public static final btn_hide:I = 0x7f0a0266

.field public static final btn_highline:I = 0x7f0a0267

.field public static final btn_huawei:I = 0x7f0a0268

.field public static final btn_i_know:I = 0x7f0a0269

.field public static final btn_i_known:I = 0x7f0a026a

.field public static final btn_intro1:I = 0x7f0a026b

.field public static final btn_intro2:I = 0x7f0a026c

.field public static final btn_intro3:I = 0x7f0a026d

.field public static final btn_intro4:I = 0x7f0a026e

.field public static final btn_ip_address:I = 0x7f0a026f

.field public static final btn_launch_guide:I = 0x7f0a0270

.field public static final btn_local_gated:I = 0x7f0a0271

.field public static final btn_local_ocr:I = 0x7f0a0272

.field public static final btn_logagent_monitor:I = 0x7f0a0273

.field public static final btn_login:I = 0x7f0a0274

.field public static final btn_login_main_next:I = 0x7f0a0275

.field public static final btn_main:I = 0x7f0a0276

.field public static final btn_message_center:I = 0x7f0a0277

.field public static final btn_miniprogram_share_doc:I = 0x7f0a0278

.field public static final btn_modify:I = 0x7f0a0279

.field public static final btn_more:I = 0x7f0a027a

.field public static final btn_move_dir:I = 0x7f0a027b

.field public static final btn_move_large_files:I = 0x7f0a027c

.field public static final btn_multiprocess_test:I = 0x7f0a027d

.field public static final btn_network_monitor:I = 0x7f0a027e

.field public static final btn_network_test_id:I = 0x7f0a027f

.field public static final btn_new_doc:I = 0x7f0a0280

.field public static final btn_next:I = 0x7f0a0281

.field public static final btn_note:I = 0x7f0a0282

.field public static final btn_nps:I = 0x7f0a0283

.field public static final btn_ocr:I = 0x7f0a0284

.field public static final btn_ocr_line_select_all:I = 0x7f0a0285

.field public static final btn_ok:I = 0x7f0a0286

.field public static final btn_onenote:I = 0x7f0a0287

.field public static final btn_open_test_admob_new:I = 0x7f0a0288

.field public static final btn_open_test_admob_old:I = 0x7f0a0289

.field public static final btn_open_test_applovin:I = 0x7f0a028a

.field public static final btn_pay:I = 0x7f0a028b

.field public static final btn_pdf_kit:I = 0x7f0a028c

.field public static final btn_phone_pwd_login_sign_in:I = 0x7f0a028d

.field public static final btn_phone_verify_code_login_get_verify_code:I = 0x7f0a028e

.field public static final btn_ping:I = 0x7f0a028f

.field public static final btn_point_cost:I = 0x7f0a0290

.field public static final btn_positive:I = 0x7f0a0291

.field public static final btn_prepaid:I = 0x7f0a0292

.field public static final btn_print:I = 0x7f0a0293

.field public static final btn_product_test:I = 0x7f0a0294

.field public static final btn_purchase_dialog:I = 0x7f0a0295

.field public static final btn_purchase_forever:I = 0x7f0a0296

.field public static final btn_purchase_point:I = 0x7f0a0297

.field public static final btn_push:I = 0x7f0a0298

.field public static final btn_pwd_login_next:I = 0x7f0a0299

.field public static final btn_query_product_list:I = 0x7f0a029a

.field public static final btn_reedit:I = 0x7f0a029b

.field public static final btn_register:I = 0x7f0a029c

.field public static final btn_reopen:I = 0x7f0a029d

.field public static final btn_request_launch:I = 0x7f0a029e

.field public static final btn_rotate:I = 0x7f0a029f

.field public static final btn_rotate_left:I = 0x7f0a02a0

.field public static final btn_save:I = 0x7f0a02a1

.field public static final btn_scale:I = 0x7f0a02a2

.field public static final btn_scan:I = 0x7f0a02a3

.field public static final btn_scan_kit:I = 0x7f0a02a4

.field public static final btn_scan_tips:I = 0x7f0a02a5

.field public static final btn_scene_banner:I = 0x7f0a02a6

.field public static final btn_see_modify:I = 0x7f0a02a7

.field public static final btn_send_verify_code:I = 0x7f0a02a8

.field public static final btn_setting_pwd_start:I = 0x7f0a02a9

.field public static final btn_share:I = 0x7f0a02aa

.field public static final btn_share_panel:I = 0x7f0a02ab

.field public static final btn_shortcut:I = 0x7f0a02ac

.field public static final btn_sign_up:I = 0x7f0a02ad

.field public static final btn_signature:I = 0x7f0a02ae

.field public static final btn_simulator:I = 0x7f0a02af

.field public static final btn_squiggly:I = 0x7f0a02b0

.field public static final btn_start:I = 0x7f0a02b1

.field public static final btn_submit:I = 0x7f0a02b2

.field public static final btn_super_vcode_validate_ok:I = 0x7f0a02b3

.field public static final btn_switch_vendor:I = 0x7f0a02b4

.field public static final btn_sync:I = 0x7f0a02b5

.field public static final btn_test:I = 0x7f0a02b6

.field public static final btn_test_cetificate_photo:I = 0x7f0a02b7

.field public static final btn_test_country:I = 0x7f0a02b8

.field public static final btn_test_doc_structure:I = 0x7f0a02b9

.field public static final btn_test_jump_set_default:I = 0x7f0a02ba

.field public static final btn_test_log:I = 0x7f0a02bb

.field public static final btn_test_perform:I = 0x7f0a02bc

.field public static final btn_test_scenario_dir:I = 0x7f0a02bd

.field public static final btn_test_set_pdf_default:I = 0x7f0a02be

.field public static final btn_test_sync:I = 0x7f0a02bf

.field public static final btn_text:I = 0x7f0a02c0

.field public static final btn_thank_dialog:I = 0x7f0a02c1

.field public static final btn_title_bar_test:I = 0x7f0a02c2

.field public static final btn_to_ad_cfg:I = 0x7f0a02c3

.field public static final btn_to_gallery_test:I = 0x7f0a02c4

.field public static final btn_to_innovationlab_test:I = 0x7f0a02c5

.field public static final btn_to_ipo_test:I = 0x7f0a02c6

.field public static final btn_to_share_preference:I = 0x7f0a02c7

.field public static final btn_to_word_test:I = 0x7f0a02c8

.field public static final btn_translate:I = 0x7f0a02c9

.field public static final btn_underline:I = 0x7f0a02ca

.field public static final btn_unsorted:I = 0x7f0a02cb

.field public static final btn_update_function:I = 0x7f0a02cc

.field public static final btn_use_now:I = 0x7f0a02cd

.field public static final btn_verify_login_next:I = 0x7f0a02ce

.field public static final btn_verify_new_email_get_verify_code:I = 0x7f0a02cf

.field public static final btn_verify_new_phone_get_verify_code:I = 0x7f0a02d0

.field public static final btn_verify_old_account_get_verify_code:I = 0x7f0a02d1

.field public static final btn_verify_phone_next:I = 0x7f0a02d2

.field public static final btn_view_note:I = 0x7f0a02d3

.field public static final btn_web_offline:I = 0x7f0a02d4

.field public static final btn_web_purchase_test:I = 0x7f0a02d5

.field public static final btn_web_search:I = 0x7f0a02d6

.field public static final btn_web_test:I = 0x7f0a02d7

.field public static final btn_wechat:I = 0x7f0a02d8

.field public static final btn_week:I = 0x7f0a02d9

.field public static final btn_word_edit:I = 0x7f0a02da

.field public static final btn_work_label:I = 0x7f0a02db

.field public static final btn_xiaomi:I = 0x7f0a02dc

.field public static final button:I = 0x7f0a02dd

.field public static final button0:I = 0x7f0a02de

.field public static final button1:I = 0x7f0a02df

.field public static final button2:I = 0x7f0a02e0

.field public static final button3:I = 0x7f0a02e1

.field public static final buttonPanel:I = 0x7f0a02e2

.field public static final button_bar:I = 0x7f0a02e3

.field public static final button_no_watermark:I = 0x7f0a02e4

.field public static final button_share:I = 0x7f0a02e5

.field public static final buttons_colla_dm:I = 0x7f0a02e6

.field public static final buttons_dm:I = 0x7f0a02e7

.field public static final buttons_mm:I = 0x7f0a02e8

.field public static final cache_measures:I = 0x7f0a02e9

.field public static final camera1:I = 0x7f0a02ea

.field public static final camera2:I = 0x7f0a02eb

.field public static final camerah:I = 0x7f0a02ec

.field public static final camerao:I = 0x7f0a02ed

.field public static final camerax:I = 0x7f0a02ee

.field public static final cancel_action:I = 0x7f0a02ef

.field public static final cancel_button:I = 0x7f0a02f0

.field public static final cancel_transferring:I = 0x7f0a02f1

.field public static final capture_camera_view:I = 0x7f0a02f2

.field public static final capture_doc_to_excel_guide:I = 0x7f0a02f3

.field public static final card1:I = 0x7f0a02f4

.field public static final card2:I = 0x7f0a02f5

.field public static final card3:I = 0x7f0a02f6

.field public static final card4:I = 0x7f0a02f7

.field public static final card5:I = 0x7f0a02f8

.field public static final card6:I = 0x7f0a02f9

.field public static final card_ad_tag:I = 0x7f0a02fa

.field public static final card_click_tip:I = 0x7f0a02fb

.field public static final card_close:I = 0x7f0a02fc

.field public static final card_image:I = 0x7f0a02fd

.field public static final card_media:I = 0x7f0a02fe

.field public static final card_view:I = 0x7f0a02ff

.field public static final card_view_content:I = 0x7f0a0300

.field public static final card_view_lottie_container:I = 0x7f0a0301

.field public static final cardview:I = 0x7f0a0302

.field public static final carryVelocity:I = 0x7f0a0303

.field public static final cav_login_tips:I = 0x7f0a0304

.field public static final cb:I = 0x7f0a0305

.field public static final cb_age:I = 0x7f0a0306

.field public static final cb_agree_use_mobile_network:I = 0x7f0a0307

.field public static final cb_agreement_explain_check:I = 0x7f0a0308

.field public static final cb_apply_to_all:I = 0x7f0a0309

.field public static final cb_button:I = 0x7f0a030a

.field public static final cb_button_1:I = 0x7f0a030b

.field public static final cb_button_2:I = 0x7f0a030c

.field public static final cb_button_3:I = 0x7f0a030d

.field public static final cb_check:I = 0x7f0a030e

.field public static final cb_check_never:I = 0x7f0a030f

.field public static final cb_check_state:I = 0x7f0a0310

.field public static final cb_checked:I = 0x7f0a0311

.field public static final cb_choose:I = 0x7f0a0312

.field public static final cb_contracts_check:I = 0x7f0a0313

.field public static final cb_dir_checked:I = 0x7f0a0314

.field public static final cb_dlg_first:I = 0x7f0a0315

.field public static final cb_email_login_pwd_eye:I = 0x7f0a0316

.field public static final cb_email_pwd_show:I = 0x7f0a0317

.field public static final cb_free_trial_enable:I = 0x7f0a0318

.field public static final cb_handwrite_convert:I = 0x7f0a0319

.field public static final cb_info_collect_agreement:I = 0x7f0a031a

.field public static final cb_login_agree:I = 0x7f0a031b

.field public static final cb_login_main_check:I = 0x7f0a031c

.field public static final cb_login_ways_check:I = 0x7f0a031d

.field public static final cb_mobile_number_agree:I = 0x7f0a031e

.field public static final cb_mobile_pwd_login_input_pwd:I = 0x7f0a031f

.field public static final cb_mobile_pwd_login_protocol:I = 0x7f0a0320

.field public static final cb_mobile_use:I = 0x7f0a0321

.field public static final cb_never_remind:I = 0x7f0a0322

.field public static final cb_not_show:I = 0x7f0a0323

.field public static final cb_not_tip:I = 0x7f0a0324

.field public static final cb_only_keep_table:I = 0x7f0a0325

.field public static final cb_only_wifi_right_check:I = 0x7f0a0326

.field public static final cb_phone_pwd_login_eye:I = 0x7f0a0327

.field public static final cb_phone_pwd_login_pwd_eye:I = 0x7f0a0328

.field public static final cb_privacy:I = 0x7f0a0329

.field public static final cb_privacy_agreement:I = 0x7f0a032a

.field public static final cb_protocol_compliant_check:I = 0x7f0a032b

.field public static final cb_protocol_compliant_check_small:I = 0x7f0a032c

.field public static final cb_pwd_show:I = 0x7f0a032d

.field public static final cb_register_agree:I = 0x7f0a032e

.field public static final cb_register_show:I = 0x7f0a032f

.field public static final cb_remove_handwrite:I = 0x7f0a0330

.field public static final cb_remove_stamp:I = 0x7f0a0331

.field public static final cb_reset_pwd_show:I = 0x7f0a0332

.field public static final cb_right_check:I = 0x7f0a0333

.field public static final cb_save_as_default:I = 0x7f0a0334

.field public static final cb_save_result_no_tip:I = 0x7f0a0335

.field public static final cb_select:I = 0x7f0a0336

.field public static final cb_set_pwd_show:I = 0x7f0a0337

.field public static final cb_setting_pwd_pwd_eye:I = 0x7f0a0338

.field public static final cb_share_image_no_prompt:I = 0x7f0a0339

.field public static final cb_text_detection:I = 0x7f0a033a

.field public static final cb_third_net_disk_right_check:I = 0x7f0a033b

.field public static final cb_trim:I = 0x7f0a033c

.field public static final cb_user_agreement:I = 0x7f0a033d

.field public static final cb_wifi_mobile_right_check:I = 0x7f0a033e

.field public static final ccdv_close:I = 0x7f0a033f

.field public static final cdv_count_down:I = 0x7f0a0340

.field public static final cdv_count_down_title:I = 0x7f0a0341

.field public static final cdv_dialog_to_retain_count_down:I = 0x7f0a0342

.field public static final cdv_discount_count_down:I = 0x7f0a0343

.field public static final cdv_discount_purchase_v2_count_down:I = 0x7f0a0344

.field public static final cdv_unsubscribe_recall_count_down:I = 0x7f0a0345

.field public static final center:I = 0x7f0a0346

.field public static final centerCrop:I = 0x7f0a0347

.field public static final centerInside:I = 0x7f0a0348

.field public static final center_horizontal:I = 0x7f0a0349

.field public static final center_vertical:I = 0x7f0a034a

.field public static final certificate_back:I = 0x7f0a034b

.field public static final certificate_import:I = 0x7f0a034c

.field public static final certificate_lottie_view:I = 0x7f0a034d

.field public static final certificate_name:I = 0x7f0a034e

.field public static final certificate_pic:I = 0x7f0a034f

.field public static final certificate_recycler_view:I = 0x7f0a0350

.field public static final certificate_shutter_button:I = 0x7f0a0351

.field public static final certificate_thumb:I = 0x7f0a0352

.field public static final certificate_thumb_num:I = 0x7f0a0353

.field public static final certificate_top:I = 0x7f0a0354

.field public static final certificate_view_container:I = 0x7f0a0355

.field public static final certificate_view_desc:I = 0x7f0a0356

.field public static final certificate_view_execute:I = 0x7f0a0357

.field public static final certificate_view_pic:I = 0x7f0a0358

.field public static final ch_apply_al:I = 0x7f0a0359

.field public static final ch_trim_switch:I = 0x7f0a035a

.field public static final chain:I = 0x7f0a035b

.field public static final chains:I = 0x7f0a035c

.field public static final checkBox_show_pwd_logined:I = 0x7f0a035d

.field public static final check_account:I = 0x7f0a035e

.field public static final checkbox:I = 0x7f0a035f

.field public static final checkbox_card:I = 0x7f0a0360

.field public static final checkbox_doc:I = 0x7f0a0361

.field public static final checkbox_folder:I = 0x7f0a0362

.field public static final checked:I = 0x7f0a0363

.field public static final checkflag:I = 0x7f0a0364

.field public static final chronometer:I = 0x7f0a0365

.field public static final circle_center:I = 0x7f0a0366

.field public static final circle_wave_view:I = 0x7f0a0367

.field public static final cl1:I = 0x7f0a0368

.field public static final cl_add_tag:I = 0x7f0a0369

.field public static final cl_all_docs:I = 0x7f0a036a

.field public static final cl_average_two_root:I = 0x7f0a036b

.field public static final cl_backup:I = 0x7f0a036c

.field public static final cl_backup_card:I = 0x7f0a036d

.field public static final cl_backup_dir_bar:I = 0x7f0a036e

.field public static final cl_body:I = 0x7f0a036f

.field public static final cl_bottom:I = 0x7f0a0370

.field public static final cl_bottom_bar:I = 0x7f0a0371

.field public static final cl_bottom_confirm:I = 0x7f0a0372

.field public static final cl_bottom_content:I = 0x7f0a0373

.field public static final cl_bottom_sheet:I = 0x7f0a0374

.field public static final cl_btm_ope:I = 0x7f0a0375

.field public static final cl_button:I = 0x7f0a0376

.field public static final cl_card:I = 0x7f0a0377

.field public static final cl_card_1:I = 0x7f0a0378

.field public static final cl_card_photo:I = 0x7f0a0379

.field public static final cl_change_mode_tips_root:I = 0x7f0a037a

.field public static final cl_click_ocr:I = 0x7f0a037b

.field public static final cl_cloud:I = 0x7f0a037c

.field public static final cl_cloud_place:I = 0x7f0a037d

.field public static final cl_cloud_switch_only_wifi:I = 0x7f0a037e

.field public static final cl_cloud_switch_wifi_mobile:I = 0x7f0a037f

.field public static final cl_code:I = 0x7f0a0380

.field public static final cl_congratulation:I = 0x7f0a0381

.field public static final cl_container:I = 0x7f0a0382

.field public static final cl_content:I = 0x7f0a0383

.field public static final cl_convert_process:I = 0x7f0a0384

.field public static final cl_convert_word_loading:I = 0x7f0a0385

.field public static final cl_copy:I = 0x7f0a0386

.field public static final cl_count_down:I = 0x7f0a0387

.field public static final cl_cs_ai_tip:I = 0x7f0a0388

.field public static final cl_cut:I = 0x7f0a0389

.field public static final cl_data_security_desc:I = 0x7f0a038a

.field public static final cl_data_show_ori:I = 0x7f0a038b

.field public static final cl_data_show_trans:I = 0x7f0a038c

.field public static final cl_description:I = 0x7f0a038d

.field public static final cl_discount:I = 0x7f0a038e

.field public static final cl_discount_purchase_v2_first_linear:I = 0x7f0a038f

.field public static final cl_discount_purchase_v2_missed_offer:I = 0x7f0a0390

.field public static final cl_discount_purchase_v2_next_24_hours:I = 0x7f0a0391

.field public static final cl_discount_purchase_v2_second_linear:I = 0x7f0a0392

.field public static final cl_doc_info:I = 0x7f0a0393

.field public static final cl_doc_name:I = 0x7f0a0394

.field public static final cl_doc_page_root:I = 0x7f0a0395

.field public static final cl_done_item:I = 0x7f0a0396

.field public static final cl_download_cs_pdf:I = 0x7f0a0397

.field public static final cl_drop_cnl_privilege_root:I = 0x7f0a0398

.field public static final cl_edit_bar:I = 0x7f0a0399

.field public static final cl_ee:I = 0x7f0a039a

.field public static final cl_encrypt_code:I = 0x7f0a039b

.field public static final cl_end:I = 0x7f0a039c

.field public static final cl_enhance_mode_select:I = 0x7f0a039d

.field public static final cl_enterprise_mall:I = 0x7f0a039e

.field public static final cl_extract_text:I = 0x7f0a039f

.field public static final cl_feed_back:I = 0x7f0a03a0

.field public static final cl_file_classification:I = 0x7f0a03a1

.field public static final cl_file_tag:I = 0x7f0a03a2

.field public static final cl_file_type:I = 0x7f0a03a3

.field public static final cl_folder_img:I = 0x7f0a03a4

.field public static final cl_folder_inner_empty:I = 0x7f0a03a5

.field public static final cl_folder_inner_empty_search:I = 0x7f0a03a6

.field public static final cl_folder_inner_header:I = 0x7f0a03a7

.field public static final cl_folder_root_header:I = 0x7f0a03a8

.field public static final cl_free_trail:I = 0x7f0a03a9

.field public static final cl_free_trial:I = 0x7f0a03aa

.field public static final cl_free_trial_enable:I = 0x7f0a03ab

.field public static final cl_free_trial_left:I = 0x7f0a03ac

.field public static final cl_free_trial_right:I = 0x7f0a03ad

.field public static final cl_full_attendance:I = 0x7f0a03ae

.field public static final cl_full_result:I = 0x7f0a03af

.field public static final cl_function:I = 0x7f0a03b0

.field public static final cl_function_btn:I = 0x7f0a03b1

.field public static final cl_function_entrance:I = 0x7f0a03b2

.field public static final cl_function_one:I = 0x7f0a03b3

.field public static final cl_function_three:I = 0x7f0a03b4

.field public static final cl_function_two:I = 0x7f0a03b5

.field public static final cl_functions:I = 0x7f0a03b6

.field public static final cl_gallery:I = 0x7f0a03b7

.field public static final cl_gen:I = 0x7f0a03b8

.field public static final cl_gift_back_content:I = 0x7f0a03b9

.field public static final cl_gift_back_content_end:I = 0x7f0a03ba

.field public static final cl_gift_back_content_middle:I = 0x7f0a03bb

.field public static final cl_gift_back_content_start:I = 0x7f0a03bc

.field public static final cl_gift_one_btn:I = 0x7f0a03bd

.field public static final cl_gift_two_btn:I = 0x7f0a03be

.field public static final cl_guide:I = 0x7f0a03bf

.field public static final cl_guide_cn_normal_desc_container:I = 0x7f0a03c0

.field public static final cl_guide_image:I = 0x7f0a03c1

.field public static final cl_head:I = 0x7f0a03c2

.field public static final cl_head_content:I = 0x7f0a03c3

.field public static final cl_header:I = 0x7f0a03c4

.field public static final cl_header_action:I = 0x7f0a03c5

.field public static final cl_header_content:I = 0x7f0a03c6

.field public static final cl_ic_small_des:I = 0x7f0a03c7

.field public static final cl_image:I = 0x7f0a03c8

.field public static final cl_import_card_guide:I = 0x7f0a03c9

.field public static final cl_import_pdf:I = 0x7f0a03ca

.field public static final cl_introduce:I = 0x7f0a03cb

.field public static final cl_invite_by_app:I = 0x7f0a03cc

.field public static final cl_invite_by_phone_email:I = 0x7f0a03cd

.field public static final cl_invite_type:I = 0x7f0a03ce

.field public static final cl_item_buy:I = 0x7f0a03cf

.field public static final cl_item_check:I = 0x7f0a03d0

.field public static final cl_item_feedback:I = 0x7f0a03d1

.field public static final cl_item_guide:I = 0x7f0a03d2

.field public static final cl_item_negative_premium_top:I = 0x7f0a03d3

.field public static final cl_item_net:I = 0x7f0a03d4

.field public static final cl_item_paper_set:I = 0x7f0a03d5

.field public static final cl_item_repeat:I = 0x7f0a03d6

.field public static final cl_item_root:I = 0x7f0a03d7

.field public static final cl_large_img_take_photo:I = 0x7f0a03d8

.field public static final cl_left:I = 0x7f0a03d9

.field public static final cl_life_time_purchase_left:I = 0x7f0a03da

.field public static final cl_life_time_purchase_middle:I = 0x7f0a03db

.field public static final cl_life_time_purchase_right:I = 0x7f0a03dc

.field public static final cl_local_root:I = 0x7f0a03dd

.field public static final cl_main_content:I = 0x7f0a03de

.field public static final cl_main_doc_menu:I = 0x7f0a03df

.field public static final cl_me_page_area_free_card_root:I = 0x7f0a03e0

.field public static final cl_me_page_bar:I = 0x7f0a03e1

.field public static final cl_me_page_card_container:I = 0x7f0a03e2

.field public static final cl_me_page_card_root:I = 0x7f0a03e3

.field public static final cl_me_page_header_root:I = 0x7f0a03e4

.field public static final cl_me_page_settings_account:I = 0x7f0a03e5

.field public static final cl_middle_content:I = 0x7f0a03e6

.field public static final cl_modify_annotation_style:I = 0x7f0a03e7

.field public static final cl_move_copy_folder_empty:I = 0x7f0a03e8

.field public static final cl_move_copy_header:I = 0x7f0a03e9

.field public static final cl_multi_fun_recommend:I = 0x7f0a03ea

.field public static final cl_new_user_scan_gift:I = 0x7f0a03eb

.field public static final cl_normal_bottom_take_photo:I = 0x7f0a03ec

.field public static final cl_novice_root_view:I = 0x7f0a03ed

.field public static final cl_ocr_edit_top:I = 0x7f0a03ee

.field public static final cl_office_root:I = 0x7f0a03ef

.field public static final cl_on_convert:I = 0x7f0a03f0

.field public static final cl_one:I = 0x7f0a03f1

.field public static final cl_only_read_first_container:I = 0x7f0a03f2

.field public static final cl_only_read_fourth_container:I = 0x7f0a03f3

.field public static final cl_only_read_second_container:I = 0x7f0a03f4

.field public static final cl_only_read_third_container:I = 0x7f0a03f5

.field public static final cl_operate_cam_exam_diversion_root:I = 0x7f0a03f6

.field public static final cl_operation_layout:I = 0x7f0a03f7

.field public static final cl_owner_info:I = 0x7f0a03f8

.field public static final cl_page_1_content:I = 0x7f0a03f9

.field public static final cl_page_1_mark_content:I = 0x7f0a03fa

.field public static final cl_page_2_content:I = 0x7f0a03fb

.field public static final cl_page_list_large_img_bottom_bar:I = 0x7f0a03fc

.field public static final cl_pagelist_ad:I = 0x7f0a03fd

.field public static final cl_pagelist_normal_mode_bottom_bar:I = 0x7f0a03fe

.field public static final cl_personal_folder:I = 0x7f0a03ff

.field public static final cl_pixel_container:I = 0x7f0a0400

.field public static final cl_pixel_root:I = 0x7f0a0401

.field public static final cl_pop_root:I = 0x7f0a0402

.field public static final cl_print_density:I = 0x7f0a0403

.field public static final cl_print_layout:I = 0x7f0a0404

.field public static final cl_print_range:I = 0x7f0a0405

.field public static final cl_print_size:I = 0x7f0a0406

.field public static final cl_product_root:I = 0x7f0a0407

.field public static final cl_prompt:I = 0x7f0a0408

.field public static final cl_public_link:I = 0x7f0a0409

.field public static final cl_public_link_bg:I = 0x7f0a040a

.field public static final cl_purchase_item:I = 0x7f0a040b

.field public static final cl_purchase_page_show_new:I = 0x7f0a040c

.field public static final cl_qr_code_history_linear:I = 0x7f0a040d

.field public static final cl_recommend:I = 0x7f0a040e

.field public static final cl_recommend_area:I = 0x7f0a040f

.field public static final cl_recommend_dir_new:I = 0x7f0a0410

.field public static final cl_recommend_layout:I = 0x7f0a0411

.field public static final cl_recommend_title:I = 0x7f0a0412

.field public static final cl_rejoin_item:I = 0x7f0a0413

.field public static final cl_remove_watermark:I = 0x7f0a0414

.field public static final cl_reward:I = 0x7f0a0415

.field public static final cl_reward_content:I = 0x7f0a0416

.field public static final cl_reward_root:I = 0x7f0a0417

.field public static final cl_right:I = 0x7f0a0418

.field public static final cl_root:I = 0x7f0a0419

.field public static final cl_root_1_gift:I = 0x7f0a041a

.field public static final cl_root_2_award:I = 0x7f0a041b

.field public static final cl_root_3_exit:I = 0x7f0a041c

.field public static final cl_root_capture_restore_guide:I = 0x7f0a041d

.field public static final cl_root_capture_topic_paper_guide:I = 0x7f0a041e

.field public static final cl_root_delete_contact:I = 0x7f0a041f

.field public static final cl_root_linear:I = 0x7f0a0420

.field public static final cl_root_view:I = 0x7f0a0421

.field public static final cl_round_angle_bg:I = 0x7f0a0422

.field public static final cl_scenario_folder_inner_empty:I = 0x7f0a0423

.field public static final cl_search_root:I = 0x7f0a0424

.field public static final cl_select:I = 0x7f0a0425

.field public static final cl_select_contactor_root:I = 0x7f0a0426

.field public static final cl_select_save_path:I = 0x7f0a0427

.field public static final cl_set_default_pdf_app_layout:I = 0x7f0a0428

.field public static final cl_setting_right_txt_line_root:I = 0x7f0a0429

.field public static final cl_share_folder:I = 0x7f0a042a

.field public static final cl_share_item:I = 0x7f0a042b

.field public static final cl_share_wx:I = 0x7f0a042c

.field public static final cl_shortcut:I = 0x7f0a042d

.field public static final cl_storage_permission_tips:I = 0x7f0a042e

.field public static final cl_sync:I = 0x7f0a042f

.field public static final cl_text_layout:I = 0x7f0a0430

.field public static final cl_three:I = 0x7f0a0431

.field public static final cl_time_line_folder_guide:I = 0x7f0a0432

.field public static final cl_tips:I = 0x7f0a0433

.field public static final cl_title:I = 0x7f0a0434

.field public static final cl_to_buy:I = 0x7f0a0435

.field public static final cl_tool_bar:I = 0x7f0a0436

.field public static final cl_top:I = 0x7f0a0437

.field public static final cl_top_half_part_bg:I = 0x7f0a0438

.field public static final cl_top_image_view:I = 0x7f0a0439

.field public static final cl_tp2_average_two_left:I = 0x7f0a043a

.field public static final cl_tp2_average_two_right:I = 0x7f0a043b

.field public static final cl_tp2_five_drop_first_line1_right_bottom:I = 0x7f0a043c

.field public static final cl_tp2_five_drop_first_line1_right_top:I = 0x7f0a043d

.field public static final cl_tp2_five_drop_first_line1_top_left:I = 0x7f0a043e

.field public static final cl_tp2_five_drop_first_line2_left:I = 0x7f0a043f

.field public static final cl_tp2_five_drop_first_line2_right:I = 0x7f0a0440

.field public static final cl_tp2_left_one_right_two_left:I = 0x7f0a0441

.field public static final cl_tp2_left_one_right_two_right_right_bottom:I = 0x7f0a0442

.field public static final cl_tp2_left_one_right_two_right_top:I = 0x7f0a0443

.field public static final cl_tp2_left_two_right_one_left_bottom:I = 0x7f0a0444

.field public static final cl_tp2_left_two_right_one_left_top:I = 0x7f0a0445

.field public static final cl_tp2_left_two_right_one_right:I = 0x7f0a0446

.field public static final cl_tp2_square_bottom_left:I = 0x7f0a0447

.field public static final cl_tp2_square_bottom_right:I = 0x7f0a0448

.field public static final cl_tp2_square_top_left:I = 0x7f0a0449

.field public static final cl_tp2_square_top_right:I = 0x7f0a044a

.field public static final cl_tp2_title_more_root:I = 0x7f0a044b

.field public static final cl_tp2_title_more_title:I = 0x7f0a044c

.field public static final cl_tp2_top_three_bottom_one_bottom:I = 0x7f0a044d

.field public static final cl_tp2_top_three_bottom_one_left:I = 0x7f0a044e

.field public static final cl_tp2_top_three_bottom_one_middle:I = 0x7f0a044f

.field public static final cl_tp2_top_three_bottom_one_right:I = 0x7f0a0450

.field public static final cl_tp2_top_three_bottom_two_bottom_left:I = 0x7f0a0451

.field public static final cl_tp2_top_three_bottom_two_bottom_right:I = 0x7f0a0452

.field public static final cl_tp2_top_three_bottom_two_top_left:I = 0x7f0a0453

.field public static final cl_tp2_top_three_bottom_two_top_middle:I = 0x7f0a0454

.field public static final cl_tp2_top_three_bottom_two_top_right:I = 0x7f0a0455

.field public static final cl_tp_2_square_top_root:I = 0x7f0a0456

.field public static final cl_translate_from_lang:I = 0x7f0a0457

.field public static final cl_trial_renew_main:I = 0x7f0a0458

.field public static final cl_two:I = 0x7f0a0459

.field public static final cl_version:I = 0x7f0a045a

.field public static final cl_vip_card:I = 0x7f0a045b

.field public static final cl_vip_discount:I = 0x7f0a045c

.field public static final cl_vip_gift_back_label:I = 0x7f0a045d

.field public static final cl_vip_rights_label:I = 0x7f0a045e

.field public static final cl_waterfall_root:I = 0x7f0a045f

.field public static final cl_word_flow_file_naming:I = 0x7f0a0460

.field public static final cl_workflow_email_edit:I = 0x7f0a0461

.field public static final cl_workflow_email_end:I = 0x7f0a0462

.field public static final cl_workflow_top_image:I = 0x7f0a0463

.field public static final clear_text:I = 0x7f0a0464

.field public static final clear_water_ink:I = 0x7f0a0465

.field public static final clickRemove:I = 0x7f0a0466

.field public static final clip_horizontal:I = 0x7f0a0467

.field public static final clip_vertical:I = 0x7f0a0468

.field public static final clockwise:I = 0x7f0a0469

.field public static final close:I = 0x7f0a046a

.field public static final close_ad_tv:I = 0x7f0a046b

.field public static final closest:I = 0x7f0a046c

.field public static final cloud_overrun_close:I = 0x7f0a046d

.field public static final cloud_overrun_image:I = 0x7f0a046e

.field public static final cloud_overrun_month:I = 0x7f0a046f

.field public static final cloud_overrun_tips1:I = 0x7f0a0470

.field public static final cloud_overrun_tips2:I = 0x7f0a0471

.field public static final cloud_overrun_title:I = 0x7f0a0472

.field public static final cloud_overrun_year:I = 0x7f0a0473

.field public static final cloud_progress_view:I = 0x7f0a0474

.field public static final cmv_bank_card:I = 0x7f0a0475

.field public static final cmv_certificate:I = 0x7f0a0476

.field public static final cmv_id_card:I = 0x7f0a0477

.field public static final collaborate_mark:I = 0x7f0a0478

.field public static final collaborate_mark_grid:I = 0x7f0a0479

.field public static final collapseActionView:I = 0x7f0a047a

.field public static final color1:I = 0x7f0a047b

.field public static final color2:I = 0x7f0a047c

.field public static final color3:I = 0x7f0a047d

.field public static final color4:I = 0x7f0a047e

.field public static final colorListView:I = 0x7f0a047f

.field public static final color_item:I = 0x7f0a0480

.field public static final color_list:I = 0x7f0a0481

.field public static final color_list_item_content:I = 0x7f0a0482

.field public static final color_list_item_parent:I = 0x7f0a0483

.field public static final color_picker:I = 0x7f0a0484

.field public static final color_picker_signature:I = 0x7f0a0485

.field public static final column:I = 0x7f0a0486

.field public static final column_reverse:I = 0x7f0a0487

.field public static final com_facebook_body_frame:I = 0x7f0a0488

.field public static final com_facebook_button_xout:I = 0x7f0a0489

.field public static final com_facebook_device_auth_instructions:I = 0x7f0a048a

.field public static final com_facebook_fragment_container:I = 0x7f0a048b

.field public static final com_facebook_login_fragment_progress_bar:I = 0x7f0a048c

.field public static final com_facebook_smart_instructions_0:I = 0x7f0a048d

.field public static final com_facebook_smart_instructions_or:I = 0x7f0a048e

.field public static final com_facebook_tooltip_bubble_view_bottom_pointer:I = 0x7f0a048f

.field public static final com_facebook_tooltip_bubble_view_text_body:I = 0x7f0a0490

.field public static final com_facebook_tooltip_bubble_view_top_pointer:I = 0x7f0a0491

.field public static final com_microsoft_aad_adal_editDummyText:I = 0x7f0a0492

.field public static final com_microsoft_aad_adal_progressBar:I = 0x7f0a0493

.field public static final com_microsoft_aad_adal_webView1:I = 0x7f0a0494

.field public static final combine_import_container:I = 0x7f0a0495

.field public static final combine_import_container_root:I = 0x7f0a0496

.field public static final combine_import_doc_container:I = 0x7f0a0497

.field public static final combine_import_doc_container_root:I = 0x7f0a0498

.field public static final combine_photo_container:I = 0x7f0a0499

.field public static final combine_photo_container_root:I = 0x7f0a049a

.field public static final comfirm_bar:I = 0x7f0a049b

.field public static final commit:I = 0x7f0a049c

.field public static final common_auth_webview:I = 0x7f0a049d

.field public static final common_auth_webview_progressbar:I = 0x7f0a049e

.field public static final common_other_login:I = 0x7f0a049f

.field public static final company_name:I = 0x7f0a04a0

.field public static final compatible:I = 0x7f0a04a1

.field public static final compress:I = 0x7f0a04a2

.field public static final confirm_button:I = 0x7f0a04a3

.field public static final confirmation_code:I = 0x7f0a04a4

.field public static final constraint:I = 0x7f0a04a5

.field public static final constraint_hide_toolbar:I = 0x7f0a04a6

.field public static final constraint_image:I = 0x7f0a04a7

.field public static final constraint_layout:I = 0x7f0a04a8

.field public static final constraint_layout_stuff:I = 0x7f0a04a9

.field public static final constraint_show_toolbar:I = 0x7f0a04aa

.field public static final contact:I = 0x7f0a04ab

.field public static final container:I = 0x7f0a04ac

.field public static final content:I = 0x7f0a04ad

.field public static final contentPanel:I = 0x7f0a04ae

.field public static final content_panel:I = 0x7f0a04af

.field public static final content_view:I = 0x7f0a04b0

.field public static final contiguous:I = 0x7f0a04b1

.field public static final continue_button:I = 0x7f0a04b2

.field public static final continuousVelocity:I = 0x7f0a04b3

.field public static final controls_view:I = 0x7f0a04b4

.field public static final coordinator:I = 0x7f0a04b5

.field public static final coordinator_layout:I = 0x7f0a04b6

.field public static final cos:I = 0x7f0a04b7

.field public static final count_number_bottom_view:I = 0x7f0a04b8

.field public static final count_number_ope_view:I = 0x7f0a04b9

.field public static final count_number_view:I = 0x7f0a04ba

.field public static final count_shutter_button:I = 0x7f0a04bb

.field public static final count_shutter_button_import:I = 0x7f0a04bc

.field public static final countdown_view:I = 0x7f0a04bd

.field public static final counterclockwise:I = 0x7f0a04be

.field public static final country_code:I = 0x7f0a04bf

.field public static final cover:I = 0x7f0a04c0

.field public static final cpb_progress:I = 0x7f0a04c1

.field public static final cradle:I = 0x7f0a04c2

.field public static final cs_app_contentview_id:I = 0x7f0a04c3

.field public static final cs_app_floatview_id:I = 0x7f0a04c4

.field public static final cs_media_view_id:I = 0x7f0a04c5

.field public static final cs_purchase_type:I = 0x7f0a04c6

.field public static final cs_switch_view:I = 0x7f0a04c7

.field public static final cs_tag_scrollview:I = 0x7f0a04c8

.field public static final cs_vendor_id:I = 0x7f0a04c9

.field public static final csl_bg:I = 0x7f0a04ca

.field public static final csl_bottom_purchase_new_style1_top_header:I = 0x7f0a04cb

.field public static final csl_bottom_purchase_new_style_1:I = 0x7f0a04cc

.field public static final csl_bottom_purchase_style_1:I = 0x7f0a04cd

.field public static final csl_cloud_get:I = 0x7f0a04ce

.field public static final csl_extra_content:I = 0x7f0a04cf

.field public static final csl_main:I = 0x7f0a04d0

.field public static final csl_month_purchase:I = 0x7f0a04d1

.field public static final csl_no_watermark_count_tip:I = 0x7f0a04d2

.field public static final csl_pdf_get:I = 0x7f0a04d3

.field public static final csl_purchase_new_style2_recyclerview_bottom_view1:I = 0x7f0a04d4

.field public static final csl_purchase_new_style2_recyclerview_bottom_view2:I = 0x7f0a04d5

.field public static final csl_purchase_new_style2_recyclerview_bottom_view3:I = 0x7f0a04d6

.field public static final csl_purchase_new_style2_recyclerview_bottom_view4:I = 0x7f0a04d7

.field public static final csl_purchase_new_style2_recyclerview_bottom_view5:I = 0x7f0a04d8

.field public static final csl_purchase_new_style2_recyclerview_bottom_view6:I = 0x7f0a04d9

.field public static final csl_purchase_new_style2_recyclerview_bottom_view7:I = 0x7f0a04da

.field public static final csl_show_get_free:I = 0x7f0a04db

.field public static final csl_show_get_free_bg:I = 0x7f0a04dc

.field public static final csl_view_bottom:I = 0x7f0a04dd

.field public static final csl_vip_get:I = 0x7f0a04de

.field public static final csl_year_purchase:I = 0x7f0a04df

.field public static final ct_guide_share_dir:I = 0x7f0a04e0

.field public static final ct_ocr_view_pop_tips:I = 0x7f0a04e1

.field public static final ctl_item_layout:I = 0x7f0a04e2

.field public static final ctv_add_page_pop_tips:I = 0x7f0a04e3

.field public static final ctv_all:I = 0x7f0a04e4

.field public static final ctv_custom:I = 0x7f0a04e5

.field public static final ctv_doc_sort_check:I = 0x7f0a04e6

.field public static final ctv_item_negative_premium_video:I = 0x7f0a04e7

.field public static final ctv_me_page_card_validate_time:I = 0x7f0a04e8

.field public static final ctv_pdf_editing_remove_tips:I = 0x7f0a04e9

.field public static final ctv_remove_tips:I = 0x7f0a04ea

.field public static final ctv_revert_book_hint:I = 0x7f0a04eb

.field public static final currentState:I = 0x7f0a04ec

.field public static final custom:I = 0x7f0a04ed

.field public static final customPanel:I = 0x7f0a04ee

.field public static final custom_panel:I = 0x7f0a04ef

.field public static final custom_toolbar:I = 0x7f0a04f0

.field public static final cut:I = 0x7f0a04f1

.field public static final cv_content:I = 0x7f0a04f2

.field public static final cv_default:I = 0x7f0a04f3

.field public static final cv_discount_purchase_v2_current_price:I = 0x7f0a04f4

.field public static final cv_item_0:I = 0x7f0a04f5

.field public static final cv_item_1:I = 0x7f0a04f6

.field public static final cv_lottie_container:I = 0x7f0a04f7

.field public static final cv_notice:I = 0x7f0a04f8

.field public static final cv_photo_img:I = 0x7f0a04f9

.field public static final cv_premium:I = 0x7f0a04fa

.field public static final cv_prestige:I = 0x7f0a04fb

.field public static final cv_recommend_layout:I = 0x7f0a04fc

.field public static final cv_set_default_pdf_app_layout:I = 0x7f0a04fd

.field public static final cv_text_layout:I = 0x7f0a04fe

.field public static final cv_tool_page_square_cell:I = 0x7f0a04ff

.field public static final dark:I = 0x7f0a0500

.field public static final dataBinding:I = 0x7f0a0501

.field public static final date:I = 0x7f0a0502

.field public static final datePicker:I = 0x7f0a0503

.field public static final date_picker_actions:I = 0x7f0a0504

.field public static final dbListView:I = 0x7f0a0505

.field public static final dcheckbox:I = 0x7f0a0506

.field public static final decelerate:I = 0x7f0a0507

.field public static final decelerateAndComplete:I = 0x7f0a0508

.field public static final decor_content_parent:I = 0x7f0a0509

.field public static final default_activity_button:I = 0x7f0a050a

.field public static final deltaRelative:I = 0x7f0a050b

.field public static final demote_common_words:I = 0x7f0a050c

.field public static final demote_rfc822_hostnames:I = 0x7f0a050d

.field public static final dependency_ordering:I = 0x7f0a050e

.field public static final description:I = 0x7f0a050f

.field public static final description_down:I = 0x7f0a0510

.field public static final description_up:I = 0x7f0a0511

.field public static final design_bottom_sheet:I = 0x7f0a0512

.field public static final design_menu_item_action_area:I = 0x7f0a0513

.field public static final design_menu_item_action_area_stub:I = 0x7f0a0514

.field public static final design_menu_item_text:I = 0x7f0a0515

.field public static final design_navigation_view:I = 0x7f0a0516

.field public static final detailImageView:I = 0x7f0a0517

.field public static final dialog_button:I = 0x7f0a0518

.field public static final dialog_discount_icon:I = 0x7f0a0519

.field public static final dialog_gp_guide_mark_new:I = 0x7f0a051a

.field public static final dialog_gp_guide_old_mark:I = 0x7f0a051b

.field public static final dialog_redeem_close:I = 0x7f0a051c

.field public static final dialog_redeem_des1:I = 0x7f0a051d

.field public static final dialog_redeem_des2:I = 0x7f0a051e

.field public static final dialog_redeem_des3:I = 0x7f0a051f

.field public static final dialog_redeem_give_up_or_month_layout:I = 0x7f0a0520

.field public static final dialog_redeem_give_up_or_month_txt:I = 0x7f0a0521

.field public static final dialog_redeem_subtitle:I = 0x7f0a0522

.field public static final dialog_redeem_title:I = 0x7f0a0523

.field public static final dialog_redeem_year_arrow:I = 0x7f0a0524

.field public static final dialog_redeem_year_style:I = 0x7f0a0525

.field public static final dialog_redeem_year_style_layout:I = 0x7f0a0526

.field public static final dialog_renewal_year_tips_2:I = 0x7f0a0527

.field public static final dialog_view:I = 0x7f0a0528

.field public static final dicon:I = 0x7f0a0529

.field public static final dimensions:I = 0x7f0a052a

.field public static final direct:I = 0x7f0a052b

.field public static final disableHome:I = 0x7f0a052c

.field public static final disableIntraAutoTransition:I = 0x7f0a052d

.field public static final disablePostScroll:I = 0x7f0a052e

.field public static final disableScroll:I = 0x7f0a052f

.field public static final disjoint:I = 0x7f0a0530

.field public static final dispatch_layout:I = 0x7f0a0531

.field public static final dispatch_layout_end:I = 0x7f0a0532

.field public static final dispatch_layout_start:I = 0x7f0a0533

.field public static final display_always:I = 0x7f0a0534

.field public static final divider:I = 0x7f0a0535

.field public static final divider1:I = 0x7f0a0536

.field public static final divider2:I = 0x7f0a0537

.field public static final divider3:I = 0x7f0a0538

.field public static final divider4:I = 0x7f0a0539

.field public static final divider_btm:I = 0x7f0a053a

.field public static final divider_share_dir:I = 0x7f0a053b

.field public static final dl_intercept:I = 0x7f0a053c

.field public static final dll_container:I = 0x7f0a053d

.field public static final dlock:I = 0x7f0a053e

.field public static final doc_colla_select_do_more:I = 0x7f0a053f

.field public static final doc_copy:I = 0x7f0a0540

.field public static final doc_gridview:I = 0x7f0a0541

.field public static final doc_listview:I = 0x7f0a0542

.field public static final doc_multi_delete:I = 0x7f0a0543

.field public static final doc_multi_lock:I = 0x7f0a0544

.field public static final doc_multi_share:I = 0x7f0a0545

.field public static final doc_multi_tag:I = 0x7f0a0546

.field public static final doc_name:I = 0x7f0a0547

.field public static final doc_saveto_gallery:I = 0x7f0a0548

.field public static final doc_select_do_more:I = 0x7f0a0549

.field public static final doc_title_bar:I = 0x7f0a054a

.field public static final doc_upload:I = 0x7f0a054b

.field public static final dot:I = 0x7f0a054c

.field public static final dot_connect:I = 0x7f0a054d

.field public static final dot_state:I = 0x7f0a054e

.field public static final dots_indicator:I = 0x7f0a054f

.field public static final dowload_apk:I = 0x7f0a0550

.field public static final downtoup:I = 0x7f0a0551

.field public static final dpagenum:I = 0x7f0a0552

.field public static final draft_view:I = 0x7f0a0553

.field public static final dragAnticlockwise:I = 0x7f0a0554

.field public static final dragClockwise:I = 0x7f0a0555

.field public static final dragDown:I = 0x7f0a0556

.field public static final dragEnd:I = 0x7f0a0557

.field public static final dragLeft:I = 0x7f0a0558

.field public static final dragRight:I = 0x7f0a0559

.field public static final dragStart:I = 0x7f0a055a

.field public static final dragUp:I = 0x7f0a055b

.field public static final drag_image:I = 0x7f0a055c

.field public static final drawView:I = 0x7f0a055d

.field public static final drawViewGroup:I = 0x7f0a055e

.field public static final drawer_layout:I = 0x7f0a055f

.field public static final dropbox_username:I = 0x7f0a0560

.field public static final dropdown_menu:I = 0x7f0a0561

.field public static final dtext:I = 0x7f0a0562

.field public static final dts_workflow_email_switcher:I = 0x7f0a0563

.field public static final dual_screen_content:I = 0x7f0a0564

.field public static final dual_screen_empty_view:I = 0x7f0a0565

.field public static final dual_screen_layout:I = 0x7f0a0566

.field public static final easeIn:I = 0x7f0a0567

.field public static final easeInOut:I = 0x7f0a0568

.field public static final easeOut:I = 0x7f0a0569

.field public static final east:I = 0x7f0a056a

.field public static final ed_name:I = 0x7f0a056b

.field public static final ed_password:I = 0x7f0a056c

.field public static final ed_title:I = 0x7f0a056d

.field public static final edge:I = 0x7f0a056e

.field public static final editPassword:I = 0x7f0a056f

.field public static final editText_text:I = 0x7f0a0570

.field public static final editUserName:I = 0x7f0a0571

.field public static final edit_email:I = 0x7f0a0572

.field public static final edit_query:I = 0x7f0a0573

.field public static final edit_search:I = 0x7f0a0574

.field public static final edit_text_view:I = 0x7f0a0575

.field public static final edt_halfpack_content:I = 0x7f0a0576

.field public static final el_word_flow_auto_send_email:I = 0x7f0a0577

.field public static final el_word_flow_cloud_storage:I = 0x7f0a0578

.field public static final el_work_flow_upload_third_net_disk:I = 0x7f0a0579

.field public static final elastic:I = 0x7f0a057a

.field public static final ell_certificate_detail_card_detail:I = 0x7f0a057b

.field public static final email:I = 0x7f0a057c

.field public static final email_report_tv:I = 0x7f0a057d

.field public static final embed:I = 0x7f0a057e

.field public static final empty_text:I = 0x7f0a057f

.field public static final empty_view:I = 0x7f0a0580

.field public static final end:I = 0x7f0a0581

.field public static final endToStart:I = 0x7f0a0582

.field public static final end_barrier:I = 0x7f0a0583

.field public static final end_padder:I = 0x7f0a0584

.field public static final enhance_bar_btn:I = 0x7f0a0585

.field public static final enhance_modes_group:I = 0x7f0a0586

.field public static final enterAlways:I = 0x7f0a0587

.field public static final enterAlwaysCollapsed:I = 0x7f0a0588

.field public static final entrance_collage:I = 0x7f0a0589

.field public static final entrance_pdf:I = 0x7f0a058a

.field public static final errorTextView:I = 0x7f0a058b

.field public static final et_account_name:I = 0x7f0a058c

.field public static final et_area_code_confirm_phone_number:I = 0x7f0a058d

.field public static final et_bind_account:I = 0x7f0a058e

.field public static final et_change_account_add_nickname:I = 0x7f0a058f

.field public static final et_container:I = 0x7f0a0590

.field public static final et_count:I = 0x7f0a0591

.field public static final et_descriptiom:I = 0x7f0a0592

.field public static final et_device_id:I = 0x7f0a0593

.field public static final et_email:I = 0x7f0a0594

.field public static final et_email_login_password:I = 0x7f0a0595

.field public static final et_email_login_pwd:I = 0x7f0a0596

.field public static final et_feedback_comment:I = 0x7f0a0597

.field public static final et_feedback_email:I = 0x7f0a0598

.field public static final et_feedback_mail:I = 0x7f0a0599

.field public static final et_filter:I = 0x7f0a059a

.field public static final et_input_new_phone_number:I = 0x7f0a059b

.field public static final et_invite_code:I = 0x7f0a059c

.field public static final et_invoice_check_edit:I = 0x7f0a059d

.field public static final et_invoice_check_type:I = 0x7f0a059e

.field public static final et_invoice_txt_edit:I = 0x7f0a059f

.field public static final et_link:I = 0x7f0a05a0

.field public static final et_locale:I = 0x7f0a05a1

.field public static final et_login_account:I = 0x7f0a05a2

.field public static final et_lr_table:I = 0x7f0a05a3

.field public static final et_name:I = 0x7f0a05a4

.field public static final et_ocr_result:I = 0x7f0a05a5

.field public static final et_origin:I = 0x7f0a05a6

.field public static final et_other:I = 0x7f0a05a7

.field public static final et_other_cards:I = 0x7f0a05a8

.field public static final et_page_waiting_time:I = 0x7f0a05a9

.field public static final et_params:I = 0x7f0a05aa

.field public static final et_phone:I = 0x7f0a05ab

.field public static final et_phone_number:I = 0x7f0a05ac

.field public static final et_phone_pwd_login:I = 0x7f0a05ad

.field public static final et_phone_pwd_login_password:I = 0x7f0a05ae

.field public static final et_pwd:I = 0x7f0a05af

.field public static final et_search:I = 0x7f0a05b0

.field public static final et_set_pwd:I = 0x7f0a05b1

.field public static final et_setting_pwd_password:I = 0x7f0a05b2

.field public static final et_start_index:I = 0x7f0a05b3

.field public static final et_super_vcode_validate_input:I = 0x7f0a05b4

.field public static final et_text:I = 0x7f0a05b5

.field public static final et_text_annotation:I = 0x7f0a05b6

.field public static final et_value:I = 0x7f0a05b7

.field public static final et_verify_code:I = 0x7f0a05b8

.field public static final et_verify_code_input:I = 0x7f0a05b9

.field public static final et_web_url_test_url:I = 0x7f0a05ba

.field public static final et_whitelist:I = 0x7f0a05bb

.field public static final et_x:I = 0x7f0a05bc

.field public static final et_y:I = 0x7f0a05bd

.field public static final et_z:I = 0x7f0a05be

.field public static final etx_faxnumber:I = 0x7f0a05bf

.field public static final evaluate_container:I = 0x7f0a05c0

.field public static final evidence_List:I = 0x7f0a05c1

.field public static final evidence_back:I = 0x7f0a05c2

.field public static final evidence_shutter_button:I = 0x7f0a05c3

.field public static final excel_history:I = 0x7f0a05c4

.field public static final excel_import:I = 0x7f0a05c5

.field public static final excel_shutter_button:I = 0x7f0a05c6

.field public static final exitUntilCollapsed:I = 0x7f0a05c7

.field public static final exit_enhance:I = 0x7f0a05c8

.field public static final exit_multi:I = 0x7f0a05c9

.field public static final exo_check:I = 0x7f0a05ca

.field public static final exo_icon:I = 0x7f0a05cb

.field public static final exo_main_text:I = 0x7f0a05cc

.field public static final exo_settings_listview:I = 0x7f0a05cd

.field public static final exo_sub_text:I = 0x7f0a05ce

.field public static final exo_text:I = 0x7f0a05cf

.field public static final exo_track_selection_view:I = 0x7f0a05d0

.field public static final expand_activities_button:I = 0x7f0a05d1

.field public static final expanded_menu:I = 0x7f0a05d2

.field public static final experience_now:I = 0x7f0a05d3

.field public static final explanation:I = 0x7f0a05d4

.field public static final fab_add_doc:I = 0x7f0a05d5

.field public static final fab_add_doc_test:I = 0x7f0a05d6

.field public static final fab_add_tag:I = 0x7f0a05d7

.field public static final fab_capture:I = 0x7f0a05d8

.field public static final fab_import_file:I = 0x7f0a05d9

.field public static final fab_lock:I = 0x7f0a05da

.field public static final fab_ocr:I = 0x7f0a05db

.field public static final fab_take_photo:I = 0x7f0a05dc

.field public static final face_book_media_view:I = 0x7f0a05dd

.field public static final facebookFollowTextView:I = 0x7f0a05de

.field public static final fade:I = 0x7f0a05df

.field public static final fax_balance_refresh:I = 0x7f0a05e0

.field public static final fax_number:I = 0x7f0a05e1

.field public static final fax_pages:I = 0x7f0a05e2

.field public static final fax_state:I = 0x7f0a05e3

.field public static final fax_time:I = 0x7f0a05e4

.field public static final fileImageView:I = 0x7f0a05e5

.field public static final fileRadioButton:I = 0x7f0a05e6

.field public static final fileTextView:I = 0x7f0a05e7

.field public static final fill:I = 0x7f0a05e8

.field public static final fillCenter:I = 0x7f0a05e9

.field public static final fillEnd:I = 0x7f0a05ea

.field public static final fillStart:I = 0x7f0a05eb

.field public static final fill_horizontal:I = 0x7f0a05ec

.field public static final fill_vertical:I = 0x7f0a05ed

.field public static final filled:I = 0x7f0a05ee

.field public static final fit:I = 0x7f0a05ef

.field public static final fitCenter:I = 0x7f0a05f0

.field public static final fitEnd:I = 0x7f0a05f1

.field public static final fitStart:I = 0x7f0a05f2

.field public static final fitToContents:I = 0x7f0a05f3

.field public static final fitXY:I = 0x7f0a05f4

.field public static final fixed:I = 0x7f0a05f5

.field public static final fixed_height:I = 0x7f0a05f6

.field public static final fixed_width:I = 0x7f0a05f7

.field public static final fl_ai_des:I = 0x7f0a05f8

.field public static final fl_ai_icon:I = 0x7f0a05f9

.field public static final fl_api_log:I = 0x7f0a05fa

.field public static final fl_banner_container:I = 0x7f0a05fb

.field public static final fl_bottom:I = 0x7f0a05fc

.field public static final fl_bottom_layout:I = 0x7f0a05fd

.field public static final fl_bottom_thumb:I = 0x7f0a05fe

.field public static final fl_capture_guide_root:I = 0x7f0a05ff

.field public static final fl_capture_middle_container:I = 0x7f0a0600

.field public static final fl_capture_orientation_auto:I = 0x7f0a0601

.field public static final fl_capture_orientation_horizontal:I = 0x7f0a0602

.field public static final fl_capture_orientation_vertical:I = 0x7f0a0603

.field public static final fl_capture_preview_container:I = 0x7f0a0604

.field public static final fl_capture_restore_guide:I = 0x7f0a0605

.field public static final fl_certificate_detail:I = 0x7f0a0606

.field public static final fl_certificate_photo:I = 0x7f0a0607

.field public static final fl_certificate_thumb:I = 0x7f0a0608

.field public static final fl_common_frame_layout:I = 0x7f0a0609

.field public static final fl_container:I = 0x7f0a060a

.field public static final fl_content:I = 0x7f0a060b

.field public static final fl_correction:I = 0x7f0a060c

.field public static final fl_discount_purchase_v2:I = 0x7f0a060d

.field public static final fl_doc:I = 0x7f0a060e

.field public static final fl_double_page_assist:I = 0x7f0a060f

.field public static final fl_edit_account_number:I = 0x7f0a0610

.field public static final fl_edit_container:I = 0x7f0a0611

.field public static final fl_edit_email_login_pwd:I = 0x7f0a0612

.field public static final fl_edit_phone_pwd:I = 0x7f0a0613

.field public static final fl_email:I = 0x7f0a0614

.field public static final fl_email_input:I = 0x7f0a0615

.field public static final fl_email_login_account:I = 0x7f0a0616

.field public static final fl_email_pwd:I = 0x7f0a0617

.field public static final fl_enhance_mode:I = 0x7f0a0618

.field public static final fl_enhance_state:I = 0x7f0a0619

.field public static final fl_excel_thumb:I = 0x7f0a061a

.field public static final fl_export_word:I = 0x7f0a061b

.field public static final fl_export_word_excel:I = 0x7f0a061c

.field public static final fl_filter_container:I = 0x7f0a061d

.field public static final fl_filter_cover:I = 0x7f0a061e

.field public static final fl_fot:I = 0x7f0a061f

.field public static final fl_fragment_content:I = 0x7f0a0620

.field public static final fl_frame_container:I = 0x7f0a0621

.field public static final fl_google:I = 0x7f0a0622

.field public static final fl_guid_container_new:I = 0x7f0a0623

.field public static final fl_guide:I = 0x7f0a0624

.field public static final fl_guide_gp_last_page_container:I = 0x7f0a0625

.field public static final fl_guide_purchase_page_container:I = 0x7f0a0626

.field public static final fl_header_view:I = 0x7f0a0627

.field public static final fl_image_share:I = 0x7f0a0628

.field public static final fl_input_dir_name:I = 0x7f0a0629

.field public static final fl_input_doc_name:I = 0x7f0a062a

.field public static final fl_known:I = 0x7f0a062b

.field public static final fl_ko_agreement:I = 0x7f0a062c

.field public static final fl_login_input:I = 0x7f0a062d

.field public static final fl_login_main:I = 0x7f0a062e

.field public static final fl_logo:I = 0x7f0a062f

.field public static final fl_mobile_number_input:I = 0x7f0a0630

.field public static final fl_mobile_pwd_login_input_number:I = 0x7f0a0631

.field public static final fl_mobile_pwd_login_input_pwd:I = 0x7f0a0632

.field public static final fl_multi_thumb:I = 0x7f0a0633

.field public static final fl_negative_premium:I = 0x7f0a0634

.field public static final fl_next:I = 0x7f0a0635

.field public static final fl_ocr_edit:I = 0x7f0a0636

.field public static final fl_ocr_edit_bottom_keyboard_btn:I = 0x7f0a0637

.field public static final fl_ocr_rec_root:I = 0x7f0a0638

.field public static final fl_ocr_result_img_root:I = 0x7f0a0639

.field public static final fl_ocr_thumb:I = 0x7f0a063a

.field public static final fl_ocr_title_container:I = 0x7f0a063b

.field public static final fl_office:I = 0x7f0a063c

.field public static final fl_operation:I = 0x7f0a063d

.field public static final fl_page_item_loading:I = 0x7f0a063e

.field public static final fl_pdf_editing_root:I = 0x7f0a063f

.field public static final fl_phone:I = 0x7f0a0640

.field public static final fl_preview_layout:I = 0x7f0a0641

.field public static final fl_previous:I = 0x7f0a0642

.field public static final fl_receive_btn:I = 0x7f0a0643

.field public static final fl_register_input:I = 0x7f0a0644

.field public static final fl_register_protocol:I = 0x7f0a0645

.field public static final fl_register_pwd:I = 0x7f0a0646

.field public static final fl_reset_pwd:I = 0x7f0a0647

.field public static final fl_root:I = 0x7f0a0648

.field public static final fl_root_book_thumb:I = 0x7f0a0649

.field public static final fl_root_view:I = 0x7f0a064a

.field public static final fl_save_colorpicker:I = 0x7f0a064b

.field public static final fl_search:I = 0x7f0a064c

.field public static final fl_set_pwd:I = 0x7f0a064d

.field public static final fl_setting_container:I = 0x7f0a064e

.field public static final fl_settings_drop_container:I = 0x7f0a064f

.field public static final fl_settings_main:I = 0x7f0a0650

.field public static final fl_share_title:I = 0x7f0a0651

.field public static final fl_suffix:I = 0x7f0a0652

.field public static final fl_tag_container:I = 0x7f0a0653

.field public static final fl_tags_before_expand:I = 0x7f0a0654

.field public static final fl_toolbar:I = 0x7f0a0655

.field public static final fl_top:I = 0x7f0a0656

.field public static final fl_topic_container:I = 0x7f0a0657

.field public static final fl_topic_thumb:I = 0x7f0a0658

.field public static final fl_translate_thumb:I = 0x7f0a0659

.field public static final fl_user_guide_mask_view:I = 0x7f0a065a

.field public static final fl_vip_card:I = 0x7f0a065b

.field public static final fl_word_content:I = 0x7f0a065c

.field public static final fl_word_thumb:I = 0x7f0a065d

.field public static final flex_end:I = 0x7f0a065e

.field public static final flex_start:I = 0x7f0a065f

.field public static final flingRemove:I = 0x7f0a0660

.field public static final flip:I = 0x7f0a0661

.field public static final float_action_view:I = 0x7f0a0662

.field public static final float_name:I = 0x7f0a0663

.field public static final floating:I = 0x7f0a0664

.field public static final flow_layout:I = 0x7f0a0665

.field public static final flush_view:I = 0x7f0a0666

.field public static final focusMarkerContainer:I = 0x7f0a0667

.field public static final focus_indicator:I = 0x7f0a0668

.field public static final focus_indicator_rotate_layout:I = 0x7f0a0669

.field public static final font_metrics:I = 0x7f0a066a

.field public static final forever:I = 0x7f0a066b

.field public static final forget_psw:I = 0x7f0a066c

.field public static final format_name:I = 0x7f0a066d

.field public static final frag_main_hint:I = 0x7f0a066e

.field public static final frag_main_top:I = 0x7f0a066f

.field public static final fragment_books_result:I = 0x7f0a0670

.field public static final fragment_container:I = 0x7f0a0671

.field public static final fragment_container_id:I = 0x7f0a0672

.field public static final fragment_container_view_tag:I = 0x7f0a0673

.field public static final fragment_invoice_check_list:I = 0x7f0a0674

.field public static final frame:I = 0x7f0a0675

.field public static final frameLayout:I = 0x7f0a0676

.field public static final frame_doc_fragment:I = 0x7f0a0677

.field public static final frame_fragment:I = 0x7f0a0678

.field public static final frame_parent:I = 0x7f0a0679

.field public static final front:I = 0x7f0a067a

.field public static final frost:I = 0x7f0a067b

.field public static final fullscreen_header:I = 0x7f0a067c

.field public static final function_container_high:I = 0x7f0a067d

.field public static final function_container_low:I = 0x7f0a067e

.field public static final function_ocr:I = 0x7f0a067f

.field public static final g_content_label_end:I = 0x7f0a0680

.field public static final g_content_title01_end:I = 0x7f0a0681

.field public static final g_free_trail_vip_left:I = 0x7f0a0682

.field public static final g_free_trail_vip_right:I = 0x7f0a0683

.field public static final g_gift_one_text_end:I = 0x7f0a0684

.field public static final g_gift_one_text_start:I = 0x7f0a0685

.field public static final g_gift_two_text_bottom:I = 0x7f0a0686

.field public static final g_gift_two_text_end:I = 0x7f0a0687

.field public static final g_gift_two_text_middle:I = 0x7f0a0688

.field public static final g_gift_two_text_start:I = 0x7f0a0689

.field public static final g_gift_two_text_top:I = 0x7f0a068a

.field public static final galaxy:I = 0x7f0a068b

.field public static final galaxy_rv:I = 0x7f0a068c

.field public static final gbmv_mask_view_excel:I = 0x7f0a068d

.field public static final gdoc_username:I = 0x7f0a068e

.field public static final get_de_moire_msg:I = 0x7f0a068f

.field public static final gfv_content:I = 0x7f0a0690

.field public static final gfv_lr:I = 0x7f0a0691

.field public static final ghost_view:I = 0x7f0a0692

.field public static final ghost_view_holder:I = 0x7f0a0693

.field public static final gl_average_two_left:I = 0x7f0a0694

.field public static final gl_average_two_right:I = 0x7f0a0695

.field public static final gl_bg_end:I = 0x7f0a0696

.field public static final gl_bg_start:I = 0x7f0a0697

.field public static final gl_bottom_right:I = 0x7f0a0698

.field public static final gl_content_end:I = 0x7f0a0699

.field public static final gl_content_start:I = 0x7f0a069a

.field public static final gl_end:I = 0x7f0a069b

.field public static final gl_left_button_end:I = 0x7f0a069c

.field public static final gl_left_button_start:I = 0x7f0a069d

.field public static final gl_middle:I = 0x7f0a069e

.field public static final gl_right_button_end:I = 0x7f0a069f

.field public static final gl_right_button_start:I = 0x7f0a06a0

.field public static final gl_tp2_five_drop_first_line2_left:I = 0x7f0a06a1

.field public static final gl_tp2_five_drop_first_line2_right:I = 0x7f0a06a2

.field public static final gl_tp2_top_three_bottom_one_bottom:I = 0x7f0a06a3

.field public static final gl_tp2_top_three_bottom_two_bottom_left:I = 0x7f0a06a4

.field public static final gl_tp_2_square_bottom_left:I = 0x7f0a06a5

.field public static final gl_tp_2_square_bottom_right:I = 0x7f0a06a6

.field public static final gl_tp_2_square_line1_right_bottom:I = 0x7f0a06a7

.field public static final gl_tp_2_square_right_bottom:I = 0x7f0a06a8

.field public static final gl_tp_2_square_right_top:I = 0x7f0a06a9

.field public static final gl_tp_2_square_top_left:I = 0x7f0a06aa

.field public static final gl_tp_2_square_top_right:I = 0x7f0a06ab

.field public static final gl_v1:I = 0x7f0a06ac

.field public static final gl_v2:I = 0x7f0a06ad

.field public static final gl_v3:I = 0x7f0a06ae

.field public static final gl_v4:I = 0x7f0a06af

.field public static final glide_custom_view_target_tag:I = 0x7f0a06b0

.field public static final glide_tag:I = 0x7f0a06b1

.field public static final go_to_main:I = 0x7f0a06b2

.field public static final gone:I = 0x7f0a06b3

.field public static final google_media_view:I = 0x7f0a06b4

.field public static final gp_guide_mark_five_starts:I = 0x7f0a06b5

.field public static final gp_guide_mark_five_starts_new:I = 0x7f0a06b6

.field public static final graph:I = 0x7f0a06b7

.field public static final graph_wrap:I = 0x7f0a06b8

.field public static final greet_card_shutter_button:I = 0x7f0a06b9

.field public static final grid_dlgshares_other:I = 0x7f0a06ba

.field public static final grid_dlgshares_recent:I = 0x7f0a06bb

.field public static final grid_enhance:I = 0x7f0a06bc

.field public static final grid_top:I = 0x7f0a06bd

.field public static final gridview:I = 0x7f0a06be

.field public static final group:I = 0x7f0a06bf

.field public static final group1:I = 0x7f0a06c0

.field public static final group2:I = 0x7f0a06c1

.field public static final group_01:I = 0x7f0a06c2

.field public static final group_02:I = 0x7f0a06c3

.field public static final group_animate:I = 0x7f0a06c4

.field public static final group_bottom:I = 0x7f0a06c5

.field public static final group_btm_bar:I = 0x7f0a06c6

.field public static final group_content:I = 0x7f0a06c7

.field public static final group_delete:I = 0x7f0a06c8

.field public static final group_discount:I = 0x7f0a06c9

.field public static final group_divider:I = 0x7f0a06ca

.field public static final group_doc_info:I = 0x7f0a06cb

.field public static final group_empty:I = 0x7f0a06cc

.field public static final group_fifth:I = 0x7f0a06cd

.field public static final group_first:I = 0x7f0a06ce

.field public static final group_fourth:I = 0x7f0a06cf

.field public static final group_free_trial:I = 0x7f0a06d0

.field public static final group_gift:I = 0x7f0a06d1

.field public static final group_invite:I = 0x7f0a06d2

.field public static final group_item_me_page_header_progressbar:I = 0x7f0a06d3

.field public static final group_line1:I = 0x7f0a06d4

.field public static final group_line2:I = 0x7f0a06d5

.field public static final group_line3:I = 0x7f0a06d6

.field public static final group_login:I = 0x7f0a06d7

.field public static final group_no:I = 0x7f0a06d8

.field public static final group_not_only_copy:I = 0x7f0a06d9

.field public static final group_ocr_result:I = 0x7f0a06da

.field public static final group_privacy:I = 0x7f0a06db

.field public static final group_protocol:I = 0x7f0a06dc

.field public static final group_retry:I = 0x7f0a06dd

.field public static final group_right_action:I = 0x7f0a06de

.field public static final group_second:I = 0x7f0a06df

.field public static final group_share:I = 0x7f0a06e0

.field public static final group_share_dir_info:I = 0x7f0a06e1

.field public static final group_share_wx:I = 0x7f0a06e2

.field public static final group_signgroups:I = 0x7f0a06e3

.field public static final group_single_paw_way:I = 0x7f0a06e4

.field public static final group_third:I = 0x7f0a06e5

.field public static final group_toolbar:I = 0x7f0a06e6

.field public static final group_top_dir:I = 0x7f0a06e7

.field public static final group_un_login:I = 0x7f0a06e8

.field public static final grouping:I = 0x7f0a06e9

.field public static final groups:I = 0x7f0a06ea

.field public static final gtv_cs_ai_titles:I = 0x7f0a06eb

.field public static final guidePages:I = 0x7f0a06ec

.field public static final guide_animation_view:I = 0x7f0a06ed

.field public static final guide_bottom:I = 0x7f0a06ee

.field public static final guide_center:I = 0x7f0a06ef

.field public static final guide_cn_purchase7_page_bottom:I = 0x7f0a06f0

.field public static final guide_cn_purchase8_page_bottom:I = 0x7f0a06f1

.field public static final guide_cn_purchase_page_bottom:I = 0x7f0a06f2

.field public static final guide_divider:I = 0x7f0a06f3

.field public static final guide_left:I = 0x7f0a06f4

.field public static final guide_line:I = 0x7f0a06f5

.field public static final guide_mask_view:I = 0x7f0a06f6

.field public static final guide_middle:I = 0x7f0a06f7

.field public static final guide_pages_image:I = 0x7f0a06f8

.field public static final guide_right:I = 0x7f0a06f9

.field public static final guide_shutter:I = 0x7f0a06fa

.field public static final guide_top:I = 0x7f0a06fb

.field public static final guideline:I = 0x7f0a06fc

.field public static final guideline1:I = 0x7f0a06fd

.field public static final guideline2:I = 0x7f0a06fe

.field public static final guideline_end:I = 0x7f0a06ff

.field public static final guideline_start:I = 0x7f0a0700

.field public static final hardware:I = 0x7f0a0701

.field public static final header_card_view:I = 0x7f0a0702

.field public static final header_title:I = 0x7f0a0703

.field public static final hideable:I = 0x7f0a0704

.field public static final hl_time:I = 0x7f0a0705

.field public static final hlrAnimView:I = 0x7f0a0706

.field public static final home:I = 0x7f0a0707

.field public static final homeAsUp:I = 0x7f0a0708

.field public static final home_banner:I = 0x7f0a0709

.field public static final honorRequest:I = 0x7f0a070a

.field public static final horizontal:I = 0x7f0a070b

.field public static final horizontalScrollView:I = 0x7f0a070c

.field public static final horizontal_guideline:I = 0x7f0a070d

.field public static final horizontal_list:I = 0x7f0a070e

.field public static final horizontal_only:I = 0x7f0a070f

.field public static final hpv_content:I = 0x7f0a0710

.field public static final hs_function:I = 0x7f0a0711

.field public static final hsvButtons:I = 0x7f0a0712

.field public static final hsv_bottom_bar:I = 0x7f0a0713

.field public static final hsv_enhance_bottom:I = 0x7f0a0714

.field public static final html:I = 0x7f0a0715

.field public static final i_know:I = 0x7f0a0716

.field public static final ib_close:I = 0x7f0a0717

.field public static final ic_check_compliance:I = 0x7f0a0718

.field public static final ic_point_fifth:I = 0x7f0a0719

.field public static final ic_point_first:I = 0x7f0a071a

.field public static final ic_point_fourth:I = 0x7f0a071b

.field public static final ic_point_second:I = 0x7f0a071c

.field public static final ic_point_third:I = 0x7f0a071d

.field public static final ic_trial_renew_full_attendance_7d:I = 0x7f0a071e

.field public static final icon:I = 0x7f0a071f

.field public static final icon_container:I = 0x7f0a0720

.field public static final icon_group:I = 0x7f0a0721

.field public static final icon_only:I = 0x7f0a0722

.field public static final icon_rv:I = 0x7f0a0723

.field public static final icon_uri:I = 0x7f0a0724

.field public static final icon_view:I = 0x7f0a0725

.field public static final ifRoom:I = 0x7f0a0726

.field public static final ignore:I = 0x7f0a0727

.field public static final ignoreRequest:I = 0x7f0a0728

.field public static final il_enhance_mode_select:I = 0x7f0a0729

.field public static final il_main_doc_header_view:I = 0x7f0a072a

.field public static final il_recommend_dir:I = 0x7f0a072b

.field public static final il_set_default_backup_tip:I = 0x7f0a072c

.field public static final il_set_default_pdf_app_tip:I = 0x7f0a072d

.field public static final il_time_line_default_middle_view:I = 0x7f0a072e

.field public static final il_time_line_office_middle_view:I = 0x7f0a072f

.field public static final image:I = 0x7f0a0730

.field public static final imageButton_twitter:I = 0x7f0a0731

.field public static final imageView:I = 0x7f0a0732

.field public static final imageView_note:I = 0x7f0a0733

.field public static final imageView_share_snap:I = 0x7f0a0734

.field public static final imageView_sns_icon:I = 0x7f0a0735

.field public static final image_adjust:I = 0x7f0a0736

.field public static final image_area:I = 0x7f0a0737

.field public static final image_arrow:I = 0x7f0a0738

.field public static final image_close_stroke_size:I = 0x7f0a0739

.field public static final image_delete:I = 0x7f0a073a

.field public static final image_edit:I = 0x7f0a073b

.field public static final image_eraser:I = 0x7f0a073c

.field public static final image_finish:I = 0x7f0a073d

.field public static final image_lasso:I = 0x7f0a073e

.field public static final image_ocr:I = 0x7f0a073f

.field public static final image_ocr_btn:I = 0x7f0a0740

.field public static final image_ok:I = 0x7f0a0741

.field public static final image_pre_step:I = 0x7f0a0742

.field public static final image_progressbar:I = 0x7f0a0743

.field public static final image_restore_bar:I = 0x7f0a0744

.field public static final image_restore_btn:I = 0x7f0a0745

.field public static final image_rotate:I = 0x7f0a0746

.field public static final image_scan_action_bar:I = 0x7f0a0747

.field public static final image_scan_back_btn:I = 0x7f0a0748

.field public static final image_scan_back_btn2:I = 0x7f0a0749

.field public static final image_scan_bound_btn:I = 0x7f0a074a

.field public static final image_scan_bound_btn2:I = 0x7f0a074b

.field public static final image_scan_finish_btn:I = 0x7f0a074c

.field public static final image_scan_over_flow_view:I = 0x7f0a074d

.field public static final image_scan_process_btn:I = 0x7f0a074e

.field public static final image_scan_process_btn2:I = 0x7f0a074f

.field public static final image_scan_step:I = 0x7f0a0750

.field public static final image_scan_turn_left:I = 0x7f0a0751

.field public static final image_scan_turn_left2:I = 0x7f0a0752

.field public static final image_scan_turn_left_2:I = 0x7f0a0753

.field public static final image_scan_turn_right:I = 0x7f0a0754

.field public static final image_scan_turn_right2:I = 0x7f0a0755

.field public static final image_scan_view:I = 0x7f0a0756

.field public static final image_show_color:I = 0x7f0a0757

.field public static final image_show_viewpager:I = 0x7f0a0758

.field public static final image_take_next_page:I = 0x7f0a0759

.field public static final image_take_next_page2:I = 0x7f0a075a

.field public static final image_unsubscribe_recall_progressbar:I = 0x7f0a075b

.field public static final image_view:I = 0x7f0a075c

.field public static final image_view_ocr_btn:I = 0x7f0a075d

.field public static final image_view_pager:I = 0x7f0a075e

.field public static final img_docitem_pdf_processing:I = 0x7f0a075f

.field public static final img_home_iqiyi_pop_newgift:I = 0x7f0a0760

.field public static final img_imgpage_ocr:I = 0x7f0a0761

.field public static final img_share_adr_tip:I = 0x7f0a0762

.field public static final immediateStop:I = 0x7f0a0763

.field public static final in_edit_keyboard:I = 0x7f0a0764

.field public static final inc_trans_lang_select:I = 0x7f0a0765

.field public static final inch_des:I = 0x7f0a0766

.field public static final inch_status_box:I = 0x7f0a0767

.field public static final inch_type:I = 0x7f0a0768

.field public static final include_bottom_container:I = 0x7f0a0769

.field public static final include_btm_funs:I = 0x7f0a076a

.field public static final include_buy:I = 0x7f0a076b

.field public static final include_certificate_card:I = 0x7f0a076c

.field public static final include_certificate_photo_guide:I = 0x7f0a076d

.field public static final include_create_dir:I = 0x7f0a076e

.field public static final include_data_show:I = 0x7f0a076f

.field public static final include_edit:I = 0x7f0a0770

.field public static final include_edit_layout:I = 0x7f0a0771

.field public static final include_edit_sign:I = 0x7f0a0772

.field public static final include_end:I = 0x7f0a0773

.field public static final include_filter:I = 0x7f0a0774

.field public static final include_header_dir:I = 0x7f0a0775

.field public static final include_header_movecopy:I = 0x7f0a0776

.field public static final include_header_root:I = 0x7f0a0777

.field public static final include_lang_select:I = 0x7f0a0778

.field public static final include_login_tips:I = 0x7f0a0779

.field public static final include_login_tips_age:I = 0x7f0a077a

.field public static final include_login_tips_info_collect:I = 0x7f0a077b

.field public static final include_login_tips_privacy:I = 0x7f0a077c

.field public static final include_login_tips_user:I = 0x7f0a077d

.field public static final include_middle:I = 0x7f0a077e

.field public static final include_multi_thumb:I = 0x7f0a077f

.field public static final include_other:I = 0x7f0a0780

.field public static final include_prepare_sign:I = 0x7f0a0781

.field public static final include_search_empty:I = 0x7f0a0782

.field public static final include_shutter_center:I = 0x7f0a0783

.field public static final include_shutter_right:I = 0x7f0a0784

.field public static final include_start:I = 0x7f0a0785

.field public static final include_toolbox:I = 0x7f0a0786

.field public static final included:I = 0x7f0a0787

.field public static final indeterminate:I = 0x7f0a0788

.field public static final indexLayout:I = 0x7f0a0789

.field public static final index_entity_types:I = 0x7f0a078a

.field public static final indicator_container:I = 0x7f0a078b

.field public static final indicator_dots:I = 0x7f0a078c

.field public static final info:I = 0x7f0a078d

.field public static final ink_setting:I = 0x7f0a078e

.field public static final inline:I = 0x7f0a078f

.field public static final input_activate_code:I = 0x7f0a0790

.field public static final input_pdf_type:I = 0x7f0a0791

.field public static final instant_message:I = 0x7f0a0792

.field public static final intent_action:I = 0x7f0a0793

.field public static final intent_activity:I = 0x7f0a0794

.field public static final intent_data:I = 0x7f0a0795

.field public static final intent_data_id:I = 0x7f0a0796

.field public static final intent_extra_data:I = 0x7f0a0797

.field public static final internal_jump:I = 0x7f0a0798

.field public static final interstitial_control_button:I = 0x7f0a0799

.field public static final interstitial_control_view:I = 0x7f0a079a

.field public static final invisible:I = 0x7f0a079b

.field public static final inward:I = 0x7f0a079c

.field public static final ip_by_service:I = 0x7f0a079d

.field public static final ip_capture_child_process_one:I = 0x7f0a079e

.field public static final ip_capture_child_process_three:I = 0x7f0a079f

.field public static final ip_capture_child_process_two:I = 0x7f0a07a0

.field public static final ip_capture_main_process:I = 0x7f0a07a1

.field public static final ip_capture_process_via_service:I = 0x7f0a07a2

.field public static final ip_child_process_one:I = 0x7f0a07a3

.field public static final ip_child_process_three:I = 0x7f0a07a4

.field public static final ip_child_process_two:I = 0x7f0a07a5

.field public static final ip_main_process:I = 0x7f0a07a6

.field public static final ip_stack_default_order:I = 0x7f0a07a7

.field public static final ip_stack_only_ipv4:I = 0x7f0a07a8

.field public static final ip_stack_only_ipv6:I = 0x7f0a07a9

.field public static final ip_stack_prefer_ipv4:I = 0x7f0a07aa

.field public static final ip_stack_prefer_ipv6:I = 0x7f0a07ab

.field public static final issuerText:I = 0x7f0a07ac

.field public static final italic:I = 0x7f0a07ad

.field public static final itb_actionbar_change_mode:I = 0x7f0a07ae

.field public static final itb_actionbar_more:I = 0x7f0a07af

.field public static final itb_actionbar_pdf:I = 0x7f0a07b0

.field public static final itb_actionbar_share:I = 0x7f0a07b1

.field public static final itb_add:I = 0x7f0a07b2

.field public static final itb_add_text:I = 0x7f0a07b3

.field public static final itb_adjust:I = 0x7f0a07b4

.field public static final itb_adjust_border:I = 0x7f0a07b5

.field public static final itb_anti_counterfeit:I = 0x7f0a07b6

.field public static final itb_append_page:I = 0x7f0a07b7

.field public static final itb_batch_ocr:I = 0x7f0a07b8

.field public static final itb_bottom_delete:I = 0x7f0a07b9

.field public static final itb_bottom_fax:I = 0x7f0a07ba

.field public static final itb_bottom_more:I = 0x7f0a07bb

.field public static final itb_bottom_move:I = 0x7f0a07bc

.field public static final itb_bottom_pdf_kit_add:I = 0x7f0a07bd

.field public static final itb_bottom_pdf_kit_delete:I = 0x7f0a07be

.field public static final itb_bottom_pdf_kit_extract:I = 0x7f0a07bf

.field public static final itb_bottom_pdf_kit_share:I = 0x7f0a07c0

.field public static final itb_bottom_pdf_kit_submit:I = 0x7f0a07c1

.field public static final itb_bottom_save_gallery:I = 0x7f0a07c2

.field public static final itb_bottom_share:I = 0x7f0a07c3

.field public static final itb_bottom_upload:I = 0x7f0a07c4

.field public static final itb_business_cooperation:I = 0x7f0a07c5

.field public static final itb_colla_doc_comment:I = 0x7f0a07c6

.field public static final itb_colla_doc_invent:I = 0x7f0a07c7

.field public static final itb_correction:I = 0x7f0a07c8

.field public static final itb_create_folder:I = 0x7f0a07c9

.field public static final itb_crop:I = 0x7f0a07ca

.field public static final itb_delete:I = 0x7f0a07cb

.field public static final itb_doc_multi_select:I = 0x7f0a07cc

.field public static final itb_doc_shopping:I = 0x7f0a07cd

.field public static final itb_edit:I = 0x7f0a07ce

.field public static final itb_edit_ocr_text:I = 0x7f0a07cf

.field public static final itb_edit_pdf:I = 0x7f0a07d0

.field public static final itb_extract:I = 0x7f0a07d1

.field public static final itb_filter:I = 0x7f0a07d2

.field public static final itb_home:I = 0x7f0a07d3

.field public static final itb_hot_msg:I = 0x7f0a07d4

.field public static final itb_import:I = 0x7f0a07d5

.field public static final itb_large_img_change_mode:I = 0x7f0a07d6

.field public static final itb_large_img_more:I = 0x7f0a07d7

.field public static final itb_large_img_share:I = 0x7f0a07d8

.field public static final itb_large_img_view_pdf:I = 0x7f0a07d9

.field public static final itb_lr_table_submit:I = 0x7f0a07da

.field public static final itb_markup:I = 0x7f0a07db

.field public static final itb_me:I = 0x7f0a07dc

.field public static final itb_member_msg:I = 0x7f0a07dd

.field public static final itb_more:I = 0x7f0a07de

.field public static final itb_more_on_presign:I = 0x7f0a07df

.field public static final itb_new_doc_import:I = 0x7f0a07e0

.field public static final itb_new_folder:I = 0x7f0a07e1

.field public static final itb_normal_add_ink:I = 0x7f0a07e2

.field public static final itb_normal_bottom_more:I = 0x7f0a07e3

.field public static final itb_normal_bottom_share:I = 0x7f0a07e4

.field public static final itb_normal_bottom_take_photo:I = 0x7f0a07e5

.field public static final itb_normal_bottom_view_pdf:I = 0x7f0a07e6

.field public static final itb_ocr:I = 0x7f0a07e7

.field public static final itb_ocr_edit_result_save:I = 0x7f0a07e8

.field public static final itb_pdf_auto_graph:I = 0x7f0a07e9

.field public static final itb_pdf_editing_compress:I = 0x7f0a07ea

.field public static final itb_pdf_editing_encrypt:I = 0x7f0a07eb

.field public static final itb_pdf_editing_image_edit:I = 0x7f0a07ec

.field public static final itb_pdf_editing_setting:I = 0x7f0a07ed

.field public static final itb_pdf_editing_water_mark:I = 0x7f0a07ee

.field public static final itb_pdf_encrypt:I = 0x7f0a07ef

.field public static final itb_pdf_signature:I = 0x7f0a07f0

.field public static final itb_pdf_transfer_word:I = 0x7f0a07f1

.field public static final itb_ppt_play:I = 0x7f0a07f2

.field public static final itb_process_pic:I = 0x7f0a07f3

.field public static final itb_re_cloud_ocr:I = 0x7f0a07f4

.field public static final itb_retake:I = 0x7f0a07f5

.field public static final itb_save_local:I = 0x7f0a07f6

.field public static final itb_save_pdf:I = 0x7f0a07f7

.field public static final itb_search:I = 0x7f0a07f8

.field public static final itb_select:I = 0x7f0a07f9

.field public static final itb_select_copy:I = 0x7f0a07fa

.field public static final itb_setting:I = 0x7f0a07fb

.field public static final itb_share:I = 0x7f0a07fc

.field public static final itb_share_doc:I = 0x7f0a07fd

.field public static final itb_share_ocr_text:I = 0x7f0a07fe

.field public static final itb_sign_detail:I = 0x7f0a07ff

.field public static final itb_signature:I = 0x7f0a0800

.field public static final itb_signature_date_group:I = 0x7f0a0801

.field public static final itb_signature_normal_group:I = 0x7f0a0802

.field public static final itb_signature_spanning_seal_group:I = 0x7f0a0803

.field public static final itb_sort_order:I = 0x7f0a0804

.field public static final itb_submit:I = 0x7f0a0805

.field public static final itb_switch_mode:I = 0x7f0a0806

.field public static final itb_system_msg:I = 0x7f0a0807

.field public static final itb_take_next:I = 0x7f0a0808

.field public static final itb_take_photo:I = 0x7f0a0809

.field public static final itb_team_introduce:I = 0x7f0a080a

.field public static final itb_team_msg:I = 0x7f0a080b

.field public static final itb_to_word:I = 0x7f0a080c

.field public static final itb_top_more:I = 0x7f0a080d

.field public static final itb_topic_empty_page:I = 0x7f0a080e

.field public static final itb_topic_model:I = 0x7f0a080f

.field public static final itb_topic_paper_smudge:I = 0x7f0a0810

.field public static final itb_topic_template:I = 0x7f0a0811

.field public static final itb_topic_water_mark:I = 0x7f0a0812

.field public static final itb_translation:I = 0x7f0a0813

.field public static final itb_turn_left:I = 0x7f0a0814

.field public static final itb_turn_right:I = 0x7f0a0815

.field public static final itb_watermark:I = 0x7f0a0816

.field public static final itb_write_annotate:I = 0x7f0a0817

.field public static final itemCheckView:I = 0x7f0a0818

.field public static final item_class:I = 0x7f0a0819

.field public static final item_csl_item1:I = 0x7f0a081a

.field public static final item_csl_item2:I = 0x7f0a081b

.field public static final item_csl_item3:I = 0x7f0a081c

.field public static final item_gp_question_image:I = 0x7f0a081d

.field public static final item_gp_question_image_desc:I = 0x7f0a081e

.field public static final item_gp_question_image_desc2:I = 0x7f0a081f

.field public static final item_gp_question_image_top_dec:I = 0x7f0a0820

.field public static final item_invoice_check_type:I = 0x7f0a0821

.field public static final item_name:I = 0x7f0a0822

.field public static final item_normal:I = 0x7f0a0823

.field public static final item_root:I = 0x7f0a0824

.field public static final item_scandone_header_new:I = 0x7f0a0825

.field public static final item_touch_helper_previous_elevation:I = 0x7f0a0826

.field public static final item_value:I = 0x7f0a0827

.field public static final item_view_add_page:I = 0x7f0a0828

.field public static final item_view_copy_text:I = 0x7f0a0829

.field public static final item_view_re_identify:I = 0x7f0a082a

.field public static final item_work:I = 0x7f0a082b

.field public static final itv_recommend:I = 0x7f0a082c

.field public static final itv_take_one_more:I = 0x7f0a082d

.field public static final iv:I = 0x7f0a082e

.field public static final iv1:I = 0x7f0a082f

.field public static final iv2:I = 0x7f0a0830

.field public static final iv3:I = 0x7f0a0831

.field public static final iv4:I = 0x7f0a0832

.field public static final iv5:I = 0x7f0a0833

.field public static final iv6:I = 0x7f0a0834

.field public static final iv_account_clear:I = 0x7f0a0835

.field public static final iv_ad_choice:I = 0x7f0a0836

.field public static final iv_ad_icon:I = 0x7f0a0837

.field public static final iv_add:I = 0x7f0a0838

.field public static final iv_add_tag:I = 0x7f0a0839

.field public static final iv_add_times:I = 0x7f0a083a

.field public static final iv_agree:I = 0x7f0a083b

.field public static final iv_ai_circle:I = 0x7f0a083c

.field public static final iv_ai_logo:I = 0x7f0a083d

.field public static final iv_alarm:I = 0x7f0a083e

.field public static final iv_all_function:I = 0x7f0a083f

.field public static final iv_animate_bg:I = 0x7f0a0840

.field public static final iv_animate_title:I = 0x7f0a0841

.field public static final iv_animate_trophy:I = 0x7f0a0842

.field public static final iv_area_free_image:I = 0x7f0a0843

.field public static final iv_arrow:I = 0x7f0a0844

.field public static final iv_arrow_right:I = 0x7f0a0845

.field public static final iv_avatar:I = 0x7f0a0846

.field public static final iv_back:I = 0x7f0a0847

.field public static final iv_backup_close:I = 0x7f0a0848

.field public static final iv_backup_tip:I = 0x7f0a0849

.field public static final iv_backward:I = 0x7f0a084a

.field public static final iv_bad_case_screenshot:I = 0x7f0a084b

.field public static final iv_bank_journal_change_select:I = 0x7f0a084c

.field public static final iv_bank_thumb:I = 0x7f0a084d

.field public static final iv_banner:I = 0x7f0a084e

.field public static final iv_banner_bg:I = 0x7f0a084f

.field public static final iv_banner_doc:I = 0x7f0a0850

.field public static final iv_banner_text:I = 0x7f0a0851

.field public static final iv_beta:I = 0x7f0a0852

.field public static final iv_bg:I = 0x7f0a0853

.field public static final iv_bg_1:I = 0x7f0a0854

.field public static final iv_bg_2:I = 0x7f0a0855

.field public static final iv_bg_icon:I = 0x7f0a0856

.field public static final iv_bg_middle:I = 0x7f0a0857

.field public static final iv_bg_scan:I = 0x7f0a0858

.field public static final iv_bg_tip:I = 0x7f0a0859

.field public static final iv_bg_top:I = 0x7f0a085a

.field public static final iv_bluetooth:I = 0x7f0a085b

.field public static final iv_blur:I = 0x7f0a085c

.field public static final iv_bottom_content:I = 0x7f0a085d

.field public static final iv_bottom_image:I = 0x7f0a085e

.field public static final iv_bottom_left:I = 0x7f0a085f

.field public static final iv_bottom_right:I = 0x7f0a0860

.field public static final iv_button_add:I = 0x7f0a0861

.field public static final iv_buy_arrow:I = 0x7f0a0862

.field public static final iv_calculate_bg:I = 0x7f0a0863

.field public static final iv_cam_icon:I = 0x7f0a0864

.field public static final iv_cancel:I = 0x7f0a0865

.field public static final iv_cancel_account:I = 0x7f0a0866

.field public static final iv_cap_new_user_guide_cancel:I = 0x7f0a0867

.field public static final iv_cap_new_user_guide_image:I = 0x7f0a0868

.field public static final iv_capture_add:I = 0x7f0a0869

.field public static final iv_capture_idcard_cancel:I = 0x7f0a086a

.field public static final iv_capture_idcard_use_now:I = 0x7f0a086b

.field public static final iv_card_icon:I = 0x7f0a086c

.field public static final iv_card_photo:I = 0x7f0a086d

.field public static final iv_card_type:I = 0x7f0a086e

.field public static final iv_cc_docbanner_hide:I = 0x7f0a086f

.field public static final iv_certificate_detail_head_logo:I = 0x7f0a0870

.field public static final iv_change_existed_account_cancel:I = 0x7f0a0871

.field public static final iv_check:I = 0x7f0a0872

.field public static final iv_check_arrow:I = 0x7f0a0873

.field public static final iv_chrome:I = 0x7f0a0874

.field public static final iv_claim_tip:I = 0x7f0a0875

.field public static final iv_clear:I = 0x7f0a0876

.field public static final iv_clear_email:I = 0x7f0a0877

.field public static final iv_clear_search:I = 0x7f0a0878

.field public static final iv_click:I = 0x7f0a0879

.field public static final iv_click_ocr:I = 0x7f0a087a

.field public static final iv_clip:I = 0x7f0a087b

.field public static final iv_clock_icon:I = 0x7f0a087c

.field public static final iv_close:I = 0x7f0a087d

.field public static final iv_close_dialog:I = 0x7f0a087e

.field public static final iv_close_guide:I = 0x7f0a087f

.field public static final iv_close_icon:I = 0x7f0a0880

.field public static final iv_close_local_purchase:I = 0x7f0a0881

.field public static final iv_close_moire_hint:I = 0x7f0a0882

.field public static final iv_close_protocol:I = 0x7f0a0883

.field public static final iv_close_searchview:I = 0x7f0a0884

.field public static final iv_close_share:I = 0x7f0a0885

.field public static final iv_close_sign_panel:I = 0x7f0a0886

.field public static final iv_close_time_line_guide:I = 0x7f0a0887

.field public static final iv_close_tip:I = 0x7f0a0888

.field public static final iv_close_tips:I = 0x7f0a0889

.field public static final iv_close_watermark:I = 0x7f0a088a

.field public static final iv_cloud:I = 0x7f0a088b

.field public static final iv_cloud_close:I = 0x7f0a088c

.field public static final iv_cloud_disk_back:I = 0x7f0a088d

.field public static final iv_cloud_disk_type:I = 0x7f0a088e

.field public static final iv_cloud_reward:I = 0x7f0a088f

.field public static final iv_cloud_switch_head_image:I = 0x7f0a0890

.field public static final iv_cloud_yes:I = 0x7f0a0891

.field public static final iv_code:I = 0x7f0a0892

.field public static final iv_code_type_image:I = 0x7f0a0893

.field public static final iv_compare:I = 0x7f0a0894

.field public static final iv_confirm:I = 0x7f0a0895

.field public static final iv_congratulation:I = 0x7f0a0896

.field public static final iv_content:I = 0x7f0a0897

.field public static final iv_conver:I = 0x7f0a0898

.field public static final iv_convert:I = 0x7f0a0899

.field public static final iv_cover:I = 0x7f0a089a

.field public static final iv_cover_image:I = 0x7f0a089b

.field public static final iv_create_folder_movecopy:I = 0x7f0a089c

.field public static final iv_create_folder_sc:I = 0x7f0a089d

.field public static final iv_create_menu:I = 0x7f0a089e

.field public static final iv_crown:I = 0x7f0a089f

.field public static final iv_cs_ai_tip:I = 0x7f0a08a0

.field public static final iv_cs_icon:I = 0x7f0a08a1

.field public static final iv_cs_logo:I = 0x7f0a08a2

.field public static final iv_cs_qr_code:I = 0x7f0a08a3

.field public static final iv_de_moire:I = 0x7f0a08a4

.field public static final iv_delete:I = 0x7f0a08a5

.field public static final iv_delete_edittext:I = 0x7f0a08a6

.field public static final iv_delete_email:I = 0x7f0a08a7

.field public static final iv_delete_name:I = 0x7f0a08a8

.field public static final iv_delete_other_cards_info:I = 0x7f0a08a9

.field public static final iv_delete_phone:I = 0x7f0a08aa

.field public static final iv_delete_screen_shot:I = 0x7f0a08ab

.field public static final iv_delete_sign:I = 0x7f0a08ac

.field public static final iv_delete_tag_item:I = 0x7f0a08ad

.field public static final iv_density_arrow:I = 0x7f0a08ae

.field public static final iv_detail:I = 0x7f0a08af

.field public static final iv_dialog_close:I = 0x7f0a08b0

.field public static final iv_dialog_to_retain_top_logo:I = 0x7f0a08b1

.field public static final iv_dialog_to_retain_top_logo_bg:I = 0x7f0a08b2

.field public static final iv_dialog_to_retain_top_ticket_bg:I = 0x7f0a08b3

.field public static final iv_dialog_to_retain_top_ticket_dec_left:I = 0x7f0a08b4

.field public static final iv_dialog_to_retain_top_ticket_dec_right:I = 0x7f0a08b5

.field public static final iv_dir_path:I = 0x7f0a08b6

.field public static final iv_discount:I = 0x7f0a08b7

.field public static final iv_discount_line:I = 0x7f0a08b8

.field public static final iv_discount_receive_now:I = 0x7f0a08b9

.field public static final iv_discount_vip:I = 0x7f0a08ba

.field public static final iv_dismiss:I = 0x7f0a08bb

.field public static final iv_doc:I = 0x7f0a08bc

.field public static final iv_doc_logo:I = 0x7f0a08bd

.field public static final iv_doc_name:I = 0x7f0a08be

.field public static final iv_doc_num:I = 0x7f0a08bf

.field public static final iv_doc_type:I = 0x7f0a08c0

.field public static final iv_docicon:I = 0x7f0a08c1

.field public static final iv_document_more_king_kong:I = 0x7f0a08c2

.field public static final iv_document_more_king_kong_vip:I = 0x7f0a08c3

.field public static final iv_document_right_arrow:I = 0x7f0a08c4

.field public static final iv_document_thumb:I = 0x7f0a08c5

.field public static final iv_done:I = 0x7f0a08c6

.field public static final iv_dot:I = 0x7f0a08c7

.field public static final iv_down:I = 0x7f0a08c8

.field public static final iv_download:I = 0x7f0a08c9

.field public static final iv_download_cs_pdf_guide:I = 0x7f0a08ca

.field public static final iv_drag:I = 0x7f0a08cb

.field public static final iv_drop_cnl_close:I = 0x7f0a08cc

.field public static final iv_dropper:I = 0x7f0a08cd

.field public static final iv_edit:I = 0x7f0a08ce

.field public static final iv_edit_clear:I = 0x7f0a08cf

.field public static final iv_edu_cover:I = 0x7f0a08d0

.field public static final iv_edu_invite_banner:I = 0x7f0a08d1

.field public static final iv_ee_arrow:I = 0x7f0a08d2

.field public static final iv_email_clear:I = 0x7f0a08d3

.field public static final iv_email_login:I = 0x7f0a08d4

.field public static final iv_email_pwd_back:I = 0x7f0a08d5

.field public static final iv_empty_doc_guide_arrow:I = 0x7f0a08d6

.field public static final iv_empty_guide:I = 0x7f0a08d7

.field public static final iv_enterprise_mall:I = 0x7f0a08d8

.field public static final iv_eraser:I = 0x7f0a08d9

.field public static final iv_esc:I = 0x7f0a08da

.field public static final iv_excel:I = 0x7f0a08db

.field public static final iv_excel_no_recognize_faq:I = 0x7f0a08dc

.field public static final iv_exit:I = 0x7f0a08dd

.field public static final iv_expand:I = 0x7f0a08de

.field public static final iv_expand_scan_done_tag:I = 0x7f0a08df

.field public static final iv_explanation:I = 0x7f0a08e0

.field public static final iv_export_vip:I = 0x7f0a08e1

.field public static final iv_extract_text:I = 0x7f0a08e2

.field public static final iv_facebook:I = 0x7f0a08e3

.field public static final iv_feedback_arrow:I = 0x7f0a08e4

.field public static final iv_file_type:I = 0x7f0a08e5

.field public static final iv_filter_cover:I = 0x7f0a08e6

.field public static final iv_finish:I = 0x7f0a08e7

.field public static final iv_first:I = 0x7f0a08e8

.field public static final iv_five_star:I = 0x7f0a08e9

.field public static final iv_five_starts_fifth:I = 0x7f0a08ea

.field public static final iv_five_starts_first:I = 0x7f0a08eb

.field public static final iv_five_starts_fourth:I = 0x7f0a08ec

.field public static final iv_five_starts_second:I = 0x7f0a08ed

.field public static final iv_five_starts_third:I = 0x7f0a08ee

.field public static final iv_flag:I = 0x7f0a08ef

.field public static final iv_folder:I = 0x7f0a08f0

.field public static final iv_folder_back:I = 0x7f0a08f1

.field public static final iv_folder_back_movecopy:I = 0x7f0a08f2

.field public static final iv_folder_bottom_icon:I = 0x7f0a08f3

.field public static final iv_folder_empty:I = 0x7f0a08f4

.field public static final iv_folder_icon:I = 0x7f0a08f5

.field public static final iv_folder_image:I = 0x7f0a08f6

.field public static final iv_folder_more:I = 0x7f0a08f7

.field public static final iv_folder_new_icon:I = 0x7f0a08f8

.field public static final iv_folder_ope_more:I = 0x7f0a08f9

.field public static final iv_forget_pwd_back:I = 0x7f0a08fa

.field public static final iv_forward:I = 0x7f0a08fb

.field public static final iv_fourth:I = 0x7f0a08fc

.field public static final iv_frame:I = 0x7f0a08fd

.field public static final iv_free:I = 0x7f0a08fe

.field public static final iv_free_trail_vip_update:I = 0x7f0a08ff

.field public static final iv_free_trial_bg:I = 0x7f0a0900

.field public static final iv_free_trial_enabled:I = 0x7f0a0901

.field public static final iv_free_trial_receive_now:I = 0x7f0a0902

.field public static final iv_from_line:I = 0x7f0a0903

.field public static final iv_full_attendance_reward_status:I = 0x7f0a0904

.field public static final iv_full_lottie:I = 0x7f0a0905

.field public static final iv_full_screen_doc:I = 0x7f0a0906

.field public static final iv_func1:I = 0x7f0a0907

.field public static final iv_func2:I = 0x7f0a0908

.field public static final iv_func_arrow:I = 0x7f0a0909

.field public static final iv_func_icon:I = 0x7f0a090a

.field public static final iv_function:I = 0x7f0a090b

.field public static final iv_function_icon:I = 0x7f0a090c

.field public static final iv_function_icon1:I = 0x7f0a090d

.field public static final iv_function_icon2:I = 0x7f0a090e

.field public static final iv_function_icon3:I = 0x7f0a090f

.field public static final iv_function_next1:I = 0x7f0a0910

.field public static final iv_function_next2:I = 0x7f0a0911

.field public static final iv_gift:I = 0x7f0a0912

.field public static final iv_gift_back_image_end:I = 0x7f0a0913

.field public static final iv_gift_back_image_middle:I = 0x7f0a0914

.field public static final iv_gift_back_image_start:I = 0x7f0a0915

.field public static final iv_gift_icon:I = 0x7f0a0916

.field public static final iv_gift_triangle:I = 0x7f0a0917

.field public static final iv_go_back:I = 0x7f0a0918

.field public static final iv_go_purchase:I = 0x7f0a0919

.field public static final iv_google_login:I = 0x7f0a091a

.field public static final iv_gp_guide_mark:I = 0x7f0a091b

.field public static final iv_gradient:I = 0x7f0a091c

.field public static final iv_guide:I = 0x7f0a091d

.field public static final iv_guide_arrow:I = 0x7f0a091e

.field public static final iv_guide_banner2:I = 0x7f0a091f

.field public static final iv_guide_banner3:I = 0x7f0a0920

.field public static final iv_guide_banner4:I = 0x7f0a0921

.field public static final iv_guide_banner5:I = 0x7f0a0922

.field public static final iv_guide_banner6:I = 0x7f0a0923

.field public static final iv_guide_close:I = 0x7f0a0924

.field public static final iv_guide_intro:I = 0x7f0a0925

.field public static final iv_handle_image:I = 0x7f0a0926

.field public static final iv_hands:I = 0x7f0a0927

.field public static final iv_hd_tag:I = 0x7f0a0928

.field public static final iv_head:I = 0x7f0a0929

.field public static final iv_header:I = 0x7f0a092a

.field public static final iv_header_more:I = 0x7f0a092b

.field public static final iv_header_search:I = 0x7f0a092c

.field public static final iv_help:I = 0x7f0a092d

.field public static final iv_hight_light_pen:I = 0x7f0a092e

.field public static final iv_hot:I = 0x7f0a092f

.field public static final iv_i_know:I = 0x7f0a0930

.field public static final iv_icon:I = 0x7f0a0931

.field public static final iv_icon_1:I = 0x7f0a0932

.field public static final iv_icon_cam_exam:I = 0x7f0a0933

.field public static final iv_icon_line_chart:I = 0x7f0a0934

.field public static final iv_icon_mask:I = 0x7f0a0935

.field public static final iv_icon_novice:I = 0x7f0a0936

.field public static final iv_icon_ope:I = 0x7f0a0937

.field public static final iv_icon_right:I = 0x7f0a0938

.field public static final iv_icon_word:I = 0x7f0a0939

.field public static final iv_id_share_activity:I = 0x7f0a093a

.field public static final iv_image:I = 0x7f0a093b

.field public static final iv_img:I = 0x7f0a093c

.field public static final iv_immersive_quit:I = 0x7f0a093d

.field public static final iv_import_file_guide:I = 0x7f0a093e

.field public static final iv_importing_state:I = 0x7f0a093f

.field public static final iv_indicator:I = 0x7f0a0940

.field public static final iv_info:I = 0x7f0a0941

.field public static final iv_info_visible:I = 0x7f0a0942

.field public static final iv_integral_reward:I = 0x7f0a0943

.field public static final iv_intro:I = 0x7f0a0944

.field public static final iv_introduce:I = 0x7f0a0945

.field public static final iv_introduce_arrow:I = 0x7f0a0946

.field public static final iv_invoice:I = 0x7f0a0947

.field public static final iv_invoice_close:I = 0x7f0a0948

.field public static final iv_invoice_retry:I = 0x7f0a0949

.field public static final iv_invoice_thumb:I = 0x7f0a094a

.field public static final iv_invoice_txt_left:I = 0x7f0a094b

.field public static final iv_item_desc:I = 0x7f0a094c

.field public static final iv_item_guide_cn_image:I = 0x7f0a094d

.field public static final iv_item_icon:I = 0x7f0a094e

.field public static final iv_item_icon1:I = 0x7f0a094f

.field public static final iv_item_icon2:I = 0x7f0a0950

.field public static final iv_item_icon3:I = 0x7f0a0951

.field public static final iv_item_icon_bg:I = 0x7f0a0952

.field public static final iv_item_icon_bg_no:I = 0x7f0a0953

.field public static final iv_item_icon_bg_shadow:I = 0x7f0a0954

.field public static final iv_item_icon_no:I = 0x7f0a0955

.field public static final iv_item_me_page_header_header:I = 0x7f0a0956

.field public static final iv_item_me_page_header_header_scan_level:I = 0x7f0a0957

.field public static final iv_item_negative_premium_image:I = 0x7f0a0958

.field public static final iv_item_purchase_new_style2:I = 0x7f0a0959

.field public static final iv_item_template:I = 0x7f0a095a

.field public static final iv_last_login_close:I = 0x7f0a095b

.field public static final iv_last_login_user_icon:I = 0x7f0a095c

.field public static final iv_last_share_tip_arrow_bottom:I = 0x7f0a095d

.field public static final iv_last_share_tip_arrow_left:I = 0x7f0a095e

.field public static final iv_last_share_tip_top:I = 0x7f0a095f

.field public static final iv_layout_arrow:I = 0x7f0a0960

.field public static final iv_left:I = 0x7f0a0961

.field public static final iv_left_half_line:I = 0x7f0a0962

.field public static final iv_left_light:I = 0x7f0a0963

.field public static final iv_light:I = 0x7f0a0964

.field public static final iv_line1_left:I = 0x7f0a0965

.field public static final iv_line1_right_bottom:I = 0x7f0a0966

.field public static final iv_line1_right_top:I = 0x7f0a0967

.field public static final iv_line2_left:I = 0x7f0a0968

.field public static final iv_line2_right:I = 0x7f0a0969

.field public static final iv_link_enc_days:I = 0x7f0a096a

.field public static final iv_little_crown:I = 0x7f0a096b

.field public static final iv_loading:I = 0x7f0a096c

.field public static final iv_lock_state:I = 0x7f0a096d

.field public static final iv_lock_state_bg:I = 0x7f0a096e

.field public static final iv_login_clear:I = 0x7f0a096f

.field public static final iv_login_close:I = 0x7f0a0970

.field public static final iv_login_ways_back:I = 0x7f0a0971

.field public static final iv_logo:I = 0x7f0a0972

.field public static final iv_lower:I = 0x7f0a0973

.field public static final iv_mac:I = 0x7f0a0974

.field public static final iv_main:I = 0x7f0a0975

.field public static final iv_main_content:I = 0x7f0a0976

.field public static final iv_main_image:I = 0x7f0a0977

.field public static final iv_main_pic:I = 0x7f0a0978

.field public static final iv_main_view:I = 0x7f0a0979

.field public static final iv_main_view_bg:I = 0x7f0a097a

.field public static final iv_main_view_bottom:I = 0x7f0a097b

.field public static final iv_main_view_close:I = 0x7f0a097c

.field public static final iv_main_view_icon:I = 0x7f0a097d

.field public static final iv_main_view_left:I = 0x7f0a097e

.field public static final iv_map:I = 0x7f0a097f

.field public static final iv_mark_pen:I = 0x7f0a0980

.field public static final iv_mask:I = 0x7f0a0981

.field public static final iv_me_page_area_free_card:I = 0x7f0a0982

.field public static final iv_me_page_auth_google:I = 0x7f0a0983

.field public static final iv_me_page_auth_mail:I = 0x7f0a0984

.field public static final iv_me_page_auth_mobile:I = 0x7f0a0985

.field public static final iv_me_page_auth_wechat:I = 0x7f0a0986

.field public static final iv_me_page_bar_capture:I = 0x7f0a0987

.field public static final iv_me_page_bar_messges:I = 0x7f0a0988

.field public static final iv_me_page_king_kong_first:I = 0x7f0a0989

.field public static final iv_me_page_king_kong_fourth:I = 0x7f0a098a

.field public static final iv_me_page_king_kong_second:I = 0x7f0a098b

.field public static final iv_me_page_king_kong_take_photo:I = 0x7f0a098c

.field public static final iv_me_page_king_kong_third:I = 0x7f0a098d

.field public static final iv_menu_icon:I = 0x7f0a098e

.field public static final iv_messges:I = 0x7f0a098f

.field public static final iv_middle:I = 0x7f0a0990

.field public static final iv_mobile_backup:I = 0x7f0a0991

.field public static final iv_mobile_import:I = 0x7f0a0992

.field public static final iv_mobile_number_clear:I = 0x7f0a0993

.field public static final iv_mobile_number_close:I = 0x7f0a0994

.field public static final iv_mobile_pwd_login_close:I = 0x7f0a0995

.field public static final iv_mobile_pwd_login_input_number_clear:I = 0x7f0a0996

.field public static final iv_mode_switch:I = 0x7f0a0997

.field public static final iv_mode_tips:I = 0x7f0a0998

.field public static final iv_modify_name:I = 0x7f0a0999

.field public static final iv_more:I = 0x7f0a099a

.field public static final iv_more_close:I = 0x7f0a099b

.field public static final iv_multi_select_sc:I = 0x7f0a099c

.field public static final iv_naire_1:I = 0x7f0a099d

.field public static final iv_naire_2:I = 0x7f0a099e

.field public static final iv_naire_3:I = 0x7f0a099f

.field public static final iv_naire_4:I = 0x7f0a09a0

.field public static final iv_naire_5:I = 0x7f0a09a1

.field public static final iv_new:I = 0x7f0a09a2

.field public static final iv_new_flag:I = 0x7f0a09a3

.field public static final iv_new_icon:I = 0x7f0a09a4

.field public static final iv_new_tips:I = 0x7f0a09a5

.field public static final iv_next:I = 0x7f0a09a6

.field public static final iv_next_page:I = 0x7f0a09a7

.field public static final iv_no_ad:I = 0x7f0a09a8

.field public static final iv_no_ad_yes:I = 0x7f0a09a9

.field public static final iv_num:I = 0x7f0a09aa

.field public static final iv_num_minus:I = 0x7f0a09ab

.field public static final iv_num_plus:I = 0x7f0a09ac

.field public static final iv_o_vip_right:I = 0x7f0a09ad

.field public static final iv_ocr:I = 0x7f0a09ae

.field public static final iv_ocr_edit_move_slider:I = 0x7f0a09af

.field public static final iv_ocr_edit_top:I = 0x7f0a09b0

.field public static final iv_ocr_result_next_page:I = 0x7f0a09b1

.field public static final iv_ocr_result_previous_page:I = 0x7f0a09b2

.field public static final iv_ocr_upgrade_vip:I = 0x7f0a09b3

.field public static final iv_office2cloud_export:I = 0x7f0a09b4

.field public static final iv_office_icon:I = 0x7f0a09b5

.field public static final iv_one_image:I = 0x7f0a09b6

.field public static final iv_one_image01:I = 0x7f0a09b7

.field public static final iv_one_image02:I = 0x7f0a09b8

.field public static final iv_one_image03:I = 0x7f0a09b9

.field public static final iv_one_login_auth_back:I = 0x7f0a09ba

.field public static final iv_one_login_auth_logo:I = 0x7f0a09bb

.field public static final iv_one_login_auth_mail:I = 0x7f0a09bc

.field public static final iv_one_login_auth_mobile:I = 0x7f0a09bd

.field public static final iv_one_login_auth_wechat:I = 0x7f0a09be

.field public static final iv_one_login_half_close:I = 0x7f0a09bf

.field public static final iv_one_login_half_close_for_login:I = 0x7f0a09c0

.field public static final iv_one_login_return_id:I = 0x7f0a09c1

.field public static final iv_one_yes:I = 0x7f0a09c2

.field public static final iv_only_read_doc_middle:I = 0x7f0a09c3

.field public static final iv_only_read_doc_top:I = 0x7f0a09c4

.field public static final iv_only_read_first_recent:I = 0x7f0a09c5

.field public static final iv_only_read_second_middle:I = 0x7f0a09c6

.field public static final iv_only_read_second_top:I = 0x7f0a09c7

.field public static final iv_only_read_third_middle:I = 0x7f0a09c8

.field public static final iv_only_read_third_top:I = 0x7f0a09c9

.field public static final iv_ope_more:I = 0x7f0a09ca

.field public static final iv_operation_banner:I = 0x7f0a09cb

.field public static final iv_operation_close:I = 0x7f0a09cc

.field public static final iv_operation_icon:I = 0x7f0a09cd

.field public static final iv_operation_icon_discount:I = 0x7f0a09ce

.field public static final iv_options:I = 0x7f0a09cf

.field public static final iv_ori_image:I = 0x7f0a09d0

.field public static final iv_page_1_wheat_end:I = 0x7f0a09d1

.field public static final iv_page_2_free_trial_month_switch:I = 0x7f0a09d2

.field public static final iv_paper:I = 0x7f0a09d3

.field public static final iv_paper_arrow:I = 0x7f0a09d4

.field public static final iv_paper_bg:I = 0x7f0a09d5

.field public static final iv_pay_way:I = 0x7f0a09d6

.field public static final iv_pdf:I = 0x7f0a09d7

.field public static final iv_pdf_arrow:I = 0x7f0a09d8

.field public static final iv_pdf_editing_cs_qr_code:I = 0x7f0a09d9

.field public static final iv_pdf_editing_display:I = 0x7f0a09da

.field public static final iv_pdf_editing_display_layout:I = 0x7f0a09db

.field public static final iv_pdf_editing_guide_arrow:I = 0x7f0a09dc

.field public static final iv_pdf_entry:I = 0x7f0a09dd

.field public static final iv_pdf_icon:I = 0x7f0a09de

.field public static final iv_pdf_page:I = 0x7f0a09df

.field public static final iv_pdf_vip:I = 0x7f0a09e0

.field public static final iv_personal_dir:I = 0x7f0a09e1

.field public static final iv_phone:I = 0x7f0a09e2

.field public static final iv_phone_email:I = 0x7f0a09e3

.field public static final iv_photo_image:I = 0x7f0a09e4

.field public static final iv_pic:I = 0x7f0a09e5

.field public static final iv_pic1:I = 0x7f0a09e6

.field public static final iv_ppt:I = 0x7f0a09e7

.field public static final iv_pre:I = 0x7f0a09e8

.field public static final iv_premium:I = 0x7f0a09e9

.field public static final iv_premium_icon:I = 0x7f0a09ea

.field public static final iv_preview_image:I = 0x7f0a09eb

.field public static final iv_preview_image_scan_light:I = 0x7f0a09ec

.field public static final iv_preview_logo:I = 0x7f0a09ed

.field public static final iv_previous:I = 0x7f0a09ee

.field public static final iv_previous_page:I = 0x7f0a09ef

.field public static final iv_print_doc:I = 0x7f0a09f0

.field public static final iv_pwd_login_over_five_cancel:I = 0x7f0a09f1

.field public static final iv_pwd_login_over_three_cancel:I = 0x7f0a09f2

.field public static final iv_qq:I = 0x7f0a09f3

.field public static final iv_qr_code:I = 0x7f0a09f4

.field public static final iv_qr_code_thumb:I = 0x7f0a09f5

.field public static final iv_qr_scan:I = 0x7f0a09f6

.field public static final iv_range_arrow:I = 0x7f0a09f7

.field public static final iv_re_enhance:I = 0x7f0a09f8

.field public static final iv_rec_close:I = 0x7f0a09f9

.field public static final iv_rec_drop_box:I = 0x7f0a09fa

.field public static final iv_rec_google_drive:I = 0x7f0a09fb

.field public static final iv_rec_one_drive:I = 0x7f0a09fc

.field public static final iv_red_dot:I = 0x7f0a09fd

.field public static final iv_red_envelope:I = 0x7f0a09fe

.field public static final iv_red_envelope_bg:I = 0x7f0a09ff

.field public static final iv_redo:I = 0x7f0a0a00

.field public static final iv_reduce_times:I = 0x7f0a0a01

.field public static final iv_register_back:I = 0x7f0a0a02

.field public static final iv_register_clear:I = 0x7f0a0a03

.field public static final iv_remove_watermark:I = 0x7f0a0a04

.field public static final iv_remove_watermark_close:I = 0x7f0a0a05

.field public static final iv_reset:I = 0x7f0a0a06

.field public static final iv_reset_pwd_back:I = 0x7f0a0a07

.field public static final iv_restore:I = 0x7f0a0a08

.field public static final iv_result:I = 0x7f0a0a09

.field public static final iv_result_image:I = 0x7f0a0a0a

.field public static final iv_retake_picture:I = 0x7f0a0a0b

.field public static final iv_reward_status:I = 0x7f0a0a0c

.field public static final iv_reward_topic:I = 0x7f0a0a0d

.field public static final iv_reward_transfer_word:I = 0x7f0a0a0e

.field public static final iv_right:I = 0x7f0a0a0f

.field public static final iv_right_arrow:I = 0x7f0a0a10

.field public static final iv_right_half_line:I = 0x7f0a0a11

.field public static final iv_right_light:I = 0x7f0a0a12

.field public static final iv_right_line:I = 0x7f0a0a13

.field public static final iv_rotate_tip:I = 0x7f0a0a14

.field public static final iv_sample:I = 0x7f0a0a15

.field public static final iv_satisfy:I = 0x7f0a0a16

.field public static final iv_save:I = 0x7f0a0a17

.field public static final iv_save_bottom:I = 0x7f0a0a18

.field public static final iv_save_success:I = 0x7f0a0a19

.field public static final iv_scan_level:I = 0x7f0a0a1a

.field public static final iv_scan_process_guide:I = 0x7f0a0a1b

.field public static final iv_scan_process_guide2:I = 0x7f0a0a1c

.field public static final iv_scan_qr_code:I = 0x7f0a0a1d

.field public static final iv_screenshot:I = 0x7f0a0a1e

.field public static final iv_search:I = 0x7f0a0a1f

.field public static final iv_search_close:I = 0x7f0a0a20

.field public static final iv_search_cover:I = 0x7f0a0a21

.field public static final iv_search_empty:I = 0x7f0a0a22

.field public static final iv_search_high_light:I = 0x7f0a0a23

.field public static final iv_search_icon:I = 0x7f0a0a24

.field public static final iv_second:I = 0x7f0a0a25

.field public static final iv_seekbar_icon:I = 0x7f0a0a26

.field public static final iv_select:I = 0x7f0a0a27

.field public static final iv_select_arrow:I = 0x7f0a0a28

.field public static final iv_select_pay_way:I = 0x7f0a0a29

.field public static final iv_select_sign_type_arrow:I = 0x7f0a0a2a

.field public static final iv_select_toolbar_close:I = 0x7f0a0a2b

.field public static final iv_selected:I = 0x7f0a0a2c

.field public static final iv_set:I = 0x7f0a0a2d

.field public static final iv_setting_line_arrow_right:I = 0x7f0a0a2e

.field public static final iv_seven_day_premium:I = 0x7f0a0a2f

.field public static final iv_shadow:I = 0x7f0a0a30

.field public static final iv_share:I = 0x7f0a0a31

.field public static final iv_share_1:I = 0x7f0a0a32

.field public static final iv_share_2:I = 0x7f0a0a33

.field public static final iv_share_avatar:I = 0x7f0a0a34

.field public static final iv_share_channel:I = 0x7f0a0a35

.field public static final iv_share_dir:I = 0x7f0a0a36

.field public static final iv_share_icon:I = 0x7f0a0a37

.field public static final iv_share_image:I = 0x7f0a0a38

.field public static final iv_share_link_list_vip:I = 0x7f0a0a39

.field public static final iv_share_link_vip:I = 0x7f0a0a3a

.field public static final iv_share_to_wehcat_icon:I = 0x7f0a0a3b

.field public static final iv_show_thumb:I = 0x7f0a0a3c

.field public static final iv_sig_actionbar_right:I = 0x7f0a0a3d

.field public static final iv_signature:I = 0x7f0a0a3e

.field public static final iv_size_arrow:I = 0x7f0a0a3f

.field public static final iv_sort_order_sc:I = 0x7f0a0a40

.field public static final iv_squared_icon:I = 0x7f0a0a41

.field public static final iv_src:I = 0x7f0a0a42

.field public static final iv_star1:I = 0x7f0a0a43

.field public static final iv_start_directly:I = 0x7f0a0a44

.field public static final iv_status:I = 0x7f0a0a45

.field public static final iv_status_icon:I = 0x7f0a0a46

.field public static final iv_sub_button:I = 0x7f0a0a47

.field public static final iv_subtitle_strong_line:I = 0x7f0a0a48

.field public static final iv_success:I = 0x7f0a0a49

.field public static final iv_super_vcode_over_five_cancel:I = 0x7f0a0a4a

.field public static final iv_super_vcode_over_there_cancel:I = 0x7f0a0a4b

.field public static final iv_switch_mode_sc:I = 0x7f0a0a4c

.field public static final iv_sync_icon:I = 0x7f0a0a4d

.field public static final iv_sync_state:I = 0x7f0a0a4e

.field public static final iv_sync_state_finished:I = 0x7f0a0a4f

.field public static final iv_tab_center:I = 0x7f0a0a50

.field public static final iv_tab_indicator:I = 0x7f0a0a51

.field public static final iv_tag:I = 0x7f0a0a52

.field public static final iv_tag_edit:I = 0x7f0a0a53

.field public static final iv_tag_selected:I = 0x7f0a0a54

.field public static final iv_task_vip:I = 0x7f0a0a55

.field public static final iv_team_empty_show:I = 0x7f0a0a56

.field public static final iv_test:I = 0x7f0a0a57

.field public static final iv_test1:I = 0x7f0a0a58

.field public static final iv_third:I = 0x7f0a0a59

.field public static final iv_third_net_disk_head_subicon:I = 0x7f0a0a5a

.field public static final iv_third_net_disk_image:I = 0x7f0a0a5b

.field public static final iv_third_netdisk_head_image:I = 0x7f0a0a5c

.field public static final iv_three_image:I = 0x7f0a0a5d

.field public static final iv_three_yes:I = 0x7f0a0a5e

.field public static final iv_thumb:I = 0x7f0a0a5f

.field public static final iv_thumb_item:I = 0x7f0a0a60

.field public static final iv_thumb_loading:I = 0x7f0a0a61

.field public static final iv_tip:I = 0x7f0a0a62

.field public static final iv_tips_barcode_close:I = 0x7f0a0a63

.field public static final iv_tips_barcode_type:I = 0x7f0a0a64

.field public static final iv_tips_des:I = 0x7f0a0a65

.field public static final iv_tips_icon:I = 0x7f0a0a66

.field public static final iv_title:I = 0x7f0a0a67

.field public static final iv_title_flower:I = 0x7f0a0a68

.field public static final iv_to_buy:I = 0x7f0a0a69

.field public static final iv_to_buy_light:I = 0x7f0a0a6a

.field public static final iv_to_line:I = 0x7f0a0a6b

.field public static final iv_to_word:I = 0x7f0a0a6c

.field public static final iv_to_word_yes:I = 0x7f0a0a6d

.field public static final iv_toast_image:I = 0x7f0a0a6e

.field public static final iv_tool_page_circle_cell_image:I = 0x7f0a0a6f

.field public static final iv_tool_page_square_cell_image:I = 0x7f0a0a70

.field public static final iv_top:I = 0x7f0a0a71

.field public static final iv_top_banner:I = 0x7f0a0a72

.field public static final iv_top_change_mode:I = 0x7f0a0a73

.field public static final iv_top_change_select:I = 0x7f0a0a74

.field public static final iv_top_icon:I = 0x7f0a0a75

.field public static final iv_top_image:I = 0x7f0a0a76

.field public static final iv_top_img:I = 0x7f0a0a77

.field public static final iv_top_label:I = 0x7f0a0a78

.field public static final iv_top_left:I = 0x7f0a0a79

.field public static final iv_top_middle:I = 0x7f0a0a7a

.field public static final iv_top_right:I = 0x7f0a0a7b

.field public static final iv_top_search:I = 0x7f0a0a7c

.field public static final iv_top_view:I = 0x7f0a0a7d

.field public static final iv_topic:I = 0x7f0a0a7e

.field public static final iv_torch:I = 0x7f0a0a7f

.field public static final iv_tp2_left_one_right_two_left:I = 0x7f0a0a80

.field public static final iv_tp2_left_one_right_two_right_bottom:I = 0x7f0a0a81

.field public static final iv_tp2_left_one_right_two_right_top:I = 0x7f0a0a82

.field public static final iv_tp2_left_two_right_one_left:I = 0x7f0a0a83

.field public static final iv_tp2_left_two_right_one_right_bottom:I = 0x7f0a0a84

.field public static final iv_tp2_left_two_right_one_right_top:I = 0x7f0a0a85

.field public static final iv_tp2_recent_first:I = 0x7f0a0a86

.field public static final iv_tp2_recent_fourth:I = 0x7f0a0a87

.field public static final iv_tp2_recent_second:I = 0x7f0a0a88

.field public static final iv_tp2_recent_third:I = 0x7f0a0a89

.field public static final iv_tp2_top_three_bottom_one_bottom:I = 0x7f0a0a8a

.field public static final iv_tp2_top_three_bottom_one_left:I = 0x7f0a0a8b

.field public static final iv_tp2_top_three_bottom_one_middle:I = 0x7f0a0a8c

.field public static final iv_tp2_top_three_bottom_one_right:I = 0x7f0a0a8d

.field public static final iv_tp_2_square_bottom_left:I = 0x7f0a0a8e

.field public static final iv_tp_2_square_bottom_right:I = 0x7f0a0a8f

.field public static final iv_tp_2_square_top_left:I = 0x7f0a0a90

.field public static final iv_tp_2_square_top_right:I = 0x7f0a0a91

.field public static final iv_translate_trans:I = 0x7f0a0a92

.field public static final iv_trial_renew_top_banner:I = 0x7f0a0a93

.field public static final iv_trial_renew_top_title:I = 0x7f0a0a94

.field public static final iv_triangle_from:I = 0x7f0a0a95

.field public static final iv_triangle_to:I = 0x7f0a0a96

.field public static final iv_trophy:I = 0x7f0a0a97

.field public static final iv_turn_select:I = 0x7f0a0a98

.field public static final iv_two_image:I = 0x7f0a0a99

.field public static final iv_two_yes:I = 0x7f0a0a9a

.field public static final iv_type:I = 0x7f0a0a9b

.field public static final iv_type_label:I = 0x7f0a0a9c

.field public static final iv_un_connect:I = 0x7f0a0a9d

.field public static final iv_underline:I = 0x7f0a0a9e

.field public static final iv_undo:I = 0x7f0a0a9f

.field public static final iv_unsubscribe_recall_close_icon:I = 0x7f0a0aa0

.field public static final iv_up:I = 0x7f0a0aa1

.field public static final iv_upgrade:I = 0x7f0a0aa2

.field public static final iv_upload_error_thumb:I = 0x7f0a0aa3

.field public static final iv_upper:I = 0x7f0a0aa4

.field public static final iv_user_guide_demo_image:I = 0x7f0a0aa5

.field public static final iv_user_icon:I = 0x7f0a0aa6

.field public static final iv_verify_code_input_back:I = 0x7f0a0aa7

.field public static final iv_version:I = 0x7f0a0aa8

.field public static final iv_view:I = 0x7f0a0aa9

.field public static final iv_view_signature:I = 0x7f0a0aaa

.field public static final iv_vip:I = 0x7f0a0aab

.field public static final iv_vip_back_label_end:I = 0x7f0a0aac

.field public static final iv_vip_back_label_start:I = 0x7f0a0aad

.field public static final iv_vip_discount:I = 0x7f0a0aae

.field public static final iv_vip_free:I = 0x7f0a0aaf

.field public static final iv_vip_icon:I = 0x7f0a0ab0

.field public static final iv_vip_level:I = 0x7f0a0ab1

.field public static final iv_vip_light:I = 0x7f0a0ab2

.field public static final iv_vip_next:I = 0x7f0a0ab3

.field public static final iv_vip_received:I = 0x7f0a0ab4

.field public static final iv_vip_right:I = 0x7f0a0ab5

.field public static final iv_vip_right_1:I = 0x7f0a0ab6

.field public static final iv_vip_right_2:I = 0x7f0a0ab7

.field public static final iv_vip_right_3:I = 0x7f0a0ab8

.field public static final iv_vip_rights_label_end:I = 0x7f0a0ab9

.field public static final iv_vip_rights_label_start:I = 0x7f0a0aba

.field public static final iv_vip_texture1:I = 0x7f0a0abb

.field public static final iv_vip_texture2:I = 0x7f0a0abc

.field public static final iv_vip_thumb:I = 0x7f0a0abd

.field public static final iv_waring:I = 0x7f0a0abe

.field public static final iv_warning:I = 0x7f0a0abf

.field public static final iv_watch_ad:I = 0x7f0a0ac0

.field public static final iv_watermark:I = 0x7f0a0ac1

.field public static final iv_watermark_remove:I = 0x7f0a0ac2

.field public static final iv_web_bg:I = 0x7f0a0ac3

.field public static final iv_web_refresh:I = 0x7f0a0ac4

.field public static final iv_wechat:I = 0x7f0a0ac5

.field public static final iv_wechat_login:I = 0x7f0a0ac6

.field public static final iv_weixi:I = 0x7f0a0ac7

.field public static final iv_whatsapp:I = 0x7f0a0ac8

.field public static final iv_word:I = 0x7f0a0ac9

.field public static final iv_word_ad_free_tag:I = 0x7f0a0aca

.field public static final iv_word_list_item:I = 0x7f0a0acb

.field public static final iv_word_vip:I = 0x7f0a0acc

.field public static final iv_work_flow_file_naming:I = 0x7f0a0acd

.field public static final iv_work_flow_file_naming_arrow_right:I = 0x7f0a0ace

.field public static final iv_workflow_email_delete:I = 0x7f0a0acf

.field public static final iv_workflow_email_one_validate_loading:I = 0x7f0a0ad0

.field public static final iv_wx:I = 0x7f0a0ad1

.field public static final iv_xin_chun_xiao_shi:I = 0x7f0a0ad2

.field public static final iv_year_count:I = 0x7f0a0ad3

.field public static final ivb_actionbar_share:I = 0x7f0a0ad4

.field public static final ivb_more_menu_btn:I = 0x7f0a0ad5

.field public static final jumpToEnd:I = 0x7f0a0ad6

.field public static final jumpToStart:I = 0x7f0a0ad7

.field public static final kbl_halfpack_root:I = 0x7f0a0ad8

.field public static final kbl_verify_root:I = 0x7f0a0ad9

.field public static final keyboard_layout:I = 0x7f0a0ada

.field public static final kl_root_layout:I = 0x7f0a0adb

.field public static final l_agree:I = 0x7f0a0adc

.field public static final l_bottom:I = 0x7f0a0add

.field public static final l_brightness_seek:I = 0x7f0a0ade

.field public static final l_code:I = 0x7f0a0adf

.field public static final l_color:I = 0x7f0a0ae0

.field public static final l_contrast_seek:I = 0x7f0a0ae1

.field public static final l_detail_seek:I = 0x7f0a0ae2

.field public static final l_doc_search:I = 0x7f0a0ae3

.field public static final l_doodle:I = 0x7f0a0ae4

.field public static final l_doodle_nav:I = 0x7f0a0ae5

.field public static final l_dropper_nav:I = 0x7f0a0ae6

.field public static final l_edit_bar:I = 0x7f0a0ae7

.field public static final l_export_word:I = 0x7f0a0ae8

.field public static final l_from:I = 0x7f0a0ae9

.field public static final l_hint:I = 0x7f0a0aea

.field public static final l_last_share_tip:I = 0x7f0a0aeb

.field public static final l_last_share_tip_color:I = 0x7f0a0aec

.field public static final l_markup:I = 0x7f0a0aed

.field public static final l_origin:I = 0x7f0a0aee

.field public static final l_pen:I = 0x7f0a0aef

.field public static final l_root:I = 0x7f0a0af0

.field public static final l_search_empty:I = 0x7f0a0af1

.field public static final l_search_history:I = 0x7f0a0af2

.field public static final l_signature:I = 0x7f0a0af3

.field public static final l_size:I = 0x7f0a0af4

.field public static final l_to:I = 0x7f0a0af5

.field public static final l_tools:I = 0x7f0a0af6

.field public static final l_translate:I = 0x7f0a0af7

.field public static final l_undo_redo:I = 0x7f0a0af8

.field public static final label_title:I = 0x7f0a0af9

.field public static final labeled:I = 0x7f0a0afa

.field public static final language_container:I = 0x7f0a0afb

.field public static final large:I = 0x7f0a0afc

.field public static final large_icon_uri:I = 0x7f0a0afd

.field public static final lav_guide:I = 0x7f0a0afe

.field public static final lav_image:I = 0x7f0a0aff

.field public static final lav_loading:I = 0x7f0a0b00

.field public static final lav_new_user_scan_gift:I = 0x7f0a0b01

.field public static final lav_top:I = 0x7f0a0b02

.field public static final layout:I = 0x7f0a0b03

.field public static final layout1:I = 0x7f0a0b04

.field public static final layout2:I = 0x7f0a0b05

.field public static final layout3:I = 0x7f0a0b06

.field public static final layout4:I = 0x7f0a0b07

.field public static final layout_activation_code:I = 0x7f0a0b08

.field public static final layout_advice:I = 0x7f0a0b09

.field public static final layout_amin_progress_bar:I = 0x7f0a0b0a

.field public static final layout_baidu:I = 0x7f0a0b0b

.field public static final layout_book:I = 0x7f0a0b0c

.field public static final layout_bottom:I = 0x7f0a0b0d

.field public static final layout_bottom_pack:I = 0x7f0a0b0e

.field public static final layout_change_account_change_account:I = 0x7f0a0b0f

.field public static final layout_change_account_modify_nickname:I = 0x7f0a0b10

.field public static final layout_connect:I = 0x7f0a0b11

.field public static final layout_cover:I = 0x7f0a0b12

.field public static final layout_cs_right:I = 0x7f0a0b13

.field public static final layout_des:I = 0x7f0a0b14

.field public static final layout_dialog_to_retain_gp_style_coupon:I = 0x7f0a0b15

.field public static final layout_dialog_to_retain_gp_style_line_chart:I = 0x7f0a0b16

.field public static final layout_discount_purchase_v2_continue:I = 0x7f0a0b17

.field public static final layout_discount_purchase_v2_top_logo:I = 0x7f0a0b18

.field public static final layout_email:I = 0x7f0a0b19

.field public static final layout_facebook:I = 0x7f0a0b1a

.field public static final layout_googlePlus:I = 0x7f0a0b1b

.field public static final layout_header_old:I = 0x7f0a0b1c

.field public static final layout_image_adjust:I = 0x7f0a0b1d

.field public static final layout_item:I = 0x7f0a0b1e

.field public static final layout_last_view:I = 0x7f0a0b1f

.field public static final layout_main:I = 0x7f0a0b20

.field public static final layout_middle:I = 0x7f0a0b21

.field public static final layout_ocr_lang:I = 0x7f0a0b22

.field public static final layout_one_key_import:I = 0x7f0a0b23

.field public static final layout_orientation_guide:I = 0x7f0a0b24

.field public static final layout_pdf_edit_bar:I = 0x7f0a0b25

.field public static final layout_pdf_right:I = 0x7f0a0b26

.field public static final layout_preview_image:I = 0x7f0a0b27

.field public static final layout_purchase:I = 0x7f0a0b28

.field public static final layout_rate:I = 0x7f0a0b29

.field public static final layout_root:I = 0x7f0a0b2a

.field public static final layout_sign_in:I = 0x7f0a0b2b

.field public static final layout_sms:I = 0x7f0a0b2c

.field public static final layout_switch_ocr_title_cloud:I = 0x7f0a0b2d

.field public static final layout_switch_ocr_title_local:I = 0x7f0a0b2e

.field public static final layout_tools:I = 0x7f0a0b2f

.field public static final layout_top:I = 0x7f0a0b30

.field public static final layout_tv_sub2:I = 0x7f0a0b31

.field public static final layout_tv_sub2_month:I = 0x7f0a0b32

.field public static final layout_twitter:I = 0x7f0a0b33

.field public static final layout_update:I = 0x7f0a0b34

.field public static final layout_verify_phone_country:I = 0x7f0a0b35

.field public static final layout_verify_phone_num:I = 0x7f0a0b36

.field public static final layout_vip:I = 0x7f0a0b37

.field public static final layout_vip_month_tips:I = 0x7f0a0b38

.field public static final layout_vk:I = 0x7f0a0b39

.field public static final layout_warn:I = 0x7f0a0b3a

.field public static final layout_weibo:I = 0x7f0a0b3b

.field public static final layout_weixin_chat:I = 0x7f0a0b3c

.field public static final layout_weixin_friends:I = 0x7f0a0b3d

.field public static final learn_more_button:I = 0x7f0a0b3e

.field public static final left:I = 0x7f0a0b3f

.field public static final leftToRight:I = 0x7f0a0b40

.field public static final left_bottom:I = 0x7f0a0b41

.field public static final left_section:I = 0x7f0a0b42

.field public static final left_top:I = 0x7f0a0b43

.field public static final legacy:I = 0x7f0a0b44

.field public static final light:I = 0x7f0a0b45

.field public static final line:I = 0x7f0a0b46

.field public static final line1:I = 0x7f0a0b47

.field public static final line2:I = 0x7f0a0b48

.field public static final line2Parent:I = 0x7f0a0b49

.field public static final line3:I = 0x7f0a0b4a

.field public static final line_bottom:I = 0x7f0a0b4b

.field public static final line_end_1:I = 0x7f0a0b4c

.field public static final line_end_2:I = 0x7f0a0b4d

.field public static final line_end_3:I = 0x7f0a0b4e

.field public static final line_middle:I = 0x7f0a0b4f

.field public static final line_start_1:I = 0x7f0a0b50

.field public static final line_start_2:I = 0x7f0a0b51

.field public static final line_start_3:I = 0x7f0a0b52

.field public static final line_top:I = 0x7f0a0b53

.field public static final linear:I = 0x7f0a0b54

.field public static final linearLayout:I = 0x7f0a0b55

.field public static final linearLayout_gdoc:I = 0x7f0a0b56

.field public static final linearLayout_scanner:I = 0x7f0a0b57

.field public static final list:I = 0x7f0a0b58

.field public static final listMode:I = 0x7f0a0b59

.field public static final listView:I = 0x7f0a0b5a

.field public static final list_data:I = 0x7f0a0b5b

.field public static final list_item:I = 0x7f0a0b5c

.field public static final list_kingkong_card:I = 0x7f0a0b5d

.field public static final list_panel:I = 0x7f0a0b5e

.field public static final list_pull_refresh_view:I = 0x7f0a0b5f

.field public static final list_view_member:I = 0x7f0a0b60

.field public static final listad:I = 0x7f0a0b61

.field public static final ll:I = 0x7f0a0b62

.field public static final llButtons:I = 0x7f0a0b63

.field public static final ll_action:I = 0x7f0a0b64

.field public static final ll_ad_close:I = 0x7f0a0b65

.field public static final ll_add:I = 0x7f0a0b66

.field public static final ll_add_cetificate:I = 0x7f0a0b67

.field public static final ll_add_image:I = 0x7f0a0b68

.field public static final ll_add_signer:I = 0x7f0a0b69

.field public static final ll_add_tag:I = 0x7f0a0b6a

.field public static final ll_age:I = 0x7f0a0b6b

.field public static final ll_agree_use_mobile_network:I = 0x7f0a0b6c

.field public static final ll_agreement_explain_message:I = 0x7f0a0b6d

.field public static final ll_ali_pay:I = 0x7f0a0b6e

.field public static final ll_ali_wx:I = 0x7f0a0b6f

.field public static final ll_all_function_guide_root:I = 0x7f0a0b70

.field public static final ll_all_tags_layout:I = 0x7f0a0b71

.field public static final ll_all_tags_scrollview:I = 0x7f0a0b72

.field public static final ll_amount:I = 0x7f0a0b73

.field public static final ll_app_banner_dots:I = 0x7f0a0b74

.field public static final ll_append_container:I = 0x7f0a0b75

.field public static final ll_attract_for_reward_check:I = 0x7f0a0b76

.field public static final ll_authority:I = 0x7f0a0b77

.field public static final ll_bad_case_img:I = 0x7f0a0b78

.field public static final ll_bank_card_middle:I = 0x7f0a0b79

.field public static final ll_banner_dots:I = 0x7f0a0b7a

.field public static final ll_bar:I = 0x7f0a0b7b

.field public static final ll_bg:I = 0x7f0a0b7c

.field public static final ll_bind_select_county_code:I = 0x7f0a0b7d

.field public static final ll_blank:I = 0x7f0a0b7e

.field public static final ll_border:I = 0x7f0a0b7f

.field public static final ll_bottom:I = 0x7f0a0b80

.field public static final ll_bottom_bar:I = 0x7f0a0b81

.field public static final ll_bottom_button_container:I = 0x7f0a0b82

.field public static final ll_bottom_container:I = 0x7f0a0b83

.field public static final ll_bottom_edit_funcs:I = 0x7f0a0b84

.field public static final ll_bottom_funcs:I = 0x7f0a0b85

.field public static final ll_bottom_logo:I = 0x7f0a0b86

.field public static final ll_bottom_menu:I = 0x7f0a0b87

.field public static final ll_bottom_purchase:I = 0x7f0a0b88

.field public static final ll_bottom_purchase_style_2:I = 0x7f0a0b89

.field public static final ll_bottom_sheet:I = 0x7f0a0b8a

.field public static final ll_bottom_status:I = 0x7f0a0b8b

.field public static final ll_bottombar_container:I = 0x7f0a0b8c

.field public static final ll_bottoms_tips:I = 0x7f0a0b8d

.field public static final ll_btn:I = 0x7f0a0b8e

.field public static final ll_bubble_hint_container:I = 0x7f0a0b8f

.field public static final ll_business_card:I = 0x7f0a0b90

.field public static final ll_buy_month:I = 0x7f0a0b91

.field public static final ll_buy_year:I = 0x7f0a0b92

.field public static final ll_camcard_doc_banner:I = 0x7f0a0b93

.field public static final ll_cancel_account:I = 0x7f0a0b94

.field public static final ll_cancel_warning:I = 0x7f0a0b95

.field public static final ll_cannot_edit_ocr:I = 0x7f0a0b96

.field public static final ll_capture_button:I = 0x7f0a0b97

.field public static final ll_capture_entry:I = 0x7f0a0b98

.field public static final ll_capture_guide_page_layout:I = 0x7f0a0b99

.field public static final ll_capture_merge_guide_root:I = 0x7f0a0b9a

.field public static final ll_capture_multi_large:I = 0x7f0a0b9b

.field public static final ll_capture_multi_normal:I = 0x7f0a0b9c

.field public static final ll_capture_qrcode_large:I = 0x7f0a0b9d

.field public static final ll_capture_qrcode_normal:I = 0x7f0a0b9e

.field public static final ll_capture_single_large:I = 0x7f0a0b9f

.field public static final ll_capture_single_normal:I = 0x7f0a0ba0

.field public static final ll_category_container:I = 0x7f0a0ba1

.field public static final ll_center_tips:I = 0x7f0a0ba2

.field public static final ll_certificate:I = 0x7f0a0ba3

.field public static final ll_certificate_container:I = 0x7f0a0ba4

.field public static final ll_certificate_detail_head_cover:I = 0x7f0a0ba5

.field public static final ll_certificate_guide_root:I = 0x7f0a0ba6

.field public static final ll_change_account_account_entire_container:I = 0x7f0a0ba7

.field public static final ll_change_batch_mode:I = 0x7f0a0ba8

.field public static final ll_change_batch_ocr_mode:I = 0x7f0a0ba9

.field public static final ll_change_image:I = 0x7f0a0baa

.field public static final ll_change_mode:I = 0x7f0a0bab

.field public static final ll_change_topic_mode:I = 0x7f0a0bac

.field public static final ll_checkbox:I = 0x7f0a0bad

.field public static final ll_child_capture_mode:I = 0x7f0a0bae

.field public static final ll_click_thumb_guide_root:I = 0x7f0a0baf

.field public static final ll_clip_container:I = 0x7f0a0bb0

.field public static final ll_close:I = 0x7f0a0bb1

.field public static final ll_cloud:I = 0x7f0a0bb2

.field public static final ll_cloud_disk_more:I = 0x7f0a0bb3

.field public static final ll_cloud_over_limit_for_invitor:I = 0x7f0a0bb4

.field public static final ll_color_select:I = 0x7f0a0bb5

.field public static final ll_common_other_login_way:I = 0x7f0a0bb6

.field public static final ll_complete_tip:I = 0x7f0a0bb7

.field public static final ll_composite:I = 0x7f0a0bb8

.field public static final ll_contact_us:I = 0x7f0a0bb9

.field public static final ll_container:I = 0x7f0a0bba

.field public static final ll_content:I = 0x7f0a0bbb

.field public static final ll_content_op:I = 0x7f0a0bbc

.field public static final ll_content_title:I = 0x7f0a0bbd

.field public static final ll_continue:I = 0x7f0a0bbe

.field public static final ll_contracts:I = 0x7f0a0bbf

.field public static final ll_copied_tip:I = 0x7f0a0bc0

.field public static final ll_count_container:I = 0x7f0a0bc1

.field public static final ll_create_folder:I = 0x7f0a0bc2

.field public static final ll_create_folder_sc:I = 0x7f0a0bc3

.field public static final ll_cur_model_root:I = 0x7f0a0bc4

.field public static final ll_data_show_export_ac:I = 0x7f0a0bc5

.field public static final ll_data_show_export_view:I = 0x7f0a0bc6

.field public static final ll_data_show_gen:I = 0x7f0a0bc7

.field public static final ll_data_show_touch_bar:I = 0x7f0a0bc8

.field public static final ll_delete:I = 0x7f0a0bc9

.field public static final ll_des:I = 0x7f0a0bca

.field public static final ll_desc_price:I = 0x7f0a0bcb

.field public static final ll_detail_des_container:I = 0x7f0a0bcc

.field public static final ll_detect_moire:I = 0x7f0a0bcd

.field public static final ll_detected_tips:I = 0x7f0a0bce

.field public static final ll_dialog_result_bottom:I = 0x7f0a0bcf

.field public static final ll_dialog_style_outer_layer:I = 0x7f0a0bd0

.field public static final ll_disconnect:I = 0x7f0a0bd1

.field public static final ll_discount_purchase_month:I = 0x7f0a0bd2

.field public static final ll_discount_purchase_v2_origin_price:I = 0x7f0a0bd3

.field public static final ll_discount_purchase_year:I = 0x7f0a0bd4

.field public static final ll_dissatisfied:I = 0x7f0a0bd5

.field public static final ll_doc_checkbox:I = 0x7f0a0bd6

.field public static final ll_doc_container:I = 0x7f0a0bd7

.field public static final ll_doc_container_sub:I = 0x7f0a0bd8

.field public static final ll_doc_state:I = 0x7f0a0bd9

.field public static final ll_doc_title:I = 0x7f0a0bda

.field public static final ll_docfrag_bottombar:I = 0x7f0a0bdb

.field public static final ll_docitem_tag_no_root:I = 0x7f0a0bdc

.field public static final ll_docitem_tag_root:I = 0x7f0a0bdd

.field public static final ll_document_capture_guide:I = 0x7f0a0bde

.field public static final ll_document_tag:I = 0x7f0a0bdf

.field public static final ll_document_title:I = 0x7f0a0be0

.field public static final ll_done:I = 0x7f0a0be1

.field public static final ll_dot_tips:I = 0x7f0a0be2

.field public static final ll_dots:I = 0x7f0a0be3

.field public static final ll_driver:I = 0x7f0a0be4

.field public static final ll_driver_cn_zh:I = 0x7f0a0be5

.field public static final ll_dynamic_feature:I = 0x7f0a0be6

.field public static final ll_e_evidence:I = 0x7f0a0be7

.field public static final ll_edit_bar_style1:I = 0x7f0a0be8

.field public static final ll_edit_bar_style2:I = 0x7f0a0be9

.field public static final ll_edittag_layout:I = 0x7f0a0bea

.field public static final ll_edu_tips:I = 0x7f0a0beb

.field public static final ll_ee_list:I = 0x7f0a0bec

.field public static final ll_email:I = 0x7f0a0bed

.field public static final ll_email_protocol:I = 0x7f0a0bee

.field public static final ll_empty:I = 0x7f0a0bef

.field public static final ll_empty_msg:I = 0x7f0a0bf0

.field public static final ll_empty_view:I = 0x7f0a0bf1

.field public static final ll_empty_view_tips:I = 0x7f0a0bf2

.field public static final ll_enhance_menu:I = 0x7f0a0bf3

.field public static final ll_enhance_panel:I = 0x7f0a0bf4

.field public static final ll_evidence_introduce:I = 0x7f0a0bf5

.field public static final ll_excel_change_batch_mode:I = 0x7f0a0bf6

.field public static final ll_excel_rec_root:I = 0x7f0a0bf7

.field public static final ll_export:I = 0x7f0a0bf8

.field public static final ll_export_btn:I = 0x7f0a0bf9

.field public static final ll_fail:I = 0x7f0a0bfa

.field public static final ll_fail_tip:I = 0x7f0a0bfb

.field public static final ll_feed_back:I = 0x7f0a0bfc

.field public static final ll_feedback_contact:I = 0x7f0a0bfd

.field public static final ll_finish_text_tips:I = 0x7f0a0bfe

.field public static final ll_float_action:I = 0x7f0a0bff

.field public static final ll_focus_container:I = 0x7f0a0c00

.field public static final ll_focus_para:I = 0x7f0a0c01

.field public static final ll_focus_table:I = 0x7f0a0c02

.field public static final ll_folder_checkbox:I = 0x7f0a0c03

.field public static final ll_func:I = 0x7f0a0c04

.field public static final ll_func1:I = 0x7f0a0c05

.field public static final ll_func2:I = 0x7f0a0c06

.field public static final ll_func_root:I = 0x7f0a0c07

.field public static final ll_function:I = 0x7f0a0c08

.field public static final ll_function_line_first:I = 0x7f0a0c09

.field public static final ll_function_line_fourth:I = 0x7f0a0c0a

.field public static final ll_function_line_second:I = 0x7f0a0c0b

.field public static final ll_function_line_third:I = 0x7f0a0c0c

.field public static final ll_gallery:I = 0x7f0a0c0d

.field public static final ll_gen:I = 0x7f0a0c0e

.field public static final ll_generate_photo:I = 0x7f0a0c0f

.field public static final ll_go2_local_file:I = 0x7f0a0c10

.field public static final ll_gp_guide_mark_middle:I = 0x7f0a0c11

.field public static final ll_gpc_cancel_container:I = 0x7f0a0c12

.field public static final ll_greet_introduce:I = 0x7f0a0c13

.field public static final ll_group_1:I = 0x7f0a0c14

.field public static final ll_guide_bottoms_dots:I = 0x7f0a0c15

.field public static final ll_guide_cn_purchase_indicator_container:I = 0x7f0a0c16

.field public static final ll_guide_container:I = 0x7f0a0c17

.field public static final ll_guide_gp_bottoms_dots:I = 0x7f0a0c18

.field public static final ll_guide_gp_item_normal_container:I = 0x7f0a0c19

.field public static final ll_guide_gp_purchase_page_shadow_bottom_container:I = 0x7f0a0c1a

.field public static final ll_halfpack_topbar:I = 0x7f0a0c1b

.field public static final ll_header_option:I = 0x7f0a0c1c

.field public static final ll_hint_container:I = 0x7f0a0c1d

.field public static final ll_home_banner_dots:I = 0x7f0a0c1e

.field public static final ll_house_property:I = 0x7f0a0c1f

.field public static final ll_ic_download:I = 0x7f0a0c20

.field public static final ll_id:I = 0x7f0a0c21

.field public static final ll_idcard_detected_prompt:I = 0x7f0a0c22

.field public static final ll_image_container:I = 0x7f0a0c23

.field public static final ll_image_quality:I = 0x7f0a0c24

.field public static final ll_image_share:I = 0x7f0a0c25

.field public static final ll_import:I = 0x7f0a0c26

.field public static final ll_import_by_camera:I = 0x7f0a0c27

.field public static final ll_import_by_file:I = 0x7f0a0c28

.field public static final ll_import_by_picture:I = 0x7f0a0c29

.field public static final ll_import_doc:I = 0x7f0a0c2a

.field public static final ll_import_from_gallery:I = 0x7f0a0c2b

.field public static final ll_import_from_wechat:I = 0x7f0a0c2c

.field public static final ll_import_root:I = 0x7f0a0c2d

.field public static final ll_import_wx_pic:I = 0x7f0a0c2e

.field public static final ll_index_change:I = 0x7f0a0c2f

.field public static final ll_indicator:I = 0x7f0a0c30

.field public static final ll_info_collect_agreement:I = 0x7f0a0c31

.field public static final ll_input:I = 0x7f0a0c32

.field public static final ll_input_root:I = 0x7f0a0c33

.field public static final ll_interval:I = 0x7f0a0c34

.field public static final ll_intro:I = 0x7f0a0c35

.field public static final ll_intro1:I = 0x7f0a0c36

.field public static final ll_intro2:I = 0x7f0a0c37

.field public static final ll_intro3:I = 0x7f0a0c38

.field public static final ll_intro4:I = 0x7f0a0c39

.field public static final ll_introduce:I = 0x7f0a0c3a

.field public static final ll_invoice_check_bottom:I = 0x7f0a0c3b

.field public static final ll_invoice_detected:I = 0x7f0a0c3c

.field public static final ll_item_container:I = 0x7f0a0c3d

.field public static final ll_item_normal_container:I = 0x7f0a0c3e

.field public static final ll_item_purchase_old_style:I = 0x7f0a0c3f

.field public static final ll_item_share_link_title_encrypt:I = 0x7f0a0c40

.field public static final ll_jpg:I = 0x7f0a0c41

.field public static final ll_ko_agree:I = 0x7f0a0c42

.field public static final ll_line:I = 0x7f0a0c43

.field public static final ll_loading:I = 0x7f0a0c44

.field public static final ll_login_contracts:I = 0x7f0a0c45

.field public static final ll_login_main_google_login_title:I = 0x7f0a0c46

.field public static final ll_login_main_other_login_title:I = 0x7f0a0c47

.field public static final ll_login_select_county_code:I = 0x7f0a0c48

.field public static final ll_login_ways:I = 0x7f0a0c49

.field public static final ll_login_ways_other_login_title:I = 0x7f0a0c4a

.field public static final ll_login_ways_protocol_with_cb:I = 0x7f0a0c4b

.field public static final ll_main:I = 0x7f0a0c4c

.field public static final ll_main_no_match_doc:I = 0x7f0a0c4d

.field public static final ll_me_page_card_left_account:I = 0x7f0a0c4e

.field public static final ll_me_page_card_s_right:I = 0x7f0a0c4f

.field public static final ll_me_page_card_validate_time:I = 0x7f0a0c50

.field public static final ll_me_page_king_kong_first:I = 0x7f0a0c51

.field public static final ll_me_page_king_kong_fourth:I = 0x7f0a0c52

.field public static final ll_me_page_king_kong_second:I = 0x7f0a0c53

.field public static final ll_me_page_king_kong_take_photo:I = 0x7f0a0c54

.field public static final ll_me_page_king_kong_third:I = 0x7f0a0c55

.field public static final ll_me_page_more_login_ways:I = 0x7f0a0c56

.field public static final ll_me_page_settings_student_rights:I = 0x7f0a0c57

.field public static final ll_members:I = 0x7f0a0c58

.field public static final ll_mobile_number_protocol:I = 0x7f0a0c59

.field public static final ll_mobile_pwd_login_protocol:I = 0x7f0a0c5a

.field public static final ll_modify_water_mark:I = 0x7f0a0c5b

.field public static final ll_moire_hint:I = 0x7f0a0c5c

.field public static final ll_moire_icon:I = 0x7f0a0c5d

.field public static final ll_movable_action:I = 0x7f0a0c5e

.field public static final ll_name:I = 0x7f0a0c5f

.field public static final ll_no_ads:I = 0x7f0a0c60

.field public static final ll_no_enough_tip:I = 0x7f0a0c61

.field public static final ll_no_watermark_tip:I = 0x7f0a0c62

.field public static final ll_normal:I = 0x7f0a0c63

.field public static final ll_normal_bar:I = 0x7f0a0c64

.field public static final ll_not_downloaded:I = 0x7f0a0c65

.field public static final ll_oK:I = 0x7f0a0c66

.field public static final ll_ocr:I = 0x7f0a0c67

.field public static final ll_ocr_region_toast:I = 0x7f0a0c68

.field public static final ll_ocr_select_language:I = 0x7f0a0c69

.field public static final ll_ocr_set_ocr_lang:I = 0x7f0a0c6a

.field public static final ll_ocr_times_tips:I = 0x7f0a0c6b

.field public static final ll_ocr_title:I = 0x7f0a0c6c

.field public static final ll_ocr_title_switch_container:I = 0x7f0a0c6d

.field public static final ll_office2cloud_export:I = 0x7f0a0c6e

.field public static final ll_one:I = 0x7f0a0c6f

.field public static final ll_one_login_auth_more_login_way:I = 0x7f0a0c70

.field public static final ll_one_login_auth_more_login_ways:I = 0x7f0a0c71

.field public static final ll_operation:I = 0x7f0a0c72

.field public static final ll_options_container:I = 0x7f0a0c73

.field public static final ll_other_cards:I = 0x7f0a0c74

.field public static final ll_other_login_ways:I = 0x7f0a0c75

.field public static final ll_page:I = 0x7f0a0c76

.field public static final ll_page_2_continue:I = 0x7f0a0c77

.field public static final ll_page_2_free_trial:I = 0x7f0a0c78

.field public static final ll_page_container:I = 0x7f0a0c79

.field public static final ll_page_index:I = 0x7f0a0c7a

.field public static final ll_page_list_bottom_info:I = 0x7f0a0c7b

.field public static final ll_page_rename:I = 0x7f0a0c7c

.field public static final ll_pageimage_bg_note:I = 0x7f0a0c7d

.field public static final ll_pages_guide:I = 0x7f0a0c7e

.field public static final ll_pair1:I = 0x7f0a0c7f

.field public static final ll_pair10:I = 0x7f0a0c80

.field public static final ll_pair2:I = 0x7f0a0c81

.field public static final ll_pair3:I = 0x7f0a0c82

.field public static final ll_pair4:I = 0x7f0a0c83

.field public static final ll_pair5:I = 0x7f0a0c84

.field public static final ll_pair6:I = 0x7f0a0c85

.field public static final ll_pair7:I = 0x7f0a0c86

.field public static final ll_pair8:I = 0x7f0a0c87

.field public static final ll_pair9:I = 0x7f0a0c88

.field public static final ll_pair_enhance_line_use_bm:I = 0x7f0a0c89

.field public static final ll_paper_page_show_raw_trimmed_paper:I = 0x7f0a0c8a

.field public static final ll_param_container:I = 0x7f0a0c8b

.field public static final ll_parent:I = 0x7f0a0c8c

.field public static final ll_passport:I = 0x7f0a0c8d

.field public static final ll_pay_type_ali:I = 0x7f0a0c8e

.field public static final ll_pay_type_wx:I = 0x7f0a0c8f

.field public static final ll_pdf_auto_graph:I = 0x7f0a0c90

.field public static final ll_pdf_editing_bottom_pack:I = 0x7f0a0c91

.field public static final ll_pdf_editing_compress:I = 0x7f0a0c92

.field public static final ll_pdf_editing_image_edit:I = 0x7f0a0c93

.field public static final ll_pdf_editing_setting:I = 0x7f0a0c94

.field public static final ll_pdf_editing_water_mark:I = 0x7f0a0c95

.field public static final ll_pdf_enhance_subscript:I = 0x7f0a0c96

.field public static final ll_pdf_transfer_word:I = 0x7f0a0c97

.field public static final ll_pdf_watermark_pdf_no_watermark_selected:I = 0x7f0a0c98

.field public static final ll_period:I = 0x7f0a0c99

.field public static final ll_phone:I = 0x7f0a0c9a

.field public static final ll_phone_pwd_login_area_code_container:I = 0x7f0a0c9b

.field public static final ll_picture:I = 0x7f0a0c9c

.field public static final ll_policy:I = 0x7f0a0c9d

.field public static final ll_policy_setting:I = 0x7f0a0c9e

.field public static final ll_pop:I = 0x7f0a0c9f

.field public static final ll_pre_back:I = 0x7f0a0ca0

.field public static final ll_pre_back2:I = 0x7f0a0ca1

.field public static final ll_price:I = 0x7f0a0ca2

.field public static final ll_print_paper:I = 0x7f0a0ca3

.field public static final ll_printer:I = 0x7f0a0ca4

.field public static final ll_privacy_agreement:I = 0x7f0a0ca5

.field public static final ll_protocol:I = 0x7f0a0ca6

.field public static final ll_protocol_compliant:I = 0x7f0a0ca7

.field public static final ll_protocol_compliant_small:I = 0x7f0a0ca8

.field public static final ll_protocol_korea:I = 0x7f0a0ca9

.field public static final ll_pull_tips:I = 0x7f0a0caa

.field public static final ll_purchase:I = 0x7f0a0cab

.field public static final ll_purchase_info:I = 0x7f0a0cac

.field public static final ll_purchase_item_layout:I = 0x7f0a0cad

.field public static final ll_purchase_page_show_new_indicator_container:I = 0x7f0a0cae

.field public static final ll_qr_code_bottom:I = 0x7f0a0caf

.field public static final ll_qr_code_history_right_check:I = 0x7f0a0cb0

.field public static final ll_question:I = 0x7f0a0cb1

.field public static final ll_quick_fun:I = 0x7f0a0cb2

.field public static final ll_reason_container:I = 0x7f0a0cb3

.field public static final ll_recommend:I = 0x7f0a0cb4

.field public static final ll_recommend_content:I = 0x7f0a0cb5

.field public static final ll_recommend_top:I = 0x7f0a0cb6

.field public static final ll_reload:I = 0x7f0a0cb7

.field public static final ll_remove_image_water_mark:I = 0x7f0a0cb8

.field public static final ll_remove_watermark:I = 0x7f0a0cb9

.field public static final ll_residence_booklet:I = 0x7f0a0cba

.field public static final ll_resort_merged_docs_title:I = 0x7f0a0cbb

.field public static final ll_restore_change_topic_mode:I = 0x7f0a0cbc

.field public static final ll_result:I = 0x7f0a0cbd

.field public static final ll_retry:I = 0x7f0a0cbe

.field public static final ll_right_check:I = 0x7f0a0cbf

.field public static final ll_root:I = 0x7f0a0cc0

.field public static final ll_root_capture_guide:I = 0x7f0a0cc1

.field public static final ll_root_chose_ocr_state:I = 0x7f0a0cc2

.field public static final ll_root_setting_layout:I = 0x7f0a0cc3

.field public static final ll_root_view:I = 0x7f0a0cc4

.field public static final ll_rotate_tip:I = 0x7f0a0cc5

.field public static final ll_satisfy:I = 0x7f0a0cc6

.field public static final ll_scan_tips_container:I = 0x7f0a0cc7

.field public static final ll_search:I = 0x7f0a0cc8

.field public static final ll_search_blank:I = 0x7f0a0cc9

.field public static final ll_search_empty:I = 0x7f0a0cca

.field public static final ll_search_no_data:I = 0x7f0a0ccb

.field public static final ll_search_referral:I = 0x7f0a0ccc

.field public static final ll_security:I = 0x7f0a0ccd

.field public static final ll_select:I = 0x7f0a0cce

.field public static final ll_select_combine_paper_property:I = 0x7f0a0ccf

.field public static final ll_select_file_info:I = 0x7f0a0cd0

.field public static final ll_select_paper_type:I = 0x7f0a0cd1

.field public static final ll_select_sign_type:I = 0x7f0a0cd2

.field public static final ll_selected_item_corner:I = 0x7f0a0cd3

.field public static final ll_selection:I = 0x7f0a0cd4

.field public static final ll_send_to_pc:I = 0x7f0a0cd5

.field public static final ll_set_default_pdf_app_layout:I = 0x7f0a0cd6

.field public static final ll_set_pwd:I = 0x7f0a0cd7

.field public static final ll_setting_right_txt_line_title:I = 0x7f0a0cd8

.field public static final ll_sfd_pay_type_spec:I = 0x7f0a0cd9

.field public static final ll_share:I = 0x7f0a0cda

.field public static final ll_share_dialog_remove_watermark:I = 0x7f0a0cdb

.field public static final ll_share_image:I = 0x7f0a0cdc

.field public static final ll_share_item_container:I = 0x7f0a0cdd

.field public static final ll_share_link_title_encrypt:I = 0x7f0a0cde

.field public static final ll_share_pdf:I = 0x7f0a0cdf

.field public static final ll_share_third_item:I = 0x7f0a0ce0

.field public static final ll_share_to_more:I = 0x7f0a0ce1

.field public static final ll_share_to_wechat:I = 0x7f0a0ce2

.field public static final ll_shared_dir_avatars:I = 0x7f0a0ce3

.field public static final ll_shutter_guide_root:I = 0x7f0a0ce4

.field public static final ll_shutter_mode_container:I = 0x7f0a0ce5

.field public static final ll_signature:I = 0x7f0a0ce6

.field public static final ll_signature_edit:I = 0x7f0a0ce7

.field public static final ll_signature_thumb:I = 0x7f0a0ce8

.field public static final ll_signature_thumb_text:I = 0x7f0a0ce9

.field public static final ll_signature_types_group:I = 0x7f0a0cea

.field public static final ll_signature_types_group_content:I = 0x7f0a0ceb

.field public static final ll_smdudge_annotation:I = 0x7f0a0cec

.field public static final ll_squared:I = 0x7f0a0ced

.field public static final ll_start_index_container:I = 0x7f0a0cee

.field public static final ll_stay_left_tag_root:I = 0x7f0a0cef

.field public static final ll_stay_top_tag_root:I = 0x7f0a0cf0

.field public static final ll_sub_button:I = 0x7f0a0cf1

.field public static final ll_submit:I = 0x7f0a0cf2

.field public static final ll_submit_ink:I = 0x7f0a0cf3

.field public static final ll_submit_mark:I = 0x7f0a0cf4

.field public static final ll_switch:I = 0x7f0a0cf5

.field public static final ll_switch_page:I = 0x7f0a0cf6

.field public static final ll_sync_tips:I = 0x7f0a0cf7

.field public static final ll_tab:I = 0x7f0a0cf8

.field public static final ll_tag_layout:I = 0x7f0a0cf9

.field public static final ll_take_next_page:I = 0x7f0a0cfa

.field public static final ll_take_next_page2:I = 0x7f0a0cfb

.field public static final ll_take_one_more:I = 0x7f0a0cfc

.field public static final ll_test:I = 0x7f0a0cfd

.field public static final ll_textarea:I = 0x7f0a0cfe

.field public static final ll_thumbs:I = 0x7f0a0cff

.field public static final ll_time_container:I = 0x7f0a0d00

.field public static final ll_tip:I = 0x7f0a0d01

.field public static final ll_tips:I = 0x7f0a0d02

.field public static final ll_tips_barcode_scan:I = 0x7f0a0d03

.field public static final ll_tips_for_login:I = 0x7f0a0d04

.field public static final ll_tips_network:I = 0x7f0a0d05

.field public static final ll_tips_sort:I = 0x7f0a0d06

.field public static final ll_title:I = 0x7f0a0d07

.field public static final ll_toolbar:I = 0x7f0a0d08

.field public static final ll_top_menu:I = 0x7f0a0d09

.field public static final ll_torch_switch:I = 0x7f0a0d0a

.field public static final ll_tp2_recent_first:I = 0x7f0a0d0b

.field public static final ll_tp2_recent_fourth:I = 0x7f0a0d0c

.field public static final ll_tp2_recent_second:I = 0x7f0a0d0d

.field public static final ll_tp2_recent_third:I = 0x7f0a0d0e

.field public static final ll_trial_renew_subtitle:I = 0x7f0a0d0f

.field public static final ll_trim_guide_root:I = 0x7f0a0d10

.field public static final ll_two:I = 0x7f0a0d11

.field public static final ll_type_share_dir:I = 0x7f0a0d12

.field public static final ll_undo:I = 0x7f0a0d13

.field public static final ll_unfinished:I = 0x7f0a0d14

.field public static final ll_unfocus:I = 0x7f0a0d15

.field public static final ll_user_agreement:I = 0x7f0a0d16

.field public static final ll_user_policy:I = 0x7f0a0d17

.field public static final ll_verify_root:I = 0x7f0a0d18

.field public static final ll_video:I = 0x7f0a0d19

.field public static final ll_vip_discount_six:I = 0x7f0a0d1a

.field public static final ll_vip_fun:I = 0x7f0a0d1b

.field public static final ll_vip_right_count:I = 0x7f0a0d1c

.field public static final ll_vip_tag:I = 0x7f0a0d1d

.field public static final ll_water_mark:I = 0x7f0a0d1e

.field public static final ll_wheel:I = 0x7f0a0d1f

.field public static final ll_widget_search_search:I = 0x7f0a0d20

.field public static final ll_word_flow_auto_send_email:I = 0x7f0a0d21

.field public static final ll_word_flow_cloud_storage:I = 0x7f0a0d22

.field public static final ll_word_guide_cap_wave:I = 0x7f0a0d23

.field public static final ll_work_flow_upload_third_net_disk:I = 0x7f0a0d24

.field public static final ll_workflow_edit_or_done:I = 0x7f0a0d25

.field public static final ll_workflow_email_one_validate:I = 0x7f0a0d26

.field public static final ll_year_count:I = 0x7f0a0d27

.field public static final llc_bottom_desc:I = 0x7f0a0d28

.field public static final llc_change_doc_name_advice:I = 0x7f0a0d29

.field public static final llc_compare:I = 0x7f0a0d2a

.field public static final llc_create_briefcase:I = 0x7f0a0d2b

.field public static final llc_create_card_bag:I = 0x7f0a0d2c

.field public static final llc_create_item_1_recommend:I = 0x7f0a0d2d

.field public static final llc_create_item_2_study:I = 0x7f0a0d2e

.field public static final llc_create_item_3_work:I = 0x7f0a0d2f

.field public static final llc_create_item_4_life:I = 0x7f0a0d30

.field public static final llc_excel_language_container:I = 0x7f0a0d31

.field public static final llc_flash_container:I = 0x7f0a0d32

.field public static final llc_loading:I = 0x7f0a0d33

.field public static final llc_pixel_all_container:I = 0x7f0a0d34

.field public static final llc_pixel_common:I = 0x7f0a0d35

.field public static final llc_recapture_last_page:I = 0x7f0a0d36

.field public static final llc_search_hl:I = 0x7f0a0d37

.field public static final llc_setting_desc:I = 0x7f0a0d38

.field public static final llc_setting_drop_filter:I = 0x7f0a0d39

.field public static final llc_setting_more_auto_capture:I = 0x7f0a0d3a

.field public static final llc_setting_more_auto_crop:I = 0x7f0a0d3b

.field public static final llc_setting_more_grid:I = 0x7f0a0d3c

.field public static final llc_setting_more_orientation:I = 0x7f0a0d3d

.field public static final llc_setting_more_orientation_container:I = 0x7f0a0d3e

.field public static final llc_setting_more_root:I = 0x7f0a0d3f

.field public static final llc_setting_more_root_outside:I = 0x7f0a0d40

.field public static final llc_setting_more_sensor:I = 0x7f0a0d41

.field public static final llc_setting_more_sound:I = 0x7f0a0d42

.field public static final llc_show_all_pixel:I = 0x7f0a0d43

.field public static final llc_show_common_pixel:I = 0x7f0a0d44

.field public static final llc_timeline_content_container:I = 0x7f0a0d45

.field public static final llc_timeline_date_container:I = 0x7f0a0d46

.field public static final llc_timeline_end_container:I = 0x7f0a0d47

.field public static final llc_top_container:I = 0x7f0a0d48

.field public static final loadProgress:I = 0x7f0a0d49

.field public static final load_more_load_complete_view:I = 0x7f0a0d4a

.field public static final load_more_load_end_view:I = 0x7f0a0d4b

.field public static final load_more_load_fail_view:I = 0x7f0a0d4c

.field public static final load_more_loading_view:I = 0x7f0a0d4d

.field public static final load_progress:I = 0x7f0a0d4e

.field public static final loading_progress:I = 0x7f0a0d4f

.field public static final loading_progress_bar:I = 0x7f0a0d50

.field public static final loading_text:I = 0x7f0a0d51

.field public static final loading_view:I = 0x7f0a0d52

.field public static final log_left_top:I = 0x7f0a0d53

.field public static final log_right_bottom:I = 0x7f0a0d54

.field public static final long_image_switch_vip:I = 0x7f0a0d55

.field public static final lottie_layer_name:I = 0x7f0a0d56

.field public static final lottie_loading:I = 0x7f0a0d57

.field public static final lottie_long_touch:I = 0x7f0a0d58

.field public static final lottie_ocr_nask:I = 0x7f0a0d59

.field public static final lottie_surface_guide:I = 0x7f0a0d5a

.field public static final lottie_view:I = 0x7f0a0d5b

.field public static final lottie_word_convert_guide:I = 0x7f0a0d5c

.field public static final lr_view:I = 0x7f0a0d5d

.field public static final lv_docs:I = 0x7f0a0d5e

.field public static final lv_purchase:I = 0x7f0a0d5f

.field public static final lv_recent_doc:I = 0x7f0a0d60

.field public static final lvp_banner:I = 0x7f0a0d61

.field public static final lvp_region_banner:I = 0x7f0a0d62

.field public static final lvp_top_banner:I = 0x7f0a0d63

.field public static final m3_side_sheet:I = 0x7f0a0d64

.field public static final magnifier_view:I = 0x7f0a0d65

.field public static final mail_to_me:I = 0x7f0a0d66

.field public static final mail_to_me_input_hint:I = 0x7f0a0d67

.field public static final mail_to_me_set_hint:I = 0x7f0a0d68

.field public static final mail_to_me_set_space:I = 0x7f0a0d69

.field public static final main:I = 0x7f0a0d6a

.field public static final main_bottom_edit_mode_tab:I = 0x7f0a0d6b

.field public static final main_bottom_tab:I = 0x7f0a0d6c

.field public static final main_bottom_tab_dot_text:I = 0x7f0a0d6d

.field public static final main_bottom_tab_image:I = 0x7f0a0d6e

.field public static final main_bottom_tab_text:I = 0x7f0a0d6f

.field public static final main_container:I = 0x7f0a0d70

.field public static final main_doc_menu:I = 0x7f0a0d71

.field public static final main_fragment_id:I = 0x7f0a0d72

.field public static final main_home_kingkong_bar:I = 0x7f0a0d73

.field public static final main_home_kingkong_image:I = 0x7f0a0d74

.field public static final main_home_kingkong_image_new:I = 0x7f0a0d75

.field public static final main_home_kingkong_text:I = 0x7f0a0d76

.field public static final main_home_list_kingkong_bar:I = 0x7f0a0d77

.field public static final main_home_message_view:I = 0x7f0a0d78

.field public static final main_home_recyclerview:I = 0x7f0a0d79

.field public static final main_home_top_bar:I = 0x7f0a0d7a

.field public static final main_home_top_bar_layout:I = 0x7f0a0d7b

.field public static final main_home_top_search:I = 0x7f0a0d7c

.field public static final main_include_screenshot:I = 0x7f0a0d7d

.field public static final main_list_pull_refresh_view:I = 0x7f0a0d7e

.field public static final main_viewpager:I = 0x7f0a0d7f

.field public static final marquee:I = 0x7f0a0d80

.field public static final mask:I = 0x7f0a0d81

.field public static final mask_end:I = 0x7f0a0d82

.field public static final mask_start:I = 0x7f0a0d83

.field public static final mask_tip_frame:I = 0x7f0a0d84

.field public static final mask_view_greet_card:I = 0x7f0a0d85

.field public static final mask_view_pop_arrow_view:I = 0x7f0a0d86

.field public static final mask_view_signature:I = 0x7f0a0d87

.field public static final mask_view_topic:I = 0x7f0a0d88

.field public static final masked:I = 0x7f0a0d89

.field public static final massage_blank:I = 0x7f0a0d8a

.field public static final match_constraint:I = 0x7f0a0d8b

.field public static final match_global_nicknames:I = 0x7f0a0d8c

.field public static final match_parent:I = 0x7f0a0d8d

.field public static final material_clock_display:I = 0x7f0a0d8e

.field public static final material_clock_display_and_toggle:I = 0x7f0a0d8f

.field public static final material_clock_face:I = 0x7f0a0d90

.field public static final material_clock_hand:I = 0x7f0a0d91

.field public static final material_clock_level:I = 0x7f0a0d92

.field public static final material_clock_period_am_button:I = 0x7f0a0d93

.field public static final material_clock_period_pm_button:I = 0x7f0a0d94

.field public static final material_clock_period_toggle:I = 0x7f0a0d95

.field public static final material_hour_text_input:I = 0x7f0a0d96

.field public static final material_hour_tv:I = 0x7f0a0d97

.field public static final material_label:I = 0x7f0a0d98

.field public static final material_minute_text_input:I = 0x7f0a0d99

.field public static final material_minute_tv:I = 0x7f0a0d9a

.field public static final material_textinput_timepicker:I = 0x7f0a0d9b

.field public static final material_timepicker_cancel_button:I = 0x7f0a0d9c

.field public static final material_timepicker_container:I = 0x7f0a0d9d

.field public static final material_timepicker_mode_button:I = 0x7f0a0d9e

.field public static final material_timepicker_ok_button:I = 0x7f0a0d9f

.field public static final material_timepicker_view:I = 0x7f0a0da0

.field public static final material_value_index:I = 0x7f0a0da1

.field public static final matrix:I = 0x7f0a0da2

.field public static final mediaView:I = 0x7f0a0da3

.field public static final media_actions:I = 0x7f0a0da4

.field public static final menu_black_white:I = 0x7f0a0da5

.field public static final menu_gray:I = 0x7f0a0da6

.field public static final menu_lighten:I = 0x7f0a0da7

.field public static final menu_list:I = 0x7f0a0da8

.field public static final menu_magic:I = 0x7f0a0da9

.field public static final menu_original:I = 0x7f0a0daa

.field public static final menu_remove_shadow:I = 0x7f0a0dab

.field public static final menu_super_filter:I = 0x7f0a0dac

.field public static final menu_title:I = 0x7f0a0dad

.field public static final menu_white_black:I = 0x7f0a0dae

.field public static final message:I = 0x7f0a0daf

.field public static final message_close:I = 0x7f0a0db0

.field public static final message_close_test_time:I = 0x7f0a0db1

.field public static final message_content:I = 0x7f0a0db2

.field public static final message_icon:I = 0x7f0a0db3

.field public static final message_panel:I = 0x7f0a0db4

.field public static final message_root:I = 0x7f0a0db5

.field public static final message_textview:I = 0x7f0a0db6

.field public static final message_view:I = 0x7f0a0db7

.field public static final middle:I = 0x7f0a0db8

.field public static final middle_banner_container:I = 0x7f0a0db9

.field public static final middle_operation_container:I = 0x7f0a0dba

.field public static final middle_point:I = 0x7f0a0dbb

.field public static final middle_view:I = 0x7f0a0dbc

.field public static final mini:I = 0x7f0a0dbd

.field public static final ml_bottom_layout:I = 0x7f0a0dbe

.field public static final ml_function_recommend:I = 0x7f0a0dbf

.field public static final ml_vip_right:I = 0x7f0a0dc0

.field public static final mode_hint_text:I = 0x7f0a0dc1

.field public static final modification:I = 0x7f0a0dc2

.field public static final month_grid:I = 0x7f0a0dc3

.field public static final month_navigation_bar:I = 0x7f0a0dc4

.field public static final month_navigation_fragment_toggle:I = 0x7f0a0dc5

.field public static final month_navigation_next:I = 0x7f0a0dc6

.field public static final month_navigation_previous:I = 0x7f0a0dc7

.field public static final month_title:I = 0x7f0a0dc8

.field public static final motion_base:I = 0x7f0a0dc9

.field public static final motion_layout:I = 0x7f0a0dca

.field public static final motion_layout_doc_page_header:I = 0x7f0a0dcb

.field public static final motion_layout_header:I = 0x7f0a0dcc

.field public static final mrec_ad_view_container:I = 0x7f0a0dcd

.field public static final mrec_control_button:I = 0x7f0a0dce

.field public static final mrec_control_view:I = 0x7f0a0dcf

.field public static final msg_container:I = 0x7f0a0dd0

.field public static final mtrl_anchor_parent:I = 0x7f0a0dd1

.field public static final mtrl_calendar_day_selector_frame:I = 0x7f0a0dd2

.field public static final mtrl_calendar_days_of_week:I = 0x7f0a0dd3

.field public static final mtrl_calendar_frame:I = 0x7f0a0dd4

.field public static final mtrl_calendar_main_pane:I = 0x7f0a0dd5

.field public static final mtrl_calendar_months:I = 0x7f0a0dd6

.field public static final mtrl_calendar_selection_frame:I = 0x7f0a0dd7

.field public static final mtrl_calendar_text_input_frame:I = 0x7f0a0dd8

.field public static final mtrl_calendar_year_selector_frame:I = 0x7f0a0dd9

.field public static final mtrl_card_checked_layer_id:I = 0x7f0a0dda

.field public static final mtrl_child_content_container:I = 0x7f0a0ddb

.field public static final mtrl_internal_children_alpha_tag:I = 0x7f0a0ddc

.field public static final mtrl_motion_snapshot_view:I = 0x7f0a0ddd

.field public static final mtrl_picker_fullscreen:I = 0x7f0a0dde

.field public static final mtrl_picker_header:I = 0x7f0a0ddf

.field public static final mtrl_picker_header_selection_text:I = 0x7f0a0de0

.field public static final mtrl_picker_header_title_and_selection:I = 0x7f0a0de1

.field public static final mtrl_picker_header_toggle:I = 0x7f0a0de2

.field public static final mtrl_picker_text_input_date:I = 0x7f0a0de3

.field public static final mtrl_picker_text_input_range_end:I = 0x7f0a0de4

.field public static final mtrl_picker_text_input_range_start:I = 0x7f0a0de5

.field public static final mtrl_picker_title_text:I = 0x7f0a0de6

.field public static final mtrl_view_tag_bottom_padding:I = 0x7f0a0de7

.field public static final multi_thumb:I = 0x7f0a0de8

.field public static final multi_thumb_num:I = 0x7f0a0de9

.field public static final multiply:I = 0x7f0a0dea

.field public static final mv_cs_pdf_drainage_entrance:I = 0x7f0a0deb

.field public static final mv_vip:I = 0x7f0a0dec

.field public static final mvp_content:I = 0x7f0a0ded

.field public static final my_view_pager:I = 0x7f0a0dee

.field public static final native_ad_view_container:I = 0x7f0a0def

.field public static final native_control_button:I = 0x7f0a0df0

.field public static final native_control_view:I = 0x7f0a0df1

.field public static final navigation_bar_item_active_indicator_view:I = 0x7f0a0df2

.field public static final navigation_bar_item_icon_container:I = 0x7f0a0df3

.field public static final navigation_bar_item_icon_view:I = 0x7f0a0df4

.field public static final navigation_bar_item_labels_group:I = 0x7f0a0df5

.field public static final navigation_bar_item_large_label_view:I = 0x7f0a0df6

.field public static final navigation_bar_item_small_label_view:I = 0x7f0a0df7

.field public static final navigation_header_container:I = 0x7f0a0df8

.field public static final negative_premium_indicator_view:I = 0x7f0a0df9

.field public static final negative_premium_life_time_indicator_view_container:I = 0x7f0a0dfa

.field public static final negative_premium_life_time_price_layout:I = 0x7f0a0dfb

.field public static final never:I = 0x7f0a0dfc

.field public static final neverCompleteToEnd:I = 0x7f0a0dfd

.field public static final neverCompleteToStart:I = 0x7f0a0dfe

.field public static final never_display:I = 0x7f0a0dff

.field public static final new_layout:I = 0x7f0a0e00

.field public static final new_password:I = 0x7f0a0e01

.field public static final new_toolbar:I = 0x7f0a0e02

.field public static final nine_photo_view:I = 0x7f0a0e03

.field public static final noScroll:I = 0x7f0a0e04

.field public static final noState:I = 0x7f0a0e05

.field public static final none:I = 0x7f0a0e06

.field public static final normal:I = 0x7f0a0e07

.field public static final normal_import:I = 0x7f0a0e08

.field public static final normal_import_doc_file:I = 0x7f0a0e09

.field public static final normal_import_picture:I = 0x7f0a0e0a

.field public static final normal_panel:I = 0x7f0a0e0b

.field public static final normal_panel_new:I = 0x7f0a0e0c

.field public static final normal_shutter_button:I = 0x7f0a0e0d

.field public static final north:I = 0x7f0a0e0e

.field public static final note_fragment_container:I = 0x7f0a0e0f

.field public static final notification_background:I = 0x7f0a0e10

.field public static final notification_main_column:I = 0x7f0a0e11

.field public static final notification_main_column_container:I = 0x7f0a0e12

.field public static final notify_me_after_transferred:I = 0x7f0a0e13

.field public static final novice_close_test_time:I = 0x7f0a0e14

.field public static final nowrap:I = 0x7f0a0e15

.field public static final nsv_content:I = 0x7f0a0e16

.field public static final nsv_data_show_content:I = 0x7f0a0e17

.field public static final number:I = 0x7f0a0e18

.field public static final numberDecimal:I = 0x7f0a0e19

.field public static final numberSigned:I = 0x7f0a0e1a

.field public static final ocrResult:I = 0x7f0a0e1b

.field public static final ocr_back:I = 0x7f0a0e1c

.field public static final ocr_description_pic:I = 0x7f0a0e1d

.field public static final ocr_description_video:I = 0x7f0a0e1e

.field public static final ocr_frame_view:I = 0x7f0a0e1f

.field public static final ocr_import:I = 0x7f0a0e20

.field public static final ocr_import_doc_file:I = 0x7f0a0e21

.field public static final ocr_region_bottom_layout:I = 0x7f0a0e22

.field public static final ocr_shutter_button:I = 0x7f0a0e23

.field public static final ocr_text_pic_down:I = 0x7f0a0e24

.field public static final ocr_text_pic_up:I = 0x7f0a0e25

.field public static final ocr_thumb:I = 0x7f0a0e26

.field public static final ocr_thumb_num:I = 0x7f0a0e27

.field public static final ocr_view_pager:I = 0x7f0a0e28

.field public static final off:I = 0x7f0a0e29

.field public static final office_bottom_bar:I = 0x7f0a0e2a

.field public static final old_layout:I = 0x7f0a0e2b

.field public static final omnibox_title_section:I = 0x7f0a0e2c

.field public static final omnibox_url_section:I = 0x7f0a0e2d

.field public static final on:I = 0x7f0a0e2e

.field public static final onAttachStateChangeListener:I = 0x7f0a0e2f

.field public static final onDateChanged:I = 0x7f0a0e30

.field public static final onDown:I = 0x7f0a0e31

.field public static final onLongPress:I = 0x7f0a0e32

.field public static final onMove:I = 0x7f0a0e33

.field public static final one:I = 0x7f0a0e34

.field public static final open_graph:I = 0x7f0a0e35

.field public static final open_link:I = 0x7f0a0e36

.field public static final open_market:I = 0x7f0a0e37

.field public static final openapi_icon:I = 0x7f0a0e38

.field public static final openapi_text:I = 0x7f0a0e39

.field public static final operation_close:I = 0x7f0a0e3a

.field public static final operation_icon:I = 0x7f0a0e3b

.field public static final operation_sub_title:I = 0x7f0a0e3c

.field public static final operation_title:I = 0x7f0a0e3d

.field public static final outline:I = 0x7f0a0e3e

.field public static final outward:I = 0x7f0a0e3f

.field public static final overshoot:I = 0x7f0a0e40

.field public static final packed:I = 0x7f0a0e41

.field public static final page:I = 0x7f0a0e42

.field public static final page_image:I = 0x7f0a0e43

.field public static final page_index:I = 0x7f0a0e44

.field public static final page_item_check:I = 0x7f0a0e45

.field public static final page_item_pagenum:I = 0x7f0a0e46

.field public static final page_layout_purchase_new_style2_content:I = 0x7f0a0e47

.field public static final page_list_ad_id:I = 0x7f0a0e48

.field public static final page_switch:I = 0x7f0a0e49

.field public static final pager_body:I = 0x7f0a0e4a

.field public static final pages:I = 0x7f0a0e4b

.field public static final paging_seal_view:I = 0x7f0a0e4c

.field public static final paper_inch_list:I = 0x7f0a0e4d

.field public static final paper_title:I = 0x7f0a0e4e

.field public static final parallax:I = 0x7f0a0e4f

.field public static final parent:I = 0x7f0a0e50

.field public static final parentPanel:I = 0x7f0a0e51

.field public static final parentRelative:I = 0x7f0a0e52

.field public static final parent_matrix:I = 0x7f0a0e53

.field public static final partner_links_textview:I = 0x7f0a0e54

.field public static final partners_content_view:I = 0x7f0a0e55

.field public static final password:I = 0x7f0a0e56

.field public static final password_toggle:I = 0x7f0a0e57

.field public static final path:I = 0x7f0a0e58

.field public static final pathRelative:I = 0x7f0a0e59

.field public static final pb:I = 0x7f0a0e5a

.field public static final pb_book_progress_bar:I = 0x7f0a0e5b

.field public static final pb_connecting:I = 0x7f0a0e5c

.field public static final pb_import:I = 0x7f0a0e5d

.field public static final pb_item_me_page_header_progressbar:I = 0x7f0a0e5e

.field public static final pb_loading:I = 0x7f0a0e5f

.field public static final pb_progress:I = 0x7f0a0e60

.field public static final pb_progress_bar:I = 0x7f0a0e61

.field public static final pb_refresh:I = 0x7f0a0e62

.field public static final pb_sync_progress:I = 0x7f0a0e63

.field public static final pb_vip_level:I = 0x7f0a0e64

.field public static final pdf_content:I = 0x7f0a0e65

.field public static final pdf_edit_root:I = 0x7f0a0e66

.field public static final pdf_edit_view:I = 0x7f0a0e67

.field public static final pdf_lottie_view:I = 0x7f0a0e68

.field public static final pdf_page_num:I = 0x7f0a0e69

.field public static final pdf_property_list:I = 0x7f0a0e6a

.field public static final pdf_root:I = 0x7f0a0e6b

.field public static final pdf_signature_action_boldline:I = 0x7f0a0e6c

.field public static final pdf_signature_action_thin:I = 0x7f0a0e6d

.field public static final pdf_signature_action_thinline:I = 0x7f0a0e6e

.field public static final pdf_signature_action_view:I = 0x7f0a0e6f

.field public static final pdf_size_Button:I = 0x7f0a0e70

.field public static final pdf_size_height_et:I = 0x7f0a0e71

.field public static final pdf_size_title:I = 0x7f0a0e72

.field public static final pdf_size_value:I = 0x7f0a0e73

.field public static final pdf_size_width_et:I = 0x7f0a0e74

.field public static final pdf_sizename:I = 0x7f0a0e75

.field public static final pdf_view:I = 0x7f0a0e76

.field public static final peekHeight:I = 0x7f0a0e77

.field public static final percent:I = 0x7f0a0e78

.field public static final performance:I = 0x7f0a0e79

.field public static final permission_setting_layout:I = 0x7f0a0e7a

.field public static final personalized_advertising_switch:I = 0x7f0a0e7b

.field public static final personalized_advertising_switch_textview:I = 0x7f0a0e7c

.field public static final pgbar_progress:I = 0x7f0a0e7d

.field public static final phone:I = 0x7f0a0e7e

.field public static final pic:I = 0x7f0a0e7f

.field public static final pickOneImage:I = 0x7f0a0e80

.field public static final pin:I = 0x7f0a0e81

.field public static final pinEditText:I = 0x7f0a0e82

.field public static final piv_forever:I = 0x7f0a0e83

.field public static final piv_month:I = 0x7f0a0e84

.field public static final piv_year:I = 0x7f0a0e85

.field public static final plain:I = 0x7f0a0e86

.field public static final popupList:I = 0x7f0a0e87

.field public static final popup_menu_listview:I = 0x7f0a0e88

.field public static final position:I = 0x7f0a0e89

.field public static final postLayout:I = 0x7f0a0e8a

.field public static final ppt_animation_view:I = 0x7f0a0e8b

.field public static final ppt_auto_scale_root:I = 0x7f0a0e8c

.field public static final ppt_back:I = 0x7f0a0e8d

.field public static final ppt_shutter_button:I = 0x7f0a0e8e

.field public static final pressed:I = 0x7f0a0e8f

.field public static final preview:I = 0x7f0a0e90

.field public static final preview_view:I = 0x7f0a0e91

.field public static final price_container:I = 0x7f0a0e92

.field public static final printer_btn:I = 0x7f0a0e93

.field public static final privacy_policy_switch:I = 0x7f0a0e94

.field public static final privacy_policy_switch_textview:I = 0x7f0a0e95

.field public static final progress:I = 0x7f0a0e96

.field public static final progressBar:I = 0x7f0a0e97

.field public static final progressNum:I = 0x7f0a0e98

.field public static final progress_bar:I = 0x7f0a0e99

.field public static final progress_circular:I = 0x7f0a0e9a

.field public static final progress_horizontal:I = 0x7f0a0e9b

.field public static final progress_number:I = 0x7f0a0e9c

.field public static final progress_percent:I = 0x7f0a0e9d

.field public static final progress_root_layout:I = 0x7f0a0e9e

.field public static final protocol_korea:I = 0x7f0a0e9f

.field public static final proxy_back:I = 0x7f0a0ea0

.field public static final pull_refresh_view:I = 0x7f0a0ea1

.field public static final pull_to_refresh_image:I = 0x7f0a0ea2

.field public static final pull_to_refresh_progress:I = 0x7f0a0ea3

.field public static final pull_to_refresh_text:I = 0x7f0a0ea4

.field public static final pull_to_refresh_updated_at:I = 0x7f0a0ea5

.field public static final purchase_describe:I = 0x7f0a0ea6

.field public static final purchase_describe_scroll:I = 0x7f0a0ea7

.field public static final purchase_dialog_price_style:I = 0x7f0a0ea8

.field public static final purchase_dialog_viewpager:I = 0x7f0a0ea9

.field public static final purchase_forever_bottom_layout:I = 0x7f0a0eaa

.field public static final purchase_forever_list_layout:I = 0x7f0a0eab

.field public static final purchase_forever_top_layout:I = 0x7f0a0eac

.field public static final purchase_free_trial:I = 0x7f0a0ead

.field public static final purchase_free_trial_layout:I = 0x7f0a0eae

.field public static final purchase_full_screen_bottom_layout:I = 0x7f0a0eaf

.field public static final purchase_full_screen_bottom_stub:I = 0x7f0a0eb0

.field public static final purchase_full_screen_bottom_stub_container:I = 0x7f0a0eb1

.field public static final purchase_full_screen_close:I = 0x7f0a0eb2

.field public static final purchase_full_screen_list_layout:I = 0x7f0a0eb3

.field public static final purchase_full_screen_top_layout:I = 0x7f0a0eb4

.field public static final purchase_full_screen_top_stub:I = 0x7f0a0eb5

.field public static final purchase_full_screen_viewpager:I = 0x7f0a0eb6

.field public static final purchase_full_screen_viewpager_layout:I = 0x7f0a0eb7

.field public static final purchase_loading:I = 0x7f0a0eb8

.field public static final purchase_price_describe:I = 0x7f0a0eb9

.field public static final purchase_price_describe_extra:I = 0x7f0a0eba

.field public static final purchase_style_layout:I = 0x7f0a0ebb

.field public static final purchase_subtitle:I = 0x7f0a0ebc

.field public static final purchase_title:I = 0x7f0a0ebd

.field public static final purchase_viewpager:I = 0x7f0a0ebe

.field public static final purchase_viewpager_dots:I = 0x7f0a0ebf

.field public static final pv_buy:I = 0x7f0a0ec0

.field public static final pv_free_trial:I = 0x7f0a0ec1

.field public static final pv_free_trial_new:I = 0x7f0a0ec2

.field public static final pv_month:I = 0x7f0a0ec3

.field public static final pv_month_style:I = 0x7f0a0ec4

.field public static final pv_point:I = 0x7f0a0ec5

.field public static final pv_points:I = 0x7f0a0ec6

.field public static final pv_try:I = 0x7f0a0ec7

.field public static final pv_week:I = 0x7f0a0ec8

.field public static final pv_year:I = 0x7f0a0ec9

.field public static final pv_year_style:I = 0x7f0a0eca

.field public static final pv_year_style_single:I = 0x7f0a0ecb

.field public static final qr_bot_line:I = 0x7f0a0ecc

.field public static final qr_top_line:I = 0x7f0a0ecd

.field public static final radio:I = 0x7f0a0ece

.field public static final radioButton:I = 0x7f0a0ecf

.field public static final radio_btn:I = 0x7f0a0ed0

.field public static final radio_btn_1:I = 0x7f0a0ed1

.field public static final radio_btn_2:I = 0x7f0a0ed2

.field public static final radio_btn_3:I = 0x7f0a0ed3

.field public static final radio_btn_ali:I = 0x7f0a0ed4

.field public static final radio_btn_wx:I = 0x7f0a0ed5

.field public static final radio_group:I = 0x7f0a0ed6

.field public static final ratio:I = 0x7f0a0ed7

.field public static final rb_attract_for_reward_check:I = 0x7f0a0ed8

.field public static final rb_camera_api_camera1:I = 0x7f0a0ed9

.field public static final rb_camera_api_camera1_old:I = 0x7f0a0eda

.field public static final rb_camera_api_camera2:I = 0x7f0a0edb

.field public static final rb_camera_api_camera_auto:I = 0x7f0a0edc

.field public static final rb_camera_api_camera_new_auto:I = 0x7f0a0edd

.field public static final rb_camera_api_camera_server:I = 0x7f0a0ede

.field public static final rb_camera_api_camerah:I = 0x7f0a0edf

.field public static final rb_camera_api_camerax:I = 0x7f0a0ee0

.field public static final rb_camera_api_camerax_old:I = 0x7f0a0ee1

.field public static final rb_camera_api_page_0:I = 0x7f0a0ee2

.field public static final rb_doc_archive:I = 0x7f0a0ee3

.field public static final rb_doc_archive_no:I = 0x7f0a0ee4

.field public static final rb_excel_check:I = 0x7f0a0ee5

.field public static final rb_ht_duty_explain_check:I = 0x7f0a0ee6

.field public static final rb_one_month:I = 0x7f0a0ee7

.field public static final rb_pay_type_ali:I = 0x7f0a0ee8

.field public static final rb_pay_type_wx:I = 0x7f0a0ee9

.field public static final rb_refactor_camera_page_0:I = 0x7f0a0eea

.field public static final rb_security_mark_color_black:I = 0x7f0a0eeb

.field public static final rb_security_mark_color_blue:I = 0x7f0a0eec

.field public static final rb_security_mark_color_green:I = 0x7f0a0eed

.field public static final rb_security_mark_color_orange:I = 0x7f0a0eee

.field public static final rb_security_mark_color_red:I = 0x7f0a0eef

.field public static final rb_security_mark_color_white:I = 0x7f0a0ef0

.field public static final rb_seven_day:I = 0x7f0a0ef1

.field public static final rb_silent_ocr_off:I = 0x7f0a0ef2

.field public static final rb_silent_ocr_on:I = 0x7f0a0ef3

.field public static final rb_silent_ocr_server:I = 0x7f0a0ef4

.field public static final rb_switch_all_picture:I = 0x7f0a0ef5

.field public static final rb_switch_can_edit:I = 0x7f0a0ef6

.field public static final rb_switch_close:I = 0x7f0a0ef7

.field public static final rb_switch_document_picture:I = 0x7f0a0ef8

.field public static final rb_switch_local_hide:I = 0x7f0a0ef9

.field public static final rb_switch_local_show:I = 0x7f0a0efa

.field public static final rb_switch_only_all_picture:I = 0x7f0a0efb

.field public static final rb_switch_open:I = 0x7f0a0efc

.field public static final rb_switch_server:I = 0x7f0a0efd

.field public static final rb_switch_upload_only:I = 0x7f0a0efe

.field public static final rb_switch_view_only:I = 0x7f0a0eff

.field public static final rb_tips_switch_close:I = 0x7f0a0f00

.field public static final rb_tips_switch_open:I = 0x7f0a0f01

.field public static final rb_type_0:I = 0x7f0a0f02

.field public static final rb_type_1:I = 0x7f0a0f03

.field public static final rb_type_2:I = 0x7f0a0f04

.field public static final rb_type_3:I = 0x7f0a0f05

.field public static final rb_type_4:I = 0x7f0a0f06

.field public static final rc_filter_list:I = 0x7f0a0f07

.field public static final rc_list:I = 0x7f0a0f08

.field public static final rc_paper_list:I = 0x7f0a0f09

.field public static final rec_view:I = 0x7f0a0f0a

.field public static final rec_view_share:I = 0x7f0a0f0b

.field public static final rec_view_share_types:I = 0x7f0a0f0c

.field public static final recent_title:I = 0x7f0a0f0d

.field public static final recognized_tag:I = 0x7f0a0f0e

.field public static final rectangles:I = 0x7f0a0f0f

.field public static final recycl_greetcard:I = 0x7f0a0f10

.field public static final recycle_certification:I = 0x7f0a0f11

.field public static final recycler_apps:I = 0x7f0a0f12

.field public static final recycler_data_list:I = 0x7f0a0f13

.field public static final recycler_frames:I = 0x7f0a0f14

.field public static final recycler_image:I = 0x7f0a0f15

.field public static final recycler_menu:I = 0x7f0a0f16

.field public static final recycler_share_view:I = 0x7f0a0f17

.field public static final recycler_view:I = 0x7f0a0f18

.field public static final recycler_view_functions:I = 0x7f0a0f19

.field public static final recycler_view_picture:I = 0x7f0a0f1a

.field public static final recycler_view_tags:I = 0x7f0a0f1b

.field public static final recyclerview:I = 0x7f0a0f1c

.field public static final recyclerview_contact:I = 0x7f0a0f1d

.field public static final recyclerview_tags:I = 0x7f0a0f1e

.field public static final recyclerview_tags_flex:I = 0x7f0a0f1f

.field public static final redEye:I = 0x7f0a0f20

.field public static final red_dot:I = 0x7f0a0f21

.field public static final redeem_renewal_bottom_layout:I = 0x7f0a0f22

.field public static final redeem_renewal_month_style:I = 0x7f0a0f23

.field public static final redeem_renewal_month_style_layout:I = 0x7f0a0f24

.field public static final redeem_renewal_month_tips:I = 0x7f0a0f25

.field public static final redeem_renewal_time_description:I = 0x7f0a0f26

.field public static final redeem_renewal_year_description:I = 0x7f0a0f27

.field public static final redeem_renewal_year_style:I = 0x7f0a0f28

.field public static final redeem_renewal_year_tips:I = 0x7f0a0f29

.field public static final regis_country_name:I = 0x7f0a0f2a

.field public static final relativeLayout_wechat:I = 0x7f0a0f2b

.field public static final rename_dialog_edit:I = 0x7f0a0f2c

.field public static final report_ad_button:I = 0x7f0a0f2d

.field public static final report_ad_tv:I = 0x7f0a0f2e

.field public static final report_complain:I = 0x7f0a0f2f

.field public static final restart:I = 0x7f0a0f30

.field public static final restore_import:I = 0x7f0a0f31

.field public static final restore_import_container:I = 0x7f0a0f32

.field public static final restore_shutter_button:I = 0x7f0a0f33

.field public static final reverse:I = 0x7f0a0f34

.field public static final reverseSawtooth:I = 0x7f0a0f35

.field public static final reward_btn:I = 0x7f0a0f36

.field public static final reward_close_icon:I = 0x7f0a0f37

.field public static final reward_des:I = 0x7f0a0f38

.field public static final reward_gift_icon:I = 0x7f0a0f39

.field public static final reward_sub_title:I = 0x7f0a0f3a

.field public static final reward_title:I = 0x7f0a0f3b

.field public static final reward_video_view:I = 0x7f0a0f3c

.field public static final rewarded_control_button:I = 0x7f0a0f3d

.field public static final rewarded_control_view:I = 0x7f0a0f3e

.field public static final rewarded_interstitial_control_button:I = 0x7f0a0f3f

.field public static final rewarded_interstitial_control_view:I = 0x7f0a0f40

.field public static final rfc822:I = 0x7f0a0f41

.field public static final rg_camera_api:I = 0x7f0a0f42

.field public static final rg_camera_api_control:I = 0x7f0a0f43

.field public static final rg_camera_api_old:I = 0x7f0a0f44

.field public static final rg_doc_archive:I = 0x7f0a0f45

.field public static final rg_edu_banner_cfg:I = 0x7f0a0f46

.field public static final rg_edu_banner_cfg_by_server:I = 0x7f0a0f47

.field public static final rg_edu_banner_cfg_local_close:I = 0x7f0a0f48

.field public static final rg_edu_banner_cfg_local_open:I = 0x7f0a0f49

.field public static final rg_security_mark_color:I = 0x7f0a0f4a

.field public static final rg_slient_ocr_control:I = 0x7f0a0f4b

.field public static final rg_switch:I = 0x7f0a0f4c

.field public static final rg_tips_switch:I = 0x7f0a0f4d

.field public static final rg_valid_period:I = 0x7f0a0f4e

.field public static final right:I = 0x7f0a0f4f

.field public static final rightToLeft:I = 0x7f0a0f50

.field public static final right_icon:I = 0x7f0a0f51

.field public static final right_section:I = 0x7f0a0f52

.field public static final right_side:I = 0x7f0a0f53

.field public static final ritb_excel_import:I = 0x7f0a0f54

.field public static final ritb_qr_history:I = 0x7f0a0f55

.field public static final ritb_qr_import:I = 0x7f0a0f56

.field public static final riv_back:I = 0x7f0a0f57

.field public static final riv_e_evidence_icon:I = 0x7f0a0f58

.field public static final riv_excel_back:I = 0x7f0a0f59

.field public static final riv_excel_record_icon:I = 0x7f0a0f5a

.field public static final riv_excel_shutter_button:I = 0x7f0a0f5b

.field public static final riv_excel_thumb:I = 0x7f0a0f5c

.field public static final riv_excel_thumb_num:I = 0x7f0a0f5d

.field public static final riv_import_doc:I = 0x7f0a0f5e

.field public static final riv_import_doc_file:I = 0x7f0a0f5f

.field public static final riv_import_pdf_to_office:I = 0x7f0a0f60

.field public static final riv_import_pic:I = 0x7f0a0f61

.field public static final riv_shutter_button:I = 0x7f0a0f62

.field public static final riv_word_back:I = 0x7f0a0f63

.field public static final rl:I = 0x7f0a0f64

.field public static final rl_action_menu:I = 0x7f0a0f65

.field public static final rl_ad_container:I = 0x7f0a0f66

.field public static final rl_ad_item:I = 0x7f0a0f67

.field public static final rl_all_files_access:I = 0x7f0a0f68

.field public static final rl_apis_gid_etc:I = 0x7f0a0f69

.field public static final rl_auto_backup:I = 0x7f0a0f6a

.field public static final rl_backup_root:I = 0x7f0a0f6b

.field public static final rl_backup_toolbar:I = 0x7f0a0f6c

.field public static final rl_backup_type:I = 0x7f0a0f6d

.field public static final rl_bg:I = 0x7f0a0f6e

.field public static final rl_bottom:I = 0x7f0a0f6f

.field public static final rl_bottom_bar:I = 0x7f0a0f70

.field public static final rl_bottom_container:I = 0x7f0a0f71

.field public static final rl_bottom_doc_item:I = 0x7f0a0f72

.field public static final rl_bottom_shutter_container:I = 0x7f0a0f73

.field public static final rl_cam_exam:I = 0x7f0a0f74

.field public static final rl_cap_guide_wave:I = 0x7f0a0f75

.field public static final rl_capture_layout:I = 0x7f0a0f76

.field public static final rl_certificate_detail_head_content:I = 0x7f0a0f77

.field public static final rl_certificate_menu:I = 0x7f0a0f78

.field public static final rl_change_camera_setting:I = 0x7f0a0f79

.field public static final rl_cloud:I = 0x7f0a0f7a

.field public static final rl_cloud_disk_file:I = 0x7f0a0f7b

.field public static final rl_cloud_disk_rec:I = 0x7f0a0f7c

.field public static final rl_cloud_disk_rec_root:I = 0x7f0a0f7d

.field public static final rl_cn_purchase_close:I = 0x7f0a0f7e

.field public static final rl_color_layout:I = 0x7f0a0f7f

.field public static final rl_content:I = 0x7f0a0f80

.field public static final rl_crash_hua_wei:I = 0x7f0a0f81

.field public static final rl_cs_loading_root:I = 0x7f0a0f82

.field public static final rl_cs_qr_code_container:I = 0x7f0a0f83

.field public static final rl_deep_clean:I = 0x7f0a0f84

.field public static final rl_doc_item:I = 0x7f0a0f85

.field public static final rl_doc_item_banner:I = 0x7f0a0f86

.field public static final rl_doc_list_item:I = 0x7f0a0f87

.field public static final rl_doc_name_setting:I = 0x7f0a0f88

.field public static final rl_doc_root_header:I = 0x7f0a0f89

.field public static final rl_download:I = 0x7f0a0f8a

.field public static final rl_draft_root:I = 0x7f0a0f8b

.field public static final rl_e_evidence_top_dialog_container:I = 0x7f0a0f8c

.field public static final rl_email_pwd_root:I = 0x7f0a0f8d

.field public static final rl_empty:I = 0x7f0a0f8e

.field public static final rl_excel:I = 0x7f0a0f8f

.field public static final rl_excel_rec_root:I = 0x7f0a0f90

.field public static final rl_excel_record:I = 0x7f0a0f91

.field public static final rl_excel_root_capture_guide:I = 0x7f0a0f92

.field public static final rl_fail:I = 0x7f0a0f93

.field public static final rl_feec_back:I = 0x7f0a0f94

.field public static final rl_feedback:I = 0x7f0a0f95

.field public static final rl_feedback_pack:I = 0x7f0a0f96

.field public static final rl_fix_database:I = 0x7f0a0f97

.field public static final rl_fix_dir_layer:I = 0x7f0a0f98

.field public static final rl_fix_doc_thumb:I = 0x7f0a0f99

.field public static final rl_fix_page_number:I = 0x7f0a0f9a

.field public static final rl_folder:I = 0x7f0a0f9b

.field public static final rl_folder_detail:I = 0x7f0a0f9c

.field public static final rl_folder_image:I = 0x7f0a0f9d

.field public static final rl_folder_item:I = 0x7f0a0f9e

.field public static final rl_folder_search_tip:I = 0x7f0a0f9f

.field public static final rl_forget_pwd:I = 0x7f0a0fa0

.field public static final rl_format:I = 0x7f0a0fa1

.field public static final rl_gen:I = 0x7f0a0fa2

.field public static final rl_go_purchase:I = 0x7f0a0fa3

.field public static final rl_go_purchase2:I = 0x7f0a0fa4

.field public static final rl_google_login:I = 0x7f0a0fa5

.field public static final rl_gp_guide_mark_bg:I = 0x7f0a0fa6

.field public static final rl_gp_purchase7_give_up:I = 0x7f0a0fa7

.field public static final rl_gp_purchase_give_up:I = 0x7f0a0fa8

.field public static final rl_guide:I = 0x7f0a0fa9

.field public static final rl_guide_cn_purchase_bottoms_buttons:I = 0x7f0a0faa

.field public static final rl_guide_gp_bottoms_buttons:I = 0x7f0a0fab

.field public static final rl_guide_root:I = 0x7f0a0fac

.field public static final rl_head:I = 0x7f0a0fad

.field public static final rl_header:I = 0x7f0a0fae

.field public static final rl_icon:I = 0x7f0a0faf

.field public static final rl_image_strategy:I = 0x7f0a0fb0

.field public static final rl_import_pdf_to_office:I = 0x7f0a0fb1

.field public static final rl_input_root:I = 0x7f0a0fb2

.field public static final rl_jigsaw_save:I = 0x7f0a0fb3

.field public static final rl_last_login:I = 0x7f0a0fb4

.field public static final rl_limited_time_offer:I = 0x7f0a0fb5

.field public static final rl_loading_item:I = 0x7f0a0fb6

.field public static final rl_login_main:I = 0x7f0a0fb7

.field public static final rl_login_main_google_login:I = 0x7f0a0fb8

.field public static final rl_login_main_other_login:I = 0x7f0a0fb9

.field public static final rl_login_opt_root:I = 0x7f0a0fba

.field public static final rl_main:I = 0x7f0a0fbb

.field public static final rl_main_empty_root:I = 0x7f0a0fbc

.field public static final rl_main_emptydoc:I = 0x7f0a0fbd

.field public static final rl_main_emptydoc_idcard:I = 0x7f0a0fbe

.field public static final rl_mobile_area_code:I = 0x7f0a0fbf

.field public static final rl_mobile_pwd_login_area_code:I = 0x7f0a0fc0

.field public static final rl_month_item_purchase:I = 0x7f0a0fc1

.field public static final rl_more_thumb:I = 0x7f0a0fc2

.field public static final rl_negative_premium_life_time_continue:I = 0x7f0a0fc3

.field public static final rl_no_redeemable_benefit:I = 0x7f0a0fc4

.field public static final rl_no_watermark_layout:I = 0x7f0a0fc5

.field public static final rl_ocr_rec_root:I = 0x7f0a0fc6

.field public static final rl_ocr_result_can_edit:I = 0x7f0a0fc7

.field public static final rl_ocr_result_cannot_edit:I = 0x7f0a0fc8

.field public static final rl_one_login_auth_title_bar:I = 0x7f0a0fc9

.field public static final rl_operation_discount:I = 0x7f0a0fca

.field public static final rl_page_2_free_trail_month_switch:I = 0x7f0a0fcb

.field public static final rl_page_item_root:I = 0x7f0a0fcc

.field public static final rl_pageitem_whole_pack:I = 0x7f0a0fcd

.field public static final rl_pdf:I = 0x7f0a0fce

.field public static final rl_pdf_editing_cs_qr_code_container:I = 0x7f0a0fcf

.field public static final rl_pdf_editing_guide_root:I = 0x7f0a0fd0

.field public static final rl_pdf_page_bottom:I = 0x7f0a0fd1

.field public static final rl_pdf_share_count_hint:I = 0x7f0a0fd2

.field public static final rl_pdf_watermark_header:I = 0x7f0a0fd3

.field public static final rl_photo_root:I = 0x7f0a0fd4

.field public static final rl_picture_thumb:I = 0x7f0a0fd5

.field public static final rl_pop_root:I = 0x7f0a0fd6

.field public static final rl_positive_premium_year_buy:I = 0x7f0a0fd7

.field public static final rl_ppt:I = 0x7f0a0fd8

.field public static final rl_purchase:I = 0x7f0a0fd9

.field public static final rl_purchase_small_screen:I = 0x7f0a0fda

.field public static final rl_recall_title:I = 0x7f0a0fdb

.field public static final rl_recommend_root:I = 0x7f0a0fdc

.field public static final rl_recover_signature_file:I = 0x7f0a0fdd

.field public static final rl_remove_icon:I = 0x7f0a0fde

.field public static final rl_reset_deviceid:I = 0x7f0a0fdf

.field public static final rl_reset_doc_version:I = 0x7f0a0fe0

.field public static final rl_reset_image_status:I = 0x7f0a0fe1

.field public static final rl_resort_merged_docs_item:I = 0x7f0a0fe2

.field public static final rl_root:I = 0x7f0a0fe3

.field public static final rl_root_loading:I = 0x7f0a0fe4

.field public static final rl_root_main:I = 0x7f0a0fe5

.field public static final rl_save_bottom:I = 0x7f0a0fe6

.field public static final rl_save_button:I = 0x7f0a0fe7

.field public static final rl_screenshot:I = 0x7f0a0fe8

.field public static final rl_search_content:I = 0x7f0a0fe9

.field public static final rl_shade:I = 0x7f0a0fea

.field public static final rl_share_dialog_recycler:I = 0x7f0a0feb

.field public static final rl_share_list:I = 0x7f0a0fec

.field public static final rl_sig_actionbar_right:I = 0x7f0a0fed

.field public static final rl_skip_container:I = 0x7f0a0fee

.field public static final rl_start_btn:I = 0x7f0a0fef

.field public static final rl_start_directly:I = 0x7f0a0ff0

.field public static final rl_status_mobile:I = 0x7f0a0ff1

.field public static final rl_title:I = 0x7f0a0ff2

.field public static final rl_title_bar:I = 0x7f0a0ff3

.field public static final rl_title_root:I = 0x7f0a0ff4

.field public static final rl_toolbar:I = 0x7f0a0ff5

.field public static final rl_top:I = 0x7f0a0ff6

.field public static final rl_top_menu:I = 0x7f0a0ff7

.field public static final rl_top_view:I = 0x7f0a0ff8

.field public static final rl_transfer:I = 0x7f0a0ff9

.field public static final rl_up:I = 0x7f0a0ffa

.field public static final rl_use_mobile:I = 0x7f0a0ffb

.field public static final rl_vip:I = 0x7f0a0ffc

.field public static final rl_web_custom_root:I = 0x7f0a0ffd

.field public static final rl_web_fail_root:I = 0x7f0a0ffe

.field public static final rl_web_root:I = 0x7f0a0fff

.field public static final rl_word:I = 0x7f0a1000

.field public static final rl_ys_item_purchase:I = 0x7f0a1001

.field public static final rootLayout:I = 0x7f0a1002

.field public static final root_capture_guide:I = 0x7f0a1003

.field public static final root_layout:I = 0x7f0a1004

.field public static final root_office_bottom:I = 0x7f0a1005

.field public static final root_recommend_create_dir:I = 0x7f0a1006

.field public static final root_rotate:I = 0x7f0a1007

.field public static final root_scroll:I = 0x7f0a1008

.field public static final root_tips:I = 0x7f0a1009

.field public static final root_user_guide_start_demo:I = 0x7f0a100a

.field public static final root_view:I = 0x7f0a100b

.field public static final root_view_new:I = 0x7f0a100c

.field public static final root_view_topic_set:I = 0x7f0a100d

.field public static final rotate_certificate_menu:I = 0x7f0a100e

.field public static final rotate_id_tips:I = 0x7f0a100f

.field public static final rotate_mode_tips:I = 0x7f0a1010

.field public static final rotate_root:I = 0x7f0a1011

.field public static final rounded:I = 0x7f0a1012

.field public static final row:I = 0x7f0a1013

.field public static final row_index_key:I = 0x7f0a1014

.field public static final row_reverse:I = 0x7f0a1015

.field public static final rtv_triangle:I = 0x7f0a1016

.field public static final rv:I = 0x7f0a1017

.field public static final rv_ad_container:I = 0x7f0a1018

.field public static final rv_app_list:I = 0x7f0a1019

.field public static final rv_authority_recycler:I = 0x7f0a101a

.field public static final rv_auto_send_email_recycler:I = 0x7f0a101b

.field public static final rv_bank_list:I = 0x7f0a101c

.field public static final rv_camera_entrance:I = 0x7f0a101d

.field public static final rv_category_list:I = 0x7f0a101e

.field public static final rv_certificate:I = 0x7f0a101f

.field public static final rv_certificate_detail:I = 0x7f0a1020

.field public static final rv_certificate_recycler_view:I = 0x7f0a1021

.field public static final rv_cfg:I = 0x7f0a1022

.field public static final rv_channel:I = 0x7f0a1023

.field public static final rv_check_recycler:I = 0x7f0a1024

.field public static final rv_chose:I = 0x7f0a1025

.field public static final rv_cloud_disk:I = 0x7f0a1026

.field public static final rv_contacts:I = 0x7f0a1027

.field public static final rv_content:I = 0x7f0a1028

.field public static final rv_debug:I = 0x7f0a1029

.field public static final rv_docs:I = 0x7f0a102a

.field public static final rv_document_more_new:I = 0x7f0a102b

.field public static final rv_drop_cnl_recycler:I = 0x7f0a102c

.field public static final rv_edit_more:I = 0x7f0a102d

.field public static final rv_empty_hint:I = 0x7f0a102e

.field public static final rv_enhance_mode:I = 0x7f0a102f

.field public static final rv_feed_back_list:I = 0x7f0a1030

.field public static final rv_feedback_pic:I = 0x7f0a1031

.field public static final rv_filter_recycler:I = 0x7f0a1032

.field public static final rv_folder_path:I = 0x7f0a1033

.field public static final rv_folder_type:I = 0x7f0a1034

.field public static final rv_folder_type_1_recommend:I = 0x7f0a1035

.field public static final rv_folder_type_2_study:I = 0x7f0a1036

.field public static final rv_folder_type_3_work:I = 0x7f0a1037

.field public static final rv_folder_type_4_life:I = 0x7f0a1038

.field public static final rv_function:I = 0x7f0a1039

.field public static final rv_header_import_entrance:I = 0x7f0a103a

.field public static final rv_history:I = 0x7f0a103b

.field public static final rv_horizontal:I = 0x7f0a103c

.field public static final rv_image_recycler:I = 0x7f0a103d

.field public static final rv_invoice:I = 0x7f0a103e

.field public static final rv_items:I = 0x7f0a103f

.field public static final rv_layout_vip:I = 0x7f0a1040

.field public static final rv_list:I = 0x7f0a1041

.field public static final rv_main:I = 0x7f0a1042

.field public static final rv_main_view:I = 0x7f0a1043

.field public static final rv_main_view_container:I = 0x7f0a1044

.field public static final rv_me_page_recycler:I = 0x7f0a1045

.field public static final rv_member_benefits:I = 0x7f0a1046

.field public static final rv_message_recycler:I = 0x7f0a1047

.field public static final rv_net_disk_recycler:I = 0x7f0a1048

.field public static final rv_o_vip_right:I = 0x7f0a1049

.field public static final rv_ocr_img:I = 0x7f0a104a

.field public static final rv_ocr_result_img:I = 0x7f0a104b

.field public static final rv_operations:I = 0x7f0a104c

.field public static final rv_pay_types:I = 0x7f0a104d

.field public static final rv_pay_way_recycler:I = 0x7f0a104e

.field public static final rv_pdf_content:I = 0x7f0a104f

.field public static final rv_pdf_editing_recycler_view:I = 0x7f0a1050

.field public static final rv_pdf_signature_content:I = 0x7f0a1051

.field public static final rv_pixel_vertical_container:I = 0x7f0a1052

.field public static final rv_polling_recycler:I = 0x7f0a1053

.field public static final rv_price_info_recycler:I = 0x7f0a1054

.field public static final rv_printer_recycler:I = 0x7f0a1055

.field public static final rv_property_recycler:I = 0x7f0a1056

.field public static final rv_purchase:I = 0x7f0a1057

.field public static final rv_purchase_type:I = 0x7f0a1058

.field public static final rv_recent:I = 0x7f0a1059

.field public static final rv_recycler:I = 0x7f0a105a

.field public static final rv_recycler_darkness:I = 0x7f0a105b

.field public static final rv_recycler_view:I = 0x7f0a105c

.field public static final rv_resort_doc_recycler:I = 0x7f0a105d

.field public static final rv_right:I = 0x7f0a105e

.field public static final rv_search_content:I = 0x7f0a105f

.field public static final rv_search_referral:I = 0x7f0a1060

.field public static final rv_search_result:I = 0x7f0a1061

.field public static final rv_settings_recycler:I = 0x7f0a1062

.field public static final rv_share_channel:I = 0x7f0a1063

.field public static final rv_share_dialog_recycler:I = 0x7f0a1064

.field public static final rv_share_link_channel:I = 0x7f0a1065

.field public static final rv_share_link_types:I = 0x7f0a1066

.field public static final rv_show_composite:I = 0x7f0a1067

.field public static final rv_show_origin:I = 0x7f0a1068

.field public static final rv_signature:I = 0x7f0a1069

.field public static final rv_subscription:I = 0x7f0a106a

.field public static final rv_tags:I = 0x7f0a106b

.field public static final rv_tags_flex:I = 0x7f0a106c

.field public static final rv_template_recycler_view:I = 0x7f0a106d

.field public static final rv_thumb:I = 0x7f0a106e

.field public static final rv_tool_page_recycler:I = 0x7f0a106f

.field public static final rv_topic_recycler_view:I = 0x7f0a1070

.field public static final rv_trial_renew_recycler:I = 0x7f0a1071

.field public static final rv_vertical:I = 0x7f0a1072

.field public static final rv_vip_desc:I = 0x7f0a1073

.field public static final rv_vip_gifts:I = 0x7f0a1074

.field public static final rv_vip_properties_container:I = 0x7f0a1075

.field public static final rv_vip_rights:I = 0x7f0a1076

.field public static final rv_watch_ad:I = 0x7f0a1077

.field public static final s_guide_bottom_line:I = 0x7f0a1078

.field public static final s_setting_right_txt_line_bottom_space:I = 0x7f0a1079

.field public static final s_workflow_top_image:I = 0x7f0a107a

.field public static final saveProgressBar:I = 0x7f0a107b

.field public static final save_non_transition_alpha:I = 0x7f0a107c

.field public static final save_overlay_view:I = 0x7f0a107d

.field public static final sawtooth:I = 0x7f0a107e

.field public static final sb_brightness:I = 0x7f0a107f

.field public static final sb_brush:I = 0x7f0a1080

.field public static final sb_code:I = 0x7f0a1081

.field public static final sb_contrast:I = 0x7f0a1082

.field public static final sb_detail:I = 0x7f0a1083

.field public static final sb_eraser:I = 0x7f0a1084

.field public static final sb_o_vip_scrollbar:I = 0x7f0a1085

.field public static final sb_paint:I = 0x7f0a1086

.field public static final sb_security_mark_alpha:I = 0x7f0a1087

.field public static final sb_security_mark_size:I = 0x7f0a1088

.field public static final sb_size:I = 0x7f0a1089

.field public static final sb_wx_share:I = 0x7f0a108a

.field public static final sc_enc_code:I = 0x7f0a108b

.field public static final sc_setting_more_auto_capture_switch:I = 0x7f0a108c

.field public static final sc_setting_more_auto_crop_switch:I = 0x7f0a108d

.field public static final sc_setting_more_grid_switch:I = 0x7f0a108e

.field public static final sc_setting_more_sensor_switch:I = 0x7f0a108f

.field public static final sc_setting_more_sound_switch:I = 0x7f0a1090

.field public static final sc_subscribe_check:I = 0x7f0a1091

.field public static final sc_switcher:I = 0x7f0a1092

.field public static final scale:I = 0x7f0a1093

.field public static final scaleIndicator:I = 0x7f0a1094

.field public static final scandone_reward:I = 0x7f0a1095

.field public static final screen:I = 0x7f0a1096

.field public static final scroll:I = 0x7f0a1097

.field public static final scrollBarView:I = 0x7f0a1098

.field public static final scrollIndicatorDown:I = 0x7f0a1099

.field public static final scrollIndicatorUp:I = 0x7f0a109a

.field public static final scrollView:I = 0x7f0a109b

.field public static final scrollView1:I = 0x7f0a109c

.field public static final scrollView_options:I = 0x7f0a109d

.field public static final scroll_ll_content:I = 0x7f0a109e

.field public static final scroll_protocol:I = 0x7f0a109f

.field public static final scroll_user_edit:I = 0x7f0a10a0

.field public static final scroll_view:I = 0x7f0a10a1

.field public static final scroll_view_payment:I = 0x7f0a10a2

.field public static final scrollable:I = 0x7f0a10a3

.field public static final scrollview:I = 0x7f0a10a4

.field public static final scv_wave_behind:I = 0x7f0a10a5

.field public static final scv_wave_front:I = 0x7f0a10a6

.field public static final search_badge:I = 0x7f0a10a7

.field public static final search_bar:I = 0x7f0a10a8

.field public static final search_bar_text_view:I = 0x7f0a10a9

.field public static final search_button:I = 0x7f0a10aa

.field public static final search_close_btn:I = 0x7f0a10ab

.field public static final search_edit_frame:I = 0x7f0a10ac

.field public static final search_go_btn:I = 0x7f0a10ad

.field public static final search_input_box:I = 0x7f0a10ae

.field public static final search_mag_icon:I = 0x7f0a10af

.field public static final search_plate:I = 0x7f0a10b0

.field public static final search_src_text:I = 0x7f0a10b1

.field public static final search_view_background:I = 0x7f0a10b2

.field public static final search_view_clear_button:I = 0x7f0a10b3

.field public static final search_view_content_container:I = 0x7f0a10b4

.field public static final search_view_divider:I = 0x7f0a10b5

.field public static final search_view_dummy_toolbar:I = 0x7f0a10b6

.field public static final search_view_edit_text:I = 0x7f0a10b7

.field public static final search_view_header_container:I = 0x7f0a10b8

.field public static final search_view_root:I = 0x7f0a10b9

.field public static final search_view_scrim:I = 0x7f0a10ba

.field public static final search_view_search_prefix:I = 0x7f0a10bb

.field public static final search_view_status_bar_spacer:I = 0x7f0a10bc

.field public static final search_view_toolbar:I = 0x7f0a10bd

.field public static final search_view_toolbar_container:I = 0x7f0a10be

.field public static final search_voice_btn:I = 0x7f0a10bf

.field public static final secondLinearLayout:I = 0x7f0a10c0

.field public static final sectioning_adapter_tag_key_view_viewholder:I = 0x7f0a10c1

.field public static final seekbar_adjust_size:I = 0x7f0a10c2

.field public static final select_dialog_listview:I = 0x7f0a10c3

.field public static final select_doc_page_frame:I = 0x7f0a10c4

.field public static final select_toolbar:I = 0x7f0a10c5

.field public static final selected:I = 0x7f0a10c6

.field public static final selection_type:I = 0x7f0a10c7

.field public static final send_leftLayout:I = 0x7f0a10c8

.field public static final sensorView:I = 0x7f0a10c9

.field public static final sep:I = 0x7f0a10ca

.field public static final sep_imagepage_bg_navibar:I = 0x7f0a10cb

.field public static final sep_imgpage_indexocr:I = 0x7f0a10cc

.field public static final sep_recent_other:I = 0x7f0a10cd

.field public static final shareIcon:I = 0x7f0a10ce

.field public static final share_type_gv:I = 0x7f0a10cf

.field public static final share_type_name:I = 0x7f0a10d0

.field public static final share_type_size:I = 0x7f0a10d1

.field public static final sharedValueSet:I = 0x7f0a10d2

.field public static final sharedValueUnset:I = 0x7f0a10d3

.field public static final shortcut:I = 0x7f0a10d4

.field public static final showCustom:I = 0x7f0a10d5

.field public static final showHome:I = 0x7f0a10d6

.field public static final showTitle:I = 0x7f0a10d7

.field public static final show_mrec_button:I = 0x7f0a10d8

.field public static final show_native_button:I = 0x7f0a10d9

.field public static final show_password_1:I = 0x7f0a10da

.field public static final shutter_bg_tips:I = 0x7f0a10db

.field public static final shutter_btns_panel:I = 0x7f0a10dc

.field public static final shutter_button:I = 0x7f0a10dd

.field public static final shutter_panel:I = 0x7f0a10de

.field public static final sign_in_btn_with_google:I = 0x7f0a10df

.field public static final signature_action_view:I = 0x7f0a10e0

.field public static final signature_shutter_button:I = 0x7f0a10e1

.field public static final signature_tab_view:I = 0x7f0a10e2

.field public static final signature_view:I = 0x7f0a10e3

.field public static final sin:I = 0x7f0a10e4

.field public static final siv_corner_image_g2:I = 0x7f0a10e5

.field public static final siv_top_banner:I = 0x7f0a10e6

.field public static final skipCollapsed:I = 0x7f0a10e7

.field public static final skipped:I = 0x7f0a10e8

.field public static final sl_start_using:I = 0x7f0a10e9

.field public static final slide:I = 0x7f0a10ea

.field public static final small:I = 0x7f0a10eb

.field public static final smart_erase_import:I = 0x7f0a10ec

.field public static final smart_erase_shutter_button:I = 0x7f0a10ed

.field public static final snackbar_action:I = 0x7f0a10ee

.field public static final snackbar_text:I = 0x7f0a10ef

.field public static final snap:I = 0x7f0a10f0

.field public static final snapMargins:I = 0x7f0a10f1

.field public static final software:I = 0x7f0a10f2

.field public static final south:I = 0x7f0a10f3

.field public static final sp_pos:I = 0x7f0a10f4

.field public static final sp_src:I = 0x7f0a10f5

.field public static final sp_type:I = 0x7f0a10f6

.field public static final space:I = 0x7f0a10f7

.field public static final space_around:I = 0x7f0a10f8

.field public static final space_arrow:I = 0x7f0a10f9

.field public static final space_banner:I = 0x7f0a10fa

.field public static final space_between:I = 0x7f0a10fb

.field public static final space_bg:I = 0x7f0a10fc

.field public static final space_bg_bottom:I = 0x7f0a10fd

.field public static final space_bottom_divider:I = 0x7f0a10fe

.field public static final space_center:I = 0x7f0a10ff

.field public static final space_color:I = 0x7f0a1100

.field public static final space_control:I = 0x7f0a1101

.field public static final space_copy_times:I = 0x7f0a1102

.field public static final space_dotted:I = 0x7f0a1103

.field public static final space_end:I = 0x7f0a1104

.field public static final space_evenly:I = 0x7f0a1105

.field public static final space_free_share:I = 0x7f0a1106

.field public static final space_me_page_card:I = 0x7f0a1107

.field public static final space_me_page_top_right:I = 0x7f0a1108

.field public static final space_shutter:I = 0x7f0a1109

.field public static final space_size:I = 0x7f0a110a

.field public static final space_start:I = 0x7f0a110b

.field public static final space_tag:I = 0x7f0a110c

.field public static final space_title:I = 0x7f0a110d

.field public static final space_top:I = 0x7f0a110e

.field public static final space_vip_card:I = 0x7f0a110f

.field public static final space_watermark:I = 0x7f0a1110

.field public static final spacer:I = 0x7f0a1111

.field public static final special_effects_controller_view_tag:I = 0x7f0a1112

.field public static final spherical_gl_surface_view:I = 0x7f0a1113

.field public static final spin_country:I = 0x7f0a1114

.field public static final spline:I = 0x7f0a1115

.field public static final split_action_bar:I = 0x7f0a1116

.field public static final spread:I = 0x7f0a1117

.field public static final spread_inside:I = 0x7f0a1118

.field public static final spring:I = 0x7f0a1119

.field public static final square:I = 0x7f0a111a

.field public static final src_atop:I = 0x7f0a111b

.field public static final src_in:I = 0x7f0a111c

.field public static final src_over:I = 0x7f0a111d

.field public static final standard:I = 0x7f0a111e

.field public static final start:I = 0x7f0a111f

.field public static final startHorizontal:I = 0x7f0a1120

.field public static final startToEnd:I = 0x7f0a1121

.field public static final startVertical:I = 0x7f0a1122

.field public static final start_barrier:I = 0x7f0a1123

.field public static final state:I = 0x7f0a1124

.field public static final staticLayout:I = 0x7f0a1125

.field public static final staticPostLayout:I = 0x7f0a1126

.field public static final statusDescTextView:I = 0x7f0a1127

.field public static final statusView:I = 0x7f0a1128

.field public static final statusViewBackground:I = 0x7f0a1129

.field public static final status_bar_latest_event_content:I = 0x7f0a112a

.field public static final status_textview:I = 0x7f0a112b

.field public static final status_view_background:I = 0x7f0a112c

.field public static final status_view_backup:I = 0x7f0a112d

.field public static final sti_all_page:I = 0x7f0a112e

.field public static final sti_cur_page:I = 0x7f0a112f

.field public static final sti_share_album:I = 0x7f0a1130

.field public static final sti_share_img:I = 0x7f0a1131

.field public static final sti_share_pdf:I = 0x7f0a1132

.field public static final sti_share_txt:I = 0x7f0a1133

.field public static final sti_share_word:I = 0x7f0a1134

.field public static final stop:I = 0x7f0a1135

.field public static final stretch:I = 0x7f0a1136

.field public static final stroke_size_panel:I = 0x7f0a1137

.field public static final stroke_size_seekbar:I = 0x7f0a1138

.field public static final stroke_size_seekbar_signature:I = 0x7f0a1139

.field public static final stub_book_splitter:I = 0x7f0a113a

.field public static final stub_book_splitter_optimizing:I = 0x7f0a113b

.field public static final stub_calibrateview:I = 0x7f0a113c

.field public static final stub_camera_hint:I = 0x7f0a113d

.field public static final stub_cap_wave:I = 0x7f0a113e

.field public static final stub_certificate_back_to_last:I = 0x7f0a113f

.field public static final stub_certificate_cn_zh:I = 0x7f0a1140

.field public static final stub_certificate_menu:I = 0x7f0a1141

.field public static final stub_certificate_menu_copy:I = 0x7f0a1142

.field public static final stub_certificate_menu_copy_new:I = 0x7f0a1143

.field public static final stub_certificate_other:I = 0x7f0a1144

.field public static final stub_certificate_us_en:I = 0x7f0a1145

.field public static final stub_doc_grid:I = 0x7f0a1146

.field public static final stub_doc_list:I = 0x7f0a1147

.field public static final stub_e_evidence_top_gps_dialog_vivo_market:I = 0x7f0a1148

.field public static final stub_frame_container:I = 0x7f0a1149

.field public static final stub_hint_empty_doc:I = 0x7f0a114a

.field public static final stub_hint_no_mathc_doc:I = 0x7f0a114b

.field public static final stub_saveprogressbar:I = 0x7f0a114c

.field public static final stub_screenshot:I = 0x7f0a114d

.field public static final stub_user_guide:I = 0x7f0a114e

.field public static final stub_user_guide_start_demo:I = 0x7f0a114f

.field public static final study:I = 0x7f0a1150

.field public static final style_container:I = 0x7f0a1151

.field public static final style_new_03:I = 0x7f0a1152

.field public static final subjectText:I = 0x7f0a1153

.field public static final submenuarrow:I = 0x7f0a1154

.field public static final submit_area:I = 0x7f0a1155

.field public static final supportScrollUp:I = 0x7f0a1156

.field public static final surface:I = 0x7f0a1157

.field public static final surface_view:I = 0x7f0a1158

.field public static final surfaceview:I = 0x7f0a1159

.field public static final sv_agreement:I = 0x7f0a115a

.field public static final sv_bottom_rule_desc:I = 0x7f0a115b

.field public static final sv_content:I = 0x7f0a115c

.field public static final sv_discount_purchase_v2_bottom_container:I = 0x7f0a115d

.field public static final sv_discount_purchase_v2_top_container:I = 0x7f0a115e

.field public static final sv_main_content:I = 0x7f0a115f

.field public static final switch_auto_backup:I = 0x7f0a1160

.field public static final switch_auto_open_website:I = 0x7f0a1161

.field public static final switch_cfg:I = 0x7f0a1162

.field public static final switch_compat:I = 0x7f0a1163

.field public static final switch_dotted_line:I = 0x7f0a1164

.field public static final switch_dotted_paper:I = 0x7f0a1165

.field public static final switch_excel:I = 0x7f0a1166

.field public static final switch_pdf:I = 0x7f0a1167

.field public static final switch_ppt:I = 0x7f0a1168

.field public static final switch_setting:I = 0x7f0a1169

.field public static final switch_to_single:I = 0x7f0a116a

.field public static final switch_use_mobile:I = 0x7f0a116b

.field public static final switch_word:I = 0x7f0a116c

.field public static final symbol1:I = 0x7f0a116d

.field public static final symbol2:I = 0x7f0a116e

.field public static final symbol3:I = 0x7f0a116f

.field public static final syncIcon:I = 0x7f0a1170

.field public static final sync_parent:I = 0x7f0a1171

.field public static final sync_state:I = 0x7f0a1172

.field public static final tabLayout:I = 0x7f0a1173

.field public static final tabMode:I = 0x7f0a1174

.field public static final tab_container:I = 0x7f0a1175

.field public static final tab_doc_list_type:I = 0x7f0a1176

.field public static final tab_doc_type:I = 0x7f0a1177

.field public static final tab_image_indicator:I = 0x7f0a1178

.field public static final tab_image_tv:I = 0x7f0a1179

.field public static final tab_layout:I = 0x7f0a117a

.field public static final tab_pdf_indicator:I = 0x7f0a117b

.field public static final tab_pdf_tv:I = 0x7f0a117c

.field public static final tab_ufp_fax:I = 0x7f0a117d

.field public static final tab_ufp_upload:I = 0x7f0a117e

.field public static final tableLayout:I = 0x7f0a117f

.field public static final tag_accessibility_actions:I = 0x7f0a1180

.field public static final tag_accessibility_clickable_spans:I = 0x7f0a1181

.field public static final tag_accessibility_heading:I = 0x7f0a1182

.field public static final tag_accessibility_pane_title:I = 0x7f0a1183

.field public static final tag_ad_id:I = 0x7f0a1184

.field public static final tag_doc_check:I = 0x7f0a1185

.field public static final tag_doc_radio:I = 0x7f0a1186

.field public static final tag_doc_tencent_id:I = 0x7f0a1187

.field public static final tag_feed_back_id:I = 0x7f0a1188

.field public static final tag_item_title:I = 0x7f0a1189

.field public static final tag_layout_check:I = 0x7f0a118a

.field public static final tag_layout_helper_bg:I = 0x7f0a118b

.field public static final tag_on_apply_window_listener:I = 0x7f0a118c

.field public static final tag_on_receive_content_listener:I = 0x7f0a118d

.field public static final tag_on_receive_content_mime_types:I = 0x7f0a118e

.field public static final tag_screen_reader_focusable:I = 0x7f0a118f

.field public static final tag_space:I = 0x7f0a1190

.field public static final tag_state_description:I = 0x7f0a1191

.field public static final tag_title:I = 0x7f0a1192

.field public static final tag_transition_group:I = 0x7f0a1193

.field public static final tag_unhandled_key_event_manager:I = 0x7f0a1194

.field public static final tag_unhandled_key_listeners:I = 0x7f0a1195

.field public static final tag_view:I = 0x7f0a1196

.field public static final tag_window_insets_animation_callback:I = 0x7f0a1197

.field public static final tags:I = 0x7f0a1198

.field public static final task_list:I = 0x7f0a1199

.field public static final task_name:I = 0x7f0a119a

.field public static final tb_header:I = 0x7f0a119b

.field public static final tb_menu:I = 0x7f0a119c

.field public static final text:I = 0x7f0a119d

.field public static final text1:I = 0x7f0a119e

.field public static final text2:I = 0x7f0a119f

.field public static final textEmailAddress:I = 0x7f0a11a0

.field public static final textEnd:I = 0x7f0a11a1

.field public static final textPassword:I = 0x7f0a11a2

.field public static final textSpacerNoButtons:I = 0x7f0a11a3

.field public static final textSpacerNoTitle:I = 0x7f0a11a4

.field public static final textStart:I = 0x7f0a11a5

.field public static final textTop:I = 0x7f0a11a6

.field public static final textUri:I = 0x7f0a11a7

.field public static final textView:I = 0x7f0a11a8

.field public static final textView1:I = 0x7f0a11a9

.field public static final textView_index:I = 0x7f0a11aa

.field public static final textView_page_note:I = 0x7f0a11ab

.field public static final textView_remain_length:I = 0x7f0a11ac

.field public static final textView_sns_result:I = 0x7f0a11ad

.field public static final textView_sns_title:I = 0x7f0a11ae

.field public static final textView_wechat_circle:I = 0x7f0a11af

.field public static final textView_wechat_friend:I = 0x7f0a11b0

.field public static final textWatcher:I = 0x7f0a11b1

.field public static final text_a4:I = 0x7f0a11b2

.field public static final text_bounds:I = 0x7f0a11b3

.field public static final text_decode_label:I = 0x7f0a11b4

.field public static final text_evernote:I = 0x7f0a11b5

.field public static final text_input_end_icon:I = 0x7f0a11b6

.field public static final text_input_error_icon:I = 0x7f0a11b7

.field public static final text_input_start_icon:I = 0x7f0a11b8

.field public static final text_note:I = 0x7f0a11b9

.field public static final text_skydrive:I = 0x7f0a11ba

.field public static final text_top:I = 0x7f0a11bb

.field public static final text_top_title:I = 0x7f0a11bc

.field public static final textinput_counter:I = 0x7f0a11bd

.field public static final textinput_error:I = 0x7f0a11be

.field public static final textinput_helper_text:I = 0x7f0a11bf

.field public static final textinput_placeholder:I = 0x7f0a11c0

.field public static final textinput_prefix_text:I = 0x7f0a11c1

.field public static final textinput_suffix_text:I = 0x7f0a11c2

.field public static final texture:I = 0x7f0a11c3

.field public static final texture_view:I = 0x7f0a11c4

.field public static final thing_proto:I = 0x7f0a11c5

.field public static final til_new_pwd_logined:I = 0x7f0a11c6

.field public static final til_old_pwd_logined:I = 0x7f0a11c7

.field public static final time:I = 0x7f0a11c8

.field public static final timePicker:I = 0x7f0a11c9

.field public static final tip_count_icon:I = 0x7f0a11ca

.field public static final tip_icon:I = 0x7f0a11cb

.field public static final tip_rl:I = 0x7f0a11cc

.field public static final tip_txt:I = 0x7f0a11cd

.field public static final tips:I = 0x7f0a11ce

.field public static final tips_desc:I = 0x7f0a11cf

.field public static final tips_title:I = 0x7f0a11d0

.field public static final title:I = 0x7f0a11d1

.field public static final titleDividerNoCustom:I = 0x7f0a11d2

.field public static final title_container:I = 0x7f0a11d3

.field public static final title_panel:I = 0x7f0a11d4

.field public static final title_space:I = 0x7f0a11d5

.field public static final title_template:I = 0x7f0a11d6

.field public static final title_textview:I = 0x7f0a11d7

.field public static final tl_enhance_type:I = 0x7f0a11d8

.field public static final tl_tabs:I = 0x7f0a11d9

.field public static final to_word:I = 0x7f0a11da

.field public static final to_word_container:I = 0x7f0a11db

.field public static final to_word_tips_display:I = 0x7f0a11dc

.field public static final toggle:I = 0x7f0a11dd

.field public static final tool_bar_container:I = 0x7f0a11de

.field public static final toolbar:I = 0x7f0a11df

.field public static final toolbar_bank_card_web_finder:I = 0x7f0a11e0

.field public static final toolbar_container:I = 0x7f0a11e1

.field public static final toolbar_doc_fragment:I = 0x7f0a11e2

.field public static final toolbar_me_price:I = 0x7f0a11e3

.field public static final toolbar_menu:I = 0x7f0a11e4

.field public static final toolbar_menu_container:I = 0x7f0a11e5

.field public static final toolbar_menu_edit:I = 0x7f0a11e6

.field public static final toolbar_menu_normal:I = 0x7f0a11e7

.field public static final toolbar_qr_code_history:I = 0x7f0a11e8

.field public static final toolbar_select:I = 0x7f0a11e9

.field public static final toolbar_tag_manage:I = 0x7f0a11ea

.field public static final toolbar_title:I = 0x7f0a11eb

.field public static final toolbar_title_container_layout:I = 0x7f0a11ec

.field public static final toolbar_title_layout:I = 0x7f0a11ed

.field public static final top:I = 0x7f0a11ee

.field public static final topPanel:I = 0x7f0a11ef

.field public static final top_action_bar:I = 0x7f0a11f0

.field public static final top_back_bar:I = 0x7f0a11f1

.field public static final top_cl:I = 0x7f0a11f2

.field public static final top_display:I = 0x7f0a11f3

.field public static final top_divider:I = 0x7f0a11f4

.field public static final top_image:I = 0x7f0a11f5

.field public static final topic_back:I = 0x7f0a11f6

.field public static final topic_import:I = 0x7f0a11f7

.field public static final topic_paper_thumb:I = 0x7f0a11f8

.field public static final topic_paper_thumb_num:I = 0x7f0a11f9

.field public static final topic_shutter_button:I = 0x7f0a11fa

.field public static final torch:I = 0x7f0a11fb

.field public static final touch_outside:I = 0x7f0a11fc

.field public static final transferring_progress:I = 0x7f0a11fd

.field public static final transitionToEnd:I = 0x7f0a11fe

.field public static final transitionToStart:I = 0x7f0a11ff

.field public static final transition_current_scene:I = 0x7f0a1200

.field public static final transition_header_translate:I = 0x7f0a1201

.field public static final transition_layout_save:I = 0x7f0a1202

.field public static final transition_main_doc:I = 0x7f0a1203

.field public static final transition_main_header_view:I = 0x7f0a1204

.field public static final transition_position:I = 0x7f0a1205

.field public static final transition_scene_layoutid_cache:I = 0x7f0a1206

.field public static final transition_transform:I = 0x7f0a1207

.field public static final transition_transform1:I = 0x7f0a1208

.field public static final transition_transform2:I = 0x7f0a1209

.field public static final translate_back:I = 0x7f0a120a

.field public static final translate_import:I = 0x7f0a120b

.field public static final translate_import_doc_file:I = 0x7f0a120c

.field public static final translate_shutter_button:I = 0x7f0a120d

.field public static final translate_thumb:I = 0x7f0a120e

.field public static final translate_thumb_num:I = 0x7f0a120f

.field public static final translate_toolbar:I = 0x7f0a1210

.field public static final triangle:I = 0x7f0a1211

.field public static final trim_bg_tips:I = 0x7f0a1212

.field public static final tt:I = 0x7f0a1213

.field public static final tt_id_area_rect_info:I = 0x7f0a1214

.field public static final tt_id_click_area_id:I = 0x7f0a1215

.field public static final tt_id_click_area_type:I = 0x7f0a1216

.field public static final tt_id_click_begin:I = 0x7f0a1217

.field public static final tt_id_click_end:I = 0x7f0a1218

.field public static final tt_id_click_tag:I = 0x7f0a1219

.field public static final tt_id_direction:I = 0x7f0a121a

.field public static final tt_id_is_video_picture:I = 0x7f0a121b

.field public static final tt_id_open_landing_page:I = 0x7f0a121c

.field public static final tt_id_ripple_bg:I = 0x7f0a121d

.field public static final tt_id_root_web_view:I = 0x7f0a121e

.field public static final tt_id_shine_width:I = 0x7f0a121f

.field public static final tt_id_width:I = 0x7f0a1220

.field public static final tv:I = 0x7f0a1221

.field public static final tv1:I = 0x7f0a1222

.field public static final tv2:I = 0x7f0a1223

.field public static final tv3:I = 0x7f0a1224

.field public static final tv4:I = 0x7f0a1225

.field public static final tv5:I = 0x7f0a1226

.field public static final tv_0:I = 0x7f0a1227

.field public static final tv_1:I = 0x7f0a1228

.field public static final tv_100_gb_cloud:I = 0x7f0a1229

.field public static final tv_1_1:I = 0x7f0a122a

.field public static final tv_2:I = 0x7f0a122b

.field public static final tv_2_1:I = 0x7f0a122c

.field public static final tv_2_bottom:I = 0x7f0a122d

.field public static final tv_2_bottom_1:I = 0x7f0a122e

.field public static final tv_2_bottom_2:I = 0x7f0a122f

.field public static final tv_3:I = 0x7f0a1230

.field public static final tv_3_1:I = 0x7f0a1231

.field public static final tv_4:I = 0x7f0a1232

.field public static final tv_4_1:I = 0x7f0a1233

.field public static final tv_4_2:I = 0x7f0a1234

.field public static final tv_5:I = 0x7f0a1235

.field public static final tv_6:I = 0x7f0a1236

.field public static final tv_7:I = 0x7f0a1237

.field public static final tv_8:I = 0x7f0a1238

.field public static final tv_a_key_login_account:I = 0x7f0a1239

.field public static final tv_a_key_login_email:I = 0x7f0a123a

.field public static final tv_a_key_login_error_msg:I = 0x7f0a123b

.field public static final tv_a_key_login_pwd_login:I = 0x7f0a123c

.field public static final tv_a_key_login_wechat:I = 0x7f0a123d

.field public static final tv_abstract:I = 0x7f0a123e

.field public static final tv_accept:I = 0x7f0a123f

.field public static final tv_account:I = 0x7f0a1240

.field public static final tv_account_des:I = 0x7f0a1241

.field public static final tv_account_disable:I = 0x7f0a1242

.field public static final tv_account_google_desc:I = 0x7f0a1243

.field public static final tv_account_info:I = 0x7f0a1244

.field public static final tv_account_name:I = 0x7f0a1245

.field public static final tv_account_warning:I = 0x7f0a1246

.field public static final tv_act:I = 0x7f0a1247

.field public static final tv_action:I = 0x7f0a1248

.field public static final tv_active_tips:I = 0x7f0a1249

.field public static final tv_ad:I = 0x7f0a124a

.field public static final tv_ad_desc:I = 0x7f0a124b

.field public static final tv_ad_dur:I = 0x7f0a124c

.field public static final tv_ad_policy:I = 0x7f0a124d

.field public static final tv_ad_pool:I = 0x7f0a124e

.field public static final tv_ad_tag:I = 0x7f0a124f

.field public static final tv_add:I = 0x7f0a1250

.field public static final tv_add_card:I = 0x7f0a1251

.field public static final tv_add_card_guide:I = 0x7f0a1252

.field public static final tv_add_cfg:I = 0x7f0a1253

.field public static final tv_add_contact_type_tips:I = 0x7f0a1254

.field public static final tv_add_count:I = 0x7f0a1255

.field public static final tv_add_desc:I = 0x7f0a1256

.field public static final tv_add_doc:I = 0x7f0a1257

.field public static final tv_add_mark:I = 0x7f0a1258

.field public static final tv_add_tag:I = 0x7f0a1259

.field public static final tv_address:I = 0x7f0a125a

.field public static final tv_after:I = 0x7f0a125b

.field public static final tv_age_des:I = 0x7f0a125c

.field public static final tv_agree:I = 0x7f0a125d

.field public static final tv_agree_buy:I = 0x7f0a125e

.field public static final tv_agree_protocol:I = 0x7f0a125f

.field public static final tv_agree_to_continue:I = 0x7f0a1260

.field public static final tv_agreement:I = 0x7f0a1261

.field public static final tv_agreement_explain_message:I = 0x7f0a1262

.field public static final tv_ai_des:I = 0x7f0a1263

.field public static final tv_ai_entrance:I = 0x7f0a1264

.field public static final tv_ai_title:I = 0x7f0a1265

.field public static final tv_alert:I = 0x7f0a1266

.field public static final tv_alias:I = 0x7f0a1267

.field public static final tv_all_docs:I = 0x7f0a1268

.field public static final tv_all_page:I = 0x7f0a1269

.field public static final tv_all_text:I = 0x7f0a126a

.field public static final tv_amazon:I = 0x7f0a126b

.field public static final tv_amount:I = 0x7f0a126c

.field public static final tv_amount_price_text:I = 0x7f0a126d

.field public static final tv_apis_gid_etc:I = 0x7f0a126e

.field public static final tv_app_icp:I = 0x7f0a126f

.field public static final tv_app_name:I = 0x7f0a1270

.field public static final tv_append_one_page:I = 0x7f0a1271

.field public static final tv_append_signer:I = 0x7f0a1272

.field public static final tv_apply_to_all:I = 0x7f0a1273

.field public static final tv_area_code_confirm_area_code:I = 0x7f0a1274

.field public static final tv_area_code_confirm_area_code_name:I = 0x7f0a1275

.field public static final tv_area_code_confirm_error_msg:I = 0x7f0a1276

.field public static final tv_attract_for_reward_message:I = 0x7f0a1277

.field public static final tv_attract_for_reward_ok:I = 0x7f0a1278

.field public static final tv_authority:I = 0x7f0a1279

.field public static final tv_auto_capture_tips:I = 0x7f0a127a

.field public static final tv_auto_open_website:I = 0x7f0a127b

.field public static final tv_back:I = 0x7f0a127c

.field public static final tv_back_main:I = 0x7f0a127d

.field public static final tv_back_to_main:I = 0x7f0a127e

.field public static final tv_back_up:I = 0x7f0a127f

.field public static final tv_backup:I = 0x7f0a1280

.field public static final tv_backup_card_bag:I = 0x7f0a1281

.field public static final tv_backup_des:I = 0x7f0a1282

.field public static final tv_backup_net_status:I = 0x7f0a1283

.field public static final tv_backup_title:I = 0x7f0a1284

.field public static final tv_backup_type_subtitle:I = 0x7f0a1285

.field public static final tv_backup_type_title:I = 0x7f0a1286

.field public static final tv_baidu:I = 0x7f0a1287

.field public static final tv_bank_date:I = 0x7f0a1288

.field public static final tv_bank_expand:I = 0x7f0a1289

.field public static final tv_bank_income:I = 0x7f0a128a

.field public static final tv_bank_journal_check:I = 0x7f0a128b

.field public static final tv_bank_title:I = 0x7f0a128c

.field public static final tv_banner_subtitle:I = 0x7f0a128d

.field public static final tv_banner_title:I = 0x7f0a128e

.field public static final tv_bar_line:I = 0x7f0a128f

.field public static final tv_base:I = 0x7f0a1290

.field public static final tv_batch:I = 0x7f0a1291

.field public static final tv_batch_ocr_export:I = 0x7f0a1292

.field public static final tv_batch_ocr_save:I = 0x7f0a1293

.field public static final tv_before:I = 0x7f0a1294

.field public static final tv_bill:I = 0x7f0a1295

.field public static final tv_bill_alipay:I = 0x7f0a1296

.field public static final tv_bind_exist_account:I = 0x7f0a1297

.field public static final tv_bind_fail_message:I = 0x7f0a1298

.field public static final tv_bind_intro:I = 0x7f0a1299

.field public static final tv_bluetooth:I = 0x7f0a129a

.field public static final tv_book_double_page:I = 0x7f0a129b

.field public static final tv_book_rotate_tips:I = 0x7f0a129c

.field public static final tv_book_single_page:I = 0x7f0a129d

.field public static final tv_border:I = 0x7f0a129e

.field public static final tv_bottom:I = 0x7f0a129f

.field public static final tv_bottom_annotation:I = 0x7f0a12a0

.field public static final tv_bottom_left:I = 0x7f0a12a1

.field public static final tv_bottom_right:I = 0x7f0a12a2

.field public static final tv_bottom_rule_desc:I = 0x7f0a12a3

.field public static final tv_bottom_tip:I = 0x7f0a12a4

.field public static final tv_bottom_tips:I = 0x7f0a12a5

.field public static final tv_bottom_tips_0:I = 0x7f0a12a6

.field public static final tv_bottom_tips_title:I = 0x7f0a12a7

.field public static final tv_bottom_title:I = 0x7f0a12a8

.field public static final tv_brightness:I = 0x7f0a12a9

.field public static final tv_brightness_value:I = 0x7f0a12aa

.field public static final tv_brush:I = 0x7f0a12ab

.field public static final tv_btn:I = 0x7f0a12ac

.field public static final tv_btn_click:I = 0x7f0a12ad

.field public static final tv_btn_click_upgrade:I = 0x7f0a12ae

.field public static final tv_btn_feedback:I = 0x7f0a12af

.field public static final tv_btn_get:I = 0x7f0a12b0

.field public static final tv_btn_open_dir:I = 0x7f0a12b1

.field public static final tv_bubble_hint:I = 0x7f0a12b2

.field public static final tv_business_card:I = 0x7f0a12b3

.field public static final tv_buy:I = 0x7f0a12b4

.field public static final tv_buy_1:I = 0x7f0a12b5

.field public static final tv_buy_10:I = 0x7f0a12b6

.field public static final tv_buy_2:I = 0x7f0a12b7

.field public static final tv_buy_points:I = 0x7f0a12b8

.field public static final tv_buy_title:I = 0x7f0a12b9

.field public static final tv_cam_exam_des_1:I = 0x7f0a12ba

.field public static final tv_cam_exam_des_2:I = 0x7f0a12bb

.field public static final tv_cam_exam_name:I = 0x7f0a12bc

.field public static final tv_can_edit_ocr:I = 0x7f0a12bd

.field public static final tv_cancel:I = 0x7f0a12be

.field public static final tv_cancel_account:I = 0x7f0a12bf

.field public static final tv_cancel_account_des:I = 0x7f0a12c0

.field public static final tv_cancel_all:I = 0x7f0a12c1

.field public static final tv_cancel_auth:I = 0x7f0a12c2

.field public static final tv_cancel_convert:I = 0x7f0a12c3

.field public static final tv_cancel_move:I = 0x7f0a12c4

.field public static final tv_cancel_purchase:I = 0x7f0a12c5

.field public static final tv_cap_new_user_guide_desc:I = 0x7f0a12c6

.field public static final tv_cap_new_user_guide_start_demo:I = 0x7f0a12c7

.field public static final tv_capture:I = 0x7f0a12c8

.field public static final tv_capture_add:I = 0x7f0a12c9

.field public static final tv_capture_guide_normal:I = 0x7f0a12ca

.field public static final tv_capture_model:I = 0x7f0a12cb

.field public static final tv_capture_signmode_tips:I = 0x7f0a12cc

.field public static final tv_card_des:I = 0x7f0a12cd

.field public static final tv_card_name:I = 0x7f0a12ce

.field public static final tv_card_number:I = 0x7f0a12cf

.field public static final tv_card_owner:I = 0x7f0a12d0

.field public static final tv_card_title:I = 0x7f0a12d1

.field public static final tv_card_type:I = 0x7f0a12d2

.field public static final tv_certificate:I = 0x7f0a12d3

.field public static final tv_certificate_detail_generate_copy:I = 0x7f0a12d4

.field public static final tv_certificate_detail_head_certi_hoder_name:I = 0x7f0a12d5

.field public static final tv_certificate_detail_head_certi_name:I = 0x7f0a12d6

.field public static final tv_certificate_detail_head_certi_no:I = 0x7f0a12d7

.field public static final tv_certificate_detail_head_certi_no_name:I = 0x7f0a12d8

.field public static final tv_certificate_detail_head_cover_start_ocr:I = 0x7f0a12d9

.field public static final tv_ch_trim_switch:I = 0x7f0a12da

.field public static final tv_change_account_account:I = 0x7f0a12db

.field public static final tv_change_account_account_type:I = 0x7f0a12dc

.field public static final tv_change_account_bottom_desc:I = 0x7f0a12dd

.field public static final tv_change_account_change_account:I = 0x7f0a12de

.field public static final tv_change_account_modify_nickname:I = 0x7f0a12df

.field public static final tv_change_camera_setting:I = 0x7f0a12e0

.field public static final tv_change_existed_account_contact_us:I = 0x7f0a12e1

.field public static final tv_change_existed_account_msg:I = 0x7f0a12e2

.field public static final tv_change_existed_account_try_later:I = 0x7f0a12e3

.field public static final tv_change_image:I = 0x7f0a12e4

.field public static final tv_change_lang:I = 0x7f0a12e5

.field public static final tv_change_locale:I = 0x7f0a12e6

.field public static final tv_change_login_mode:I = 0x7f0a12e7

.field public static final tv_change_subscription:I = 0x7f0a12e8

.field public static final tv_char_count:I = 0x7f0a12e9

.field public static final tv_charge:I = 0x7f0a12ea

.field public static final tv_charge_count:I = 0x7f0a12eb

.field public static final tv_charge_title:I = 0x7f0a12ec

.field public static final tv_check:I = 0x7f0a12ed

.field public static final tv_check_now:I = 0x7f0a12ee

.field public static final tv_choose_country_code:I = 0x7f0a12ef

.field public static final tv_chose_img:I = 0x7f0a12f0

.field public static final tv_chose_index:I = 0x7f0a12f1

.field public static final tv_claim_gifts:I = 0x7f0a12f2

.field public static final tv_clarify_content_title:I = 0x7f0a12f3

.field public static final tv_clear:I = 0x7f0a12f4

.field public static final tv_clear_mark:I = 0x7f0a12f5

.field public static final tv_clear_watermark_tip:I = 0x7f0a12f6

.field public static final tv_click_clean:I = 0x7f0a12f7

.field public static final tv_click_ocr:I = 0x7f0a12f8

.field public static final tv_click_ocr_recognize:I = 0x7f0a12f9

.field public static final tv_click_to_recognize:I = 0x7f0a12fa

.field public static final tv_clock_title:I = 0x7f0a12fb

.field public static final tv_close:I = 0x7f0a12fc

.field public static final tv_cloud:I = 0x7f0a12fd

.field public static final tv_cloud_activity:I = 0x7f0a12fe

.field public static final tv_cloud_disk_folder_name:I = 0x7f0a12ff

.field public static final tv_cloud_disk_src:I = 0x7f0a1300

.field public static final tv_cloud_expand:I = 0x7f0a1301

.field public static final tv_cloud_login:I = 0x7f0a1302

.field public static final tv_cloud_remain:I = 0x7f0a1303

.field public static final tv_cloud_subtitle:I = 0x7f0a1304

.field public static final tv_cloud_switch_head_subtitle:I = 0x7f0a1305

.field public static final tv_cloud_switch_head_title:I = 0x7f0a1306

.field public static final tv_cloud_switch_only_wifi:I = 0x7f0a1307

.field public static final tv_cloud_switch_wifi_mobile:I = 0x7f0a1308

.field public static final tv_code_desc:I = 0x7f0a1309

.field public static final tv_code_title:I = 0x7f0a130a

.field public static final tv_code_type:I = 0x7f0a130b

.field public static final tv_collect:I = 0x7f0a130c

.field public static final tv_color:I = 0x7f0a130d

.field public static final tv_combine_paper_property_title:I = 0x7f0a130e

.field public static final tv_comment:I = 0x7f0a130f

.field public static final tv_commit:I = 0x7f0a1310

.field public static final tv_commit_account_cancel:I = 0x7f0a1311

.field public static final tv_compare_img:I = 0x7f0a1312

.field public static final tv_complete:I = 0x7f0a1313

.field public static final tv_complete_des:I = 0x7f0a1314

.field public static final tv_complete_tip:I = 0x7f0a1315

.field public static final tv_composite:I = 0x7f0a1316

.field public static final tv_config:I = 0x7f0a1317

.field public static final tv_config_name:I = 0x7f0a1318

.field public static final tv_config_net:I = 0x7f0a1319

.field public static final tv_confirm:I = 0x7f0a131a

.field public static final tv_congratulation:I = 0x7f0a131b

.field public static final tv_connec:I = 0x7f0a131c

.field public static final tv_connect_des:I = 0x7f0a131d

.field public static final tv_connect_name:I = 0x7f0a131e

.field public static final tv_contact_us:I = 0x7f0a131f

.field public static final tv_content:I = 0x7f0a1320

.field public static final tv_content_en:I = 0x7f0a1321

.field public static final tv_content_fifth:I = 0x7f0a1322

.field public static final tv_content_first:I = 0x7f0a1323

.field public static final tv_content_fourth:I = 0x7f0a1324

.field public static final tv_content_label:I = 0x7f0a1325

.field public static final tv_content_second:I = 0x7f0a1326

.field public static final tv_content_sixth:I = 0x7f0a1327

.field public static final tv_content_subtitle:I = 0x7f0a1328

.field public static final tv_content_third:I = 0x7f0a1329

.field public static final tv_content_title:I = 0x7f0a132a

.field public static final tv_continue:I = 0x7f0a132b

.field public static final tv_continue_add:I = 0x7f0a132c

.field public static final tv_continue_below:I = 0x7f0a132d

.field public static final tv_continue_tip:I = 0x7f0a132e

.field public static final tv_continue_top:I = 0x7f0a132f

.field public static final tv_contracts_link:I = 0x7f0a1330

.field public static final tv_contrast:I = 0x7f0a1331

.field public static final tv_contrast_value:I = 0x7f0a1332

.field public static final tv_convert:I = 0x7f0a1333

.field public static final tv_convert_background:I = 0x7f0a1334

.field public static final tv_convert_des:I = 0x7f0a1335

.field public static final tv_convert_result_des:I = 0x7f0a1336

.field public static final tv_convert_result_title:I = 0x7f0a1337

.field public static final tv_copied_tip:I = 0x7f0a1338

.field public static final tv_copy:I = 0x7f0a1339

.field public static final tv_copy_1:I = 0x7f0a133a

.field public static final tv_copy_2:I = 0x7f0a133b

.field public static final tv_copy_3:I = 0x7f0a133c

.field public static final tv_copy_only:I = 0x7f0a133d

.field public static final tv_copy_right:I = 0x7f0a133e

.field public static final tv_copy_times:I = 0x7f0a133f

.field public static final tv_count:I = 0x7f0a1340

.field public static final tv_count_bottom_intro:I = 0x7f0a1341

.field public static final tv_count_bottom_num:I = 0x7f0a1342

.field public static final tv_count_title:I = 0x7f0a1343

.field public static final tv_country:I = 0x7f0a1344

.field public static final tv_coupon_local_purchase:I = 0x7f0a1345

.field public static final tv_cover_num:I = 0x7f0a1346

.field public static final tv_crash_hua_wei:I = 0x7f0a1347

.field public static final tv_create:I = 0x7f0a1348

.field public static final tv_create_folder:I = 0x7f0a1349

.field public static final tv_create_time:I = 0x7f0a134a

.field public static final tv_cs_protocols_agree:I = 0x7f0a134b

.field public static final tv_cs_protocols_content:I = 0x7f0a134c

.field public static final tv_cs_protocols_content_1:I = 0x7f0a134d

.field public static final tv_cs_protocols_content_3:I = 0x7f0a134e

.field public static final tv_cs_protocols_content_4:I = 0x7f0a134f

.field public static final tv_cs_protocols_content_5:I = 0x7f0a1350

.field public static final tv_cs_protocols_dis_agree:I = 0x7f0a1351

.field public static final tv_cs_protocols_dis_agree_but_read:I = 0x7f0a1352

.field public static final tv_cs_protocols_not_agree:I = 0x7f0a1353

.field public static final tv_cs_user_mark:I = 0x7f0a1354

.field public static final tv_cur_point:I = 0x7f0a1355

.field public static final tv_current_page:I = 0x7f0a1356

.field public static final tv_current_time:I = 0x7f0a1357

.field public static final tv_cut:I = 0x7f0a1358

.field public static final tv_data_show_export_ac:I = 0x7f0a1359

.field public static final tv_data_show_export_view:I = 0x7f0a135a

.field public static final tv_data_show_ori_content:I = 0x7f0a135b

.field public static final tv_data_show_ori_lang:I = 0x7f0a135c

.field public static final tv_data_show_ori_tag:I = 0x7f0a135d

.field public static final tv_data_show_trans_content:I = 0x7f0a135e

.field public static final tv_data_show_trans_lang:I = 0x7f0a135f

.field public static final tv_data_show_trans_tag:I = 0x7f0a1360

.field public static final tv_date:I = 0x7f0a1361

.field public static final tv_date_time:I = 0x7f0a1362

.field public static final tv_de_moire:I = 0x7f0a1363

.field public static final tv_deadline:I = 0x7f0a1364

.field public static final tv_debug:I = 0x7f0a1365

.field public static final tv_debug_engine:I = 0x7f0a1366

.field public static final tv_dec:I = 0x7f0a1367

.field public static final tv_del_mark:I = 0x7f0a1368

.field public static final tv_delete:I = 0x7f0a1369

.field public static final tv_delete_count:I = 0x7f0a136a

.field public static final tv_density_title:I = 0x7f0a136b

.field public static final tv_density_value:I = 0x7f0a136c

.field public static final tv_des:I = 0x7f0a136d

.field public static final tv_des_1:I = 0x7f0a136e

.field public static final tv_des_enterprise_mall:I = 0x7f0a136f

.field public static final tv_des_left:I = 0x7f0a1370

.field public static final tv_des_right:I = 0x7f0a1371

.field public static final tv_desc:I = 0x7f0a1372

.field public static final tv_desc01:I = 0x7f0a1373

.field public static final tv_desc02:I = 0x7f0a1374

.field public static final tv_desc1:I = 0x7f0a1375

.field public static final tv_desc2:I = 0x7f0a1376

.field public static final tv_desc_title:I = 0x7f0a1377

.field public static final tv_desc_topic:I = 0x7f0a1378

.field public static final tv_description:I = 0x7f0a1379

.field public static final tv_description_bottom:I = 0x7f0a137a

.field public static final tv_deselect_current:I = 0x7f0a137b

.field public static final tv_detail:I = 0x7f0a137c

.field public static final tv_detail_value:I = 0x7f0a137d

.field public static final tv_detected_des:I = 0x7f0a137e

.field public static final tv_device_id:I = 0x7f0a137f

.field public static final tv_device_number:I = 0x7f0a1380

.field public static final tv_dialog_enable:I = 0x7f0a1381

.field public static final tv_dialog_result_cancel:I = 0x7f0a1382

.field public static final tv_dialog_result_description:I = 0x7f0a1383

.field public static final tv_dialog_result_know:I = 0x7f0a1384

.field public static final tv_dialog_result_merge:I = 0x7f0a1385

.field public static final tv_dialog_result_subtitle:I = 0x7f0a1386

.field public static final tv_dialog_result_title:I = 0x7f0a1387

.field public static final tv_dialog_title:I = 0x7f0a1388

.field public static final tv_dialog_to_retain_buy_now:I = 0x7f0a1389

.field public static final tv_dialog_to_retain_common_continue:I = 0x7f0a138a

.field public static final tv_dialog_to_retain_common_leave:I = 0x7f0a138b

.field public static final tv_dialog_to_retain_common_top_title:I = 0x7f0a138c

.field public static final tv_dialog_to_retain_common_top_title_off:I = 0x7f0a138d

.field public static final tv_dialog_to_retain_no_thanks:I = 0x7f0a138e

.field public static final tv_dialog_to_retain_top_end_time:I = 0x7f0a138f

.field public static final tv_dialog_to_retain_top_price_gray:I = 0x7f0a1390

.field public static final tv_dialog_to_retain_top_ticket_dec:I = 0x7f0a1391

.field public static final tv_dialog_to_retain_top_ticket_off:I = 0x7f0a1392

.field public static final tv_dialog_to_retain_top_ticket_price:I = 0x7f0a1393

.field public static final tv_dialog_to_retain_top_ticket_price_bg:I = 0x7f0a1394

.field public static final tv_dialog_to_retain_top_ticket_price_right:I = 0x7f0a1395

.field public static final tv_dialog_to_retain_top_title:I = 0x7f0a1396

.field public static final tv_dialog_to_retain_top_title_off:I = 0x7f0a1397

.field public static final tv_dialog_unable:I = 0x7f0a1398

.field public static final tv_diff_price:I = 0x7f0a1399

.field public static final tv_dir_path:I = 0x7f0a139a

.field public static final tv_dis_agree:I = 0x7f0a139b

.field public static final tv_disagree:I = 0x7f0a139c

.field public static final tv_disconnect:I = 0x7f0a139d

.field public static final tv_disconnect_0:I = 0x7f0a139e

.field public static final tv_discount:I = 0x7f0a139f

.field public static final tv_discount_desc:I = 0x7f0a13a0

.field public static final tv_discount_got_it:I = 0x7f0a13a1

.field public static final tv_discount_label:I = 0x7f0a13a2

.field public static final tv_discount_price_arrow:I = 0x7f0a13a3

.field public static final tv_discount_price_calculate:I = 0x7f0a13a4

.field public static final tv_discount_price_top:I = 0x7f0a13a5

.field public static final tv_discount_purchase_v2_continue:I = 0x7f0a13a6

.field public static final tv_discount_purchase_v2_continue_dec:I = 0x7f0a13a7

.field public static final tv_discount_purchase_v2_current_month_price:I = 0x7f0a13a8

.field public static final tv_discount_purchase_v2_current_month_price_subscribe:I = 0x7f0a13a9

.field public static final tv_discount_purchase_v2_current_price_name:I = 0x7f0a13aa

.field public static final tv_discount_purchase_v2_discount:I = 0x7f0a13ab

.field public static final tv_discount_purchase_v2_document:I = 0x7f0a13ac

.field public static final tv_discount_purchase_v2_month_subscribe_missed_offer:I = 0x7f0a13ad

.field public static final tv_discount_purchase_v2_month_subscribe_next_24_hours:I = 0x7f0a13ae

.field public static final tv_discount_purchase_v2_origin_month_subscribe:I = 0x7f0a13af

.field public static final tv_discount_purchase_v2_origin_year_price:I = 0x7f0a13b0

.field public static final tv_discount_purchase_v2_pdf:I = 0x7f0a13b1

.field public static final tv_discount_purchase_v2_subtitle01:I = 0x7f0a13b2

.field public static final tv_discount_purchase_v2_title:I = 0x7f0a13b3

.field public static final tv_discount_purchase_v2_watermark:I = 0x7f0a13b4

.field public static final tv_discount_purchase_v2_year_price_missed_offer:I = 0x7f0a13b5

.field public static final tv_discount_purchase_v2_year_price_next_24_hours:I = 0x7f0a13b6

.field public static final tv_discount_subtitle:I = 0x7f0a13b7

.field public static final tv_discount_title:I = 0x7f0a13b8

.field public static final tv_discount_year_desc:I = 0x7f0a13b9

.field public static final tv_doc_name:I = 0x7f0a13ba

.field public static final tv_doc_num:I = 0x7f0a13bb

.field public static final tv_doc_path:I = 0x7f0a13bc

.field public static final tv_doc_size:I = 0x7f0a13bd

.field public static final tv_doc_state:I = 0x7f0a13be

.field public static final tv_doc_timestamp:I = 0x7f0a13bf

.field public static final tv_doc_title:I = 0x7f0a13c0

.field public static final tv_docitem_tag_label:I = 0x7f0a13c1

.field public static final tv_docitem_tag_no_label:I = 0x7f0a13c2

.field public static final tv_docitem_tag_num:I = 0x7f0a13c3

.field public static final tv_document_more_king_kong:I = 0x7f0a13c4

.field public static final tv_document_title:I = 0x7f0a13c5

.field public static final tv_done:I = 0x7f0a13c6

.field public static final tv_download:I = 0x7f0a13c7

.field public static final tv_download_app:I = 0x7f0a13c8

.field public static final tv_draft_color_item:I = 0x7f0a13c9

.field public static final tv_driver_certificate:I = 0x7f0a13ca

.field public static final tv_driver_cn_zh:I = 0x7f0a13cb

.field public static final tv_drop_cnl_close_new:I = 0x7f0a13cc

.field public static final tv_dynamic_feature:I = 0x7f0a13cd

.field public static final tv_e_evidence_bottom_tips:I = 0x7f0a13ce

.field public static final tv_e_evidence_bottom_tips_new:I = 0x7f0a13cf

.field public static final tv_e_evidence_known_usage:I = 0x7f0a13d0

.field public static final tv_e_evidence_message_explain:I = 0x7f0a13d1

.field public static final tv_e_evidence_start_immediately:I = 0x7f0a13d2

.field public static final tv_edit:I = 0x7f0a13d3

.field public static final tv_edit_dir_title:I = 0x7f0a13d4

.field public static final tv_edit_doc_title:I = 0x7f0a13d5

.field public static final tv_edit_more:I = 0x7f0a13d6

.field public static final tv_edit_more_item:I = 0x7f0a13d7

.field public static final tv_edit_pic:I = 0x7f0a13d8

.field public static final tv_edit_title:I = 0x7f0a13d9

.field public static final tv_edu_ad:I = 0x7f0a13da

.field public static final tv_edu_des:I = 0x7f0a13db

.field public static final tv_edu_des_title:I = 0x7f0a13dc

.field public static final tv_ee_action:I = 0x7f0a13dd

.field public static final tv_ee_sub_title:I = 0x7f0a13de

.field public static final tv_ee_title:I = 0x7f0a13df

.field public static final tv_email_forget_pwd:I = 0x7f0a13e0

.field public static final tv_email_input:I = 0x7f0a13e1

.field public static final tv_email_login:I = 0x7f0a13e2

.field public static final tv_email_login_account:I = 0x7f0a13e3

.field public static final tv_email_login_email:I = 0x7f0a13e4

.field public static final tv_email_login_error_msg:I = 0x7f0a13e5

.field public static final tv_email_login_forget_password:I = 0x7f0a13e6

.field public static final tv_email_login_title:I = 0x7f0a13e7

.field public static final tv_email_pwd:I = 0x7f0a13e8

.field public static final tv_email_pwd_error:I = 0x7f0a13e9

.field public static final tv_email_pwd_other_login_ways:I = 0x7f0a13ea

.field public static final tv_email_pwd_title:I = 0x7f0a13eb

.field public static final tv_email_register:I = 0x7f0a13ec

.field public static final tv_email_register_title:I = 0x7f0a13ed

.field public static final tv_empty:I = 0x7f0a13ee

.field public static final tv_empty_add:I = 0x7f0a13ef

.field public static final tv_empty_hint:I = 0x7f0a13f0

.field public static final tv_empty_view:I = 0x7f0a13f1

.field public static final tv_en_name:I = 0x7f0a13f2

.field public static final tv_enable_invoice:I = 0x7f0a13f3

.field public static final tv_enc_code_tips:I = 0x7f0a13f4

.field public static final tv_enhance_fail_des:I = 0x7f0a13f5

.field public static final tv_enhance_mode:I = 0x7f0a13f6

.field public static final tv_ensure:I = 0x7f0a13f7

.field public static final tv_error:I = 0x7f0a13f8

.field public static final tv_error_msg:I = 0x7f0a13f9

.field public static final tv_error_tips:I = 0x7f0a13fa

.field public static final tv_esign_endtime:I = 0x7f0a13fb

.field public static final tv_esign_endtime_value:I = 0x7f0a13fc

.field public static final tv_esign_mark:I = 0x7f0a13fd

.field public static final tv_esign_owner:I = 0x7f0a13fe

.field public static final tv_esign_owner_value:I = 0x7f0a13ff

.field public static final tv_esign_siner_list_title:I = 0x7f0a1400

.field public static final tv_esign_starttime:I = 0x7f0a1401

.field public static final tv_esign_starttime_value:I = 0x7f0a1402

.field public static final tv_esign_status:I = 0x7f0a1403

.field public static final tv_esign_status_value:I = 0x7f0a1404

.field public static final tv_example:I = 0x7f0a1405

.field public static final tv_excel:I = 0x7f0a1406

.field public static final tv_excel_bottom_prompt:I = 0x7f0a1407

.field public static final tv_excel_cancel:I = 0x7f0a1408

.field public static final tv_excel_capture:I = 0x7f0a1409

.field public static final tv_excel_go_capture:I = 0x7f0a140a

.field public static final tv_excel_multi_mode:I = 0x7f0a140b

.field public static final tv_excel_ok:I = 0x7f0a140c

.field public static final tv_excel_rec_see_example:I = 0x7f0a140d

.field public static final tv_excel_retake:I = 0x7f0a140e

.field public static final tv_excel_single_mode:I = 0x7f0a140f

.field public static final tv_exit:I = 0x7f0a1410

.field public static final tv_expenses:I = 0x7f0a1411

.field public static final tv_experience:I = 0x7f0a1412

.field public static final tv_experience_free:I = 0x7f0a1413

.field public static final tv_experience_tips:I = 0x7f0a1414

.field public static final tv_expire_time:I = 0x7f0a1415

.field public static final tv_expire_title:I = 0x7f0a1416

.field public static final tv_explore_text:I = 0x7f0a1417

.field public static final tv_export:I = 0x7f0a1418

.field public static final tv_export_bank_journal:I = 0x7f0a1419

.field public static final tv_export_invoice:I = 0x7f0a141a

.field public static final tv_export_title:I = 0x7f0a141b

.field public static final tv_export_word_or_excel:I = 0x7f0a141c

.field public static final tv_extra_months:I = 0x7f0a141d

.field public static final tv_extract_text_subtitle:I = 0x7f0a141e

.field public static final tv_extract_text_title:I = 0x7f0a141f

.field public static final tv_fail_tip:I = 0x7f0a1420

.field public static final tv_fail_title:I = 0x7f0a1421

.field public static final tv_faoverleManager:I = 0x7f0a1422

.field public static final tv_fax_balance:I = 0x7f0a1423

.field public static final tv_fax_country:I = 0x7f0a1424

.field public static final tv_faxstate:I = 0x7f0a1425

.field public static final tv_feec_back:I = 0x7f0a1426

.field public static final tv_feed_back:I = 0x7f0a1427

.field public static final tv_feed_back_more:I = 0x7f0a1428

.field public static final tv_feed_back_title:I = 0x7f0a1429

.field public static final tv_feed_back_type:I = 0x7f0a142a

.field public static final tv_feedback:I = 0x7f0a142b

.field public static final tv_feedback_email_label:I = 0x7f0a142c

.field public static final tv_file_classification:I = 0x7f0a142d

.field public static final tv_file_date:I = 0x7f0a142e

.field public static final tv_file_info:I = 0x7f0a142f

.field public static final tv_file_name:I = 0x7f0a1430

.field public static final tv_file_num:I = 0x7f0a1431

.field public static final tv_file_path:I = 0x7f0a1432

.field public static final tv_file_size:I = 0x7f0a1433

.field public static final tv_file_tag:I = 0x7f0a1434

.field public static final tv_file_time:I = 0x7f0a1435

.field public static final tv_file_timestamp:I = 0x7f0a1436

.field public static final tv_file_title:I = 0x7f0a1437

.field public static final tv_file_type:I = 0x7f0a1438

.field public static final tv_file_type_des:I = 0x7f0a1439

.field public static final tv_fill_in:I = 0x7f0a143a

.field public static final tv_filter:I = 0x7f0a143b

.field public static final tv_filter_name:I = 0x7f0a143c

.field public static final tv_find_pwd:I = 0x7f0a143d

.field public static final tv_finish:I = 0x7f0a143e

.field public static final tv_finish_remind:I = 0x7f0a143f

.field public static final tv_finish_select:I = 0x7f0a1440

.field public static final tv_first:I = 0x7f0a1441

.field public static final tv_first_line:I = 0x7f0a1442

.field public static final tv_first_login_create_hint:I = 0x7f0a1443

.field public static final tv_first_page:I = 0x7f0a1444

.field public static final tv_fix_database:I = 0x7f0a1445

.field public static final tv_fix_dir_layer:I = 0x7f0a1446

.field public static final tv_fix_page_number:I = 0x7f0a1447

.field public static final tv_flag:I = 0x7f0a1448

.field public static final tv_folder:I = 0x7f0a1449

.field public static final tv_folder_des:I = 0x7f0a144a

.field public static final tv_folder_empty_hint:I = 0x7f0a144b

.field public static final tv_folder_name:I = 0x7f0a144c

.field public static final tv_folder_num:I = 0x7f0a144d

.field public static final tv_folder_other_move_in:I = 0x7f0a144e

.field public static final tv_folder_search_tip:I = 0x7f0a144f

.field public static final tv_folder_title:I = 0x7f0a1450

.field public static final tv_force_quit:I = 0x7f0a1451

.field public static final tv_forget_password:I = 0x7f0a1452

.field public static final tv_forget_password_logined:I = 0x7f0a1453

.field public static final tv_forget_password_unlogined:I = 0x7f0a1454

.field public static final tv_forget_pwd_account:I = 0x7f0a1455

.field public static final tv_forget_pwd_area_code:I = 0x7f0a1456

.field public static final tv_forget_pwd_contact_us:I = 0x7f0a1457

.field public static final tv_forget_pwd_error:I = 0x7f0a1458

.field public static final tv_forget_pwd_error_msg:I = 0x7f0a1459

.field public static final tv_forget_pwd_get_vcode:I = 0x7f0a145a

.field public static final tv_forget_pwd_title:I = 0x7f0a145b

.field public static final tv_fourth:I = 0x7f0a145c

.field public static final tv_free:I = 0x7f0a145d

.field public static final tv_free_ad_des:I = 0x7f0a145e

.field public static final tv_free_ad_title:I = 0x7f0a145f

.field public static final tv_free_ocr_times:I = 0x7f0a1460

.field public static final tv_free_slogan1:I = 0x7f0a1461

.field public static final tv_free_slogan2:I = 0x7f0a1462

.field public static final tv_free_trail_got_it:I = 0x7f0a1463

.field public static final tv_free_trail_left_subtitle:I = 0x7f0a1464

.field public static final tv_free_trail_left_title:I = 0x7f0a1465

.field public static final tv_free_trail_right_title:I = 0x7f0a1466

.field public static final tv_free_trail_subtitle:I = 0x7f0a1467

.field public static final tv_free_trail_title:I = 0x7f0a1468

.field public static final tv_free_trail_vip_subtitle:I = 0x7f0a1469

.field public static final tv_free_trial:I = 0x7f0a146a

.field public static final tv_free_trial_desc:I = 0x7f0a146b

.field public static final tv_free_trial_enable:I = 0x7f0a146c

.field public static final tv_free_trial_enabled:I = 0x7f0a146d

.field public static final tv_free_trial_left_title:I = 0x7f0a146e

.field public static final tv_free_trial_purchase:I = 0x7f0a146f

.field public static final tv_free_trial_right_subtitle:I = 0x7f0a1470

.field public static final tv_free_trial_right_title:I = 0x7f0a1471

.field public static final tv_free_try_subtitle:I = 0x7f0a1472

.field public static final tv_free_txt_left:I = 0x7f0a1473

.field public static final tv_from:I = 0x7f0a1474

.field public static final tv_from_copy:I = 0x7f0a1475

.field public static final tv_full_attendance:I = 0x7f0a1476

.field public static final tv_full_attendance_status:I = 0x7f0a1477

.field public static final tv_full_result:I = 0x7f0a1478

.field public static final tv_full_screen_subtitle:I = 0x7f0a1479

.field public static final tv_full_screen_title:I = 0x7f0a147a

.field public static final tv_func1:I = 0x7f0a147b

.field public static final tv_func2:I = 0x7f0a147c

.field public static final tv_func_desc:I = 0x7f0a147d

.field public static final tv_function:I = 0x7f0a147e

.field public static final tv_function_des:I = 0x7f0a147f

.field public static final tv_function_desc:I = 0x7f0a1480

.field public static final tv_function_more:I = 0x7f0a1481

.field public static final tv_function_normal_1:I = 0x7f0a1482

.field public static final tv_function_normal_2:I = 0x7f0a1483

.field public static final tv_function_normal_3:I = 0x7f0a1484

.field public static final tv_function_title:I = 0x7f0a1485

.field public static final tv_function_title1:I = 0x7f0a1486

.field public static final tv_function_title2:I = 0x7f0a1487

.field public static final tv_function_title3:I = 0x7f0a1488

.field public static final tv_function_with_bold:I = 0x7f0a1489

.field public static final tv_get_cs_cfg:I = 0x7f0a148a

.field public static final tv_get_free:I = 0x7f0a148b

.field public static final tv_get_vcode:I = 0x7f0a148c

.field public static final tv_gift:I = 0x7f0a148d

.field public static final tv_gift_back_image_end_subtitle:I = 0x7f0a148e

.field public static final tv_gift_back_image_end_title:I = 0x7f0a148f

.field public static final tv_gift_back_image_middle_subtitle:I = 0x7f0a1490

.field public static final tv_gift_back_image_middle_title:I = 0x7f0a1491

.field public static final tv_gift_back_image_start_subtitle:I = 0x7f0a1492

.field public static final tv_gift_back_image_start_title:I = 0x7f0a1493

.field public static final tv_gift_icon:I = 0x7f0a1494

.field public static final tv_gift_name:I = 0x7f0a1495

.field public static final tv_gift_one_gift_one_btn:I = 0x7f0a1496

.field public static final tv_gift_tips:I = 0x7f0a1497

.field public static final tv_gift_title:I = 0x7f0a1498

.field public static final tv_gift_two_gift_two_btn:I = 0x7f0a1499

.field public static final tv_give_up:I = 0x7f0a149a

.field public static final tv_give_up_bottom:I = 0x7f0a149b

.field public static final tv_go2_guide:I = 0x7f0a149c

.field public static final tv_go_buy_vip:I = 0x7f0a149d

.field public static final tv_go_download:I = 0x7f0a149e

.field public static final tv_go_know:I = 0x7f0a149f

.field public static final tv_go_purchase:I = 0x7f0a14a0

.field public static final tv_go_restore:I = 0x7f0a14a1

.field public static final tv_go_scan:I = 0x7f0a14a2

.field public static final tv_go_to_download_or_login:I = 0x7f0a14a3

.field public static final tv_go_to_setting:I = 0x7f0a14a4

.field public static final tv_go_to_sync:I = 0x7f0a14a5

.field public static final tv_google:I = 0x7f0a14a6

.field public static final tv_got_it:I = 0x7f0a14a7

.field public static final tv_goto_composite:I = 0x7f0a14a8

.field public static final tv_goto_discuss:I = 0x7f0a14a9

.field public static final tv_goto_experience:I = 0x7f0a14aa

.field public static final tv_goto_mark:I = 0x7f0a14ab

.field public static final tv_goto_sns:I = 0x7f0a14ac

.field public static final tv_goto_weixin:I = 0x7f0a14ad

.field public static final tv_gp_guide_mark_cancel:I = 0x7f0a14ae

.field public static final tv_gp_guide_mark_go_to_mark:I = 0x7f0a14af

.field public static final tv_gp_guide_mark_go_to_mark_new:I = 0x7f0a14b0

.field public static final tv_gp_guide_mark_new_cancel_new:I = 0x7f0a14b1

.field public static final tv_gp_guide_mark_new_dec:I = 0x7f0a14b2

.field public static final tv_gp_guide_mark_new_title:I = 0x7f0a14b3

.field public static final tv_gp_guide_mark_title:I = 0x7f0a14b4

.field public static final tv_gp_guide_mark_title_dec:I = 0x7f0a14b5

.field public static final tv_gp_tip_guide:I = 0x7f0a14b6

.field public static final tv_grade:I = 0x7f0a14b7

.field public static final tv_greet_instrucitons:I = 0x7f0a14b8

.field public static final tv_group_name:I = 0x7f0a14b9

.field public static final tv_group_title:I = 0x7f0a14ba

.field public static final tv_guide:I = 0x7f0a14bb

.field public static final tv_guide_auto_capture_tips:I = 0x7f0a14bc

.field public static final tv_guide_cn_purchase_register:I = 0x7f0a14bd

.field public static final tv_guide_cn_purchase_use_now:I = 0x7f0a14be

.field public static final tv_guide_for_not_downloaded_image:I = 0x7f0a14bf

.field public static final tv_guide_gp_item_header_left_title:I = 0x7f0a14c0

.field public static final tv_guide_gp_item_middle_title:I = 0x7f0a14c1

.field public static final tv_guide_gp_item_normal_left_title:I = 0x7f0a14c2

.field public static final tv_guide_gp_item_normal_middle_title:I = 0x7f0a14c3

.field public static final tv_guide_gp_item_normal_right_title:I = 0x7f0a14c4

.field public static final tv_guide_gp_item_right_title:I = 0x7f0a14c5

.field public static final tv_guide_gp_purchase_page_shadow_title_1:I = 0x7f0a14c6

.field public static final tv_guide_gp_purchase_page_shadow_title_2:I = 0x7f0a14c7

.field public static final tv_guide_gp_register:I = 0x7f0a14c8

.field public static final tv_guide_gp_use_now:I = 0x7f0a14c9

.field public static final tv_guide_intro:I = 0x7f0a14ca

.field public static final tv_guide_login:I = 0x7f0a14cb

.field public static final tv_guide_subtitle:I = 0x7f0a14cc

.field public static final tv_guide_text:I = 0x7f0a14cd

.field public static final tv_guide_tips:I = 0x7f0a14ce

.field public static final tv_guide_title:I = 0x7f0a14cf

.field public static final tv_guide_title_en:I = 0x7f0a14d0

.field public static final tv_handwriting:I = 0x7f0a14d1

.field public static final tv_head:I = 0x7f0a14d2

.field public static final tv_header_directory:I = 0x7f0a14d3

.field public static final tv_header_import:I = 0x7f0a14d4

.field public static final tv_header_name:I = 0x7f0a14d5

.field public static final tv_header_option:I = 0x7f0a14d6

.field public static final tv_hear_from:I = 0x7f0a14d7

.field public static final tv_help:I = 0x7f0a14d8

.field public static final tv_help_tips:I = 0x7f0a14d9

.field public static final tv_hint:I = 0x7f0a14da

.field public static final tv_hint_for_give_gift:I = 0x7f0a14db

.field public static final tv_hint_for_qr_code:I = 0x7f0a14dc

.field public static final tv_history_title:I = 0x7f0a14dd

.field public static final tv_hour:I = 0x7f0a14de

.field public static final tv_how_to:I = 0x7f0a14df

.field public static final tv_how_to_capture:I = 0x7f0a14e0

.field public static final tv_how_use:I = 0x7f0a14e1

.field public static final tv_ht_duty_explain_agree:I = 0x7f0a14e2

.field public static final tv_ht_duty_explain_explain:I = 0x7f0a14e3

.field public static final tv_ht_duty_explain_message:I = 0x7f0a14e4

.field public static final tv_ht_duty_explain_not_agree:I = 0x7f0a14e5

.field public static final tv_ht_duty_explain_title:I = 0x7f0a14e6

.field public static final tv_i_know:I = 0x7f0a14e7

.field public static final tv_ic_house_property:I = 0x7f0a14e8

.field public static final tv_id_certificate:I = 0x7f0a14e9

.field public static final tv_illegal_image_tips:I = 0x7f0a14ea

.field public static final tv_image_ok:I = 0x7f0a14eb

.field public static final tv_image_quality_close:I = 0x7f0a14ec

.field public static final tv_image_quality_replace:I = 0x7f0a14ed

.field public static final tv_image_quality_tips:I = 0x7f0a14ee

.field public static final tv_image_restore:I = 0x7f0a14ef

.field public static final tv_image_restore_title:I = 0x7f0a14f0

.field public static final tv_image_tips:I = 0x7f0a14f1

.field public static final tv_img_on_deal:I = 0x7f0a14f2

.field public static final tv_import:I = 0x7f0a14f3

.field public static final tv_import_backup:I = 0x7f0a14f4

.field public static final tv_import_file:I = 0x7f0a14f5

.field public static final tv_import_img:I = 0x7f0a14f6

.field public static final tv_import_office_hint:I = 0x7f0a14f7

.field public static final tv_import_pdf_to_office:I = 0x7f0a14f8

.field public static final tv_income:I = 0x7f0a14f9

.field public static final tv_index:I = 0x7f0a14fa

.field public static final tv_indicator:I = 0x7f0a14fb

.field public static final tv_info:I = 0x7f0a14fc

.field public static final tv_info_collect_agreement_desc:I = 0x7f0a14fd

.field public static final tv_instrucitons:I = 0x7f0a14fe

.field public static final tv_interval:I = 0x7f0a14ff

.field public static final tv_intro:I = 0x7f0a1500

.field public static final tv_introduce:I = 0x7f0a1501

.field public static final tv_invite:I = 0x7f0a1502

.field public static final tv_invite_by_app:I = 0x7f0a1503

.field public static final tv_invite_by_phone_email:I = 0x7f0a1504

.field public static final tv_invite_share_dir:I = 0x7f0a1505

.field public static final tv_invoice_count:I = 0x7f0a1506

.field public static final tv_invoice_date_content:I = 0x7f0a1507

.field public static final tv_invoice_date_intro:I = 0x7f0a1508

.field public static final tv_invoice_title:I = 0x7f0a1509

.field public static final tv_invoice_txt_value:I = 0x7f0a150a

.field public static final tv_ip:I = 0x7f0a150b

.field public static final tv_is_origin_invited:I = 0x7f0a150c

.field public static final tv_item1_text1:I = 0x7f0a150d

.field public static final tv_item1_text2:I = 0x7f0a150e

.field public static final tv_item1_text2_left:I = 0x7f0a150f

.field public static final tv_item2_text1:I = 0x7f0a1510

.field public static final tv_item2_text2:I = 0x7f0a1511

.field public static final tv_item2_text2_left:I = 0x7f0a1512

.field public static final tv_item2_text3:I = 0x7f0a1513

.field public static final tv_item3_text1:I = 0x7f0a1514

.field public static final tv_item3_text2:I = 0x7f0a1515

.field public static final tv_item3_text2_left:I = 0x7f0a1516

.field public static final tv_item3_text3:I = 0x7f0a1517

.field public static final tv_item_basic_title:I = 0x7f0a1518

.field public static final tv_item_bind_account_action:I = 0x7f0a1519

.field public static final tv_item_bind_account_subtitle:I = 0x7f0a151a

.field public static final tv_item_bind_account_title:I = 0x7f0a151b

.field public static final tv_item_btn:I = 0x7f0a151c

.field public static final tv_item_certificate_detail_left:I = 0x7f0a151d

.field public static final tv_item_certificate_detail_right:I = 0x7f0a151e

.field public static final tv_item_date:I = 0x7f0a151f

.field public static final tv_item_desc:I = 0x7f0a1520

.field public static final tv_item_get_now:I = 0x7f0a1521

.field public static final tv_item_gold_title:I = 0x7f0a1522

.field public static final tv_item_guide_cn_title:I = 0x7f0a1523

.field public static final tv_item_logo:I = 0x7f0a1524

.field public static final tv_item_me_page_header_account:I = 0x7f0a1525

.field public static final tv_item_me_page_header_account_sub_title:I = 0x7f0a1526

.field public static final tv_item_me_page_header_cloud_space_count:I = 0x7f0a1527

.field public static final tv_item_me_page_header_edu:I = 0x7f0a1528

.field public static final tv_item_me_page_header_id_tag:I = 0x7f0a1529

.field public static final tv_item_name_1:I = 0x7f0a152a

.field public static final tv_item_name_2:I = 0x7f0a152b

.field public static final tv_item_name_3:I = 0x7f0a152c

.field public static final tv_item_negative_premium_title:I = 0x7f0a152d

.field public static final tv_item_no_time:I = 0x7f0a152e

.field public static final tv_item_no_time_left:I = 0x7f0a152f

.field public static final tv_item_no_time_right:I = 0x7f0a1530

.field public static final tv_item_purchase_new_style2:I = 0x7f0a1531

.field public static final tv_item_seal:I = 0x7f0a1532

.field public static final tv_item_share_link_title:I = 0x7f0a1533

.field public static final tv_item_signature:I = 0x7f0a1534

.field public static final tv_item_template:I = 0x7f0a1535

.field public static final tv_item_time:I = 0x7f0a1536

.field public static final tv_item_title:I = 0x7f0a1537

.field public static final tv_item_title_2:I = 0x7f0a1538

.field public static final tv_item_title_3:I = 0x7f0a1539

.field public static final tv_item_title_no:I = 0x7f0a153a

.field public static final tv_item_value_1:I = 0x7f0a153b

.field public static final tv_item_value_2:I = 0x7f0a153c

.field public static final tv_item_value_3:I = 0x7f0a153d

.field public static final tv_job:I = 0x7f0a153e

.field public static final tv_jpg_file:I = 0x7f0a153f

.field public static final tv_jump:I = 0x7f0a1540

.field public static final tv_jump_2_camexam:I = 0x7f0a1541

.field public static final tv_key:I = 0x7f0a1542

.field public static final tv_key_name:I = 0x7f0a1543

.field public static final tv_keyboard_cancel:I = 0x7f0a1544

.field public static final tv_keyboard_finish:I = 0x7f0a1545

.field public static final tv_know:I = 0x7f0a1546

.field public static final tv_know_introduce:I = 0x7f0a1547

.field public static final tv_know_login:I = 0x7f0a1548

.field public static final tv_known:I = 0x7f0a1549

.field public static final tv_label:I = 0x7f0a154a

.field public static final tv_label_language:I = 0x7f0a154b

.field public static final tv_lan_1:I = 0x7f0a154c

.field public static final tv_lan_2:I = 0x7f0a154d

.field public static final tv_lan_3:I = 0x7f0a154e

.field public static final tv_lan_auto:I = 0x7f0a154f

.field public static final tv_last_intro:I = 0x7f0a1550

.field public static final tv_last_login:I = 0x7f0a1551

.field public static final tv_last_login_nick_name:I = 0x7f0a1552

.field public static final tv_last_login_other_login_ways:I = 0x7f0a1553

.field public static final tv_last_login_tips:I = 0x7f0a1554

.field public static final tv_last_share:I = 0x7f0a1555

.field public static final tv_last_share_tip:I = 0x7f0a1556

.field public static final tv_later:I = 0x7f0a1557

.field public static final tv_layout_title:I = 0x7f0a1558

.field public static final tv_layout_value:I = 0x7f0a1559

.field public static final tv_left:I = 0x7f0a155a

.field public static final tv_left_annotation:I = 0x7f0a155b

.field public static final tv_left_bubble_tips:I = 0x7f0a155c

.field public static final tv_left_count:I = 0x7f0a155d

.field public static final tv_left_count_tips:I = 0x7f0a155e

.field public static final tv_left_counts:I = 0x7f0a155f

.field public static final tv_left_item_normal_privileges_description:I = 0x7f0a1560

.field public static final tv_left_title_fifth:I = 0x7f0a1561

.field public static final tv_left_title_first:I = 0x7f0a1562

.field public static final tv_left_title_fourth:I = 0x7f0a1563

.field public static final tv_left_title_second:I = 0x7f0a1564

.field public static final tv_left_title_third:I = 0x7f0a1565

.field public static final tv_licence:I = 0x7f0a1566

.field public static final tv_license_tips:I = 0x7f0a1567

.field public static final tv_life_time_purchase_left_discount:I = 0x7f0a1568

.field public static final tv_life_time_purchase_left_label:I = 0x7f0a1569

.field public static final tv_life_time_purchase_left_lifetime_permit:I = 0x7f0a156a

.field public static final tv_life_time_purchase_left_lifetime_permit_price:I = 0x7f0a156b

.field public static final tv_life_time_purchase_left_month_unit:I = 0x7f0a156c

.field public static final tv_life_time_purchase_left_total_price:I = 0x7f0a156d

.field public static final tv_life_time_purchase_middle_label:I = 0x7f0a156e

.field public static final tv_life_time_purchase_middle_month_number:I = 0x7f0a156f

.field public static final tv_life_time_purchase_middle_month_price:I = 0x7f0a1570

.field public static final tv_life_time_purchase_middle_month_unit:I = 0x7f0a1571

.field public static final tv_life_time_purchase_middle_saving_percent:I = 0x7f0a1572

.field public static final tv_life_time_purchase_middle_total_price:I = 0x7f0a1573

.field public static final tv_life_time_purchase_right_label:I = 0x7f0a1574

.field public static final tv_life_time_purchase_right_month_number:I = 0x7f0a1575

.field public static final tv_life_time_purchase_right_month_price:I = 0x7f0a1576

.field public static final tv_life_time_purchase_right_month_unit:I = 0x7f0a1577

.field public static final tv_life_time_purchase_right_saving_percent:I = 0x7f0a1578

.field public static final tv_life_time_purchase_right_total_price:I = 0x7f0a1579

.field public static final tv_limit_date:I = 0x7f0a157a

.field public static final tv_limit_free:I = 0x7f0a157b

.field public static final tv_line1_left:I = 0x7f0a157c

.field public static final tv_line1_right_bottom:I = 0x7f0a157d

.field public static final tv_line1_right_top:I = 0x7f0a157e

.field public static final tv_line2_left:I = 0x7f0a157f

.field public static final tv_line2_right:I = 0x7f0a1580

.field public static final tv_lineation:I = 0x7f0a1581

.field public static final tv_link:I = 0x7f0a1582

.field public static final tv_link_enc_days:I = 0x7f0a1583

.field public static final tv_loading_msg:I = 0x7f0a1584

.field public static final tv_local_points:I = 0x7f0a1585

.field public static final tv_login_account:I = 0x7f0a1586

.field public static final tv_login_des:I = 0x7f0a1587

.field public static final tv_login_intro:I = 0x7f0a1588

.field public static final tv_login_main_account:I = 0x7f0a1589

.field public static final tv_login_main_contracts_link:I = 0x7f0a158a

.field public static final tv_login_main_error_msg:I = 0x7f0a158b

.field public static final tv_login_main_last_login_tips:I = 0x7f0a158c

.field public static final tv_login_main_we_chat:I = 0x7f0a158d

.field public static final tv_login_mobile:I = 0x7f0a158e

.field public static final tv_login_opt_login:I = 0x7f0a158f

.field public static final tv_login_opt_other_login:I = 0x7f0a1590

.field public static final tv_login_protocol:I = 0x7f0a1591

.field public static final tv_login_ways_contracts_link:I = 0x7f0a1592

.field public static final tv_login_ways_last_login_tips:I = 0x7f0a1593

.field public static final tv_login_ways_learn:I = 0x7f0a1594

.field public static final tv_login_ways_license_tips:I = 0x7f0a1595

.field public static final tv_login_ways_protocol_no_cb:I = 0x7f0a1596

.field public static final tv_main_title:I = 0x7f0a1597

.field public static final tv_manage_tag:I = 0x7f0a1598

.field public static final tv_mark_all_read:I = 0x7f0a1599

.field public static final tv_mark_text:I = 0x7f0a159a

.field public static final tv_mask:I = 0x7f0a159b

.field public static final tv_me_page_card_account_status:I = 0x7f0a159c

.field public static final tv_me_page_card_edu_intro:I = 0x7f0a159d

.field public static final tv_me_page_card_privilege:I = 0x7f0a159e

.field public static final tv_me_page_card_right_arrow:I = 0x7f0a159f

.field public static final tv_me_page_card_right_btn:I = 0x7f0a15a0

.field public static final tv_me_page_card_validate_time:I = 0x7f0a15a1

.field public static final tv_me_page_header_enlarge_space:I = 0x7f0a15a2

.field public static final tv_me_page_header_nickname:I = 0x7f0a15a3

.field public static final tv_me_page_king_kong_first_subtitle:I = 0x7f0a15a4

.field public static final tv_me_page_king_kong_first_title:I = 0x7f0a15a5

.field public static final tv_me_page_king_kong_fourth_subtitle:I = 0x7f0a15a6

.field public static final tv_me_page_king_kong_fourth_title:I = 0x7f0a15a7

.field public static final tv_me_page_king_kong_second_subtitle:I = 0x7f0a15a8

.field public static final tv_me_page_king_kong_second_title:I = 0x7f0a15a9

.field public static final tv_me_page_king_kong_sub:I = 0x7f0a15aa

.field public static final tv_me_page_king_kong_third_subtitle:I = 0x7f0a15ab

.field public static final tv_me_page_king_kong_third_title:I = 0x7f0a15ac

.field public static final tv_me_page_king_kong_title:I = 0x7f0a15ad

.field public static final tv_me_page_settings_about:I = 0x7f0a15ae

.field public static final tv_me_page_settings_account:I = 0x7f0a15af

.field public static final tv_me_page_settings_account_red_dot:I = 0x7f0a15b0

.field public static final tv_me_page_settings_add_widget:I = 0x7f0a15b1

.field public static final tv_me_page_settings_doc_management:I = 0x7f0a15b2

.field public static final tv_me_page_settings_doc_scan:I = 0x7f0a15b3

.field public static final tv_me_page_settings_faq_help:I = 0x7f0a15b4

.field public static final tv_me_page_settings_more:I = 0x7f0a15b5

.field public static final tv_me_page_settings_recommend:I = 0x7f0a15b6

.field public static final tv_me_page_settings_student_rights:I = 0x7f0a15b7

.field public static final tv_me_page_settings_subscription_management:I = 0x7f0a15b8

.field public static final tv_me_page_settings_sync:I = 0x7f0a15b9

.field public static final tv_me_page_upgrade_vip:I = 0x7f0a15ba

.field public static final tv_member_count:I = 0x7f0a15bb

.field public static final tv_member_list:I = 0x7f0a15bc

.field public static final tv_menu_text:I = 0x7f0a15bd

.field public static final tv_message:I = 0x7f0a15be

.field public static final tv_middle:I = 0x7f0a15bf

.field public static final tv_middle_item_normal_basic_description:I = 0x7f0a15c0

.field public static final tv_middle_title:I = 0x7f0a15c1

.field public static final tv_minute:I = 0x7f0a15c2

.field public static final tv_mobile_area_code:I = 0x7f0a15c3

.field public static final tv_mobile_country:I = 0x7f0a15c4

.field public static final tv_mobile_name:I = 0x7f0a15c5

.field public static final tv_mobile_never_tip:I = 0x7f0a15c6

.field public static final tv_mobile_number_error:I = 0x7f0a15c7

.field public static final tv_mobile_number_get_vcode:I = 0x7f0a15c8

.field public static final tv_mobile_number_input:I = 0x7f0a15c9

.field public static final tv_mobile_number_protocol:I = 0x7f0a15ca

.field public static final tv_mobile_number_pwd_login:I = 0x7f0a15cb

.field public static final tv_mobile_number_title:I = 0x7f0a15cc

.field public static final tv_mobile_pwd_login:I = 0x7f0a15cd

.field public static final tv_mobile_pwd_login_area_code:I = 0x7f0a15ce

.field public static final tv_mobile_pwd_login_country:I = 0x7f0a15cf

.field public static final tv_mobile_pwd_login_error:I = 0x7f0a15d0

.field public static final tv_mobile_pwd_login_find_pwd:I = 0x7f0a15d1

.field public static final tv_mobile_pwd_login_input_number:I = 0x7f0a15d2

.field public static final tv_mobile_pwd_login_input_pwd:I = 0x7f0a15d3

.field public static final tv_mobile_pwd_login_protocol:I = 0x7f0a15d4

.field public static final tv_mobile_pwd_login_title:I = 0x7f0a15d5

.field public static final tv_mobile_sms_login:I = 0x7f0a15d6

.field public static final tv_mode_tips:I = 0x7f0a15d7

.field public static final tv_model_name:I = 0x7f0a15d8

.field public static final tv_model_size:I = 0x7f0a15d9

.field public static final tv_modify:I = 0x7f0a15da

.field public static final tv_modify_mark:I = 0x7f0a15db

.field public static final tv_modify_style:I = 0x7f0a15dc

.field public static final tv_modify_time:I = 0x7f0a15dd

.field public static final tv_monitor:I = 0x7f0a15de

.field public static final tv_month_card_title:I = 0x7f0a15df

.field public static final tv_more:I = 0x7f0a15e0

.field public static final tv_more_feature:I = 0x7f0a15e1

.field public static final tv_more_fun_1:I = 0x7f0a15e2

.field public static final tv_more_fun_2:I = 0x7f0a15e3

.field public static final tv_more_privilege:I = 0x7f0a15e4

.field public static final tv_more_privileges:I = 0x7f0a15e5

.field public static final tv_more_title:I = 0x7f0a15e6

.field public static final tv_move_doc:I = 0x7f0a15e7

.field public static final tv_move_page_top_notice_bar:I = 0x7f0a15e8

.field public static final tv_msg:I = 0x7f0a15e9

.field public static final tv_msg_desc:I = 0x7f0a15ea

.field public static final tv_multi_mode:I = 0x7f0a15eb

.field public static final tv_my_device:I = 0x7f0a15ec

.field public static final tv_my_tag:I = 0x7f0a15ed

.field public static final tv_name:I = 0x7f0a15ee

.field public static final tv_name1:I = 0x7f0a15ef

.field public static final tv_name10:I = 0x7f0a15f0

.field public static final tv_name2:I = 0x7f0a15f1

.field public static final tv_name3:I = 0x7f0a15f2

.field public static final tv_name4:I = 0x7f0a15f3

.field public static final tv_name5:I = 0x7f0a15f4

.field public static final tv_name6:I = 0x7f0a15f5

.field public static final tv_name7:I = 0x7f0a15f6

.field public static final tv_name8:I = 0x7f0a15f7

.field public static final tv_name9:I = 0x7f0a15f8

.field public static final tv_name_pair_enhance_line_use_bm:I = 0x7f0a15f9

.field public static final tv_name_share:I = 0x7f0a15fa

.field public static final tv_name_title:I = 0x7f0a15fb

.field public static final tv_negative:I = 0x7f0a15fc

.field public static final tv_never_show:I = 0x7f0a15fd

.field public static final tv_never_tip:I = 0x7f0a15fe

.field public static final tv_new:I = 0x7f0a15ff

.field public static final tv_new_user_scan_doc_count:I = 0x7f0a1600

.field public static final tv_new_user_scan_gift:I = 0x7f0a1601

.field public static final tv_new_year:I = 0x7f0a1602

.field public static final tv_next:I = 0x7f0a1603

.field public static final tv_next_charge_time:I = 0x7f0a1604

.field public static final tv_next_time:I = 0x7f0a1605

.field public static final tv_next_tip:I = 0x7f0a1606

.field public static final tv_nickname:I = 0x7f0a1607

.field public static final tv_no_ad_subtitle:I = 0x7f0a1608

.field public static final tv_no_ad_title:I = 0x7f0a1609

.field public static final tv_no_cache:I = 0x7f0a160a

.field public static final tv_no_enhance:I = 0x7f0a160b

.field public static final tv_no_file:I = 0x7f0a160c

.field public static final tv_no_thanks:I = 0x7f0a160d

.field public static final tv_no_watermark_title:I = 0x7f0a160e

.field public static final tv_not_agree_agreement:I = 0x7f0a160f

.field public static final tv_not_open:I = 0x7f0a1610

.field public static final tv_not_printer:I = 0x7f0a1611

.field public static final tv_not_quit:I = 0x7f0a1612

.field public static final tv_not_recognize_hint:I = 0x7f0a1613

.field public static final tv_not_start:I = 0x7f0a1614

.field public static final tv_notice_des:I = 0x7f0a1615

.field public static final tv_notice_title:I = 0x7f0a1616

.field public static final tv_num:I = 0x7f0a1617

.field public static final tv_num_desc:I = 0x7f0a1618

.field public static final tv_num_one:I = 0x7f0a1619

.field public static final tv_num_two:I = 0x7f0a161a

.field public static final tv_number:I = 0x7f0a161b

.field public static final tv_o_vip_right_times:I = 0x7f0a161c

.field public static final tv_o_vip_right_title:I = 0x7f0a161d

.field public static final tv_o_vip_title:I = 0x7f0a161e

.field public static final tv_observe_back_upped_files:I = 0x7f0a161f

.field public static final tv_observe_doc:I = 0x7f0a1620

.field public static final tv_ocr:I = 0x7f0a1621

.field public static final tv_ocr_auto_wrap:I = 0x7f0a1622

.field public static final tv_ocr_experience_example:I = 0x7f0a1623

.field public static final tv_ocr_lang_name:I = 0x7f0a1624

.field public static final tv_ocr_limit_des:I = 0x7f0a1625

.field public static final tv_ocr_multi_mode:I = 0x7f0a1626

.field public static final tv_ocr_progress:I = 0x7f0a1627

.field public static final tv_ocr_result:I = 0x7f0a1628

.field public static final tv_ocr_result_empty_view:I = 0x7f0a1629

.field public static final tv_ocr_result_page_num:I = 0x7f0a162a

.field public static final tv_ocr_set_ocr_lang:I = 0x7f0a162b

.field public static final tv_ocr_single_mode:I = 0x7f0a162c

.field public static final tv_ocr_tip:I = 0x7f0a162d

.field public static final tv_ocr_title_tip:I = 0x7f0a162e

.field public static final tv_ocr_upgrade_des:I = 0x7f0a162f

.field public static final tv_ocr_upgrade_title:I = 0x7f0a1630

.field public static final tv_office2cloud_export:I = 0x7f0a1631

.field public static final tv_office2cloud_save_as:I = 0x7f0a1632

.field public static final tv_ok:I = 0x7f0a1633

.field public static final tv_one_binding_other_phone:I = 0x7f0a1634

.field public static final tv_one_key_import:I = 0x7f0a1635

.field public static final tv_one_login_auth_mail:I = 0x7f0a1636

.field public static final tv_one_login_auth_more_login_way:I = 0x7f0a1637

.field public static final tv_one_login_auth_wechat:I = 0x7f0a1638

.field public static final tv_one_login_half_bind_other_phone:I = 0x7f0a1639

.field public static final tv_one_login_half_intro:I = 0x7f0a163a

.field public static final tv_one_login_half_intro_for_login:I = 0x7f0a163b

.field public static final tv_one_login_other_phone_login:I = 0x7f0a163c

.field public static final tv_one_login_skip:I = 0x7f0a163d

.field public static final tv_one_login_title:I = 0x7f0a163e

.field public static final tv_one_text:I = 0x7f0a163f

.field public static final tv_onenote:I = 0x7f0a1640

.field public static final tv_only_read_capture:I = 0x7f0a1641

.field public static final tv_only_read_first:I = 0x7f0a1642

.field public static final tv_only_read_four:I = 0x7f0a1643

.field public static final tv_only_read_second:I = 0x7f0a1644

.field public static final tv_only_read_third:I = 0x7f0a1645

.field public static final tv_ope:I = 0x7f0a1646

.field public static final tv_open:I = 0x7f0a1647

.field public static final tv_open_camera:I = 0x7f0a1648

.field public static final tv_open_explore_input:I = 0x7f0a1649

.field public static final tv_open_explore_input_2:I = 0x7f0a164a

.field public static final tv_open_or_search:I = 0x7f0a164b

.field public static final tv_open_sync_hint:I = 0x7f0a164c

.field public static final tv_operation_feedback:I = 0x7f0a164d

.field public static final tv_operation_open:I = 0x7f0a164e

.field public static final tv_operation_share:I = 0x7f0a164f

.field public static final tv_operationn:I = 0x7f0a1650

.field public static final tv_operator_type:I = 0x7f0a1651

.field public static final tv_opt_1:I = 0x7f0a1652

.field public static final tv_opt_2:I = 0x7f0a1653

.field public static final tv_orientation:I = 0x7f0a1654

.field public static final tv_original_price:I = 0x7f0a1655

.field public static final tv_original_price1:I = 0x7f0a1656

.field public static final tv_original_price2:I = 0x7f0a1657

.field public static final tv_original_price3:I = 0x7f0a1658

.field public static final tv_other_name:I = 0x7f0a1659

.field public static final tv_other_share_way:I = 0x7f0a165a

.field public static final tv_other_vip_desc:I = 0x7f0a165b

.field public static final tv_over:I = 0x7f0a165c

.field public static final tv_owner_name:I = 0x7f0a165d

.field public static final tv_owner_phone_email:I = 0x7f0a165e

.field public static final tv_page:I = 0x7f0a165f

.field public static final tv_page_1_divider:I = 0x7f0a1660

.field public static final tv_page_1_mark_title:I = 0x7f0a1661

.field public static final tv_page_1_next:I = 0x7f0a1662

.field public static final tv_page_1_title:I = 0x7f0a1663

.field public static final tv_page_1_top_title:I = 0x7f0a1664

.field public static final tv_page_1_year_subtitle:I = 0x7f0a1665

.field public static final tv_page_1_year_tittle:I = 0x7f0a1666

.field public static final tv_page_2_continue:I = 0x7f0a1667

.field public static final tv_page_2_free_trail_title:I = 0x7f0a1668

.field public static final tv_page_2_free_trial_month_switch_title:I = 0x7f0a1669

.field public static final tv_page_2_payment:I = 0x7f0a166a

.field public static final tv_page_2_start_limited_version:I = 0x7f0a166b

.field public static final tv_page_2_top_title:I = 0x7f0a166c

.field public static final tv_page_count_0:I = 0x7f0a166d

.field public static final tv_page_count_1:I = 0x7f0a166e

.field public static final tv_page_count_2:I = 0x7f0a166f

.field public static final tv_page_count_3:I = 0x7f0a1670

.field public static final tv_page_divider:I = 0x7f0a1671

.field public static final tv_page_index:I = 0x7f0a1672

.field public static final tv_page_num:I = 0x7f0a1673

.field public static final tv_page_other_title:I = 0x7f0a1674

.field public static final tv_page_range:I = 0x7f0a1675

.field public static final tv_page_range_title:I = 0x7f0a1676

.field public static final tv_page_tag:I = 0x7f0a1677

.field public static final tv_page_title:I = 0x7f0a1678

.field public static final tv_paper:I = 0x7f0a1679

.field public static final tv_paper_show_raw_trimmed_hint:I = 0x7f0a167a

.field public static final tv_paper_subject_grade_semester:I = 0x7f0a167b

.field public static final tv_paper_title:I = 0x7f0a167c

.field public static final tv_paper_type:I = 0x7f0a167d

.field public static final tv_passport_certificate:I = 0x7f0a167e

.field public static final tv_password_login:I = 0x7f0a167f

.field public static final tv_password_tip:I = 0x7f0a1680

.field public static final tv_paste:I = 0x7f0a1681

.field public static final tv_pay:I = 0x7f0a1682

.field public static final tv_pay_price:I = 0x7f0a1683

.field public static final tv_pay_price_yuan:I = 0x7f0a1684

.field public static final tv_pay_way:I = 0x7f0a1685

.field public static final tv_payment:I = 0x7f0a1686

.field public static final tv_payment_content:I = 0x7f0a1687

.field public static final tv_pdf:I = 0x7f0a1688

.field public static final tv_pdf_editing_cancel_pdf_password:I = 0x7f0a1689

.field public static final tv_pdf_editing_compress:I = 0x7f0a168a

.field public static final tv_pdf_editing_compress_small:I = 0x7f0a168b

.field public static final tv_pdf_editing_compress_small_checkbox:I = 0x7f0a168c

.field public static final tv_pdf_editing_compress_small_layout:I = 0x7f0a168d

.field public static final tv_pdf_editing_no_compress:I = 0x7f0a168e

.field public static final tv_pdf_editing_page_index:I = 0x7f0a168f

.field public static final tv_pdf_editing_reset_password:I = 0x7f0a1690

.field public static final tv_pdf_editing_watermark_modify:I = 0x7f0a1691

.field public static final tv_pdf_editing_watermark_remove:I = 0x7f0a1692

.field public static final tv_pdf_entry:I = 0x7f0a1693

.field public static final tv_pdf_extract:I = 0x7f0a1694

.field public static final tv_pdf_file:I = 0x7f0a1695

.field public static final tv_pdf_name:I = 0x7f0a1696

.field public static final tv_pdf_print_suggest_label:I = 0x7f0a1697

.field public static final tv_pdf_title:I = 0x7f0a1698

.field public static final tv_pdf_watermark_file_size:I = 0x7f0a1699

.field public static final tv_pdf_watermark_pdf_no_watermark_selected:I = 0x7f0a169a

.field public static final tv_pdf_watermark_pdf_selected:I = 0x7f0a169b

.field public static final tv_pdf_watermark_real_size_switch:I = 0x7f0a169c

.field public static final tv_pdf_watermark_small_size_switch:I = 0x7f0a169d

.field public static final tv_per_point:I = 0x7f0a169e

.field public static final tv_percent:I = 0x7f0a169f

.field public static final tv_period_day:I = 0x7f0a16a0

.field public static final tv_permission_tip_title:I = 0x7f0a16a1

.field public static final tv_permission_tips:I = 0x7f0a16a2

.field public static final tv_personal_dir_subtitle:I = 0x7f0a16a3

.field public static final tv_personal_dir_title:I = 0x7f0a16a4

.field public static final tv_phone_area_code:I = 0x7f0a16a5

.field public static final tv_phone_area_code_name:I = 0x7f0a16a6

.field public static final tv_phone_email:I = 0x7f0a16a7

.field public static final tv_phone_name:I = 0x7f0a16a8

.field public static final tv_phone_pwd_login_area_code:I = 0x7f0a16a9

.field public static final tv_phone_pwd_login_error_msg:I = 0x7f0a16aa

.field public static final tv_phone_pwd_login_forget_password:I = 0x7f0a16ab

.field public static final tv_phone_pwd_login_phone_number:I = 0x7f0a16ac

.field public static final tv_phone_pwd_login_verify_code_login:I = 0x7f0a16ad

.field public static final tv_phone_verify_code_login_account:I = 0x7f0a16ae

.field public static final tv_phone_verify_code_login_error_msg:I = 0x7f0a16af

.field public static final tv_phone_verify_code_login_pwd_login:I = 0x7f0a16b0

.field public static final tv_photo_desc:I = 0x7f0a16b1

.field public static final tv_photo_name:I = 0x7f0a16b2

.field public static final tv_photo_policy:I = 0x7f0a16b3

.field public static final tv_photo_provide:I = 0x7f0a16b4

.field public static final tv_photo_size:I = 0x7f0a16b5

.field public static final tv_plus_num:I = 0x7f0a16b6

.field public static final tv_points_introduce:I = 0x7f0a16b7

.field public static final tv_points_left:I = 0x7f0a16b8

.field public static final tv_points_no_enough:I = 0x7f0a16b9

.field public static final tv_points_price:I = 0x7f0a16ba

.field public static final tv_points_tip:I = 0x7f0a16bb

.field public static final tv_positive:I = 0x7f0a16bc

.field public static final tv_positive_golden_premium_year_price_label:I = 0x7f0a16bd

.field public static final tv_ppt_auto_scale_tips:I = 0x7f0a16be

.field public static final tv_premium_notice:I = 0x7f0a16bf

.field public static final tv_premium_notice_1:I = 0x7f0a16c0

.field public static final tv_preview_tips:I = 0x7f0a16c1

.field public static final tv_price:I = 0x7f0a16c2

.field public static final tv_price_desc:I = 0x7f0a16c3

.field public static final tv_price_description:I = 0x7f0a16c4

.field public static final tv_price_discount:I = 0x7f0a16c5

.field public static final tv_price_info:I = 0x7f0a16c6

.field public static final tv_price_local_purchase:I = 0x7f0a16c7

.field public static final tv_price_purchase_convert_month_subtitle:I = 0x7f0a16c8

.field public static final tv_price_purchase_month_headline:I = 0x7f0a16c9

.field public static final tv_price_purchase_month_subtitle:I = 0x7f0a16ca

.field public static final tv_price_purchase_year_headline:I = 0x7f0a16cb

.field public static final tv_price_show:I = 0x7f0a16cc

.field public static final tv_price_sub_local_purchase:I = 0x7f0a16cd

.field public static final tv_price_top:I = 0x7f0a16ce

.field public static final tv_print:I = 0x7f0a16cf

.field public static final tv_print_cs:I = 0x7f0a16d0

.field public static final tv_print_doc:I = 0x7f0a16d1

.field public static final tv_print_other:I = 0x7f0a16d2

.field public static final tv_print_paper:I = 0x7f0a16d3

.field public static final tv_print_spread:I = 0x7f0a16d4

.field public static final tv_print_times:I = 0x7f0a16d5

.field public static final tv_printer_des:I = 0x7f0a16d6

.field public static final tv_privacy:I = 0x7f0a16d7

.field public static final tv_privacy_agreement_des:I = 0x7f0a16d8

.field public static final tv_privilege:I = 0x7f0a16d9

.field public static final tv_privileges_left_title:I = 0x7f0a16da

.field public static final tv_product_name:I = 0x7f0a16db

.field public static final tv_product_price:I = 0x7f0a16dc

.field public static final tv_progress:I = 0x7f0a16dd

.field public static final tv_prompt:I = 0x7f0a16de

.field public static final tv_protocol:I = 0x7f0a16df

.field public static final tv_protocol_1:I = 0x7f0a16e0

.field public static final tv_protocol_2:I = 0x7f0a16e1

.field public static final tv_protocol_compliant_message:I = 0x7f0a16e2

.field public static final tv_protocol_compliant_message_small:I = 0x7f0a16e3

.field public static final tv_protocol_detail:I = 0x7f0a16e4

.field public static final tv_public_link_subtitle:I = 0x7f0a16e5

.field public static final tv_public_link_title:I = 0x7f0a16e6

.field public static final tv_publish:I = 0x7f0a16e7

.field public static final tv_pull:I = 0x7f0a16e8

.field public static final tv_purcahse2:I = 0x7f0a16e9

.field public static final tv_purchase:I = 0x7f0a16ea

.field public static final tv_purchase_cancel:I = 0x7f0a16eb

.field public static final tv_purchase_cn:I = 0x7f0a16ec

.field public static final tv_purchase_item:I = 0x7f0a16ed

.field public static final tv_purchase_item_desc:I = 0x7f0a16ee

.field public static final tv_purchase_licence:I = 0x7f0a16ef

.field public static final tv_purchase_month:I = 0x7f0a16f0

.field public static final tv_purchase_month_price:I = 0x7f0a16f1

.field public static final tv_purchase_month_sign:I = 0x7f0a16f2

.field public static final tv_purchase_price:I = 0x7f0a16f3

.field public static final tv_purchase_small_screen:I = 0x7f0a16f4

.field public static final tv_purchase_time:I = 0x7f0a16f5

.field public static final tv_purchase_title:I = 0x7f0a16f6

.field public static final tv_purchase_year:I = 0x7f0a16f7

.field public static final tv_purchase_year_price:I = 0x7f0a16f8

.field public static final tv_purchase_ys_sign:I = 0x7f0a16f9

.field public static final tv_pwd_login_over_five_contact_us:I = 0x7f0a16fa

.field public static final tv_pwd_login_over_five_find_pwd:I = 0x7f0a16fb

.field public static final tv_pwd_login_over_five_find_pwd_tip:I = 0x7f0a16fc

.field public static final tv_pwd_login_over_three_contact_us:I = 0x7f0a16fd

.field public static final tv_pwd_login_over_three_find_pwd:I = 0x7f0a16fe

.field public static final tv_pwd_or_verify_code_login:I = 0x7f0a16ff

.field public static final tv_qq:I = 0x7f0a1700

.field public static final tv_qr_code_history_title:I = 0x7f0a1701

.field public static final tv_query_key_upgrade_btn:I = 0x7f0a1702

.field public static final tv_query_key_upgrade_tips:I = 0x7f0a1703

.field public static final tv_question:I = 0x7f0a1704

.field public static final tv_quit:I = 0x7f0a1705

.field public static final tv_radar_status:I = 0x7f0a1706

.field public static final tv_range_title:I = 0x7f0a1707

.field public static final tv_range_value:I = 0x7f0a1708

.field public static final tv_raw_config:I = 0x7f0a1709

.field public static final tv_re_recognition:I = 0x7f0a170a

.field public static final tv_re_search:I = 0x7f0a170b

.field public static final tv_real_size:I = 0x7f0a170c

.field public static final tv_rec_title:I = 0x7f0a170d

.field public static final tv_receive:I = 0x7f0a170e

.field public static final tv_receive_gift:I = 0x7f0a170f

.field public static final tv_receive_success_tip:I = 0x7f0a1710

.field public static final tv_recent_docs:I = 0x7f0a1711

.field public static final tv_recognize_export:I = 0x7f0a1712

.field public static final tv_recognize_state:I = 0x7f0a1713

.field public static final tv_recommend_close:I = 0x7f0a1714

.field public static final tv_recommend_for_you:I = 0x7f0a1715

.field public static final tv_recommend_new:I = 0x7f0a1716

.field public static final tv_recommend_tip_titile:I = 0x7f0a1717

.field public static final tv_recommend_title:I = 0x7f0a1718

.field public static final tv_record_btn:I = 0x7f0a1719

.field public static final tv_recover_signature_file:I = 0x7f0a171a

.field public static final tv_redeem_invite_code:I = 0x7f0a171b

.field public static final tv_redo:I = 0x7f0a171c

.field public static final tv_refresh:I = 0x7f0a171d

.field public static final tv_refuse:I = 0x7f0a171e

.field public static final tv_register:I = 0x7f0a171f

.field public static final tv_register_account:I = 0x7f0a1720

.field public static final tv_register_error:I = 0x7f0a1721

.field public static final tv_register_new_account:I = 0x7f0a1722

.field public static final tv_register_protocol:I = 0x7f0a1723

.field public static final tv_register_pwd:I = 0x7f0a1724

.field public static final tv_register_pwd_require:I = 0x7f0a1725

.field public static final tv_register_title:I = 0x7f0a1726

.field public static final tv_rejoin_eight_tip:I = 0x7f0a1727

.field public static final tv_rejoin_eight_vip:I = 0x7f0a1728

.field public static final tv_reload:I = 0x7f0a1729

.field public static final tv_remark:I = 0x7f0a172a

.field public static final tv_remove:I = 0x7f0a172b

.field public static final tv_remove_btn:I = 0x7f0a172c

.field public static final tv_remove_moire:I = 0x7f0a172d

.field public static final tv_remove_water_mark:I = 0x7f0a172e

.field public static final tv_remove_watermark:I = 0x7f0a172f

.field public static final tv_rename:I = 0x7f0a1730

.field public static final tv_rename_error_msg:I = 0x7f0a1731

.field public static final tv_request:I = 0x7f0a1732

.field public static final tv_reset:I = 0x7f0a1733

.field public static final tv_reset_deviceid:I = 0x7f0a1734

.field public static final tv_reset_doc_version:I = 0x7f0a1735

.field public static final tv_reset_image_status:I = 0x7f0a1736

.field public static final tv_reset_pwd:I = 0x7f0a1737

.field public static final tv_reset_pwd_content:I = 0x7f0a1738

.field public static final tv_reset_pwd_error_msg:I = 0x7f0a1739

.field public static final tv_reset_pwd_require:I = 0x7f0a173a

.field public static final tv_reset_pwd_title:I = 0x7f0a173b

.field public static final tv_residence_booklet_certificate:I = 0x7f0a173c

.field public static final tv_resort_merged_doc_title:I = 0x7f0a173d

.field public static final tv_resort_merged_docs_done:I = 0x7f0a173e

.field public static final tv_rest_amount:I = 0x7f0a173f

.field public static final tv_restoration_tips:I = 0x7f0a1740

.field public static final tv_restore_never_tip:I = 0x7f0a1741

.field public static final tv_restore_sample:I = 0x7f0a1742

.field public static final tv_restore_title:I = 0x7f0a1743

.field public static final tv_result:I = 0x7f0a1744

.field public static final tv_retake:I = 0x7f0a1745

.field public static final tv_retry_download:I = 0x7f0a1746

.field public static final tv_retry_ocr:I = 0x7f0a1747

.field public static final tv_retry_ocr_hint:I = 0x7f0a1748

.field public static final tv_retry_upload:I = 0x7f0a1749

.field public static final tv_review_state:I = 0x7f0a174a

.field public static final tv_reward_topic:I = 0x7f0a174b

.field public static final tv_reward_transfer_word:I = 0x7f0a174c

.field public static final tv_right:I = 0x7f0a174d

.field public static final tv_right_1:I = 0x7f0a174e

.field public static final tv_right_2:I = 0x7f0a174f

.field public static final tv_right_3:I = 0x7f0a1750

.field public static final tv_right_4:I = 0x7f0a1751

.field public static final tv_right_action:I = 0x7f0a1752

.field public static final tv_right_item_normal_premium_description:I = 0x7f0a1753

.field public static final tv_right_title_fifth:I = 0x7f0a1754

.field public static final tv_right_title_first:I = 0x7f0a1755

.field public static final tv_right_title_fourth:I = 0x7f0a1756

.field public static final tv_right_title_second:I = 0x7f0a1757

.field public static final tv_right_title_third:I = 0x7f0a1758

.field public static final tv_rights_left:I = 0x7f0a1759

.field public static final tv_rights_middle:I = 0x7f0a175a

.field public static final tv_rights_right:I = 0x7f0a175b

.field public static final tv_rule:I = 0x7f0a175c

.field public static final tv_rules_title:I = 0x7f0a175d

.field public static final tv_satis:I = 0x7f0a175e

.field public static final tv_satisfy:I = 0x7f0a175f

.field public static final tv_save:I = 0x7f0a1760

.field public static final tv_save_and_share:I = 0x7f0a1761

.field public static final tv_save_as_default:I = 0x7f0a1762

.field public static final tv_save_new:I = 0x7f0a1763

.field public static final tv_save_path:I = 0x7f0a1764

.field public static final tv_save_result_ok:I = 0x7f0a1765

.field public static final tv_save_result_tip:I = 0x7f0a1766

.field public static final tv_save_screenshot:I = 0x7f0a1767

.field public static final tv_scan:I = 0x7f0a1768

.field public static final tv_scan_doc:I = 0x7f0a1769

.field public static final tv_scan_level:I = 0x7f0a176a

.field public static final tv_scan_now:I = 0x7f0a176b

.field public static final tv_scanning:I = 0x7f0a176c

.field public static final tv_seal:I = 0x7f0a176d

.field public static final tv_search:I = 0x7f0a176e

.field public static final tv_search_all_2:I = 0x7f0a176f

.field public static final tv_search_all_des:I = 0x7f0a1770

.field public static final tv_search_hl:I = 0x7f0a1771

.field public static final tv_search_hl_tag:I = 0x7f0a1772

.field public static final tv_search_inner:I = 0x7f0a1773

.field public static final tv_search_sub_folder_tips:I = 0x7f0a1774

.field public static final tv_search_team_msg:I = 0x7f0a1775

.field public static final tv_search_tips:I = 0x7f0a1776

.field public static final tv_second:I = 0x7f0a1777

.field public static final tv_second_line:I = 0x7f0a1778

.field public static final tv_second_page:I = 0x7f0a1779

.field public static final tv_security_color:I = 0x7f0a177a

.field public static final tv_security_tip:I = 0x7f0a177b

.field public static final tv_see:I = 0x7f0a177c

.field public static final tv_seekbar_title:I = 0x7f0a177d

.field public static final tv_select:I = 0x7f0a177e

.field public static final tv_select_all:I = 0x7f0a177f

.field public static final tv_select_and_drag_num:I = 0x7f0a1780

.field public static final tv_select_invite:I = 0x7f0a1781

.field public static final tv_select_num:I = 0x7f0a1782

.field public static final tv_select_number:I = 0x7f0a1783

.field public static final tv_select_ocr_content:I = 0x7f0a1784

.field public static final tv_select_sign_type:I = 0x7f0a1785

.field public static final tv_select_title:I = 0x7f0a1786

.field public static final tv_select_toolbar_num:I = 0x7f0a1787

.field public static final tv_selected:I = 0x7f0a1788

.field public static final tv_self_account_label:I = 0x7f0a1789

.field public static final tv_self_account_value:I = 0x7f0a178a

.field public static final tv_self_accounte_no:I = 0x7f0a178b

.field public static final tv_self_name_label:I = 0x7f0a178c

.field public static final tv_send_text:I = 0x7f0a178d

.field public static final tv_send_word:I = 0x7f0a178e

.field public static final tv_sensitive_warning:I = 0x7f0a178f

.field public static final tv_set:I = 0x7f0a1790

.field public static final tv_set_config:I = 0x7f0a1791

.field public static final tv_set_hint:I = 0x7f0a1792

.field public static final tv_set_pwd:I = 0x7f0a1793

.field public static final tv_set_pwd_error:I = 0x7f0a1794

.field public static final tv_set_pwd_intro:I = 0x7f0a1795

.field public static final tv_set_pwd_login:I = 0x7f0a1796

.field public static final tv_set_pwd_require:I = 0x7f0a1797

.field public static final tv_set_tag:I = 0x7f0a1798

.field public static final tv_setting:I = 0x7f0a1799

.field public static final tv_setting_line_subtitle:I = 0x7f0a179a

.field public static final tv_setting_line_title:I = 0x7f0a179b

.field public static final tv_setting_my_account_login_or_out:I = 0x7f0a179c

.field public static final tv_setting_pwd_error_msg:I = 0x7f0a179d

.field public static final tv_setting_pwd_title:I = 0x7f0a179e

.field public static final tv_setting_right_txt_line_right_title:I = 0x7f0a179f

.field public static final tv_setting_right_txt_line_subtitle:I = 0x7f0a17a0

.field public static final tv_setting_right_txt_line_subtitle2:I = 0x7f0a17a1

.field public static final tv_setting_right_txt_line_title:I = 0x7f0a17a2

.field public static final tv_setting_right_txt_line_title_label:I = 0x7f0a17a3

.field public static final tv_setting_right_txt_line_title_red_dot:I = 0x7f0a17a4

.field public static final tv_setting_title:I = 0x7f0a17a5

.field public static final tv_share:I = 0x7f0a17a6

.field public static final tv_share_1:I = 0x7f0a17a7

.field public static final tv_share_2:I = 0x7f0a17a8

.field public static final tv_share_channel:I = 0x7f0a17a9

.field public static final tv_share_count:I = 0x7f0a17aa

.field public static final tv_share_desc:I = 0x7f0a17ab

.field public static final tv_share_dialog_pdf_share_limit:I = 0x7f0a17ac

.field public static final tv_share_dialog_pdf_share_limit_btn:I = 0x7f0a17ad

.field public static final tv_share_dialog_pdf_share_limit_layout:I = 0x7f0a17ae

.field public static final tv_share_dialog_preview:I = 0x7f0a17af

.field public static final tv_share_dialog_remove:I = 0x7f0a17b0

.field public static final tv_share_dialog_title:I = 0x7f0a17b1

.field public static final tv_share_dialog_watermark_desc:I = 0x7f0a17b2

.field public static final tv_share_dir_guide_dialog_create:I = 0x7f0a17b3

.field public static final tv_share_dir_guide_dialog_intro:I = 0x7f0a17b4

.field public static final tv_share_dir_subtitle:I = 0x7f0a17b5

.field public static final tv_share_dir_title:I = 0x7f0a17b6

.field public static final tv_share_doc:I = 0x7f0a17b7

.field public static final tv_share_done:I = 0x7f0a17b8

.field public static final tv_share_image_no_water_mark:I = 0x7f0a17b9

.field public static final tv_share_image_water_mark:I = 0x7f0a17ba

.field public static final tv_share_link_grid_image_name:I = 0x7f0a17bb

.field public static final tv_share_link_hor_list_image_name:I = 0x7f0a17bc

.field public static final tv_share_link_list_image_des:I = 0x7f0a17bd

.field public static final tv_share_link_list_image_name:I = 0x7f0a17be

.field public static final tv_share_link_list_image_size:I = 0x7f0a17bf

.field public static final tv_share_link_title:I = 0x7f0a17c0

.field public static final tv_share_other:I = 0x7f0a17c1

.field public static final tv_share_pdf:I = 0x7f0a17c2

.field public static final tv_share_pdf_desc:I = 0x7f0a17c3

.field public static final tv_share_pdf_no_mark_count:I = 0x7f0a17c4

.field public static final tv_share_special:I = 0x7f0a17c5

.field public static final tv_share_text:I = 0x7f0a17c6

.field public static final tv_share_title:I = 0x7f0a17c7

.field public static final tv_share_to_other:I = 0x7f0a17c8

.field public static final tv_share_to_wehcat_desc:I = 0x7f0a17c9

.field public static final tv_shared_dir_members:I = 0x7f0a17ca

.field public static final tv_show_composite:I = 0x7f0a17cb

.field public static final tv_show_function:I = 0x7f0a17cc

.field public static final tv_show_origin:I = 0x7f0a17cd

.field public static final tv_show_snack_bar:I = 0x7f0a17ce

.field public static final tv_show_thumb:I = 0x7f0a17cf

.field public static final tv_sig_actionbar_right:I = 0x7f0a17d0

.field public static final tv_sign:I = 0x7f0a17d1

.field public static final tv_sign_date:I = 0x7f0a17d2

.field public static final tv_sign_group_area_hint:I = 0x7f0a17d3

.field public static final tv_sign_owner:I = 0x7f0a17d4

.field public static final tv_sign_promoter:I = 0x7f0a17d5

.field public static final tv_sign_status:I = 0x7f0a17d6

.field public static final tv_signature_desc:I = 0x7f0a17d7

.field public static final tv_signature_guide_text:I = 0x7f0a17d8

.field public static final tv_signature_now:I = 0x7f0a17d9

.field public static final tv_signby_others:I = 0x7f0a17da

.field public static final tv_signby_self:I = 0x7f0a17db

.field public static final tv_signby_self_and_others:I = 0x7f0a17dc

.field public static final tv_single_mode:I = 0x7f0a17dd

.field public static final tv_single_text:I = 0x7f0a17de

.field public static final tv_size:I = 0x7f0a17df

.field public static final tv_size_1:I = 0x7f0a17e0

.field public static final tv_size_2:I = 0x7f0a17e1

.field public static final tv_size_3:I = 0x7f0a17e2

.field public static final tv_size_deep_clean:I = 0x7f0a17e3

.field public static final tv_size_des:I = 0x7f0a17e4

.field public static final tv_size_scan_state:I = 0x7f0a17e5

.field public static final tv_size_text:I = 0x7f0a17e6

.field public static final tv_size_title:I = 0x7f0a17e7

.field public static final tv_size_value:I = 0x7f0a17e8

.field public static final tv_skip:I = 0x7f0a17e9

.field public static final tv_skip_ad:I = 0x7f0a17ea

.field public static final tv_skip_demo:I = 0x7f0a17eb

.field public static final tv_skip_purchase:I = 0x7f0a17ec

.field public static final tv_small_image_name:I = 0x7f0a17ed

.field public static final tv_small_size:I = 0x7f0a17ee

.field public static final tv_smart_erase_intro:I = 0x7f0a17ef

.field public static final tv_sort_item:I = 0x7f0a17f0

.field public static final tv_squared_label:I = 0x7f0a17f1

.field public static final tv_start_back_up:I = 0x7f0a17f2

.field public static final tv_start_composite:I = 0x7f0a17f3

.field public static final tv_start_directly:I = 0x7f0a17f4

.field public static final tv_start_restore_capture:I = 0x7f0a17f5

.field public static final tv_start_sync:I = 0x7f0a17f6

.field public static final tv_start_trial:I = 0x7f0a17f7

.field public static final tv_start_use:I = 0x7f0a17f8

.field public static final tv_start_using:I = 0x7f0a17f9

.field public static final tv_start_with_limited:I = 0x7f0a17fa

.field public static final tv_status:I = 0x7f0a17fb

.field public static final tv_step1:I = 0x7f0a17fc

.field public static final tv_step2:I = 0x7f0a17fd

.field public static final tv_step3:I = 0x7f0a17fe

.field public static final tv_step_des1:I = 0x7f0a17ff

.field public static final tv_step_des2:I = 0x7f0a1800

.field public static final tv_step_des3:I = 0x7f0a1801

.field public static final tv_step_two_scan_qrcode:I = 0x7f0a1802

.field public static final tv_style1:I = 0x7f0a1803

.field public static final tv_style2:I = 0x7f0a1804

.field public static final tv_style3:I = 0x7f0a1805

.field public static final tv_sub:I = 0x7f0a1806

.field public static final tv_sub1:I = 0x7f0a1807

.field public static final tv_sub11:I = 0x7f0a1808

.field public static final tv_sub2:I = 0x7f0a1809

.field public static final tv_sub22:I = 0x7f0a180a

.field public static final tv_sub_button:I = 0x7f0a180b

.field public static final tv_sub_des:I = 0x7f0a180c

.field public static final tv_sub_detail_pdf_tools:I = 0x7f0a180d

.field public static final tv_sub_detail_remove_watermark:I = 0x7f0a180e

.field public static final tv_sub_tips:I = 0x7f0a180f

.field public static final tv_sub_title:I = 0x7f0a1810

.field public static final tv_subject:I = 0x7f0a1811

.field public static final tv_submit:I = 0x7f0a1812

.field public static final tv_subscription:I = 0x7f0a1813

.field public static final tv_subscription_name:I = 0x7f0a1814

.field public static final tv_subtitle:I = 0x7f0a1815

.field public static final tv_subtitle_for_not_downloaded_image:I = 0x7f0a1816

.field public static final tv_subtitle_g2:I = 0x7f0a1817

.field public static final tv_subtitle_movecopy:I = 0x7f0a1818

.field public static final tv_success:I = 0x7f0a1819

.field public static final tv_summary:I = 0x7f0a181a

.field public static final tv_super_vcode_over_five_contact_us:I = 0x7f0a181b

.field public static final tv_super_vcode_over_there_contact_us:I = 0x7f0a181c

.field public static final tv_super_vcode_validate_error_msg:I = 0x7f0a181d

.field public static final tv_sure:I = 0x7f0a181e

.field public static final tv_sync:I = 0x7f0a181f

.field public static final tv_sync_des:I = 0x7f0a1820

.field public static final tv_sync_detail:I = 0x7f0a1821

.field public static final tv_sync_mode:I = 0x7f0a1822

.field public static final tv_sync_now:I = 0x7f0a1823

.field public static final tv_sync_percent:I = 0x7f0a1824

.field public static final tv_sync_state:I = 0x7f0a1825

.field public static final tv_sync_subtitle:I = 0x7f0a1826

.field public static final tv_sync_tips:I = 0x7f0a1827

.field public static final tv_sync_tips_msg:I = 0x7f0a1828

.field public static final tv_sync_title:I = 0x7f0a1829

.field public static final tv_tab:I = 0x7f0a182a

.field public static final tv_tab1:I = 0x7f0a182b

.field public static final tv_tab2:I = 0x7f0a182c

.field public static final tv_tab3:I = 0x7f0a182d

.field public static final tv_tab_end:I = 0x7f0a182e

.field public static final tv_tab_start:I = 0x7f0a182f

.field public static final tv_tab_title:I = 0x7f0a1830

.field public static final tv_tag:I = 0x7f0a1831

.field public static final tv_tag_group_title:I = 0x7f0a1832

.field public static final tv_tag_high_tech:I = 0x7f0a1833

.field public static final tv_tag_item_num:I = 0x7f0a1834

.field public static final tv_tag_item_title:I = 0x7f0a1835

.field public static final tv_tag_manage:I = 0x7f0a1836

.field public static final tv_tag_name:I = 0x7f0a1837

.field public static final tv_tag_number:I = 0x7f0a1838

.field public static final tv_tag_select:I = 0x7f0a1839

.field public static final tv_tag_title:I = 0x7f0a183a

.field public static final tv_take_reward_topic:I = 0x7f0a183b

.field public static final tv_take_reward_transfer_word:I = 0x7f0a183c

.field public static final tv_target_position:I = 0x7f0a183d

.field public static final tv_team_empty_invite:I = 0x7f0a183e

.field public static final tv_team_empty_line_divider:I = 0x7f0a183f

.field public static final tv_team_empty_line_one:I = 0x7f0a1840

.field public static final tv_team_empty_line_one_msg:I = 0x7f0a1841

.field public static final tv_team_empty_line_two:I = 0x7f0a1842

.field public static final tv_team_empty_line_two_msg:I = 0x7f0a1843

.field public static final tv_team_empty_show_one:I = 0x7f0a1844

.field public static final tv_team_expire_tips:I = 0x7f0a1845

.field public static final tv_tequan:I = 0x7f0a1846

.field public static final tv_test:I = 0x7f0a1847

.field public static final tv_text:I = 0x7f0a1848

.field public static final tv_text_length:I = 0x7f0a1849

.field public static final tv_thanks:I = 0x7f0a184a

.field public static final tv_third:I = 0x7f0a184b

.field public static final tv_third_desc:I = 0x7f0a184c

.field public static final tv_third_desc_sub:I = 0x7f0a184d

.field public static final tv_third_name:I = 0x7f0a184e

.field public static final tv_third_name_sub:I = 0x7f0a184f

.field public static final tv_third_net_disk_head_subtitle:I = 0x7f0a1850

.field public static final tv_third_net_disk_title:I = 0x7f0a1851

.field public static final tv_third_netdisk_head_subtitle:I = 0x7f0a1852

.field public static final tv_third_netdisk_head_title:I = 0x7f0a1853

.field public static final tv_three_text:I = 0x7f0a1854

.field public static final tv_thumb:I = 0x7f0a1855

.field public static final tv_thumb_num:I = 0x7f0a1856

.field public static final tv_time:I = 0x7f0a1857

.field public static final tv_time_0:I = 0x7f0a1858

.field public static final tv_time_1:I = 0x7f0a1859

.field public static final tv_time_2:I = 0x7f0a185a

.field public static final tv_time_3:I = 0x7f0a185b

.field public static final tv_time_line_folder_guide:I = 0x7f0a185c

.field public static final tv_time_page_split:I = 0x7f0a185d

.field public static final tv_time_tips:I = 0x7f0a185e

.field public static final tv_tip:I = 0x7f0a185f

.field public static final tv_tip_1:I = 0x7f0a1860

.field public static final tv_tip_2:I = 0x7f0a1861

.field public static final tv_tip_desc:I = 0x7f0a1862

.field public static final tv_tip_msg:I = 0x7f0a1863

.field public static final tv_tip_name_wrong:I = 0x7f0a1864

.field public static final tv_tip_new_document:I = 0x7f0a1865

.field public static final tv_tip_warning:I = 0x7f0a1866

.field public static final tv_tips:I = 0x7f0a1867

.field public static final tv_tips_1:I = 0x7f0a1868

.field public static final tv_tips_2:I = 0x7f0a1869

.field public static final tv_tips_alpha:I = 0x7f0a186a

.field public static final tv_tips_barcode_content:I = 0x7f0a186b

.field public static final tv_tips_content:I = 0x7f0a186c

.field public static final tv_tips_hint:I = 0x7f0a186d

.field public static final tv_tips_jigsaw:I = 0x7f0a186e

.field public static final tv_tips_msg_1:I = 0x7f0a186f

.field public static final tv_tips_msg_2:I = 0x7f0a1870

.field public static final tv_tips_name:I = 0x7f0a1871

.field public static final tv_tips_network:I = 0x7f0a1872

.field public static final tv_tips_one:I = 0x7f0a1873

.field public static final tv_tips_share:I = 0x7f0a1874

.field public static final tv_tips_size:I = 0x7f0a1875

.field public static final tv_tips_title:I = 0x7f0a1876

.field public static final tv_tips_two:I = 0x7f0a1877

.field public static final tv_tips_type:I = 0x7f0a1878

.field public static final tv_tips_value:I = 0x7f0a1879

.field public static final tv_titile:I = 0x7f0a187a

.field public static final tv_title:I = 0x7f0a187b

.field public static final tv_title02:I = 0x7f0a187c

.field public static final tv_title_0:I = 0x7f0a187d

.field public static final tv_title_1:I = 0x7f0a187e

.field public static final tv_title_2:I = 0x7f0a187f

.field public static final tv_title_3:I = 0x7f0a1880

.field public static final tv_title_bind:I = 0x7f0a1881

.field public static final tv_title_coupon_price:I = 0x7f0a1882

.field public static final tv_title_dec:I = 0x7f0a1883

.field public static final tv_title_des:I = 0x7f0a1884

.field public static final tv_title_desc:I = 0x7f0a1885

.field public static final tv_title_dir:I = 0x7f0a1886

.field public static final tv_title_enterprise_mall:I = 0x7f0a1887

.field public static final tv_title_g2:I = 0x7f0a1888

.field public static final tv_title_hint:I = 0x7f0a1889

.field public static final tv_title_left:I = 0x7f0a188a

.field public static final tv_title_login:I = 0x7f0a188b

.field public static final tv_title_movecopy:I = 0x7f0a188c

.field public static final tv_title_newbie_scan_reward:I = 0x7f0a188d

.field public static final tv_title_newbie_scan_reward_select:I = 0x7f0a188e

.field public static final tv_title_right:I = 0x7f0a188f

.field public static final tv_title_show:I = 0x7f0a1890

.field public static final tv_title_tips:I = 0x7f0a1891

.field public static final tv_tittle:I = 0x7f0a1892

.field public static final tv_to:I = 0x7f0a1893

.field public static final tv_to_buy:I = 0x7f0a1894

.field public static final tv_to_buy_label:I = 0x7f0a1895

.field public static final tv_to_experience:I = 0x7f0a1896

.field public static final tv_to_next:I = 0x7f0a1897

.field public static final tv_to_other_type:I = 0x7f0a1898

.field public static final tv_to_recognize:I = 0x7f0a1899

.field public static final tv_to_see:I = 0x7f0a189a

.field public static final tv_to_setting:I = 0x7f0a189b

.field public static final tv_to_use:I = 0x7f0a189c

.field public static final tv_to_watermark_title:I = 0x7f0a189d

.field public static final tv_to_word:I = 0x7f0a189e

.field public static final tv_to_word_subtitle:I = 0x7f0a189f

.field public static final tv_toast:I = 0x7f0a18a0

.field public static final tv_todo:I = 0x7f0a18a1

.field public static final tv_tool_page_circle_cell_image_desc:I = 0x7f0a18a2

.field public static final tv_tool_page_square_cell_image_desc:I = 0x7f0a18a3

.field public static final tv_tool_page_title:I = 0x7f0a18a4

.field public static final tv_top_des:I = 0x7f0a18a5

.field public static final tv_top_desc:I = 0x7f0a18a6

.field public static final tv_top_directory:I = 0x7f0a18a7

.field public static final tv_top_hint:I = 0x7f0a18a8

.field public static final tv_top_left:I = 0x7f0a18a9

.field public static final tv_top_middle:I = 0x7f0a18aa

.field public static final tv_top_right:I = 0x7f0a18ab

.field public static final tv_top_search:I = 0x7f0a18ac

.field public static final tv_top_subtitle:I = 0x7f0a18ad

.field public static final tv_top_title:I = 0x7f0a18ae

.field public static final tv_topic_black_and_white:I = 0x7f0a18af

.field public static final tv_topic_page:I = 0x7f0a18b0

.field public static final tv_topic_paper:I = 0x7f0a18b1

.field public static final tv_topic_paper_desc:I = 0x7f0a18b2

.field public static final tv_topic_question:I = 0x7f0a18b3

.field public static final tv_topic_restoration:I = 0x7f0a18b4

.field public static final tv_torch:I = 0x7f0a18b5

.field public static final tv_total:I = 0x7f0a18b6

.field public static final tv_total_height:I = 0x7f0a18b7

.field public static final tv_tp2_left_one_right_two_left:I = 0x7f0a18b8

.field public static final tv_tp2_left_one_right_two_right_bottom:I = 0x7f0a18b9

.field public static final tv_tp2_left_one_right_two_right_top:I = 0x7f0a18ba

.field public static final tv_tp2_left_two_right_one_left:I = 0x7f0a18bb

.field public static final tv_tp2_left_two_right_one_right_bottom:I = 0x7f0a18bc

.field public static final tv_tp2_left_two_right_one_right_top:I = 0x7f0a18bd

.field public static final tv_tp2_recent_first:I = 0x7f0a18be

.field public static final tv_tp2_recent_fourth:I = 0x7f0a18bf

.field public static final tv_tp2_recent_second:I = 0x7f0a18c0

.field public static final tv_tp2_recent_third:I = 0x7f0a18c1

.field public static final tv_tp2_top_three_bottom_one_bottom:I = 0x7f0a18c2

.field public static final tv_tp2_top_three_bottom_one_left:I = 0x7f0a18c3

.field public static final tv_tp2_top_three_bottom_one_middle:I = 0x7f0a18c4

.field public static final tv_tp2_top_three_bottom_one_right:I = 0x7f0a18c5

.field public static final tv_tp_2_square_bottom_left:I = 0x7f0a18c6

.field public static final tv_tp_2_square_bottom_right:I = 0x7f0a18c7

.field public static final tv_tp_2_square_top_left:I = 0x7f0a18c8

.field public static final tv_tp_2_square_top_right:I = 0x7f0a18c9

.field public static final tv_trade_amount:I = 0x7f0a18ca

.field public static final tv_transform:I = 0x7f0a18cb

.field public static final tv_translate:I = 0x7f0a18cc

.field public static final tv_translate_from_lang:I = 0x7f0a18cd

.field public static final tv_translate_language:I = 0x7f0a18ce

.field public static final tv_translate_to_lang:I = 0x7f0a18cf

.field public static final tv_trial_renew_bottom_desc:I = 0x7f0a18d0

.field public static final tv_trial_renew_subtitle:I = 0x7f0a18d1

.field public static final tv_try_desc:I = 0x7f0a18d2

.field public static final tv_two_text:I = 0x7f0a18d3

.field public static final tv_type:I = 0x7f0a18d4

.field public static final tv_un_login_content:I = 0x7f0a18d5

.field public static final tv_un_sync_docs:I = 0x7f0a18d6

.field public static final tv_undo:I = 0x7f0a18d7

.field public static final tv_unreviewed_desc:I = 0x7f0a18d8

.field public static final tv_unsatis:I = 0x7f0a18d9

.field public static final tv_unsync_doc:I = 0x7f0a18da

.field public static final tv_untodo:I = 0x7f0a18db

.field public static final tv_update:I = 0x7f0a18dc

.field public static final tv_upgrade:I = 0x7f0a18dd

.field public static final tv_upgrade_btn:I = 0x7f0a18de

.field public static final tv_upgrade_now:I = 0x7f0a18df

.field public static final tv_upgrade_tips:I = 0x7f0a18e0

.field public static final tv_upgrade_to_premium:I = 0x7f0a18e1

.field public static final tv_upload_member:I = 0x7f0a18e2

.field public static final tv_use:I = 0x7f0a18e3

.field public static final tv_use_it:I = 0x7f0a18e4

.field public static final tv_use_mobile_subtitle:I = 0x7f0a18e5

.field public static final tv_use_mobile_title:I = 0x7f0a18e6

.field public static final tv_use_point:I = 0x7f0a18e7

.field public static final tv_use_point_tips:I = 0x7f0a18e8

.field public static final tv_use_tips:I = 0x7f0a18e9

.field public static final tv_user_agreement_des:I = 0x7f0a18ea

.field public static final tv_user_name:I = 0x7f0a18eb

.field public static final tv_user_nickname:I = 0x7f0a18ec

.field public static final tv_user_sign_status:I = 0x7f0a18ed

.field public static final tv_value:I = 0x7f0a18ee

.field public static final tv_value1:I = 0x7f0a18ef

.field public static final tv_value10:I = 0x7f0a18f0

.field public static final tv_value2:I = 0x7f0a18f1

.field public static final tv_value3:I = 0x7f0a18f2

.field public static final tv_value4:I = 0x7f0a18f3

.field public static final tv_value5:I = 0x7f0a18f4

.field public static final tv_value6:I = 0x7f0a18f5

.field public static final tv_value7:I = 0x7f0a18f6

.field public static final tv_value8:I = 0x7f0a18f7

.field public static final tv_value9:I = 0x7f0a18f8

.field public static final tv_value_pair_enhance_line_use_bm:I = 0x7f0a18f9

.field public static final tv_verify_bind:I = 0x7f0a18fa

.field public static final tv_verify_code_1:I = 0x7f0a18fb

.field public static final tv_verify_code_2:I = 0x7f0a18fc

.field public static final tv_verify_code_3:I = 0x7f0a18fd

.field public static final tv_verify_code_4:I = 0x7f0a18fe

.field public static final tv_verify_code_5:I = 0x7f0a18ff

.field public static final tv_verify_code_6:I = 0x7f0a1900

.field public static final tv_verify_code_check_activate_mail:I = 0x7f0a1901

.field public static final tv_verify_code_check_email:I = 0x7f0a1902

.field public static final tv_verify_code_check_email_hint:I = 0x7f0a1903

.field public static final tv_verify_code_contact_us:I = 0x7f0a1904

.field public static final tv_verify_code_error:I = 0x7f0a1905

.field public static final tv_verify_code_error_msg:I = 0x7f0a1906

.field public static final tv_verify_code_input_title:I = 0x7f0a1907

.field public static final tv_verify_code_login:I = 0x7f0a1908

.field public static final tv_verify_code_none_other_login_way:I = 0x7f0a1909

.field public static final tv_verify_code_none_receive_contact_us:I = 0x7f0a190a

.field public static final tv_verify_code_none_receive_use_super_verify_code:I = 0x7f0a190b

.field public static final tv_verify_code_not_receive:I = 0x7f0a190c

.field public static final tv_verify_code_register_account:I = 0x7f0a190d

.field public static final tv_verify_code_resend:I = 0x7f0a190e

.field public static final tv_verify_code_send_to:I = 0x7f0a190f

.field public static final tv_verify_count_down:I = 0x7f0a1910

.field public static final tv_verify_error:I = 0x7f0a1911

.field public static final tv_verify_intro:I = 0x7f0a1912

.field public static final tv_verify_new_email_email:I = 0x7f0a1913

.field public static final tv_verify_new_email_error_msg:I = 0x7f0a1914

.field public static final tv_verify_new_phone_area_code:I = 0x7f0a1915

.field public static final tv_verify_new_phone_area_code_name:I = 0x7f0a1916

.field public static final tv_verify_new_phone_error_msg:I = 0x7f0a1917

.field public static final tv_verify_old_account_account:I = 0x7f0a1918

.field public static final tv_verify_old_account_contact_us:I = 0x7f0a1919

.field public static final tv_verify_old_account_error_msg:I = 0x7f0a191a

.field public static final tv_version:I = 0x7f0a191b

.field public static final tv_video_vip_desc:I = 0x7f0a191c

.field public static final tv_view_all:I = 0x7f0a191d

.field public static final tv_view_folder:I = 0x7f0a191e

.field public static final tv_vip:I = 0x7f0a191f

.field public static final tv_vip_back_label_title:I = 0x7f0a1920

.field public static final tv_vip_des:I = 0x7f0a1921

.field public static final tv_vip_desc:I = 0x7f0a1922

.field public static final tv_vip_gift:I = 0x7f0a1923

.field public static final tv_vip_level:I = 0x7f0a1924

.field public static final tv_vip_level_detail:I = 0x7f0a1925

.field public static final tv_vip_level_subtitle:I = 0x7f0a1926

.field public static final tv_vip_level_title:I = 0x7f0a1927

.field public static final tv_vip_level_value:I = 0x7f0a1928

.field public static final tv_vip_only_tips:I = 0x7f0a1929

.field public static final tv_vip_right:I = 0x7f0a192a

.field public static final tv_vip_right_count:I = 0x7f0a192b

.field public static final tv_vip_right_cycle:I = 0x7f0a192c

.field public static final tv_vip_right_title:I = 0x7f0a192d

.field public static final tv_vip_rights_label_title:I = 0x7f0a192e

.field public static final tv_warning:I = 0x7f0a192f

.field public static final tv_warning_message:I = 0x7f0a1930

.field public static final tv_watch_ad:I = 0x7f0a1931

.field public static final tv_watch_ad_btn:I = 0x7f0a1932

.field public static final tv_watch_video_des:I = 0x7f0a1933

.field public static final tv_watermark:I = 0x7f0a1934

.field public static final tv_watermark_desc:I = 0x7f0a1935

.field public static final tv_web_url_test_add_cs_suffix:I = 0x7f0a1936

.field public static final tv_web_url_test_clear_url:I = 0x7f0a1937

.field public static final tv_web_url_test_open_cs_web_page:I = 0x7f0a1938

.field public static final tv_web_url_test_remove_cs_subfix:I = 0x7f0a1939

.field public static final tv_webview_menu_url_source:I = 0x7f0a193a

.field public static final tv_weixin_friends:I = 0x7f0a193b

.field public static final tv_welcome_back:I = 0x7f0a193c

.field public static final tv_whitelist_title:I = 0x7f0a193d

.field public static final tv_widget_search_certificate:I = 0x7f0a193e

.field public static final tv_widget_search_multi:I = 0x7f0a193f

.field public static final tv_widget_search_ocr:I = 0x7f0a1940

.field public static final tv_widget_search_single:I = 0x7f0a1941

.field public static final tv_wifi:I = 0x7f0a1942

.field public static final tv_word:I = 0x7f0a1943

.field public static final tv_word_list_item:I = 0x7f0a1944

.field public static final tv_word_subtitle:I = 0x7f0a1945

.field public static final tv_work_flow_file_naming_subtitle:I = 0x7f0a1946

.field public static final tv_work_flow_file_naming_title:I = 0x7f0a1947

.field public static final tv_workflow_add_email:I = 0x7f0a1948

.field public static final tv_workflow_done:I = 0x7f0a1949

.field public static final tv_workflow_edit:I = 0x7f0a194a

.field public static final tv_workflow_email_one:I = 0x7f0a194b

.field public static final tv_workflow_email_one_resend:I = 0x7f0a194c

.field public static final tv_workflow_email_one_validate:I = 0x7f0a194d

.field public static final tv_workflow_explanation:I = 0x7f0a194e

.field public static final tv_workflow_explanation2:I = 0x7f0a194f

.field public static final tv_workflow_explanation_desc:I = 0x7f0a1950

.field public static final tv_workflow_explanation_desc2:I = 0x7f0a1951

.field public static final tv_wx:I = 0x7f0a1952

.field public static final tv_wx_share_desc:I = 0x7f0a1953

.field public static final tv_wx_share_title:I = 0x7f0a1954

.field public static final tv_xin_chun_xiao_shi:I = 0x7f0a1955

.field public static final tv_xin_chun_xiao_shi_bottom:I = 0x7f0a1956

.field public static final tv_year_card_title:I = 0x7f0a1957

.field public static final twitterFollowTextView:I = 0x7f0a1958

.field public static final txt_activate_device_id:I = 0x7f0a1959

.field public static final txt_add_tag:I = 0x7f0a195a

.field public static final txt_advice:I = 0x7f0a195b

.field public static final txt_country:I = 0x7f0a195c

.field public static final txt_creat_pd_ag:I = 0x7f0a195d

.field public static final txt_create_pd:I = 0x7f0a195e

.field public static final txt_decode_pd:I = 0x7f0a195f

.field public static final txt_dlg_msg:I = 0x7f0a1960

.field public static final txt_halfpack_title:I = 0x7f0a1961

.field public static final txt_no_task:I = 0x7f0a1962

.field public static final txt_number:I = 0x7f0a1963

.field public static final txt_pageitem_img_size:I = 0x7f0a1964

.field public static final txt_pageitem_modified_time:I = 0x7f0a1965

.field public static final txt_pagelist_page_name:I = 0x7f0a1966

.field public static final txt_warn:I = 0x7f0a1967

.field public static final type_inch:I = 0x7f0a1968

.field public static final type_milli:I = 0x7f0a1969

.field public static final type_point:I = 0x7f0a196a

.field public static final unchecked:I = 0x7f0a196b

.field public static final understand_and_confirm_button:I = 0x7f0a196c

.field public static final uniform:I = 0x7f0a196d

.field public static final unknown:I = 0x7f0a196e

.field public static final unlabeled:I = 0x7f0a196f

.field public static final unlock:I = 0x7f0a1970

.field public static final up:I = 0x7f0a1971

.field public static final update_account:I = 0x7f0a1972

.field public static final update_time:I = 0x7f0a1973

.field public static final upload_file_name:I = 0x7f0a1974

.field public static final upload_file_size:I = 0x7f0a1975

.field public static final upload_file_state:I = 0x7f0a1976

.field public static final upload_progress_bar:I = 0x7f0a1977

.field public static final upload_site_icon:I = 0x7f0a1978

.field public static final uptodown:I = 0x7f0a1979

.field public static final url:I = 0x7f0a197a

.field public static final useLogo:I = 0x7f0a197b

.field public static final user_edit:I = 0x7f0a197c

.field public static final v_add:I = 0x7f0a197d

.field public static final v_add_signature:I = 0x7f0a197e

.field public static final v_backup_dir_line:I = 0x7f0a197f

.field public static final v_bg:I = 0x7f0a1980

.field public static final v_bg_1:I = 0x7f0a1981

.field public static final v_bg_2:I = 0x7f0a1982

.field public static final v_bg_blue_cover:I = 0x7f0a1983

.field public static final v_bg_bottom:I = 0x7f0a1984

.field public static final v_bg_convert_g2:I = 0x7f0a1985

.field public static final v_bg_cover_g2:I = 0x7f0a1986

.field public static final v_bg_text:I = 0x7f0a1987

.field public static final v_bg_top:I = 0x7f0a1988

.field public static final v_boarder:I = 0x7f0a1989

.field public static final v_book_thumb_mask:I = 0x7f0a198a

.field public static final v_border:I = 0x7f0a198b

.field public static final v_bottom:I = 0x7f0a198c

.field public static final v_bottom_bg:I = 0x7f0a198d

.field public static final v_bottom_center:I = 0x7f0a198e

.field public static final v_bottom_gradient_bg:I = 0x7f0a198f

.field public static final v_bottom_mask:I = 0x7f0a1990

.field public static final v_brush_mode:I = 0x7f0a1991

.field public static final v_cancel:I = 0x7f0a1992

.field public static final v_certificate_divider:I = 0x7f0a1993

.field public static final v_change:I = 0x7f0a1994

.field public static final v_checkmask:I = 0x7f0a1995

.field public static final v_chose_mask:I = 0x7f0a1996

.field public static final v_clear_history:I = 0x7f0a1997

.field public static final v_close:I = 0x7f0a1998

.field public static final v_content:I = 0x7f0a1999

.field public static final v_content_bg:I = 0x7f0a199a

.field public static final v_continue:I = 0x7f0a199b

.field public static final v_convert_bg:I = 0x7f0a199c

.field public static final v_copy:I = 0x7f0a199d

.field public static final v_cover_in_edit:I = 0x7f0a199e

.field public static final v_data_show_top_line:I = 0x7f0a199f

.field public static final v_delete:I = 0x7f0a19a0

.field public static final v_discount_bg_1:I = 0x7f0a19a1

.field public static final v_diveder:I = 0x7f0a19a2

.field public static final v_diver:I = 0x7f0a19a3

.field public static final v_divide:I = 0x7f0a19a4

.field public static final v_divider:I = 0x7f0a19a5

.field public static final v_divider_1:I = 0x7f0a19a6

.field public static final v_divider_2:I = 0x7f0a19a7

.field public static final v_divider_check:I = 0x7f0a19a8

.field public static final v_divider_left:I = 0x7f0a19a9

.field public static final v_divider_right:I = 0x7f0a19aa

.field public static final v_dot:I = 0x7f0a19ab

.field public static final v_dropper_free:I = 0x7f0a19ac

.field public static final v_dropper_touch:I = 0x7f0a19ad

.field public static final v_flag:I = 0x7f0a19ae

.field public static final v_focus_bg:I = 0x7f0a19af

.field public static final v_focus_mask:I = 0x7f0a19b0

.field public static final v_grid:I = 0x7f0a19b1

.field public static final v_header_divider:I = 0x7f0a19b2

.field public static final v_history_delete:I = 0x7f0a19b3

.field public static final v_item_me_page_header_bg:I = 0x7f0a19b4

.field public static final v_left_line:I = 0x7f0a19b5

.field public static final v_line:I = 0x7f0a19b6

.field public static final v_line_0:I = 0x7f0a19b7

.field public static final v_line_1:I = 0x7f0a19b8

.field public static final v_line_2:I = 0x7f0a19b9

.field public static final v_lottie_anim:I = 0x7f0a19ba

.field public static final v_main_bg:I = 0x7f0a19bb

.field public static final v_main_content:I = 0x7f0a19bc

.field public static final v_main_content_bg:I = 0x7f0a19bd

.field public static final v_main_view_bg:I = 0x7f0a19be

.field public static final v_mask:I = 0x7f0a19bf

.field public static final v_mask_1:I = 0x7f0a19c0

.field public static final v_mask_2:I = 0x7f0a19c1

.field public static final v_mask_3:I = 0x7f0a19c2

.field public static final v_me_page_area_free_card_vip_bg:I = 0x7f0a19c3

.field public static final v_me_page_card_c:I = 0x7f0a19c4

.field public static final v_me_page_card_floating:I = 0x7f0a19c5

.field public static final v_me_page_card_s_vip:I = 0x7f0a19c6

.field public static final v_me_page_card_vip_bg:I = 0x7f0a19c7

.field public static final v_me_page_vip_card_right:I = 0x7f0a19c8

.field public static final v_media:I = 0x7f0a19c9

.field public static final v_msg_dot:I = 0x7f0a19ca

.field public static final v_office2cloud:I = 0x7f0a19cb

.field public static final v_paging:I = 0x7f0a19cc

.field public static final v_pay_type:I = 0x7f0a19cd

.field public static final v_pen_size:I = 0x7f0a19ce

.field public static final v_photo_divider:I = 0x7f0a19cf

.field public static final v_placeholder:I = 0x7f0a19d0

.field public static final v_product_bg:I = 0x7f0a19d1

.field public static final v_purchase_bg:I = 0x7f0a19d2

.field public static final v_re_translate:I = 0x7f0a19d3

.field public static final v_rectangle_mode:I = 0x7f0a19d4

.field public static final v_resort_merged_docs_cover:I = 0x7f0a19d5

.field public static final v_right_line:I = 0x7f0a19d6

.field public static final v_select:I = 0x7f0a19d7

.field public static final v_select_func:I = 0x7f0a19d8

.field public static final v_setting_right_txt_line_bottom_divider:I = 0x7f0a19d9

.field public static final v_shade:I = 0x7f0a19da

.field public static final v_shadow:I = 0x7f0a19db

.field public static final v_shutter_mode_guide:I = 0x7f0a19dc

.field public static final v_signature_divider:I = 0x7f0a19dd

.field public static final v_split:I = 0x7f0a19de

.field public static final v_tab_mask:I = 0x7f0a19df

.field public static final v_third_net_disk_bottom_line:I = 0x7f0a19e0

.field public static final v_title_bg:I = 0x7f0a19e1

.field public static final v_to_buy:I = 0x7f0a19e2

.field public static final v_to_word:I = 0x7f0a19e3

.field public static final v_top_bg:I = 0x7f0a19e4

.field public static final v_top_divider:I = 0x7f0a19e5

.field public static final v_top_mask:I = 0x7f0a19e6

.field public static final v_top_shadow:I = 0x7f0a19e7

.field public static final v_top_space:I = 0x7f0a19e8

.field public static final v_touch:I = 0x7f0a19e9

.field public static final v_up_down:I = 0x7f0a19ea

.field public static final v_vip_bg:I = 0x7f0a19eb

.field public static final v_vip_card_bg:I = 0x7f0a19ec

.field public static final v_water_spread:I = 0x7f0a19ed

.field public static final v_workflow_email_bottom_line:I = 0x7f0a19ee

.field public static final vc_code:I = 0x7f0a19ef

.field public static final vc_code_verify_code:I = 0x7f0a19f0

.field public static final vcv_verify_code_input:I = 0x7f0a19f1

.field public static final verify_new_password:I = 0x7f0a19f2

.field public static final vertical:I = 0x7f0a19f3

.field public static final verticalScrollView:I = 0x7f0a19f4

.field public static final vertical_guideline:I = 0x7f0a19f5

.field public static final vertical_only:I = 0x7f0a19f6

.field public static final video_decoder_gl_surface_view:I = 0x7f0a19f7

.field public static final video_view:I = 0x7f0a19f8

.field public static final video_view_signature:I = 0x7f0a19f9

.field public static final view:I = 0x7f0a19fa

.field public static final viewPager:I = 0x7f0a19fb

.field public static final view_ai_entrance:I = 0x7f0a19fc

.field public static final view_background:I = 0x7f0a19fd

.field public static final view_bg:I = 0x7f0a19fe

.field public static final view_bg_purchase7:I = 0x7f0a19ff

.field public static final view_bg_purchase8:I = 0x7f0a1a00

.field public static final view_book_tips:I = 0x7f0a1a01

.field public static final view_book_tips_refactor:I = 0x7f0a1a02

.field public static final view_booksplitter:I = 0x7f0a1a03

.field public static final view_bottom:I = 0x7f0a1a04

.field public static final view_bottom_bg:I = 0x7f0a1a05

.field public static final view_bottom_line:I = 0x7f0a1a06

.field public static final view_bottom_menu:I = 0x7f0a1a07

.field public static final view_btn_bg:I = 0x7f0a1a08

.field public static final view_checked_border:I = 0x7f0a1a09

.field public static final view_close:I = 0x7f0a1a0a

.field public static final view_cn_unsubscribe_recall_item:I = 0x7f0a1a0b

.field public static final view_color_mask:I = 0x7f0a1a0c

.field public static final view_container:I = 0x7f0a1a0d

.field public static final view_content:I = 0x7f0a1a0e

.field public static final view_content_op:I = 0x7f0a1a0f

.field public static final view_demo_mask:I = 0x7f0a1a10

.field public static final view_desc_price_bg:I = 0x7f0a1a11

.field public static final view_dialog_result_divider:I = 0x7f0a1a12

.field public static final view_divider:I = 0x7f0a1a13

.field public static final view_divider_auto_backup:I = 0x7f0a1a14

.field public static final view_divider_use_mobile:I = 0x7f0a1a15

.field public static final view_divier:I = 0x7f0a1a16

.field public static final view_doc_margin_folder:I = 0x7f0a1a17

.field public static final view_empty:I = 0x7f0a1a18

.field public static final view_export_all:I = 0x7f0a1a19

.field public static final view_export_table:I = 0x7f0a1a1a

.field public static final view_file_bg:I = 0x7f0a1a1b

.field public static final view_flipper:I = 0x7f0a1a1c

.field public static final view_folder_bg:I = 0x7f0a1a1d

.field public static final view_folder_item_mask:I = 0x7f0a1a1e

.field public static final view_func_divider:I = 0x7f0a1a1f

.field public static final view_gp_guide_mark_new_bg:I = 0x7f0a1a20

.field public static final view_gradient:I = 0x7f0a1a21

.field public static final view_head_cover:I = 0x7f0a1a22

.field public static final view_head_pin:I = 0x7f0a1a23

.field public static final view_header:I = 0x7f0a1a24

.field public static final view_import_image:I = 0x7f0a1a25

.field public static final view_import_pdf:I = 0x7f0a1a26

.field public static final view_indicator:I = 0x7f0a1a27

.field public static final view_item_bg:I = 0x7f0a1a28

.field public static final view_item_mask:I = 0x7f0a1a29

.field public static final view_line:I = 0x7f0a1a2a

.field public static final view_line1:I = 0x7f0a1a2b

.field public static final view_line2:I = 0x7f0a1a2c

.field public static final view_line3:I = 0x7f0a1a2d

.field public static final view_line_btn:I = 0x7f0a1a2e

.field public static final view_line_centerVertical:I = 0x7f0a1a2f

.field public static final view_line_share_wx:I = 0x7f0a1a30

.field public static final view_line_tip_new_document_left:I = 0x7f0a1a31

.field public static final view_line_tip_new_document_right:I = 0x7f0a1a32

.field public static final view_line_tips_share_left:I = 0x7f0a1a33

.field public static final view_line_tips_share_right:I = 0x7f0a1a34

.field public static final view_listen_touch:I = 0x7f0a1a35

.field public static final view_loading_anim:I = 0x7f0a1a36

.field public static final view_main_view_bg:I = 0x7f0a1a37

.field public static final view_mask:I = 0x7f0a1a38

.field public static final view_middle:I = 0x7f0a1a39

.field public static final view_middle_size:I = 0x7f0a1a3a

.field public static final view_offset_helper:I = 0x7f0a1a3b

.field public static final view_ope:I = 0x7f0a1a3c

.field public static final view_original_size:I = 0x7f0a1a3d

.field public static final view_outside:I = 0x7f0a1a3e

.field public static final view_pager:I = 0x7f0a1a3f

.field public static final view_pdf_editing_guide_root:I = 0x7f0a1a40

.field public static final view_pdf_watermark_bottom:I = 0x7f0a1a41

.field public static final view_pdf_watermark_head:I = 0x7f0a1a42

.field public static final view_preference_line:I = 0x7f0a1a43

.field public static final view_radius:I = 0x7f0a1a44

.field public static final view_red_dot:I = 0x7f0a1a45

.field public static final view_ref:I = 0x7f0a1a46

.field public static final view_scan_finish_btn:I = 0x7f0a1a47

.field public static final view_scanner_line:I = 0x7f0a1a48

.field public static final view_separate:I = 0x7f0a1a49

.field public static final view_separator:I = 0x7f0a1a4a

.field public static final view_setting_direction:I = 0x7f0a1a4b

.field public static final view_setting_encrypt:I = 0x7f0a1a4c

.field public static final view_setting_size:I = 0x7f0a1a4d

.field public static final view_shadow_toolbar:I = 0x7f0a1a4e

.field public static final view_share_type_link:I = 0x7f0a1a4f

.field public static final view_share_type_pdf:I = 0x7f0a1a50

.field public static final view_shutter:I = 0x7f0a1a51

.field public static final view_shutter_close:I = 0x7f0a1a52

.field public static final view_small_size:I = 0x7f0a1a53

.field public static final view_smart_erase:I = 0x7f0a1a54

.field public static final view_status:I = 0x7f0a1a55

.field public static final view_status_bar:I = 0x7f0a1a56

.field public static final view_stroke:I = 0x7f0a1a57

.field public static final view_stub_bank_card_paper_thumb:I = 0x7f0a1a58

.field public static final view_stub_bottom:I = 0x7f0a1a59

.field public static final view_stub_bottom_unmodify:I = 0x7f0a1a5a

.field public static final view_stub_certificate_thumb:I = 0x7f0a1a5b

.field public static final view_stub_click_thumb_guide:I = 0x7f0a1a5c

.field public static final view_stub_excel_thumb:I = 0x7f0a1a5d

.field public static final view_stub_filter_pop_guide:I = 0x7f0a1a5e

.field public static final view_stub_ko_agreement:I = 0x7f0a1a5f

.field public static final view_stub_markup_guide:I = 0x7f0a1a60

.field public static final view_stub_merge_guide:I = 0x7f0a1a61

.field public static final view_stub_ocr_thumb:I = 0x7f0a1a62

.field public static final view_stub_pop_all_function_guide:I = 0x7f0a1a63

.field public static final view_stub_pop_guide:I = 0x7f0a1a64

.field public static final view_stub_qr_scan_guide:I = 0x7f0a1a65

.field public static final view_stub_setting_more:I = 0x7f0a1a66

.field public static final view_stub_shutter_guide:I = 0x7f0a1a67

.field public static final view_stub_topic_paper_thumb:I = 0x7f0a1a68

.field public static final view_stub_translate_thumb:I = 0x7f0a1a69

.field public static final view_stub_trim_guide:I = 0x7f0a1a6a

.field public static final view_stub_word_thumb:I = 0x7f0a1a6b

.field public static final view_superfilter_anim:I = 0x7f0a1a6c

.field public static final view_to_return_operation_item:I = 0x7f0a1a6d

.field public static final view_toolbar:I = 0x7f0a1a6e

.field public static final view_top_bg:I = 0x7f0a1a6f

.field public static final view_top_menu_divider:I = 0x7f0a1a70

.field public static final view_transition:I = 0x7f0a1a71

.field public static final view_tree_lifecycle_owner:I = 0x7f0a1a72

.field public static final view_tree_on_back_pressed_dispatcher_owner:I = 0x7f0a1a73

.field public static final view_tree_saved_state_registry_owner:I = 0x7f0a1a74

.field public static final view_tree_view_model_store_owner:I = 0x7f0a1a75

.field public static final view_trim_close:I = 0x7f0a1a76

.field public static final view_type_img:I = 0x7f0a1a77

.field public static final view_type_pdf:I = 0x7f0a1a78

.field public static final view_underline:I = 0x7f0a1a79

.field public static final view_wave:I = 0x7f0a1a7a

.field public static final view_word_content_op:I = 0x7f0a1a7b

.field public static final viewfinder_view:I = 0x7f0a1a7c

.field public static final viewpager:I = 0x7f0a1a7d

.field public static final viewpager2:I = 0x7f0a1a7e

.field public static final viewpager_images:I = 0x7f0a1a7f

.field public static final viewpager_ocr_frame_view:I = 0x7f0a1a80

.field public static final viewpager_top:I = 0x7f0a1a81

.field public static final viewstub_tag:I = 0x7f0a1a82

.field public static final vipIcon:I = 0x7f0a1a83

.field public static final visible:I = 0x7f0a1a84

.field public static final visible_removing_fragment_view_tag:I = 0x7f0a1a85

.field public static final vp_comments:I = 0x7f0a1a86

.field public static final vp_function:I = 0x7f0a1a87

.field public static final vp_gallery:I = 0x7f0a1a88

.field public static final vp_guide_cn_purchase_top_pager:I = 0x7f0a1a89

.field public static final vp_guide_gp_pages:I = 0x7f0a1a8a

.field public static final vp_invoice:I = 0x7f0a1a8b

.field public static final vp_pages:I = 0x7f0a1a8c

.field public static final vp_presentation:I = 0x7f0a1a8d

.field public static final vs_ai_function_recommend:I = 0x7f0a1a8e

.field public static final vs_card_1:I = 0x7f0a1a8f

.field public static final vs_card_2:I = 0x7f0a1a90

.field public static final vs_certificate_photo:I = 0x7f0a1a91

.field public static final vs_excel_bottom_prompt:I = 0x7f0a1a92

.field public static final vs_excel_rec:I = 0x7f0a1a93

.field public static final vs_first_scan:I = 0x7f0a1a94

.field public static final vs_focal_operation:I = 0x7f0a1a95

.field public static final vs_gpt_entrance:I = 0x7f0a1a96

.field public static final vs_guide:I = 0x7f0a1a97

.field public static final vs_guide_gp_purchase_ios_faker3:I = 0x7f0a1a98

.field public static final vs_guide_gp_purchase_skip:I = 0x7f0a1a99

.field public static final vs_image_edit_guide:I = 0x7f0a1a9a

.field public static final vs_indicator_view_red:I = 0x7f0a1a9b

.field public static final vs_iv_bg:I = 0x7f0a1a9c

.field public static final vs_negative_premium_life_time_indicator_view_golden:I = 0x7f0a1a9d

.field public static final vs_new_user_gift_box:I = 0x7f0a1a9e

.field public static final vs_no_ai_function_recommend:I = 0x7f0a1a9f

.field public static final vs_not_recognize:I = 0x7f0a1aa0

.field public static final vs_novice_task:I = 0x7f0a1aa1

.field public static final vs_ocr_rec:I = 0x7f0a1aa2

.field public static final vs_orientation_guide:I = 0x7f0a1aa3

.field public static final vs_pdf_enhance_fail:I = 0x7f0a1aa4

.field public static final vs_pdf_enhance_pdf_on_enhance:I = 0x7f0a1aa5

.field public static final vs_scan_doc:I = 0x7f0a1aa6

.field public static final vs_skins:I = 0x7f0a1aa7

.field public static final vs_vip_month_red_envelope:I = 0x7f0a1aa8

.field public static final vs_vip_task_toast:I = 0x7f0a1aa9

.field public static final vs_waterfall:I = 0x7f0a1aaa

.field public static final vs_waterfall_root:I = 0x7f0a1aab

.field public static final vt_camscanner_website_guide_link:I = 0x7f0a1aac

.field public static final vt_camscanner_website_guide_title:I = 0x7f0a1aad

.field public static final waterImageView:I = 0x7f0a1aae

.field public static final waterInvisibleET:I = 0x7f0a1aaf

.field public static final water_color_cotent:I = 0x7f0a1ab0

.field public static final water_mark_view:I = 0x7f0a1ab1

.field public static final watermark_checkbox:I = 0x7f0a1ab2

.field public static final web:I = 0x7f0a1ab3

.field public static final webView:I = 0x7f0a1ab4

.field public static final webicon:I = 0x7f0a1ab5

.field public static final webview:I = 0x7f0a1ab6

.field public static final weiboFollowTextView:I = 0x7f0a1ab7

.field public static final west:I = 0x7f0a1ab8

.field public static final wheel_end:I = 0x7f0a1ab9

.field public static final wheel_start:I = 0x7f0a1aba

.field public static final wheelview:I = 0x7f0a1abb

.field public static final wheelview2:I = 0x7f0a1abc

.field public static final wheelview3:I = 0x7f0a1abd

.field public static final when_playing:I = 0x7f0a1abe

.field public static final wide:I = 0x7f0a1abf

.field public static final widget_Rotate:I = 0x7f0a1ac0

.field public static final widget_imageview:I = 0x7f0a1ac1

.field public static final widget_list_item_layout:I = 0x7f0a1ac2

.field public static final widget_textview:I = 0x7f0a1ac3

.field public static final withText:I = 0x7f0a1ac4

.field public static final with_icon:I = 0x7f0a1ac5

.field public static final withinBounds:I = 0x7f0a1ac6

.field public static final wmiSignature:I = 0x7f0a1ac7

.field public static final wmv_pdf_editing_water_mark:I = 0x7f0a1ac8

.field public static final word_list_bottom_bar:I = 0x7f0a1ac9

.field public static final word_thumb:I = 0x7f0a1aca

.field public static final word_thumb_num:I = 0x7f0a1acb

.field public static final wrap:I = 0x7f0a1acc

.field public static final wrap_content:I = 0x7f0a1acd

.field public static final wrap_content_constrained:I = 0x7f0a1ace

.field public static final wrap_reverse:I = 0x7f0a1acf

.field public static final x_left:I = 0x7f0a1ad0

.field public static final x_right:I = 0x7f0a1ad1

.field public static final x_tv:I = 0x7f0a1ad2

.field public static final xbutton:I = 0x7f0a1ad3

.field public static final xedit:I = 0x7f0a1ad4

.field public static final xedit_layout:I = 0x7f0a1ad5

.field public static final xetSearch:I = 0x7f0a1ad6

.field public static final y_tv:I = 0x7f0a1ad7

.field public static final yubikit_prompt_cancel_btn:I = 0x7f0a1ad8

.field public static final yubikit_prompt_enable_nfc_btn:I = 0x7f0a1ad9

.field public static final yubikit_prompt_help_text_view:I = 0x7f0a1ada

.field public static final yubikit_prompt_title:I = 0x7f0a1adb

.field public static final z_tv:I = 0x7f0a1adc

.field public static final ziv_image:I = 0x7f0a1add

.field public static final zl_scan_view:I = 0x7f0a1ade

.field public static final zoom:I = 0x7f0a1adf

.field public static final zoom_bar:I = 0x7f0a1ae0

.field public static final zoom_bar_new:I = 0x7f0a1ae1

.field public static final zoom_image_view:I = 0x7f0a1ae2

.field public static final zoom_in:I = 0x7f0a1ae3

.field public static final zoom_in_new:I = 0x7f0a1ae4

.field public static final zoom_layout:I = 0x7f0a1ae5

.field public static final zoom_new:I = 0x7f0a1ae6

.field public static final zoom_out:I = 0x7f0a1ae7

.field public static final zoom_out_new:I = 0x7f0a1ae8

.field public static final zoomlayout:I = 0x7f0a1ae9


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

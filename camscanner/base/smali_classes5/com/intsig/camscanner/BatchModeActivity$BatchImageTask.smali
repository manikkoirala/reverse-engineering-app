.class Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;
.super Landroid/os/AsyncTask;
.source "BatchModeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/BatchModeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BatchImageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/util/ArrayList;",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Z

.field Oo08:I

.field final synthetic o〇0:Lcom/intsig/camscanner/BatchModeActivity;

.field private 〇080:Z

.field private 〇o00〇〇Oo:I

.field private 〇o〇:I


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/BatchModeActivity;)V
    .locals 1

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x1

    .line 3
    iput-boolean p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇080:Z

    const/4 v0, 0x0

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8:Z

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->Oo08:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/BatchModeActivity;Lcom/intsig/camscanner/〇o00〇〇Oo;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;-><init>(Lcom/intsig/camscanner/BatchModeActivity;)V

    return-void
.end method

.method private O8(Ljava/lang/String;II)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;
    .locals 27

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    invoke-static/range {p1 .. p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    const-string v3, "handlePage, path = "

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    const-string v5, "BatchModeActivity"

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    new-instance v2, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-object v4

    .line 35
    :cond_0
    const/4 v2, 0x0

    .line 36
    const/4 v6, 0x1

    .line 37
    if-nez p2, :cond_1

    .line 38
    .line 39
    const/16 v23, 0x1

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const/16 v23, 0x0

    .line 43
    .line 44
    :goto_0
    iget v7, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 45
    .line 46
    add-int/2addr v7, v6

    .line 47
    iput v7, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v10

    .line 53
    new-instance v7, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v8

    .line 62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    const-string v8, ".jpg"

    .line 69
    .line 70
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v15

    .line 77
    invoke-static {v0, v15}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    invoke-static {v15}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v24

    .line 84
    invoke-static {v15}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 85
    .line 86
    .line 87
    move-result v7

    .line 88
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo800〇〇()Z

    .line 89
    .line 90
    .line 91
    move-result v8

    .line 92
    new-instance v9, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    const-string v0, " ==> "

    .line 104
    .line 105
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v0, ", needTrim: "

    .line 112
    .line 113
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 117
    .line 118
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OoO〇OOo8o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    const-string v0, " , detectRotation: "

    .line 126
    .line 127
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    const-string v0, " , rawImagePath is exist: "

    .line 134
    .line 135
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-static {v15}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 153
    .line 154
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OoO〇OOo8o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 155
    .line 156
    .line 157
    move-result v0

    .line 158
    const/4 v3, 0x2

    .line 159
    const-wide/16 v25, 0x0

    .line 160
    .line 161
    if-nez v0, :cond_2

    .line 162
    .line 163
    if-eqz v8, :cond_9

    .line 164
    .line 165
    :cond_2
    invoke-static {v15}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 166
    .line 167
    .line 168
    move-result v0

    .line 169
    if-eqz v0, :cond_9

    .line 170
    .line 171
    invoke-static {v15}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;)I

    .line 172
    .line 173
    .line 174
    move-result v0

    .line 175
    int-to-long v11, v0

    .line 176
    invoke-static {v11, v12}, Lcom/intsig/camscanner/scanner/ScannerUtils;->isLegalImageStruct(J)Z

    .line 177
    .line 178
    .line 179
    move-result v9

    .line 180
    new-instance v11, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v12, "handlePage decodeImagesS: "

    .line 186
    .line 187
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    const-string v12, " , isLegalImageStruct: "

    .line 194
    .line 195
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v11

    .line 205
    invoke-static {v5, v11}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    if-eqz v9, :cond_9

    .line 209
    .line 210
    iget-object v9, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 211
    .line 212
    invoke-static {v9}, Lcom/intsig/camscanner/BatchModeActivity;->OoO〇OOo8o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 213
    .line 214
    .line 215
    move-result v9

    .line 216
    if-eqz v9, :cond_4

    .line 217
    .line 218
    const/16 v9, 0x8

    .line 219
    .line 220
    new-array v9, v9, [I

    .line 221
    .line 222
    move/from16 v11, p3

    .line 223
    .line 224
    invoke-static {v0, v9, v11}, Lcom/intsig/camscanner/scanner/ScannerUtils;->detectImageS(I[II)I

    .line 225
    .line 226
    .line 227
    move-result v11

    .line 228
    if-gez v11, :cond_3

    .line 229
    .line 230
    new-instance v9, Ljava/lang/StringBuilder;

    .line 231
    .line 232
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    .line 234
    .line 235
    const-string v12, "detectImageS result="

    .line 236
    .line 237
    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 241
    .line 242
    .line 243
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 244
    .line 245
    .line 246
    move-result-object v9

    .line 247
    invoke-static {v5, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .line 249
    .line 250
    move-object v9, v4

    .line 251
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    .line 252
    .line 253
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .line 255
    .line 256
    const-string v12, "multi detectImageBorder border:"

    .line 257
    .line 258
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-static {v9}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 262
    .line 263
    .line 264
    move-result-object v12

    .line 265
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    .line 267
    .line 268
    const-string v12, " rawImagePath:"

    .line 269
    .line 270
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    .line 275
    .line 276
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 277
    .line 278
    .line 279
    move-result-object v11

    .line 280
    invoke-static {v5, v11}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    goto :goto_1

    .line 284
    :cond_4
    move-object v9, v4

    .line 285
    :goto_1
    if-nez v9, :cond_5

    .line 286
    .line 287
    if-eqz v8, :cond_5

    .line 288
    .line 289
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getStructSize(I)[I

    .line 290
    .line 291
    .line 292
    move-result-object v8

    .line 293
    if-eqz v8, :cond_5

    .line 294
    .line 295
    array-length v11, v8

    .line 296
    if-lt v11, v3, :cond_5

    .line 297
    .line 298
    aget v9, v8, v2

    .line 299
    .line 300
    aget v8, v8, v6

    .line 301
    .line 302
    invoke-static {v9, v8}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getFullBorder(II)[I

    .line 303
    .line 304
    .line 305
    move-result-object v9

    .line 306
    :cond_5
    if-eqz v9, :cond_8

    .line 307
    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 309
    .line 310
    .line 311
    move-result-wide v11

    .line 312
    invoke-static {v0}, Lcom/intsig/scanner/ScannerEngine;->getImageStructPointer(I)J

    .line 313
    .line 314
    .line 315
    move-result-wide v13

    .line 316
    cmp-long v8, v13, v25

    .line 317
    .line 318
    if-eqz v8, :cond_7

    .line 319
    .line 320
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo800〇〇()Z

    .line 321
    .line 322
    .line 323
    move-result v8

    .line 324
    if-eqz v8, :cond_7

    .line 325
    .line 326
    invoke-static {v13, v14, v9, v2}, Lcom/intsig/camscanner/scanner/DocDirectionUtilKt;->detectDirection(J[IZ)I

    .line 327
    .line 328
    .line 329
    move-result v8

    .line 330
    if-ltz v8, :cond_6

    .line 331
    .line 332
    move v7, v8

    .line 333
    :cond_6
    new-instance v13, Ljava/lang/StringBuilder;

    .line 334
    .line 335
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .line 337
    .line 338
    const-string v14, "handlePage while multi import, rotation = "

    .line 339
    .line 340
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    .line 342
    .line 343
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 344
    .line 345
    .line 346
    const-string v8, "; cost time="

    .line 347
    .line 348
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 352
    .line 353
    .line 354
    move-result-wide v16

    .line 355
    sub-long v11, v16, v11

    .line 356
    .line 357
    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 358
    .line 359
    .line 360
    const-string v8, "\' border = "

    .line 361
    .line 362
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    .line 364
    .line 365
    invoke-static {v9}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 366
    .line 367
    .line 368
    move-result-object v8

    .line 369
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    .line 371
    .line 372
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 373
    .line 374
    .line 375
    move-result-object v8

    .line 376
    invoke-static {v5, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    .line 378
    .line 379
    goto :goto_2

    .line 380
    :cond_7
    new-instance v8, Ljava/lang/StringBuilder;

    .line 381
    .line 382
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 383
    .line 384
    .line 385
    const-string v11, "handlePage while multi import but ptr is illegal! "

    .line 386
    .line 387
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    .line 389
    .line 390
    invoke-virtual {v8, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 391
    .line 392
    .line 393
    const-string v11, ", rotation = "

    .line 394
    .line 395
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    .line 397
    .line 398
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 399
    .line 400
    .line 401
    const-string v11, "; imageUUID="

    .line 402
    .line 403
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    .line 405
    .line 406
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    .line 408
    .line 409
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object v8

    .line 413
    invoke-static {v5, v8}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    .line 415
    .line 416
    :cond_8
    :goto_2
    invoke-static {v0}, Lcom/intsig/scanner/ScannerEngine;->releaseImageS(I)I

    .line 417
    .line 418
    .line 419
    move v0, v7

    .line 420
    move-object v13, v9

    .line 421
    goto :goto_3

    .line 422
    :cond_9
    move-object v13, v4

    .line 423
    move v0, v7

    .line 424
    :goto_3
    iget-object v7, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 425
    .line 426
    invoke-static {v7}, Lcom/intsig/camscanner/BatchModeActivity;->OoO〇OOo8o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 427
    .line 428
    .line 429
    move-result v7

    .line 430
    if-eqz v7, :cond_a

    .line 431
    .line 432
    const/16 v22, 0x1

    .line 433
    .line 434
    goto :goto_4

    .line 435
    :cond_a
    const/16 v22, 0x2

    .line 436
    .line 437
    :goto_4
    sget-object v7, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 438
    .line 439
    iget-object v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 440
    .line 441
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 442
    .line 443
    .line 444
    move-result-wide v8

    .line 445
    iget v11, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 446
    .line 447
    const/4 v12, 0x1

    .line 448
    const/4 v14, 0x0

    .line 449
    iget-object v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 450
    .line 451
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->OO〇〇o0oO(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 452
    .line 453
    .line 454
    move-result v16

    .line 455
    iget-object v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 456
    .line 457
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 458
    .line 459
    .line 460
    move-result-object v3

    .line 461
    invoke-static {v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getCurrentEnhanceMode(Landroid/content/Context;)I

    .line 462
    .line 463
    .line 464
    move-result v3

    .line 465
    iget-object v2, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 466
    .line 467
    invoke-static {v2}, Lcom/intsig/camscanner/BatchModeActivity;->o〇o08〇(Lcom/intsig/camscanner/BatchModeActivity;)I

    .line 468
    .line 469
    .line 470
    move-result v2

    .line 471
    if-ne v3, v2, :cond_b

    .line 472
    .line 473
    const/16 v17, 0x1

    .line 474
    .line 475
    goto :goto_5

    .line 476
    :cond_b
    const/16 v17, 0x0

    .line 477
    .line 478
    :goto_5
    const/16 v18, 0x0

    .line 479
    .line 480
    const/16 v19, 0x1

    .line 481
    .line 482
    iget v2, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 483
    .line 484
    move-object v3, v15

    .line 485
    move v15, v0

    .line 486
    move/from16 v20, v2

    .line 487
    .line 488
    move/from16 v21, p2

    .line 489
    .line 490
    invoke-virtual/range {v7 .. v22}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇O〇(JLjava/lang/String;IZ[IIIZZZZIII)Landroid/net/Uri;

    .line 491
    .line 492
    .line 493
    move-result-object v0

    .line 494
    if-eqz v0, :cond_c

    .line 495
    .line 496
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 497
    .line 498
    .line 499
    move-result-wide v7

    .line 500
    goto :goto_6

    .line 501
    :cond_c
    const-wide/16 v7, -0x1

    .line 502
    .line 503
    :goto_6
    move-wide v12, v7

    .line 504
    cmp-long v0, v12, v25

    .line 505
    .line 506
    if-lez v0, :cond_10

    .line 507
    .line 508
    new-instance v2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 509
    .line 510
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 511
    .line 512
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OoO〇OOo8o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 513
    .line 514
    .line 515
    move-result v16

    .line 516
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 517
    .line 518
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o〇o08〇(Lcom/intsig/camscanner/BatchModeActivity;)I

    .line 519
    .line 520
    .line 521
    move-result v17

    .line 522
    iget v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 523
    .line 524
    iget-object v7, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 525
    .line 526
    invoke-static {v7}, Lcom/intsig/camscanner/BatchModeActivity;->〇oO88o(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 527
    .line 528
    .line 529
    move-result v19

    .line 530
    const/16 v20, 0x0

    .line 531
    .line 532
    move-object v11, v2

    .line 533
    move-object v14, v3

    .line 534
    move-object/from16 v15, v24

    .line 535
    .line 536
    move/from16 v18, v0

    .line 537
    .line 538
    invoke-direct/range {v11 .. v20}, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;-><init>(JLjava/lang/String;Ljava/lang/String;ZIIZLjava/lang/String;)V

    .line 539
    .line 540
    .line 541
    sget-object v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 542
    .line 543
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->isCropDewrapOn()Z

    .line 544
    .line 545
    .line 546
    move-result v0

    .line 547
    if-eqz v0, :cond_d

    .line 548
    .line 549
    iput-boolean v6, v2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->Oooo8o0〇:Z

    .line 550
    .line 551
    :cond_d
    if-eqz v23, :cond_e

    .line 552
    .line 553
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 554
    .line 555
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 556
    .line 557
    .line 558
    move-result-wide v7

    .line 559
    cmp-long v0, v7, v25

    .line 560
    .line 561
    if-ltz v0, :cond_e

    .line 562
    .line 563
    iget-boolean v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8:Z

    .line 564
    .line 565
    if-eqz v0, :cond_e

    .line 566
    .line 567
    new-instance v0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask$1;

    .line 568
    .line 569
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask$1;-><init>(Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;)V

    .line 570
    .line 571
    .line 572
    iput-object v0, v2, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->〇〇8O0〇8:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;

    .line 573
    .line 574
    goto :goto_7

    .line 575
    :cond_e
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 576
    .line 577
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 578
    .line 579
    .line 580
    move-result-wide v7

    .line 581
    cmp-long v0, v7, v25

    .line 582
    .line 583
    if-gez v0, :cond_f

    .line 584
    .line 585
    new-instance v0, Ljava/lang/StringBuilder;

    .line 586
    .line 587
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 588
    .line 589
    .line 590
    const-string v3, "handlePage, but mDocId="

    .line 591
    .line 592
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 593
    .line 594
    .line 595
    iget-object v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 596
    .line 597
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 598
    .line 599
    .line 600
    move-result-wide v7

    .line 601
    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 602
    .line 603
    .line 604
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 605
    .line 606
    .line 607
    move-result-object v0

    .line 608
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    .line 610
    .line 611
    :cond_f
    :goto_7
    new-instance v0, Landroid/content/ContentValues;

    .line 612
    .line 613
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 614
    .line 615
    .line 616
    iget v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 617
    .line 618
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 619
    .line 620
    .line 621
    move-result-object v3

    .line 622
    const-string v7, "pages"

    .line 623
    .line 624
    invoke-virtual {v0, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 625
    .line 626
    .line 627
    const-string v3, "state"

    .line 628
    .line 629
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 630
    .line 631
    .line 632
    move-result-object v6

    .line 633
    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 634
    .line 635
    .line 636
    :try_start_0
    iget-object v3, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 637
    .line 638
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 639
    .line 640
    .line 641
    move-result-object v3

    .line 642
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 643
    .line 644
    .line 645
    move-result-object v3

    .line 646
    iget-object v6, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 647
    .line 648
    invoke-static {v6}, Lcom/intsig/camscanner/BatchModeActivity;->o〇08oO80o(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/net/Uri;

    .line 649
    .line 650
    .line 651
    move-result-object v6

    .line 652
    invoke-virtual {v3, v6, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 653
    .line 654
    .line 655
    goto :goto_8

    .line 656
    :catch_0
    move-exception v0

    .line 657
    const-string v3, "RuntimeException"

    .line 658
    .line 659
    invoke-static {v5, v3, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 660
    .line 661
    .line 662
    :goto_8
    move-object v4, v2

    .line 663
    :cond_10
    return-object v4
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private 〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, ".jpg"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {p1, v0}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    const-string p1, "BatchModeActivity"

    .line 18
    .line 19
    const-string v0, "copy2ScannerRawPath failed"

    .line 20
    .line 21
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const/4 p1, 0x0

    .line 25
    return-object p1

    .line 26
    :cond_0
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method protected Oo08(Ljava/lang/Boolean;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const-string v1, "BatchModeActivity"

    .line 8
    .line 9
    if-lez p1, :cond_2

    .line 10
    .line 11
    new-instance p1, Landroid/content/ContentValues;

    .line 12
    .line 13
    invoke-direct {p1}, Landroid/content/ContentValues;-><init>()V

    .line 14
    .line 15
    .line 16
    iget v2, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 17
    .line 18
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const-string v3, "pages"

    .line 23
    .line 24
    invoke-virtual {p1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 25
    .line 26
    .line 27
    const-string v2, "state"

    .line 28
    .line 29
    const/4 v3, 0x1

    .line 30
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    invoke-virtual {p1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 38
    .line 39
    invoke-static {v2}, Lcom/intsig/camscanner/BatchModeActivity;->OO〇〇o0oO(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    const-string v2, "folder_type"

    .line 46
    .line 47
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 52
    .line 53
    .line 54
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 55
    .line 56
    invoke-static {v2}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    iget-object v3, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 65
    .line 66
    invoke-static {v3}, Lcom/intsig/camscanner/BatchModeActivity;->o〇08oO80o(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/net/Uri;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    invoke-virtual {v2, v3, p1, v0, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 71
    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 74
    .line 75
    invoke-static {p1}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 80
    .line 81
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 82
    .line 83
    .line 84
    move-result-wide v2

    .line 85
    invoke-static {p1, v2, v3}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 86
    .line 87
    .line 88
    new-instance p1, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v0, "update Doc pages number :"

    .line 94
    .line 95
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    iget v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 99
    .line 100
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :catch_0
    move-exception p1

    .line 112
    const-string v0, "RuntimeException"

    .line 113
    .line 114
    invoke-static {v1, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 118
    .line 119
    iget v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 120
    .line 121
    sub-int/2addr p1, v0

    .line 122
    if-lez p1, :cond_1

    .line 123
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    .line 125
    .line 126
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .line 128
    .line 129
    const-string v2, "miss = "

    .line 130
    .line 131
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 145
    .line 146
    invoke-static {p1}, Lcom/intsig/camscanner/BatchModeActivity;->o808o8o08(Lcom/intsig/camscanner/BatchModeActivity;)Lcom/intsig/camscanner/BatchModeActivity$MoldInterface;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    invoke-interface {p1}, Lcom/intsig/camscanner/BatchModeActivity$MoldInterface;->〇o00〇〇Oo()V

    .line 151
    .line 152
    .line 153
    goto :goto_2

    .line 154
    :cond_2
    iget-boolean p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8:Z

    .line 155
    .line 156
    if-eqz p1, :cond_3

    .line 157
    .line 158
    :try_start_1
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 159
    .line 160
    invoke-static {p1}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    iget-object v2, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 169
    .line 170
    invoke-static {v2}, Lcom/intsig/camscanner/BatchModeActivity;->o〇08oO80o(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/net/Uri;

    .line 171
    .line 172
    .line 173
    move-result-object v2

    .line 174
    invoke-virtual {p1, v2, v0, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 175
    .line 176
    .line 177
    goto :goto_1

    .line 178
    :catch_1
    move-exception p1

    .line 179
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 180
    .line 181
    .line 182
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 183
    .line 184
    const v0, 0x7f130083

    .line 185
    .line 186
    .line 187
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 188
    .line 189
    .line 190
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 191
    .line 192
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 193
    .line 194
    .line 195
    new-instance p1, Ljava/lang/StringBuilder;

    .line 196
    .line 197
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .line 199
    .line 200
    const-string v0, "mNeedGo2Doc = "

    .line 201
    .line 202
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    iget-object v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 206
    .line 207
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->〇ooO8Ooo〇(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 212
    .line 213
    .line 214
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object p1

    .line 218
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 222
    .line 223
    invoke-static {p1}, Lcom/intsig/camscanner/BatchModeActivity;->O8〇o0〇〇8(Lcom/intsig/camscanner/BatchModeActivity;)V

    .line 224
    .line 225
    .line 226
    return-void
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇([Ljava/util/ArrayList;)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Boolean;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->Oo08(Ljava/lang/Boolean;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected onPreExecute()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->〇ooO〇000(Lcom/intsig/camscanner/BatchModeActivity;)I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/BatchModeActivity;->O〇0o8o8〇(Lcom/intsig/camscanner/BatchModeActivity;I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0([Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected varargs o〇0([Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->Oo08:I

    .line 7
    .line 8
    add-int/lit8 v1, v0, 0x1

    .line 9
    .line 10
    iput v1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->Oo08:I

    .line 11
    .line 12
    invoke-static {p1, v0}, Lcom/intsig/camscanner/BatchModeActivity;->O0o0(Lcom/intsig/camscanner/BatchModeActivity;I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method protected varargs 〇o〇([Ljava/util/ArrayList;)Ljava/lang/Boolean;
    .locals 18

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v2, " , border: "

    .line 4
    .line 5
    const-string v3, "BatchImageTask trim: "

    .line 6
    .line 7
    iget-boolean v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇080:Z

    .line 8
    .line 9
    const/4 v4, 0x1

    .line 10
    const/4 v5, 0x0

    .line 11
    if-eqz v0, :cond_2

    .line 12
    .line 13
    aget-object v0, p1, v5

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v7

    .line 21
    if-lez v7, :cond_0

    .line 22
    .line 23
    const/4 v7, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v7, 0x0

    .line 26
    :goto_0
    if-eqz v7, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 29
    .line 30
    .line 31
    move-result v8

    .line 32
    iput v8, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 33
    .line 34
    :cond_1
    const/4 v8, 0x0

    .line 35
    move/from16 v17, v7

    .line 36
    .line 37
    move-object v7, v0

    .line 38
    move/from16 v0, v17

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_2
    aget-object v0, p1, v5

    .line 42
    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 46
    .line 47
    .line 48
    move-result v7

    .line 49
    if-lez v7, :cond_3

    .line 50
    .line 51
    const/4 v7, 0x1

    .line 52
    goto :goto_1

    .line 53
    :cond_3
    const/4 v7, 0x0

    .line 54
    :goto_1
    if-eqz v7, :cond_4

    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 57
    .line 58
    .line 59
    move-result v8

    .line 60
    iput v8, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 61
    .line 62
    :cond_4
    move-object v8, v0

    .line 63
    move v0, v7

    .line 64
    const/4 v7, 0x0

    .line 65
    :goto_2
    if-eqz v0, :cond_15

    .line 66
    .line 67
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 68
    .line 69
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 70
    .line 71
    .line 72
    move-result-wide v9

    .line 73
    const-wide/16 v11, 0x0

    .line 74
    .line 75
    const-string v13, "BatchModeActivity"

    .line 76
    .line 77
    cmp-long v0, v9, v11

    .line 78
    .line 79
    if-lez v0, :cond_5

    .line 80
    .line 81
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 82
    .line 83
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iget-object v9, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 88
    .line 89
    invoke-static {v9}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 90
    .line 91
    .line 92
    move-result-wide v9

    .line 93
    invoke-static {v0, v9, v10}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    iput v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o〇:I

    .line 98
    .line 99
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 100
    .line 101
    sget-object v9, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 102
    .line 103
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 104
    .line 105
    .line 106
    move-result-wide v10

    .line 107
    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 108
    .line 109
    .line 110
    move-result-object v9

    .line 111
    invoke-static {v0, v9}, Lcom/intsig/camscanner/BatchModeActivity;->oOO8oo0(Lcom/intsig/camscanner/BatchModeActivity;Landroid/net/Uri;)V

    .line 112
    .line 113
    .line 114
    goto :goto_3

    .line 115
    :cond_5
    iput-boolean v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8:Z

    .line 116
    .line 117
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 118
    .line 119
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o808o8o08(Lcom/intsig/camscanner/BatchModeActivity;)Lcom/intsig/camscanner/BatchModeActivity$MoldInterface;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-interface {v0}, Lcom/intsig/camscanner/BatchModeActivity$MoldInterface;->〇080()V

    .line 124
    .line 125
    .line 126
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 127
    .line 128
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o〇08oO80o(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/net/Uri;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    if-nez v0, :cond_6

    .line 133
    .line 134
    const-string v0, "mDocUri == null"

    .line 135
    .line 136
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 140
    .line 141
    return-object v0

    .line 142
    :cond_6
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 143
    .line 144
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o〇08oO80o(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/net/Uri;

    .line 145
    .line 146
    .line 147
    move-result-object v9

    .line 148
    invoke-static {v9}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 149
    .line 150
    .line 151
    move-result-wide v9

    .line 152
    invoke-static {v0, v9, v10}, Lcom/intsig/camscanner/BatchModeActivity;->OO0o(Lcom/intsig/camscanner/BatchModeActivity;J)V

    .line 153
    .line 154
    .line 155
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 156
    .line 157
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->o0OO(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 158
    .line 159
    .line 160
    move-result-wide v9

    .line 161
    cmp-long v0, v9, v11

    .line 162
    .line 163
    if-lez v0, :cond_7

    .line 164
    .line 165
    new-instance v0, Landroid/content/ContentValues;

    .line 166
    .line 167
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 168
    .line 169
    .line 170
    iget-object v9, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 171
    .line 172
    invoke-static {v9}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 173
    .line 174
    .line 175
    move-result-wide v9

    .line 176
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 177
    .line 178
    .line 179
    move-result-object v9

    .line 180
    const-string v10, "document_id"

    .line 181
    .line 182
    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 183
    .line 184
    .line 185
    iget-object v9, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 186
    .line 187
    invoke-static {v9}, Lcom/intsig/camscanner/BatchModeActivity;->o0OO(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 188
    .line 189
    .line 190
    move-result-wide v9

    .line 191
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 192
    .line 193
    .line 194
    move-result-object v9

    .line 195
    const-string v10, "tag_id"

    .line 196
    .line 197
    invoke-virtual {v0, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    .line 199
    .line 200
    iget-object v9, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 201
    .line 202
    invoke-static {v9}, Lcom/intsig/camscanner/BatchModeActivity;->o0Oo(Lcom/intsig/camscanner/BatchModeActivity;)Landroid/content/Context;

    .line 203
    .line 204
    .line 205
    move-result-object v9

    .line 206
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 207
    .line 208
    .line 209
    move-result-object v9

    .line 210
    sget-object v10, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 211
    .line 212
    invoke-virtual {v9, v10, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 213
    .line 214
    .line 215
    :cond_7
    :goto_3
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 216
    .line 217
    .line 218
    move-result-object v9

    .line 219
    new-instance v10, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;

    .line 220
    .line 221
    iget-object v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 222
    .line 223
    invoke-static {v0}, Lcom/intsig/camscanner/BatchModeActivity;->OO0O(Lcom/intsig/camscanner/BatchModeActivity;)J

    .line 224
    .line 225
    .line 226
    move-result-wide v11

    .line 227
    invoke-direct {v10, v11, v12}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;-><init>(J)V

    .line 228
    .line 229
    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    .line 231
    .line 232
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    .line 234
    .line 235
    const-string v11, "mSendPhotosNum="

    .line 236
    .line 237
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    iget v11, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 241
    .line 242
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    .line 251
    .line 252
    sget-object v0, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->〇O〇:Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;

    .line 253
    .line 254
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;->〇080()Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->〇80〇808〇O()I

    .line 259
    .line 260
    .line 261
    move-result v11

    .line 262
    const/4 v12, 0x0

    .line 263
    const/4 v14, 0x0

    .line 264
    const/4 v15, 0x0

    .line 265
    :goto_4
    iget v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 266
    .line 267
    if-ge v12, v0, :cond_14

    .line 268
    .line 269
    iget-boolean v0, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇080:Z

    .line 270
    .line 271
    if-eqz v0, :cond_8

    .line 272
    .line 273
    if-eqz v7, :cond_9

    .line 274
    .line 275
    invoke-virtual {v7, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    check-cast v0, Landroid/net/Uri;

    .line 280
    .line 281
    iget-object v6, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 282
    .line 283
    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 284
    .line 285
    .line 286
    move-result-object v6

    .line 287
    invoke-static {v6, v0, v5}, Lcom/intsig/camscanner/multiimageedit/util/MultiImageEditPageManagerUtil;->〇〇808〇(Landroid/content/Context;Landroid/net/Uri;Z)Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v0

    .line 291
    goto :goto_5

    .line 292
    :cond_8
    if-eqz v8, :cond_9

    .line 293
    .line 294
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 295
    .line 296
    .line 297
    move-result-object v0

    .line 298
    check-cast v0, Ljava/lang/String;

    .line 299
    .line 300
    goto :goto_5

    .line 301
    :cond_9
    const/4 v0, 0x0

    .line 302
    :goto_5
    if-eqz v0, :cond_13

    .line 303
    .line 304
    :try_start_0
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 305
    .line 306
    .line 307
    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 308
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    .line 309
    .line 310
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .line 312
    .line 313
    const-string v5, "srcPath="

    .line 314
    .line 315
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    .line 320
    .line 321
    const-string v5, " index="

    .line 322
    .line 323
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    .line 325
    .line 326
    add-int/lit8 v15, v15, 0x1

    .line 327
    .line 328
    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 329
    .line 330
    .line 331
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 332
    .line 333
    .line 334
    move-result-object v4

    .line 335
    invoke-static {v13, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    .line 337
    .line 338
    const/4 v4, 0x0

    .line 339
    invoke-static {v0, v4}, Lcom/intsig/utils/FileUtil;->〇o(Ljava/lang/String;Z)Z

    .line 340
    .line 341
    .line 342
    move-result v5

    .line 343
    if-nez v5, :cond_a

    .line 344
    .line 345
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->o〇〇0〇(Ljava/lang/String;)Z

    .line 346
    .line 347
    .line 348
    move-result v4

    .line 349
    if-eqz v4, :cond_a

    .line 350
    .line 351
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇8(Ljava/lang/String;)V

    .line 352
    .line 353
    .line 354
    :cond_a
    const/4 v4, 0x1

    .line 355
    new-array v5, v4, [Z

    .line 356
    .line 357
    const/16 v16, 0x0

    .line 358
    .line 359
    aput-boolean v16, v5, v16

    .line 360
    .line 361
    iget-object v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 362
    .line 363
    invoke-static {v4, v6, v5}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->oO80(Landroid/content/Context;Ljava/lang/String;[Z)Z

    .line 364
    .line 365
    .line 366
    move-result v4

    .line 367
    aget-boolean v5, v5, v16

    .line 368
    .line 369
    if-eqz v5, :cond_b

    .line 370
    .line 371
    add-int/lit8 v14, v14, 0x1

    .line 372
    .line 373
    :cond_b
    if-nez v4, :cond_d

    .line 374
    .line 375
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 376
    .line 377
    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    .line 379
    .line 380
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 381
    .line 382
    .line 383
    const-string v4, "BatchImageTask, delete rawPath because compress error! rawPath="

    .line 384
    .line 385
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    .line 387
    .line 388
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    .line 390
    .line 391
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 392
    .line 393
    .line 394
    move-result-object v0

    .line 395
    invoke-static {v13, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396
    .line 397
    .line 398
    invoke-direct {v1, v6, v12, v11}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8(Ljava/lang/String;II)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 399
    .line 400
    .line 401
    move-result-object v0

    .line 402
    if-eqz v0, :cond_c

    .line 403
    .line 404
    new-instance v4, Ljava/lang/StringBuilder;

    .line 405
    .line 406
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 407
    .line 408
    .line 409
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    .line 411
    .line 412
    iget-boolean v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->O8:Z

    .line 413
    .line 414
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 415
    .line 416
    .line 417
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    .line 419
    .line 420
    iget-object v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 421
    .line 422
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 423
    .line 424
    .line 425
    move-result-object v5

    .line 426
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 427
    .line 428
    .line 429
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v4

    .line 433
    invoke-static {v13, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .line 435
    .line 436
    invoke-virtual {v10, v0}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 437
    .line 438
    .line 439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 440
    .line 441
    .line 442
    move-result-wide v4

    .line 443
    invoke-virtual {v9, v10, v4, v5}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇8(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;J)V

    .line 444
    .line 445
    .line 446
    :cond_c
    const/4 v4, 0x0

    .line 447
    new-array v0, v4, [Ljava/lang/String;

    .line 448
    .line 449
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 450
    .line 451
    .line 452
    goto/16 :goto_7

    .line 453
    .line 454
    :cond_d
    :try_start_2
    iget-object v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 455
    .line 456
    invoke-static {v4}, Lcom/intsig/camscanner/BatchModeActivity;->O00OoO〇(Lcom/intsig/camscanner/BatchModeActivity;)Z

    .line 457
    .line 458
    .line 459
    move-result v4

    .line 460
    if-eqz v4, :cond_f

    .line 461
    .line 462
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 463
    .line 464
    .line 465
    move-result v4

    .line 466
    if-nez v4, :cond_f

    .line 467
    .line 468
    iget-object v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 469
    .line 470
    invoke-static {v4}, Lcom/intsig/camscanner/BatchModeActivity;->O〇〇o8O(Lcom/intsig/camscanner/BatchModeActivity;)Ljava/util/ArrayList;

    .line 471
    .line 472
    .line 473
    move-result-object v4

    .line 474
    if-nez v4, :cond_e

    .line 475
    .line 476
    iget-object v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 477
    .line 478
    new-instance v5, Ljava/util/ArrayList;

    .line 479
    .line 480
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 481
    .line 482
    .line 483
    invoke-static {v4, v5}, Lcom/intsig/camscanner/BatchModeActivity;->O80OO(Lcom/intsig/camscanner/BatchModeActivity;Ljava/util/ArrayList;)V

    .line 484
    .line 485
    .line 486
    :cond_e
    iget-object v4, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->o〇0:Lcom/intsig/camscanner/BatchModeActivity;

    .line 487
    .line 488
    invoke-static {v4}, Lcom/intsig/camscanner/BatchModeActivity;->O〇〇o8O(Lcom/intsig/camscanner/BatchModeActivity;)Ljava/util/ArrayList;

    .line 489
    .line 490
    .line 491
    move-result-object v4

    .line 492
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 493
    .line 494
    .line 495
    move-result-object v0

    .line 496
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497
    .line 498
    .line 499
    :cond_f
    invoke-direct {v1, v6, v12, v11}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8(Ljava/lang/String;II)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 500
    .line 501
    .line 502
    move-result-object v0

    .line 503
    if-eqz v0, :cond_10

    .line 504
    .line 505
    new-instance v4, Ljava/lang/StringBuilder;

    .line 506
    .line 507
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 508
    .line 509
    .line 510
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    .line 512
    .line 513
    iget-boolean v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->O8:Z

    .line 514
    .line 515
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 516
    .line 517
    .line 518
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    .line 520
    .line 521
    iget-object v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 522
    .line 523
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 524
    .line 525
    .line 526
    move-result-object v5

    .line 527
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    .line 529
    .line 530
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 531
    .line 532
    .line 533
    move-result-object v4

    .line 534
    invoke-static {v13, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    .line 536
    .line 537
    invoke-virtual {v10, v0}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 538
    .line 539
    .line 540
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 541
    .line 542
    .line 543
    move-result-wide v4

    .line 544
    invoke-virtual {v9, v10, v4, v5}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇8(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;J)V

    .line 545
    .line 546
    .line 547
    :cond_10
    const/4 v4, 0x0

    .line 548
    new-array v0, v4, [Ljava/lang/String;

    .line 549
    .line 550
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 551
    .line 552
    .line 553
    goto :goto_7

    .line 554
    :catchall_0
    move-exception v0

    .line 555
    goto :goto_8

    .line 556
    :catch_0
    move-exception v0

    .line 557
    goto :goto_6

    .line 558
    :catchall_1
    move-exception v0

    .line 559
    const/4 v6, 0x0

    .line 560
    goto :goto_8

    .line 561
    :catch_1
    move-exception v0

    .line 562
    const/4 v6, 0x0

    .line 563
    :goto_6
    :try_start_3
    const-string v4, "doInBackground copy image"

    .line 564
    .line 565
    invoke-static {v13, v4, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 566
    .line 567
    .line 568
    invoke-direct {v1, v6, v12, v11}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8(Ljava/lang/String;II)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 569
    .line 570
    .line 571
    move-result-object v0

    .line 572
    if-eqz v0, :cond_11

    .line 573
    .line 574
    new-instance v4, Ljava/lang/StringBuilder;

    .line 575
    .line 576
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 577
    .line 578
    .line 579
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 580
    .line 581
    .line 582
    iget-boolean v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->O8:Z

    .line 583
    .line 584
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 585
    .line 586
    .line 587
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    .line 589
    .line 590
    iget-object v5, v0, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 591
    .line 592
    invoke-static {v5}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 593
    .line 594
    .line 595
    move-result-object v5

    .line 596
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    .line 598
    .line 599
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 600
    .line 601
    .line 602
    move-result-object v4

    .line 603
    invoke-static {v13, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    .line 605
    .line 606
    invoke-virtual {v10, v0}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 607
    .line 608
    .line 609
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 610
    .line 611
    .line 612
    move-result-wide v4

    .line 613
    invoke-virtual {v9, v10, v4, v5}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇8(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;J)V

    .line 614
    .line 615
    .line 616
    :cond_11
    const/4 v4, 0x0

    .line 617
    new-array v0, v4, [Ljava/lang/String;

    .line 618
    .line 619
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 620
    .line 621
    .line 622
    :goto_7
    const/4 v4, 0x0

    .line 623
    goto :goto_9

    .line 624
    :goto_8
    invoke-direct {v1, v6, v12, v11}, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->O8(Ljava/lang/String;II)Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;

    .line 625
    .line 626
    .line 627
    move-result-object v4

    .line 628
    if-eqz v4, :cond_12

    .line 629
    .line 630
    new-instance v5, Ljava/lang/StringBuilder;

    .line 631
    .line 632
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 633
    .line 634
    .line 635
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    .line 637
    .line 638
    iget-boolean v3, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->O8:Z

    .line 639
    .line 640
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 641
    .line 642
    .line 643
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    .line 645
    .line 646
    iget-object v2, v4, Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;->OO0o〇〇〇〇0:[I

    .line 647
    .line 648
    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 649
    .line 650
    .line 651
    move-result-object v2

    .line 652
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 653
    .line 654
    .line 655
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 656
    .line 657
    .line 658
    move-result-object v2

    .line 659
    invoke-static {v13, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    .line 661
    .line 662
    invoke-virtual {v10, v4}, Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;->〇o00〇〇Oo(Lcom/intsig/camscanner/background_batch/model/BackScanPageModel;)V

    .line 663
    .line 664
    .line 665
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 666
    .line 667
    .line 668
    move-result-wide v2

    .line 669
    invoke-virtual {v9, v10, v2, v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇8(Lcom/intsig/camscanner/background_batch/model/BackScanDocModel;J)V

    .line 670
    .line 671
    .line 672
    :cond_12
    const/4 v4, 0x0

    .line 673
    new-array v2, v4, [Ljava/lang/String;

    .line 674
    .line 675
    invoke-virtual {v1, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 676
    .line 677
    .line 678
    throw v0

    .line 679
    :cond_13
    const/4 v4, 0x0

    .line 680
    new-array v0, v4, [Ljava/lang/String;

    .line 681
    .line 682
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 683
    .line 684
    .line 685
    :goto_9
    add-int/lit8 v12, v12, 0x1

    .line 686
    .line 687
    const/4 v4, 0x1

    .line 688
    const/4 v5, 0x0

    .line 689
    goto/16 :goto_4

    .line 690
    .line 691
    :cond_14
    sget-object v0, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->〇O〇:Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;

    .line 692
    .line 693
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;->〇080()Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;

    .line 694
    .line 695
    .line 696
    move-result-object v0

    .line 697
    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->oo88o8O(I)V

    .line 698
    .line 699
    .line 700
    sget-object v0, Lcom/intsig/camscanner/scanner/SpecialImageCollectRunnable;->Companion:Lcom/intsig/camscanner/scanner/SpecialImageCollectRunnable$Companion;

    .line 701
    .line 702
    iget v2, v1, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇o00〇〇Oo:I

    .line 703
    .line 704
    invoke-virtual {v0, v14, v2}, Lcom/intsig/camscanner/scanner/SpecialImageCollectRunnable$Companion;->collectBigImageNumberWhileImport(II)V

    .line 705
    .line 706
    .line 707
    :cond_15
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 708
    .line 709
    return-object v0
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
.end method

.method public 〇〇888(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/BatchModeActivity$BatchImageTask;->〇080:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

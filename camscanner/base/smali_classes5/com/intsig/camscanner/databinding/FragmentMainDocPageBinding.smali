.class public final Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
.super Ljava/lang/Object;
.source "FragmentMainDocPageBinding.java"

# interfaces
.implements Landroidx/viewbinding/ViewBinding;


# instance fields
.field public final O0O:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final O88O:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final O8o08O8O:Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final OO:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final OO〇00〇8oO:Landroid/widget/FrameLayout;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field private final o0:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final o8oOOo:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final o8〇OO0〇0o:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final oOO〇〇:Landroid/view/ViewStub;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final oOo0:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final oOo〇8o008:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final ooo0〇〇O:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final o〇00O:Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇080OO8〇0:Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇0O:Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇OOo8〇0:Lcom/google/android/material/appbar/AppBarLayout;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇O〇〇O8:Landroidx/recyclerview/widget/RecyclerView;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇o0O:Landroidx/appcompat/widget/Toolbar;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field

.field public final 〇〇08O:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;
    .annotation build Landroidx/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/google/android/material/appbar/AppBarLayout;Landroidx/constraintlayout/widget/ConstraintLayout;Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;Landroid/widget/FrameLayout;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;Lcom/intsig/camscanner/view/PullToSyncRecyclerView;Landroidx/recyclerview/widget/RecyclerView;Landroidx/appcompat/widget/Toolbar;Landroid/widget/TextView;Landroid/view/ViewStub;)V
    .locals 2
    .param p1    # Landroidx/constraintlayout/widget/ConstraintLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/material/appbar/AppBarLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Landroidx/constraintlayout/widget/ConstraintLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Landroidx/constraintlayout/widget/ConstraintLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p6    # Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p7    # Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p8    # Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p9    # Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p10    # Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p11    # Landroid/widget/FrameLayout;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p12    # Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p13    # Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p14    # Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p15    # Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p16    # Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p17    # Lcom/intsig/camscanner/view/PullToSyncRecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p18    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p19    # Landroidx/appcompat/widget/Toolbar;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p20    # Landroid/widget/TextView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p21    # Landroid/view/ViewStub;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object v0, p0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    .line 4
    .line 5
    move-object v1, p1

    .line 6
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->o0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 7
    .line 8
    move-object v1, p2

    .line 9
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇OOo8〇0:Lcom/google/android/material/appbar/AppBarLayout;

    .line 10
    .line 11
    move-object v1, p3

    .line 12
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 13
    .line 14
    move-object v1, p4

    .line 15
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 16
    .line 17
    move-object v1, p5

    .line 18
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->o〇00O:Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;

    .line 19
    .line 20
    move-object v1, p6

    .line 21
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;

    .line 22
    .line 23
    move-object v1, p7

    .line 24
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;

    .line 25
    .line 26
    move-object v1, p8

    .line 27
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇0O:Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;

    .line 28
    .line 29
    move-object v1, p9

    .line 30
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->oOo〇8o008:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 31
    .line 32
    move-object v1, p10

    .line 33
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->oOo0:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 34
    .line 35
    move-object v1, p11

    .line 36
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->OO〇00〇8oO:Landroid/widget/FrameLayout;

    .line 37
    .line 38
    move-object v1, p12

    .line 39
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;

    .line 40
    .line 41
    move-object v1, p13

    .line 42
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;

    .line 43
    .line 44
    move-object/from16 v1, p14

    .line 45
    .line 46
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;

    .line 47
    .line 48
    move-object/from16 v1, p15

    .line 49
    .line 50
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇〇08O:Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;

    .line 51
    .line 52
    move-object/from16 v1, p16

    .line 53
    .line 54
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 55
    .line 56
    move-object/from16 v1, p17

    .line 57
    .line 58
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->o8oOOo:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 59
    .line 60
    move-object/from16 v1, p18

    .line 61
    .line 62
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇O〇〇O8:Landroidx/recyclerview/widget/RecyclerView;

    .line 63
    .line 64
    move-object/from16 v1, p19

    .line 65
    .line 66
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇o0O:Landroidx/appcompat/widget/Toolbar;

    .line 67
    .line 68
    move-object/from16 v1, p20

    .line 69
    .line 70
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->O88O:Landroid/widget/TextView;

    .line 71
    .line 72
    move-object/from16 v1, p21

    .line 73
    .line 74
    iput-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->oOO〇〇:Landroid/view/ViewStub;

    .line 75
    .line 76
    return-void
.end method

.method public static bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
    .locals 25
    .param p0    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const v1, 0x7f0a0142

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    move-object v5, v2

    .line 11
    check-cast v5, Lcom/google/android/material/appbar/AppBarLayout;

    .line 12
    .line 13
    if-eqz v5, :cond_0

    .line 14
    .line 15
    const v1, 0x7f0a0383

    .line 16
    .line 17
    .line 18
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    move-object v6, v2

    .line 23
    check-cast v6, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 24
    .line 25
    if-eqz v6, :cond_0

    .line 26
    .line 27
    move-object v7, v0

    .line 28
    check-cast v7, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 29
    .line 30
    const v1, 0x7f0a03a5

    .line 31
    .line 32
    .line 33
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    if-eqz v2, :cond_0

    .line 38
    .line 39
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;

    .line 40
    .line 41
    .line 42
    move-result-object v8

    .line 43
    const v1, 0x7f0a03a6

    .line 44
    .line 45
    .line 46
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    if-eqz v2, :cond_0

    .line 51
    .line 52
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;

    .line 53
    .line 54
    .line 55
    move-result-object v9

    .line 56
    const v1, 0x7f0a03e8

    .line 57
    .line 58
    .line 59
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    if-eqz v2, :cond_0

    .line 64
    .line 65
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;

    .line 66
    .line 67
    .line 68
    move-result-object v10

    .line 69
    const v1, 0x7f0a0423

    .line 70
    .line 71
    .line 72
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    if-eqz v2, :cond_0

    .line 77
    .line 78
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;

    .line 79
    .line 80
    .line 81
    move-result-object v11

    .line 82
    const v1, 0x7f0a05d8

    .line 83
    .line 84
    .line 85
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    move-object v12, v2

    .line 90
    check-cast v12, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 91
    .line 92
    if-eqz v12, :cond_0

    .line 93
    .line 94
    const v1, 0x7f0a05d9

    .line 95
    .line 96
    .line 97
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    move-object v13, v2

    .line 102
    check-cast v13, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 103
    .line 104
    if-eqz v13, :cond_0

    .line 105
    .line 106
    const v1, 0x7f0a066e

    .line 107
    .line 108
    .line 109
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    move-object v14, v2

    .line 114
    check-cast v14, Landroid/widget/FrameLayout;

    .line 115
    .line 116
    if-eqz v14, :cond_0

    .line 117
    .line 118
    const v1, 0x7f0a072a

    .line 119
    .line 120
    .line 121
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 122
    .line 123
    .line 124
    move-result-object v2

    .line 125
    if-eqz v2, :cond_0

    .line 126
    .line 127
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;

    .line 128
    .line 129
    .line 130
    move-result-object v15

    .line 131
    const v1, 0x7f0a076c

    .line 132
    .line 133
    .line 134
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    if-eqz v2, :cond_0

    .line 139
    .line 140
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;

    .line 141
    .line 142
    .line 143
    move-result-object v16

    .line 144
    const v1, 0x7f0a0775

    .line 145
    .line 146
    .line 147
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    if-eqz v2, :cond_0

    .line 152
    .line 153
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;

    .line 154
    .line 155
    .line 156
    move-result-object v17

    .line 157
    const v1, 0x7f0a0777

    .line 158
    .line 159
    .line 160
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 161
    .line 162
    .line 163
    move-result-object v2

    .line 164
    if-eqz v2, :cond_0

    .line 165
    .line 166
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;

    .line 167
    .line 168
    .line 169
    move-result-object v18

    .line 170
    const v1, 0x7f0a0cef

    .line 171
    .line 172
    .line 173
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 174
    .line 175
    .line 176
    move-result-object v2

    .line 177
    if-eqz v2, :cond_0

    .line 178
    .line 179
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 180
    .line 181
    .line 182
    move-result-object v19

    .line 183
    const v1, 0x7f0a0d7e

    .line 184
    .line 185
    .line 186
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 187
    .line 188
    .line 189
    move-result-object v2

    .line 190
    move-object/from16 v20, v2

    .line 191
    .line 192
    check-cast v20, Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 193
    .line 194
    if-eqz v20, :cond_0

    .line 195
    .line 196
    const v1, 0x7f0a0f18

    .line 197
    .line 198
    .line 199
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 200
    .line 201
    .line 202
    move-result-object v2

    .line 203
    move-object/from16 v21, v2

    .line 204
    .line 205
    check-cast v21, Landroidx/recyclerview/widget/RecyclerView;

    .line 206
    .line 207
    if-eqz v21, :cond_0

    .line 208
    .line 209
    const v1, 0x7f0a11e2

    .line 210
    .line 211
    .line 212
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 213
    .line 214
    .line 215
    move-result-object v2

    .line 216
    move-object/from16 v22, v2

    .line 217
    .line 218
    check-cast v22, Landroidx/appcompat/widget/Toolbar;

    .line 219
    .line 220
    if-eqz v22, :cond_0

    .line 221
    .line 222
    const v1, 0x7f0a1251

    .line 223
    .line 224
    .line 225
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    move-object/from16 v23, v2

    .line 230
    .line 231
    check-cast v23, Landroid/widget/TextView;

    .line 232
    .line 233
    if-eqz v23, :cond_0

    .line 234
    .line 235
    const v1, 0x7f0a1a5e

    .line 236
    .line 237
    .line 238
    invoke-static {v0, v1}, Landroidx/viewbinding/ViewBindings;->findChildViewById(Landroid/view/View;I)Landroid/view/View;

    .line 239
    .line 240
    .line 241
    move-result-object v2

    .line 242
    move-object/from16 v24, v2

    .line 243
    .line 244
    check-cast v24, Landroid/view/ViewStub;

    .line 245
    .line 246
    if-eqz v24, :cond_0

    .line 247
    .line 248
    new-instance v0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 249
    .line 250
    move-object v3, v0

    .line 251
    move-object v4, v7

    .line 252
    invoke-direct/range {v3 .. v24}, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/google/android/material/appbar/AppBarLayout;Landroidx/constraintlayout/widget/ConstraintLayout;Landroidx/constraintlayout/widget/ConstraintLayout;Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewBinding;Lcom/intsig/camscanner/databinding/LayoutDocPageEmptyViewSearchBinding;Lcom/intsig/camscanner/databinding/LayoutMoveCopyDocPageEmptyViewBinding;Lcom/intsig/camscanner/databinding/LayoutScenarioFolderEmptyViewBinding;Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;Landroid/widget/FrameLayout;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderViewBinding;Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderDirBinding;Lcom/intsig/camscanner/databinding/IncludeMainDocHeaderRootBinding;Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;Lcom/intsig/camscanner/view/PullToSyncRecyclerView;Landroidx/recyclerview/widget/RecyclerView;Landroidx/appcompat/widget/Toolbar;Landroid/widget/TextView;Landroid/view/ViewStub;)V

    .line 253
    .line 254
    .line 255
    return-object v0

    .line 256
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    .line 257
    .line 258
    .line 259
    move-result-object v0

    .line 260
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    new-instance v1, Ljava/lang/NullPointerException;

    .line 265
    .line 266
    const-string v2, "Missing required view with ID: "

    .line 267
    .line 268
    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v0

    .line 272
    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 273
    .line 274
    .line 275
    throw v1
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public static inflate(Landroid/view/LayoutInflater;)Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
    .locals 2
    .param p0    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇o00〇〇Oo(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static 〇o00〇〇Oo(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
    .locals 2
    .param p0    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    const v0, 0x7f0d02fe

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object p0

    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    return-object p0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
.end method


# virtual methods
.method public bridge synthetic getRoot()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->o0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

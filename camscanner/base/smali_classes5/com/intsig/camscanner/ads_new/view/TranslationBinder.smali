.class public final Lcom/intsig/camscanner/ads_new/view/TranslationBinder;
.super Ljava/lang/Object;
.source "TranslationBinder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;,
        Lcom/intsig/camscanner/ads_new/view/TranslationBinder$WhenMappings;,
        Lcom/intsig/camscanner/ads_new/view/TranslationBinder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/intsig/camscanner/ads_new/view/TranslationBinder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Ljava/lang/Runnable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Landroid/os/Handler;

.field private 〇o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/ads_new/view/TranslationBinder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->Oo08:Lcom/intsig/camscanner/ads_new/view/TranslationBinder$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "appLaunchAdActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 10
    .line 11
    const-string p1, ""

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o〇:Ljava/lang/String;

    .line 14
    .line 15
    new-instance p1, L〇00O0O0/Oo08;

    .line 16
    .line 17
    invoke-direct {p1, p0}, L〇00O0O0/Oo08;-><init>(Lcom/intsig/camscanner/ads_new/view/TranslationBinder;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->O8:Ljava/lang/Runnable;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/ads_new/view/TranslationBinder;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o〇(Lcom/intsig/camscanner/ads_new/view/TranslationBinder;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private static final 〇o〇(Lcom/intsig/camscanner/ads_new/view/TranslationBinder;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "AppLaunchAdActivity_trans"

    .line 7
    .line 8
    const-string v1, "time out close page"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    iget-object p0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->〇00()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method


# virtual methods
.method public final commandMsg(Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/ads_new/view/TranslationBinder$CommandMsg;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "msg"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "on commandMsg--------"

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "AppLaunchAdActivity_trans"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 29
    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->O8:Ljava/lang/Runnable;

    .line 33
    .line 34
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    sget-object v0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder$WhenMappings;->〇080:[I

    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    aget p1, v0, p1

    .line 47
    .line 48
    const/4 v0, 0x1

    .line 49
    if-eq p1, v0, :cond_2

    .line 50
    .line 51
    const/4 v0, 0x2

    .line 52
    if-eq p1, v0, :cond_1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    const-string p1, "command close page"

    .line 56
    .line 57
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 61
    .line 62
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->〇00()V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    const-string p1, "command show ad"

    .line 67
    .line 68
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    new-instance p1, Lcom/intsig/camscanner/ads_new/view/AdBinder;

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 74
    .line 75
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/ads_new/view/AdBinder;-><init>(Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o〇:Ljava/lang/String;

    .line 79
    .line 80
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/ads_new/view/AdBinder;->Oo08(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public final 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "posId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o〇:Ljava/lang/String;

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/launcher/LauncherView;->〇080(Landroid/app/Activity;)Landroid/widget/LinearLayout;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/16 v0, 0x51

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 20
    .line 21
    .line 22
    const v0, 0x7f0a04c9

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    check-cast v0, Landroid/widget/ImageView;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 32
    .line 33
    invoke-virtual {v1, p1}, Landroidx/appcompat/app/AppCompatActivity;->setContentView(Landroid/view/View;)V

    .line 34
    .line 35
    .line 36
    new-instance p1, Lcom/intsig/PrivateMethodImp;

    .line 37
    .line 38
    invoke-direct {p1}, Lcom/intsig/PrivateMethodImp;-><init>()V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 42
    .line 43
    sget-object v2, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {p1, v1, v2, v0}, Lcom/intsig/PrivateMethodImp;->〇080(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;)V

    .line 46
    .line 47
    .line 48
    new-instance p1, Landroid/os/Handler;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇080:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->O8:Ljava/lang/Runnable;

    .line 60
    .line 61
    const-wide/16 v1, 0x7d0

    .line 62
    .line 63
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 64
    .line 65
    .line 66
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/TranslationBinder;->〇o00〇〇Oo:Landroid/os/Handler;

    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

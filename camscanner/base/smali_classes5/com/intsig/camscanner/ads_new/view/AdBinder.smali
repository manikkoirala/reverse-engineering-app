.class public final Lcom/intsig/camscanner/ads_new/view/AdBinder;
.super Ljava/lang/Object;
.source "AdBinder.kt"

# interfaces
.implements Landroidx/lifecycle/DefaultLifecycleObserver;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Z

.field private OO:Lcom/intsig/advertisement/interfaces/RealRequestAbs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;"
        }
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Z

.field private final 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/advertisement/enums/SourceType;

.field private final 〇0O:Lcom/intsig/camscanner/ads_new/view/AdBinder$adShowListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/advertisement/enums/PositionType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "appLaunchAdActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/advertisement/enums/PositionType;->AppLaunch:Lcom/intsig/advertisement/enums/PositionType;

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇OOo8〇0:Lcom/intsig/advertisement/enums/PositionType;

    .line 14
    .line 15
    const-string p1, "AppLaunchAdActivity_ad_binder"

    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇080OO8〇0:Ljava/lang/String;

    .line 18
    .line 19
    new-instance p1, Lcom/intsig/camscanner/ads_new/view/AdBinder$adShowListener$1;

    .line 20
    .line 21
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/ads_new/view/AdBinder$adShowListener$1;-><init>(Lcom/intsig/camscanner/ads_new/view/AdBinder;)V

    .line 22
    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇0O:Lcom/intsig/camscanner/ads_new/view/AdBinder$adShowListener$1;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/ads_new/view/AdBinder;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o〇00O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final oO80(Lcom/intsig/camscanner/ads_new/view/AdBinder;Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$this_apply"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 14
    .line 15
    .line 16
    move-result p0

    .line 17
    if-nez p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;->Oooo8o0〇()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/ads_new/view/AdBinder;Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/ads_new/view/AdBinder;->oO80(Lcom/intsig/camscanner/ads_new/view/AdBinder;Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/ads_new/view/AdBinder;)Lcom/intsig/advertisement/enums/SourceType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇08O〇00〇o:Lcom/intsig/advertisement/enums/SourceType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/ads_new/view/AdBinder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->O8o08O8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final 〇〇888()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->OO:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇0O:Lcom/intsig/camscanner/ads_new/view/AdBinder$adShowListener$1;

    .line 8
    .line 9
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;-><init>(Landroid/app/Activity;Lcom/intsig/advertisement/interfaces/RealRequestAbs;Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 13
    .line 14
    const v2, 0x7f080fb7

    .line 15
    .line 16
    .line 17
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->OO:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 24
    .line 25
    invoke-static {v2, v3}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇oo〇(Landroid/content/Context;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;->〇00(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 30
    .line 31
    .line 32
    new-instance v1, L〇00O0O0/〇080;

    .line 33
    .line 34
    invoke-direct {v1, p0, v0}, L〇00O0O0/〇080;-><init>(Lcom/intsig/camscanner/ads_new/view/AdBinder;Lcom/intsig/camscanner/ads_new/view/AppLaunchAdContainer;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 41
    .line 42
    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AppCompatActivity;->setContentView(Landroid/view/View;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method


# virtual methods
.method public final Oo08(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "posId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->AppLaunch:Lcom/intsig/advertisement/enums/PositionType;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, 0x0

    .line 17
    const/4 v3, 0x0

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇OOo8〇0:Lcom/intsig/advertisement/enums/PositionType;

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-virtual {p1, v3}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->OOO〇O0(I)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->ShareDone:Lcom/intsig/advertisement/enums/PositionType;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-eqz p1, :cond_1

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇OOo8〇0:Lcom/intsig/advertisement/enums/PositionType;

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1, v3}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->OOO〇O0(I)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    goto :goto_0

    .line 54
    :cond_1
    move-object p1, v2

    .line 55
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->OO:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 56
    .line 57
    if-nez p1, :cond_2

    .line 58
    .line 59
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇080OO8〇0:Ljava/lang/String;

    .line 60
    .line 61
    const-string v0, "ad is null and finish"

    .line 62
    .line 63
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->〇00()V

    .line 69
    .line 70
    .line 71
    return-void

    .line 72
    :cond_2
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const-string v0, "requestAdABs!!.requestParam.sourceType"

    .line 84
    .line 85
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    iput-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇08O〇00〇o:Lcom/intsig/advertisement/enums/SourceType;

    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 91
    .line 92
    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 97
    .line 98
    .line 99
    :try_start_0
    invoke-direct {p0}, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇〇888()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :catch_0
    move-exception p1

    .line 104
    sget-object v0, Lcom/intsig/advertisement/crash/AdCrashManager;->〇080:Lcom/intsig/advertisement/crash/AdCrashManager;

    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->〇08O〇00〇o:Lcom/intsig/advertisement/enums/SourceType;

    .line 107
    .line 108
    if-nez v1, :cond_3

    .line 109
    .line 110
    const-string v1, "mSourceType"

    .line 111
    .line 112
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    goto :goto_1

    .line 116
    :cond_3
    move-object v2, v1

    .line 117
    :goto_1
    invoke-virtual {v0, v2, p1}, Lcom/intsig/advertisement/crash/AdCrashManager;->〇080(Lcom/intsig/advertisement/enums/SourceType;Ljava/lang/Exception;)V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->〇00()V

    .line 123
    .line 124
    .line 125
    :goto_2
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
.end method

.method public synthetic onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇080(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public synthetic onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o00〇〇Oo(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o〇(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->O8o08O8O:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->O8(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->O8o08O8O:Z

    .line 11
    .line 12
    iget-boolean p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o〇00O:Z

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->〇00()V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public synthetic onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->Oo08(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public synthetic onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->o〇0(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public final o〇0()Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ads_new/view/AdBinder;->o0:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

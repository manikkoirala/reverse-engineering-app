.class public Lcom/intsig/camscanner/attention/CallAppData;
.super Lcom/intsig/tianshu/base/BaseJsonObj;
.source "CallAppData.java"


# static fields
.field public static final ACTION_APP_INVITE_V2:Ljava/lang/String; = "invite_v2"

.field public static final ACTION_APP_PAY:Ljava/lang/String; = "app_pay"

.field public static final ACTION_APP_REFERRAL:Ljava/lang/String; = "app_referral"

.field public static final ACTION_BACK_VIEW:Ljava/lang/String; = "back_view"

.field public static final ACTION_BEGIN_SYNC:Ljava/lang/String; = "begin_sync"

.field public static final ACTION_CHOOSE_IMAGE:Ljava/lang/String; = "choose_image"

.field public static final ACTION_CLEAR_LOCAL_DOC:Ljava/lang/String; = "clear_local_doc"

.field public static final ACTION_CLOSE_ACCOUNT:Ljava/lang/String; = "close_account"

.field public static final ACTION_CLOSE_SHARE:Ljava/lang/String; = "close_share"

.field public static final ACTION_CLOSE_WEBVIEW:Ljava/lang/String; = "close_view"

.field public static final ACTION_COPY_TO_CLIPBOARD:Ljava/lang/String; = "copy_to_clipboard"

.field public static final ACTION_DISABLE_BACK_BUTTON:Ljava/lang/String; = "disable_back_button"

.field public static final ACTION_DONE:Ljava/lang/String; = "done"

.field public static final ACTION_EDU_VERIFY:Ljava/lang/String; = "edu_verify"

.field public static final ACTION_ENABLE_BACK_BUTTON:Ljava/lang/String; = "enable_back_button"

.field public static final ACTION_EVENT_TRACKING:Ljava/lang/String; = "event_tracking"

.field public static final ACTION_EXCEL_BATCH_SHARE_FILE:Ljava/lang/String; = "batch_share_file"

.field public static final ACTION_FINISH_CLOSE_ACCOUNT:Ljava/lang/String; = "finish_close_account"

.field public static final ACTION_FOLLOW_WECHAT:Ljava/lang/String; = "open_wechat_offical"

.field public static final ACTION_FREEZE_ACCOUNT:Ljava/lang/String; = "freeze_account"

.field public static final ACTION_GET_ACCOUNT:Ljava/lang/String; = "get_account"

.field public static final ACTION_GET_APIS_DOMAIN:Ljava/lang/String; = "get_apis_domain"

.field public static final ACTION_GET_DOC_ID:Ljava/lang/String; = "get_doc_id"

.field public static final ACTION_GET_INTERFACE_DATA:Ljava/lang/String; = "get_interface_data"

.field public static final ACTION_GET_OCR_DETAIL:Ljava/lang/String; = "get_ocr_detail"

.field public static final ACTION_GET_REWARD:Ljava/lang/String; = "get_reward"

.field public static final ACTION_GET_THUMBNAIL_IMAGE:Ljava/lang/String; = "get_thumbnail_image"

.field public static final ACTION_GET_TOKEN:Ljava/lang/String; = "get_token"

.field public static final ACTION_HAS_TEAM:Ljava/lang/String; = "has_team"

.field public static final ACTION_INVITE_WX_FRIEND:Ljava/lang/String; = "invite_wxfriend"

.field public static final ACTION_JUMP:Ljava/lang/String; = "jump"

.field public static final ACTION_OFFICE_SHARE_FILE:Ljava/lang/String; = "share_file"

.field public static final ACTION_OPEN_YOUZAN:Ljava/lang/String; = "enter_youzan_mall"

.field public static final ACTION_PAY_RESULT:Ljava/lang/String; = "pay_result"

.field public static final ACTION_PERSONALIZED_AD:Ljava/lang/String; = "personalized_ad"

.field public static final ACTION_PREMIUM_PAY:Ljava/lang/String; = "premium_pay"

.field public static final ACTION_QUERY_EXCEL_FILES_DIRECTORY:Ljava/lang/String; = "query_files_directory"

.field public static final ACTION_RATING:Ljava/lang/String; = "rating"

.field public static final ACTION_RECEIVE_COUPON:Ljava/lang/String; = "receive_coupon"

.field public static final ACTION_RECOMMEND:Ljava/lang/String; = "recommend"

.field public static final ACTION_REFRESH_EDU:Ljava/lang/String; = "refresh_edu"

.field public static final ACTION_SAVE_IMG:Ljava/lang/String; = "save_img"

.field public static final ACTION_SET_RENEW_VALID:Ljava/lang/String; = "set_renew_valid"

.field public static final ACTION_SHARE_QQ:Ljava/lang/String; = "share_qq"

.field public static final ACTION_SHARE_RESOURCE:Ljava/lang/String; = "share_resource"

.field public static final ACTION_SHARE_WX:Ljava/lang/String; = "share_wx"

.field public static final ACTION_SHOW_AD_VIDEO:Ljava/lang/String; = "show_ad"

.field public static final ACTION_SHOW_DETAIN:Ljava/lang/String; = "show_detain"

.field public static final ACTION_SWITCH_TO_TAB:Ljava/lang/String; = "switch_to_tab"

.field public static final ACTION_TOAST:Ljava/lang/String; = "toast"

.field public static final ACTION_UNFREEZE_ACCOUNT:Ljava/lang/String; = "unfreeze_account"

.field public static final ACTION_UPDATE_ACCOUNT:Ljava/lang/String; = "update_account"

.field public static final ACTION_UPDATE_USER_INFO:Ljava/lang/String; = "update_user_info"

.field public static final ACTION_UPLOAD_FAQ_INFO:Ljava/lang/String; = "upload_faq_info"

.field public static final ACTION_UPLOAD_LOG_ID:Ljava/lang/String; = "upload_log_id"

.field public static final ACTION_USA_SHARE:Ljava/lang/String; = "usa_share"

.field public static final ACTION_WATCH_AD_LOTTERY:Ljava/lang/String; = "watchad_lottery"

.field public static final ACTION_WEB_TEST:Ljava/lang/String; = "web_test"

.field public static final ACTION_WEB_VERIFY:Ljava/lang/String; = "web_verify"

.field public static final ACTION_WECHAT_BIND:Ljava/lang/String; = "bind_weixin"

.field public static final ACTION_WEIXIN:Ljava/lang/String; = "activity_wx"

.field public static final ACTION_WE_CHAT_FORWARDING:Ljava/lang/String; = "open_wechat"

.field public static final ADD_WIDGET:Ljava/lang/String; = "add_widget"

.field public static final CANCEL_SHARE_DIRECTORY:Ljava/lang/String; = "cancel_share_directory"

.field public static final CHECK_ACCOUNT_MERGE:Ljava/lang/String; = "check_account_merge"

.field public static final CS_AI_DATA:Ljava/lang/String; = "give_document_info"

.field public static final EXIT_SHARE_DIRECTORY:Ljava/lang/String; = "exit_share_directory"

.field public static final GET_DOCUMENT_NUM:Ljava/lang/String; = "get_document_num"

.field public static final GET_NOTICE_STATUS:Ljava/lang/String; = "get_notice_status"

.field public static final GET_OFFLINE_DATA:Ljava/lang/String; = "get_offline_data"

.field public static final GO_BIND_PHONE:Ljava/lang/String; = "go_bind_phone"

.field public static final GO_CAM_EXAM:Ljava/lang/String; = "goto_camexam"

.field public static final GO_LOGIN:Ljava/lang/String; = "go_login"

.field public static final INVITE_JOIN_DIRECTORY:Ljava/lang/String; = "invite_join_directory"

.field public static final NOTICE:Ljava/lang/String; = "notice"

.field public static final OPEN_USER_AUTH:Ljava/lang/String; = "open_user_auth"

.field public static final SHARE_PANEL:Ljava/lang/String; = "share_panel"

.field public static final SHARE_WEB_RESOURCE:Ljava/lang/String; = "share_web_resource"

.field public static final START_DATA_MIGRATION:Ljava/lang/String; = "start_data_migration"

.field public static final STRIPE_PAY_SUCCESS:Ljava/lang/String; = "stripe_pay_success"

.field private static final TAG:Ljava/lang/String; = "CallAppData"

.field public static final WEB_CS_PDF_RIVER:Ljava/lang/String; = "cs_pdf_river"

.field public static final WEB_DATA_RIVER_TYPE:Ljava/lang/String; = "pdf_river_type"

.field public static final WEB_JUMP_FUN:Ljava/lang/String; = "jump_fn"

.field public static final WEB_LOG_PRINT:Ljava/lang/String; = "web_log_print"


# instance fields
.field public action:Ljava/lang/String;

.field public closeWeb:Z

.field public close_web:I

.field private currentUrl:Ljava/lang/String;

.field public data:Ljava/lang/String;

.field public id:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/attention/CallAppData;-><init>(Lorg/json/JSONObject;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/attention/CallAppData;-><init>(Lorg/json/JSONObject;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/tianshu/base/BaseJsonObj;-><init>(Lorg/json/JSONObject;)V

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/attention/CallAppData;->currentUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public decodeData(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    :try_start_0
    new-instance v0, Ljava/lang/String;

    .line 8
    .line 9
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-static {p1, v1}, Lcom/intsig/utils/Base64FromCC;->〇080([BI)[B

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p1}, Lcom/intsig/utils/AESNopadding;->〇080([B)[B

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    goto :goto_0

    .line 30
    :catch_0
    move-exception p1

    .line 31
    sget-object v0, Lcom/intsig/camscanner/attention/CallAppData;->TAG:Ljava/lang/String;

    .line 32
    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    const/4 p1, 0x0

    .line 37
    :goto_0
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public getCurrentUrl()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/attention/CallAppData;->currentUrl:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public isShouldCloseWebActivity()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/attention/CallAppData;->close_web:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.class public Lcom/intsig/camscanner/attention/PremiumBuy;
.super Ljava/lang/Object;
.source "PremiumBuy.java"


# instance fields
.field private O8:Z

.field Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private o〇0:Landroid/os/CountDownTimer;

.field private 〇080:Landroid/app/Activity;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/attention/CallAppData;

.field private 〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private 〇〇888:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/attention/CallAppData;)V
    .locals 13

    .line 1
    const-string v0, "page_status"

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x1

    .line 9
    iput-boolean v2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->O8:Z

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    iput-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 13
    .line 14
    new-instance v3, Lcom/intsig/camscanner/attention/PremiumBuy$1;

    .line 15
    .line 16
    const-wide/16 v6, 0x7d0

    .line 17
    .line 18
    const-wide/16 v8, 0xa

    .line 19
    .line 20
    move-object v4, v3

    .line 21
    move-object v5, p0

    .line 22
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/attention/PremiumBuy$1;-><init>(Lcom/intsig/camscanner/attention/PremiumBuy;JJ)V

    .line 23
    .line 24
    .line 25
    iput-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->o〇0:Landroid/os/CountDownTimer;

    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇080:Landroid/app/Activity;

    .line 28
    .line 29
    iput-object p2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o00〇〇Oo:Lcom/intsig/camscanner/attention/CallAppData;

    .line 30
    .line 31
    iget-object v3, p2, Lcom/intsig/camscanner/attention/CallAppData;->data:Ljava/lang/String;

    .line 32
    .line 33
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    if-eqz v4, :cond_0

    .line 38
    .line 39
    return-void

    .line 40
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v5, "data = "

    .line 46
    .line 47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    const-string v5, "PremiumBuy"

    .line 58
    .line 59
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    const/4 v4, 0x0

    .line 63
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 64
    .line 65
    .line 66
    move-result-object v6

    .line 67
    if-eqz v6, :cond_1

    .line 68
    .line 69
    const-string v7, "purchase_tracker"

    .line 70
    .line 71
    invoke-virtual {v6, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 72
    .line 73
    .line 74
    move-result-object v6

    .line 75
    instance-of v7, v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 76
    .line 77
    if-eqz v7, :cond_1

    .line 78
    .line 79
    check-cast v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 80
    .line 81
    iput-object v6, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 82
    .line 83
    const/4 v6, 0x1

    .line 84
    goto :goto_0

    .line 85
    :cond_1
    const/4 v6, 0x0

    .line 86
    :goto_0
    iget-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 87
    .line 88
    if-nez v7, :cond_2

    .line 89
    .line 90
    new-instance v7, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 91
    .line 92
    invoke-direct {v7}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 93
    .line 94
    .line 95
    iput-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 96
    .line 97
    :cond_2
    iget-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 98
    .line 99
    sget-object v8, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPage:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 100
    .line 101
    invoke-virtual {v7, v8}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 102
    .line 103
    .line 104
    iget-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 105
    .line 106
    iget-object v8, v7, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 107
    .line 108
    sget-object v9, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 109
    .line 110
    if-ne v8, v9, :cond_3

    .line 111
    .line 112
    sget-object v8, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 113
    .line 114
    iput-object v8, v7, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 115
    .line 116
    :cond_3
    new-instance v7, Lorg/json/JSONObject;

    .line 117
    .line 118
    invoke-direct {v7, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 122
    .line 123
    const-string v8, "coupon"

    .line 124
    .line 125
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v8

    .line 129
    iput-object v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->couponId:Ljava/lang/String;

    .line 130
    .line 131
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 132
    .line 133
    const-string v8, "show_expired"

    .line 134
    .line 135
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 136
    .line 137
    .line 138
    move-result v8

    .line 139
    iput v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->showExpire:I

    .line 140
    .line 141
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 142
    .line 143
    const-string v8, "act_1"

    .line 144
    .line 145
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 146
    .line 147
    .line 148
    move-result v8

    .line 149
    iput v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->act_1:I

    .line 150
    .line 151
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 152
    .line 153
    const-string v8, "buy_note"

    .line 154
    .line 155
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v8

    .line 159
    iput-object v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->buy_note:Ljava/lang/String;

    .line 160
    .line 161
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 162
    .line 163
    const-string v8, "scheme_type"

    .line 164
    .line 165
    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v8

    .line 169
    iput-object v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme_type:Ljava/lang/String;

    .line 170
    .line 171
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 172
    .line 173
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v8

    .line 177
    iput-object v8, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;

    .line 178
    .line 179
    const-string v3, "extra"

    .line 180
    .line 181
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v3

    .line 185
    iput-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇〇888:Ljava/lang/String;

    .line 186
    .line 187
    const-string v3, "show_vip_type"

    .line 188
    .line 189
    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4

    .line 193
    :try_start_1
    const-string v8, "is_claim_gift"

    .line 194
    .line 195
    invoke-virtual {v7, v8, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    .line 196
    .line 197
    .line 198
    move-result v8
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3

    .line 199
    :try_start_2
    const-string v9, "actionType"

    .line 200
    .line 201
    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v9
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 205
    :try_start_3
    new-instance v10, Ljava/lang/StringBuilder;

    .line 206
    .line 207
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    .line 209
    .line 210
    const-string v11, "extra is"

    .line 211
    .line 212
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    iget-object v11, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇〇888:Ljava/lang/String;

    .line 216
    .line 217
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    .line 222
    .line 223
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 228
    .line 229
    .line 230
    const-string v1, "from_part"

    .line 231
    .line 232
    const-string v10, "from"

    .line 233
    .line 234
    if-nez v6, :cond_6

    .line 235
    .line 236
    :try_start_4
    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 237
    .line 238
    .line 239
    move-result-object v6

    .line 240
    const-string v11, "cs_esignatures_h5"

    .line 241
    .line 242
    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 243
    .line 244
    .line 245
    move-result v11

    .line 246
    if-eqz v11, :cond_4

    .line 247
    .line 248
    iget-object v6, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 249
    .line 250
    sget-object v11, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 251
    .line 252
    invoke-virtual {v6, v11}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 253
    .line 254
    .line 255
    iget-object v6, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 256
    .line 257
    sget-object v11, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 258
    .line 259
    iput-object v11, v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 260
    .line 261
    sget-object v11, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_ESIGNATURES_H5:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 262
    .line 263
    iput-object v11, v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 264
    .line 265
    goto :goto_1

    .line 266
    :cond_4
    sget-object v11, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ICLOUD_GUIDE_WEB:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 267
    .line 268
    invoke-virtual {v11}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 269
    .line 270
    .line 271
    move-result-object v12

    .line 272
    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 273
    .line 274
    .line 275
    move-result v6

    .line 276
    if-eqz v6, :cond_5

    .line 277
    .line 278
    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object v6

    .line 282
    iget-object v12, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 283
    .line 284
    invoke-static {v6}, Lcom/intsig/camscanner/purchase/entity/Function;->getFunction(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/entity/Function;

    .line 285
    .line 286
    .line 287
    move-result-object v6

    .line 288
    iput-object v6, v12, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 289
    .line 290
    iget-object v6, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 291
    .line 292
    iput-object v11, v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 293
    .line 294
    goto :goto_1

    .line 295
    :cond_5
    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 296
    .line 297
    .line 298
    move-result-object v6

    .line 299
    sget-object v11, Lcom/intsig/camscanner/purchase/entity/Function;->BACK_SCHOOL_GIFT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 300
    .line 301
    iget-object v12, v11, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    .line 302
    .line 303
    invoke-static {v12, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 304
    .line 305
    .line 306
    move-result v6

    .line 307
    if-eqz v6, :cond_6

    .line 308
    .line 309
    iget-object v6, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 310
    .line 311
    iput-object v11, v6, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 312
    .line 313
    :cond_6
    :goto_1
    const-string v6, "web_track_param"

    .line 314
    .line 315
    invoke-virtual {v7, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    .line 316
    .line 317
    .line 318
    move-result-object v6
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    .line 319
    if-eqz v6, :cond_b

    .line 320
    .line 321
    :try_start_5
    const-string v7, "page_id"

    .line 322
    .line 323
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v7

    .line 327
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 328
    .line 329
    .line 330
    move-result v11

    .line 331
    if-nez v11, :cond_7

    .line 332
    .line 333
    iget-object v11, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 334
    .line 335
    sget-object v12, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSFakePage:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 336
    .line 337
    invoke-virtual {v12, v7}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 338
    .line 339
    .line 340
    move-result-object v7

    .line 341
    invoke-virtual {v11, v7}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 342
    .line 343
    .line 344
    :cond_7
    const-string v7, "scheme"

    .line 345
    .line 346
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 347
    .line 348
    .line 349
    move-result-object v7

    .line 350
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 351
    .line 352
    .line 353
    move-result v11

    .line 354
    if-nez v11, :cond_8

    .line 355
    .line 356
    iget-object v11, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 357
    .line 358
    sget-object v12, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->FAKE_SCHEME:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 359
    .line 360
    invoke-virtual {v12, v7}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 361
    .line 362
    .line 363
    move-result-object v7

    .line 364
    invoke-virtual {v11, v7}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 365
    .line 366
    .line 367
    :cond_8
    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 368
    .line 369
    .line 370
    move-result-object v1

    .line 371
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 372
    .line 373
    .line 374
    move-result v7

    .line 375
    if-nez v7, :cond_9

    .line 376
    .line 377
    iget-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 378
    .line 379
    sget-object v11, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_FAKE_ENTRANCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 380
    .line 381
    invoke-virtual {v11, v1}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 382
    .line 383
    .line 384
    move-result-object v1

    .line 385
    invoke-virtual {v7, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 386
    .line 387
    .line 388
    :cond_9
    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 389
    .line 390
    .line 391
    move-result-object v1

    .line 392
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 393
    .line 394
    .line 395
    move-result v7

    .line 396
    if-nez v7, :cond_a

    .line 397
    .line 398
    iget-object v7, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 399
    .line 400
    sget-object v10, Lcom/intsig/camscanner/purchase/entity/Function;->FAKE_FUNCTION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 401
    .line 402
    invoke-virtual {v10, v1}, Lcom/intsig/camscanner/purchase/entity/Function;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/entity/Function;

    .line 403
    .line 404
    .line 405
    move-result-object v1

    .line 406
    invoke-virtual {v7, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 407
    .line 408
    .line 409
    :cond_a
    iget-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 410
    .line 411
    const-string v7, "user_status"

    .line 412
    .line 413
    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 414
    .line 415
    .line 416
    move-result-object v7

    .line 417
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->user_status:Ljava/lang/String;

    .line 418
    .line 419
    iget-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 420
    .line 421
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 422
    .line 423
    .line 424
    move-result-object v0

    .line 425
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    .line 426
    .line 427
    const/4 v0, 0x1

    .line 428
    goto :goto_5

    .line 429
    :catch_0
    move-exception v0

    .line 430
    const/4 v1, 0x1

    .line 431
    goto :goto_4

    .line 432
    :cond_b
    const/4 v0, 0x0

    .line 433
    goto :goto_5

    .line 434
    :catch_1
    move-exception v0

    .line 435
    goto :goto_2

    .line 436
    :catch_2
    move-exception v0

    .line 437
    move-object v9, v1

    .line 438
    :goto_2
    const/4 v1, 0x0

    .line 439
    goto :goto_4

    .line 440
    :catch_3
    move-exception v0

    .line 441
    move-object v9, v1

    .line 442
    goto :goto_3

    .line 443
    :catch_4
    move-exception v0

    .line 444
    move-object v3, v1

    .line 445
    move-object v9, v3

    .line 446
    :goto_3
    const/4 v1, 0x0

    .line 447
    const/4 v8, 0x0

    .line 448
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 449
    .line 450
    .line 451
    move-result-object v0

    .line 452
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    .line 454
    .line 455
    move v0, v1

    .line 456
    :goto_5
    move-object v10, v3

    .line 457
    move-object v11, v9

    .line 458
    new-instance v1, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 459
    .line 460
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 461
    .line 462
    invoke-direct {v1, p1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 463
    .line 464
    .line 465
    iput-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 466
    .line 467
    iget-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇〇888:Ljava/lang/String;

    .line 468
    .line 469
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    .line 470
    .line 471
    .line 472
    move-result v1

    .line 473
    if-nez v1, :cond_c

    .line 474
    .line 475
    iget-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 476
    .line 477
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇〇888:Ljava/lang/String;

    .line 478
    .line 479
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o〇O(Ljava/lang/String;)V

    .line 480
    .line 481
    .line 482
    :cond_c
    if-eqz v0, :cond_d

    .line 483
    .line 484
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 485
    .line 486
    iput-boolean v4, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OoO8:Z

    .line 487
    .line 488
    :cond_d
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 489
    .line 490
    iput-boolean v8, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇0〇O0088o:Z

    .line 491
    .line 492
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 493
    .line 494
    .line 495
    move-result v0

    .line 496
    if-nez v0, :cond_e

    .line 497
    .line 498
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 499
    .line 500
    iput-boolean v2, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o800o8O:Z

    .line 501
    .line 502
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    .line 503
    .line 504
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 505
    .line 506
    .line 507
    const-string v1, "buy, isClaimGift: "

    .line 508
    .line 509
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    .line 511
    .line 512
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 513
    .line 514
    .line 515
    const-string v1, ", showVipType: "

    .line 516
    .line 517
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    .line 519
    .line 520
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521
    .line 522
    .line 523
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 524
    .line 525
    .line 526
    move-result-object v0

    .line 527
    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    .line 529
    .line 530
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 531
    .line 532
    new-instance v1, Lcom/intsig/camscanner/attention/oo88o8O;

    .line 533
    .line 534
    move-object v6, v1

    .line 535
    move-object v7, p0

    .line 536
    move-object v9, p1

    .line 537
    move-object v12, p2

    .line 538
    invoke-direct/range {v6 .. v12}, Lcom/intsig/camscanner/attention/oo88o8O;-><init>(Lcom/intsig/camscanner/attention/PremiumBuy;ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/attention/CallAppData;)V

    .line 539
    .line 540
    .line 541
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 542
    .line 543
    .line 544
    return-void
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
.end method

.method private static O8(Landroid/app/Activity;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const-string p2, "1"

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p2, "2"

    .line 7
    .line 8
    :goto_0
    :try_start_0
    instance-of v0, p0, Lcom/intsig/webview/WebViewActivity;

    .line 9
    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    move-object v0, p0

    .line 13
    check-cast v0, Lcom/intsig/webview/WebViewActivity;

    .line 14
    .line 15
    new-instance v1, Lorg/json/JSONObject;

    .line 16
    .line 17
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "product_id"

    .line 21
    .line 22
    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 23
    .line 24
    .line 25
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 26
    .line 27
    .line 28
    move-result p0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    const-string p1, "intsig_key"

    .line 30
    .line 31
    if-eqz p0, :cond_1

    .line 32
    .line 33
    :try_start_1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p0

    .line 37
    invoke-static {p0}, Lcom/intsig/utils/AESNopadding;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    invoke-virtual {v1, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 42
    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    const-string p0, ""

    .line 46
    .line 47
    invoke-virtual {v1, p1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 48
    .line 49
    .line 50
    :goto_1
    const-string p0, "encrypt_uid"

    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-static {p1}, Lcom/intsig/utils/AESNopadding;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {v1, p0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    .line 62
    .line 63
    const-string p0, "status"

    .line 64
    .line 65
    invoke-virtual {v1, p0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 66
    .line 67
    .line 68
    new-instance p0, Lorg/json/JSONObject;

    .line 69
    .line 70
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 71
    .line 72
    .line 73
    const-string p1, "id"

    .line 74
    .line 75
    invoke-virtual {p0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 76
    .line 77
    .line 78
    const-string p1, "ret"

    .line 79
    .line 80
    invoke-virtual {p0, p1, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    invoke-virtual {v0, p0}, Lcom/intsig/webview/WebViewActivity;->o0Oo(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 88
    .line 89
    .line 90
    goto :goto_2

    .line 91
    :catch_0
    move-exception p0

    .line 92
    const-string p1, "PremiumBuy"

    .line 93
    .line 94
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 95
    .line 96
    .line 97
    :cond_2
    :goto_2
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

.method private Oo08(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string p1, "PremiumBuy"

    .line 14
    .line 15
    const-string p2, "productId is null"

    .line 16
    .line 17
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇080:Landroid/app/Activity;

    .line 21
    .line 22
    const p2, 0x7f1302fe

    .line 23
    .line 24
    .line 25
    invoke-static {p1, p2}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    const/4 v1, 0x1

    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇080:Landroid/app/Activity;

    .line 37
    .line 38
    invoke-static {p3}, Lcom/intsig/camscanner/app/AppSwitch;->〇o〇(Landroid/content/Context;)I

    .line 39
    .line 40
    .line 41
    move-result p3

    .line 42
    if-eq p3, v1, :cond_1

    .line 43
    .line 44
    const/4 p3, 0x4

    .line 45
    goto :goto_0

    .line 46
    :cond_1
    const/4 p3, 0x1

    .line 47
    goto :goto_0

    .line 48
    :cond_2
    invoke-static {p3}, Lcom/intsig/camscanner/attention/PremiumBuy;->oO80(Ljava/lang/String;)I

    .line 49
    .line 50
    .line 51
    move-result p3

    .line 52
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 53
    .line 54
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 55
    .line 56
    .line 57
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 58
    .line 59
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->Ooo(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 63
    .line 64
    invoke-virtual {p3, p5}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->oO00OOO(I)V

    .line 65
    .line 66
    .line 67
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 68
    .line 69
    invoke-virtual {p3, p6}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o〇8oOO88(Z)V

    .line 70
    .line 71
    .line 72
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 73
    .line 74
    invoke-virtual {p3, p7}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o〇O(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 78
    .line 79
    invoke-virtual {p3, p8}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->ooo〇8oO(Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object p3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 83
    .line 84
    iput-boolean v1, p3, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->oo88o8O:Z

    .line 85
    .line 86
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 87
    .line 88
    .line 89
    move-result p3

    .line 90
    if-eqz p3, :cond_3

    .line 91
    .line 92
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 93
    .line 94
    .line 95
    move-result p3

    .line 96
    if-nez p3, :cond_3

    .line 97
    .line 98
    invoke-static {p1}, Lcom/intsig/comm/purchase/entity/ProductEnum;->switchWebProductId(Ljava/lang/String;)Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 99
    .line 100
    .line 101
    move-result-object p1

    .line 102
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->OoO8(Lcom/intsig/comm/purchase/entity/ProductEnum;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    iget-object p2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 107
    .line 108
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O8O〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;)V

    .line 109
    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 113
    .line 114
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    :goto_1
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
.end method

.method public static oO80(Ljava/lang/String;)I
    .locals 1

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    .line 2
    .line 3
    .line 4
    const-string v0, "alipay"

    .line 5
    .line 6
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    const-string v0, "wechat"

    .line 13
    .line 14
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result p0

    .line 18
    if-nez p0, :cond_0

    .line 19
    .line 20
    const/4 p0, 0x4

    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 p0, 0x2

    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 p0, 0x1

    .line 25
    :goto_0
    return p0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private o〇0(Ljava/lang/String;)Z
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "CamScanner_NADVIP"

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    const/4 p1, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/attention/PremiumBuy;ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/attention/CallAppData;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p7}, Lcom/intsig/camscanner/attention/PremiumBuy;->〇〇888(ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/attention/CallAppData;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/attention/PremiumBuy;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->O8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇〇888(ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/attention/CallAppData;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "buy end, isClaimGift: "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ", success: "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "PremiumBuy"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {p2}, Lcom/intsig/camscanner/app/AppSwitch;->OO0o〇〇〇〇0(Landroid/content/Context;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const/4 v2, 0x1

    .line 36
    const/4 v3, 0x0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    invoke-static {p7, v3, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇oOO8O8(ZZZ)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_0

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 46
    .line 47
    invoke-static {p2, p1}, Lcom/intsig/camscanner/purchase/activity/GPRedeemActivity;->startActivity(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-static {p7, v3}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇0000OOO(ZZ)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    invoke-static {p2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇o(Landroid/app/Activity;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    if-eqz p1, :cond_2

    .line 62
    .line 63
    if-eqz p7, :cond_2

    .line 64
    .line 65
    const-string p1, "svip"

    .line 66
    .line 67
    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result p1

    .line 71
    invoke-static {p2, p1}, Lcom/intsig/camscanner/purchase/GpWebPurchaseManager;->OO0o〇〇〇〇0(Landroid/app/Activity;Z)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    const/4 p3, -0x1

    .line 80
    if-nez p1, :cond_3

    .line 81
    .line 82
    if-eqz p7, :cond_3

    .line 83
    .line 84
    invoke-static {p2, p4}, Lcom/intsig/camscanner/purchase/manager/WebPurchaseBuySuccessManager;->〇080(Landroid/app/Activity;Ljava/lang/String;)Z

    .line 85
    .line 86
    .line 87
    move-result p1

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    .line 89
    .line 90
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .line 92
    .line 93
    const-string v3, "buy end, finalActionType: "

    .line 94
    .line 95
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    const-string p4, ", showDialog: "

    .line 102
    .line 103
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p2, p3}, Landroid/app/Activity;->setResult(I)V

    .line 117
    .line 118
    .line 119
    iget-object p1, p6, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 120
    .line 121
    iget-object p3, p5, Lcom/intsig/camscanner/attention/CallAppData;->id:Ljava/lang/String;

    .line 122
    .line 123
    invoke-static {p2, p1, v2, p3}, Lcom/intsig/camscanner/attention/PremiumBuy;->O8(Landroid/app/Activity;Ljava/lang/String;ZLjava/lang/String;)V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_3
    if-eqz p7, :cond_4

    .line 128
    .line 129
    const/4 v3, -0x1

    .line 130
    :cond_4
    invoke-virtual {p2, v3}, Landroid/app/Activity;->setResult(I)V

    .line 131
    .line 132
    .line 133
    iget-object p1, p6, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 134
    .line 135
    iget-object p3, p5, Lcom/intsig/camscanner/attention/CallAppData;->id:Ljava/lang/String;

    .line 136
    .line 137
    invoke-static {p2, p1, p7, p3}, Lcom/intsig/camscanner/attention/PremiumBuy;->O8(Landroid/app/Activity;Ljava/lang/String;ZLjava/lang/String;)V

    .line 138
    .line 139
    .line 140
    :goto_0
    iget-object p1, p6, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 141
    .line 142
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/attention/PremiumBuy;->o〇0(Ljava/lang/String;)Z

    .line 143
    .line 144
    .line 145
    move-result p1

    .line 146
    if-eqz p1, :cond_6

    .line 147
    .line 148
    if-eqz p7, :cond_5

    .line 149
    .line 150
    const-string p1, "buy_success"

    .line 151
    .line 152
    goto :goto_1

    .line 153
    :cond_5
    const-string p1, "buy_fail"

    .line 154
    .line 155
    :goto_1
    iget-object p2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 156
    .line 157
    iget-object p2, p2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->functionStr:Ljava/lang/String;

    .line 158
    .line 159
    const-string p3, "CSPremiumAdPage"

    .line 160
    .line 161
    const-string p4, "from_part"

    .line 162
    .line 163
    invoke-static {p3, p1, p4, p2}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    :cond_6
    return-void
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
.end method


# virtual methods
.method public 〇o〇()V
    .locals 13

    .line 1
    const-string v0, "s_first_pay"

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    iget-boolean v2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->O8:Z

    .line 6
    .line 7
    if-eqz v2, :cond_2

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    iput-boolean v2, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->O8:Z

    .line 11
    .line 12
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->o〇0:Landroid/os/CountDownTimer;

    .line 13
    .line 14
    invoke-virtual {v3}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 15
    .line 16
    .line 17
    iget-object v3, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇o00〇〇Oo:Lcom/intsig/camscanner/attention/CallAppData;

    .line 18
    .line 19
    iget-object v3, v3, Lcom/intsig/camscanner/attention/CallAppData;->data:Ljava/lang/String;

    .line 20
    .line 21
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v4, :cond_0

    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    .line 29
    .line 30
    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const-string v3, "product_id"

    .line 34
    .line 35
    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    .line 39
    :try_start_1
    const-string v5, "pay_product_id"

    .line 40
    .line 41
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 45
    :try_start_2
    const-string v6, "paytype"

    .line 46
    .line 47
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 51
    :try_start_3
    const-string v7, "product_price_str"

    .line 52
    .line 53
    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 57
    :try_start_4
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 58
    .line 59
    .line 60
    move-result v8
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 61
    :try_start_5
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    .line 62
    .line 63
    .line 64
    move-result v8

    .line 65
    const-string v0, "closeWeb"

    .line 66
    .line 67
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    const-string v0, "extra"

    .line 72
    .line 73
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v9
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 77
    :try_start_6
    const-string v0, "uuid"

    .line 78
    .line 79
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 83
    move-object v4, v6

    .line 84
    move v6, v8

    .line 85
    move-object v8, v9

    .line 86
    move-object v9, v0

    .line 87
    goto :goto_5

    .line 88
    :catch_0
    move-exception v0

    .line 89
    goto :goto_4

    .line 90
    :catch_1
    move-exception v0

    .line 91
    move-object v9, v1

    .line 92
    goto :goto_4

    .line 93
    :catch_2
    move-exception v0

    .line 94
    move-object v9, v1

    .line 95
    goto :goto_3

    .line 96
    :catch_3
    move-exception v0

    .line 97
    move-object v7, v1

    .line 98
    goto :goto_2

    .line 99
    :catch_4
    move-exception v0

    .line 100
    move-object v6, v1

    .line 101
    goto :goto_1

    .line 102
    :catch_5
    move-exception v0

    .line 103
    move-object v5, v1

    .line 104
    goto :goto_0

    .line 105
    :catch_6
    move-exception v0

    .line 106
    move-object v3, v1

    .line 107
    move-object v5, v3

    .line 108
    :goto_0
    move-object v6, v5

    .line 109
    :goto_1
    move-object v7, v6

    .line 110
    :goto_2
    move-object v9, v7

    .line 111
    :goto_3
    const/4 v8, 0x0

    .line 112
    :goto_4
    const-string v4, "PremiumBuy"

    .line 113
    .line 114
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    move-object v4, v6

    .line 118
    move v6, v8

    .line 119
    move-object v8, v9

    .line 120
    move-object v9, v1

    .line 121
    :goto_5
    move-object v12, v7

    .line 122
    move v7, v2

    .line 123
    move-object v2, v3

    .line 124
    move-object v3, v5

    .line 125
    move-object v5, v12

    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->〇080:Landroid/app/Activity;

    .line 127
    .line 128
    instance-of v1, v0, Lcom/intsig/webview/WebViewActivity;

    .line 129
    .line 130
    if-eqz v1, :cond_1

    .line 131
    .line 132
    check-cast v0, Lcom/intsig/webview/WebViewActivity;

    .line 133
    .line 134
    invoke-virtual {v0}, Lcom/intsig/webview/WebViewActivity;->OO〇〇o0oO()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇oOO8O8(Ljava/lang/String;)Z

    .line 139
    .line 140
    .line 141
    move-result v1

    .line 142
    if-eqz v1, :cond_1

    .line 143
    .line 144
    iget-object v1, p0, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 145
    .line 146
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->functionStr:Ljava/lang/String;

    .line 147
    .line 148
    const-string v1, "subscription"

    .line 149
    .line 150
    const-string v10, "from_part"

    .line 151
    .line 152
    const-string v11, "CSPremiumAdPage"

    .line 153
    .line 154
    invoke-static {v11, v1, v10, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    :cond_1
    move-object v1, p0

    .line 158
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/attention/PremiumBuy;->Oo08(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    :cond_2
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

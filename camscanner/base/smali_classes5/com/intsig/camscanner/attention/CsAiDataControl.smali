.class public final Lcom/intsig/camscanner/attention/CsAiDataControl;
.super Lcom/intsig/camscanner/attention/AbsWebViewJsonControl;
.source "CsAiDataControl.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/attention/CsAiDataControl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o0:Lcom/intsig/camscanner/attention/CsAiDataControl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/attention/CsAiDataControl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/attention/CsAiDataControl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/attention/CsAiDataControl;->o0:Lcom/intsig/camscanner/attention/CsAiDataControl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/attention/AbsWebViewJsonControl;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public 〇080(Landroid/app/Activity;Lcom/intsig/camscanner/attention/CallAppData;)V
    .locals 9

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p2, Lcom/intsig/camscanner/attention/CallAppData;->data:Ljava/lang/String;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 p2, 0x0

    .line 7
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "execute: callAppData: "

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v2, ", threadName: "

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    const-string v1, "CsAiDataControl"

    .line 41
    .line 42
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const/4 v0, 0x0

    .line 46
    const/4 v2, 0x1

    .line 47
    if-eqz p2, :cond_2

    .line 48
    .line 49
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    if-nez v3, :cond_1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    const/4 v3, 0x0

    .line 57
    goto :goto_2

    .line 58
    :cond_2
    :goto_1
    const/4 v3, 0x1

    .line 59
    :goto_2
    if-eqz v3, :cond_3

    .line 60
    .line 61
    return-void

    .line 62
    :cond_3
    new-instance v3, Lorg/json/JSONObject;

    .line 63
    .line 64
    invoke-direct {v3, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    const-string p2, "document_id"

    .line 68
    .line 69
    invoke-virtual {v3, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    const-string v4, "document_name"

    .line 74
    .line 75
    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    const-string v5, "document_keys"

    .line 80
    .line 81
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    if-eqz p2, :cond_5

    .line 86
    .line 87
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 88
    .line 89
    .line 90
    move-result v5

    .line 91
    if-nez v5, :cond_4

    .line 92
    .line 93
    goto :goto_3

    .line 94
    :cond_4
    const/4 v5, 0x0

    .line 95
    goto :goto_4

    .line 96
    :cond_5
    :goto_3
    const/4 v5, 0x1

    .line 97
    :goto_4
    if-eqz v5, :cond_6

    .line 98
    .line 99
    return-void

    .line 100
    :cond_6
    invoke-static {p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)J

    .line 101
    .line 102
    .line 103
    move-result-wide v5

    .line 104
    new-instance p2, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v7, "execute: docId: "

    .line 110
    .line 111
    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {p2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    const-wide/16 v7, 0x0

    .line 125
    .line 126
    cmp-long p2, v5, v7

    .line 127
    .line 128
    if-gez p2, :cond_7

    .line 129
    .line 130
    return-void

    .line 131
    :cond_7
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 132
    .line 133
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 134
    .line 135
    .line 136
    move-result-object p2

    .line 137
    invoke-static {p2, v5, v6}, Lcom/intsig/camscanner/db/dao/MTagDao;->〇o〇(Landroid/content/Context;J)Z

    .line 138
    .line 139
    .line 140
    move-result p2

    .line 141
    new-instance v7, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    const-string v8, "execute: hasAiTag: "

    .line 147
    .line 148
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v7

    .line 158
    invoke-static {v1, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    if-nez p2, :cond_a

    .line 162
    .line 163
    if-eqz v3, :cond_a

    .line 164
    .line 165
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    .line 166
    .line 167
    .line 168
    move-result p2

    .line 169
    if-lez p2, :cond_a

    .line 170
    .line 171
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    .line 172
    .line 173
    .line 174
    move-result p2

    .line 175
    const/4 v1, 0x0

    .line 176
    const/4 v7, 0x0

    .line 177
    :goto_5
    if-ge v1, p2, :cond_b

    .line 178
    .line 179
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    .line 180
    .line 181
    .line 182
    move-result-object v7

    .line 183
    instance-of v8, v7, Ljava/lang/String;

    .line 184
    .line 185
    if-eqz v8, :cond_9

    .line 186
    .line 187
    move-object v8, v7

    .line 188
    check-cast v8, Ljava/lang/CharSequence;

    .line 189
    .line 190
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    .line 191
    .line 192
    .line 193
    move-result v8

    .line 194
    if-lez v8, :cond_8

    .line 195
    .line 196
    const/4 v8, 0x1

    .line 197
    goto :goto_6

    .line 198
    :cond_8
    const/4 v8, 0x0

    .line 199
    :goto_6
    if-eqz v8, :cond_9

    .line 200
    .line 201
    check-cast v7, Ljava/lang/String;

    .line 202
    .line 203
    invoke-static {v7}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 204
    .line 205
    .line 206
    move-result-wide v7

    .line 207
    invoke-static {v5, v6, v7, v8, v2}, Lcom/intsig/camscanner/app/DBUtil;->o08〇〇0O(JJI)J

    .line 208
    .line 209
    .line 210
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 211
    .line 212
    const/4 v7, 0x1

    .line 213
    goto :goto_5

    .line 214
    :cond_a
    const/4 v7, 0x0

    .line 215
    :cond_b
    if-eqz v4, :cond_d

    .line 216
    .line 217
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 218
    .line 219
    .line 220
    move-result p2

    .line 221
    if-nez p2, :cond_c

    .line 222
    .line 223
    goto :goto_7

    .line 224
    :cond_c
    const/4 p2, 0x0

    .line 225
    goto :goto_8

    .line 226
    :cond_d
    :goto_7
    const/4 p2, 0x1

    .line 227
    :goto_8
    if-nez p2, :cond_11

    .line 228
    .line 229
    invoke-static {p1, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object p1

    .line 233
    if-eqz p1, :cond_e

    .line 234
    .line 235
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 236
    .line 237
    .line 238
    move-result p2

    .line 239
    if-nez p2, :cond_f

    .line 240
    .line 241
    :cond_e
    const/4 v0, 0x1

    .line 242
    :cond_f
    if-nez v0, :cond_10

    .line 243
    .line 244
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 245
    .line 246
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 247
    .line 248
    .line 249
    move-result-object p2

    .line 250
    const v0, 0x7f1304ed

    .line 251
    .line 252
    .line 253
    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object p2

    .line 257
    const-string v0, "ApplicationHelper.sConte\u2026ame\n                    )"

    .line 258
    .line 259
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    .line 261
    .line 262
    invoke-static {p1, p2, v2}, Lkotlin/text/StringsKt;->oo〇(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 263
    .line 264
    .line 265
    move-result p1

    .line 266
    if-eqz p1, :cond_11

    .line 267
    .line 268
    :cond_10
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 269
    .line 270
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 271
    .line 272
    .line 273
    move-result-object p2

    .line 274
    invoke-static {p2, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OoO8(Landroid/content/Context;J)Ljava/lang/String;

    .line 275
    .line 276
    .line 277
    move-result-object p2

    .line 278
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 279
    .line 280
    .line 281
    move-result-object p1

    .line 282
    invoke-static {v5, v6, v4, p2, p1}, Lcom/intsig/camscanner/util/Util;->o8O0(JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    .line 283
    .line 284
    .line 285
    :cond_11
    if-eqz v7, :cond_12

    .line 286
    .line 287
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 288
    .line 289
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 290
    .line 291
    .line 292
    move-result-object p1

    .line 293
    const/4 p2, 0x3

    .line 294
    invoke-static {p1, v5, v6, p2, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 295
    .line 296
    .line 297
    :cond_12
    return-void
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
.end method

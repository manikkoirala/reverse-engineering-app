.class public final Lcom/intsig/camscanner/attention/share/FileIntentCreator;
.super Ljava/lang/Object;
.source "FileIntentCreator.kt"

# interfaces
.implements Lcom/intsig/camscanner/attention/share/IIntentCreator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/attention/share/FileIntentCreator$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final 〇o00〇〇Oo:Lcom/intsig/camscanner/attention/share/FileIntentCreator$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇080:Lcom/intsig/app/BaseProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/attention/share/FileIntentCreator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator;->〇o00〇〇Oo:Lcom/intsig/camscanner/attention/share/FileIntentCreator$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8(Landroidx/fragment/app/FragmentActivity;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$hideLoading$1;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/attention/share/FileIntentCreator$hideLoading$1;-><init>(Lcom/intsig/camscanner/attention/share/FileIntentCreator;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, v0}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenResumed(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method private final Oo08(Landroidx/fragment/app/FragmentActivity;)V
    .locals 3

    .line 1
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/attention/share/FileIntentCreator$showLoading$1;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, p1, v2}, Lcom/intsig/camscanner/attention/share/FileIntentCreator$showLoading$1;-><init>(Lcom/intsig/camscanner/attention/share/FileIntentCreator;Landroidx/fragment/app/FragmentActivity;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenResumed(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/attention/share/FileIntentCreator;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/attention/share/FileIntentCreator;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/attention/share/FileIntentCreator;Lcom/intsig/app/BaseProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/attention/share/FileIntentCreator;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public 〇080(Landroidx/fragment/app/FragmentActivity;Landroid/content/Intent;Lcom/intsig/camscanner/attention/share/ShareWebResourceData$Data;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 8
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Landroid/content/Intent;",
            "Lcom/intsig/camscanner/attention/share/ShareWebResourceData$Data;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p4, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p4

    .line 6
    check-cast v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->O8o08O8O:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->O8o08O8O:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p4}, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;-><init>(Lcom/intsig/camscanner/attention/share/FileIntentCreator;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p4, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->O8o08O8O:I

    .line 32
    .line 33
    const-string v3, "FileIntentCreator"

    .line 34
    .line 35
    const/4 v4, 0x1

    .line 36
    if-eqz v2, :cond_2

    .line 37
    .line 38
    if-ne v2, v4, :cond_1

    .line 39
    .line 40
    iget-object p1, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->OO:Ljava/lang/Object;

    .line 41
    .line 42
    move-object p2, p1

    .line 43
    check-cast p2, Landroid/content/Intent;

    .line 44
    .line 45
    iget-object p1, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 46
    .line 47
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    .line 48
    .line 49
    iget-object p3, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->o0:Ljava/lang/Object;

    .line 50
    .line 51
    check-cast p3, Lcom/intsig/camscanner/attention/share/FileIntentCreator;

    .line 52
    .line 53
    invoke-static {p4}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    goto/16 :goto_8

    .line 57
    .line 58
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 59
    .line 60
    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 61
    .line 62
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    throw p1

    .line 66
    :cond_2
    invoke-static {p4}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    new-instance p4, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v2, "buildIntent, data: "

    .line 75
    .line 76
    invoke-virtual {p4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p4

    .line 86
    invoke-static {v3, p4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    const/4 p4, 0x0

    .line 90
    if-eqz p3, :cond_3

    .line 91
    .line 92
    invoke-virtual {p3}, Lcom/intsig/camscanner/attention/share/ShareWebResourceData$Data;->getContent()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    goto :goto_1

    .line 97
    :cond_3
    move-object v2, p4

    .line 98
    :goto_1
    if-eqz p3, :cond_4

    .line 99
    .line 100
    invoke-virtual {p3}, Lcom/intsig/camscanner/attention/share/ShareWebResourceData$Data;->getFile_type()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v5

    .line 104
    goto :goto_2

    .line 105
    :cond_4
    move-object v5, p4

    .line 106
    :goto_2
    if-eqz p3, :cond_5

    .line 107
    .line 108
    invoke-virtual {p3}, Lcom/intsig/camscanner/attention/share/ShareWebResourceData$Data;->getFile_name()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object p3

    .line 112
    goto :goto_3

    .line 113
    :cond_5
    move-object p3, p4

    .line 114
    :goto_3
    const/4 v6, 0x0

    .line 115
    if-eqz v2, :cond_7

    .line 116
    .line 117
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 118
    .line 119
    .line 120
    move-result v7

    .line 121
    if-nez v7, :cond_6

    .line 122
    .line 123
    goto :goto_4

    .line 124
    :cond_6
    const/4 v7, 0x0

    .line 125
    goto :goto_5

    .line 126
    :cond_7
    :goto_4
    const/4 v7, 0x1

    .line 127
    :goto_5
    if-nez v7, :cond_f

    .line 128
    .line 129
    if-eqz v5, :cond_9

    .line 130
    .line 131
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    .line 132
    .line 133
    .line 134
    move-result v7

    .line 135
    if-nez v7, :cond_8

    .line 136
    .line 137
    goto :goto_6

    .line 138
    :cond_8
    const/4 v7, 0x0

    .line 139
    goto :goto_7

    .line 140
    :cond_9
    :goto_6
    const/4 v7, 0x1

    .line 141
    :goto_7
    if-nez v7, :cond_f

    .line 142
    .line 143
    if-eqz p3, :cond_a

    .line 144
    .line 145
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    .line 146
    .line 147
    .line 148
    move-result v7

    .line 149
    if-nez v7, :cond_b

    .line 150
    .line 151
    :cond_a
    const/4 v6, 0x1

    .line 152
    :cond_b
    if-eqz v6, :cond_c

    .line 153
    .line 154
    goto :goto_9

    .line 155
    :cond_c
    invoke-virtual {p2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    .line 157
    .line 158
    const-string v5, "android.intent.extra.SUBJECT"

    .line 159
    .line 160
    invoke-virtual {p2, v5, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    .line 162
    .line 163
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/attention/share/FileIntentCreator;->Oo08(Landroidx/fragment/app/FragmentActivity;)V

    .line 164
    .line 165
    .line 166
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 167
    .line 168
    .line 169
    move-result-object p3

    .line 170
    new-instance v5, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$path$1;

    .line 171
    .line 172
    invoke-direct {v5, p1, v2, p4}, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$path$1;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 173
    .line 174
    .line 175
    iput-object p0, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->o0:Ljava/lang/Object;

    .line 176
    .line 177
    iput-object p1, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 178
    .line 179
    iput-object p2, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->OO:Ljava/lang/Object;

    .line 180
    .line 181
    iput v4, v0, Lcom/intsig/camscanner/attention/share/FileIntentCreator$buildIntent$1;->O8o08O8O:I

    .line 182
    .line 183
    invoke-static {p3, v5, v0}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object p4

    .line 187
    if-ne p4, v1, :cond_d

    .line 188
    .line 189
    return-object v1

    .line 190
    :cond_d
    move-object p3, p0

    .line 191
    :goto_8
    check-cast p4, Ljava/lang/String;

    .line 192
    .line 193
    invoke-direct {p3, p1}, Lcom/intsig/camscanner/attention/share/FileIntentCreator;->O8(Landroidx/fragment/app/FragmentActivity;)V

    .line 194
    .line 195
    .line 196
    new-instance p3, Ljava/lang/StringBuilder;

    .line 197
    .line 198
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    .line 200
    .line 201
    const-string v0, "buildIntent, path: "

    .line 202
    .line 203
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object p3

    .line 213
    invoke-static {v3, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    invoke-static {p4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 217
    .line 218
    .line 219
    move-result p3

    .line 220
    if-eqz p3, :cond_e

    .line 221
    .line 222
    invoke-static {p1, p2, p4}, Lcom/intsig/camscanner/share/type/BaseShare;->〇oOO8O8(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)Landroid/net/Uri;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    const-string p3, "android.intent.extra.STREAM"

    .line 227
    .line 228
    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 229
    .line 230
    .line 231
    :cond_e
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 232
    .line 233
    return-object p1

    .line 234
    :cond_f
    :goto_9
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 235
    .line 236
    return-object p1
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
.end method

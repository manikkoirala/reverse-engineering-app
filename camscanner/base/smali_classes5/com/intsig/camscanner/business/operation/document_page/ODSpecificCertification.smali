.class public Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;
.super Ljava/lang/Object;
.source "ODSpecificCertification.java"

# interfaces
.implements Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;


# instance fields
.field private OO:Ljava/lang/String;

.field private o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

.field private final o〇00O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public 〇OOo8〇0:I


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;)V
    .locals 1
    .param p2    # Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o〇00O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private Oo08(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const p1, 0x7f080e8c

    .line 5
    .line 6
    .line 7
    iput p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇OOo8〇0:I

    .line 8
    .line 9
    const-string p1, "collage_id_card"

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_ID_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_0
    const/4 v0, 0x4

    .line 19
    if-eq p1, v0, :cond_5

    .line 20
    .line 21
    const/16 v0, 0x71

    .line 22
    .line 23
    if-ne p1, v0, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/16 v0, 0x8

    .line 27
    .line 28
    if-eq p1, v0, :cond_4

    .line 29
    .line 30
    const/16 v0, 0x72

    .line 31
    .line 32
    if-ne p1, v0, :cond_2

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_2
    const/16 v0, 0xd

    .line 36
    .line 37
    if-ne p1, v0, :cond_3

    .line 38
    .line 39
    const p1, 0x7f080e87

    .line 40
    .line 41
    .line 42
    iput p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇OOo8〇0:I

    .line 43
    .line 44
    const-string p1, "collage_bank_card"

    .line 45
    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 47
    .line 48
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_BANK_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 49
    .line 50
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_3
    const/4 p1, 0x0

    .line 54
    goto :goto_3

    .line 55
    :cond_4
    :goto_0
    const p1, 0x7f080e9c

    .line 56
    .line 57
    .line 58
    iput p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇OOo8〇0:I

    .line 59
    .line 60
    const-string p1, "collage_car_licence"

    .line 61
    .line 62
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 63
    .line 64
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_CAR_LICENCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 65
    .line 66
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_5
    :goto_1
    const p1, 0x7f080e89

    .line 70
    .line 71
    .line 72
    iput p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇OOo8〇0:I

    .line 73
    .line 74
    const-string p1, "collage_drive_licence"

    .line 75
    .line 76
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 77
    .line 78
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_DRIVE_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 79
    .line 80
    iput-object p1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 81
    .line 82
    :goto_2
    const/4 p1, 0x1

    .line 83
    :goto_3
    return p1
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method


# virtual methods
.method public O8(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 5

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o〇0()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 12
    .line 13
    const/16 v1, 0x8

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇0O:Landroid/widget/TextView;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->oOo〇8o008:Landroid/widget/TextView;

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o〇00O:Landroid/widget/ImageView;

    .line 29
    .line 30
    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o〇00O:Landroid/widget/ImageView;

    .line 35
    .line 36
    iget v3, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇OOo8〇0:I

    .line 37
    .line 38
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 42
    .line 43
    const v3, 0x7f0801d4

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 50
    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 52
    .line 53
    iget-object v3, v3, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o〇:Landroid/app/Activity;

    .line 54
    .line 55
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    const v4, 0x7f0600bd

    .line 60
    .line 61
    .line 62
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 70
    .line 71
    const v3, 0x7f13068a

    .line 72
    .line 73
    .line 74
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 78
    .line 79
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    iget-object p1, p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 83
    .line 84
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
.end method

.method public getPriority()I
    .locals 1

    .line 1
    const/16 v0, 0x406

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o〇0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o〇00O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    .line 10
    .line 11
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "type"

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    .line 20
    .line 21
    const-string v1, "CSListDocBanner"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    const-string v1, "ODSpecificCertification"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    .line 32
    .line 33
    :cond_0
    :goto_0
    return-void
.end method

.method public 〇080()I
    .locals 1

    .line 1
    const/16 v0, 0xe

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o00〇〇Oo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->oO80:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 9
    .line 10
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 17
    .line 18
    if-nez v0, :cond_2

    .line 19
    .line 20
    :cond_1
    const-string v0, "ODSpecificCertification"

    .line 21
    .line 22
    const-string v1, "procedure occurs exception"

    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_2
    const/4 v0, 0x1

    .line 28
    new-array v0, v0, [Landroid/util/Pair;

    .line 29
    .line 30
    new-instance v1, Landroid/util/Pair;

    .line 31
    .line 32
    const-string v2, "type"

    .line 33
    .line 34
    iget-object v3, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->OO:Ljava/lang/String;

    .line 35
    .line 36
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    const/4 v2, 0x0

    .line 40
    aput-object v1, v0, v2

    .line 41
    .line 42
    const-string v1, "CSListDocBanner"

    .line 43
    .line 44
    const-string v2, "click"

    .line 45
    .line 46
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 50
    .line 51
    iget-object v0, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->oO80:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 54
    .line 55
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;->O8(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
.end method

.method public 〇o〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 2
    .line 3
    iget v0, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o00〇〇Oo:I

    .line 4
    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->Oo08(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/business/operation/document_page/ODSpecificCertification;->o0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 12
    .line 13
    iget v0, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇080:I

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    sub-int/2addr v0, v1

    .line 17
    const/4 v2, 0x2

    .line 18
    if-ne v0, v2, :cond_0

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v1, 0x0

    .line 28
    :goto_0
    return v1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

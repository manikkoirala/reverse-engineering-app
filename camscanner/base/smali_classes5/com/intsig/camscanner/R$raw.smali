.class public final Lcom/intsig/camscanner/R$raw;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "raw"
.end annotation


# static fields
.field public static final appconfig:I = 0x7f120000

.field public static final applovin_consent_flow_gdpr:I = 0x7f120001

.field public static final applovin_consent_flow_privacy_policy:I = 0x7f120002

.field public static final applovin_consent_flow_terms_of_service_and_privacy_policy:I = 0x7f120003

.field public static final back_up_photo:I = 0x7f120004

.field public static final capture_guide_smart_erase:I = 0x7f120005

.field public static final comm_loading:I = 0x7f120006

.field public static final comm_loading_progress:I = 0x7f120007

.field public static final firebase_common_keep:I = 0x7f120008

.field public static final green_mode_import:I = 0x7f120009

.field public static final green_mode_migration:I = 0x7f12000a

.field public static final guide_hand:I = 0x7f12000b

.field public static final id_card:I = 0x7f12000c

.field public static final keep:I = 0x7f12000d

.field public static final key:I = 0x7f12000e

.field public static final keyconfig:I = 0x7f12000f

.field public static final lottie_batch_process:I = 0x7f120010

.field public static final lottie_camexam:I = 0x7f120011

.field public static final lottie_capture_count_guide:I = 0x7f120012

.field public static final lottie_capture_demo_cn:I = 0x7f120013

.field public static final lottie_capture_demo_en:I = 0x7f120014

.field public static final lottie_card_long_touch_guide:I = 0x7f120015

.field public static final lottie_certificate:I = 0x7f120016

.field public static final lottie_cn_annual_premium_main_discount:I = 0x7f120017

.field public static final lottie_cn_annual_premium_main_free_trail:I = 0x7f120018

.field public static final lottie_cn_renew_recall_flower:I = 0x7f120019

.field public static final lottie_edit_card_ocr_mask:I = 0x7f12001a

.field public static final lottie_eyedropper:I = 0x7f12001b

.field public static final lottie_first_in_ocr_result:I = 0x7f12001c

.field public static final lottie_flower_annual_premium:I = 0x7f12001d

.field public static final lottie_flower_coloured_ribbon:I = 0x7f12001e

.field public static final lottie_gallery_radar_guide:I = 0x7f12001f

.field public static final lottie_gp_annual_premium_main_discount:I = 0x7f120020

.field public static final lottie_gp_annual_premium_main_free_trail:I = 0x7f120021

.field public static final lottie_gp_card_detail_certificate_scan:I = 0x7f120022

.field public static final lottie_gp_card_detail_fill_in_prompt:I = 0x7f120023

.field public static final lottie_guide_count_number:I = 0x7f120024

.field public static final lottie_home_id_share_activity_icon:I = 0x7f120025

.field public static final lottie_home_rejoin_benefit_icon:I = 0x7f120026

.field public static final lottie_home_vip_icon_1:I = 0x7f120027

.field public static final lottie_home_vip_icon_2_free:I = 0x7f120028

.field public static final lottie_home_vip_icon_2_other:I = 0x7f120029

.field public static final lottie_home_vip_icon_2_pay:I = 0x7f12002a

.field public static final lottie_home_vip_icon_3:I = 0x7f12002b

.field public static final lottie_home_vip_icon_4_free:I = 0x7f12002c

.field public static final lottie_home_vip_icon_4_other:I = 0x7f12002d

.field public static final lottie_home_vip_icon_4_pay:I = 0x7f12002e

.field public static final lottie_home_vip_icon_oversea_1:I = 0x7f12002f

.field public static final lottie_home_vip_icon_oversea_2_free:I = 0x7f120030

.field public static final lottie_home_vip_icon_oversea_2_pay:I = 0x7f120031

.field public static final lottie_image_edit_long_click_guide:I = 0x7f120032

.field public static final lottie_image_editing_guide:I = 0x7f120033

.field public static final lottie_loading_normal_type:I = 0x7f120034

.field public static final lottie_loading_ppt_to_pdf:I = 0x7f120035

.field public static final lottie_loading_to_image_restore:I = 0x7f120036

.field public static final lottie_loading_to_word:I = 0x7f120037

.field public static final lottie_loading_word_to_pdf:I = 0x7f120038

.field public static final lottie_main_home_arrow_cn:I = 0x7f120039

.field public static final lottie_main_home_arrow_gp:I = 0x7f12003a

.field public static final lottie_main_home_gift_box:I = 0x7f12003b

.field public static final lottie_main_home_none_doc:I = 0x7f12003c

.field public static final lottie_moire_description:I = 0x7f12003d

.field public static final lottie_nps_crop:I = 0x7f12003e

.field public static final lottie_nps_filter:I = 0x7f12003f

.field public static final lottie_ocr_capture_guide:I = 0x7f120040

.field public static final lottie_only_read:I = 0x7f120041

.field public static final lottie_pagelist_edit:I = 0x7f120042

.field public static final lottie_pagelist_function_move:I = 0x7f120043

.field public static final lottie_passport_mode:I = 0x7f120044

.field public static final lottie_pdf_kit_move:I = 0x7f120045

.field public static final lottie_pdf_remove_watermark:I = 0x7f120046

.field public static final lottie_pdf_remove_watermark_green:I = 0x7f120047

.field public static final lottie_pdf_remove_watermark_qrcode:I = 0x7f120048

.field public static final lottie_pdf_remove_watermark_qrcode_white:I = 0x7f120049

.field public static final lottie_pdf_water_ad_video_cn:I = 0x7f12004a

.field public static final lottie_pdf_watermark_advideo:I = 0x7f12004b

.field public static final lottie_pop_flower:I = 0x7f12004c

.field public static final lottie_print_update:I = 0x7f12004d

.field public static final lottie_printer_printing:I = 0x7f12004e

.field public static final lottie_printer_search:I = 0x7f12004f

.field public static final lottie_ps_detect:I = 0x7f120050

.field public static final lottie_satisfy:I = 0x7f120051

.field public static final lottie_scan_doc_full_screen:I = 0x7f120052

.field public static final lottie_scan_first_doc:I = 0x7f120053

.field public static final lottie_smart_erase:I = 0x7f120054

.field public static final lottie_super_filter_scan_mode_guide_65200:I = 0x7f120055

.field public static final lottie_watermark:I = 0x7f120056

.field public static final lottie_word:I = 0x7f120057

.field public static final lottie_word_list_guide_cn:I = 0x7f120058

.field public static final lottie_word_list_guide_eg:I = 0x7f120059

.field public static final omsdk_v_1_0:I = 0x7f12005a

.field public static final paper_abroad:I = 0x7f12005b

.field public static final ppt_guide:I = 0x7f12005c

.field public static final right_move_hand:I = 0x7f12005d

.field public static final scan_document:I = 0x7f12005e

.field public static final shake_accelerometer:I = 0x7f12005f

.field public static final shake_turn:I = 0x7f120060

.field public static final temp_py:I = 0x7f120061

.field public static final text_recognition:I = 0x7f120062

.field public static final welcome_guide_video:I = 0x7f120063


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

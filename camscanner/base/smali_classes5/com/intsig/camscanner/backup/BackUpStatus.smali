.class public abstract Lcom/intsig/camscanner/backup/BackUpStatus;
.super Ljava/lang/Object;
.source "BackUpStatus.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/backup/BackUpStatus$Init;,
        Lcom/intsig/camscanner/backup/BackUpStatus$Searching;,
        Lcom/intsig/camscanner/backup/BackUpStatus$NoBackUpFiles;,
        Lcom/intsig/camscanner/backup/BackUpStatus$BackUpping;,
        Lcom/intsig/camscanner/backup/BackUpStatus$Pause;,
        Lcom/intsig/camscanner/backup/BackUpStatus$Completed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/backup/BackUpStatus;-><init>()V

    return-void
.end method

.class public final Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;
.super Ljava/lang/Object;
.source "DocsCopyAction.kt"

# interfaces
.implements Lcom/intsig/camscanner/movecopyactivity/action/IAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80〇808〇O:Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private final Oo08:I

.field private oO80:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

.field private final o〇0:[J
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Z

.field private 〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇80〇808〇O:Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Ljava/util/List;Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;ZLcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;",
            "Z",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ")V"
        }
    .end annotation

    const-string v0, "docItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActivity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇080:Ljava/util/List;

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 4
    iput-boolean p3, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o〇:Z

    .line 5
    iput-object p4, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->O8:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    iput p2, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08:I

    .line 7
    check-cast p1, Ljava/lang/Iterable;

    .line 8
    new-instance p2, Ljava/util/ArrayList;

    const/16 p3, 0xa

    invoke-static {p1, p3}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    move-result p3

    invoke-direct {p2, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 9
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    .line 10
    check-cast p3, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 11
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide p3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    .line 12
    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13
    :cond_0
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->OOO8o〇〇(Ljava/util/Collection;)[J

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->o〇0:[J

    .line 14
    sget-object p1, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;ZLcom/intsig/camscanner/datastruct/FolderItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 15
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;-><init>(Ljava/util/List;Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;ZLcom/intsig/camscanner/datastruct/FolderItem;)V

    return-void
.end method

.method private final O8ooOoo〇(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    invoke-static {v0, p1, v3, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇O8〇〇o(Landroid/content/Context;Ljava/util/List;ZILjava/lang/Object;)Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 11
    .line 12
    invoke-static {v1, p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 19
    .line 20
    .line 21
    check-cast p1, Ljava/util/Collection;

    .line 22
    .line 23
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/app/DBUtil;->o〇0o〇〇(Landroid/content/Context;Ljava/util/List;I)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O00(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OoO8(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OooO〇()V

    .line 9
    .line 10
    .line 11
    new-instance v0, Landroid/content/Intent;

    .line 12
    .line 13
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "targetDirSyncId"

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O888o0o()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "Intent().putExtra(HomeCo\u2026C_ID, targetParentSyncId)"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 32
    .line 33
    const/4 v2, -0x1

    .line 34
    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 35
    .line 36
    .line 37
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 38
    .line 39
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇8O8〇008(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/util/LongSparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "tag_id"

    .line 2
    .line 3
    const-string v1, "document_id"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v4

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "document_id in "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v5

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v6, 0x0

    .line 33
    const/4 v7, 0x0

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 41
    .line 42
    .line 43
    move-result p2

    .line 44
    if-eqz p2, :cond_1

    .line 45
    .line 46
    const/4 p2, 0x1

    .line 47
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 48
    .line 49
    .line 50
    move-result-wide v0

    .line 51
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-ltz v0, :cond_0

    .line 56
    .line 57
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 58
    .line 59
    .line 60
    move-result-wide v0

    .line 61
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    const-string v0, "tagMap[cursor.getLong(1)]"

    .line 66
    .line 67
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    check-cast p2, Ljava/util/ArrayList;

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 79
    .line 80
    .line 81
    move-result-wide v1

    .line 82
    invoke-virtual {p3, v1, v2, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    move-object p2, v0

    .line 86
    :goto_1
    const/4 v0, 0x0

    .line 87
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 88
    .line 89
    .line 90
    move-result-wide v0

    .line 91
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 100
    .line 101
    .line 102
    :cond_2
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final o800o8O()Lcom/intsig/camscanner/datastruct/FolderItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8O()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇8o8o〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->OoO8(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oo88o8O()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇O8〇〇o()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN_OFFLINE:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇00()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0000OOO(Landroid/content/Context;JLjava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p4, :cond_4

    .line 2
    .line 3
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_2

    .line 10
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object p4

    .line 19
    :cond_1
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_2

    .line 24
    .line 25
    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    check-cast v1, Ljava/lang/Long;

    .line 30
    .line 31
    const-string v2, "tagId"

    .line 32
    .line 33
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 37
    .line 38
    .line 39
    move-result-wide v2

    .line 40
    invoke-static {p1, v2, v3}, Lcom/intsig/camscanner/db/dao/TagDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-eqz v2, :cond_1

    .line 45
    .line 46
    new-instance v2, Landroid/content/ContentValues;

    .line 47
    .line 48
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v3, "document_id"

    .line 52
    .line 53
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 58
    .line 59
    .line 60
    const-string v3, "tag_id"

    .line 61
    .line 62
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 63
    .line 64
    .line 65
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 66
    .line 67
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 84
    .line 85
    .line 86
    move-result p2

    .line 87
    if-lez p2, :cond_3

    .line 88
    .line 89
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    sget-object p2, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 94
    .line 95
    invoke-virtual {p1, p2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :catch_0
    move-exception p1

    .line 100
    sget-object p2, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object p2

    .line 106
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    .line 108
    .line 109
    :cond_3
    :goto_1
    return-void

    .line 110
    :cond_4
    :goto_2
    sget-object p1, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 111
    .line 112
    invoke-virtual {p1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    const-string p2, "tagIds is empty"

    .line 117
    .line 118
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final 〇0〇O0088o(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OooO〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇0〇O0088o(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇808〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇O00(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 31

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "this$0"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil;->〇080:Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;

    .line 9
    .line 10
    const-string v2, "other_copy"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/logagent/NewDocLogAgentUtil$Companion;->o〇0(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->o〇0:[J

    .line 16
    .line 17
    invoke-static {v1}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->〇o00〇〇Oo([J)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    new-instance v2, Landroid/util/LongSparseArray;

    .line 22
    .line 23
    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    .line 24
    .line 25
    .line 26
    iget-object v3, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 27
    .line 28
    const-string v4, "filters"

    .line 29
    .line 30
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->O〇8O8〇008(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V

    .line 34
    .line 35
    .line 36
    new-instance v1, Lkotlin/jvm/internal/Ref$IntRef;

    .line 37
    .line 38
    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 39
    .line 40
    .line 41
    new-instance v3, Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .line 45
    .line 46
    iget-object v4, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇080:Ljava/util/List;

    .line 47
    .line 48
    check-cast v4, Ljava/lang/Iterable;

    .line 49
    .line 50
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 55
    .line 56
    .line 57
    move-result v5

    .line 58
    const/4 v6, 0x0

    .line 59
    if-eqz v5, :cond_19

    .line 60
    .line 61
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 66
    .line 67
    iget v7, v1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 68
    .line 69
    const/4 v8, 0x1

    .line 70
    add-int/2addr v7, v8

    .line 71
    iput v7, v1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 72
    .line 73
    new-instance v7, LO8o〇O0/O8;

    .line 74
    .line 75
    invoke-direct {v7, v0, v1}, LO8o〇O0/O8;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V

    .line 76
    .line 77
    .line 78
    invoke-direct {v0, v7}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇oOO8O8(Ljava/lang/Runnable;)V

    .line 79
    .line 80
    .line 81
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 82
    .line 83
    .line 84
    move-result-object v7

    .line 85
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O888o0o()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v9

    .line 89
    invoke-static {v7, v9}, Lcom/intsig/camscanner/app/DBUtil;->〇o〇8(Landroid/content/Context;Ljava/lang/String;)Z

    .line 90
    .line 91
    .line 92
    move-result v19

    .line 93
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 94
    .line 95
    .line 96
    move-result-wide v14

    .line 97
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 98
    .line 99
    .line 100
    move-result-object v20

    .line 101
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v21

    .line 105
    const/16 v22, 0x1

    .line 106
    .line 107
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O888o0o()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v23

    .line 111
    const/16 v24, 0x0

    .line 112
    .line 113
    const/16 v25, 0x1

    .line 114
    .line 115
    invoke-static/range {v20 .. v25}, Lcom/intsig/camscanner/util/Util;->ooo〇8oO(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v11

    .line 119
    new-instance v7, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 120
    .line 121
    const/4 v12, 0x0

    .line 122
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O888o0o()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v13

    .line 126
    const/4 v9, 0x0

    .line 127
    const-string v16, ""

    .line 128
    .line 129
    const/16 v17, 0x0

    .line 130
    .line 131
    const/16 v18, 0x0

    .line 132
    .line 133
    const/16 v20, 0x0

    .line 134
    .line 135
    iget-object v10, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 136
    .line 137
    move-object/from16 v21, v10

    .line 138
    .line 139
    move-object v10, v7

    .line 140
    move-wide/from16 v22, v14

    .line 141
    .line 142
    move v14, v9

    .line 143
    move-object/from16 v15, v16

    .line 144
    .line 145
    move-object/from16 v16, v17

    .line 146
    .line 147
    move/from16 v17, v18

    .line 148
    .line 149
    move/from16 v18, v20

    .line 150
    .line 151
    move-object/from16 v20, v21

    .line 152
    .line 153
    invoke-direct/range {v10 .. v20}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 154
    .line 155
    .line 156
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v9

    .line 160
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v10

    .line 164
    new-instance v11, Ljava/lang/StringBuilder;

    .line 165
    .line 166
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .line 168
    .line 169
    const-string v12, "executeCopyDoc oldDocSyncId: "

    .line 170
    .line 171
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v11

    .line 181
    const-string v12, "DocsCopyAction"

    .line 182
    .line 183
    invoke-static {v12, v11}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    if-eqz v10, :cond_1

    .line 187
    .line 188
    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    .line 189
    .line 190
    .line 191
    move-result v11

    .line 192
    if-nez v11, :cond_0

    .line 193
    .line 194
    goto :goto_1

    .line 195
    :cond_0
    const/4 v11, 0x0

    .line 196
    goto :goto_2

    .line 197
    :cond_1
    :goto_1
    const/4 v11, 0x1

    .line 198
    :goto_2
    if-nez v11, :cond_e

    .line 199
    .line 200
    invoke-static {v10}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 201
    .line 202
    .line 203
    move-result v11

    .line 204
    if-eqz v11, :cond_e

    .line 205
    .line 206
    sget-object v11, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 207
    .line 208
    invoke-virtual {v11, v10}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 209
    .line 210
    .line 211
    move-result-object v14

    .line 212
    if-eqz v14, :cond_2

    .line 213
    .line 214
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v9

    .line 218
    invoke-virtual {v14}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getIdSuffix()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object v14

    .line 222
    new-instance v15, Ljava/lang/StringBuilder;

    .line 223
    .line 224
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    const-string v9, "_"

    .line 231
    .line 232
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v9

    .line 242
    :cond_2
    const-string v14, "newDocSyncId"

    .line 243
    .line 244
    invoke-static {v9, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    invoke-virtual {v11, v9}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇〇888(Ljava/lang/String;)Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object v14

    .line 251
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object v15

    .line 255
    if-eqz v15, :cond_4

    .line 256
    .line 257
    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    .line 258
    .line 259
    .line 260
    move-result v15

    .line 261
    if-nez v15, :cond_3

    .line 262
    .line 263
    goto :goto_3

    .line 264
    :cond_3
    const/4 v15, 0x0

    .line 265
    goto :goto_4

    .line 266
    :cond_4
    :goto_3
    const/4 v15, 0x1

    .line 267
    :goto_4
    if-eqz v15, :cond_6

    .line 268
    .line 269
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 270
    .line 271
    .line 272
    move-result-object v15

    .line 273
    move-object/from16 v17, v9

    .line 274
    .line 275
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 276
    .line 277
    .line 278
    move-result-wide v8

    .line 279
    invoke-static {v15, v8, v9}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 280
    .line 281
    .line 282
    move-result-object v8

    .line 283
    if-eqz v8, :cond_5

    .line 284
    .line 285
    invoke-virtual {v8}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v8

    .line 289
    goto :goto_5

    .line 290
    :cond_5
    const/4 v8, 0x0

    .line 291
    :goto_5
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/datastruct/DocItem;->〇0(Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    goto :goto_6

    .line 295
    :cond_6
    move-object/from16 v17, v9

    .line 296
    .line 297
    :goto_6
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 298
    .line 299
    .line 300
    move-result-object v8

    .line 301
    if-eqz v8, :cond_8

    .line 302
    .line 303
    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    .line 304
    .line 305
    .line 306
    move-result v8

    .line 307
    if-nez v8, :cond_7

    .line 308
    .line 309
    goto :goto_7

    .line 310
    :cond_7
    const/4 v8, 0x0

    .line 311
    goto :goto_8

    .line 312
    :cond_8
    :goto_7
    const/4 v8, 0x1

    .line 313
    :goto_8
    if-nez v8, :cond_b

    .line 314
    .line 315
    if-eqz v14, :cond_a

    .line 316
    .line 317
    invoke-interface {v14}, Ljava/lang/CharSequence;->length()I

    .line 318
    .line 319
    .line 320
    move-result v8

    .line 321
    if-nez v8, :cond_9

    .line 322
    .line 323
    goto :goto_9

    .line 324
    :cond_9
    const/4 v8, 0x0

    .line 325
    goto :goto_a

    .line 326
    :cond_a
    :goto_9
    const/4 v8, 0x1

    .line 327
    :goto_a
    if-nez v8, :cond_b

    .line 328
    .line 329
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 330
    .line 331
    .line 332
    move-result-object v8

    .line 333
    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 334
    .line 335
    .line 336
    invoke-static {v8, v14}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 337
    .line 338
    .line 339
    move-result v8

    .line 340
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 341
    .line 342
    .line 343
    move-result-object v9

    .line 344
    new-instance v15, Ljava/lang/StringBuilder;

    .line 345
    .line 346
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 347
    .line 348
    .line 349
    const-string v13, "executeCopyDoc: copyOfficeFile: "

    .line 350
    .line 351
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    .line 353
    .line 354
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    .line 356
    .line 357
    const-string v9, " to: "

    .line 358
    .line 359
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    .line 361
    .line 362
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    .line 364
    .line 365
    const-string v9, ", result: "

    .line 366
    .line 367
    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    .line 369
    .line 370
    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 371
    .line 372
    .line 373
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 374
    .line 375
    .line 376
    move-result-object v8

    .line 377
    invoke-static {v12, v8}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    .line 379
    .line 380
    iput-object v14, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->OoO8:Ljava/lang/String;

    .line 381
    .line 382
    :cond_b
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 383
    .line 384
    .line 385
    move-result-object v8

    .line 386
    if-eqz v8, :cond_c

    .line 387
    .line 388
    invoke-virtual {v11, v8}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 389
    .line 390
    .line 391
    move-result-object v8

    .line 392
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 393
    .line 394
    .line 395
    move-result v9

    .line 396
    if-eqz v9, :cond_c

    .line 397
    .line 398
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 399
    .line 400
    .line 401
    move-result-object v9

    .line 402
    const-string v13, "thumbImgId"

    .line 403
    .line 404
    invoke-static {v9, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    .line 406
    .line 407
    invoke-virtual {v11, v9}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 408
    .line 409
    .line 410
    move-result-object v11

    .line 411
    invoke-static {v8, v11}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 412
    .line 413
    .line 414
    move-result v8

    .line 415
    if-eqz v8, :cond_c

    .line 416
    .line 417
    iput-object v9, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->o800o8O:Ljava/lang/String;

    .line 418
    .line 419
    const-string v8, "executeCopyDoc copy first page thumb"

    .line 420
    .line 421
    invoke-static {v12, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    :cond_c
    invoke-static {v10}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oo〇(Ljava/lang/String;)Z

    .line 425
    .line 426
    .line 427
    move-result v8

    .line 428
    if-eqz v8, :cond_d

    .line 429
    .line 430
    invoke-static {v10}, Lcom/intsig/camscanner/util/SDStorageManager;->OO8oO0o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 431
    .line 432
    .line 433
    move-result-object v8

    .line 434
    invoke-static {v8}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 435
    .line 436
    .line 437
    move-result v9

    .line 438
    if-eqz v9, :cond_d

    .line 439
    .line 440
    invoke-static/range {v17 .. v17}, Lcom/intsig/camscanner/util/SDStorageManager;->OO8oO0o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 441
    .line 442
    .line 443
    move-result-object v9

    .line 444
    invoke-static {v8, v9}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 445
    .line 446
    .line 447
    :cond_d
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8()I

    .line 448
    .line 449
    .line 450
    move-result v8

    .line 451
    iput v8, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O888o0o:I

    .line 452
    .line 453
    move-object/from16 v9, v17

    .line 454
    .line 455
    :cond_e
    iput-object v9, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O〇:Ljava/lang/String;

    .line 456
    .line 457
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 458
    .line 459
    .line 460
    move-result-object v8

    .line 461
    iput-object v8, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇8O0〇8:Ljava/lang/String;

    .line 462
    .line 463
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->OOO〇O0()I

    .line 464
    .line 465
    .line 466
    move-result v8

    .line 467
    iput v8, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->〇0〇O0088o:I

    .line 468
    .line 469
    iput-boolean v6, v7, Lcom/intsig/camscanner/datastruct/DocProperty;->〇oo〇:Z

    .line 470
    .line 471
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 472
    .line 473
    .line 474
    move-result-object v8

    .line 475
    invoke-static {v8, v7}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 476
    .line 477
    .line 478
    move-result-object v7

    .line 479
    if-nez v7, :cond_f

    .line 480
    .line 481
    sget-object v5, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 482
    .line 483
    invoke-virtual {v5}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 484
    .line 485
    .line 486
    move-result-object v5

    .line 487
    const-string v6, "executeCopy newDocUri == null"

    .line 488
    .line 489
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    .line 491
    .line 492
    new-instance v5, LO8o〇O0/Oo08;

    .line 493
    .line 494
    invoke-direct {v5, v0}, LO8o〇O0/Oo08;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 495
    .line 496
    .line 497
    invoke-direct {v0, v5}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇oOO8O8(Ljava/lang/Runnable;)V

    .line 498
    .line 499
    .line 500
    move-object/from16 v21, v1

    .line 501
    .line 502
    move-object v5, v3

    .line 503
    move-object/from16 v24, v4

    .line 504
    .line 505
    goto/16 :goto_13

    .line 506
    .line 507
    :cond_f
    invoke-static {v7}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 508
    .line 509
    .line 510
    move-result-wide v14

    .line 511
    sget-object v8, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 512
    .line 513
    invoke-virtual {v8}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->O0o〇〇Oo()Z

    .line 514
    .line 515
    .line 516
    move-result v9

    .line 517
    if-eqz v9, :cond_10

    .line 518
    .line 519
    const/4 v9, 0x1

    .line 520
    new-array v10, v9, [Ljava/lang/Long;

    .line 521
    .line 522
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 523
    .line 524
    .line 525
    move-result-object v9

    .line 526
    aput-object v9, v10, v6

    .line 527
    .line 528
    invoke-static {v10}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 529
    .line 530
    .line 531
    move-result-object v9

    .line 532
    invoke-virtual {v8, v9}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->OOO〇O0(Ljava/util/List;)Z

    .line 533
    .line 534
    .line 535
    move-result v8

    .line 536
    if-eqz v8, :cond_10

    .line 537
    .line 538
    const/16 v17, 0x1

    .line 539
    .line 540
    goto :goto_b

    .line 541
    :cond_10
    const/16 v17, 0x0

    .line 542
    .line 543
    :goto_b
    const-string v13, "_id"

    .line 544
    .line 545
    const-string v11, "_data"

    .line 546
    .line 547
    filled-new-array {v13, v11}, [Ljava/lang/String;

    .line 548
    .line 549
    .line 550
    move-result-object v26

    .line 551
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 552
    .line 553
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 554
    .line 555
    .line 556
    move-result-object v24

    .line 557
    invoke-static/range {v22 .. v23}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 558
    .line 559
    .line 560
    move-result-object v25

    .line 561
    const/16 v27, 0x0

    .line 562
    .line 563
    const/16 v28, 0x0

    .line 564
    .line 565
    const-string v29, "page_num ASC"

    .line 566
    .line 567
    invoke-virtual/range {v24 .. v29}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 568
    .line 569
    .line 570
    move-result-object v12

    .line 571
    if-eqz v12, :cond_17

    .line 572
    .line 573
    new-instance v9, Landroid/content/ContentValues;

    .line 574
    .line 575
    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 576
    .line 577
    .line 578
    const/4 v8, 0x0

    .line 579
    :goto_c
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    .line 580
    .line 581
    .line 582
    move-result v10

    .line 583
    if-eqz v10, :cond_16

    .line 584
    .line 585
    invoke-interface {v12, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 586
    .line 587
    .line 588
    move-result v10

    .line 589
    move-object/from16 v20, v7

    .line 590
    .line 591
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getLong(I)J

    .line 592
    .line 593
    .line 594
    move-result-wide v6

    .line 595
    invoke-interface {v12, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 596
    .line 597
    .line 598
    move-result v10

    .line 599
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 600
    .line 601
    .line 602
    move-result-object v10

    .line 603
    invoke-static {v10}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    .line 604
    .line 605
    .line 606
    move-result v21

    .line 607
    if-nez v21, :cond_12

    .line 608
    .line 609
    move-object/from16 v21, v1

    .line 610
    .line 611
    iget-object v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 612
    .line 613
    move-object/from16 v24, v4

    .line 614
    .line 615
    sget-object v4, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 616
    .line 617
    if-ne v1, v4, :cond_11

    .line 618
    .line 619
    goto :goto_e

    .line 620
    :cond_11
    sget-object v1, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 621
    .line 622
    invoke-virtual {v1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 623
    .line 624
    .line 625
    move-result-object v1

    .line 626
    new-instance v4, Ljava/lang/StringBuilder;

    .line 627
    .line 628
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 629
    .line 630
    .line 631
    const-string v6, "mergeDocuments file not exist path = "

    .line 632
    .line 633
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 634
    .line 635
    .line 636
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 637
    .line 638
    .line 639
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 640
    .line 641
    .line 642
    move-result-object v4

    .line 643
    invoke-static {v1, v4}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    .line 645
    .line 646
    move-object/from16 v7, v20

    .line 647
    .line 648
    move-object/from16 v1, v21

    .line 649
    .line 650
    move-object/from16 v4, v24

    .line 651
    .line 652
    :goto_d
    const/4 v6, 0x0

    .line 653
    goto :goto_c

    .line 654
    :cond_12
    move-object/from16 v21, v1

    .line 655
    .line 656
    move-object/from16 v24, v4

    .line 657
    .line 658
    :goto_e
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 659
    .line 660
    .line 661
    iget-object v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 662
    .line 663
    add-int/lit8 v4, v8, 0x1

    .line 664
    .line 665
    const/16 v25, 0x1

    .line 666
    .line 667
    move-object v8, v1

    .line 668
    move-object v1, v9

    .line 669
    move-wide v9, v6

    .line 670
    move-object/from16 v27, v11

    .line 671
    .line 672
    move-object/from16 v26, v12

    .line 673
    .line 674
    move-wide v11, v14

    .line 675
    move-object/from16 v28, v13

    .line 676
    .line 677
    move v13, v4

    .line 678
    move-object/from16 v29, v3

    .line 679
    .line 680
    move/from16 v30, v4

    .line 681
    .line 682
    move-wide v3, v14

    .line 683
    move-object v14, v1

    .line 684
    move/from16 v15, v25

    .line 685
    .line 686
    invoke-static/range {v8 .. v15}, Lcom/intsig/camscanner/app/DBUtil;->OO0o〇〇〇〇0(Landroid/content/Context;JJILandroid/content/ContentValues;Z)V

    .line 687
    .line 688
    .line 689
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 690
    .line 691
    sget-object v9, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 692
    .line 693
    const-string v10, "folder_type"

    .line 694
    .line 695
    if-ne v8, v9, :cond_13

    .line 696
    .line 697
    const/4 v9, 0x1

    .line 698
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 699
    .line 700
    .line 701
    move-result-object v8

    .line 702
    invoke-virtual {v1, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 703
    .line 704
    .line 705
    goto :goto_f

    .line 706
    :cond_13
    const/4 v9, 0x1

    .line 707
    sget-object v11, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 708
    .line 709
    if-ne v8, v11, :cond_14

    .line 710
    .line 711
    const/4 v11, 0x0

    .line 712
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 713
    .line 714
    .line 715
    move-result-object v8

    .line 716
    invoke-virtual {v1, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 717
    .line 718
    .line 719
    goto :goto_10

    .line 720
    :cond_14
    :goto_f
    const/4 v11, 0x0

    .line 721
    :goto_10
    sget-object v8, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 722
    .line 723
    const-string v10, "DocsCopyAction-executeCopyDoc-COPY"

    .line 724
    .line 725
    invoke-virtual {v8, v10}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇00(Ljava/lang/String;)V

    .line 726
    .line 727
    .line 728
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 729
    .line 730
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 731
    .line 732
    .line 733
    move-result-object v8

    .line 734
    sget-object v10, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 735
    .line 736
    invoke-virtual {v8, v10, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 737
    .line 738
    .line 739
    move-result-object v8

    .line 740
    if-eqz v8, :cond_15

    .line 741
    .line 742
    invoke-static {v8}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 743
    .line 744
    .line 745
    move-result-wide v12

    .line 746
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 747
    .line 748
    invoke-static {v8, v6, v7, v12, v13}, Lcom/intsig/camscanner/app/DBUtil;->〇0〇O0088o(Landroid/content/Context;JJ)V

    .line 749
    .line 750
    .line 751
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 752
    .line 753
    invoke-static {v8, v6, v7, v12, v13}, Lcom/intsig/camscanner/app/DBUtil;->〇〇8O0〇8(Landroid/content/Context;JJ)V

    .line 754
    .line 755
    .line 756
    iget-object v8, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 757
    .line 758
    invoke-static {v8, v6, v7, v12, v13}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇o00〇〇Oo(Landroid/content/Context;JJ)V

    .line 759
    .line 760
    .line 761
    goto :goto_11

    .line 762
    :cond_15
    sget-object v6, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 763
    .line 764
    invoke-virtual {v6}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 765
    .line 766
    .line 767
    move-result-object v6

    .line 768
    const-string v7, "mergeDocuments insert failed"

    .line 769
    .line 770
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    .line 772
    .line 773
    :goto_11
    move-object v9, v1

    .line 774
    move-wide v14, v3

    .line 775
    move-object/from16 v7, v20

    .line 776
    .line 777
    move-object/from16 v1, v21

    .line 778
    .line 779
    move-object/from16 v4, v24

    .line 780
    .line 781
    move-object/from16 v12, v26

    .line 782
    .line 783
    move-object/from16 v11, v27

    .line 784
    .line 785
    move-object/from16 v13, v28

    .line 786
    .line 787
    move-object/from16 v3, v29

    .line 788
    .line 789
    move/from16 v8, v30

    .line 790
    .line 791
    goto/16 :goto_d

    .line 792
    .line 793
    :cond_16
    move-object/from16 v21, v1

    .line 794
    .line 795
    move-object/from16 v29, v3

    .line 796
    .line 797
    move-object/from16 v24, v4

    .line 798
    .line 799
    move-object/from16 v20, v7

    .line 800
    .line 801
    move-object v1, v9

    .line 802
    move-object/from16 v26, v12

    .line 803
    .line 804
    move-wide v3, v14

    .line 805
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 806
    .line 807
    .line 808
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 809
    .line 810
    .line 811
    const-string v6, "pages"

    .line 812
    .line 813
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 814
    .line 815
    .line 816
    move-result-object v7

    .line 817
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 818
    .line 819
    .line 820
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 821
    .line 822
    .line 823
    move-result v6

    .line 824
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 825
    .line 826
    .line 827
    move-result-object v6

    .line 828
    const-string v7, "type"

    .line 829
    .line 830
    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 831
    .line 832
    .line 833
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 834
    .line 835
    .line 836
    move-result v6

    .line 837
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 838
    .line 839
    .line 840
    move-result-object v6

    .line 841
    const-string v7, "scenario_doc_type"

    .line 842
    .line 843
    invoke-virtual {v1, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 844
    .line 845
    .line 846
    const-string v6, "property"

    .line 847
    .line 848
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O888o0o()Ljava/lang/String;

    .line 849
    .line 850
    .line 851
    move-result-object v5

    .line 852
    invoke-virtual {v1, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    .line 854
    .line 855
    iget-object v5, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 856
    .line 857
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 858
    .line 859
    .line 860
    move-result-object v5

    .line 861
    move-object/from16 v6, v20

    .line 862
    .line 863
    const/4 v7, 0x0

    .line 864
    invoke-virtual {v5, v6, v1, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 865
    .line 866
    .line 867
    iget-object v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 868
    .line 869
    move-wide/from16 v5, v22

    .line 870
    .line 871
    invoke-virtual {v2, v5, v6}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 872
    .line 873
    .line 874
    move-result-object v5

    .line 875
    check-cast v5, Ljava/util/ArrayList;

    .line 876
    .line 877
    invoke-direct {v0, v1, v3, v4, v5}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇0000OOO(Landroid/content/Context;JLjava/util/ArrayList;)V

    .line 878
    .line 879
    .line 880
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 881
    .line 882
    .line 883
    move-result-object v1

    .line 884
    move-object/from16 v5, v29

    .line 885
    .line 886
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 887
    .line 888
    .line 889
    goto :goto_12

    .line 890
    :cond_17
    move-object/from16 v21, v1

    .line 891
    .line 892
    move-object v5, v3

    .line 893
    move-object/from16 v24, v4

    .line 894
    .line 895
    move-wide v3, v14

    .line 896
    :goto_12
    if-eqz v17, :cond_18

    .line 897
    .line 898
    sget-object v1, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 899
    .line 900
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->O〇8O8〇008()Ljava/util/HashMap;

    .line 901
    .line 902
    .line 903
    move-result-object v6

    .line 904
    invoke-virtual {v1, v3, v4, v6}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->Oo8Oo00oo(JLjava/util/HashMap;)V

    .line 905
    .line 906
    .line 907
    :cond_18
    :goto_13
    move-object v3, v5

    .line 908
    move-object/from16 v1, v21

    .line 909
    .line 910
    move-object/from16 v4, v24

    .line 911
    .line 912
    goto/16 :goto_0

    .line 913
    .line 914
    :cond_19
    move-object v5, v3

    .line 915
    const/4 v11, 0x0

    .line 916
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 917
    .line 918
    .line 919
    move-result v1

    .line 920
    if-lez v1, :cond_1a

    .line 921
    .line 922
    sget-object v1, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager;->Oo08:Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;

    .line 923
    .line 924
    const-wide/16 v2, 0x0

    .line 925
    .line 926
    invoke-virtual {v1, v5, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/PriorityUploadDocManager$Companion;->〇O00(Ljava/util/List;J)V

    .line 927
    .line 928
    .line 929
    :cond_1a
    iget-boolean v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o〇:Z

    .line 930
    .line 931
    if-eqz v1, :cond_1c

    .line 932
    .line 933
    new-instance v1, Ljava/util/ArrayList;

    .line 934
    .line 935
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 936
    .line 937
    .line 938
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->o〇0:[J

    .line 939
    .line 940
    array-length v3, v2

    .line 941
    const/4 v6, 0x0

    .line 942
    :goto_14
    if-ge v6, v3, :cond_1b

    .line 943
    .line 944
    aget-wide v4, v2, v6

    .line 945
    .line 946
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 947
    .line 948
    .line 949
    move-result-object v4

    .line 950
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 951
    .line 952
    .line 953
    add-int/lit8 v6, v6, 0x1

    .line 954
    .line 955
    goto :goto_14

    .line 956
    :cond_1b
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->O8ooOoo〇(Ljava/util/ArrayList;)V

    .line 957
    .line 958
    .line 959
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 960
    .line 961
    const/4 v3, 0x2

    .line 962
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 963
    .line 964
    .line 965
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 966
    .line 967
    invoke-static {v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 968
    .line 969
    .line 970
    :cond_1c
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 971
    .line 972
    .line 973
    move-result-object v1

    .line 974
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 975
    .line 976
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 977
    .line 978
    .line 979
    move-result-wide v1

    .line 980
    sget-object v3, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 981
    .line 982
    invoke-virtual {v3}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 983
    .line 984
    .line 985
    move-result-object v4

    .line 986
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O888o0o()Ljava/lang/String;

    .line 987
    .line 988
    .line 989
    move-result-object v5

    .line 990
    invoke-static {v4, v5, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 991
    .line 992
    .line 993
    invoke-virtual {v3}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 994
    .line 995
    .line 996
    move-result-object v1

    .line 997
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 998
    .line 999
    .line 1000
    new-instance v1, LO8o〇O0/o〇0;

    .line 1001
    .line 1002
    invoke-direct {v1, v0}, LO8o〇O0/o〇0;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 1003
    .line 1004
    .line 1005
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇oOO8O8(Ljava/lang/Runnable;)V

    .line 1006
    .line 1007
    .line 1008
    return-void
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method private final 〇O888o0o()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->O8:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8O()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇〇888()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    :goto_0
    return-object v0
    .line 21
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇8O0〇8(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->〇OoO0o0()Lcom/intsig/app/BaseProgressDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->〇OoO0o0()Lcom/intsig/app/BaseProgressDialog;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08:I

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇0OOo〇0(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 23
    .line 24
    iget v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08:I

    .line 25
    .line 26
    new-instance v2, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v3, "0/"

    .line 32
    .line 33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8〇o0〇〇8(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    new-instance v0, LO8o〇O0/〇o〇;

    .line 47
    .line 48
    invoke-direct {v0, p0}, LO8o〇O0/〇o〇;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oOO8O8(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oo〇()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇〇888:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    :goto_0
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇〇808〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "DocsCopyAction"

    .line 7
    .line 8
    const-string v1, "execute copy into offline folder"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O〇()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇8O0〇8(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docProgress"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->〇OoO0o0()Lcom/intsig/app/BaseProgressDialog;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget v1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->oO(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->〇OoO0o0()Lcom/intsig/app/BaseProgressDialog;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iget p1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 29
    .line 30
    iget p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08:I

    .line 31
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string p1, "/"

    .line 41
    .line 42
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p0

    .line 52
    invoke-virtual {v0, p0}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public O8(Lcom/intsig/camscanner/datastruct/FolderItem;)Z
    .locals 1
    .param p1    # Lcom/intsig/camscanner/datastruct/FolderItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "folderItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇〇0〇()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 22
    :goto_1
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public Oo08()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->oo88o8O()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇00()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const-string v1, "DocsCopyAction"

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const-string v0, "copy out of offline folder"

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 18
    .line 19
    new-instance v1, LO8o〇O0/〇080;

    .line 20
    .line 21
    invoke-direct {v1, p0}, LO8o〇O0/〇080;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 22
    .line 23
    .line 24
    const v2, 0x7f1301e4

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OO0O(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 36
    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇oo〇()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_2

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->o〇O8〇〇o()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const-string v0, "no relationship with offline folder"

    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇O〇()V

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_2
    :goto_0
    const-string v0, "copy into offline folder"

    .line 62
    .line 63
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    new-instance v0, Lcom/intsig/camscanner/business/folders/OfflineFolder;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 69
    .line 70
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/business/folders/OfflineFolder;-><init>(Landroid/app/Activity;)V

    .line 71
    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇080:Ljava/util/List;

    .line 74
    .line 75
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    new-instance v2, LO8o〇O0/〇o00〇〇Oo;

    .line 80
    .line 81
    invoke-direct {v2, p0}, LO8o〇O0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;)V

    .line 82
    .line 83
    .line 84
    const/4 v3, 0x1

    .line 85
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->o〇0(ZILcom/intsig/camscanner/business/folders/OfflineFolder$OnUsesTipsListener;)V

    .line 86
    .line 87
    .line 88
    :goto_1
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public getCommonParams()Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->oO80:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    const v1, 0x7f130415

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "mActivity.getString(R.string.a_title_copy)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(Ljava/lang/String;)Lcom/intsig/camscanner/morc/entity/SelectionItem;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/morc/entity/SelectionItem;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/morc/entity/SelectionItem;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 11
    .line 12
    .line 13
    move-result v3

    .line 14
    if-nez v3, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v3, 0x0

    .line 18
    goto :goto_1

    .line 19
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 20
    :goto_1
    const-string v4, "5"

    .line 21
    .line 22
    const-string v5, "2"

    .line 23
    .line 24
    if-eqz v3, :cond_2

    .line 25
    .line 26
    filled-new-array {v5, v4}, [Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string v1, "team_token IS NULL AND sync_state != ? AND sync_state != ? AND parent_sync_id IS NULL"

    .line 35
    .line 36
    goto :goto_2

    .line 37
    :cond_2
    const/4 v3, 0x3

    .line 38
    new-array v3, v3, [Ljava/lang/String;

    .line 39
    .line 40
    aput-object v5, v3, v2

    .line 41
    .line 42
    aput-object v4, v3, v1

    .line 43
    .line 44
    const/4 v1, 0x2

    .line 45
    aput-object p1, v3, v1

    .line 46
    .line 47
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    const-string v1, "team_token IS NULL AND sync_state != ? AND sync_state != ? and parent_sync_id=?"

    .line 52
    .line 53
    :goto_2
    sget-object v3, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 54
    .line 55
    iget-object v4, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇080:Ljava/util/List;

    .line 56
    .line 57
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O8ooOoo〇(Ljava/util/List;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_3

    .line 62
    .line 63
    sget-object v3, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 64
    .line 65
    invoke-virtual {v3}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O00()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    new-instance v4, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    :cond_3
    iput-object v1, v0, Lcom/intsig/camscanner/morc/entity/SelectionItem;->〇080:Ljava/lang/String;

    .line 85
    .line 86
    check-cast p1, Ljava/util/Collection;

    .line 87
    .line 88
    new-array v1, v2, [Ljava/lang/String;

    .line 89
    .line 90
    invoke-interface {p1, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    check-cast p1, [Ljava/lang/String;

    .line 95
    .line 96
    iput-object p1, v0, Lcom/intsig/camscanner/morc/entity/SelectionItem;->〇o00〇〇Oo:[Ljava/lang/String;

    .line 97
    .line 98
    return-object v0
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇080(Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->oO80:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->o800o8O()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->OOO〇O0(Lcom/intsig/camscanner/datastruct/FolderItem;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇080:Ljava/util/List;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O8ooOoo〇(Ljava/util/List;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/4 v0, 0x1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    const v1, 0x7f131db5

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "mActivity.getString(R.string.menu_title_copy)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08:I

    .line 16
    .line 17
    if-lez v1, :cond_0

    .line 18
    .line 19
    new-instance v2, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v0, "("

    .line 28
    .line 29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v0, ")"

    .line 36
    .line 37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    :cond_0
    return-object v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

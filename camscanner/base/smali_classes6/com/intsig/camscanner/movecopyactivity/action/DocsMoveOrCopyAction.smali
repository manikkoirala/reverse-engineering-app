.class public Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;
.super Ljava/lang/Object;
.source "DocsMoveOrCopyAction.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docItems"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final OO0o〇〇([JLcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;ZLjava/lang/String;Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V
    .locals 34

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move-object/from16 v13, p3

    .line 6
    .line 7
    move-object/from16 v14, p4

    .line 8
    .line 9
    const-string v2, "$srcDocIds"

    .line 10
    .line 11
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v2, "this$0"

    .line 15
    .line 16
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const-string v2, "$opDirection"

    .line 20
    .line 21
    invoke-static {v14, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static/range {p0 .. p0}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->〇o00〇〇Oo([J)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    new-instance v15, Landroid/util/LongSparseArray;

    .line 29
    .line 30
    invoke-direct {v15}, Landroid/util/LongSparseArray;-><init>()V

    .line 31
    .line 32
    .line 33
    iget-object v3, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 34
    .line 35
    const-string v4, "filters"

    .line 36
    .line 37
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {v1, v3, v2, v15}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇0〇O0088o(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V

    .line 41
    .line 42
    .line 43
    new-instance v12, Lkotlin/jvm/internal/Ref$IntRef;

    .line 44
    .line 45
    invoke-direct {v12}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 46
    .line 47
    .line 48
    iget-object v2, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 49
    .line 50
    check-cast v2, Ljava/lang/Iterable;

    .line 51
    .line 52
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 53
    .line 54
    .line 55
    move-result-object v16

    .line 56
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    const/16 v17, 0x0

    .line 61
    .line 62
    if-eqz v2, :cond_15

    .line 63
    .line 64
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    move-object v11, v2

    .line 69
    check-cast v11, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 70
    .line 71
    iget v2, v12, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 72
    .line 73
    const/16 v18, 0x1

    .line 74
    .line 75
    add-int/lit8 v2, v2, 0x1

    .line 76
    .line 77
    iput v2, v12, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 78
    .line 79
    new-instance v2, LO8o〇O0/〇8o8o〇;

    .line 80
    .line 81
    invoke-direct {v2, v1, v12}, LO8o〇O0/〇8o8o〇;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V

    .line 82
    .line 83
    .line 84
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->o800o8O(Ljava/lang/Runnable;)V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-static {v2, v13}, Lcom/intsig/camscanner/app/DBUtil;->〇o〇8(Landroid/content/Context;Ljava/lang/String;)Z

    .line 92
    .line 93
    .line 94
    move-result v19

    .line 95
    invoke-virtual {v11}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 96
    .line 97
    .line 98
    move-result-wide v9

    .line 99
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-virtual {v11}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    const/4 v4, 0x1

    .line 108
    const/4 v6, 0x0

    .line 109
    const/4 v7, 0x1

    .line 110
    move-object/from16 v5, p3

    .line 111
    .line 112
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/util/Util;->ooo〇8oO(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v3

    .line 116
    new-instance v8, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 117
    .line 118
    const/4 v4, 0x0

    .line 119
    const/4 v6, 0x0

    .line 120
    const-string v7, ""

    .line 121
    .line 122
    const/16 v20, 0x0

    .line 123
    .line 124
    const/16 v21, 0x0

    .line 125
    .line 126
    const/16 v22, 0x0

    .line 127
    .line 128
    move-object v2, v8

    .line 129
    move-object/from16 v23, v8

    .line 130
    .line 131
    move-object/from16 v8, v20

    .line 132
    .line 133
    move-wide/from16 v24, v9

    .line 134
    .line 135
    move/from16 v9, v21

    .line 136
    .line 137
    move/from16 v10, v22

    .line 138
    .line 139
    move-object/from16 v20, v11

    .line 140
    .line 141
    move/from16 v11, v19

    .line 142
    .line 143
    move-object/from16 v19, v12

    .line 144
    .line 145
    move-object/from16 v12, p4

    .line 146
    .line 147
    invoke-direct/range {v2 .. v12}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZIZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 148
    .line 149
    .line 150
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v2

    .line 154
    invoke-virtual/range {v20 .. v20}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v3

    .line 158
    new-instance v4, Ljava/lang/StringBuilder;

    .line 159
    .line 160
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    .line 162
    .line 163
    const-string v5, "executeCopyDoc oldDocSyncId: "

    .line 164
    .line 165
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v4

    .line 175
    const-string v5, "DocsCopyAction"

    .line 176
    .line 177
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    if-eqz v3, :cond_1

    .line 181
    .line 182
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 183
    .line 184
    .line 185
    move-result v4

    .line 186
    if-nez v4, :cond_0

    .line 187
    .line 188
    goto :goto_1

    .line 189
    :cond_0
    const/4 v4, 0x0

    .line 190
    goto :goto_2

    .line 191
    :cond_1
    :goto_1
    const/4 v4, 0x1

    .line 192
    :goto_2
    if-nez v4, :cond_c

    .line 193
    .line 194
    invoke-static {v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 195
    .line 196
    .line 197
    move-result v4

    .line 198
    if-eqz v4, :cond_c

    .line 199
    .line 200
    sget-object v4, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 201
    .line 202
    invoke-virtual {v4, v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 203
    .line 204
    .line 205
    move-result-object v3

    .line 206
    if-eqz v3, :cond_2

    .line 207
    .line 208
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v2

    .line 212
    invoke-virtual {v3}, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->getIdSuffix()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v3

    .line 216
    new-instance v7, Ljava/lang/StringBuilder;

    .line 217
    .line 218
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    const-string v2, "_"

    .line 225
    .line 226
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    :cond_2
    const-string v3, "newDocSyncId"

    .line 237
    .line 238
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇〇888(Ljava/lang/String;)Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v3

    .line 245
    invoke-virtual/range {v20 .. v20}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 246
    .line 247
    .line 248
    move-result-object v4

    .line 249
    if-eqz v4, :cond_4

    .line 250
    .line 251
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 252
    .line 253
    .line 254
    move-result v4

    .line 255
    if-nez v4, :cond_3

    .line 256
    .line 257
    goto :goto_3

    .line 258
    :cond_3
    const/4 v4, 0x0

    .line 259
    goto :goto_4

    .line 260
    :cond_4
    :goto_3
    const/4 v4, 0x1

    .line 261
    :goto_4
    if-eqz v4, :cond_6

    .line 262
    .line 263
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 264
    .line 265
    .line 266
    move-result-object v4

    .line 267
    invoke-virtual/range {v20 .. v20}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 268
    .line 269
    .line 270
    move-result-wide v7

    .line 271
    invoke-static {v4, v7, v8}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 272
    .line 273
    .line 274
    move-result-object v4

    .line 275
    if-eqz v4, :cond_5

    .line 276
    .line 277
    invoke-virtual {v4}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object v4

    .line 281
    move-object/from16 v7, v20

    .line 282
    .line 283
    goto :goto_5

    .line 284
    :cond_5
    move-object/from16 v7, v20

    .line 285
    .line 286
    const/4 v4, 0x0

    .line 287
    :goto_5
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇0(Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    goto :goto_6

    .line 291
    :cond_6
    move-object/from16 v7, v20

    .line 292
    .line 293
    :goto_6
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object v4

    .line 297
    if-eqz v4, :cond_8

    .line 298
    .line 299
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 300
    .line 301
    .line 302
    move-result v4

    .line 303
    if-nez v4, :cond_7

    .line 304
    .line 305
    goto :goto_7

    .line 306
    :cond_7
    const/4 v4, 0x0

    .line 307
    goto :goto_8

    .line 308
    :cond_8
    :goto_7
    const/4 v4, 0x1

    .line 309
    :goto_8
    if-nez v4, :cond_b

    .line 310
    .line 311
    if-eqz v3, :cond_a

    .line 312
    .line 313
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 314
    .line 315
    .line 316
    move-result v4

    .line 317
    if-nez v4, :cond_9

    .line 318
    .line 319
    goto :goto_9

    .line 320
    :cond_9
    const/4 v4, 0x0

    .line 321
    goto :goto_a

    .line 322
    :cond_a
    :goto_9
    const/4 v4, 0x1

    .line 323
    :goto_a
    if-nez v4, :cond_b

    .line 324
    .line 325
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 326
    .line 327
    .line 328
    move-result-object v4

    .line 329
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 330
    .line 331
    .line 332
    invoke-static {v4, v3}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 333
    .line 334
    .line 335
    move-result v4

    .line 336
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇〇0〇()Ljava/lang/String;

    .line 337
    .line 338
    .line 339
    move-result-object v8

    .line 340
    new-instance v9, Ljava/lang/StringBuilder;

    .line 341
    .line 342
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    .line 344
    .line 345
    const-string v10, "executeCopyDoc: copyOfficeFile: "

    .line 346
    .line 347
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    .line 349
    .line 350
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    .line 352
    .line 353
    const-string v8, " to: "

    .line 354
    .line 355
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    .line 357
    .line 358
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    .line 360
    .line 361
    const-string v8, ", result: "

    .line 362
    .line 363
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    .line 365
    .line 366
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 367
    .line 368
    .line 369
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 370
    .line 371
    .line 372
    move-result-object v4

    .line 373
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    .line 375
    .line 376
    move-object/from16 v4, v23

    .line 377
    .line 378
    iput-object v3, v4, Lcom/intsig/camscanner/datastruct/DocProperty;->OoO8:Ljava/lang/String;

    .line 379
    .line 380
    goto :goto_c

    .line 381
    :cond_b
    :goto_b
    move-object/from16 v4, v23

    .line 382
    .line 383
    goto :goto_c

    .line 384
    :cond_c
    move-object/from16 v7, v20

    .line 385
    .line 386
    goto :goto_b

    .line 387
    :goto_c
    iput-object v2, v4, Lcom/intsig/camscanner/datastruct/DocProperty;->〇O〇:Ljava/lang/String;

    .line 388
    .line 389
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object v2

    .line 393
    iput-object v2, v4, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇8O0〇8:Ljava/lang/String;

    .line 394
    .line 395
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->OOO〇O0()I

    .line 396
    .line 397
    .line 398
    move-result v2

    .line 399
    iput v2, v4, Lcom/intsig/camscanner/datastruct/DocProperty;->〇0〇O0088o:I

    .line 400
    .line 401
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 402
    .line 403
    .line 404
    move-result-object v2

    .line 405
    invoke-static {v2, v4}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 406
    .line 407
    .line 408
    move-result-object v2

    .line 409
    if-nez v2, :cond_d

    .line 410
    .line 411
    sget-object v2, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 412
    .line 413
    invoke-virtual {v2}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 414
    .line 415
    .line 416
    move-result-object v2

    .line 417
    const-string v3, "executeCopy newDocUri == null"

    .line 418
    .line 419
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    .line 421
    .line 422
    new-instance v2, LO8o〇O0/〇O8o08O;

    .line 423
    .line 424
    invoke-direct {v2, v1}, LO8o〇O0/〇O8o08O;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;)V

    .line 425
    .line 426
    .line 427
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->o800o8O(Ljava/lang/Runnable;)V

    .line 428
    .line 429
    .line 430
    goto/16 :goto_11

    .line 431
    .line 432
    :cond_d
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 433
    .line 434
    .line 435
    move-result-wide v3

    .line 436
    const-string v5, "_id"

    .line 437
    .line 438
    const-string v8, "_data"

    .line 439
    .line 440
    filled-new-array {v5, v8}, [Ljava/lang/String;

    .line 441
    .line 442
    .line 443
    move-result-object v28

    .line 444
    iget-object v9, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 445
    .line 446
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 447
    .line 448
    .line 449
    move-result-object v26

    .line 450
    invoke-static/range {v24 .. v25}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 451
    .line 452
    .line 453
    move-result-object v27

    .line 454
    const/16 v29, 0x0

    .line 455
    .line 456
    const/16 v30, 0x0

    .line 457
    .line 458
    const-string v31, "page_num ASC"

    .line 459
    .line 460
    invoke-virtual/range {v26 .. v31}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 461
    .line 462
    .line 463
    move-result-object v9

    .line 464
    if-eqz v9, :cond_14

    .line 465
    .line 466
    new-instance v10, Landroid/content/ContentValues;

    .line 467
    .line 468
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 469
    .line 470
    .line 471
    const/4 v11, 0x0

    .line 472
    :goto_d
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    .line 473
    .line 474
    .line 475
    move-result v12

    .line 476
    if-eqz v12, :cond_13

    .line 477
    .line 478
    invoke-interface {v9, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 479
    .line 480
    .line 481
    move-result v12

    .line 482
    move-object/from16 v21, v7

    .line 483
    .line 484
    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    .line 485
    .line 486
    .line 487
    move-result-wide v6

    .line 488
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 489
    .line 490
    .line 491
    move-result v12

    .line 492
    invoke-interface {v9, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 493
    .line 494
    .line 495
    move-result-object v12

    .line 496
    invoke-static {v12}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    .line 497
    .line 498
    .line 499
    move-result v22

    .line 500
    if-nez v22, :cond_f

    .line 501
    .line 502
    move-object/from16 v22, v5

    .line 503
    .line 504
    sget-object v5, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 505
    .line 506
    if-ne v14, v5, :cond_e

    .line 507
    .line 508
    goto :goto_e

    .line 509
    :cond_e
    sget-object v5, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 510
    .line 511
    invoke-virtual {v5}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 512
    .line 513
    .line 514
    move-result-object v5

    .line 515
    new-instance v6, Ljava/lang/StringBuilder;

    .line 516
    .line 517
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 518
    .line 519
    .line 520
    const-string v7, "mergeDocuments file not exist path = "

    .line 521
    .line 522
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    .line 524
    .line 525
    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 526
    .line 527
    .line 528
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 529
    .line 530
    .line 531
    move-result-object v6

    .line 532
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    .line 534
    .line 535
    move-object/from16 v7, v21

    .line 536
    .line 537
    move-object/from16 v5, v22

    .line 538
    .line 539
    goto :goto_d

    .line 540
    :cond_f
    move-object/from16 v22, v5

    .line 541
    .line 542
    :goto_e
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    .line 543
    .line 544
    .line 545
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 546
    .line 547
    add-int/lit8 v11, v11, 0x1

    .line 548
    .line 549
    const/16 v33, 0x1

    .line 550
    .line 551
    move-object/from16 v26, v5

    .line 552
    .line 553
    move-wide/from16 v27, v6

    .line 554
    .line 555
    move-wide/from16 v29, v3

    .line 556
    .line 557
    move/from16 v31, v11

    .line 558
    .line 559
    move-object/from16 v32, v10

    .line 560
    .line 561
    invoke-static/range {v26 .. v33}, Lcom/intsig/camscanner/app/DBUtil;->OO0o〇〇〇〇0(Landroid/content/Context;JJILandroid/content/ContentValues;Z)V

    .line 562
    .line 563
    .line 564
    sget-object v5, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 565
    .line 566
    const-string v12, "folder_type"

    .line 567
    .line 568
    if-ne v14, v5, :cond_10

    .line 569
    .line 570
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 571
    .line 572
    .line 573
    move-result-object v5

    .line 574
    invoke-virtual {v10, v12, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 575
    .line 576
    .line 577
    goto :goto_f

    .line 578
    :cond_10
    sget-object v5, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 579
    .line 580
    if-ne v14, v5, :cond_11

    .line 581
    .line 582
    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 583
    .line 584
    .line 585
    move-result-object v5

    .line 586
    invoke-virtual {v10, v12, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 587
    .line 588
    .line 589
    :cond_11
    :goto_f
    sget-object v5, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 590
    .line 591
    const-string v12, "DocsCopyAction-executeCopyDoc-COPY"

    .line 592
    .line 593
    invoke-virtual {v5, v12}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇00(Ljava/lang/String;)V

    .line 594
    .line 595
    .line 596
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 597
    .line 598
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 599
    .line 600
    .line 601
    move-result-object v5

    .line 602
    sget-object v12, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 603
    .line 604
    invoke-virtual {v5, v12, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 605
    .line 606
    .line 607
    move-result-object v5

    .line 608
    if-eqz v5, :cond_12

    .line 609
    .line 610
    move/from16 v23, v11

    .line 611
    .line 612
    invoke-static {v5}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 613
    .line 614
    .line 615
    move-result-wide v11

    .line 616
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 617
    .line 618
    invoke-static {v5, v6, v7, v11, v12}, Lcom/intsig/camscanner/app/DBUtil;->〇0〇O0088o(Landroid/content/Context;JJ)V

    .line 619
    .line 620
    .line 621
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 622
    .line 623
    invoke-static {v5, v6, v7, v11, v12}, Lcom/intsig/camscanner/app/DBUtil;->〇〇8O0〇8(Landroid/content/Context;JJ)V

    .line 624
    .line 625
    .line 626
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 627
    .line 628
    invoke-static {v5, v6, v7, v11, v12}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇o00〇〇Oo(Landroid/content/Context;JJ)V

    .line 629
    .line 630
    .line 631
    goto :goto_10

    .line 632
    :cond_12
    move/from16 v23, v11

    .line 633
    .line 634
    sget-object v5, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 635
    .line 636
    invoke-virtual {v5}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 637
    .line 638
    .line 639
    move-result-object v5

    .line 640
    const-string v6, "mergeDocuments insert failed"

    .line 641
    .line 642
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    .line 644
    .line 645
    :goto_10
    move-object/from16 v7, v21

    .line 646
    .line 647
    move-object/from16 v5, v22

    .line 648
    .line 649
    move/from16 v11, v23

    .line 650
    .line 651
    goto/16 :goto_d

    .line 652
    .line 653
    :cond_13
    move-object/from16 v21, v7

    .line 654
    .line 655
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 656
    .line 657
    .line 658
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    .line 659
    .line 660
    .line 661
    const-string v5, "pages"

    .line 662
    .line 663
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 664
    .line 665
    .line 666
    move-result-object v6

    .line 667
    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 668
    .line 669
    .line 670
    invoke-virtual/range {v21 .. v21}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 671
    .line 672
    .line 673
    move-result v5

    .line 674
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 675
    .line 676
    .line 677
    move-result-object v5

    .line 678
    const-string v6, "type"

    .line 679
    .line 680
    invoke-virtual {v10, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 681
    .line 682
    .line 683
    invoke-virtual/range {v21 .. v21}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 684
    .line 685
    .line 686
    move-result v5

    .line 687
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 688
    .line 689
    .line 690
    move-result-object v5

    .line 691
    const-string v6, "scenario_doc_type"

    .line 692
    .line 693
    invoke-virtual {v10, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 694
    .line 695
    .line 696
    const-string v5, "property"

    .line 697
    .line 698
    invoke-virtual/range {v21 .. v21}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O888o0o()Ljava/lang/String;

    .line 699
    .line 700
    .line 701
    move-result-object v6

    .line 702
    invoke-virtual {v10, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    .line 704
    .line 705
    iget-object v5, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 706
    .line 707
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 708
    .line 709
    .line 710
    move-result-object v5

    .line 711
    const/4 v6, 0x0

    .line 712
    invoke-virtual {v5, v2, v10, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 713
    .line 714
    .line 715
    iget-object v2, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 716
    .line 717
    move-wide/from16 v5, v24

    .line 718
    .line 719
    invoke-virtual {v15, v5, v6}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 720
    .line 721
    .line 722
    move-result-object v5

    .line 723
    check-cast v5, Ljava/util/ArrayList;

    .line 724
    .line 725
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇O888o0o(Landroid/content/Context;JLjava/util/ArrayList;)V

    .line 726
    .line 727
    .line 728
    :cond_14
    :goto_11
    move-object/from16 v12, v19

    .line 729
    .line 730
    goto/16 :goto_0

    .line 731
    .line 732
    :cond_15
    if-eqz p2, :cond_17

    .line 733
    .line 734
    new-instance v2, Ljava/util/ArrayList;

    .line 735
    .line 736
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 737
    .line 738
    .line 739
    array-length v3, v0

    .line 740
    const/4 v4, 0x0

    .line 741
    :goto_12
    if-ge v4, v3, :cond_16

    .line 742
    .line 743
    aget-wide v5, v0, v4

    .line 744
    .line 745
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 746
    .line 747
    .line 748
    move-result-object v5

    .line 749
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 750
    .line 751
    .line 752
    add-int/lit8 v4, v4, 0x1

    .line 753
    .line 754
    goto :goto_12

    .line 755
    :cond_16
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->OoO8(Ljava/util/ArrayList;)V

    .line 756
    .line 757
    .line 758
    iget-object v0, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 759
    .line 760
    const/4 v3, 0x2

    .line 761
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 762
    .line 763
    .line 764
    iget-object v0, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 765
    .line 766
    invoke-static {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 767
    .line 768
    .line 769
    :cond_17
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 770
    .line 771
    .line 772
    move-result-object v0

    .line 773
    iget-object v2, v1, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 774
    .line 775
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 776
    .line 777
    .line 778
    move-result-wide v2

    .line 779
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 780
    .line 781
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 782
    .line 783
    .line 784
    move-result-object v4

    .line 785
    invoke-static {v4, v13, v2, v3}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 786
    .line 787
    .line 788
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 789
    .line 790
    .line 791
    move-result-object v0

    .line 792
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 793
    .line 794
    .line 795
    new-instance v0, LO8o〇O0/OO0o〇〇;

    .line 796
    .line 797
    invoke-direct {v0, v1, v13}, LO8o〇O0/OO0o〇〇;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Ljava/lang/String;)V

    .line 798
    .line 799
    .line 800
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->o800o8O(Ljava/lang/Runnable;)V

    .line 801
    .line 802
    .line 803
    return-void
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇O〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OoO8(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    const/4 v1, 0x4

    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    invoke-static {v0, p1, v3, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇O8〇〇o(Landroid/content/Context;Ljava/util/List;ZILjava/lang/Object;)Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 11
    .line 12
    invoke-static {v1, p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    new-instance v1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 19
    .line 20
    .line 21
    check-cast p1, Ljava/util/Collection;

    .line 22
    .line 23
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/app/DBUtil;->o〇0o〇〇(Landroid/content/Context;Ljava/util/List;I)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docProgress"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->〇OoO0o0()Lcom/intsig/app/BaseProgressDialog;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 18
    .line 19
    const v2, 0x7f130043

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    iget p1, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 27
    .line 28
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {p0}, Ljava/util/List;->size()I

    .line 31
    .line 32
    .line 33
    move-result p0

    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string p1, "/"

    .line 46
    .line 47
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p0

    .line 57
    invoke-virtual {v0, p0}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o800o8O(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇〇808〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0〇O0088o(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/util/LongSparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "tag_id"

    .line 2
    .line 3
    const-string v1, "document_id"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v4

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "document_id in "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v5

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v6, 0x0

    .line 33
    const/4 v7, 0x0

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 41
    .line 42
    .line 43
    move-result p2

    .line 44
    if-eqz p2, :cond_1

    .line 45
    .line 46
    const/4 p2, 0x1

    .line 47
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 48
    .line 49
    .line 50
    move-result-wide v0

    .line 51
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-ltz v0, :cond_0

    .line 56
    .line 57
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 58
    .line 59
    .line 60
    move-result-wide v0

    .line 61
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    const-string v0, "tagMap[cursor.getLong(1)]"

    .line 66
    .line 67
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    check-cast p2, Ljava/util/ArrayList;

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 79
    .line 80
    .line 81
    move-result-wide v1

    .line 82
    invoke-virtual {p3, v1, v2, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    move-object p2, v0

    .line 86
    :goto_1
    const/4 v0, 0x0

    .line 87
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 88
    .line 89
    .line 90
    move-result-wide v0

    .line 91
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 100
    .line 101
    .line 102
    :cond_2
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic 〇80〇808〇O([JLcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;ZLjava/lang/String;Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->OO0o〇〇([JLcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;ZLjava/lang/String;Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Lkotlin/jvm/internal/Ref$IntRef;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O888o0o(Landroid/content/Context;JLjava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p4, :cond_4

    .line 2
    .line 3
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_2

    .line 10
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object p4

    .line 19
    :cond_1
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_2

    .line 24
    .line 25
    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    check-cast v1, Ljava/lang/Long;

    .line 30
    .line 31
    const-string v2, "tagId"

    .line 32
    .line 33
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    .line 37
    .line 38
    .line 39
    move-result-wide v2

    .line 40
    invoke-static {p1, v2, v3}, Lcom/intsig/camscanner/db/dao/TagDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-eqz v2, :cond_1

    .line 45
    .line 46
    new-instance v2, Landroid/content/ContentValues;

    .line 47
    .line 48
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v3, "document_id"

    .line 52
    .line 53
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 58
    .line 59
    .line 60
    const-string v3, "tag_id"

    .line 61
    .line 62
    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 63
    .line 64
    .line 65
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 66
    .line 67
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 84
    .line 85
    .line 86
    move-result p2

    .line 87
    if-lez p2, :cond_3

    .line 88
    .line 89
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    sget-object p2, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 94
    .line 95
    invoke-virtual {p1, p2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :catch_0
    move-exception p1

    .line 100
    sget-object p2, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object p2

    .line 106
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    .line 108
    .line 109
    :cond_3
    :goto_1
    return-void

    .line 110
    :cond_4
    :goto_2
    sget-object p1, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->o8o:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;

    .line 111
    .line 112
    invoke-virtual {p1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity$Companion;->〇080()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    const-string p2, "tagIds is empty"

    .line 117
    .line 118
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final 〇O〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;Ljava/lang/String;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OooO〇()V

    .line 9
    .line 10
    .line 11
    new-instance v0, Landroid/content/Intent;

    .line 12
    .line 13
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "targetDirSyncId"

    .line 17
    .line 18
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string v0, "Intent().putExtra(HomeCo\u2026C_ID, targetParentSyncId)"

    .line 23
    .line 24
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 28
    .line 29
    const/4 v1, -0x1

    .line 30
    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 31
    .line 32
    .line 33
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇〇808〇(Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OooO〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected final 〇O00()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O(Ljava/lang/String;Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;Z)V
    .locals 9
    .param p2    # Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "opDirection"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 7
    .line 8
    const v1, 0x7f130043

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    new-instance v3, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v1, "0/"

    .line 30
    .line 31
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8〇o0〇〇8(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇o00〇〇Oo:Ljava/util/List;

    .line 45
    .line 46
    check-cast v0, Ljava/lang/Iterable;

    .line 47
    .line 48
    new-instance v1, Ljava/util/ArrayList;

    .line 49
    .line 50
    const/16 v2, 0xa

    .line 51
    .line 52
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    .line 58
    .line 59
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    if-eqz v2, :cond_0

    .line 68
    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 74
    .line 75
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 76
    .line 77
    .line 78
    move-result-wide v2

    .line 79
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_0
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->OOO8o〇〇(Ljava/util/Collection;)[J

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    new-instance v0, LO8o〇O0/OO0o〇〇〇〇0;

    .line 92
    .line 93
    move-object v3, v0

    .line 94
    move-object v5, p0

    .line 95
    move v6, p3

    .line 96
    move-object v7, p1

    .line 97
    move-object v8, p2

    .line 98
    invoke-direct/range {v3 .. v8}, LO8o〇O0/OO0o〇〇〇〇0;-><init>([JLcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;ZLjava/lang/String;Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 99
    .line 100
    .line 101
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method protected final 〇〇8O0〇8()Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/DocsMoveOrCopyAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;
.super Ljava/lang/Object;
.source "OtherMoveInAction.kt"

# interfaces
.implements Lcom/intsig/camscanner/movecopyactivity/action/IAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

.field private Oo08:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:I

.field private final o〇0:Ljava/lang/String;

.field private final 〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:I

.field private final 〇〇888:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇8o8o〇:Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;Lcom/intsig/camscanner/datastruct/FolderItem;I)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/datastruct/FolderItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "targetFolderItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 17
    .line 18
    iput p3, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o〇:I

    .line 19
    .line 20
    new-instance p1, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 26
    .line 27
    new-instance p1, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oooo8o0〇()J

    .line 41
    .line 42
    .line 43
    move-result-wide v0

    .line 44
    iput-wide v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇〇888:J

    .line 45
    .line 46
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    iput p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->oO80:I

    .line 51
    .line 52
    new-instance p1, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$mDisabledParentSyncId$2;

    .line 53
    .line 54
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$mDisabledParentSyncId$2;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)V

    .line 55
    .line 56
    .line 57
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇80〇808〇O:Lkotlin/Lazy;

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇〇8O0〇8()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;I)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 14
    .line 15
    invoke-direct {p1, v0, v1, v2, p0}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;-><init>(Ljava/util/List;Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;ZLcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/movecopyactivity/action/DocsCopyAction;->Oo08()V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oooo8o0〇(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇〇808〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->oO80:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇808〇()V
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    new-instance v3, Ljava/util/HashSet;

    .line 16
    .line 17
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 18
    .line 19
    .line 20
    const-wide/16 v4, 0x1

    .line 21
    .line 22
    const/4 v6, 0x0

    .line 23
    const-string v7, "upload_time"

    .line 24
    .line 25
    const-string v8, "OtherMoveInAction"

    .line 26
    .line 27
    const/4 v9, 0x0

    .line 28
    const/4 v10, 0x1

    .line 29
    if-nez v2, :cond_6

    .line 30
    .line 31
    const-string v2, "start move docs into this folder"

    .line 32
    .line 33
    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 37
    .line 38
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    new-array v2, v2, [J

    .line 43
    .line 44
    iget-object v11, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 45
    .line 46
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 47
    .line 48
    .line 49
    move-result-object v11

    .line 50
    const/4 v12, 0x0

    .line 51
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    .line 52
    .line 53
    .line 54
    move-result v13

    .line 55
    if-eqz v13, :cond_1

    .line 56
    .line 57
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v13

    .line 61
    check-cast v13, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 62
    .line 63
    invoke-virtual {v13}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo08()J

    .line 64
    .line 65
    .line 66
    move-result-wide v14

    .line 67
    invoke-virtual {v13}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇0()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v13

    .line 71
    aput-wide v14, v2, v12

    .line 72
    .line 73
    if-eqz v13, :cond_0

    .line 74
    .line 75
    invoke-virtual {v3, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    :cond_0
    add-int/lit8 v12, v12, 0x1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    invoke-static {v2}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->〇o00〇〇Oo([J)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    new-instance v11, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    const-string v12, "(_id in "

    .line 91
    .line 92
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    const-string v2, ")"

    .line 99
    .line 100
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v2

    .line 107
    new-instance v11, Landroid/content/ContentValues;

    .line 108
    .line 109
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 110
    .line 111
    .line 112
    iget-object v12, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 113
    .line 114
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 115
    .line 116
    .line 117
    move-result v12

    .line 118
    const-string v13, "sync_dir_id"

    .line 119
    .line 120
    if-eqz v12, :cond_2

    .line 121
    .line 122
    invoke-virtual {v11, v13}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_2
    iget-object v12, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 127
    .line 128
    invoke-virtual {v11, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    :goto_1
    iget-object v12, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 132
    .line 133
    iget-object v13, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 134
    .line 135
    invoke-static {v12, v13}, Lcom/intsig/camscanner/app/DBUtil;->OOo8o〇O(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v12

    .line 139
    if-eqz v12, :cond_4

    .line 140
    .line 141
    invoke-static {v12}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 142
    .line 143
    .line 144
    move-result v13

    .line 145
    if-eqz v13, :cond_3

    .line 146
    .line 147
    goto :goto_2

    .line 148
    :cond_3
    const/4 v13, 0x0

    .line 149
    goto :goto_3

    .line 150
    :cond_4
    :goto_2
    const/4 v13, 0x1

    .line 151
    :goto_3
    if-nez v13, :cond_5

    .line 152
    .line 153
    iget-object v13, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 154
    .line 155
    invoke-static {v13, v12}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇080〇o0(Landroid/content/Context;Ljava/lang/String;)J

    .line 156
    .line 157
    .line 158
    move-result-wide v12

    .line 159
    add-long/2addr v12, v4

    .line 160
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 161
    .line 162
    .line 163
    move-result-object v12

    .line 164
    invoke-virtual {v11, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 165
    .line 166
    .line 167
    goto :goto_4

    .line 168
    :cond_5
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 169
    .line 170
    .line 171
    move-result-object v12

    .line 172
    invoke-virtual {v11, v7, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 173
    .line 174
    .line 175
    :goto_4
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 176
    .line 177
    .line 178
    move-result-object v12

    .line 179
    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 180
    .line 181
    .line 182
    move-result-object v12

    .line 183
    sget-object v13, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 184
    .line 185
    invoke-virtual {v12, v13, v11, v2, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 186
    .line 187
    .line 188
    move-result v11

    .line 189
    iget-object v12, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 190
    .line 191
    new-instance v13, Ljava/lang/StringBuilder;

    .line 192
    .line 193
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    .line 195
    .line 196
    const-string v14, "executeMove num="

    .line 197
    .line 198
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    const-string v11, " where="

    .line 205
    .line 206
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    const-string v2, " mParentSyncId="

    .line 213
    .line 214
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v2

    .line 224
    invoke-static {v8, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    .line 226
    .line 227
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 228
    .line 229
    .line 230
    move-result-object v2

    .line 231
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 232
    .line 233
    .line 234
    move-result-object v11

    .line 235
    invoke-virtual {v2, v11}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 236
    .line 237
    .line 238
    move-result-wide v11

    .line 239
    if-nez v1, :cond_f

    .line 240
    .line 241
    const-string v1, "start move folders into this folder"

    .line 242
    .line 243
    invoke-static {v8, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 247
    .line 248
    .line 249
    move-result-object v1

    .line 250
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 251
    .line 252
    invoke-static {v1, v2}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 253
    .line 254
    .line 255
    move-result-object v1

    .line 256
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 257
    .line 258
    .line 259
    move-result-object v2

    .line 260
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 261
    .line 262
    .line 263
    move-result v2

    .line 264
    if-nez v2, :cond_7

    .line 265
    .line 266
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 267
    .line 268
    .line 269
    move-result v2

    .line 270
    if-ne v2, v10, :cond_7

    .line 271
    .line 272
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 277
    .line 278
    .line 279
    move-result-object v8

    .line 280
    invoke-static {v2, v8}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->OO0o〇〇〇〇0(Landroid/content/Context;Ljava/lang/String;)J

    .line 281
    .line 282
    .line 283
    move-result-wide v11

    .line 284
    goto :goto_5

    .line 285
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 286
    .line 287
    .line 288
    move-result-object v2

    .line 289
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 290
    .line 291
    .line 292
    move-result-object v8

    .line 293
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 294
    .line 295
    .line 296
    move-result-wide v11

    .line 297
    :goto_5
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 298
    .line 299
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 300
    .line 301
    .line 302
    move-result-object v2

    .line 303
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 304
    .line 305
    .line 306
    move-result v8

    .line 307
    if-eqz v8, :cond_f

    .line 308
    .line 309
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 310
    .line 311
    .line 312
    move-result-object v8

    .line 313
    check-cast v8, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 314
    .line 315
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 316
    .line 317
    .line 318
    move-result-object v13

    .line 319
    if-eqz v13, :cond_9

    .line 320
    .line 321
    invoke-static {v13}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 322
    .line 323
    .line 324
    move-result v13

    .line 325
    if-eqz v13, :cond_8

    .line 326
    .line 327
    goto :goto_7

    .line 328
    :cond_8
    const/4 v13, 0x0

    .line 329
    goto :goto_8

    .line 330
    :cond_9
    :goto_7
    const/4 v13, 0x1

    .line 331
    :goto_8
    if-eqz v13, :cond_a

    .line 332
    .line 333
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 334
    .line 335
    .line 336
    move-result-object v13

    .line 337
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oooo8o0〇()J

    .line 338
    .line 339
    .line 340
    move-result-wide v14

    .line 341
    invoke-static {v13, v14, v15}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 342
    .line 343
    .line 344
    move-result-object v13

    .line 345
    invoke-virtual {v13}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o〇()I

    .line 346
    .line 347
    .line 348
    move-result v13

    .line 349
    if-ne v13, v10, :cond_a

    .line 350
    .line 351
    const/4 v13, 0x0

    .line 352
    goto :goto_9

    .line 353
    :cond_a
    const/4 v13, 0x1

    .line 354
    :goto_9
    sget-object v14, Lcom/intsig/camscanner/provider/Documents$Dir;->〇080:Landroid/net/Uri;

    .line 355
    .line 356
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oooo8o0〇()J

    .line 357
    .line 358
    .line 359
    move-result-wide v9

    .line 360
    invoke-static {v14, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 361
    .line 362
    .line 363
    move-result-object v9

    .line 364
    const-string v10, "withAppendedId(Documents\u2026folderItem.getFolderId())"

    .line 365
    .line 366
    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 367
    .line 368
    .line 369
    new-instance v10, Landroid/content/ContentValues;

    .line 370
    .line 371
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 372
    .line 373
    .line 374
    add-long v16, v11, v4

    .line 375
    .line 376
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 377
    .line 378
    .line 379
    move-result-object v14

    .line 380
    invoke-virtual {v10, v7, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 381
    .line 382
    .line 383
    const-string v14, "parent_sync_id"

    .line 384
    .line 385
    iget-object v4, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 386
    .line 387
    invoke-virtual {v10, v14, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    .line 389
    .line 390
    const/4 v4, 0x3

    .line 391
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 392
    .line 393
    .line 394
    move-result-object v4

    .line 395
    const-string v5, "sync_state"

    .line 396
    .line 397
    invoke-virtual {v10, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 398
    .line 399
    .line 400
    if-eqz v13, :cond_c

    .line 401
    .line 402
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 403
    .line 404
    .line 405
    move-result v4

    .line 406
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 407
    .line 408
    .line 409
    move-result-object v4

    .line 410
    const-string v5, "share_status"

    .line 411
    .line 412
    invoke-virtual {v10, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 413
    .line 414
    .line 415
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 416
    .line 417
    .line 418
    move-result-object v4

    .line 419
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 420
    .line 421
    .line 422
    move-result v4

    .line 423
    const-string v5, "share_id"

    .line 424
    .line 425
    if-eqz v4, :cond_b

    .line 426
    .line 427
    invoke-virtual {v10, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 428
    .line 429
    .line 430
    goto :goto_a

    .line 431
    :cond_b
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 432
    .line 433
    .line 434
    move-result-object v4

    .line 435
    invoke-virtual {v10, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    .line 437
    .line 438
    :cond_c
    :goto_a
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 439
    .line 440
    .line 441
    move-result-object v18

    .line 442
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O00()Ljava/lang/String;

    .line 443
    .line 444
    .line 445
    move-result-object v19

    .line 446
    const/16 v20, 0x1

    .line 447
    .line 448
    iget-object v4, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 449
    .line 450
    const/16 v22, 0x1

    .line 451
    .line 452
    const/16 v23, 0x0

    .line 453
    .line 454
    move-object/from16 v21, v4

    .line 455
    .line 456
    invoke-static/range {v18 .. v23}, Lcom/intsig/camscanner/util/Util;->Ooo(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;ZI)Ljava/lang/String;

    .line 457
    .line 458
    .line 459
    move-result-object v4

    .line 460
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O00()Ljava/lang/String;

    .line 461
    .line 462
    .line 463
    move-result-object v5

    .line 464
    const/4 v14, 0x1

    .line 465
    invoke-static {v4, v5, v14}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 466
    .line 467
    .line 468
    move-result v5

    .line 469
    if-nez v5, :cond_d

    .line 470
    .line 471
    const-string v5, "title"

    .line 472
    .line 473
    invoke-virtual {v10, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    .line 475
    .line 476
    const-string v5, "title_sort_index"

    .line 477
    .line 478
    invoke-static {v4}, Lcom/intsig/nativelib/PinyinUtil;->getPinyinOf(Ljava/lang/String;)Ljava/lang/String;

    .line 479
    .line 480
    .line 481
    move-result-object v4

    .line 482
    invoke-virtual {v10, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    .line 484
    .line 485
    :cond_d
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 486
    .line 487
    .line 488
    move-result-object v4

    .line 489
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 490
    .line 491
    .line 492
    move-result-object v4

    .line 493
    invoke-virtual {v4, v9, v10, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 494
    .line 495
    .line 496
    move-result v4

    .line 497
    if-lez v4, :cond_e

    .line 498
    .line 499
    if-eqz v13, :cond_e

    .line 500
    .line 501
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 502
    .line 503
    .line 504
    move-result-object v4

    .line 505
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 506
    .line 507
    .line 508
    move-result-object v5

    .line 509
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 510
    .line 511
    .line 512
    move-result-object v8

    .line 513
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 514
    .line 515
    .line 516
    move-result v9

    .line 517
    invoke-static {v4, v5, v8, v9}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->O〇8O8〇008(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 518
    .line 519
    .line 520
    :cond_e
    const-wide/16 v4, 0x1

    .line 521
    .line 522
    const/4 v9, 0x0

    .line 523
    const/4 v10, 0x1

    .line 524
    goto/16 :goto_6

    .line 525
    .line 526
    :cond_f
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 527
    .line 528
    .line 529
    move-result-object v1

    .line 530
    iget-object v2, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 531
    .line 532
    invoke-static {v1, v2, v11, v12}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 533
    .line 534
    .line 535
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 536
    .line 537
    .line 538
    move-result-object v1

    .line 539
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 540
    .line 541
    .line 542
    move-result v2

    .line 543
    if-eqz v2, :cond_10

    .line 544
    .line 545
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 546
    .line 547
    .line 548
    move-result-object v2

    .line 549
    check-cast v2, Ljava/lang/String;

    .line 550
    .line 551
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 552
    .line 553
    .line 554
    move-result-object v3

    .line 555
    invoke-static {v3, v2, v11, v12}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 556
    .line 557
    .line 558
    goto :goto_b

    .line 559
    :cond_10
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 560
    .line 561
    .line 562
    move-result-object v1

    .line 563
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 564
    .line 565
    .line 566
    return-void
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇〇8O0〇8()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 16
    .line 17
    check-cast v2, Ljava/lang/Iterable;

    .line 18
    .line 19
    new-instance v3, Ljava/util/ArrayList;

    .line 20
    .line 21
    const/16 v4, 0xa

    .line 22
    .line 23
    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 24
    .line 25
    .line 26
    move-result v4

    .line 27
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 28
    .line 29
    .line 30
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    if-eqz v4, :cond_1

    .line 39
    .line 40
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v4

    .line 44
    check-cast v4, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 45
    .line 46
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    invoke-static {v0, v3}, Lcom/intsig/camscanner/morc/util/MoveDirLayerUtil;->〇080(Landroid/content/Context;Ljava/util/List;)Ljava/util/ArrayList;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->o〇0:Ljava/lang/String;

    .line 61
    .line 62
    invoke-static {v2, v3}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    const/4 v4, 0x1

    .line 71
    if-ne v3, v4, :cond_5

    .line 72
    .line 73
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v3

    .line 77
    if-eqz v3, :cond_3

    .line 78
    .line 79
    invoke-static {v3}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    if-eqz v3, :cond_2

    .line 84
    .line 85
    goto :goto_1

    .line 86
    :cond_2
    const/4 v3, 0x0

    .line 87
    goto :goto_2

    .line 88
    :cond_3
    :goto_1
    const/4 v3, 0x1

    .line 89
    :goto_2
    if-nez v3, :cond_5

    .line 90
    .line 91
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v2

    .line 95
    invoke-static {v2}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇〇8O0〇8(Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/SharedDirEntryData;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    if-eqz v2, :cond_4

    .line 100
    .line 101
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/data/SharedDirEntryData;->getDir_layer()Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    if-eqz v2, :cond_4

    .line 106
    .line 107
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    goto :goto_3

    .line 112
    :cond_4
    const/4 v2, 0x0

    .line 113
    goto :goto_3

    .line 114
    :cond_5
    iget v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->oO80:I

    .line 115
    .line 116
    if-le v2, v4, :cond_6

    .line 117
    .line 118
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇O〇()I

    .line 119
    .line 120
    .line 121
    move-result v2

    .line 122
    goto :goto_3

    .line 123
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0O〇8o0O(Landroid/content/Context;)I

    .line 128
    .line 129
    .line 130
    move-result v2

    .line 131
    :goto_3
    const-string v3, "layerList"

    .line 132
    .line 133
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    :cond_7
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 141
    .line 142
    .line 143
    move-result v3

    .line 144
    if-eqz v3, :cond_8

    .line 145
    .line 146
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v3

    .line 150
    check-cast v3, Landroid/util/Pair;

    .line 151
    .line 152
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 153
    .line 154
    check-cast v3, Ljava/lang/Number;

    .line 155
    .line 156
    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    .line 157
    .line 158
    .line 159
    move-result v3

    .line 160
    iget v5, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o〇:I

    .line 161
    .line 162
    add-int/2addr v3, v5

    .line 163
    if-lt v3, v2, :cond_7

    .line 164
    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    const-string v5, "bothLayer = "

    .line 171
    .line 172
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 176
    .line 177
    .line 178
    const-string v3, " dirLayerUpperNum = "

    .line 179
    .line 180
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object v1

    .line 190
    const-string v3, "OtherMoveInAction"

    .line 191
    .line 192
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    const/4 v1, 0x1

    .line 196
    goto :goto_4

    .line 197
    :cond_8
    return v1
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method


# virtual methods
.method public O8(Lcom/intsig/camscanner/datastruct/FolderItem;)Z
    .locals 1
    .param p1    # Lcom/intsig/camscanner/datastruct/FolderItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "folderItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇〇0〇()Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 22
    :goto_1
    return p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public Oo08()V
    .locals 8

    .line 1
    const-string v0, "CSDirectory"

    .line 2
    .line 3
    const-string v1, "move_folder"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8O()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇8o8o〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    move-object v0, v1

    .line 27
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 28
    .line 29
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/FolderItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 34
    .line 35
    check-cast v3, Ljava/util/Collection;

    .line 36
    .line 37
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    xor-int/lit8 v3, v3, 0x1

    .line 42
    .line 43
    if-eqz v3, :cond_2

    .line 44
    .line 45
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-nez v0, :cond_2

    .line 50
    .line 51
    const-string v0, "OtherMoveInAction"

    .line 52
    .line 53
    const-string v1, "move between share dir and normal dir, copy and delete"

    .line 54
    .line 55
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 59
    .line 60
    check-cast v0, Ljava/lang/Iterable;

    .line 61
    .line 62
    new-instance v1, Ljava/util/ArrayList;

    .line 63
    .line 64
    const/16 v2, 0xa

    .line 65
    .line 66
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 67
    .line 68
    .line 69
    move-result v2

    .line 70
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 71
    .line 72
    .line 73
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    if-eqz v2, :cond_1

    .line 82
    .line 83
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 90
    .line 91
    .line 92
    move-result-wide v2

    .line 93
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 102
    .line 103
    new-instance v2, Ljava/util/ArrayList;

    .line 104
    .line 105
    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 106
    .line 107
    .line 108
    new-instance v1, LO8o〇O0/Oooo8o0〇;

    .line 109
    .line 110
    invoke-direct {v1, p0}, LO8o〇O0/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;)V

    .line 111
    .line 112
    .line 113
    invoke-static {v0, v2, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 114
    .line 115
    .line 116
    return-void

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 118
    .line 119
    const v2, 0x7f130044

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    const-string v3, "mActivity.getString(R.st\u2026ng.a_document_msg_moving)"

    .line 127
    .line 128
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->O8〇o0〇〇8(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 135
    .line 136
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 137
    .line 138
    .line 139
    move-result-object v2

    .line 140
    const/4 v3, 0x0

    .line 141
    const/4 v4, 0x0

    .line 142
    new-instance v5, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$executeAction$2;

    .line 143
    .line 144
    invoke-direct {v5, p0, v1}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction$executeAction$2;-><init>(Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;Lkotlin/coroutines/Continuation;)V

    .line 145
    .line 146
    .line 147
    const/4 v6, 0x3

    .line 148
    const/4 v7, 0x0

    .line 149
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 150
    .line 151
    .line 152
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final OoO8(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getCommonParams()Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 2
    .line 3
    const v1, 0x7f131db6

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "mActivity.getString(R.string.menu_title_cut)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(Ljava/lang/String;)Lcom/intsig/camscanner/morc/entity/SelectionItem;
    .locals 16
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/morc/entity/SelectionItem;

    .line 4
    .line 5
    invoke-direct {v1}, Lcom/intsig/camscanner/morc/entity/SelectionItem;-><init>()V

    .line 6
    .line 7
    .line 8
    new-instance v2, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    const/4 v4, 0x0

    .line 15
    if-eqz p1, :cond_1

    .line 16
    .line 17
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    .line 18
    .line 19
    .line 20
    move-result v5

    .line 21
    if-nez v5, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v5, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    :goto_0
    const/4 v5, 0x1

    .line 27
    :goto_1
    const/4 v6, 0x5

    .line 28
    const-string v7, "1"

    .line 29
    .line 30
    const-string v9, "dir_mycard"

    .line 31
    .line 32
    const-string v11, "5"

    .line 33
    .line 34
    const-string v13, "2"

    .line 35
    .line 36
    const-string v14, "folder_type != ? AND "

    .line 37
    .line 38
    const-string v15, "sync_dir_id != ? AND "

    .line 39
    .line 40
    const-string v8, "_id != ? AND "

    .line 41
    .line 42
    const-string v10, "team_token IS NULL AND "

    .line 43
    .line 44
    const-string v12, "sync_state != ? AND "

    .line 45
    .line 46
    if-eqz v5, :cond_2

    .line 47
    .line 48
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v5, "parent_sync_id IS NULL"

    .line 67
    .line 68
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    new-array v5, v6, [Ljava/lang/String;

    .line 72
    .line 73
    iget-wide v14, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇〇888:J

    .line 74
    .line 75
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    aput-object v6, v5, v4

    .line 80
    .line 81
    aput-object v13, v5, v3

    .line 82
    .line 83
    const/4 v3, 0x2

    .line 84
    aput-object v11, v5, v3

    .line 85
    .line 86
    const/4 v3, 0x3

    .line 87
    aput-object v9, v5, v3

    .line 88
    .line 89
    const/4 v3, 0x4

    .line 90
    aput-object v7, v5, v3

    .line 91
    .line 92
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 93
    .line 94
    .line 95
    move-result-object v3

    .line 96
    goto :goto_2

    .line 97
    :cond_2
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    const-string v5, "parent_sync_id=?"

    .line 116
    .line 117
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const/4 v5, 0x6

    .line 121
    new-array v5, v5, [Ljava/lang/String;

    .line 122
    .line 123
    iget-wide v14, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇〇888:J

    .line 124
    .line 125
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v8

    .line 129
    aput-object v8, v5, v4

    .line 130
    .line 131
    aput-object v13, v5, v3

    .line 132
    .line 133
    const/4 v3, 0x2

    .line 134
    aput-object v11, v5, v3

    .line 135
    .line 136
    const/4 v3, 0x3

    .line 137
    aput-object v9, v5, v3

    .line 138
    .line 139
    const/4 v3, 0x4

    .line 140
    aput-object v7, v5, v3

    .line 141
    .line 142
    aput-object p1, v5, v6

    .line 143
    .line 144
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 145
    .line 146
    .line 147
    move-result-object v3

    .line 148
    :goto_2
    sget-object v5, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 149
    .line 150
    iget-object v6, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 151
    .line 152
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O8ooOoo〇(Ljava/util/List;)Z

    .line 153
    .line 154
    .line 155
    move-result v5

    .line 156
    if-eqz v5, :cond_3

    .line 157
    .line 158
    sget-object v5, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 159
    .line 160
    invoke-virtual {v5}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O00()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v5

    .line 164
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    goto :goto_3

    .line 168
    :cond_3
    iget-object v5, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 169
    .line 170
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/FolderItem;->oO()Z

    .line 171
    .line 172
    .line 173
    move-result v5

    .line 174
    if-eqz v5, :cond_4

    .line 175
    .line 176
    const-string v5, " AND share_id IS NULL "

    .line 177
    .line 178
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    :cond_4
    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 182
    .line 183
    .line 184
    move-result-object v2

    .line 185
    iput-object v2, v1, Lcom/intsig/camscanner/morc/entity/SelectionItem;->〇080:Ljava/lang/String;

    .line 186
    .line 187
    check-cast v3, Ljava/util/Collection;

    .line 188
    .line 189
    new-array v2, v4, [Ljava/lang/String;

    .line 190
    .line 191
    invoke-interface {v3, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 192
    .line 193
    .line 194
    move-result-object v2

    .line 195
    check-cast v2, [Ljava/lang/String;

    .line 196
    .line 197
    iput-object v2, v1, Lcom/intsig/camscanner/morc/entity/SelectionItem;->〇o00〇〇Oo:[Ljava/lang/String;

    .line 198
    .line 199
    return-object v1
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇080(Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/movecopyactivity/MoveCopyParams;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇0〇O0088o(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O00()Lcom/intsig/camscanner/datastruct/FolderItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇o00〇〇Oo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇80〇808〇O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/List;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 8
    .line 9
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    add-int/2addr v0, v1

    .line 14
    if-gtz v0, :cond_0

    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    return v0
    .line 20
    .line 21
.end method

.method public 〇o〇()Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/movecopyactivity/action/IAction$DefaultImpls;->〇080(Lcom/intsig/camscanner/movecopyactivity/action/IAction;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->oO80:I

    .line 2
    .line 3
    const/16 v1, 0x69

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 8
    .line 9
    const v1, 0x7f13105d

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->〇080:Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 18
    .line 19
    const v1, 0x7f131db6

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :goto_0
    const-string v1, "if (targetFolderScenario\u2026menu_title_cut)\n        }"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->O8:Ljava/util/List;

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;->Oo08:Ljava/util/List;

    .line 38
    .line 39
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    add-int/2addr v1, v2

    .line 44
    if-lez v1, :cond_1

    .line 45
    .line 46
    new-instance v2, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v0, "("

    .line 55
    .line 56
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v0, ")"

    .line 63
    .line 64
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    :cond_1
    return-object v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

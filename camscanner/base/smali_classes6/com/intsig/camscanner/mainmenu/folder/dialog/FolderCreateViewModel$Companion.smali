.class public final Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;
.super Ljava/lang/Object;
.source "FolderCreateViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;-><init>()V

    return-void
.end method

.method private final O8()Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x2

    .line 7
    const/4 v3, 0x1

    .line 8
    if-ne v0, v3, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x3

    .line 11
    new-array v0, v0, [Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 12
    .line 13
    new-instance v10, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 14
    .line 15
    const/4 v5, 0x0

    .line 16
    const/16 v6, 0x69

    .line 17
    .line 18
    const/4 v7, 0x0

    .line 19
    const/4 v8, 0x5

    .line 20
    const/4 v9, 0x0

    .line 21
    move-object v4, v10

    .line 22
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 23
    .line 24
    .line 25
    aput-object v10, v0, v1

    .line 26
    .line 27
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 28
    .line 29
    const/4 v12, 0x0

    .line 30
    const/16 v13, 0x66

    .line 31
    .line 32
    const/4 v14, 0x0

    .line 33
    const/4 v15, 0x5

    .line 34
    const/16 v16, 0x0

    .line 35
    .line 36
    move-object v11, v1

    .line 37
    invoke-direct/range {v11 .. v16}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 38
    .line 39
    .line 40
    aput-object v1, v0, v3

    .line 41
    .line 42
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 43
    .line 44
    const/16 v6, 0x6a

    .line 45
    .line 46
    move-object v4, v1

    .line 47
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 48
    .line 49
    .line 50
    aput-object v1, v0, v2

    .line 51
    .line 52
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    goto :goto_0

    .line 57
    :cond_0
    new-array v0, v2, [Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 58
    .line 59
    new-instance v2, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 60
    .line 61
    const/4 v5, 0x0

    .line 62
    const/16 v6, 0x69

    .line 63
    .line 64
    const/4 v7, 0x0

    .line 65
    const/4 v8, 0x5

    .line 66
    const/4 v9, 0x0

    .line 67
    move-object v4, v2

    .line 68
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 69
    .line 70
    .line 71
    aput-object v2, v0, v1

    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 74
    .line 75
    const/4 v11, 0x0

    .line 76
    const/16 v12, 0x66

    .line 77
    .line 78
    const/4 v13, 0x0

    .line 79
    const/4 v14, 0x5

    .line 80
    const/4 v15, 0x0

    .line 81
    move-object v10, v1

    .line 82
    invoke-direct/range {v10 .. v15}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 83
    .line 84
    .line 85
    aput-object v1, v0, v3

    .line 86
    .line 87
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    :goto_0
    return-object v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o〇0(Ljava/util/List;Ljava/lang/String;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p2

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->length()I

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    if-nez v3, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v3, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 17
    :goto_1
    const-string v4, "FolderCreateViewModel"

    .line 18
    .line 19
    if-eqz v3, :cond_2

    .line 20
    .line 21
    new-instance v1, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v2, "loadTemplateData serverListString="

    .line 27
    .line 28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_2
    :try_start_0
    new-instance v3, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion$loadTemplateData$templateFolderData$1;

    .line 43
    .line 44
    invoke-direct {v3}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion$loadTemplateData$templateFolderData$1;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v3}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-static {v0, v3}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    check-cast v0, Ljava/util/List;

    .line 56
    .line 57
    move-object v3, v0

    .line 58
    check-cast v3, Ljava/util/Collection;

    .line 59
    .line 60
    if-eqz v3, :cond_3

    .line 61
    .line 62
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-eqz v3, :cond_4

    .line 67
    .line 68
    :cond_3
    const/4 v1, 0x1

    .line 69
    :cond_4
    if-eqz v1, :cond_5

    .line 70
    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    .line 72
    .line 73
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .line 75
    .line 76
    const-string v2, "loadTemplateData olderData="

    .line 77
    .line 78
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    return-void

    .line 92
    :cond_5
    const-string v1, "reLoadTemplateData isNullOrEmpty"

    .line 93
    .line 94
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    if-eqz v1, :cond_7

    .line 106
    .line 107
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    move-object v8, v1

    .line 112
    check-cast v8, Lcom/intsig/camscanner/scenariodir/data/TemplateFolderData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 113
    .line 114
    :try_start_1
    invoke-virtual {v8}, Lcom/intsig/camscanner/scenariodir/data/TemplateFolderData;->getDir_type()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    if-eqz v1, :cond_6

    .line 119
    .line 120
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 121
    .line 122
    .line 123
    move-result v7

    .line 124
    packed-switch v7, :pswitch_data_0

    .line 125
    .line 126
    .line 127
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 128
    .line 129
    goto :goto_3

    .line 130
    :pswitch_0
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 131
    .line 132
    const/4 v10, 0x0

    .line 133
    const/16 v11, 0x6a

    .line 134
    .line 135
    const/4 v12, 0x0

    .line 136
    const/4 v13, 0x5

    .line 137
    const/4 v14, 0x0

    .line 138
    move-object v9, v1

    .line 139
    invoke-direct/range {v9 .. v14}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 140
    .line 141
    .line 142
    goto :goto_4

    .line 143
    :pswitch_1
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 144
    .line 145
    const/16 v16, 0x0

    .line 146
    .line 147
    const/16 v17, 0x69

    .line 148
    .line 149
    const/16 v18, 0x0

    .line 150
    .line 151
    const/16 v19, 0x5

    .line 152
    .line 153
    const/16 v20, 0x0

    .line 154
    .line 155
    move-object v15, v1

    .line 156
    invoke-direct/range {v15 .. v20}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 157
    .line 158
    .line 159
    goto :goto_4

    .line 160
    :pswitch_2
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 161
    .line 162
    const/4 v6, 0x0

    .line 163
    const/16 v7, 0x68

    .line 164
    .line 165
    const/4 v8, 0x0

    .line 166
    const/4 v9, 0x5

    .line 167
    const/4 v10, 0x0

    .line 168
    move-object v5, v1

    .line 169
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 170
    .line 171
    .line 172
    goto :goto_4

    .line 173
    :pswitch_3
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 174
    .line 175
    const/4 v12, 0x0

    .line 176
    const/16 v13, 0x67

    .line 177
    .line 178
    const/4 v14, 0x0

    .line 179
    const/4 v15, 0x5

    .line 180
    const/16 v16, 0x0

    .line 181
    .line 182
    move-object v11, v1

    .line 183
    invoke-direct/range {v11 .. v16}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 184
    .line 185
    .line 186
    goto :goto_4

    .line 187
    :pswitch_4
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 188
    .line 189
    const/4 v6, 0x0

    .line 190
    const/16 v7, 0x66

    .line 191
    .line 192
    const/4 v8, 0x0

    .line 193
    const/4 v9, 0x5

    .line 194
    const/4 v10, 0x0

    .line 195
    move-object v5, v1

    .line 196
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 197
    .line 198
    .line 199
    goto :goto_4

    .line 200
    :pswitch_5
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 201
    .line 202
    const/4 v12, 0x0

    .line 203
    const/16 v13, 0x65

    .line 204
    .line 205
    const/4 v14, 0x0

    .line 206
    const/4 v15, 0x5

    .line 207
    const/16 v16, 0x0

    .line 208
    .line 209
    move-object v11, v1

    .line 210
    invoke-direct/range {v11 .. v16}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 211
    .line 212
    .line 213
    goto :goto_4

    .line 214
    :goto_3
    const/4 v6, 0x0

    .line 215
    const/4 v9, 0x1

    .line 216
    const/4 v10, 0x0

    .line 217
    move-object v5, v1

    .line 218
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 219
    .line 220
    .line 221
    :goto_4
    move-object/from16 v2, p1

    .line 222
    .line 223
    :try_start_2
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 224
    .line 225
    .line 226
    goto :goto_2

    .line 227
    :cond_6
    move-object/from16 v2, p1

    .line 228
    .line 229
    goto/16 :goto_2

    .line 230
    .line 231
    :catch_0
    move-object/from16 v2, p1

    .line 232
    .line 233
    :catch_1
    :try_start_3
    const-string v1, "loadTemplateData - NumberFormatException FOUND!"

    .line 234
    .line 235
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 236
    .line 237
    .line 238
    goto/16 :goto_2

    .line 239
    .line 240
    :catch_2
    move-exception v0

    .line 241
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    .line 246
    .line 247
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .line 249
    .line 250
    const-string v2, "parseTemplateDirData error :"

    .line 251
    .line 252
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    .line 254
    .line 255
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    .line 257
    .line 258
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 259
    .line 260
    .line 261
    move-result-object v0

    .line 262
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    .line 264
    .line 265
    :cond_7
    return-void

    .line 266
    nop

    .line 267
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇080()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇80〇808〇O()Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    const/16 v3, 0x65

    .line 17
    .line 18
    const/4 v4, 0x0

    .line 19
    const/4 v5, 0x5

    .line 20
    const/4 v6, 0x0

    .line 21
    move-object v1, v0

    .line 22
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v0, 0x2

    .line 31
    new-array v0, v0, [Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 32
    .line 33
    new-instance v7, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    const/16 v3, 0x65

    .line 37
    .line 38
    const/4 v4, 0x0

    .line 39
    const/4 v5, 0x5

    .line 40
    const/4 v6, 0x0

    .line 41
    move-object v1, v7

    .line 42
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 43
    .line 44
    .line 45
    const/4 v1, 0x0

    .line 46
    aput-object v7, v0, v1

    .line 47
    .line 48
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 49
    .line 50
    const/4 v9, 0x0

    .line 51
    const/16 v10, 0x68

    .line 52
    .line 53
    const/4 v11, 0x0

    .line 54
    const/4 v12, 0x5

    .line 55
    const/4 v13, 0x0

    .line 56
    move-object v8, v1

    .line 57
    invoke-direct/range {v8 .. v13}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 58
    .line 59
    .line 60
    const/4 v2, 0x1

    .line 61
    aput-object v1, v0, v2

    .line 62
    .line 63
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    :goto_0
    return-object v0
    .line 68
.end method

.method private final 〇o00〇〇Oo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇80〇808〇O()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o〇()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇80〇808〇O()Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇80〇808〇O()Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    const/16 v3, 0x67

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    const/4 v5, 0x5

    .line 25
    const/4 v6, 0x0

    .line 26
    move-object v1, v0

    .line 27
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;-><init>(Ljava/lang/String;ILcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    :goto_0
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final Oo08(Ljava/util/List;Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 1
    const-string v0, "list"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 7
    .line 8
    .line 9
    if-eqz p2, :cond_0

    .line 10
    .line 11
    :try_start_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;

    .line 12
    .line 13
    new-instance v1, Lorg/json/JSONObject;

    .line 14
    .line 15
    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string p2, "dir_list"

    .line 19
    .line 20
    invoke-virtual {v1, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->o〇0(Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :catchall_0
    move-exception p2

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v1, "initOneList ERROR! category="

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v1, ", t="

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    const-string v0, "FolderCreateViewModel"

    .line 55
    .line 56
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 60
    .line 61
    .line 62
    move-result p2

    .line 63
    if-eqz p2, :cond_5

    .line 64
    .line 65
    const/4 p2, 0x1

    .line 66
    if-eq p3, p2, :cond_4

    .line 67
    .line 68
    const/4 p2, 0x2

    .line 69
    if-eq p3, p2, :cond_3

    .line 70
    .line 71
    const/4 p2, 0x3

    .line 72
    if-eq p3, p2, :cond_2

    .line 73
    .line 74
    const/4 p2, 0x4

    .line 75
    if-eq p3, p2, :cond_1

    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->〇o〇()Ljava/util/List;

    .line 78
    .line 79
    .line 80
    move-result-object p2

    .line 81
    goto :goto_1

    .line 82
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->〇080()Ljava/util/List;

    .line 83
    .line 84
    .line 85
    move-result-object p2

    .line 86
    goto :goto_1

    .line 87
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->O8()Ljava/util/List;

    .line 88
    .line 89
    .line 90
    move-result-object p2

    .line 91
    goto :goto_1

    .line 92
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->〇o〇()Ljava/util/List;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    goto :goto_1

    .line 97
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->〇o00〇〇Oo()Ljava/util/List;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    :goto_1
    check-cast p2, Ljava/util/Collection;

    .line 102
    .line 103
    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 104
    .line 105
    .line 106
    :cond_5
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

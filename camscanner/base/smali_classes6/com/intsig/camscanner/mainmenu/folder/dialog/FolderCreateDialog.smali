.class public final Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "FolderCreateDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

.field private OO:Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;

.field private o0:Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;

.field private o〇00O:I

.field private 〇08O〇00〇o:Lcom/intsig/callback/CsCommonCallback2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/callback/CsCommonCallback2<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/camscanner/scenariodir/data/TemplateFolderData;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o880(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOo〇08〇(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;)Lcom/intsig/callback/CsCommonCallback2;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇08O〇00〇o:Lcom/intsig/callback/CsCommonCallback2;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oO〇oo()V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;->〇080:Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;->〇o00〇〇Oo()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Iterable;

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    move-object v7, v1

    .line 24
    check-cast v7, Lcom/intsig/camscanner/scenariodir/data/TemplateFolderData;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 27
    .line 28
    new-instance v10, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 29
    .line 30
    const/4 v3, 0x0

    .line 31
    const/16 v4, 0xc9

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    const/4 v6, 0x0

    .line 35
    const/16 v8, 0xd

    .line 36
    .line 37
    const/4 v9, 0x0

    .line 38
    move-object v2, v10

    .line 39
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oooO888()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->o0:Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 6
    .line 7
    new-instance v2, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-direct {v2, v3}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 17
    .line 18
    .line 19
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;

    .line 20
    .line 21
    invoke-direct {v1}, Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->OO:Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;

    .line 25
    .line 26
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 27
    .line 28
    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->OO:Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;

    .line 32
    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->OO:Lcom/intsig/camscanner/mainmenu/folder/adapter/FolderCreateAdapter;

    .line 41
    .line 42
    if-eqz v1, :cond_1

    .line 43
    .line 44
    new-instance v2, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$initView$1$1;

    .line 45
    .line 46
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog$initView$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 50
    .line 51
    .line 52
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 53
    .line 54
    new-instance v1, Lcom/intsig/camscanner/mainmenu/folder/dialog/〇080;

    .line 55
    .line 56
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    .line 61
    .line 62
    :cond_2
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->o880(Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8oOo0()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 7
    .line 8
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 9
    .line 10
    const v1, 0x7f1310da

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    const-string v1, "getString(R.string.cs_620_folder_nom)"

    .line 18
    .line 19
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x0

    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    const/16 v7, 0x1e

    .line 27
    .line 28
    const/4 v8, 0x0

    .line 29
    move-object v1, v9

    .line 30
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShareDirOpen()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const/4 v1, 0x1

    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 48
    .line 49
    const/4 v2, 0x0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;->〇080()Z

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    if-ne v0, v1, :cond_0

    .line 57
    .line 58
    const/4 v2, 0x1

    .line 59
    :cond_0
    if-nez v2, :cond_1

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 62
    .line 63
    new-instance v10, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 64
    .line 65
    const v2, 0x7f131006

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    const-string v2, "getString(R.string.cs_617_share61)"

    .line 73
    .line 74
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const/4 v4, 0x1

    .line 78
    const/4 v5, 0x0

    .line 79
    const/4 v6, 0x0

    .line 80
    const/4 v7, 0x0

    .line 81
    const/16 v8, 0x1c

    .line 82
    .line 83
    const/4 v9, 0x0

    .line 84
    move-object v2, v10

    .line 85
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    if-eqz v0, :cond_3

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 98
    .line 99
    new-instance v10, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 100
    .line 101
    const v2, 0x7f131036

    .line 102
    .line 103
    .line 104
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v3

    .line 108
    const-string v2, "getString(R.string.cs_618_folder_title01)"

    .line 109
    .line 110
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    const/4 v4, 0x0

    .line 114
    const/4 v5, 0x0

    .line 115
    const/4 v6, 0x0

    .line 116
    const/4 v7, 0x0

    .line 117
    const/16 v8, 0x1a

    .line 118
    .line 119
    const/4 v9, 0x0

    .line 120
    move-object v2, v10

    .line 121
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->oO〇oo()V

    .line 128
    .line 129
    .line 130
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->o〇00O:I

    .line 131
    .line 132
    if-ne v0, v1, :cond_2

    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 135
    .line 136
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 137
    .line 138
    const/4 v2, 0x0

    .line 139
    const/16 v3, 0x66

    .line 140
    .line 141
    const/4 v4, 0x0

    .line 142
    const/4 v5, 0x0

    .line 143
    const/4 v6, 0x0

    .line 144
    const/16 v7, 0x1d

    .line 145
    .line 146
    const/4 v8, 0x0

    .line 147
    move-object v1, v9

    .line 148
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    .line 153
    .line 154
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 155
    .line 156
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 157
    .line 158
    const/16 v3, 0x69

    .line 159
    .line 160
    move-object v1, v9

    .line 161
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 162
    .line 163
    .line 164
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    .line 166
    .line 167
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 168
    .line 169
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 170
    .line 171
    const/16 v3, 0x65

    .line 172
    .line 173
    move-object v1, v9

    .line 174
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 181
    .line 182
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 183
    .line 184
    const/16 v3, 0x6a

    .line 185
    .line 186
    move-object v1, v9

    .line 187
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    goto :goto_0

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 195
    .line 196
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 197
    .line 198
    const/4 v2, 0x0

    .line 199
    const/16 v3, 0x66

    .line 200
    .line 201
    const/4 v4, 0x0

    .line 202
    const/4 v5, 0x0

    .line 203
    const/4 v6, 0x0

    .line 204
    const/16 v7, 0x1d

    .line 205
    .line 206
    const/4 v8, 0x0

    .line 207
    move-object v1, v9

    .line 208
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 215
    .line 216
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 217
    .line 218
    const/16 v3, 0x69

    .line 219
    .line 220
    move-object v1, v9

    .line 221
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    .line 226
    .line 227
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 228
    .line 229
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 230
    .line 231
    const/16 v3, 0x65

    .line 232
    .line 233
    move-object v1, v9

    .line 234
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 235
    .line 236
    .line 237
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    .line 239
    .line 240
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 241
    .line 242
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 243
    .line 244
    const/16 v3, 0x68

    .line 245
    .line 246
    move-object v1, v9

    .line 247
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    .line 252
    .line 253
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 254
    .line 255
    new-instance v9, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;

    .line 256
    .line 257
    const/16 v3, 0x67

    .line 258
    .line 259
    move-object v1, v9

    .line 260
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mainmenu/folder/data/FolderItemData;-><init>(Ljava/lang/String;IIZLcom/intsig/camscanner/scenariodir/data/TemplateFolderData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 261
    .line 262
    .line 263
    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    .line 265
    .line 266
    :cond_3
    :goto_0
    return-void
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method


# virtual methods
.method public final o00〇88〇08(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/intsig/callback/CsCommonCallback2;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/callback/CsCommonCallback2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentManager;",
            "Ljava/lang/String;",
            "Lcom/intsig/callback/CsCommonCallback2<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/camscanner/scenariodir/data/TemplateFolderData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "manager"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "callback"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    :try_start_0
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇08O〇00〇o:Lcom/intsig/callback/CsCommonCallback2;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const-string p3, "manager.beginTransaction()"

    .line 18
    .line 19
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, p0, p2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :catch_0
    move-exception p1

    .line 30
    const-string p2, "FolderCreateDialog"

    .line 31
    .line 32
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const p1, 0x7f140192

    .line 5
    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p0, v0, p1}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const-string v0, "key_folder_from"

    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    :cond_0
    iput v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->o〇00O:I

    .line 24
    .line 25
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    const-string v0, "extra_custom_folder_create_model_item"

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    check-cast p1, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d019f

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->o0:Lcom/intsig/camscanner/databinding/DialogCreateFolderBinding;

    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->〇O8oOo0()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateDialog;->oooO888()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

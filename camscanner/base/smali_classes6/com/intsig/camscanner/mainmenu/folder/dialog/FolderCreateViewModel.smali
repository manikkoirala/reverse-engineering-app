.class public final Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "FolderCreateViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇080OO8〇0:Ljava/lang/Boolean;


# instance fields
.field private final OO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

.field private final o〇00O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->customize_dir_entry:Lcom/intsig/tsapp/sync/AppConfigJson$ScenarioDirEnTry;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson$ScenarioDirEnTry;->isUsingOversea()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    :cond_0
    sput-object v1, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇080OO8〇0:Ljava/lang/Boolean;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    new-instance p1, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇OOo8〇0:Ljava/util/List;

    .line 15
    .line 16
    new-instance p1, Ljava/util/ArrayList;

    .line 17
    .line 18
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->OO:Ljava/util/List;

    .line 22
    .line 23
    new-instance p1, Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .line 27
    .line 28
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 29
    .line 30
    new-instance p1, Ljava/util/ArrayList;

    .line 31
    .line 32
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->o〇00O:Ljava/util/List;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇80〇808〇O()Ljava/lang/Boolean;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇080OO8〇0:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O8ooOoo〇(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "extra_custom_folder_create_model_item"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    check-cast p1, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->o0:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oooo8o0〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->o〇00O:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->OO:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->create_dir_cate:Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇OOo8〇0:Ljava/util/List;

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v4, v0, Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;->recommend:Ljava/lang/String;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v4, v3

    .line 18
    :goto_0
    const/4 v5, 0x1

    .line 19
    invoke-virtual {v1, v2, v4, v5}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->Oo08(Ljava/util/List;Ljava/lang/String;I)V

    .line 20
    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->OO:Ljava/util/List;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object v4, v0, Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;->learn:Ljava/lang/String;

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    move-object v4, v3

    .line 30
    :goto_1
    const/4 v5, 0x2

    .line 31
    invoke-virtual {v1, v2, v4, v5}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->Oo08(Ljava/util/List;Ljava/lang/String;I)V

    .line 32
    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 35
    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    iget-object v4, v0, Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;->work:Ljava/lang/String;

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_2
    move-object v4, v3

    .line 42
    :goto_2
    const/4 v5, 0x3

    .line 43
    invoke-virtual {v1, v2, v4, v5}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->Oo08(Ljava/util/List;Ljava/lang/String;I)V

    .line 44
    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->o〇00O:Ljava/util/List;

    .line 47
    .line 48
    if-eqz v0, :cond_3

    .line 49
    .line 50
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$CreateDirContent;->life:Ljava/lang/String;

    .line 51
    .line 52
    :cond_3
    const/4 v0, 0x4

    .line 53
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel$Companion;->Oo08(Ljava/util/List;Ljava/lang/String;I)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇8o8o〇()Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->o0:Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O00()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇OOo8〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/folder/data/FolderScenarioCreateItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/folder/dialog/FolderCreateViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "SeniorFolderMainGuideControl.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl;->〇8o8o〇(Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private 〇080:I

.field final synthetic 〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 2
    .line 3
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;->onScrollStateChanged(Landroidx/recyclerview/widget/RecyclerView;I)V

    .line 7
    .line 8
    .line 9
    if-nez p2, :cond_0

    .line 10
    .line 11
    iget p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇080:I

    .line 12
    .line 13
    const/16 p2, 0x1f4

    .line 14
    .line 15
    if-lt p1, p2, :cond_0

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 18
    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_0

    .line 30
    .line 31
    const-string p1, "SeniorFolderMainGuideControl"

    .line 32
    .line 33
    const-string p2, "hide layout on scrolling"

    .line 34
    .line 35
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const-string p1, "CSAdvancedFolderGuideBubble"

    .line 39
    .line 40
    const-string p2, "close"

    .line 41
    .line 42
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 46
    .line 47
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/LayoutSeniorFolderCertificateCardBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const-string p2, "binding.includeCertificateCard.root"

    .line 54
    .line 55
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const/4 p2, 0x0

    .line 59
    invoke-static {p1, p2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 63
    .line 64
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;->〇O〇〇O8:Landroidx/recyclerview/widget/RecyclerView;

    .line 65
    .line 66
    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 67
    .line 68
    .line 69
    :cond_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;->onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V

    .line 7
    .line 8
    .line 9
    iget p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇080:I

    .line 10
    .line 11
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    add-int/2addr p1, p2

    .line 16
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl$setLayoutVisibilityWhenScrolling$1;->〇080:I

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "SearchOpAdProvider.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/DocMultiEntity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docAdapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mContext"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->O8o08O8O:Landroid/content/Context;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V
    .locals 2
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/DocMultiEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p2, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 16
    .line 17
    instance-of v0, p1, Landroid/widget/FrameLayout;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const-string v0, "null cannot be cast to non-null type android.widget.FrameLayout"

    .line 22
    .line 23
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    check-cast p1, Landroid/widget/FrameLayout;

    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/ads/operation/SearchBarOperation;

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->O8o08O8O:Landroid/content/Context;

    .line 31
    .line 32
    check-cast p2, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;->Oo08()Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    invoke-direct {v0, v1, p2}, Lcom/intsig/camscanner/ads/operation/SearchBarOperation;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 39
    .line 40
    .line 41
    new-instance p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider$convert$1;

    .line 42
    .line 43
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider$convert$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/ads/operation/OperationAdAbs;->o〇0OOo〇0(Lcom/intsig/camscanner/ads/operation/SimpleAdEventListener;)V

    .line 47
    .line 48
    .line 49
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->O8o08O8O:Landroid/content/Context;

    .line 50
    .line 51
    invoke-virtual {v0, p2, p1}, Lcom/intsig/camscanner/ads/operation/OperationAdAbs;->OOO〇O0(Landroid/content/Context;Landroid/widget/FrameLayout;)Z

    .line 52
    .line 53
    .line 54
    :cond_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d04a9

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O()Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/DocMultiEntity;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0xf

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

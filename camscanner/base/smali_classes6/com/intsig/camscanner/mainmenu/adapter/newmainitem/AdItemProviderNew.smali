.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "AdItemProviderNew.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/DocMultiEntity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docAdapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mContext"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->〇80()Lcom/intsig/advertisement/adapters/positions/DocListManager;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 25
    .line 26
    .line 27
    move-result p2

    .line 28
    invoke-virtual {p1, p2}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->〇0(Z)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;

    .line 11
    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v0, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-direct {p2, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;-><init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;)V

    .line 26
    .line 27
    .line 28
    return-object p2
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V
    .locals 10
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/DocMultiEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, p1

    .line 12
    check-cast v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;

    .line 13
    .line 14
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 17
    .line 18
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 19
    .line 20
    const-string v3, "helper.itemView"

    .line 21
    .line 22
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v2, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->Oo08(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/view/View;)V

    .line 26
    .line 27
    .line 28
    check-cast p2, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 35
    .line 36
    .line 37
    move-result v6

    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;->〇00()Landroid/widget/RelativeLayout;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_0

    .line 49
    .line 50
    const/high16 p2, 0x41000000    # 8.0f

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/high16 p2, 0x40800000    # 4.0f

    .line 54
    .line 55
    :goto_0
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    if-eqz p1, :cond_1

    .line 60
    .line 61
    const/high16 p1, 0x42a00000    # 80.0f

    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_1
    const/high16 p1, 0x42e00000    # 112.0f

    .line 65
    .line 66
    :goto_1
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    iget-object p1, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 71
    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    if-eqz p1, :cond_2

    .line 77
    .line 78
    iput v5, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 79
    .line 80
    iget-object p1, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 81
    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getPaddingStart()I

    .line 83
    .line 84
    .line 85
    move-result v1

    .line 86
    iget-object v2, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 87
    .line 88
    invoke-virtual {v2}, Landroid/view/View;->getPaddingEnd()I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    invoke-virtual {p1, v1, p2, v2, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 93
    .line 94
    .line 95
    :cond_2
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->〇o〇()Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 100
    .line 101
    if-eqz p2, :cond_3

    .line 102
    .line 103
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;->O8ooOoo〇()Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object p2

    .line 107
    check-cast p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 108
    .line 109
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OoO〇()Z

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    xor-int/lit8 p1, p1, 0x1

    .line 114
    .line 115
    invoke-static {p2, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 116
    .line 117
    .line 118
    :cond_3
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->〇80()Lcom/intsig/advertisement/adapters/positions/DocListManager;

    .line 119
    .line 120
    .line 121
    move-result-object v1

    .line 122
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 123
    .line 124
    move-object v2, p1

    .line 125
    check-cast v2, Landroid/app/Activity;

    .line 126
    .line 127
    const/4 v4, -0x1

    .line 128
    const/4 v7, 0x1

    .line 129
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew$AdViewHolder;->O〇8O8〇008()Landroid/widget/ImageView;

    .line 130
    .line 131
    .line 132
    move-result-object v8

    .line 133
    const/4 v9, 0x0

    .line 134
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->O000(Landroid/app/Activity;Landroid/view/ViewGroup;IIIZLandroid/widget/ImageView;Lcom/intsig/advertisement/listener/OnFeedBackListener;)Z

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d043c

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/DocMultiEntity;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0xd

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "CloudDiskRecProvider.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/DocMultiEntity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:I

.field private final 〇0O:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mAdapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mContext"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->O8o08O8O:Landroid/content/Context;

    .line 17
    .line 18
    const/16 p1, 0x16

    .line 19
    .line 20
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇080OO8〇0:I

    .line 21
    .line 22
    const p1, 0x7f0d042c

    .line 23
    .line 24
    .line 25
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇0O:I

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇00(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->o〇O8〇〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSHome"

    .line 7
    .line 8
    const-string v0, "app_recommend"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance p1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    .line 14
    .line 15
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->O8o08O8O:Landroid/content/Context;

    .line 16
    .line 17
    move-object v2, p0

    .line 18
    check-cast v2, Landroidx/fragment/app/FragmentActivity;

    .line 19
    .line 20
    new-instance v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 21
    .line 22
    const/4 p0, 0x1

    .line 23
    const/4 v0, 0x0

    .line 24
    const/4 v1, 0x0

    .line 25
    const/16 v4, 0x25f

    .line 26
    .line 27
    invoke-direct {v3, v1, v4, p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    const/4 v4, 0x0

    .line 31
    const/4 v5, 0x4

    .line 32
    const/4 v6, 0x0

    .line 33
    move-object v1, p1

    .line 34
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    const/4 v3, 0x0

    .line 39
    const/4 v5, 0x7

    .line 40
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇oo〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;ZLcom/intsig/camscanner/newsign/CsImportUsage;ILjava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇00(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇〇808〇(Z)V

    .line 10
    .line 11
    .line 12
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-static {p0, p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇080O0()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const-string v0, "cs_main"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v0, "cs_home"

    .line 18
    .line 19
    :goto_0
    const-string v1, "CSOtherAppRecommend"

    .line 20
    .line 21
    const-string v2, "from_part"

    .line 22
    .line 23
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    return-object p1
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/DocMultiEntity;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇oo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇oo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/DocMultiEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of p2, p2, Lcom/intsig/camscanner/util/MainMenuTipsChecker$CloudDiskRecEntity;

    .line 12
    .line 13
    if-eqz p2, :cond_0

    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->O8o08O8O:Landroid/content/Context;

    .line 16
    .line 17
    instance-of p2, p2, Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    iget-object p2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 22
    .line 23
    const v0, 0x7f0a09fb

    .line 24
    .line 25
    .line 26
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    const-string v0, "helper.itemView.findView\u2026R.id.iv_rec_google_drive)"

    .line 31
    .line 32
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->O8()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 40
    .line 41
    .line 42
    iget-object p2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 43
    .line 44
    const v0, 0x7f0a09fa

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    const-string v0, "helper.itemView.findView\u2026ew>(R.id.iv_rec_drop_box)"

    .line 52
    .line 53
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇o00〇〇Oo()Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 61
    .line 62
    .line 63
    iget-object p2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 64
    .line 65
    const v0, 0x7f0a09fc

    .line 66
    .line 67
    .line 68
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    const-string v0, "helper.itemView.findView\u2026w>(R.id.iv_rec_one_drive)"

    .line 73
    .line 74
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->o〇0()Z

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 82
    .line 83
    .line 84
    iget-object p2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 85
    .line 86
    new-instance v0, LOOo/〇o00〇〇Oo;

    .line 87
    .line 88
    invoke-direct {v0, p0}, LOOo/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 95
    .line 96
    const p2, 0x7f0a09f9

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    check-cast p1, Landroid/widget/ImageView;

    .line 104
    .line 105
    new-instance p2, LOOo/〇o〇;

    .line 106
    .line 107
    invoke-direct {p2, p0}, LOOo/〇o〇;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;)V

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    .line 112
    .line 113
    :cond_0
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

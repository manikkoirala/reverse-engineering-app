.class public final Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;
.super Ljava/lang/Object;
.source "MainDocLayoutManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static O8:I

.field private static Oo08:I

.field private static o〇0:I

.field public static final 〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/FolderStackManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 8
    .line 9
    const/high16 v0, 0x42d00000    # 104.0f

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    sput v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->O8:I

    .line 16
    .line 17
    const/high16 v0, 0x41000000    # 8.0f

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    sput v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08:I

    .line 24
    .line 25
    const/high16 v0, 0x41800000    # 16.0f

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    sput v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0:I

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/FolderStackManager;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/FolderStackManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "folderStackManager"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇080:Landroid/content/Context;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-eqz p1, :cond_0

    .line 23
    .line 24
    const/high16 p1, 0x42c00000    # 96.0f

    .line 25
    .line 26
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    sput p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->O8:I

    .line 31
    .line 32
    const/high16 p1, 0x41000000    # 8.0f

    .line 33
    .line 34
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    sput p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08:I

    .line 39
    .line 40
    const/high16 p1, 0x41400000    # 12.0f

    .line 41
    .line 42
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    sput p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0:I

    .line 47
    .line 48
    :cond_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇080()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o00〇〇Oo()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇888()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/high16 v0, 0x42000000    # 32.0f

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O8(Landroid/content/Context;)I
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 15
    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->oO80(Landroid/content/Context;)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    sub-int/2addr v0, p1

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇〇888()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    sub-int/2addr v0, p1

    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final Oo08()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇080:Landroid/content/Context;

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->O8(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇080(Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80(Landroid/content/Context;)I
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO0o88()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, 0x0

    .line 17
    const/4 v3, 0x1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iget p1, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 34
    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇080:Landroid/content/Context;

    .line 38
    .line 39
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOo(Landroid/content/Context;)I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-ne v0, v3, :cond_1

    .line 44
    .line 45
    sget v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08:I

    .line 46
    .line 47
    sub-int/2addr p1, v0

    .line 48
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 49
    .line 50
    invoke-static {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇080(Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;I)I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    sget v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0:I

    .line 55
    .line 56
    mul-int/lit8 v2, v1, 0x2

    .line 57
    .line 58
    sub-int/2addr p1, v2

    .line 59
    add-int/lit8 v2, v0, -0x1

    .line 60
    .line 61
    sget v3, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08:I

    .line 62
    .line 63
    mul-int v2, v2, v3

    .line 64
    .line 65
    sub-int/2addr p1, v2

    .line 66
    div-int/2addr p1, v0

    .line 67
    add-int v2, p1, v1

    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_1
    div-int/lit8 p1, p1, 0x4

    .line 71
    .line 72
    sget v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0:I

    .line 73
    .line 74
    add-int v2, p1, v0

    .line 75
    .line 76
    :cond_2
    :goto_1
    return v2
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final o〇0()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO0o88()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const/4 v0, 0x3

    .line 21
    goto :goto_1

    .line 22
    :cond_1
    const/4 v0, 0x4

    .line 23
    :goto_1
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public final Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "MainDocAdapter.kt"

# interfaces
.implements Lcom/intsig/camscanner/adapter/UpdateNotCompleteDoc;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;,
        Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/DocMultiEntity;",
        ">;",
        "Lcom/intsig/camscanner/adapter/UpdateNotCompleteDoc;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0oO〇oo00:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8〇8000:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0〇0:Z

.field private final O8o〇O0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O8〇o〇88:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/LastItemPaddingProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇OOo:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

.field private final Oo0〇Ooo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private final Ooo8o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O〇08oOOO0:Lcom/intsig/camscanner/adapter/QueryInterface;

.field private final O〇O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o00〇88〇08:Z

.field private final o0OoOOo0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o880:Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

.field private final o8O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO:Z

.field private final oO00〇o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO0880O:I

.field private final oOO8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOoO8OO〇:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/DocMultiEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOoo80oO:Landroid/os/CountDownTimer;

.field private final oOo〇08〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇8O8oOo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇oo:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

.field private final ooO:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oooO888:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mDocItemProviderNewView$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0〇o:Z

.field private o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

.field private o〇o〇Oo88:Z

.field private final 〇00O0:Landroidx/fragment/app/Fragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇088O:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

.field private 〇0O〇O00O:Z

.field private 〇0ooOOo:Z

.field private 〇0〇0:I

.field private final 〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80O8o8O〇:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DataSecurityDescItemProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇80o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/webstorage/RemoteFile;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇OOoooo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8〇o88:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O8oOo0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OO8ooO8〇:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "Lcom/intsig/webstorage/RemoteFile;",
            "Lcom/intsig/webstorage/RemoteFile;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇OO〇00〇0O:Z

.field private 〇o08:Ljava/lang/Integer;

.field private final 〇oo〇O〇80:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocOpticalRecognizeProvider;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇88〇8:Lkotlinx/coroutines/Job;

.field private 〇〇O80〇0o:Ljava/lang/Long;

.field private 〇〇o0〇8:Z

.field private 〇〇〇0:Lkotlinx/coroutines/Job;

.field private final 〇〇〇00:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇〇0o〇〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamEntry;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0oO〇oo00:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "MainDocAdapter::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/adapter/QueryInterface;ZLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;Lcom/intsig/camscanner/datastruct/FolderItem;Lkotlin/jvm/functions/Function3;)V
    .locals 8
    .param p1    # Landroidx/fragment/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/Fragment;",
            "Lcom/intsig/camscanner/adapter/QueryInterface;",
            "Z",
            "Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lcom/intsig/webstorage/RemoteFile;",
            "-",
            "Lcom/intsig/webstorage/RemoteFile;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "mFragment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2
    invoke-direct {p0, v0, v1, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 5
    iput-boolean p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o8〇OO:Z

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 7
    iput-object p6, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇OO8ooO8〇:Lkotlin/jvm/functions/Function3;

    .line 8
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    const-string p3, "mFragment.requireActivity()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 9
    instance-of p3, p2, Lcom/intsig/camscanner/searchactivity/SearchActivity;

    iput-boolean p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇OO〇00〇0O:Z

    .line 10
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 11
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    .line 12
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 13
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 15
    new-instance v2, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mainDocRecManager$2;

    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mainDocRecManager$2;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    invoke-static {v2}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v2

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOo〇08〇:Lkotlin/Lazy;

    .line 16
    new-instance v2, Landroidx/lifecycle/MutableLiveData;

    invoke-direct {v2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8oOo0:Landroidx/lifecycle/MutableLiveData;

    .line 17
    new-instance v2, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mDocItemProviderNewView$1;

    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mDocItemProviderNewView$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oooO888:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mDocItemProviderNewView$1;

    .line 18
    instance-of v3, p1, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    if-eqz v3, :cond_1

    .line 19
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    move-object v3, p1

    check-cast v3, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    invoke-direct {v0, p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;)V

    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 20
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 21
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TimeLineYearItemProvider;

    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TimeLineYearItemProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 22
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TimeLineNewDocItemProvider;

    const/4 v3, 0x0

    if-eqz p5, :cond_0

    .line 23
    invoke-virtual {p5}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    move-result p5

    if-ne p5, v1, :cond_0

    const/4 v3, 0x1

    .line 24
    :cond_0
    invoke-direct {v0, p0, v3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TimeLineNewDocItemProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Z)V

    .line 25
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    goto :goto_0

    .line 26
    :cond_1
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 27
    :goto_0
    new-instance p5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;

    invoke-direct {p5, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    iput-object p5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇OOo:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;

    .line 28
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    invoke-direct {v0, p0, p2, v2, p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 29
    new-instance p4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;

    invoke-direct {p4, p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V

    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8o〇O0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/AdItemProviderNew;

    .line 30
    new-instance v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;

    invoke-direct {v2, p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchOpAdProvider;

    .line 31
    new-instance v3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocOpticalRecognizeProvider;

    invoke-direct {v3, p0, p2, p6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocOpticalRecognizeProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;Lkotlin/jvm/functions/Function3;)V

    .line 32
    iput-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇oo〇O〇80:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocOpticalRecognizeProvider;

    .line 33
    new-instance p6, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/LastItemPaddingProvider;

    invoke-direct {p6, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/LastItemPaddingProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    iput-object p6, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8〇o〇88:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/LastItemPaddingProvider;

    .line 34
    new-instance v4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;

    invoke-direct {v4, p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroidx/fragment/app/FragmentActivity;)V

    iput-object v4, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;

    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8()Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "CSHome"

    goto :goto_1

    :cond_2
    const-string v5, "CSMain"

    .line 36
    :goto_1
    new-instance v6, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;

    invoke-direct {v6, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    iput-object v6, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇o88:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;

    .line 37
    new-instance v7, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DataSecurityDescItemProvider;

    invoke-direct {v7, p0, v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DataSecurityDescItemProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇80O8o8O〇:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DataSecurityDescItemProvider;

    .line 38
    invoke-virtual {p0, v6}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 39
    invoke-virtual {p0, v3}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 40
    invoke-virtual {p0, p5}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 41
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 42
    invoke-virtual {p0, p4}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 43
    invoke-virtual {p0, p6}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 44
    new-instance p4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/EmptyCardBagProvider;

    invoke-direct {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/EmptyCardBagProvider;-><init>()V

    invoke-virtual {p0, p4}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    if-eqz p3, :cond_3

    .line 45
    invoke-virtual {p0, v2}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 46
    new-instance p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchFooterItemProviderNew;

    invoke-direct {p3, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/SearchFooterItemProviderNew;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p0, p3}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 47
    :cond_3
    invoke-virtual {p0, v7}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 48
    new-instance p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;

    invoke-direct {p3, p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/CloudDiskRecProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;)V

    invoke-virtual {p0, p3}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 49
    invoke-virtual {p0, v4}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 50
    new-instance p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider;

    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o8O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider;

    .line 51
    invoke-virtual {p0, p2}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇Oo〇o8()V

    .line 53
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8oOo0:Landroidx/lifecycle/MutableLiveData;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    move-result-object p1

    new-instance p3, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$1;

    invoke-direct {p3, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    new-instance p4, LO0〇oO〇o/oO80;

    invoke-direct {p4, p3}, LO0〇oO〇o/oO80;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {p2, p1, p4}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 54
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o00〇88〇08:Z

    .line 55
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇o0〇8:Z

    .line 56
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0〇0:Z

    .line 57
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇OOoooo:Ljava/util/ArrayList;

    .line 58
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    const/4 p1, 0x4

    .line 59
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0〇0:I

    .line 60
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 61
    new-instance p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mainDocLayoutManager$2;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$mainDocLayoutManager$2;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8o:Lkotlin/Lazy;

    const/4 p1, -0x1

    .line 62
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 63
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇00:Ljava/util/Set;

    const-wide/16 p1, -0x1

    .line 64
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇O80〇0o:Ljava/lang/Long;

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/adapter/QueryInterface;ZLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;Lcom/intsig/camscanner/datastruct/FolderItem;Lkotlin/jvm/functions/Function3;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 v0, p7, 0x2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    and-int/lit8 v2, p7, 0x4

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    move v2, p3

    :goto_1
    and-int/lit8 v3, p7, 0x8

    if-eqz v3, :cond_2

    move-object v3, v1

    goto :goto_2

    :cond_2
    move-object v3, p4

    :goto_2
    and-int/lit8 v4, p7, 0x10

    if-eqz v4, :cond_3

    move-object v4, v1

    goto :goto_3

    :cond_3
    move-object v4, p5

    :goto_3
    and-int/lit8 v5, p7, 0x20

    if-eqz v5, :cond_4

    goto :goto_4

    :cond_4
    move-object v1, p6

    :goto_4
    move-object p2, p0

    move-object p3, p1

    move-object p4, v0

    move p5, v2

    move-object p6, v3

    move-object p7, v4

    move-object p8, v1

    .line 1
    invoke-direct/range {p2 .. p8}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;-><init>(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/adapter/QueryInterface;ZLcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;Lcom/intsig/camscanner/datastruct/FolderItem;Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public static synthetic O00(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇Oo(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O0o(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0oO(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o〇o(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final O0oO008(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O0oo0o0〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇OO〇00〇0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0o〇O0〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇O00〇8(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O80〇O〇080()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O〇0()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    return v2

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 18
    .line 19
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 20
    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 24
    .line 25
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    instance-of v0, v0, Lcom/intsig/camscanner/targetdir/TargetDirActivity;

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    return v2

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O88o〇()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 7
    .line 8
    .line 9
    move-result v2

    .line 10
    if-eqz v2, :cond_1

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    const/4 v3, 0x3

    .line 22
    if-ne v2, v3, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/16 v2, 0x69

    .line 29
    .line 30
    if-ne v0, v2, :cond_1

    .line 31
    .line 32
    const/4 v1, 0x1

    .line 33
    :cond_1
    :goto_0
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OO88o(Ljava/util/HashMap;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lcom/intsig/DocMultiEntity;",
            ">;I)V"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v7, Lcom/intsig/camscanner/datastruct/EmptyCardItem;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, 0x6

    .line 10
    const/4 v6, 0x0

    .line 11
    move-object v1, v7

    .line 12
    move v2, p2

    .line 13
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/datastruct/EmptyCardItem;-><init>(ILjava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    invoke-interface {p1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OOo88OOo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-eqz v3, :cond_2

    .line 24
    .line 25
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    check-cast v3, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 30
    .line 31
    invoke-virtual {v3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v3}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-ne v3, v1, :cond_1

    .line 40
    .line 41
    sget-object p1, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080:Ljava/lang/String;

    .line 42
    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v3, "DocList duplicate ! unable add to list ="

    .line 49
    .line 50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    return v0

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 65
    .line 66
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    return p1
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final OO〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 9

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-lez v1, :cond_0

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 25
    .line 26
    check-cast v1, Ljava/lang/Iterable;

    .line 27
    .line 28
    new-instance v2, Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    const/4 v4, 0x1

    .line 42
    if-eqz v3, :cond_2

    .line 43
    .line 44
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    move-object v5, v3

    .line 49
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 50
    .line 51
    invoke-static {}, Lcom/intsig/camscanner/office_doc/DocImportManager;->O8()Ljava/util/List;

    .line 52
    .line 53
    .line 54
    move-result-object v6

    .line 55
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 56
    .line 57
    .line 58
    move-result-wide v7

    .line 59
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 60
    .line 61
    .line 62
    move-result-object v5

    .line 63
    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    move-result v5

    .line 67
    xor-int/2addr v4, v5

    .line 68
    if-eqz v4, :cond_1

    .line 69
    .line 70
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-lez v1, :cond_3

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇OO〇()Z

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    if-eqz v1, :cond_3

    .line 88
    .line 89
    new-instance v1, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;

    .line 90
    .line 91
    invoke-direct {v1}, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;-><init>()V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 98
    .line 99
    check-cast v1, Ljava/util/Collection;

    .line 100
    .line 101
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    xor-int/2addr v1, v4

    .line 106
    if-eqz v1, :cond_4

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->safety_statement_show:I

    .line 113
    .line 114
    if-ne v1, v4, :cond_4

    .line 115
    .line 116
    new-instance v1, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;

    .line 117
    .line 118
    invoke-direct {v1}, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;-><init>()V

    .line 119
    .line 120
    .line 121
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    :cond_4
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 125
    .line 126
    .line 127
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final OO〇0008O8(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/DocMultiEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->O008o8oo()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0O〇O00O:Z

    .line 16
    .line 17
    if-nez v0, :cond_2

    .line 18
    .line 19
    :cond_1
    return-void

    .line 20
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O88o〇()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const/4 v1, 0x0

    .line 25
    const/4 v2, 0x1

    .line 26
    if-eqz v0, :cond_16

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 29
    .line 30
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 31
    .line 32
    .line 33
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 34
    .line 35
    const-string v3, "addPlaceHolderData deal card_bag data"

    .line 36
    .line 37
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-ne v0, v2, :cond_3

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 47
    .line 48
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 49
    .line 50
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 51
    .line 52
    .line 53
    move-result v4

    .line 54
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 55
    .line 56
    .line 57
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 58
    .line 59
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 60
    .line 61
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 62
    .line 63
    .line 64
    move-result v4

    .line 65
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 66
    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 69
    .line 70
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 71
    .line 72
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 73
    .line 74
    .line 75
    move-result v4

    .line 76
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 77
    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 80
    .line 81
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 82
    .line 83
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 84
    .line 85
    .line 86
    move-result v4

    .line 87
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_3
    if-nez v0, :cond_4

    .line 92
    .line 93
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 94
    .line 95
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 96
    .line 97
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 98
    .line 99
    .line 100
    move-result v4

    .line 101
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 102
    .line 103
    .line 104
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 105
    .line 106
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 107
    .line 108
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 109
    .line 110
    .line 111
    move-result v4

    .line 112
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 113
    .line 114
    .line 115
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 116
    .line 117
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 118
    .line 119
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 124
    .line 125
    .line 126
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 127
    .line 128
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 129
    .line 130
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 131
    .line 132
    .line 133
    move-result v4

    .line 134
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 135
    .line 136
    .line 137
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 138
    .line 139
    sget-object v4, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 140
    .line 141
    invoke-virtual {v4}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 142
    .line 143
    .line 144
    move-result v4

    .line 145
    invoke-direct {p0, v3, v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88o(Ljava/util/HashMap;I)V

    .line 146
    .line 147
    .line 148
    :cond_4
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 149
    .line 150
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 151
    .line 152
    .line 153
    move-result-object v3

    .line 154
    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 155
    .line 156
    .line 157
    move-result v4

    .line 158
    if-eqz v4, :cond_15

    .line 159
    .line 160
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 161
    .line 162
    .line 163
    move-result-object v4

    .line 164
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 165
    .line 166
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->Oooo8o0〇()I

    .line 167
    .line 168
    .line 169
    move-result v5

    .line 170
    if-eq v5, v2, :cond_5

    .line 171
    .line 172
    if-eqz v0, :cond_c

    .line 173
    .line 174
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 175
    .line 176
    .line 177
    move-result v5

    .line 178
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 179
    .line 180
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 181
    .line 182
    .line 183
    move-result v6

    .line 184
    if-ne v5, v6, :cond_6

    .line 185
    .line 186
    :goto_2
    const/4 v6, 0x1

    .line 187
    goto :goto_3

    .line 188
    :cond_6
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 189
    .line 190
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 191
    .line 192
    .line 193
    move-result v6

    .line 194
    if-ne v5, v6, :cond_7

    .line 195
    .line 196
    goto :goto_2

    .line 197
    :cond_7
    const/4 v6, 0x0

    .line 198
    :goto_3
    if-eqz v6, :cond_8

    .line 199
    .line 200
    :goto_4
    const/4 v6, 0x1

    .line 201
    goto :goto_5

    .line 202
    :cond_8
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 203
    .line 204
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 205
    .line 206
    .line 207
    move-result v6

    .line 208
    if-ne v5, v6, :cond_9

    .line 209
    .line 210
    goto :goto_4

    .line 211
    :cond_9
    const/4 v6, 0x0

    .line 212
    :goto_5
    if-eqz v6, :cond_a

    .line 213
    .line 214
    :goto_6
    const/4 v5, 0x1

    .line 215
    goto :goto_7

    .line 216
    :cond_a
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 217
    .line 218
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 219
    .line 220
    .line 221
    move-result v6

    .line 222
    if-ne v5, v6, :cond_b

    .line 223
    .line 224
    goto :goto_6

    .line 225
    :cond_b
    const/4 v5, 0x0

    .line 226
    :goto_7
    if-eqz v5, :cond_5

    .line 227
    .line 228
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 229
    .line 230
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 231
    .line 232
    .line 233
    move-result v6

    .line 234
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 235
    .line 236
    .line 237
    move-result-object v6

    .line 238
    invoke-virtual {v5, v6}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    .line 239
    .line 240
    .line 241
    move-result v5

    .line 242
    if-eqz v5, :cond_5

    .line 243
    .line 244
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 245
    .line 246
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 247
    .line 248
    .line 249
    move-result v4

    .line 250
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 251
    .line 252
    .line 253
    move-result-object v4

    .line 254
    invoke-virtual {v5, v4}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    .line 256
    .line 257
    goto :goto_1

    .line 258
    :cond_c
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 259
    .line 260
    .line 261
    move-result v5

    .line 262
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 263
    .line 264
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 265
    .line 266
    .line 267
    move-result v6

    .line 268
    if-ne v5, v6, :cond_d

    .line 269
    .line 270
    :goto_8
    const/4 v6, 0x1

    .line 271
    goto :goto_9

    .line 272
    :cond_d
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 273
    .line 274
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 275
    .line 276
    .line 277
    move-result v6

    .line 278
    if-ne v5, v6, :cond_e

    .line 279
    .line 280
    goto :goto_8

    .line 281
    :cond_e
    const/4 v6, 0x0

    .line 282
    :goto_9
    if-eqz v6, :cond_f

    .line 283
    .line 284
    :goto_a
    const/4 v6, 0x1

    .line 285
    goto :goto_b

    .line 286
    :cond_f
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 287
    .line 288
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 289
    .line 290
    .line 291
    move-result v6

    .line 292
    if-ne v5, v6, :cond_10

    .line 293
    .line 294
    goto :goto_a

    .line 295
    :cond_10
    const/4 v6, 0x0

    .line 296
    :goto_b
    if-eqz v6, :cond_11

    .line 297
    .line 298
    :goto_c
    const/4 v6, 0x1

    .line 299
    goto :goto_d

    .line 300
    :cond_11
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 301
    .line 302
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 303
    .line 304
    .line 305
    move-result v6

    .line 306
    if-ne v5, v6, :cond_12

    .line 307
    .line 308
    goto :goto_c

    .line 309
    :cond_12
    const/4 v6, 0x0

    .line 310
    :goto_d
    if-eqz v6, :cond_13

    .line 311
    .line 312
    :goto_e
    const/4 v5, 0x1

    .line 313
    goto :goto_f

    .line 314
    :cond_13
    sget-object v6, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 315
    .line 316
    invoke-virtual {v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 317
    .line 318
    .line 319
    move-result v6

    .line 320
    if-ne v5, v6, :cond_14

    .line 321
    .line 322
    goto :goto_e

    .line 323
    :cond_14
    const/4 v5, 0x0

    .line 324
    :goto_f
    if-eqz v5, :cond_5

    .line 325
    .line 326
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 327
    .line 328
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 329
    .line 330
    .line 331
    move-result v6

    .line 332
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 333
    .line 334
    .line 335
    move-result-object v6

    .line 336
    invoke-virtual {v5, v6}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    move-result v5

    .line 340
    if-eqz v5, :cond_5

    .line 341
    .line 342
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 343
    .line 344
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 345
    .line 346
    .line 347
    move-result v4

    .line 348
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 349
    .line 350
    .line 351
    move-result-object v4

    .line 352
    invoke-virtual {v5, v4}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    .line 354
    .line 355
    goto/16 :goto_1

    .line 356
    .line 357
    :cond_15
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 358
    .line 359
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 360
    .line 361
    invoke-virtual {v1}, Ljava/util/AbstractMap;->size()I

    .line 362
    .line 363
    .line 364
    move-result v1

    .line 365
    new-instance v2, Ljava/lang/StringBuilder;

    .line 366
    .line 367
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    .line 369
    .line 370
    const-string v3, "card_bag data empty_card_size :"

    .line 371
    .line 372
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 376
    .line 377
    .line 378
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v1

    .line 382
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    .line 384
    .line 385
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 386
    .line 387
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 388
    .line 389
    .line 390
    move-result-object v0

    .line 391
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 392
    .line 393
    .line 394
    move-result-object v0

    .line 395
    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 396
    .line 397
    .line 398
    move-result v1

    .line 399
    if-eqz v1, :cond_1d

    .line 400
    .line 401
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 402
    .line 403
    .line 404
    move-result-object v1

    .line 405
    check-cast v1, Ljava/util/Map$Entry;

    .line 406
    .line 407
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 408
    .line 409
    .line 410
    move-result-object v1

    .line 411
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    .line 413
    .line 414
    goto :goto_10

    .line 415
    :cond_16
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 416
    .line 417
    if-eqz v0, :cond_17

    .line 418
    .line 419
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->O8O〇()Z

    .line 420
    .line 421
    .line 422
    move-result v0

    .line 423
    if-ne v0, v2, :cond_17

    .line 424
    .line 425
    goto :goto_11

    .line 426
    :cond_17
    const/4 v2, 0x0

    .line 427
    :goto_11
    if-eqz v2, :cond_1d

    .line 428
    .line 429
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 430
    .line 431
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->O8ooOoo〇()Ljava/lang/String;

    .line 432
    .line 433
    .line 434
    move-result-object v0

    .line 435
    if-eqz v0, :cond_1d

    .line 436
    .line 437
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 438
    .line 439
    new-instance v3, Ljava/lang/StringBuilder;

    .line 440
    .line 441
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 442
    .line 443
    .line 444
    const-string v4, "addPlaceHolderData deal template empty data templateId:"

    .line 445
    .line 446
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    .line 448
    .line 449
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    .line 451
    .line 452
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 453
    .line 454
    .line 455
    move-result-object v3

    .line 456
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    .line 458
    .line 459
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 460
    .line 461
    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->clear()V

    .line 462
    .line 463
    .line 464
    sget-object v2, Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;->〇080:Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;

    .line 465
    .line 466
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/scenariodir/util/TemplateFolderUtil;->〇o〇(Ljava/lang/String;)Ljava/util/List;

    .line 467
    .line 468
    .line 469
    move-result-object v0

    .line 470
    check-cast v0, Ljava/lang/Iterable;

    .line 471
    .line 472
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 473
    .line 474
    .line 475
    move-result-object v0

    .line 476
    const/4 v2, 0x0

    .line 477
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 478
    .line 479
    .line 480
    move-result v3

    .line 481
    if-eqz v3, :cond_1c

    .line 482
    .line 483
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 484
    .line 485
    .line 486
    move-result-object v3

    .line 487
    add-int/lit8 v4, v2, 0x1

    .line 488
    .line 489
    if-gez v2, :cond_18

    .line 490
    .line 491
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 492
    .line 493
    .line 494
    :cond_18
    check-cast v3, Lcom/intsig/camscanner/scenariodir/data/PresetPlace;

    .line 495
    .line 496
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/PresetPlace;->getTitle()Ljava/lang/String;

    .line 497
    .line 498
    .line 499
    move-result-object v5

    .line 500
    if-eqz v5, :cond_1b

    .line 501
    .line 502
    iget-object v6, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 503
    .line 504
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 505
    .line 506
    .line 507
    move-result-object v6

    .line 508
    :cond_19
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 509
    .line 510
    .line 511
    move-result v7

    .line 512
    if-eqz v7, :cond_1a

    .line 513
    .line 514
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 515
    .line 516
    .line 517
    move-result-object v7

    .line 518
    check-cast v7, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 519
    .line 520
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 521
    .line 522
    .line 523
    move-result-object v7

    .line 524
    if-eqz v7, :cond_19

    .line 525
    .line 526
    const/4 v8, 0x2

    .line 527
    const/4 v9, 0x0

    .line 528
    invoke-static {v7, v5, v1, v8, v9}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 529
    .line 530
    .line 531
    move-result v7

    .line 532
    if-eqz v7, :cond_19

    .line 533
    .line 534
    goto :goto_13

    .line 535
    :cond_1a
    new-instance v6, Lcom/intsig/camscanner/datastruct/EmptyCardItem;

    .line 536
    .line 537
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/PresetPlace;->getDplink()Ljava/lang/String;

    .line 538
    .line 539
    .line 540
    move-result-object v3

    .line 541
    invoke-direct {v6, v1, v5, v3}, Lcom/intsig/camscanner/datastruct/EmptyCardItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 542
    .line 543
    .line 544
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 545
    .line 546
    .line 547
    move-result-object v2

    .line 548
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 549
    .line 550
    invoke-interface {v3, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    .line 552
    .line 553
    :cond_1b
    :goto_13
    move v2, v4

    .line 554
    goto :goto_12

    .line 555
    :cond_1c
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 556
    .line 557
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 558
    .line 559
    .line 560
    move-result-object v0

    .line 561
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 562
    .line 563
    .line 564
    move-result-object v0

    .line 565
    :goto_14
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 566
    .line 567
    .line 568
    move-result v1

    .line 569
    if-eqz v1, :cond_1d

    .line 570
    .line 571
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 572
    .line 573
    .line 574
    move-result-object v1

    .line 575
    check-cast v1, Ljava/util/Map$Entry;

    .line 576
    .line 577
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 578
    .line 579
    .line 580
    move-result-object v1

    .line 581
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    .line 583
    .line 584
    goto :goto_14

    .line 585
    :cond_1d
    return-void
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public static synthetic Oo0oO〇O〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/util/HashSet;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO0(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/util/HashSet;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇0〇o808〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇Oo()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o00〇88〇08:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o0O〇8o0O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o0oO(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 9

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/mainmenu/adapter/docrec/entities/ExternalImportEntity;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 14
    .line 15
    const v3, 0x7f131b60

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/entities/ExternalImportEntity;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-lez v1, :cond_0

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 39
    .line 40
    .line 41
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 42
    .line 43
    check-cast v1, Ljava/lang/Iterable;

    .line 44
    .line 45
    new-instance v2, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 55
    .line 56
    .line 57
    move-result v3

    .line 58
    const/4 v4, 0x1

    .line 59
    if-eqz v3, :cond_2

    .line 60
    .line 61
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    move-object v5, v3

    .line 66
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 67
    .line 68
    invoke-static {}, Lcom/intsig/camscanner/office_doc/DocImportManager;->O8()Ljava/util/List;

    .line 69
    .line 70
    .line 71
    move-result-object v6

    .line 72
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 73
    .line 74
    .line 75
    move-result-wide v7

    .line 76
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 77
    .line 78
    .line 79
    move-result-object v5

    .line 80
    invoke-interface {v6, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    xor-int/2addr v4, v5

    .line 85
    if-eqz v4, :cond_1

    .line 86
    .line 87
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    if-lez v1, :cond_3

    .line 99
    .line 100
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇OO〇()Z

    .line 101
    .line 102
    .line 103
    move-result v1

    .line 104
    if-eqz v1, :cond_3

    .line 105
    .line 106
    new-instance v1, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;

    .line 107
    .line 108
    invoke-direct {v1}, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;-><init>()V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 115
    .line 116
    check-cast v1, Ljava/util/Collection;

    .line 117
    .line 118
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    xor-int/2addr v1, v4

    .line 123
    if-eqz v1, :cond_4

    .line 124
    .line 125
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->safety_statement_show:I

    .line 130
    .line 131
    if-ne v1, v4, :cond_4

    .line 132
    .line 133
    new-instance v1, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;

    .line 134
    .line 135
    invoke-direct {v1}, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;-><init>()V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    .line 140
    .line 141
    :cond_4
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 142
    .line 143
    .line 144
    return-void
    .line 145
    .line 146
    .line 147
.end method

.method private final o88O8(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/DocMultiEntity;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/util/Date;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 11
    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/4 v3, 0x5

    .line 19
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    const/4 v5, 0x2

    .line 24
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    .line 25
    .line 26
    .line 27
    move-result v6

    .line 28
    iget-object v7, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 29
    .line 30
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v7

    .line 34
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v8

    .line 38
    if-eqz v8, :cond_5

    .line 39
    .line 40
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v8

    .line 44
    check-cast v8, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 45
    .line 46
    new-instance v9, Ljava/util/Date;

    .line 47
    .line 48
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    .line 49
    .line 50
    .line 51
    move-result-wide v10

    .line 52
    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇()Z

    .line 59
    .line 60
    .line 61
    move-result v9

    .line 62
    if-eqz v9, :cond_0

    .line 63
    .line 64
    iget-object v9, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 65
    .line 66
    invoke-interface {v9, v8}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    .line 67
    .line 68
    .line 69
    move-result v9

    .line 70
    if-eqz v9, :cond_2

    .line 71
    .line 72
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 73
    .line 74
    .line 75
    move-result v9

    .line 76
    if-ne v2, v9, :cond_2

    .line 77
    .line 78
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    .line 79
    .line 80
    .line 81
    move-result v9

    .line 82
    if-ne v6, v9, :cond_2

    .line 83
    .line 84
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    .line 85
    .line 86
    .line 87
    move-result v6

    .line 88
    if-eq v4, v6, :cond_1

    .line 89
    .line 90
    goto :goto_1

    .line 91
    :cond_1
    const/4 v4, 0x0

    .line 92
    goto :goto_2

    .line 93
    :cond_2
    :goto_1
    const/4 v4, 0x1

    .line 94
    :goto_2
    invoke-virtual {v8, v4}, Lcom/intsig/camscanner/datastruct/DocItem;->OO8oO0o〇(Z)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 98
    .line 99
    .line 100
    move-result v4

    .line 101
    if-eq v2, v4, :cond_4

    .line 102
    .line 103
    const/4 v4, -0x1

    .line 104
    if-eq v2, v4, :cond_3

    .line 105
    .line 106
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 107
    .line 108
    .line 109
    move-result v2

    .line 110
    new-instance v4, Lcom/intsig/camscanner/mainmenu/entity/TimeLineYearEntity;

    .line 111
    .line 112
    invoke-direct {v4, v2}, Lcom/intsig/camscanner/mainmenu/entity/TimeLineYearEntity;-><init>(I)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_3
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    .line 120
    .line 121
    .line 122
    move-result v2

    .line 123
    :cond_4
    :goto_3
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    .line 124
    .line 125
    .line 126
    move-result v6

    .line 127
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    .line 128
    .line 129
    .line 130
    move-result v4

    .line 131
    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    .line 133
    .line 134
    goto :goto_0

    .line 135
    :cond_5
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o8O0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0oO008(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o8〇(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isAttachedToWindow()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 25
    .line 26
    .line 27
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 32
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 33
    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "recyclerView is null "

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    new-instance v1, LO0〇oO〇o/Oo08;

    .line 57
    .line 58
    invoke-direct {v1, p0, p1}, LO0〇oO〇o/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Z)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method private static final oO0(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/util/HashSet;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$set"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Ooo()Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    invoke-virtual {p0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->addAll(Ljava/util/Collection;)Z

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO0〇〇O8o(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/widget/ImageView;ILcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p5, 0x8

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    const/4 p4, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO88〇OOO(Landroid/widget/ImageView;ILcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method private final oO0〇〇o8〇(Landroid/widget/ImageView;I)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOo(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o8〇OO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final ooo8o〇o〇()Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOo〇08〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecManager;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic ooo〇〇O〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇0O8ooO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isAttachedToWindow()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 25
    .line 26
    .line 27
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 32
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 33
    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "refreshBackupDirList recyclerView is null "

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    new-instance v1, LO0〇oO〇o/〇o〇;

    .line 57
    .line 58
    invoke-direct {v1, p0}, LO0〇oO〇o/〇o〇;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇0o〇〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->O008o8oo()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0O〇O00O:Z

    .line 17
    .line 18
    if-nez v0, :cond_2

    .line 19
    .line 20
    :cond_1
    return v2

    .line 21
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O88o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    const/4 v1, 0x1

    .line 26
    if-eqz v0, :cond_3

    .line 27
    .line 28
    return v1

    .line 29
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 30
    .line 31
    if-eqz v0, :cond_4

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->O8O〇()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-ne v0, v1, :cond_4

    .line 38
    .line 39
    const/4 v2, 0x1

    .line 40
    :cond_4
    return v2
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇0〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->o0ooO()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 15
    .line 16
    instance-of v3, v2, Ljava/util/Collection;

    .line 17
    .line 18
    const/4 v4, 0x1

    .line 19
    if-eqz v3, :cond_1

    .line 20
    .line 21
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-eqz v3, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_3

    .line 37
    .line 38
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    check-cast v3, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 43
    .line 44
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v5

    .line 52
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    if-eqz v3, :cond_2

    .line 57
    .line 58
    const/4 v4, 0x0

    .line 59
    :cond_3
    :goto_0
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->oo〇(Z)V

    .line 60
    .line 61
    .line 62
    :cond_4
    :goto_1
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o〇8〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇0o8O〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0OO8(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)Landroidx/lifecycle/MutableLiveData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8oOo0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8〇o〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isAttachedToWindow()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 25
    .line 26
    .line 27
    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 32
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 33
    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "refreshBackupDirList recyclerView is null "

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    new-instance v1, LO0〇oO〇o/O8;

    .line 57
    .line 58
    invoke-direct {v1, p0}, LO0〇oO〇o/O8;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇O8〇OO〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    :cond_1
    const/4 v0, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_2
    const/4 v0, 0x0

    .line 20
    :goto_0
    return v0
    .line 21
.end method

.method private final 〇OO8Oo0〇()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08880o0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/16 v0, 0xa

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/16 v0, 0x32

    .line 11
    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇Oo(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    check-cast p0, Ljava/lang/Number;

    .line 11
    .line 12
    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result p0

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇Oo〇o8()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->〇80()Lcom/intsig/advertisement/adapters/positions/DocListManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->oO00OOO()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oOo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O〇O888O()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 13
    .line 14
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇Oo80()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇8(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/os/CountDownTimer;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoo80oO:Landroid/os/CountDownTimer;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇00OO(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)Landroidx/fragment/app/FragmentActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0O8ooO()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8oO〇()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_4

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooo0〇O88O(Ljava/lang/Long;)V

    .line 17
    .line 18
    .line 19
    new-instance v2, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 25
    .line 26
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    const/4 v4, 0x0

    .line 31
    const/4 v5, 0x0

    .line 32
    :goto_0
    if-ge v5, v3, :cond_1

    .line 33
    .line 34
    iget-object v6, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 35
    .line 36
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    check-cast v6, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 41
    .line 42
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 43
    .line 44
    .line 45
    move-result-wide v7

    .line 46
    cmp-long v9, v7, v0

    .line 47
    .line 48
    if-nez v9, :cond_0

    .line 49
    .line 50
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    const/4 v1, 0x1

    .line 61
    if-ne v0, v1, :cond_2

    .line 62
    .line 63
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const-string v1, "highlightList[0]"

    .line 68
    .line 69
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 73
    .line 74
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8O〇8oo08(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 75
    .line 76
    .line 77
    goto :goto_1

    .line 78
    :cond_2
    const/4 v2, 0x2

    .line 79
    if-gt v2, v0, :cond_3

    .line 80
    .line 81
    const v2, 0x7fffffff

    .line 82
    .line 83
    .line 84
    if-gt v0, v2, :cond_3

    .line 85
    .line 86
    const/4 v4, 0x1

    .line 87
    :cond_3
    if-eqz v4, :cond_4

    .line 88
    .line 89
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 90
    .line 91
    .line 92
    :cond_4
    :goto_1
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇0o8O〇〇()V
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 7
    .line 8
    .line 9
    const/4 v1, 0x4

    .line 10
    iput v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0〇0:I

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    invoke-static {v2}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->Oo08(Landroid/content/Context;)Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/4 v3, 0x0

    .line 19
    const/4 v4, 0x1

    .line 20
    if-eqz v2, :cond_2

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o00〇〇Oo()Lcom/intsig/webstorage/WebStorageAPIFactory;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 27
    .line 28
    invoke-virtual {v2, v1, v5}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇080(ILandroid/content/Context;)Lcom/intsig/webstorage/WebStorageApi;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {p0, v0, v2, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o8OO0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;I)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 36
    .line 37
    const-string v2, "4"

    .line 38
    .line 39
    invoke-static {v2, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇〇888(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    move-object v5, v1

    .line 44
    check-cast v5, Ljava/util/Collection;

    .line 45
    .line 46
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    xor-int/2addr v5, v4

    .line 51
    if-eqz v5, :cond_0

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    move-object v1, v0

    .line 55
    :goto_0
    if-eqz v1, :cond_1

    .line 56
    .line 57
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v5

    .line 61
    check-cast v5, Lcom/intsig/webstorage/RemoteFile;

    .line 62
    .line 63
    iput-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    .line 64
    .line 65
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-le v5, v4, :cond_1

    .line 70
    .line 71
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result v5

    .line 75
    sub-int/2addr v5, v4

    .line 76
    invoke-interface {v1, v3, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    check-cast v1, Ljava/lang/Iterable;

    .line 81
    .line 82
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 87
    .line 88
    .line 89
    move-result v5

    .line 90
    if-eqz v5, :cond_1

    .line 91
    .line 92
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v5

    .line 96
    check-cast v5, Lcom/intsig/webstorage/RemoteFile;

    .line 97
    .line 98
    invoke-static {v2, v5}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080(Ljava/lang/String;Lcom/intsig/webstorage/RemoteFile;)V

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 103
    .line 104
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 105
    .line 106
    .line 107
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 108
    .line 109
    invoke-static {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇o〇(Landroid/content/Context;)Z

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    if-eqz v1, :cond_6

    .line 114
    .line 115
    invoke-static {}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o00〇〇Oo()Lcom/intsig/webstorage/WebStorageAPIFactory;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 120
    .line 121
    const/4 v5, 0x2

    .line 122
    invoke-virtual {v1, v5, v2}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇080(ILandroid/content/Context;)Lcom/intsig/webstorage/WebStorageApi;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    invoke-virtual {p0, v0, v1, v5}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o8OO0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;I)V

    .line 127
    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 130
    .line 131
    const-string v2, "2"

    .line 132
    .line 133
    invoke-static {v2, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇〇888(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    move-object v6, v1

    .line 138
    check-cast v6, Ljava/util/Collection;

    .line 139
    .line 140
    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    .line 141
    .line 142
    .line 143
    move-result v6

    .line 144
    xor-int/2addr v6, v4

    .line 145
    if-eqz v6, :cond_3

    .line 146
    .line 147
    goto :goto_2

    .line 148
    :cond_3
    move-object v1, v0

    .line 149
    :goto_2
    if-eqz v1, :cond_5

    .line 150
    .line 151
    move-object v6, v1

    .line 152
    check-cast v6, Ljava/lang/Iterable;

    .line 153
    .line 154
    new-instance v7, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getCloudDiskData$lambda$10$$inlined$sortedBy$1;

    .line 155
    .line 156
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getCloudDiskData$lambda$10$$inlined$sortedBy$1;-><init>()V

    .line 157
    .line 158
    .line 159
    invoke-static {v6, v7}, Lkotlin/collections/CollectionsKt;->〇o0O0O8(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    .line 160
    .line 161
    .line 162
    move-result-object v6

    .line 163
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 164
    .line 165
    .line 166
    move-result-object v1

    .line 167
    check-cast v1, Lcom/intsig/webstorage/RemoteFile;

    .line 168
    .line 169
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    .line 170
    .line 171
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    if-le v1, v4, :cond_4

    .line 176
    .line 177
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 178
    .line 179
    .line 180
    move-result v1

    .line 181
    sub-int/2addr v1, v4

    .line 182
    invoke-interface {v6, v3, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    check-cast v1, Ljava/lang/Iterable;

    .line 187
    .line 188
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 193
    .line 194
    .line 195
    move-result v6

    .line 196
    if-eqz v6, :cond_4

    .line 197
    .line 198
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 199
    .line 200
    .line 201
    move-result-object v6

    .line 202
    check-cast v6, Lcom/intsig/webstorage/RemoteFile;

    .line 203
    .line 204
    invoke-static {v2, v6}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080(Ljava/lang/String;Lcom/intsig/webstorage/RemoteFile;)V

    .line 205
    .line 206
    .line 207
    goto :goto_3

    .line 208
    :cond_4
    iput v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0〇0:I

    .line 209
    .line 210
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 211
    .line 212
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 213
    .line 214
    .line 215
    :cond_6
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 216
    .line 217
    invoke-static {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇〇888(Landroid/content/Context;)Z

    .line 218
    .line 219
    .line 220
    move-result v1

    .line 221
    if-eqz v1, :cond_a

    .line 222
    .line 223
    invoke-static {}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇o00〇〇Oo()Lcom/intsig/webstorage/WebStorageAPIFactory;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 228
    .line 229
    invoke-virtual {v1, v3, v2}, Lcom/intsig/webstorage/WebStorageAPIFactory;->〇080(ILandroid/content/Context;)Lcom/intsig/webstorage/WebStorageApi;

    .line 230
    .line 231
    .line 232
    move-result-object v1

    .line 233
    invoke-virtual {p0, v0, v1, v3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o8OO0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;I)V

    .line 234
    .line 235
    .line 236
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 237
    .line 238
    const-string v2, "0"

    .line 239
    .line 240
    invoke-static {v2, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇〇888(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    .line 241
    .line 242
    .line 243
    move-result-object v1

    .line 244
    move-object v5, v1

    .line 245
    check-cast v5, Ljava/util/Collection;

    .line 246
    .line 247
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 248
    .line 249
    .line 250
    move-result v5

    .line 251
    xor-int/2addr v5, v4

    .line 252
    if-eqz v5, :cond_7

    .line 253
    .line 254
    move-object v0, v1

    .line 255
    :cond_7
    if-eqz v0, :cond_9

    .line 256
    .line 257
    move-object v1, v0

    .line 258
    check-cast v1, Ljava/lang/Iterable;

    .line 259
    .line 260
    new-instance v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getCloudDiskData$lambda$15$$inlined$sortedBy$1;

    .line 261
    .line 262
    invoke-direct {v5}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getCloudDiskData$lambda$15$$inlined$sortedBy$1;-><init>()V

    .line 263
    .line 264
    .line 265
    invoke-static {v1, v5}, Lkotlin/collections/CollectionsKt;->〇o0O0O8(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    .line 266
    .line 267
    .line 268
    move-result-object v1

    .line 269
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O〇80o08O(Ljava/util/List;)Ljava/lang/Object;

    .line 270
    .line 271
    .line 272
    move-result-object v0

    .line 273
    check-cast v0, Lcom/intsig/webstorage/RemoteFile;

    .line 274
    .line 275
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    .line 276
    .line 277
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 278
    .line 279
    .line 280
    move-result v0

    .line 281
    if-le v0, v4, :cond_8

    .line 282
    .line 283
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    sub-int/2addr v0, v4

    .line 288
    invoke-interface {v1, v3, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 289
    .line 290
    .line 291
    move-result-object v0

    .line 292
    check-cast v0, Ljava/lang/Iterable;

    .line 293
    .line 294
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 295
    .line 296
    .line 297
    move-result-object v0

    .line 298
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 299
    .line 300
    .line 301
    move-result v1

    .line 302
    if-eqz v1, :cond_8

    .line 303
    .line 304
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 305
    .line 306
    .line 307
    move-result-object v1

    .line 308
    check-cast v1, Lcom/intsig/webstorage/RemoteFile;

    .line 309
    .line 310
    invoke-static {v2, v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080(Ljava/lang/String;Lcom/intsig/webstorage/RemoteFile;)V

    .line 311
    .line 312
    .line 313
    goto :goto_4

    .line 314
    :cond_8
    iput v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0〇0:I

    .line 315
    .line 316
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 317
    .line 318
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 319
    .line 320
    .line 321
    :cond_a
    return-void
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final 〇〇O00〇8(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Z)V
    .locals 11

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->〇o00〇〇Oo()Z

    move-result v4

    if-nez v4, :cond_0

    .line 3
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->O8()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->〇080()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇o〇()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8oOo0:Landroidx/lifecycle/MutableLiveData;

    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecEntity;

    if-eqz p1, :cond_6

    .line 6
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 7
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇088O:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->O8()Z

    move-result v1

    if-nez v1, :cond_1

    .line 8
    iput-boolean v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0〇o:Z

    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇088O:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 10
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇080()Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    if-eqz p1, :cond_5

    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇o00〇〇Oo()Z

    move-result p1

    if-nez p1, :cond_5

    .line 11
    new-instance p1, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 12
    sget-object v5, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;->PDF:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;

    iget-object v10, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇O8OO:Lcom/intsig/webstorage/RemoteFile;

    if-eqz v10, :cond_2

    iget-object v1, v10, Lcom/intsig/webstorage/RemoteFile;->〇o00〇〇Oo:Ljava/lang/String;

    move-object v6, v1

    goto :goto_0

    :cond_2
    move-object v6, v2

    :goto_0
    if-eqz v10, :cond_3

    iget-object v1, v10, Lcom/intsig/webstorage/RemoteFile;->Oo08:Ljava/lang/String;

    move-object v7, v1

    goto :goto_1

    :cond_3
    move-object v7, v2

    :goto_1
    if-eqz v10, :cond_4

    .line 13
    iget-object v1, v10, Lcom/intsig/webstorage/RemoteFile;->〇8o8o〇:Ljava/lang/String;

    move-object v8, v1

    goto :goto_2

    :cond_4
    move-object v8, v2

    :goto_2
    iget v9, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0〇0:I

    move-object v4, p1

    .line 14
    invoke-direct/range {v4 .. v10}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;-><init>(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/intsig/webstorage/RemoteFile;)V

    .line 15
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 16
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskExp;->〇080()Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇080:Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;

    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇o〇()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇o00〇〇Oo()Z

    move-result p1

    if-nez p1, :cond_6

    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 18
    invoke-static {p1}, Lcom/intsig/camscanner/gallery/cloud_disk/CloudDiskHelper;->〇8o8o〇(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 19
    new-instance p1, Lcom/intsig/camscanner/util/MainMenuTipsChecker$CloudDiskRecEntity;

    invoke-direct {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$CloudDiskRecEntity;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    :cond_6
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_7

    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 22
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 23
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 24
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇〇0〇()Z

    move-result v4

    if-eqz v4, :cond_8

    goto :goto_4

    :cond_9
    move-object v1, v2

    :goto_4
    check-cast v1, Lcom/intsig/camscanner/datastruct/FolderItem;

    const/4 p1, -0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_c

    .line 25
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 26
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 27
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    const/4 v7, 0x0

    :goto_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 28
    check-cast v8, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 29
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oo8Oo00oo()Z

    move-result v8

    if-eqz v8, :cond_a

    goto :goto_6

    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_b
    const/4 v7, -0x1

    :goto_6
    add-int/2addr v7, v3

    invoke-virtual {v5, v7, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 30
    :cond_c
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇008〇o0〇〇()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 32
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 33
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇00〇8()Z

    move-result v6

    if-eqz v6, :cond_d

    goto :goto_7

    :cond_e
    move-object v5, v2

    :goto_7
    check-cast v5, Lcom/intsig/camscanner/datastruct/FolderItem;

    if-eqz v5, :cond_16

    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 36
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v6, 0x0

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 37
    check-cast v7, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 38
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇〇0〇()Z

    move-result v7

    if-eqz v7, :cond_f

    goto :goto_9

    :cond_f
    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    :cond_10
    const/4 v6, -0x1

    :goto_9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v6

    if-eq v6, p1, :cond_11

    const/4 v6, 0x1

    goto :goto_a

    :cond_11
    const/4 v6, 0x0

    :goto_a
    if-eqz v6, :cond_12

    goto :goto_b

    :cond_12
    move-object v1, v2

    :goto_b
    if-eqz v1, :cond_13

    .line 40
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_d

    .line 41
    :cond_13
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 42
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v6, 0x0

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 43
    check-cast v7, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 44
    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oo8Oo00oo()Z

    move-result v7

    if-eqz v7, :cond_14

    move p1, v6

    goto :goto_d

    :cond_14
    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    .line 45
    :cond_15
    :goto_d
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    add-int/2addr p1, v3

    invoke-virtual {v1, p1, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 46
    :cond_16
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 47
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_17
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 48
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oo8Oo00oo()Z

    move-result v5

    if-eqz v5, :cond_17

    goto :goto_e

    :cond_18
    move-object v1, v2

    :goto_e
    check-cast v1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 49
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 50
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇80(Landroid/content/Context;Ljava/util/Set;)V

    if-eqz v1, :cond_19

    .line 51
    invoke-static {}, Lcom/intsig/camscanner/external_import/ExternalImportOptExp;->〇o〇()Z

    move-result v5

    if-eqz v5, :cond_19

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_19

    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 53
    :cond_19
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_1a

    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 55
    :cond_1a
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 56
    new-instance v1, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {p1, v5}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 57
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_f
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 58
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 59
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 60
    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 61
    :cond_1b
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8()Z

    move-result v1

    invoke-static {p1, v1}, Lcom/intsig/camscanner/office_doc/DocImportManager;->〇o〇(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 62
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_10
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/intsig/camscanner/office_doc/DocImportCell;

    .line 63
    new-instance v5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider$DocImportEntity;

    invoke-direct {v5, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider$DocImportEntity;-><init>(Lcom/intsig/camscanner/office_doc/DocImportCell;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 64
    :cond_1c
    iget p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    if-eq p1, v3, :cond_1d

    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0880()Z

    move-result p1

    if-eqz p1, :cond_1d

    .line 65
    new-instance p1, Lcom/intsig/camscanner/mainmenu/entity/TimeLineNewDocEntity;

    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/entity/TimeLineNewDocEntity;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_1d
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_22

    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    instance-of v1, v1, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    if-nez v1, :cond_1e

    goto :goto_11

    :cond_1e
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v5, 0x4

    if-ne v1, v5, :cond_1f

    .line 69
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o88O8(Ljava/util/ArrayList;)V

    goto :goto_13

    .line 70
    :cond_1f
    :goto_11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 71
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 72
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_20
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 73
    invoke-static {}, Lcom/intsig/camscanner/office_doc/DocImportManager;->O8()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v7}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v7, v3

    if-eqz v7, :cond_20

    .line 74
    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_12

    .line 75
    :cond_21
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 76
    :cond_22
    :goto_13
    iget v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    if-eq v1, v3, :cond_23

    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O80〇O〇080()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 77
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇0008O8(Ljava/util/ArrayList;)V

    .line 78
    :cond_23
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_24

    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇OO〇()Z

    move-result v1

    if-eqz v1, :cond_24

    .line 79
    new-instance v1, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;

    invoke-direct {v1}, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_24
    iget-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇OO〇00〇0O:Z

    if-eqz v1, :cond_29

    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o880:Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    if-eqz v1, :cond_25

    .line 82
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_25
    sget-object v1, Lcom/intsig/camscanner/searchactivity/SearchUtil;->〇080:Lcom/intsig/camscanner/searchactivity/SearchUtil;

    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/adapter/QueryInterface;

    if-eqz v5, :cond_26

    invoke-interface {v5}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    move-result-object v2

    :cond_26
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/searchactivity/SearchUtil;->〇80〇808〇O([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_28

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_27

    goto :goto_14

    :cond_27
    const/4 v1, 0x0

    goto :goto_15

    :cond_28
    :goto_14
    const/4 v1, 0x1

    :goto_15
    if-nez v1, :cond_29

    .line 84
    new-instance v1, Lcom/intsig/camscanner/datastruct/SearchFooterItem;

    invoke-direct {v1}, Lcom/intsig/camscanner/datastruct/SearchFooterItem;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_29
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇Oo()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2a
    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 87
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2b
    :goto_17
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 88
    invoke-virtual {v6}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    move-result-object v7

    invoke-virtual {v7}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    move-result v7

    if-nez v2, :cond_2c

    goto :goto_17

    :cond_2c
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v7, v8, :cond_2b

    .line 89
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v2, p1

    .line 90
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gt v2, v5, :cond_2a

    .line 91
    invoke-virtual {v0, v2, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_16

    .line 92
    :cond_2d
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    if-eqz p1, :cond_2e

    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇08O8o〇0()Z

    move-result p1

    goto :goto_18

    :cond_2e
    const/4 p1, 0x0

    .line 93
    :goto_18
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o8oO()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 94
    new-instance v1, Lcom/intsig/camscanner/mainmenu/entity/MainShowMoreEntity;

    invoke-direct {v1}, Lcom/intsig/camscanner/mainmenu/entity/MainShowMoreEntity;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_2f
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_30

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v1

    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->safety_statement_show:I

    if-ne v1, v3, :cond_30

    if-nez p1, :cond_30

    .line 96
    new-instance p1, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;

    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_30
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 98
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v3

    if-eqz p1, :cond_31

    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    instance-of v0, p1, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    if-eqz v0, :cond_31

    check-cast p1, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O〇0()Z

    move-result p1

    if-eqz p1, :cond_31

    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    check-cast p1, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O8〇〇o8〇(J)V

    .line 100
    :cond_31
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    instance-of p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    if-eqz p1, :cond_32

    .line 101
    check-cast p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->〇8ooOO()V

    :cond_32
    return-void
.end method

.method private final 〇〇〇0880()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x4

    .line 17
    if-ne v0, v1, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 22
    :goto_1
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O00O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇o〇Oo88:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O08O0〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O〇0()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0ooOOo:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O0O〇OOo(Ljava/lang/Long;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇O80〇0o:Ljava/lang/Long;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O0oO0(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamEntry;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "list"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    .line 12
    .line 13
    check-cast p1, Ljava/util/Collection;

    .line 14
    .line 15
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x1

    .line 19
    const/4 v0, 0x0

    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-static {p0, v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final O0o〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0ooOOo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O0〇oO〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇0o()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0ooOOo:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O0〇oo()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇00:Ljava/util/Set;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8888()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/AbstractMap;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O88〇〇o0O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0ooOOo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8OO08o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;)V
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "holder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 7
    .line 8
    .line 9
    instance-of v0, p1, Lcom/intsig/camscanner/mainmenu/adapter/GlideClearViewHolder;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/camscanner/mainmenu/adapter/GlideClearViewHolder;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    if-eqz p1, :cond_1

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/GlideClearViewHolder;->〇00()V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final O8O〇8oo08(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->Oo8Oo00oo()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    add-int/2addr p1, v0

    .line 15
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public final O8oOo80()Landroidx/fragment/app/Fragment;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0〇〇8()V
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity;->O8o08O8O:Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity$Companion;

    .line 6
    .line 7
    invoke-virtual {v2}, Lcom/intsig/camscanner/ads_new/view/AppLaunchAdActivity$Companion;->〇080()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    sub-long/2addr v0, v2

    .line 12
    const-wide/16 v2, 0xbb8

    .line 13
    .line 14
    cmp-long v4, v0, v2

    .line 15
    .line 16
    if-gez v4, :cond_0

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "appLaunch start and not clear"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 34
    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->〇80()Lcom/intsig/advertisement/adapters/positions/DocListManager;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/DocListManager;->oO00OOO()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final OO88〇OOO(Landroid/widget/ImageView;ILcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;Z)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-ne v0, p2, :cond_1

    .line 11
    .line 12
    if-nez p4, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    :goto_0
    sget-object p4, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 16
    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v1, "setDocLayoutManager --> docViewMode:"

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, " "

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-static {p4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    if-eqz p3, :cond_2

    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 45
    .line 46
    .line 47
    move-result-object p4

    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 49
    .line 50
    invoke-virtual {p4, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->oO80(Landroid/content/Context;)I

    .line 51
    .line 52
    .line 53
    move-result p4

    .line 54
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o〇0(I)V

    .line 55
    .line 56
    .line 57
    :cond_2
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 58
    .line 59
    .line 60
    move-result-object p3

    .line 61
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getItemDecorationCount()I

    .line 62
    .line 63
    .line 64
    move-result p3

    .line 65
    const/4 p4, 0x0

    .line 66
    if-lez p3, :cond_3

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 69
    .line 70
    .line 71
    move-result-object p3

    .line 72
    invoke-virtual {p3, p4}, Landroidx/recyclerview/widget/RecyclerView;->removeItemDecorationAt(I)V

    .line 73
    .line 74
    .line 75
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇oOo〇()V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇08〇0〇o〇8()V

    .line 79
    .line 80
    .line 81
    const/4 p3, 0x4

    .line 82
    if-ne p2, p3, :cond_6

    .line 83
    .line 84
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    instance-of v1, v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 93
    .line 94
    if-eqz v1, :cond_4

    .line 95
    .line 96
    check-cast v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_4
    const/4 v0, 0x0

    .line 100
    :goto_1
    if-nez v0, :cond_5

    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_5
    invoke-virtual {v0, p4}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 104
    .line 105
    .line 106
    :cond_6
    :goto_2
    if-eqz p2, :cond_d

    .line 107
    .line 108
    const/4 v0, 0x1

    .line 109
    const/high16 v1, 0x40c00000    # 6.0f

    .line 110
    .line 111
    const/high16 v2, 0x41800000    # 16.0f

    .line 112
    .line 113
    if-eq p2, v0, :cond_a

    .line 114
    .line 115
    const/4 v0, 0x2

    .line 116
    if-eq p2, v0, :cond_8

    .line 117
    .line 118
    const/4 p1, 0x3

    .line 119
    if-eq p2, p1, :cond_7

    .line 120
    .line 121
    if-eq p2, p3, :cond_7

    .line 122
    .line 123
    goto/16 :goto_4

    .line 124
    .line 125
    :cond_7
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 130
    .line 131
    .line 132
    move-result p3

    .line 133
    const/high16 v0, 0x41400000    # 12.0f

    .line 134
    .line 135
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    invoke-virtual {p1, p3, v0, v1, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    new-instance p3, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;

    .line 151
    .line 152
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 153
    .line 154
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 155
    .line 156
    .line 157
    move-result-object p4

    .line 158
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08()I

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    const/4 v3, 0x1

    .line 163
    const/4 v4, 0x0

    .line 164
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$8;->o0:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$8;

    .line 165
    .line 166
    const/4 v6, 0x1

    .line 167
    move-object v0, p3

    .line 168
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;-><init>(Landroid/content/Context;IIZLkotlin/jvm/functions/Function1;Z)V

    .line 169
    .line 170
    .line 171
    new-instance p4, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$9$1;

    .line 172
    .line 173
    invoke-direct {p4, p0, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$9$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {p3, p4}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 180
    .line 181
    .line 182
    goto/16 :goto_4

    .line 183
    .line 184
    :cond_8
    if-eqz p1, :cond_9

    .line 185
    .line 186
    const p3, 0x7f080bb4

    .line 187
    .line 188
    .line 189
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO0〇〇o8〇(Landroid/widget/ImageView;I)V

    .line 190
    .line 191
    .line 192
    :cond_9
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 193
    .line 194
    .line 195
    move-result-object p1

    .line 196
    invoke-virtual {p1, p4, p4, p4, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    new-instance p3, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;

    .line 204
    .line 205
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 206
    .line 207
    const/4 v2, 0x1

    .line 208
    const/4 v3, 0x0

    .line 209
    sget-object v4, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$5;->o0:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$5;

    .line 210
    .line 211
    const/4 v5, 0x1

    .line 212
    move-object v0, p3

    .line 213
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;-><init>(Landroid/content/Context;IZLkotlin/jvm/functions/Function1;Z)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 217
    .line 218
    .line 219
    goto/16 :goto_4

    .line 220
    .line 221
    :cond_a
    if-eqz p1, :cond_b

    .line 222
    .line 223
    const p3, 0x7f080bb3

    .line 224
    .line 225
    .line 226
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO0〇〇o8〇(Landroid/widget/ImageView;I)V

    .line 227
    .line 228
    .line 229
    :cond_b
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 230
    .line 231
    .line 232
    move-result p1

    .line 233
    const/high16 p3, 0x41000000    # 8.0f

    .line 234
    .line 235
    if-nez p1, :cond_c

    .line 236
    .line 237
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 238
    .line 239
    .line 240
    move-result-object p1

    .line 241
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 242
    .line 243
    .line 244
    move-result v0

    .line 245
    invoke-static {p3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 246
    .line 247
    .line 248
    move-result p3

    .line 249
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 250
    .line 251
    .line 252
    move-result v1

    .line 253
    invoke-virtual {p1, v0, p3, v1, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 254
    .line 255
    .line 256
    goto :goto_3

    .line 257
    :cond_c
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 258
    .line 259
    .line 260
    move-result-object p1

    .line 261
    invoke-static {p3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 262
    .line 263
    .line 264
    move-result p3

    .line 265
    invoke-virtual {p1, p4, p3, p4, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 266
    .line 267
    .line 268
    :goto_3
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    new-instance p3, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;

    .line 273
    .line 274
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 275
    .line 276
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 277
    .line 278
    .line 279
    move-result-object p4

    .line 280
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08()I

    .line 281
    .line 282
    .line 283
    move-result v2

    .line 284
    const/4 v3, 0x1

    .line 285
    const/4 v4, 0x0

    .line 286
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$2;->o0:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$2;

    .line 287
    .line 288
    const/4 v6, 0x1

    .line 289
    move-object v0, p3

    .line 290
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;-><init>(Landroid/content/Context;IIZLkotlin/jvm/functions/Function1;Z)V

    .line 291
    .line 292
    .line 293
    new-instance p4, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$3$1;

    .line 294
    .line 295
    invoke-direct {p4, p0, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$3$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {p3, p4}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 299
    .line 300
    .line 301
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 302
    .line 303
    .line 304
    goto :goto_4

    .line 305
    :cond_d
    if-eqz p1, :cond_e

    .line 306
    .line 307
    const p3, 0x7f080bb5

    .line 308
    .line 309
    .line 310
    invoke-direct {p0, p1, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO0〇〇o8〇(Landroid/widget/ImageView;I)V

    .line 311
    .line 312
    .line 313
    :cond_e
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 314
    .line 315
    .line 316
    move-result-object p1

    .line 317
    invoke-virtual {p1, p4, p4, p4, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 321
    .line 322
    .line 323
    move-result-object p1

    .line 324
    new-instance p3, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;

    .line 325
    .line 326
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 327
    .line 328
    const/4 v2, 0x1

    .line 329
    const/4 v3, 0x0

    .line 330
    sget-object v4, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$7;->o0:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$setDocLayoutManager$7;

    .line 331
    .line 332
    const/4 v5, 0x1

    .line 333
    move-object v0, p3

    .line 334
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;-><init>(Landroid/content/Context;IZLkotlin/jvm/functions/Function1;Z)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 338
    .line 339
    .line 340
    :goto_4
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 341
    .line 342
    .line 343
    move-result-object p1

    .line 344
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 345
    .line 346
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 347
    .line 348
    .line 349
    move-result-object p1

    .line 350
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getRecycledViewPool()Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 351
    .line 352
    .line 353
    move-result-object p1

    .line 354
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;->clear()V

    .line 355
    .line 356
    .line 357
    return-void
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public final OO8〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 3
    .param p1    # Lcom/intsig/advertisement/interfaces/RealRequestAbs;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "realRequestAbs"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const/4 p1, 0x1

    .line 37
    const/4 v0, 0x0

    .line 38
    const/4 v1, 0x0

    .line 39
    invoke-static {p0, v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    sget-object v0, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v2, "cannot close  ad index="

    .line 59
    .line 60
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final OOo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OOo88OOo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o0OoOOo0:Ljava/util/ArrayList;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$updateAdInfo$1;->o0:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$updateAdInfo$1;

    .line 10
    .line 11
    new-instance v1, LO0〇oO〇o/〇〇888;

    .line 12
    .line 13
    invoke-direct {v1, v0}, LO0〇oO〇o/〇〇888;-><init>(Lkotlin/jvm/functions/Function2;)V

    .line 14
    .line 15
    .line 16
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->〇oo〇(Ljava/util/List;Ljava/util/Comparator;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇0〇o808〇()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇Oo()Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-eqz p1, :cond_0

    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    const/4 v0, 0x0

    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-static {p0, v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final OOoo(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .param p1    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 3
    .line 4
    const-string v0, "checkHasOfficeDoc"

    .line 5
    .line 6
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 10
    .line 11
    check-cast p1, Ljava/lang/Iterable;

    .line 12
    .line 13
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    const/4 v1, 0x1

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇00(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    invoke-static {v1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇080(Z)Ljava/lang/Boolean;

    .line 41
    .line 42
    .line 43
    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit p0

    .line 45
    return-object p1

    .line 46
    :cond_1
    :try_start_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 47
    .line 48
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    if-eqz v0, :cond_3

    .line 57
    .line 58
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 63
    .line 64
    sget-object v2, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 65
    .line 66
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->OoO8(Lcom/intsig/camscanner/datastruct/FolderItem;)Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    if-eqz v0, :cond_2

    .line 71
    .line 72
    invoke-static {v1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇080(Z)Ljava/lang/Boolean;

    .line 73
    .line 74
    .line 75
    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    monitor-exit p0

    .line 77
    return-object p1

    .line 78
    :cond_3
    const/4 p1, 0x0

    .line 79
    :try_start_2
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇080(Z)Ljava/lang/Boolean;

    .line 80
    .line 81
    .line 82
    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 83
    monitor-exit p0

    .line 84
    return-object p1

    .line 85
    :catchall_0
    move-exception p1

    .line 86
    monitor-exit p0

    .line 87
    throw p1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final Oo08OO8oO()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0〇o:Z

    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇00O0O〇o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo0O080(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o00〇88〇08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo0oOo〇0()Ljava/lang/Long;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇O80〇0o:Ljava/lang/Long;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OoOOo8()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇o〇Oo88:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OoO〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇o0〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Ooo8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo〇()Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇o00〇〇Oo()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    :goto_0
    return v1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8oOo8O()Lcom/intsig/camscanner/datastruct/FolderItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇oO〇oo8o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0o〇〇Oo()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇〇()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇O〇80o08O()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o08O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo08:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/backup/BackUpHelperKt;->〇o00〇〇Oo(Ljava/lang/String;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v2, 0x1

    .line 17
    if-ne v0, v2, :cond_0

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    :cond_0
    return v1
    .line 21
.end method

.method public final o08oOO()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamEntry;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o08〇〇0O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO00〇o:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o88O〇8()I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    instance-of v2, v0, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;

    .line 12
    .line 13
    if-eqz v2, :cond_1

    .line 14
    .line 15
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v0, 0x0

    .line 19
    :goto_0
    if-eqz v0, :cond_2

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 22
    .line 23
    instance-of v1, v1, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 24
    .line 25
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;->O0oO0(Z)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    :cond_2
    return v1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o88o0O(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "runnable"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public final o8o〇〇0O()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO8008O()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->o〇8()Ljava/util/HashSet;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-static {}, Lkotlin/collections/SetsKt;->O8()Ljava/util/Set;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80OOO〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "itemClickListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇oo:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OOo8o〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇o88:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇oo:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 16
    .line 17
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/ShowMoreItemProvider;->O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public final oO8o()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/DocMultiEntity;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/DocMultiEntity;

    .line 11
    .line 12
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 13
    .line 14
    if-eqz p2, :cond_0

    .line 15
    .line 16
    const/16 p1, 0xa

    .line 17
    .line 18
    goto/16 :goto_0

    .line 19
    .line 20
    :cond_0
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 21
    .line 22
    const/16 v0, 0xb

    .line 23
    .line 24
    if-eqz p2, :cond_2

    .line 25
    .line 26
    :cond_1
    const/16 p1, 0xb

    .line 27
    .line 28
    goto/16 :goto_0

    .line 29
    .line 30
    :cond_2
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/EmptyCardItem;

    .line 31
    .line 32
    if-eqz p2, :cond_3

    .line 33
    .line 34
    const/16 p1, 0x13

    .line 35
    .line 36
    goto/16 :goto_0

    .line 37
    .line 38
    :cond_3
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/EmptyFooterItem;

    .line 39
    .line 40
    if-eqz p2, :cond_4

    .line 41
    .line 42
    const/16 p1, 0x12

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_4
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamEntry;

    .line 46
    .line 47
    if-eqz p2, :cond_5

    .line 48
    .line 49
    const/16 p1, 0xc

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_5
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/SearchFooterItem;

    .line 53
    .line 54
    if-eqz p2, :cond_6

    .line 55
    .line 56
    const/16 p1, 0xe

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_6
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 60
    .line 61
    if-eqz p2, :cond_7

    .line 62
    .line 63
    const/16 p1, 0xf

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_7
    instance-of p2, p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 67
    .line 68
    if-eqz p2, :cond_8

    .line 69
    .line 70
    const/16 p1, 0xd

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_8
    instance-of p2, p1, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 74
    .line 75
    if-eqz p2, :cond_9

    .line 76
    .line 77
    const/16 p1, 0x10

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_9
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/entity/MainDataSecurityEntity;

    .line 81
    .line 82
    if-eqz p2, :cond_a

    .line 83
    .line 84
    const/16 p1, 0x11

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_a
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/entity/TimeLineYearEntity;

    .line 88
    .line 89
    if-eqz p2, :cond_b

    .line 90
    .line 91
    const/16 p1, 0x14

    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_b
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/entity/TimeLineNewDocEntity;

    .line 95
    .line 96
    if-eqz p2, :cond_c

    .line 97
    .line 98
    const/16 p1, 0x15

    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_c
    instance-of p2, p1, Lcom/intsig/camscanner/util/MainMenuTipsChecker$CloudDiskRecEntity;

    .line 102
    .line 103
    if-eqz p2, :cond_d

    .line 104
    .line 105
    const/16 p1, 0x16

    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_d
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecEntity;

    .line 109
    .line 110
    if-eqz p2, :cond_e

    .line 111
    .line 112
    const/16 p1, 0x17

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_e
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/entity/MainShowMoreEntity;

    .line 116
    .line 117
    if-eqz p2, :cond_f

    .line 118
    .line 119
    const/16 p1, 0x18

    .line 120
    .line 121
    goto :goto_0

    .line 122
    :cond_f
    instance-of p1, p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocImportingProvider$DocImportEntity;

    .line 123
    .line 124
    if-eqz p1, :cond_1

    .line 125
    .line 126
    const/16 p1, 0x19

    .line 127
    .line 128
    :goto_0
    return p1
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8OO08o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final oo08OO〇0()V
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ne v0, v2, :cond_2

    .line 6
    .line 7
    iput v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 8
    .line 9
    iput-boolean v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇o0〇8:Z

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->〇〇0o(Z)V

    .line 17
    .line 18
    .line 19
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇OOo:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;

    .line 20
    .line 21
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;->〇oo〇(Z)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇0o()V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->o〇〇0〇()V

    .line 34
    .line 35
    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;->oO00OOO(Z)V

    .line 39
    .line 40
    .line 41
    goto :goto_2

    .line 42
    :cond_2
    iput v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/MainDocRecProvider;->oO00OOO(Z)V

    .line 47
    .line 48
    .line 49
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇o0〇8:Z

    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooO:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 54
    .line 55
    if-eqz v0, :cond_4

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 58
    .line 59
    if-nez v0, :cond_3

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_3
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->〇〇0o(Z)V

    .line 63
    .line 64
    .line 65
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇OOo:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;->〇oo〇(Z)V

    .line 68
    .line 69
    .line 70
    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0880()Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-nez v0, :cond_6

    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 77
    .line 78
    invoke-virtual {v0}, Ljava/util/AbstractMap;->size()I

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    if-lez v0, :cond_5

    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0o〇〇()Z

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-eqz v0, :cond_5

    .line 89
    .line 90
    goto :goto_3

    .line 91
    :cond_5
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 92
    .line 93
    .line 94
    goto :goto_4

    .line 95
    :cond_6
    :goto_3
    const/4 v0, 0x0

    .line 96
    invoke-static {p0, v1, v2, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇oOo〇()V

    .line 100
    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final oo0O〇0〇〇〇()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OOO()Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(Ljava/util/HashSet;)V
    .locals 1
    .param p1    # Ljava/util/HashSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "set"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, LO0〇oO〇o/o〇0;

    .line 7
    .line 8
    invoke-direct {v0, p0, p1}, LO0〇oO〇o/o〇0;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/util/HashSet;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇00O0O〇o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o〇88〇8:Lkotlinx/coroutines/Job;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-interface {v0}, Lkotlinx/coroutines/Job;->isActive()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-ne v0, v2, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    :cond_0
    if-eqz v1, :cond_1

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "docOpticalJob is active"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwnerLiveData()Landroidx/lifecycle/LiveData;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    if-nez v0, :cond_2

    .line 35
    .line 36
    return-void

    .line 37
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 38
    .line 39
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "mFragment.viewLifecycleOwner"

    .line 44
    .line 45
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    const/4 v3, 0x0

    .line 53
    const/4 v4, 0x0

    .line 54
    new-instance v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$updateDocOptical$1;

    .line 55
    .line 56
    const/4 v0, 0x0

    .line 57
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$updateDocOptical$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Lkotlin/coroutines/Continuation;)V

    .line 58
    .line 59
    .line 60
    const/4 v6, 0x3

    .line 61
    const/4 v7, 0x0

    .line 62
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o〇88〇8:Lkotlinx/coroutines/Job;

    .line 67
    .line 68
    return-void
.end method

.method public final o〇OOo000()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇00:Ljava/util/Set;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/app/ScenarioDBUtilKt;->〇o〇(Ljava/util/Set;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇008〇o0〇〇()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇080O0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 8
    .line 9
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.docpage.MainDocFragment"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo〇O〇o〇8()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇008〇oo()Lcom/intsig/camscanner/adapter/QueryInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇080O0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇00O0:Landroidx/fragment/app/Fragment;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 4
    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O8o8(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80〇808〇O()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 13
    .line 14
    const-string v0, "docItem unable select"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 21
    .line 22
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoO8OO〇:Ljava/util/LinkedHashMap;

    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/util/AbstractMap;->size()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-lez v0, :cond_1

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0o〇〇()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    const/4 p1, 0x1

    .line 40
    const/4 v0, 0x0

    .line 41
    const/4 v1, 0x0

    .line 42
    invoke-static {p0, v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_1
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->Oo8Oo00oo()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    add-int/2addr p1, v0

    .line 55
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇08〇0〇o〇8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇o()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0O00oO(Ljava/lang/Long;Landroidx/lifecycle/LifecycleCoroutineScope;)V
    .locals 9
    .param p2    # Landroidx/lifecycle/LifecycleCoroutineScope;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const-string v0, "lifecycleScope"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    const-wide/16 v2, -0x1

    .line 14
    .line 15
    cmp-long v4, v0, v2

    .line 16
    .line 17
    if-nez v4, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0:Lkotlinx/coroutines/Job;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    invoke-static {v0, v1, v2, v1}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    :cond_2
    const/4 v4, 0x0

    .line 33
    const/4 v5, 0x0

    .line 34
    new-instance v6, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$scrollNewESignDocToTopAndHighLight$1;

    .line 35
    .line 36
    invoke-direct {v6, p0, p1, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$scrollNewESignDocToTopAndHighLight$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Ljava/lang/Long;Lkotlin/coroutines/Continuation;)V

    .line 37
    .line 38
    .line 39
    const/4 v7, 0x3

    .line 40
    const/4 v8, 0x0

    .line 41
    move-object v3, p2

    .line 42
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0:Lkotlinx/coroutines/Job;

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇8O0O808〇(Ljava/lang/Long;)V
    .locals 8

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x0

    .line 11
    :goto_0
    if-ge v1, v0, :cond_2

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 14
    .line 15
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 22
    .line 23
    .line 24
    move-result-wide v3

    .line 25
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 26
    .line 27
    .line 28
    move-result-wide v5

    .line 29
    cmp-long v7, v3, v5

    .line 30
    .line 31
    if-nez v7, :cond_1

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 34
    .line 35
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooo0〇O88O(Ljava/lang/Long;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8O〇8oo08(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 39
    .line 40
    .line 41
    new-instance p1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$highlightItemByDocId$1;

    .line 42
    .line 43
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$highlightItemByDocId$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOoo80oO:Landroid/os/CountDownTimer;

    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_2
    :goto_1
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇8o(Lcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/datastruct/FolderItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "folderItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0O0o8:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;->O〇8O8〇008(Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->Oo8Oo00oo()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    add-int/2addr p1, v0

    .line 22
    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o88O〇8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v0, v1, :cond_3

    .line 9
    .line 10
    const/4 v1, 0x2

    .line 11
    if-eq v0, v1, :cond_2

    .line 12
    .line 13
    const/4 v1, 0x3

    .line 14
    if-eq v0, v1, :cond_1

    .line 15
    .line 16
    const/4 v1, 0x4

    .line 17
    if-eq v0, v1, :cond_0

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 35
    .line 36
    :goto_0
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇8o〇〇8080()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O80〇oOo(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "docs"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇0O〇O00O:Z

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {p0, v0, p1, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇OO0(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "folders"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 12
    .line 13
    check-cast p1, Ljava/util/Collection;

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x1

    .line 19
    const/4 v0, 0x0

    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-static {p0, v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0〇()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇o8OO0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;I)V
    .locals 3

    .line 1
    if-nez p3, :cond_0

    .line 2
    .line 3
    if-eqz p2, :cond_2

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/intsig/webstorage/WebStorageApi;->OO0o〇〇〇〇0(Lcom/intsig/webstorage/RemoteFile;)Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    if-eqz p1, :cond_2

    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 12
    .line 13
    check-cast p1, Ljava/util/Collection;

    .line 14
    .line 15
    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 16
    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    if-eqz p2, :cond_2

    .line 20
    .line 21
    invoke-virtual {p2, p1}, Lcom/intsig/webstorage/WebStorageApi;->OO0o〇〇〇〇0(Lcom/intsig/webstorage/RemoteFile;)Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_2

    .line 26
    .line 27
    check-cast p1, Ljava/lang/Iterable;

    .line 28
    .line 29
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_2

    .line 38
    .line 39
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    check-cast v0, Lcom/intsig/webstorage/RemoteFile;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/webstorage/RemoteFile;->oO80()Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    invoke-virtual {p0, v0, p2, p3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o8OO0(Lcom/intsig/webstorage/RemoteFile;Lcom/intsig/webstorage/WebStorageApi;I)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇80o:Ljava/util/List;

    .line 56
    .line 57
    const-string v2, "it"

    .line 58
    .line 59
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :catch_0
    move-exception p1

    .line 67
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 68
    .line 69
    new-instance p3, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v0, "getAllItems fail, error is "

    .line 75
    .line 76
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :cond_2
    :goto_1
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final 〇o8oO()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇080(Ljava/util/List;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇OO8Oo0〇()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lt v0, v1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Ooo8()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇oO8O0〇〇O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO〇8O8oOo:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇〇〇0o〇〇0:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇oOoo〇(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    new-instance v1, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 5
    .line 6
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;-><init>(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 7
    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    move-object v1, v0

    .line 11
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o880:Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-static {p0, p1, v1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o8〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ZILjava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public final 〇oo()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->ooo8o〇o〇()Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getDocRecEntity$1;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$getDocRecEntity$1;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecManager;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇Oo0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇800OO〇0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o〇o(Z)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OoOOo8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8〇o〇8()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o8〇(Z)V

    .line 22
    .line 23
    .line 24
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇〇00O〇0o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o880:Lcom/intsig/camscanner/datastruct/SearchOperationAdItem;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇0o〇o8(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇088O:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇0〇0o8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇0〇o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇8(Ljava/util/HashMap;)V
    .locals 1
    .param p1    # Ljava/util/HashMap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "foldersNumMap"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->OO〇OOo:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;->oo88o8O(Ljava/util/HashMap;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇o0o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇o08:Ljava/lang/Integer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x1

    .line 11
    if-eq v0, v1, :cond_1

    .line 12
    .line 13
    :goto_0
    return-void

    .line 14
    :cond_1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    instance-of v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    check-cast v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterGridLayoutManager;

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_2
    const/4 v0, 0x0

    .line 30
    :goto_1
    if-nez v0, :cond_3

    .line 31
    .line 32
    return-void

    .line 33
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->Oo08()I

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇O8〇8000:Ljava/lang/String;

    .line 42
    .line 43
    new-instance v3, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v4, "refreshGridLayout: new spanCount: "

    .line 49
    .line 50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanCount(I)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
.end method

.method public final 〇〇〇()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oOO0880O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

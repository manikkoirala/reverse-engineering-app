.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "DocItemProviderNew.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;,
        Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;,
        Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/DocMultiEntity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final Oo80:Landroidx/collection/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/collection/LruCache<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇08〇o0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O88O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O8o08O8O:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8o:Lcom/intsig/camscanner/tsapp/request/RequestTask;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8oOOo:Ljava/text/SimpleDateFormat;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

.field private final oOO〇〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final ooo0〇〇O:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇oO:Ljava/lang/Long;

.field private final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/datastruct/DocItem;

.field private final 〇O〇〇O8:Ljava/text/SimpleDateFormat;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇08〇o0O:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "DocItemProviderNew::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 21
    .line 22
    new-instance v0, Landroidx/collection/LruCache;

    .line 23
    .line 24
    const/16 v1, 0x40

    .line 25
    .line 26
    invoke-direct {v0, v1}, Landroidx/collection/LruCache;-><init>(I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo80:Landroidx/collection/LruCache;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docAdapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mContext"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "mDocItemProviderNewView"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;

    .line 24
    .line 25
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 26
    .line 27
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 28
    .line 29
    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 33
    .line 34
    new-instance p1, Ljava/util/LinkedHashSet;

    .line 35
    .line 36
    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 40
    .line 41
    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 42
    .line 43
    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    .line 44
    .line 45
    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 47
    .line 48
    new-instance p1, LOOo/OoO8;

    .line 49
    .line 50
    invoke-direct {p1, p0}, LOOo/OoO8;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 51
    .line 52
    .line 53
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooo0〇〇O:Landroid/view/View$OnClickListener;

    .line 54
    .line 55
    invoke-static {p2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0OO8(Landroid/content/Context;)I

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇08O:I

    .line 60
    .line 61
    new-instance p1, Ljava/util/LinkedHashSet;

    .line 62
    .line 63
    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 64
    .line 65
    .line 66
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0O:Ljava/util/Set;

    .line 67
    .line 68
    new-instance p1, Ljava/text/SimpleDateFormat;

    .line 69
    .line 70
    invoke-direct {p1}, Ljava/text/SimpleDateFormat;-><init>()V

    .line 71
    .line 72
    .line 73
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8oOOo:Ljava/text/SimpleDateFormat;

    .line 74
    .line 75
    new-instance p1, Ljava/text/SimpleDateFormat;

    .line 76
    .line 77
    const-string p2, "yyyy-MM-dd HH:mm"

    .line 78
    .line 79
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 80
    .line 81
    .line 82
    move-result-object p3

    .line 83
    invoke-direct {p1, p2, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 84
    .line 85
    .line 86
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇O〇〇O8:Ljava/text/SimpleDateFormat;

    .line 87
    .line 88
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 89
    .line 90
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$mRotateAnimation$2;->o0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$mRotateAnimation$2;

    .line 91
    .line 92
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇o0O:Lkotlin/Lazy;

    .line 97
    .line 98
    new-instance p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$glideRequestOptions$2;

    .line 99
    .line 100
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$glideRequestOptions$2;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 101
    .line 102
    .line 103
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O88O:Lkotlin/Lazy;

    .line 108
    .line 109
    new-instance p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$docThumbLoad$2;

    .line 110
    .line 111
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$docThumbLoad$2;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 112
    .line 113
    .line 114
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOO〇〇:Lkotlin/Lazy;

    .line 119
    .line 120
    new-instance p1, Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 121
    .line 122
    const/4 p2, 0x1

    .line 123
    const/4 p3, 0x0

    .line 124
    const-wide/16 v0, 0x0

    .line 125
    .line 126
    invoke-direct {p1, v0, v1, p2, p3}, Lcom/intsig/camscanner/tsapp/request/RequestTask;-><init>(JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 127
    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->o〇0()Ljava/util/concurrent/ExecutorService;

    .line 130
    .line 131
    .line 132
    move-result-object p2

    .line 133
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇00(Ljava/util/concurrent/ExecutorService;)V

    .line 134
    .line 135
    .line 136
    const/4 p2, 0x4

    .line 137
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->o〇O8〇〇o(I)V

    .line 138
    .line 139
    .line 140
    const/16 p2, 0x10

    .line 141
    .line 142
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇0000OOO(I)V

    .line 143
    .line 144
    .line 145
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8o:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 146
    .line 147
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    .line 148
    .line 149
    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 150
    .line 151
    .line 152
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "view"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    instance-of v0, p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object p1, v1

    .line 24
    :goto_0
    if-eqz p1, :cond_2

    .line 25
    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8oOo80()Landroidx/fragment/app/Fragment;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 35
    .line 36
    if-eqz v1, :cond_1

    .line 37
    .line 38
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O〇0()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    const-string v1, "file_more"

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇80o(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O〇08(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 52
    .line 53
    .line 54
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 55
    .line 56
    :cond_2
    if-nez v1, :cond_3

    .line 57
    .line 58
    sget-object p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 59
    .line 60
    const-string p1, "mMoreOpeClickListener v == null"

    .line 61
    .line 62
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O000()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OoO〇OOo8o()Lcom/intsig/camscanner/movecopyactivity/action/IAction;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    instance-of v0, v0, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;

    .line 14
    .line 15
    return v0

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOO〇〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0O8OO088(Lcom/intsig/camscanner/datastruct/DocItem;Landroid/widget/LinearLayout;Ljava/util/ArrayList;I)V
    .locals 20
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;I)V"
        }
    .end annotation

    .line 1
    move-object/from16 v9, p0

    .line 2
    .line 3
    move-object/from16 v10, p2

    .line 4
    .line 5
    move/from16 v11, p4

    .line 6
    .line 7
    if-nez v10, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/16 v12, 0x8

    .line 15
    .line 16
    const/4 v13, 0x0

    .line 17
    if-gtz v0, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    :goto_0
    const/4 v1, 0x4

    .line 21
    if-ge v0, v1, :cond_1

    .line 22
    .line 23
    iget-object v1, v9, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 24
    .line 25
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const v2, 0x7f0d0426

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, v2, v10, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v1, v12}, Landroid/view/View;->setVisibility(I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v10, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 40
    .line 41
    .line 42
    add-int/lit8 v0, v0, 0x1

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, v9, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o〇〇0〇88()Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iget-object v1, v9, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->O8(Landroid/content/Context;)I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->o〇0()I

    .line 58
    .line 59
    .line 60
    move-result v14

    .line 61
    const/high16 v0, 0x41000000    # 8.0f

    .line 62
    .line 63
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    add-int/lit8 v2, v14, -0x1

    .line 68
    .line 69
    mul-int v0, v0, v2

    .line 70
    .line 71
    sub-int/2addr v1, v0

    .line 72
    const/high16 v0, 0x41800000    # 16.0f

    .line 73
    .line 74
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    mul-int/lit8 v0, v0, 0x2

    .line 79
    .line 80
    sub-int/2addr v1, v0

    .line 81
    div-int v15, v1, v14

    .line 82
    .line 83
    if-eqz p3, :cond_2

    .line 84
    .line 85
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-nez v0, :cond_4

    .line 90
    .line 91
    :cond_2
    if-lez v11, :cond_4

    .line 92
    .line 93
    invoke-static {v14, v11}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    .line 98
    .line 99
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 100
    .line 101
    .line 102
    const/4 v2, 0x0

    .line 103
    :goto_1
    if-ge v2, v0, :cond_3

    .line 104
    .line 105
    const/4 v3, 0x0

    .line 106
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    .line 108
    .line 109
    add-int/lit8 v2, v2, 0x1

    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_3
    move-object/from16 v16, v1

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_4
    move-object/from16 v16, p3

    .line 116
    .line 117
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇00〇8(Ljava/lang/String;)Z

    .line 122
    .line 123
    .line 124
    move-result v0

    .line 125
    if-eqz v0, :cond_5

    .line 126
    .line 127
    invoke-virtual {v10, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    if-eqz v1, :cond_8

    .line 132
    .line 133
    const/4 v2, 0x0

    .line 134
    const/4 v7, 0x0

    .line 135
    const/4 v8, 0x0

    .line 136
    move-object/from16 v0, p0

    .line 137
    .line 138
    move-object/from16 v3, p1

    .line 139
    .line 140
    move/from16 v4, p4

    .line 141
    .line 142
    move v5, v14

    .line 143
    move v6, v15

    .line 144
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇o0O0O8(Landroid/view/View;ILcom/intsig/camscanner/datastruct/DocItem;IIILandroid/util/Pair;I)V

    .line 145
    .line 146
    .line 147
    goto :goto_5

    .line 148
    :cond_5
    if-eqz v16, :cond_8

    .line 149
    .line 150
    invoke-interface/range {v16 .. v16}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 151
    .line 152
    .line 153
    move-result-object v17

    .line 154
    const/4 v8, 0x0

    .line 155
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    .line 156
    .line 157
    .line 158
    move-result v0

    .line 159
    if-eqz v0, :cond_8

    .line 160
    .line 161
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    add-int/lit8 v13, v8, 0x1

    .line 166
    .line 167
    if-gez v8, :cond_6

    .line 168
    .line 169
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 170
    .line 171
    .line 172
    :cond_6
    move-object v7, v0

    .line 173
    check-cast v7, Landroid/util/Pair;

    .line 174
    .line 175
    invoke-virtual {v10, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    if-eqz v1, :cond_7

    .line 180
    .line 181
    const-string v0, "getChildAt(childIndex)"

    .line 182
    .line 183
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    .line 187
    .line 188
    .line 189
    move-result v18

    .line 190
    move-object/from16 v0, p0

    .line 191
    .line 192
    move v2, v8

    .line 193
    move-object/from16 v3, p1

    .line 194
    .line 195
    move/from16 v4, p4

    .line 196
    .line 197
    move v5, v14

    .line 198
    move v6, v15

    .line 199
    move/from16 v19, v8

    .line 200
    .line 201
    move/from16 v8, v18

    .line 202
    .line 203
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇o0O0O8(Landroid/view/View;ILcom/intsig/camscanner/datastruct/DocItem;IIILandroid/util/Pair;I)V

    .line 204
    .line 205
    .line 206
    goto :goto_4

    .line 207
    :cond_7
    move/from16 v19, v8

    .line 208
    .line 209
    :goto_4
    move v8, v13

    .line 210
    move/from16 v13, v19

    .line 211
    .line 212
    goto :goto_3

    .line 213
    :cond_8
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    .line 214
    .line 215
    .line 216
    move-result v0

    .line 217
    add-int/lit8 v0, v0, -0x1

    .line 218
    .line 219
    if-ge v13, v0, :cond_a

    .line 220
    .line 221
    add-int/lit8 v13, v13, 0x1

    .line 222
    .line 223
    invoke-virtual/range {p2 .. p2}, Landroid/view/ViewGroup;->getChildCount()I

    .line 224
    .line 225
    .line 226
    move-result v0

    .line 227
    :goto_6
    if-ge v13, v0, :cond_a

    .line 228
    .line 229
    invoke-virtual {v10, v13}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 230
    .line 231
    .line 232
    move-result-object v1

    .line 233
    if-eqz v1, :cond_9

    .line 234
    .line 235
    invoke-virtual {v1, v12}, Landroid/view/View;->setVisibility(I)V

    .line 236
    .line 237
    .line 238
    :cond_9
    add-int/lit8 v13, v13, 0x1

    .line 239
    .line 240
    goto :goto_6

    .line 241
    :cond_a
    return-void
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private final O0OO8〇0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇O〇80o08O()Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooo〇8oO(Lcom/intsig/camscanner/datastruct/DocItem;)I

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_4

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    if-ltz p2, :cond_0

    .line 25
    .line 26
    const/16 v1, 0x3e8

    .line 27
    .line 28
    if-ge p2, v1, :cond_0

    .line 29
    .line 30
    const/4 v0, 0x1

    .line 31
    :cond_0
    if-eqz v0, :cond_2

    .line 32
    .line 33
    if-nez p1, :cond_1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    if-nez p1, :cond_3

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_3
    const-string p2, "999+"

    .line 48
    .line 49
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_4
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 54
    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-eqz v1, :cond_6

    .line 60
    .line 61
    if-nez p1, :cond_5

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_5
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p2

    .line 68
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_6
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 73
    .line 74
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-nez p1, :cond_7

    .line 79
    .line 80
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 81
    .line 82
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    move-result p1

    .line 86
    if-nez p1, :cond_7

    .line 87
    .line 88
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 89
    .line 90
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    :cond_7
    :goto_0
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O0〇OO8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;ZLcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    const/4 v2, 0x1

    .line 4
    if-nez p2, :cond_0

    .line 5
    .line 6
    goto :goto_3

    .line 7
    :cond_0
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    if-eqz p2, :cond_1

    .line 12
    .line 13
    invoke-virtual {p2}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_title()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    goto :goto_0

    .line 18
    :cond_1
    move-object p2, v0

    .line 19
    :goto_0
    if-eqz p2, :cond_3

    .line 20
    .line 21
    invoke-static {p2}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    if-eqz p2, :cond_2

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_2
    const/4 p2, 0x0

    .line 29
    goto :goto_2

    .line 30
    :cond_3
    :goto_1
    const/4 p2, 0x1

    .line 31
    :goto_2
    if-nez p2, :cond_4

    .line 32
    .line 33
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    if-eqz p2, :cond_5

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_title()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    goto :goto_3

    .line 44
    :cond_4
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    if-nez p2, :cond_5

    .line 49
    .line 50
    invoke-direct {p0, p3, p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o88〇OO08〇(Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;)Z

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    if-nez p2, :cond_5

    .line 55
    .line 56
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    invoke-virtual {p2}, Ljava/util/HashMap;->isEmpty()Z

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    const/4 p4, 0x2

    .line 69
    if-eqz p2, :cond_7

    .line 70
    .line 71
    :cond_6
    :goto_4
    const/4 p2, 0x1

    .line 72
    goto :goto_8

    .line 73
    :cond_7
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->〇〇0o()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    if-eqz p2, :cond_9

    .line 78
    .line 79
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    if-nez v3, :cond_8

    .line 84
    .line 85
    goto :goto_5

    .line 86
    :cond_8
    const/4 v3, 0x0

    .line 87
    goto :goto_6

    .line 88
    :cond_9
    :goto_5
    const/4 v3, 0x1

    .line 89
    :goto_6
    if-nez v3, :cond_6

    .line 90
    .line 91
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 92
    .line 93
    .line 94
    move-result p2

    .line 95
    if-ne p2, v2, :cond_a

    .line 96
    .line 97
    goto :goto_4

    .line 98
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 99
    .line 100
    .line 101
    move-result-object p2

    .line 102
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 103
    .line 104
    .line 105
    move-result-wide v3

    .line 106
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    invoke-virtual {p2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    move-result-object p2

    .line 114
    check-cast p2, Ljava/lang/String;

    .line 115
    .line 116
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    const-string v4, "ACCESS_BY_PASSWORD"

    .line 121
    .line 122
    if-eqz v3, :cond_b

    .line 123
    .line 124
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 125
    .line 126
    .line 127
    move-result-object p2

    .line 128
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 129
    .line 130
    .line 131
    move-result-wide v5

    .line 132
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 133
    .line 134
    .line 135
    move-result-object p3

    .line 136
    invoke-interface {p2, p3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    .line 138
    .line 139
    :goto_7
    const/4 p2, 0x2

    .line 140
    goto :goto_8

    .line 141
    :cond_b
    const-string p3, "ACCESS_DIRECTLY"

    .line 142
    .line 143
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    move-result p3

    .line 147
    if-eqz p3, :cond_c

    .line 148
    .line 149
    goto :goto_4

    .line 150
    :cond_c
    invoke-static {v4, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 151
    .line 152
    .line 153
    move-result p2

    .line 154
    if-eqz p2, :cond_d

    .line 155
    .line 156
    goto :goto_7

    .line 157
    :cond_d
    const/4 p2, 0x0

    .line 158
    :goto_8
    const/16 p3, 0x8

    .line 159
    .line 160
    if-eqz p2, :cond_1d

    .line 161
    .line 162
    if-eq p2, v2, :cond_11

    .line 163
    .line 164
    if-eq p2, p4, :cond_e

    .line 165
    .line 166
    goto/16 :goto_e

    .line 167
    .line 168
    :cond_e
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇8oOO88()Landroid/widget/ImageView;

    .line 169
    .line 170
    .line 171
    move-result-object p2

    .line 172
    if-nez p2, :cond_f

    .line 173
    .line 174
    goto :goto_9

    .line 175
    :cond_f
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    .line 177
    .line 178
    :goto_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 179
    .line 180
    .line 181
    move-result-object p1

    .line 182
    if-nez p1, :cond_10

    .line 183
    .line 184
    goto/16 :goto_e

    .line 185
    .line 186
    :cond_10
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 187
    .line 188
    .line 189
    goto/16 :goto_e

    .line 190
    .line 191
    :cond_11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇8oOO88()Landroid/widget/ImageView;

    .line 192
    .line 193
    .line 194
    move-result-object p2

    .line 195
    if-nez p2, :cond_12

    .line 196
    .line 197
    goto :goto_a

    .line 198
    :cond_12
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 199
    .line 200
    .line 201
    :goto_a
    if-eqz v0, :cond_14

    .line 202
    .line 203
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 204
    .line 205
    .line 206
    move-result p2

    .line 207
    if-nez p2, :cond_13

    .line 208
    .line 209
    goto :goto_b

    .line 210
    :cond_13
    const/4 v2, 0x0

    .line 211
    :cond_14
    :goto_b
    if-eqz v2, :cond_16

    .line 212
    .line 213
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 214
    .line 215
    .line 216
    move-result-object p1

    .line 217
    if-nez p1, :cond_15

    .line 218
    .line 219
    goto :goto_e

    .line 220
    :cond_15
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 221
    .line 222
    .line 223
    goto :goto_e

    .line 224
    :cond_16
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 225
    .line 226
    .line 227
    move-result-object p2

    .line 228
    if-nez p2, :cond_17

    .line 229
    .line 230
    goto :goto_c

    .line 231
    :cond_17
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    .line 233
    .line 234
    :goto_c
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO8oO0o〇()Z

    .line 235
    .line 236
    .line 237
    move-result p2

    .line 238
    if-eqz p2, :cond_1b

    .line 239
    .line 240
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80()Z

    .line 241
    .line 242
    .line 243
    move-result p2

    .line 244
    if-eqz p2, :cond_19

    .line 245
    .line 246
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 247
    .line 248
    .line 249
    move-result-object p1

    .line 250
    if-nez p1, :cond_18

    .line 251
    .line 252
    goto :goto_e

    .line 253
    :cond_18
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    .line 255
    .line 256
    goto :goto_e

    .line 257
    :cond_19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 258
    .line 259
    .line 260
    move-result-object p1

    .line 261
    if-nez p1, :cond_1a

    .line 262
    .line 263
    goto :goto_e

    .line 264
    :cond_1a
    const-string p2, "**"

    .line 265
    .line 266
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    .line 268
    .line 269
    goto :goto_e

    .line 270
    :cond_1b
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 271
    .line 272
    .line 273
    move-result-object p1

    .line 274
    if-nez p1, :cond_1c

    .line 275
    .line 276
    goto :goto_e

    .line 277
    :cond_1c
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    .line 279
    .line 280
    goto :goto_e

    .line 281
    :cond_1d
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 282
    .line 283
    .line 284
    move-result-object p2

    .line 285
    if-nez p2, :cond_1e

    .line 286
    .line 287
    goto :goto_d

    .line 288
    :cond_1e
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 289
    .line 290
    .line 291
    :goto_d
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇8oOO88()Landroid/widget/ImageView;

    .line 292
    .line 293
    .line 294
    move-result-object p1

    .line 295
    if-nez p1, :cond_1f

    .line 296
    .line 297
    goto :goto_e

    .line 298
    :cond_1f
    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    .line 299
    .line 300
    .line 301
    :goto_e
    return-void
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private final O880oOO08(ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;)V
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0()Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 9
    .line 10
    .line 11
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 19
    .line 20
    .line 21
    :goto_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O000()Landroid/widget/TextView;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    if-nez p2, :cond_2

    .line 26
    .line 27
    goto :goto_2

    .line 28
    :cond_2
    invoke-virtual {p2, p1}, Landroid/view/View;->setClickable(Z)V

    .line 29
    .line 30
    .line 31
    :goto_2
    return-void
    .line 32
    .line 33
.end method

.method private final O8O〇(Landroid/util/Pair;Landroid/widget/ImageView;JLjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/widget/ImageView;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    if-eqz p1, :cond_1

    .line 5
    .line 6
    iget-object p1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 7
    .line 8
    check-cast p1, Ljava/lang/String;

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_1
    const/4 p1, 0x0

    .line 12
    :goto_0
    invoke-static {p5}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->O8(Ljava/lang/String;)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    const/4 v2, 0x0

    .line 23
    const/4 v3, 0x1

    .line 24
    if-eqz v1, :cond_3

    .line 25
    .line 26
    invoke-static {p5}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result p5

    .line 30
    if-eqz p5, :cond_2

    .line 31
    .line 32
    :goto_1
    const/4 v2, 0x1

    .line 33
    goto :goto_2

    .line 34
    :cond_2
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 35
    .line 36
    .line 37
    move-result p5

    .line 38
    if-nez p5, :cond_4

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_3
    invoke-static {p5}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    .line 42
    .line 43
    .line 44
    move-result p5

    .line 45
    if-nez p5, :cond_4

    .line 46
    .line 47
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result p5

    .line 51
    if-nez p5, :cond_4

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_4
    :goto_2
    if-eqz v0, :cond_d

    .line 55
    .line 56
    if-eqz v2, :cond_d

    .line 57
    .line 58
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    const/16 p3, 0x10

    .line 65
    .line 66
    const/16 p4, 0xc

    .line 67
    .line 68
    if-nez p1, :cond_8

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-eqz p1, :cond_5

    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    sget-object p5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 86
    .line 87
    invoke-static {p1, p5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-eqz p1, :cond_6

    .line 92
    .line 93
    invoke-static {p4}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    float-to-int p1, p1

    .line 98
    invoke-virtual {p2, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 99
    .line 100
    .line 101
    goto/16 :goto_4

    .line 102
    .line 103
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    sget-object p4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 110
    .line 111
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    if-eqz p1, :cond_7

    .line 116
    .line 117
    const/16 p1, 0x14

    .line 118
    .line 119
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    float-to-int p1, p1

    .line 124
    invoke-virtual {p2, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 125
    .line 126
    .line 127
    goto/16 :goto_4

    .line 128
    .line 129
    :cond_7
    invoke-static {p3}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 130
    .line 131
    .line 132
    move-result p1

    .line 133
    float-to-int p1, p1

    .line 134
    invoke-virtual {p2, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 135
    .line 136
    .line 137
    goto/16 :goto_4

    .line 138
    .line 139
    :cond_8
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 140
    .line 141
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 142
    .line 143
    .line 144
    move-result-object p1

    .line 145
    sget-object p5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 146
    .line 147
    invoke-static {p1, p5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    move-result p1

    .line 151
    if-eqz p1, :cond_9

    .line 152
    .line 153
    invoke-static {p4}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 154
    .line 155
    .line 156
    move-result p1

    .line 157
    float-to-int p1, p1

    .line 158
    invoke-virtual {p2, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 159
    .line 160
    .line 161
    goto/16 :goto_4

    .line 162
    .line 163
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    sget-object p4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 170
    .line 171
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 172
    .line 173
    .line 174
    move-result p1

    .line 175
    if-eqz p1, :cond_a

    .line 176
    .line 177
    const/16 p1, 0x17

    .line 178
    .line 179
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 180
    .line 181
    .line 182
    move-result p3

    .line 183
    float-to-int p3, p3

    .line 184
    const/16 p4, 0xd

    .line 185
    .line 186
    invoke-static {p4}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 187
    .line 188
    .line 189
    move-result p5

    .line 190
    float-to-int p5, p5

    .line 191
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 192
    .line 193
    .line 194
    move-result p1

    .line 195
    float-to-int p1, p1

    .line 196
    invoke-static {p4}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 197
    .line 198
    .line 199
    move-result p4

    .line 200
    float-to-int p4, p4

    .line 201
    invoke-virtual {p2, p3, p5, p1, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 202
    .line 203
    .line 204
    goto :goto_4

    .line 205
    :cond_a
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 206
    .line 207
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 208
    .line 209
    .line 210
    move-result-object p1

    .line 211
    sget-object p4, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 212
    .line 213
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 214
    .line 215
    .line 216
    move-result p1

    .line 217
    if-eqz p1, :cond_c

    .line 218
    .line 219
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 220
    .line 221
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 222
    .line 223
    .line 224
    move-result p1

    .line 225
    if-eqz p1, :cond_b

    .line 226
    .line 227
    const/4 p1, 0x6

    .line 228
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 229
    .line 230
    .line 231
    move-result p3

    .line 232
    float-to-int p3, p3

    .line 233
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 234
    .line 235
    .line 236
    move-result p4

    .line 237
    float-to-int p4, p4

    .line 238
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 239
    .line 240
    .line 241
    move-result p5

    .line 242
    float-to-int p5, p5

    .line 243
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 244
    .line 245
    .line 246
    move-result p1

    .line 247
    float-to-int p1, p1

    .line 248
    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 249
    .line 250
    .line 251
    goto :goto_4

    .line 252
    :cond_b
    const/16 p1, 0xa

    .line 253
    .line 254
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 255
    .line 256
    .line 257
    move-result p3

    .line 258
    float-to-int p3, p3

    .line 259
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 260
    .line 261
    .line 262
    move-result p4

    .line 263
    float-to-int p4, p4

    .line 264
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 265
    .line 266
    .line 267
    move-result p5

    .line 268
    float-to-int p5, p5

    .line 269
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 270
    .line 271
    .line 272
    move-result p1

    .line 273
    float-to-int p1, p1

    .line 274
    invoke-virtual {p2, p3, p4, p5, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 275
    .line 276
    .line 277
    goto :goto_4

    .line 278
    :cond_c
    invoke-static {p3}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 279
    .line 280
    .line 281
    move-result p1

    .line 282
    float-to-int p1, p1

    .line 283
    invoke-virtual {p2, p1, p1, p1, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 284
    .line 285
    .line 286
    :goto_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 287
    .line 288
    .line 289
    move-result p1

    .line 290
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 291
    .line 292
    .line 293
    return-void

    .line 294
    :cond_d
    invoke-static {v3}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 295
    .line 296
    .line 297
    move-result p5

    .line 298
    float-to-int p5, p5

    .line 299
    invoke-virtual {p2, p5, p5, p5, p5}, Landroid/view/View;->setPadding(IIII)V

    .line 300
    .line 301
    .line 302
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 303
    .line 304
    .line 305
    move-result-object p5

    .line 306
    invoke-static {p5}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 307
    .line 308
    .line 309
    move-result-object p5

    .line 310
    invoke-virtual {p5, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 311
    .line 312
    .line 313
    move-result-object p1

    .line 314
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O〇O〇oO()Lcom/bumptech/glide/request/RequestOptions;

    .line 315
    .line 316
    .line 317
    move-result-object p5

    .line 318
    invoke-virtual {p1, p5}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 319
    .line 320
    .line 321
    move-result-object p1

    .line 322
    const p5, 0x3f19999a    # 0.6f

    .line 323
    .line 324
    .line 325
    invoke-virtual {p1, p5}, Lcom/bumptech/glide/RequestBuilder;->O00(F)Lcom/bumptech/glide/RequestBuilder;

    .line 326
    .line 327
    .line 328
    move-result-object p1

    .line 329
    new-instance p5, Lcom/bumptech/glide/signature/ObjectKey;

    .line 330
    .line 331
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 332
    .line 333
    .line 334
    move-result-object p3

    .line 335
    invoke-direct {p5, p3}, Lcom/bumptech/glide/signature/ObjectKey;-><init>(Ljava/lang/Object;)V

    .line 336
    .line 337
    .line 338
    invoke-virtual {p1, p5}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 339
    .line 340
    .line 341
    move-result-object p1

    .line 342
    check-cast p1, Lcom/bumptech/glide/RequestBuilder;

    .line 343
    .line 344
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 345
    .line 346
    .line 347
    return-void
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private final O8O〇88oO0(Ljava/util/List;Lcom/intsig/view/TagLinearLayout;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/intsig/view/TagLinearLayout;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    const/high16 v0, 0x41400000    # 12.0f

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const/high16 v1, 0x41a00000    # 20.0f

    .line 25
    .line 26
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/high16 v2, 0x40000000    # 2.0f

    .line 31
    .line 32
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    const/high16 v4, 0x41000000    # 8.0f

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const/high16 v0, 0x41800000    # 16.0f

    .line 44
    .line 45
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    const/high16 v1, 0x41f00000    # 30.0f

    .line 50
    .line 51
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    const/high16 v2, 0x40800000    # 4.0f

    .line 56
    .line 57
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    const/high16 v4, 0x41200000    # 10.0f

    .line 66
    .line 67
    :goto_0
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 68
    .line 69
    const/4 v6, -0x2

    .line 70
    invoke-direct {v5, v6, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 71
    .line 72
    .line 73
    const/4 v6, 0x0

    .line 74
    invoke-virtual {v5, v6, v6, v3, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 75
    .line 76
    .line 77
    const/4 v3, 0x1

    .line 78
    if-eqz p1, :cond_1

    .line 79
    .line 80
    move-object v7, p1

    .line 81
    check-cast v7, Ljava/util/Collection;

    .line 82
    .line 83
    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    .line 84
    .line 85
    .line 86
    move-result v7

    .line 87
    xor-int/2addr v7, v3

    .line 88
    if-ne v7, v3, :cond_1

    .line 89
    .line 90
    const/4 v7, 0x1

    .line 91
    goto :goto_1

    .line 92
    :cond_1
    const/4 v7, 0x0

    .line 93
    :goto_1
    if-eqz v7, :cond_3

    .line 94
    .line 95
    check-cast p1, Ljava/lang/Iterable;

    .line 96
    .line 97
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 102
    .line 103
    .line 104
    move-result v7

    .line 105
    const v8, 0x7f060207

    .line 106
    .line 107
    .line 108
    if-eqz v7, :cond_2

    .line 109
    .line 110
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    move-result-object v7

    .line 114
    check-cast v7, Ljava/lang/CharSequence;

    .line 115
    .line 116
    iget-object v9, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 117
    .line 118
    const v10, 0x7f0d03dc

    .line 119
    .line 120
    .line 121
    const/4 v11, 0x0

    .line 122
    invoke-static {v9, v10, v11}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 123
    .line 124
    .line 125
    move-result-object v9

    .line 126
    const-string v10, "null cannot be cast to non-null type android.widget.TextView"

    .line 127
    .line 128
    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    check-cast v9, Landroid/widget/TextView;

    .line 132
    .line 133
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v9, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v9, v2, v6, v2, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 146
    .line 147
    .line 148
    const v7, 0x7f080215

    .line 149
    .line 150
    .line 151
    invoke-virtual {v9, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 152
    .line 153
    .line 154
    iget-object v7, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 155
    .line 156
    invoke-static {v7, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 157
    .line 158
    .line 159
    move-result v7

    .line 160
    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {p2, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 164
    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_2
    new-instance p1, Landroid/widget/TextView;

    .line 168
    .line 169
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 170
    .line 171
    invoke-direct {p1, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 172
    .line 173
    .line 174
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 175
    .line 176
    invoke-direct {v1, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 177
    .line 178
    .line 179
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    .line 181
    .line 182
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 183
    .line 184
    .line 185
    const/16 v0, 0x11

    .line 186
    .line 187
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 188
    .line 189
    .line 190
    const-string v0, "..."

    .line 191
    .line 192
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    .line 194
    .line 195
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 196
    .line 197
    invoke-static {v0, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 198
    .line 199
    .line 200
    move-result v0

    .line 201
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 202
    .line 203
    .line 204
    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    .line 205
    .line 206
    .line 207
    move-result-object v0

    .line 208
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 209
    .line 210
    .line 211
    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 212
    .line 213
    .line 214
    :cond_3
    return-void
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static final synthetic O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Ljava/util/Map;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇〇0〇〇0(Ljava/util/Map;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO8oO0o〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇8oOo8O()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/16 v2, 0x69

    .line 15
    .line 16
    if-ne v0, v2, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    return v1
    .line 20
    .line 21
.end method

.method private final OOO8o〇〇(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    instance-of v0, p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    if-eqz p1, :cond_1

    .line 14
    .line 15
    new-instance v0, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 21
    .line 22
    .line 23
    move-result-wide v1

    .line 24
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 32
    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;->〇o〇(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic OOO〇O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OOo0O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_9

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇8oOo8O()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_9

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    goto/16 :goto_0

    .line 30
    .line 31
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 32
    .line 33
    const-string v1, "showCardBag"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const/4 v2, 0x0

    .line 47
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8〇o()Landroid/widget/ImageView;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    if-eqz v1, :cond_1

    .line 55
    .line 56
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 57
    .line 58
    .line 59
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO00OOO()Landroid/widget/TextView;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    if-eqz v1, :cond_2

    .line 64
    .line 65
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 66
    .line 67
    .line 68
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇O()Landroid/widget/TextView;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    if-eqz v1, :cond_3

    .line 73
    .line 74
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 75
    .line 76
    .line 77
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    invoke-static {v1}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->oO80(I)Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO8oO0o〇()Z

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    const/4 v4, 0x1

    .line 90
    if-eqz v3, :cond_4

    .line 91
    .line 92
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->Oooo8o0〇()I

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    if-eq v3, v4, :cond_4

    .line 97
    .line 98
    if-eqz v1, :cond_4

    .line 99
    .line 100
    const/4 v2, 0x1

    .line 101
    :cond_4
    invoke-direct {p0, p1, v2, p2, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0〇OO8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;ZLcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;)V

    .line 102
    .line 103
    .line 104
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO8oO0o〇()Z

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    if-nez v2, :cond_5

    .line 109
    .line 110
    const-string p1, "is not card bag"

    .line 111
    .line 112
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    return-void

    .line 116
    :cond_5
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->Oooo8o0〇()I

    .line 117
    .line 118
    .line 119
    move-result p2

    .line 120
    if-ne p2, v4, :cond_7

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO00OOO()Landroid/widget/TextView;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    if-eqz p1, :cond_6

    .line 127
    .line 128
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 129
    .line 130
    .line 131
    :cond_6
    return-void

    .line 132
    :cond_7
    if-nez v1, :cond_8

    .line 133
    .line 134
    return-void

    .line 135
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 136
    .line 137
    .line 138
    move-result-object p2

    .line 139
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 140
    .line 141
    .line 142
    move-result-object p2

    .line 143
    invoke-virtual {p2, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNameId()I

    .line 155
    .line 156
    .line 157
    move-result v2

    .line 158
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8〇o()Landroid/widget/ImageView;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    if-eqz p1, :cond_9

    .line 170
    .line 171
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getIconId()I

    .line 175
    .line 176
    .line 177
    move-result p2

    .line 178
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 179
    .line 180
    .line 181
    :cond_9
    :goto_0
    return-void
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V
    .locals 7

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 8
    .line 9
    .line 10
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    if-nez p2, :cond_2

    .line 17
    .line 18
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 19
    .line 20
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    if-eqz p2, :cond_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/high16 p2, 0x40000000    # 2.0f

    .line 28
    .line 29
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    const/4 v2, 0x0

    .line 34
    const/4 v3, 0x0

    .line 35
    const/4 v4, 0x0

    .line 36
    const/16 v5, 0xe

    .line 37
    .line 38
    const/4 v6, 0x0

    .line 39
    move-object v0, p1

    .line 40
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    if-eqz p3, :cond_1

    .line 44
    .line 45
    invoke-virtual {p3}, Ljava/lang/Number;->floatValue()F

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 50
    .line 51
    .line 52
    move-result p3

    .line 53
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    invoke-static {p1, p3, p2}, Lcom/intsig/camscanner/util/ViewExtKt;->OoO8(Landroid/view/View;II)V

    .line 58
    .line 59
    .line 60
    :cond_1
    const/high16 p2, -0x40800000    # -1.0f

    .line 61
    .line 62
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    invoke-virtual {p1, p2, p2, p2, p2}, Landroid/view/View;->setPadding(IIII)V

    .line 67
    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_2
    :goto_0
    const/high16 p2, 0x40800000    # 4.0f

    .line 71
    .line 72
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    const/4 v3, 0x0

    .line 81
    const/4 v4, 0x0

    .line 82
    const/16 v5, 0xc

    .line 83
    .line 84
    const/4 v6, 0x0

    .line 85
    move-object v0, p1

    .line 86
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 87
    .line 88
    .line 89
    if-eqz p3, :cond_3

    .line 90
    .line 91
    invoke-virtual {p3}, Ljava/lang/Number;->floatValue()F

    .line 92
    .line 93
    .line 94
    move-result p2

    .line 95
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 96
    .line 97
    .line 98
    move-result p3

    .line 99
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 100
    .line 101
    .line 102
    move-result p2

    .line 103
    invoke-static {p1, p3, p2}, Lcom/intsig/camscanner/util/ViewExtKt;->OoO8(Landroid/view/View;II)V

    .line 104
    .line 105
    .line 106
    :cond_3
    :goto_1
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final Oo8Oo00oo(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 16

    .line 1
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇00〇8(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    const/4 v3, 0x1

    .line 11
    if-eqz v1, :cond_4

    .line 12
    .line 13
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8()I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇O〇80o08O()Landroid/widget/TextView;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    if-eqz v4, :cond_1

    .line 27
    .line 28
    xor-int/lit8 v5, v1, 0x1

    .line 29
    .line 30
    invoke-static {v4, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 31
    .line 32
    .line 33
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o()Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    if-eqz v4, :cond_2

    .line 38
    .line 39
    xor-int/lit8 v5, v1, 0x1

    .line 40
    .line 41
    invoke-static {v4, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇O〇80o08O()Landroid/widget/TextView;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    if-nez v4, :cond_3

    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8()I

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v5

    .line 59
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    .line 61
    .line 62
    :goto_1
    sget-object v4, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 63
    .line 64
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 65
    .line 66
    .line 67
    move-result v4

    .line 68
    if-eqz v4, :cond_4

    .line 69
    .line 70
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8O〇()Landroid/widget/TextView;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    if-eqz v4, :cond_4

    .line 75
    .line 76
    xor-int/2addr v1, v3

    .line 77
    invoke-static {v4, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 78
    .line 79
    .line 80
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-nez v1, :cond_5

    .line 85
    .line 86
    return-void

    .line 87
    :cond_5
    move-object/from16 v1, p0

    .line 88
    .line 89
    iget-object v4, v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 90
    .line 91
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 92
    .line 93
    .line 94
    move-result-object v4

    .line 95
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 96
    .line 97
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    if-eqz v5, :cond_8

    .line 102
    .line 103
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    const/high16 v2, 0x41a00000    # 20.0f

    .line 112
    .line 113
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 114
    .line 115
    .line 116
    move-result v2

    .line 117
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 118
    .line 119
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 124
    .line 125
    .line 126
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    const/high16 v2, 0x41400000    # 12.0f

    .line 131
    .line 132
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 133
    .line 134
    .line 135
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o88〇OO08〇()Landroid/widget/TextView;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    if-nez v0, :cond_6

    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_6
    const/high16 v2, 0x41200000    # 10.0f

    .line 143
    .line 144
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 145
    .line 146
    .line 147
    :goto_2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 148
    .line 149
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    if-nez v0, :cond_7

    .line 154
    .line 155
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o()Landroid/view/View;

    .line 156
    .line 157
    .line 158
    move-result-object v2

    .line 159
    if-eqz v2, :cond_11

    .line 160
    .line 161
    const/4 v3, 0x0

    .line 162
    const/high16 v0, 0x41b00000    # 22.0f

    .line 163
    .line 164
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 165
    .line 166
    .line 167
    move-result v4

    .line 168
    const/4 v5, 0x0

    .line 169
    const/4 v6, 0x0

    .line 170
    const/16 v7, 0xd

    .line 171
    .line 172
    const/4 v8, 0x0

    .line 173
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 174
    .line 175
    .line 176
    goto/16 :goto_4

    .line 177
    .line 178
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o88〇OO08〇()Landroid/widget/TextView;

    .line 179
    .line 180
    .line 181
    move-result-object v9

    .line 182
    if-eqz v9, :cond_11

    .line 183
    .line 184
    const/4 v10, 0x0

    .line 185
    const/4 v11, 0x0

    .line 186
    const/4 v12, 0x0

    .line 187
    const/4 v13, 0x0

    .line 188
    const/16 v14, 0xd

    .line 189
    .line 190
    const/4 v15, 0x0

    .line 191
    invoke-static/range {v9 .. v15}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 192
    .line 193
    .line 194
    goto/16 :goto_4

    .line 195
    .line 196
    :cond_8
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 197
    .line 198
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 199
    .line 200
    .line 201
    move-result v5

    .line 202
    if-eqz v5, :cond_a

    .line 203
    .line 204
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇00〇8(Ljava/lang/String;)Z

    .line 205
    .line 206
    .line 207
    move-result v2

    .line 208
    if-eqz v2, :cond_11

    .line 209
    .line 210
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v2

    .line 214
    invoke-static {v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O〇8O8〇008(Ljava/lang/String;)Z

    .line 215
    .line 216
    .line 217
    move-result v2

    .line 218
    if-eqz v2, :cond_9

    .line 219
    .line 220
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    goto :goto_3

    .line 225
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 226
    .line 227
    .line 228
    move-result-object v2

    .line 229
    new-instance v3, Ljava/lang/StringBuilder;

    .line 230
    .line 231
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 232
    .line 233
    .line 234
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    const-string v2, "."

    .line 238
    .line 239
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    .line 241
    .line 242
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 246
    .line 247
    .line 248
    move-result-object v0

    .line 249
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 250
    .line 251
    .line 252
    move-result-object v2

    .line 253
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    .line 255
    .line 256
    goto :goto_4

    .line 257
    :cond_a
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 258
    .line 259
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 260
    .line 261
    .line 262
    move-result v4

    .line 263
    if-eqz v4, :cond_11

    .line 264
    .line 265
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->O8(Ljava/lang/String;)Ljava/lang/Integer;

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    if-eqz v0, :cond_f

    .line 270
    .line 271
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇0〇o〇O()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 272
    .line 273
    .line 274
    move-result-object v4

    .line 275
    if-eqz v4, :cond_b

    .line 276
    .line 277
    invoke-static {v4, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 278
    .line 279
    .line 280
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇O〇oO()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 281
    .line 282
    .line 283
    move-result-object v2

    .line 284
    if-eqz v2, :cond_c

    .line 285
    .line 286
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 287
    .line 288
    .line 289
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇8()Landroidx/appcompat/widget/AppCompatImageView;

    .line 290
    .line 291
    .line 292
    move-result-object v2

    .line 293
    if-eqz v2, :cond_d

    .line 294
    .line 295
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 296
    .line 297
    .line 298
    move-result v0

    .line 299
    invoke-virtual {v2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 300
    .line 301
    .line 302
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0o〇〇Oo()Landroidx/appcompat/widget/AppCompatTextView;

    .line 303
    .line 304
    .line 305
    move-result-object v0

    .line 306
    if-nez v0, :cond_e

    .line 307
    .line 308
    goto :goto_4

    .line 309
    :cond_e
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object v2

    .line 313
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    .line 315
    .line 316
    goto :goto_4

    .line 317
    :cond_f
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇0〇o〇O()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 318
    .line 319
    .line 320
    move-result-object v0

    .line 321
    if-eqz v0, :cond_10

    .line 322
    .line 323
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 324
    .line 325
    .line 326
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇O〇oO()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 327
    .line 328
    .line 329
    move-result-object v0

    .line 330
    if-eqz v0, :cond_11

    .line 331
    .line 332
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 333
    .line 334
    .line 335
    :cond_11
    :goto_4
    return-void
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private static final Ooo8〇〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0O〇Oo(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo〇O(Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/view/NinePhotoView;Ljava/util/ArrayList;I)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            "Lcom/intsig/view/NinePhotoView;",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;I)V"
        }
    .end annotation

    .line 1
    if-eqz p3, :cond_0

    .line 2
    .line 3
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_2

    .line 8
    .line 9
    :cond_0
    if-lez p4, :cond_2

    .line 10
    .line 11
    const/16 p3, 0x9

    .line 12
    .line 13
    invoke-static {p3, p4}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 14
    .line 15
    .line 16
    move-result p3

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    .line 20
    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    :goto_0
    if-ge v1, p3, :cond_1

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    add-int/lit8 v1, v1, 0x1

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    move-object v6, v0

    .line 33
    goto :goto_1

    .line 34
    :cond_2
    move-object v6, p3

    .line 35
    :goto_1
    new-instance p3, LOOo/o800o8O;

    .line 36
    .line 37
    move-object v2, p3

    .line 38
    move-object v3, p2

    .line 39
    move v4, p4

    .line 40
    move-object v5, p1

    .line 41
    move-object v7, p0

    .line 42
    invoke-direct/range {v2 .. v7}, LOOo/o800o8O;-><init>(Lcom/intsig/view/NinePhotoView;ILcom/intsig/camscanner/datastruct/DocItem;Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p2, p3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final Oo〇O8o〇8(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;I)V
    .locals 5
    .param p3    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 17
    .line 18
    .line 19
    goto/16 :goto_3

    .line 20
    .line 21
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇〇0o()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v3, 0x1

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    if-nez v4, :cond_1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 v4, 0x0

    .line 36
    goto :goto_1

    .line 37
    :cond_2
    :goto_0
    const/4 v4, 0x1

    .line 38
    :goto_1
    if-nez v4, :cond_6

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-ne v0, v3, :cond_3

    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 52
    .line 53
    .line 54
    move-result-wide v3

    .line 55
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    check-cast v0, Ljava/lang/String;

    .line 64
    .line 65
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    const v3, 0x7f0810fd

    .line 70
    .line 71
    .line 72
    const-string v4, "ACCESS_BY_PASSWORD"

    .line 73
    .line 74
    if-eqz v1, :cond_4

    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO00OOO()Ljava/util/HashMap;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 81
    .line 82
    .line 83
    move-result-wide v1

    .line 84
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 85
    .line 86
    .line 87
    move-result-object p2

    .line 88
    invoke-interface {v0, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 95
    .line 96
    .line 97
    goto :goto_3

    .line 98
    :cond_4
    const-string p2, "ACCESS_DIRECTLY"

    .line 99
    .line 100
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    if-eqz p2, :cond_5

    .line 105
    .line 106
    const p2, 0x7f080cd4

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 113
    .line 114
    .line 115
    goto :goto_3

    .line 116
    :cond_5
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    move-result p2

    .line 120
    if-eqz p2, :cond_7

    .line 121
    .line 122
    invoke-virtual {p1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 126
    .line 127
    .line 128
    goto :goto_3

    .line 129
    :cond_6
    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    .line 131
    .line 132
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 133
    .line 134
    .line 135
    :cond_7
    :goto_3
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static synthetic Oo〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;ILjava/lang/Float;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final O〇08(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showMorePopMenu"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/mainmenu/folder/timeline/TimeLineDocMoreDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/folder/timeline/TimeLineDocMoreDialog$Companion;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇8oOo8O()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/mainmenu/folder/timeline/TimeLineDocMoreDialog$Companion;->〇080(Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/datastruct/FolderItem;)Lcom/intsig/camscanner/mainmenu/folder/timeline/TimeLineDocMoreDialog;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 21
    .line 22
    instance-of v1, v0, Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    check-cast v0, Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    move-object v0, v2

    .line 31
    :goto_0
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    :cond_1
    if-eqz v2, :cond_2

    .line 44
    .line 45
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v2, p1, v0}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 54
    .line 55
    .line 56
    :cond_2
    if-eqz v2, :cond_3

    .line 57
    .line 58
    invoke-virtual {v2}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 59
    .line 60
    .line 61
    :cond_3
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Ooo8〇〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇OO(Landroid/content/Context;Lcom/intsig/camscanner/adapter/QueryInterface;Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;)V
    .locals 7

    .line 1
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 9
    .line 10
    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/searchactivity/SearchUtil;->〇080:Lcom/intsig/camscanner/searchactivity/SearchUtil;

    .line 12
    .line 13
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 14
    .line 15
    .line 16
    move-result-wide v2

    .line 17
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-interface {p2}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    invoke-virtual {v0, p1, v2, p3, p2}, Lcom/intsig/camscanner/searchactivity/SearchUtil;->o〇0(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;[Ljava/lang/String;)Landroid/util/SparseArray;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    .line 34
    .line 35
    .line 36
    move-result p3

    .line 37
    const/4 v0, 0x0

    .line 38
    :goto_0
    if-ge v0, p3, :cond_10

    .line 39
    .line 40
    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-virtual {p2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    check-cast v3, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;

    .line 49
    .line 50
    if-eqz v2, :cond_e

    .line 51
    .line 52
    const-string v4, " "

    .line 53
    .line 54
    const/4 v5, 0x1

    .line 55
    if-eq v2, v5, :cond_a

    .line 56
    .line 57
    const/4 v6, 0x2

    .line 58
    if-eq v2, v6, :cond_6

    .line 59
    .line 60
    const/4 v4, 0x3

    .line 61
    if-eq v2, v4, :cond_2

    .line 62
    .line 63
    const/4 v4, 0x4

    .line 64
    if-eq v2, v4, :cond_1

    .line 65
    .line 66
    goto/16 :goto_4

    .line 67
    .line 68
    :cond_1
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oo〇()Lcom/intsig/view/TagLinearLayout;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    if-eqz v2, :cond_f

    .line 73
    .line 74
    invoke-virtual {v3}, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;->〇o00〇〇Oo()Ljava/util/List;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    invoke-direct {p0, v3, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8O〇88oO0(Ljava/util/List;Lcom/intsig/view/TagLinearLayout;)V

    .line 79
    .line 80
    .line 81
    goto/16 :goto_4

    .line 82
    .line 83
    :cond_2
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    if-eqz v2, :cond_3

    .line 88
    .line 89
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 90
    .line 91
    .line 92
    :cond_3
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO8oO0o〇()Landroid/widget/TextView;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    if-nez v2, :cond_4

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_4
    invoke-virtual {v3}, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;->〇080()Ljava/lang/CharSequence;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    .line 105
    .line 106
    :goto_1
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0O0()Landroid/widget/TextView;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    if-eqz v2, :cond_5

    .line 111
    .line 112
    invoke-static {v2, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 113
    .line 114
    .line 115
    :cond_5
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8ooOoo〇()Landroid/widget/ImageView;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    if-eqz v2, :cond_f

    .line 120
    .line 121
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 122
    .line 123
    .line 124
    goto/16 :goto_4

    .line 125
    .line 126
    :cond_6
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    if-eqz v2, :cond_7

    .line 131
    .line 132
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 133
    .line 134
    .line 135
    :cond_7
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO8oO0o〇()Landroid/widget/TextView;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    if-nez v2, :cond_8

    .line 140
    .line 141
    goto :goto_2

    .line 142
    :cond_8
    invoke-virtual {v3}, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;->〇080()Ljava/lang/CharSequence;

    .line 143
    .line 144
    .line 145
    move-result-object v3

    .line 146
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    .line 148
    .line 149
    :goto_2
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0O0()Landroid/widget/TextView;

    .line 150
    .line 151
    .line 152
    move-result-object v2

    .line 153
    if-eqz v2, :cond_9

    .line 154
    .line 155
    const v3, 0x7f131c52

    .line 156
    .line 157
    .line 158
    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    new-instance v6, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v3

    .line 180
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    .line 182
    .line 183
    const-string v3, "#19BC51"

    .line 184
    .line 185
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 186
    .line 187
    .line 188
    move-result v3

    .line 189
    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 194
    .line 195
    .line 196
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 197
    .line 198
    .line 199
    :cond_9
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8ooOoo〇()Landroid/widget/ImageView;

    .line 200
    .line 201
    .line 202
    move-result-object v2

    .line 203
    if-eqz v2, :cond_f

    .line 204
    .line 205
    invoke-static {v2, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 206
    .line 207
    .line 208
    goto :goto_4

    .line 209
    :cond_a
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 210
    .line 211
    .line 212
    move-result-object v2

    .line 213
    if-eqz v2, :cond_b

    .line 214
    .line 215
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 216
    .line 217
    .line 218
    :cond_b
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO8oO0o〇()Landroid/widget/TextView;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    if-nez v2, :cond_c

    .line 223
    .line 224
    goto :goto_3

    .line 225
    :cond_c
    invoke-virtual {v3}, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;->〇080()Ljava/lang/CharSequence;

    .line 226
    .line 227
    .line 228
    move-result-object v3

    .line 229
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    .line 231
    .line 232
    :goto_3
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0O0()Landroid/widget/TextView;

    .line 233
    .line 234
    .line 235
    move-result-object v2

    .line 236
    if-eqz v2, :cond_d

    .line 237
    .line 238
    const v3, 0x7f130aa9

    .line 239
    .line 240
    .line 241
    invoke-virtual {p1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    .line 242
    .line 243
    .line 244
    move-result-object v3

    .line 245
    new-instance v6, Ljava/lang/StringBuilder;

    .line 246
    .line 247
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .line 249
    .line 250
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v3

    .line 263
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    .line 265
    .line 266
    const-string v3, "#4580F2"

    .line 267
    .line 268
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 269
    .line 270
    .line 271
    move-result v3

    .line 272
    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 273
    .line 274
    .line 275
    move-result-object v3

    .line 276
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 277
    .line 278
    .line 279
    invoke-static {v2, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 280
    .line 281
    .line 282
    :cond_d
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8ooOoo〇()Landroid/widget/ImageView;

    .line 283
    .line 284
    .line 285
    move-result-object v2

    .line 286
    if-eqz v2, :cond_f

    .line 287
    .line 288
    invoke-static {v2, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 289
    .line 290
    .line 291
    goto :goto_4

    .line 292
    :cond_e
    invoke-virtual {p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 293
    .line 294
    .line 295
    move-result-object v2

    .line 296
    invoke-virtual {v3}, Lcom/intsig/camscanner/searchactivity/SearchUtil$SearchHighlightEntity;->〇080()Ljava/lang/CharSequence;

    .line 297
    .line 298
    .line 299
    move-result-object v3

    .line 300
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    .line 302
    .line 303
    :cond_f
    :goto_4
    add-int/lit8 v0, v0, 0x1

    .line 304
    .line 305
    goto/16 :goto_0

    .line 306
    .line 307
    :cond_10
    return-void
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method private final O〇Oooo〇〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇0〇O0088o()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    xor-int/lit8 v0, v0, 0x1

    .line 25
    .line 26
    const/16 v1, 0x8

    .line 27
    .line 28
    if-eqz v0, :cond_4

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇80()Landroid/widget/TextView;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    if-nez v2, :cond_1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇0〇O0088o()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇80()Landroid/widget/TextView;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    const/4 v2, 0x0

    .line 49
    if-nez p2, :cond_2

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_2
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 53
    .line 54
    .line 55
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00〇8()Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    if-nez p2, :cond_3

    .line 60
    .line 61
    goto :goto_3

    .line 62
    :cond_3
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇80()Landroid/widget/TextView;

    .line 67
    .line 68
    .line 69
    move-result-object p2

    .line 70
    if-nez p2, :cond_5

    .line 71
    .line 72
    goto :goto_2

    .line 73
    :cond_5
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 74
    .line 75
    .line 76
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00〇8()Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    if-nez p2, :cond_6

    .line 81
    .line 82
    goto :goto_3

    .line 83
    :cond_6
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    :goto_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 91
    .line 92
    .line 93
    move-result-object p2

    .line 94
    instance-of v2, p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 95
    .line 96
    const/16 v3, 0xc

    .line 97
    .line 98
    if-eqz v2, :cond_8

    .line 99
    .line 100
    check-cast p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 101
    .line 102
    if-eqz v0, :cond_7

    .line 103
    .line 104
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 105
    .line 106
    const/4 v4, 0x2

    .line 107
    invoke-static {v2, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    goto :goto_4

    .line 112
    :cond_7
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 113
    .line 114
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    :goto_4
    iput v2, p2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 119
    .line 120
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OOO()Landroid/view/View;

    .line 121
    .line 122
    .line 123
    move-result-object p2

    .line 124
    const/4 v2, 0x0

    .line 125
    if-eqz p2, :cond_9

    .line 126
    .line 127
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 128
    .line 129
    .line 130
    move-result-object p2

    .line 131
    goto :goto_5

    .line 132
    :cond_9
    move-object p2, v2

    .line 133
    :goto_5
    instance-of v4, p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 134
    .line 135
    const/4 v5, 0x5

    .line 136
    if-eqz v4, :cond_b

    .line 137
    .line 138
    check-cast p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 139
    .line 140
    if-eqz v0, :cond_a

    .line 141
    .line 142
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 143
    .line 144
    invoke-static {v1, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 145
    .line 146
    .line 147
    move-result v1

    .line 148
    goto :goto_6

    .line 149
    :cond_a
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 150
    .line 151
    invoke-static {v4, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 152
    .line 153
    .line 154
    move-result v1

    .line 155
    :goto_6
    iput v1, p2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 156
    .line 157
    :cond_b
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o()Landroid/view/View;

    .line 158
    .line 159
    .line 160
    move-result-object p1

    .line 161
    if-eqz p1, :cond_c

    .line 162
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    :cond_c
    instance-of p1, v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 168
    .line 169
    if-eqz p1, :cond_e

    .line 170
    .line 171
    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 172
    .line 173
    if-eqz v0, :cond_d

    .line 174
    .line 175
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 176
    .line 177
    invoke-static {p1, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 178
    .line 179
    .line 180
    move-result p1

    .line 181
    goto :goto_7

    .line 182
    :cond_d
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 183
    .line 184
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 185
    .line 186
    .line 187
    move-result p1

    .line 188
    :goto_7
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 189
    .line 190
    :cond_e
    return-void
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final O〇O〇oO()Lcom/bumptech/glide/request/RequestOptions;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O88O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o0O0(Lcom/intsig/camscanner/datastruct/DocItem;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇oO:Ljava/lang/Long;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇oO:Ljava/lang/Long;

    .line 12
    .line 13
    if-nez p1, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 17
    .line 18
    .line 19
    move-result-wide v4

    .line 20
    cmp-long p1, v2, v4

    .line 21
    .line 22
    if-nez p1, :cond_2

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    :cond_2
    :goto_0
    return v1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o0ooO(ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .line 1
    const/4 p3, 0x1

    .line 2
    const/4 v0, 0x4

    .line 3
    if-eqz p1, :cond_2

    .line 4
    .line 5
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇0o()Landroid/view/ViewGroup;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/16 v2, 0x8

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o0O0O8()Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    if-nez v1, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0()Landroid/widget/TextView;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const v2, 0x7f060208

    .line 33
    .line 34
    .line 35
    if-eqz v1, :cond_3

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-static {v3, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 46
    .line 47
    .line 48
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    if-eqz v1, :cond_4

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    invoke-static {v3, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    .line 64
    .line 65
    :cond_4
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O000()Landroid/widget/TextView;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    if-eqz v1, :cond_5

    .line 70
    .line 71
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    invoke-static {v3, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    .line 81
    .line 82
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 83
    .line 84
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o〇〇8080()Ljava/util/List;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-ne v1, p3, :cond_7

    .line 93
    .line 94
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o0O0O8()Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    if-nez v1, :cond_6

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_6
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 102
    .line 103
    .line 104
    goto :goto_1

    .line 105
    :cond_7
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o0O0O8()Landroid/view/View;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    if-nez v0, :cond_8

    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_8
    const/4 v1, 0x0

    .line 113
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 114
    .line 115
    .line 116
    :goto_1
    xor-int/2addr p1, p3

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O880oOO08(ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇00O0O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o80ooO(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Ljava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o88〇OO08〇()Landroid/widget/TextView;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 23
    .line 24
    const/4 p2, 0x4

    .line 25
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    const/4 v4, 0x0

    .line 30
    const/4 v5, 0x0

    .line 31
    const/16 v6, 0xd

    .line 32
    .line 33
    const/4 v7, 0x0

    .line 34
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    return-void

    .line 38
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 45
    .line 46
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-nez v0, :cond_2

    .line 51
    .line 52
    return-void

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇80()Landroid/widget/TextView;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    const/16 v1, 0x8

    .line 58
    .line 59
    if-nez v0, :cond_3

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00〇8()Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-nez v0, :cond_4

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 73
    .line 74
    .line 75
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    instance-of v0, p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 84
    .line 85
    if-eqz v0, :cond_8

    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 88
    .line 89
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    const/4 v2, 0x0

    .line 94
    if-eqz v0, :cond_5

    .line 95
    .line 96
    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 97
    .line 98
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 99
    .line 100
    invoke-static {p2, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 101
    .line 102
    .line 103
    move-result p2

    .line 104
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 105
    .line 106
    goto :goto_3

    .line 107
    :cond_5
    if-eqz p2, :cond_7

    .line 108
    .line 109
    const-string v0, ""

    .line 110
    .line 111
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    move-result p2

    .line 115
    if-eqz p2, :cond_6

    .line 116
    .line 117
    goto :goto_2

    .line 118
    :cond_6
    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 119
    .line 120
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 121
    .line 122
    invoke-static {p2, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 127
    .line 128
    goto :goto_3

    .line 129
    :cond_7
    :goto_2
    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 130
    .line 131
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 132
    .line 133
    invoke-static {p2, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 134
    .line 135
    .line 136
    move-result p2

    .line 137
    iput p2, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 138
    .line 139
    :cond_8
    :goto_3
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o88〇OO08〇(Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;)Z
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p2}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNameId()I

    .line 12
    .line 13
    .line 14
    move-result p2

    .line 15
    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    return p1

    .line 24
    :cond_0
    const/4 p1, 0x0

    .line 25
    return p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o8O〇(Lcom/intsig/view/NinePhotoView;ILcom/intsig/camscanner/datastruct/DocItem;Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V
    .locals 1

    .line 1
    const-string v0, "$ninePhotoView"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "this$0"

    .line 12
    .line 13
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/view/NinePhotoView;->o〇0(I)Lcom/intsig/view/NinePhotoView;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    new-instance p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$loadTimeLineThumbs$1$1;

    .line 21
    .line 22
    invoke-direct {p1, p3, p4, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$loadTimeLineThumbs$1$1;-><init>(Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, p1}, Lcom/intsig/view/NinePhotoView;->〇〇888(Lcom/intsig/view/NinePhotoView$NinePhotosAddItemListener;)Lcom/intsig/view/NinePhotoView;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    .line 30
    .line 31
    .line 32
    move-result-wide p1

    .line 33
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p0, p1}, Lcom/intsig/view/NinePhotoView;->Oo08(Ljava/lang/Long;)Lcom/intsig/view/NinePhotoView;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    invoke-virtual {p0}, Lcom/intsig/view/NinePhotoView;->〇080()Lcom/intsig/view/NinePhotoView;

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final oO(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Lcom/intsig/camscanner/datastruct/DocItem;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$docItem"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object p2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o88O〇8()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->OO0o〇〇〇〇0(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o88O〇8()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇8o8o〇(I)V

    .line 29
    .line 30
    .line 31
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 32
    .line 33
    instance-of p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 34
    .line 35
    if-eqz p2, :cond_0

    .line 36
    .line 37
    check-cast p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const/4 p0, 0x0

    .line 41
    :goto_0
    if-eqz p0, :cond_1

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->〇oO88o()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 44
    .line 45
    .line 46
    move-result-object p0

    .line 47
    if-eqz p0, :cond_1

    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    if-eqz p0, :cond_1

    .line 54
    .line 55
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o0O0O8(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 56
    .line 57
    .line 58
    :cond_1
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO00OOO()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇888()Ljava/util/HashMap;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oO〇(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 6

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O08000()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇8〇0〇o〇O()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    new-instance v4, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v5, "updateSyncStateView: docName: "

    .line 21
    .line 22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v3, ", syncState: "

    .line 29
    .line 30
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v3, ", syncUiState: "

    .line 37
    .line 38
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O000()Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    const/4 v3, 0x0

    .line 56
    if-nez v2, :cond_a

    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 59
    .line 60
    invoke-static {v2}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    if-eqz v2, :cond_0

    .line 65
    .line 66
    goto/16 :goto_2

    .line 67
    .line 68
    :cond_0
    const/4 v2, 0x1

    .line 69
    if-eq v1, v2, :cond_5

    .line 70
    .line 71
    const/4 v4, 0x2

    .line 72
    if-eq v1, v4, :cond_5

    .line 73
    .line 74
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 75
    .line 76
    .line 77
    if-nez v0, :cond_1

    .line 78
    .line 79
    if-nez v1, :cond_1

    .line 80
    .line 81
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    .line 83
    .line 84
    goto/16 :goto_3

    .line 85
    .line 86
    :cond_1
    const/4 v4, 0x4

    .line 87
    if-ne v1, v4, :cond_2

    .line 88
    .line 89
    const p2, 0x7f080cb0

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    .line 94
    .line 95
    goto/16 :goto_3

    .line 96
    .line 97
    :cond_2
    if-ne v0, v2, :cond_4

    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 100
    .line 101
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-eqz v0, :cond_4

    .line 106
    .line 107
    const/4 v0, 0x3

    .line 108
    if-eq v1, v0, :cond_4

    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 111
    .line 112
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    if-eqz v0, :cond_3

    .line 117
    .line 118
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o8oO〇()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p2

    .line 122
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 123
    .line 124
    .line 125
    move-result p2

    .line 126
    if-eqz p2, :cond_3

    .line 127
    .line 128
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 129
    .line 130
    .line 131
    goto :goto_3

    .line 132
    :cond_3
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 133
    .line 134
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 135
    .line 136
    .line 137
    move-result p2

    .line 138
    if-nez p2, :cond_b

    .line 139
    .line 140
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 141
    .line 142
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 143
    .line 144
    .line 145
    move-result p2

    .line 146
    if-nez p2, :cond_b

    .line 147
    .line 148
    const p2, 0x7f080cb4

    .line 149
    .line 150
    .line 151
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 152
    .line 153
    .line 154
    goto :goto_3

    .line 155
    :cond_4
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 156
    .line 157
    .line 158
    goto :goto_3

    .line 159
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 160
    .line 161
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 162
    .line 163
    .line 164
    move-result v0

    .line 165
    if-eqz v0, :cond_9

    .line 166
    .line 167
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooo〇8oO()Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-nez v0, :cond_6

    .line 172
    .line 173
    sget-object v0, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->〇080:Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;

    .line 174
    .line 175
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 176
    .line 177
    .line 178
    move-result-wide v1

    .line 179
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->〇〇888(J)Z

    .line 180
    .line 181
    .line 182
    move-result p2

    .line 183
    if-eqz p2, :cond_9

    .line 184
    .line 185
    :cond_6
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 186
    .line 187
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 188
    .line 189
    .line 190
    move-result p2

    .line 191
    if-nez p2, :cond_8

    .line 192
    .line 193
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 194
    .line 195
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 196
    .line 197
    .line 198
    move-result p2

    .line 199
    if-eqz p2, :cond_7

    .line 200
    .line 201
    goto :goto_0

    .line 202
    :cond_7
    const p2, 0x7f080cb2

    .line 203
    .line 204
    .line 205
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 206
    .line 207
    .line 208
    goto :goto_1

    .line 209
    :cond_8
    :goto_0
    const p2, 0x7f080cb3

    .line 210
    .line 211
    .line 212
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 213
    .line 214
    .line 215
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇80()Landroid/view/animation/RotateAnimation;

    .line 216
    .line 217
    .line 218
    move-result-object p2

    .line 219
    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 220
    .line 221
    .line 222
    goto :goto_3

    .line 223
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 224
    .line 225
    .line 226
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 227
    .line 228
    .line 229
    goto :goto_3

    .line 230
    :cond_a
    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 231
    .line 232
    .line 233
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 234
    .line 235
    .line 236
    :cond_b
    :goto_3
    return-void
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇000O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final ooOO(Lcom/intsig/camscanner/datastruct/DocItem;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/doc2officeactivity/Doc2OfficeActivity;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    check-cast v0, Lcom/intsig/camscanner/doc2officeactivity/Doc2OfficeActivity;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/doc2officeactivity/Doc2OfficeActivity;->〇OoO0o0()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_2

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o8oO〇()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    if-nez p1, :cond_2

    .line 43
    .line 44
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 45
    :cond_2
    return v2
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final ooO〇00O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;ILcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-nez p2, :cond_0

    .line 4
    .line 5
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oO00OOO()Z

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    if-nez p2, :cond_0

    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 12
    .line 13
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8oOo80()Landroidx/fragment/app/Fragment;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    instance-of p2, p2, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 18
    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 22
    .line 23
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8oOo80()Landroidx/fragment/app/Fragment;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 28
    .line 29
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 30
    .line 31
    .line 32
    move-result-object p2

    .line 33
    instance-of p2, p2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 34
    .line 35
    if-eqz p2, :cond_0

    .line 36
    .line 37
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 38
    .line 39
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO8o()I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    if-nez p2, :cond_0

    .line 44
    .line 45
    const/4 p2, 0x1

    .line 46
    goto :goto_0

    .line 47
    :cond_0
    const/4 p2, 0x0

    .line 48
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇0o()Landroid/view/ViewGroup;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    if-eqz v2, :cond_2

    .line 53
    .line 54
    if-eqz p2, :cond_1

    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇08880o0()Z

    .line 57
    .line 58
    .line 59
    move-result v3

    .line 60
    if-eqz v3, :cond_1

    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_1
    const/4 v0, 0x0

    .line 64
    :goto_1
    invoke-static {v2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 65
    .line 66
    .line 67
    :cond_2
    if-eqz p2, :cond_b

    .line 68
    .line 69
    sget-object p2, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 70
    .line 71
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const-string v1, ""

    .line 76
    .line 77
    if-nez v0, :cond_3

    .line 78
    .line 79
    move-object v0, v1

    .line 80
    :cond_3
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    if-nez p2, :cond_4

    .line 85
    .line 86
    move-object p2, v1

    .line 87
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;->PDF:Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 88
    .line 89
    const v2, 0x7f1314f5

    .line 90
    .line 91
    .line 92
    if-ne p2, v0, :cond_6

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    if-nez p1, :cond_5

    .line 99
    .line 100
    goto :goto_3

    .line 101
    :cond_5
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 102
    .line 103
    .line 104
    move-result-object p2

    .line 105
    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    .line 111
    .line 112
    goto :goto_3

    .line 113
    :cond_6
    invoke-virtual {p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    if-nez p2, :cond_7

    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_7
    move-object v1, p2

    .line 121
    :goto_2
    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 122
    .line 123
    .line 124
    move-result p2

    .line 125
    if-eqz p2, :cond_9

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    if-nez p1, :cond_8

    .line 132
    .line 133
    goto :goto_3

    .line 134
    :cond_8
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 135
    .line 136
    .line 137
    move-result-object p2

    .line 138
    const p3, 0x7f131374

    .line 139
    .line 140
    .line 141
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object p2

    .line 145
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    .line 147
    .line 148
    goto :goto_3

    .line 149
    :cond_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    if-nez p1, :cond_a

    .line 154
    .line 155
    goto :goto_3

    .line 156
    :cond_a
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 157
    .line 158
    .line 159
    move-result-object p2

    .line 160
    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object p2

    .line 164
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    .line 166
    .line 167
    :cond_b
    :goto_3
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final ooo〇8oO(Lcom/intsig/camscanner/datastruct/DocItem;)I
    .locals 6

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 6
    .line 7
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->O0o〇〇Oo()Z

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    if-eqz v3, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 18
    .line 19
    .line 20
    move-result-wide v4

    .line 21
    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->O8〇o(Landroid/content/Context;J)Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-eqz v3, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 32
    .line 33
    .line 34
    move-result-wide v4

    .line 35
    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->oo〇(Landroid/content/Context;J)Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-eqz v3, :cond_0

    .line 40
    .line 41
    sget-object v3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 42
    .line 43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 44
    .line 45
    .line 46
    move-result-wide v4

    .line 47
    sub-long/2addr v4, v0

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v1, "getShowNum certificate time = "

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 73
    .line 74
    .line 75
    move-result-wide v3

    .line 76
    invoke-virtual {v2, v0, v3, v4}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->oo88o8O(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 81
    .line 82
    .line 83
    move-result p1

    .line 84
    return p1

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 86
    .line 87
    .line 88
    move-result-wide v2

    .line 89
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-static {v2, v3}, Lcom/intsig/camscanner/card_photo/CardPhotoHelper;->Oo08(Ljava/lang/Long;Ljava/lang/Integer;)Lcom/intsig/camscanner/datastruct/DocItem;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    if-eqz v2, :cond_1

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 108
    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 110
    .line 111
    .line 112
    move-result-wide v3

    .line 113
    sub-long/2addr v3, v0

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    .line 115
    .line 116
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .line 118
    .line 119
    const-string v1, "getShowNum cardPhoto time = "

    .line 120
    .line 121
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 135
    .line 136
    .line 137
    move-result p1

    .line 138
    add-int/lit8 p1, p1, -0x1

    .line 139
    .line 140
    return p1

    .line 141
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 142
    .line 143
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 144
    .line 145
    .line 146
    move-result-wide v3

    .line 147
    sub-long/2addr v3, v0

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    .line 149
    .line 150
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .line 152
    .line 153
    const-string v1, "getShowNum def time = "

    .line 154
    .line 155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 169
    .line 170
    .line 171
    move-result p1

    .line 172
    return p1
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic oo〇()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇0OOo〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o0ooO(ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final o〇8oOO88()Landroid/graphics/drawable/Drawable;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v0, "#494742"

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string v0, "#FFFBF4"

    .line 15
    .line 16
    :goto_0
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 21
    .line 22
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const/16 v1, 0x8

    .line 30
    .line 31
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 44
    .line 45
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇O(ZLcom/intsig/camscanner/datastruct/DocItem;)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 17
    .line 18
    check-cast v0, Ljava/lang/String;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v1

    .line 22
    :goto_0
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;

    .line 30
    .line 31
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    const/4 v2, 0x0

    .line 36
    const/4 v3, 0x2

    .line 37
    invoke-static {v0, p2, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->〇o〇(Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    const v0, 0x7f0810fc

    .line 46
    .line 47
    .line 48
    const v2, 0x7f0810fe

    .line 49
    .line 50
    .line 51
    if-nez p2, :cond_6

    .line 52
    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    goto :goto_5

    .line 60
    :cond_2
    if-eqz p1, :cond_3

    .line 61
    .line 62
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-nez p1, :cond_8

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-eqz p1, :cond_c

    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 80
    .line 81
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    if-nez p1, :cond_5

    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 88
    .line 89
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    if-eqz p1, :cond_4

    .line 94
    .line 95
    goto :goto_2

    .line 96
    :cond_4
    const v0, 0x7f0810fd

    .line 97
    .line 98
    .line 99
    goto :goto_5

    .line 100
    :cond_5
    :goto_2
    const v0, 0x7f081131

    .line 101
    .line 102
    .line 103
    goto :goto_5

    .line 104
    :cond_6
    if-eqz v1, :cond_7

    .line 105
    .line 106
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    goto :goto_5

    .line 111
    :cond_7
    if-eqz p1, :cond_9

    .line 112
    .line 113
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 114
    .line 115
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 116
    .line 117
    .line 118
    move-result p1

    .line 119
    if-nez p1, :cond_8

    .line 120
    .line 121
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    if-eqz p1, :cond_c

    .line 128
    .line 129
    :cond_8
    :goto_3
    const v0, 0x7f0810fe

    .line 130
    .line 131
    .line 132
    goto :goto_5

    .line 133
    :cond_9
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 134
    .line 135
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 136
    .line 137
    .line 138
    move-result p1

    .line 139
    if-nez p1, :cond_b

    .line 140
    .line 141
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_a

    .line 148
    .line 149
    goto :goto_4

    .line 150
    :cond_a
    const v0, 0x7f081138

    .line 151
    .line 152
    .line 153
    goto :goto_5

    .line 154
    :cond_b
    :goto_4
    const v0, 0x7f081130

    .line 155
    .line 156
    .line 157
    :cond_c
    :goto_5
    return v0
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/view/NinePhotoView;ILcom/intsig/camscanner/datastruct/DocItem;Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8O〇(Lcom/intsig/view/NinePhotoView;ILcom/intsig/camscanner/datastruct/DocItem;Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic o〇〇0〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇8〇0〇o〇O()Ljava/text/SimpleDateFormat;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const/4 v1, 0x0

    .line 22
    if-eqz p1, :cond_0

    .line 23
    .line 24
    const-string v2, "dateTime"

    .line 25
    .line 26
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    invoke-static {p1, v0, v2}, Lkotlin/text/StringsKt;->o〇8(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-ne p1, v2, :cond_0

    .line 35
    .line 36
    const/4 v1, 0x1

    .line 37
    :cond_0
    return v1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0000OOO()Landroidx/collection/LruCache;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo80:Landroidx/collection/LruCache;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇000O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇O(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇000〇〇08(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;IILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const p3, 0x7f080cd2

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇O8o〇8(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private static final 〇00O0O0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OOO8o〇〇(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇00〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇O8o〇8(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇0O〇Oo(Landroid/view/View;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇o00〇〇Oo()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇o〇(J)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    instance-of v0, p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_2

    .line 29
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    .line 31
    .line 32
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 36
    .line 37
    .line 38
    move-result-wide v1

    .line 39
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 47
    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;->〇080(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8(Z)I
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f080195

    .line 6
    .line 7
    .line 8
    if-nez v0, :cond_1

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    return v1

    .line 13
    :cond_0
    const p1, 0x7f08112b

    .line 14
    .line 15
    .line 16
    return p1

    .line 17
    :cond_1
    if-eqz p1, :cond_3

    .line 18
    .line 19
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-nez p1, :cond_6

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_2
    const v1, 0x7f080197

    .line 37
    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-nez p1, :cond_5

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_4

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_4
    const v1, 0x7f081139

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_5
    :goto_0
    const v1, 0x7f081131

    .line 62
    .line 63
    .line 64
    :cond_6
    :goto_1
    return v1
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇80()Landroid/view/animation/RotateAnimation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/view/animation/RotateAnimation;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8〇0〇o〇O(JI)Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 27
    .line 28
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇O888o0o(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p3

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8oOOo:Ljava/text/SimpleDateFormat;

    .line 33
    .line 34
    new-instance v1, Ljava/util/Date;

    .line 35
    .line 36
    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    new-instance p2, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    goto :goto_1

    .line 59
    :cond_1
    :goto_0
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇O〇〇O8:Ljava/text/SimpleDateFormat;

    .line 60
    .line 61
    new-instance v0, Ljava/util/Date;

    .line 62
    .line 63
    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    goto :goto_1

    .line 71
    :cond_2
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8oOOo:Ljava/text/SimpleDateFormat;

    .line 72
    .line 73
    new-instance v0, Ljava/util/Date;

    .line 74
    .line 75
    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    :goto_1
    return-object p1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇O(Landroid/view/View;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇o00〇〇Oo()Ljava/lang/Long;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v1

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇o〇(J)V

    .line 14
    .line 15
    .line 16
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    instance-of v0, p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_2

    .line 29
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    .line 31
    .line 32
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 36
    .line 37
    .line 38
    move-result-wide v1

    .line 39
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 47
    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oO8o()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const v1, 0x7f0803f6

    .line 8
    .line 9
    .line 10
    const v2, 0x7f080196

    .line 11
    .line 12
    .line 13
    const v3, 0x7f080194

    .line 14
    .line 15
    .line 16
    const/4 v4, 0x0

    .line 17
    const/4 v5, 0x1

    .line 18
    const/4 v6, 0x0

    .line 19
    if-nez v0, :cond_17

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    sget-object v7, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 28
    .line 29
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v7

    .line 33
    if-eqz v7, :cond_0

    .line 34
    .line 35
    const/4 v7, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    sget-object v7, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 38
    .line 39
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v7

    .line 43
    :goto_0
    if-eqz v7, :cond_7

    .line 44
    .line 45
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o0O0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 58
    .line 59
    .line 60
    :cond_1
    const/4 v6, 0x1

    .line 61
    const/4 v8, 0x0

    .line 62
    const/4 v9, 0x4

    .line 63
    const/4 v10, 0x0

    .line 64
    move-object v5, p0

    .line 65
    move-object v7, p1

    .line 66
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    goto :goto_4

    .line 70
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 71
    .line 72
    .line 73
    move-result-wide v0

    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0oOo〇0()Ljava/lang/Long;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    if-nez v2, :cond_3

    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 84
    .line 85
    .line 86
    move-result-wide v2

    .line 87
    cmp-long v7, v0, v2

    .line 88
    .line 89
    if-nez v7, :cond_5

    .line 90
    .line 91
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-nez v0, :cond_4

    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8oOO88()Landroid/graphics/drawable/Drawable;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 103
    .line 104
    .line 105
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8oOO88()Landroid/graphics/drawable/Drawable;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-direct {p0, v5, p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o0ooO(ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;)V

    .line 110
    .line 111
    .line 112
    goto :goto_4

    .line 113
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    if-nez v0, :cond_6

    .line 118
    .line 119
    goto :goto_3

    .line 120
    :cond_6
    invoke-virtual {v0, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 121
    .line 122
    .line 123
    :goto_3
    const/4 v8, 0x0

    .line 124
    const/4 v10, 0x0

    .line 125
    const/4 v11, 0x4

    .line 126
    const/4 v12, 0x0

    .line 127
    move-object v7, p0

    .line 128
    move-object v9, p1

    .line 129
    invoke-static/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V

    .line 130
    .line 131
    .line 132
    :goto_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    if-eqz p1, :cond_25

    .line 137
    .line 138
    invoke-direct {p0, v4, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇O(ZLcom/intsig/camscanner/datastruct/DocItem;)I

    .line 139
    .line 140
    .line 141
    move-result p2

    .line 142
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 143
    .line 144
    .line 145
    goto/16 :goto_d

    .line 146
    .line 147
    :cond_7
    sget-object v3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 148
    .line 149
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    if-eqz v3, :cond_11

    .line 154
    .line 155
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    if-eqz v0, :cond_8

    .line 160
    .line 161
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .line 162
    .line 163
    check-cast v0, Ljava/lang/String;

    .line 164
    .line 165
    goto :goto_5

    .line 166
    :cond_8
    move-object v0, v6

    .line 167
    :goto_5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-eqz v0, :cond_9

    .line 172
    .line 173
    move-object v0, v6

    .line 174
    goto :goto_6

    .line 175
    :cond_9
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;

    .line 176
    .line 177
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 178
    .line 179
    .line 180
    move-result-object v1

    .line 181
    invoke-virtual {v0, v1, v4}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeViewManager;->〇o00〇〇Oo(Ljava/lang/String;Z)Ljava/lang/Integer;

    .line 182
    .line 183
    .line 184
    move-result-object v0

    .line 185
    :goto_6
    if-eqz v0, :cond_a

    .line 186
    .line 187
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 188
    .line 189
    .line 190
    move-result v0

    .line 191
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    .line 192
    .line 193
    .line 194
    move-result-object v1

    .line 195
    if-eqz v1, :cond_a

    .line 196
    .line 197
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 198
    .line 199
    .line 200
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 201
    .line 202
    goto :goto_7

    .line 203
    :cond_a
    move-object v0, v6

    .line 204
    :goto_7
    if-nez v0, :cond_c

    .line 205
    .line 206
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    if-nez v0, :cond_b

    .line 211
    .line 212
    goto :goto_8

    .line 213
    :cond_b
    invoke-virtual {v0, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 214
    .line 215
    .line 216
    :cond_c
    :goto_8
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o0O0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    .line 217
    .line 218
    .line 219
    move-result v0

    .line 220
    if-eqz v0, :cond_d

    .line 221
    .line 222
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    if-eqz p1, :cond_25

    .line 227
    .line 228
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8(Z)I

    .line 229
    .line 230
    .line 231
    move-result p2

    .line 232
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 233
    .line 234
    .line 235
    goto/16 :goto_d

    .line 236
    .line 237
    :cond_d
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 238
    .line 239
    .line 240
    move-result-wide v0

    .line 241
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 242
    .line 243
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0oOo〇0()Ljava/lang/Long;

    .line 244
    .line 245
    .line 246
    move-result-object p2

    .line 247
    if-nez p2, :cond_e

    .line 248
    .line 249
    goto :goto_9

    .line 250
    :cond_e
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 251
    .line 252
    .line 253
    move-result-wide v2

    .line 254
    cmp-long p2, v0, v2

    .line 255
    .line 256
    if-nez p2, :cond_10

    .line 257
    .line 258
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 259
    .line 260
    .line 261
    move-result-object p1

    .line 262
    if-nez p1, :cond_f

    .line 263
    .line 264
    goto/16 :goto_d

    .line 265
    .line 266
    :cond_f
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8oOO88()Landroid/graphics/drawable/Drawable;

    .line 267
    .line 268
    .line 269
    move-result-object p2

    .line 270
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 271
    .line 272
    .line 273
    goto/16 :goto_d

    .line 274
    .line 275
    :cond_10
    :goto_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    if-eqz p1, :cond_25

    .line 280
    .line 281
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8(Z)I

    .line 282
    .line 283
    .line 284
    move-result p2

    .line 285
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 286
    .line 287
    .line 288
    goto/16 :goto_d

    .line 289
    .line 290
    :cond_11
    sget-object v3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 291
    .line 292
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 293
    .line 294
    .line 295
    move-result v3

    .line 296
    if-eqz v3, :cond_16

    .line 297
    .line 298
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o0O0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    .line 299
    .line 300
    .line 301
    move-result v0

    .line 302
    if-eqz v0, :cond_12

    .line 303
    .line 304
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 305
    .line 306
    .line 307
    move-result-object p1

    .line 308
    if-eqz p1, :cond_25

    .line 309
    .line 310
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 311
    .line 312
    .line 313
    goto/16 :goto_d

    .line 314
    .line 315
    :cond_12
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 316
    .line 317
    .line 318
    move-result-wide v2

    .line 319
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 320
    .line 321
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo0oOo〇0()Ljava/lang/Long;

    .line 322
    .line 323
    .line 324
    move-result-object p2

    .line 325
    if-nez p2, :cond_13

    .line 326
    .line 327
    goto :goto_a

    .line 328
    :cond_13
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 329
    .line 330
    .line 331
    move-result-wide v4

    .line 332
    cmp-long p2, v2, v4

    .line 333
    .line 334
    if-nez p2, :cond_15

    .line 335
    .line 336
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 337
    .line 338
    .line 339
    move-result-object p1

    .line 340
    if-nez p1, :cond_14

    .line 341
    .line 342
    goto/16 :goto_d

    .line 343
    .line 344
    :cond_14
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8oOO88()Landroid/graphics/drawable/Drawable;

    .line 345
    .line 346
    .line 347
    move-result-object p2

    .line 348
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 349
    .line 350
    .line 351
    goto/16 :goto_d

    .line 352
    .line 353
    :cond_15
    :goto_a
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 354
    .line 355
    .line 356
    move-result-object p1

    .line 357
    if-eqz p1, :cond_25

    .line 358
    .line 359
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 360
    .line 361
    .line 362
    goto/16 :goto_d

    .line 363
    .line 364
    :cond_16
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 365
    .line 366
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    goto/16 :goto_d

    .line 370
    .line 371
    :cond_17
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇0OOo〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    .line 372
    .line 373
    .line 374
    move-result v0

    .line 375
    if-eqz v0, :cond_1e

    .line 376
    .line 377
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 378
    .line 379
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 380
    .line 381
    .line 382
    move-result-object v0

    .line 383
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 384
    .line 385
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 386
    .line 387
    .line 388
    move-result v1

    .line 389
    if-eqz v1, :cond_1a

    .line 390
    .line 391
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 392
    .line 393
    .line 394
    move-result-object v0

    .line 395
    if-eqz v0, :cond_18

    .line 396
    .line 397
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 398
    .line 399
    .line 400
    :cond_18
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    .line 401
    .line 402
    .line 403
    move-result-object v0

    .line 404
    if-eqz v0, :cond_19

    .line 405
    .line 406
    invoke-direct {p0, v5, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇O(ZLcom/intsig/camscanner/datastruct/DocItem;)I

    .line 407
    .line 408
    .line 409
    move-result p2

    .line 410
    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 411
    .line 412
    .line 413
    :cond_19
    const/4 v2, 0x1

    .line 414
    const/4 v4, 0x0

    .line 415
    const/4 v5, 0x4

    .line 416
    const/4 v6, 0x0

    .line 417
    move-object v1, p0

    .line 418
    move-object v3, p1

    .line 419
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V

    .line 420
    .line 421
    .line 422
    goto/16 :goto_d

    .line 423
    .line 424
    :cond_1a
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 425
    .line 426
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 427
    .line 428
    .line 429
    move-result p2

    .line 430
    if-eqz p2, :cond_1b

    .line 431
    .line 432
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 433
    .line 434
    .line 435
    move-result-object p1

    .line 436
    if-eqz p1, :cond_25

    .line 437
    .line 438
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8(Z)I

    .line 439
    .line 440
    .line 441
    move-result p2

    .line 442
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 443
    .line 444
    .line 445
    goto/16 :goto_d

    .line 446
    .line 447
    :cond_1b
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 448
    .line 449
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 450
    .line 451
    .line 452
    move-result p2

    .line 453
    if-eqz p2, :cond_1c

    .line 454
    .line 455
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 456
    .line 457
    .line 458
    move-result-object p1

    .line 459
    if-eqz p1, :cond_25

    .line 460
    .line 461
    invoke-virtual {p1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 462
    .line 463
    .line 464
    goto/16 :goto_d

    .line 465
    .line 466
    :cond_1c
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 467
    .line 468
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 469
    .line 470
    .line 471
    move-result p2

    .line 472
    if-eqz p2, :cond_1d

    .line 473
    .line 474
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 475
    .line 476
    .line 477
    move-result-object p1

    .line 478
    if-eqz p1, :cond_25

    .line 479
    .line 480
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 481
    .line 482
    .line 483
    goto/16 :goto_d

    .line 484
    .line 485
    :cond_1d
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 486
    .line 487
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 488
    .line 489
    .line 490
    goto :goto_d

    .line 491
    :cond_1e
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 492
    .line 493
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 494
    .line 495
    .line 496
    move-result-object v0

    .line 497
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 498
    .line 499
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 500
    .line 501
    .line 502
    move-result v2

    .line 503
    if-eqz v2, :cond_1f

    .line 504
    .line 505
    goto :goto_b

    .line 506
    :cond_1f
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 507
    .line 508
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 509
    .line 510
    .line 511
    move-result v5

    .line 512
    :goto_b
    if-eqz v5, :cond_22

    .line 513
    .line 514
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 515
    .line 516
    .line 517
    move-result-object v0

    .line 518
    if-nez v0, :cond_20

    .line 519
    .line 520
    goto :goto_c

    .line 521
    :cond_20
    invoke-virtual {v0, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 522
    .line 523
    .line 524
    :goto_c
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    .line 525
    .line 526
    .line 527
    move-result-object v0

    .line 528
    if-eqz v0, :cond_21

    .line 529
    .line 530
    invoke-direct {p0, v4, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇O(ZLcom/intsig/camscanner/datastruct/DocItem;)I

    .line 531
    .line 532
    .line 533
    move-result p2

    .line 534
    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 535
    .line 536
    .line 537
    :cond_21
    const/4 v2, 0x0

    .line 538
    const/4 v4, 0x0

    .line 539
    const/4 v5, 0x4

    .line 540
    const/4 v6, 0x0

    .line 541
    move-object v1, p0

    .line 542
    move-object v3, p1

    .line 543
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;ZLcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Landroid/graphics/drawable/Drawable;ILjava/lang/Object;)V

    .line 544
    .line 545
    .line 546
    goto :goto_d

    .line 547
    :cond_22
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 548
    .line 549
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 550
    .line 551
    .line 552
    move-result p2

    .line 553
    if-eqz p2, :cond_23

    .line 554
    .line 555
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 556
    .line 557
    .line 558
    move-result-object p1

    .line 559
    if-eqz p1, :cond_25

    .line 560
    .line 561
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8(Z)I

    .line 562
    .line 563
    .line 564
    move-result p2

    .line 565
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 566
    .line 567
    .line 568
    goto :goto_d

    .line 569
    :cond_23
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 570
    .line 571
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 572
    .line 573
    .line 574
    move-result p2

    .line 575
    if-eqz p2, :cond_24

    .line 576
    .line 577
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 578
    .line 579
    .line 580
    move-result-object p1

    .line 581
    if-eqz p1, :cond_25

    .line 582
    .line 583
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 584
    .line 585
    .line 586
    goto :goto_d

    .line 587
    :cond_24
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 588
    .line 589
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 590
    .line 591
    .line 592
    :cond_25
    :goto_d
    return-void
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇o0O0O8(Landroid/view/View;ILcom/intsig/camscanner/datastruct/DocItem;IIILandroid/util/Pair;I)V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            "III",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .line 1
    move-object v6, p0

    .line 2
    move-object v7, p1

    .line 3
    move/from16 v8, p2

    .line 4
    .line 5
    move-object/from16 v9, p3

    .line 6
    .line 7
    move/from16 v10, p5

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 11
    .line 12
    .line 13
    const v1, 0x7f0a08c0

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Landroid/widget/ImageView;

    .line 21
    .line 22
    const v2, 0x7f0a0a60

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    move-object v11, v2

    .line 30
    check-cast v11, Landroid/widget/ImageView;

    .line 31
    .line 32
    const v2, 0x7f0a0a4e

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    move-object v12, v2

    .line 40
    check-cast v12, Landroid/widget/ImageView;

    .line 41
    .line 42
    const v2, 0x7f0a096d

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    move-object v13, v2

    .line 50
    check-cast v13, Landroid/widget/ImageView;

    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_0

    .line 57
    .line 58
    if-eqz v11, :cond_0

    .line 59
    .line 60
    invoke-direct {p0, v0, v9}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇O(ZLcom/intsig/camscanner/datastruct/DocItem;)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    invoke-virtual {v11, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 65
    .line 66
    .line 67
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    const/16 v3, 0x7b

    .line 72
    .line 73
    if-ne v2, v3, :cond_3

    .line 74
    .line 75
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    if-nez v0, :cond_2

    .line 82
    .line 83
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_1

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_1
    const v2, 0x7f080730

    .line 93
    .line 94
    .line 95
    const/4 v3, 0x0

    .line 96
    const/4 v4, 0x4

    .line 97
    const/4 v5, 0x0

    .line 98
    move-object v0, p0

    .line 99
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;ILjava/lang/Float;ILjava/lang/Object;)V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_2
    :goto_0
    const v2, 0x7f080e7b

    .line 104
    .line 105
    .line 106
    const/4 v3, 0x0

    .line 107
    const/4 v4, 0x4

    .line 108
    const/4 v5, 0x0

    .line 109
    move-object v0, p0

    .line 110
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;ILjava/lang/Float;ILjava/lang/Object;)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_3
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    const/16 v3, 0x7c

    .line 119
    .line 120
    if-ne v2, v3, :cond_6

    .line 121
    .line 122
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 123
    .line 124
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    if-nez v0, :cond_5

    .line 129
    .line 130
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 131
    .line 132
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    if-eqz v0, :cond_4

    .line 137
    .line 138
    goto :goto_1

    .line 139
    :cond_4
    const v2, 0x7f08072e

    .line 140
    .line 141
    .line 142
    const/4 v3, 0x0

    .line 143
    const/4 v4, 0x4

    .line 144
    const/4 v5, 0x0

    .line 145
    move-object v0, p0

    .line 146
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;ILjava/lang/Float;ILjava/lang/Object;)V

    .line 147
    .line 148
    .line 149
    goto :goto_2

    .line 150
    :cond_5
    :goto_1
    const v2, 0x7f080e7a

    .line 151
    .line 152
    .line 153
    const/4 v3, 0x0

    .line 154
    const/4 v4, 0x4

    .line 155
    const/4 v5, 0x0

    .line 156
    move-object v0, p0

    .line 157
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;ILjava/lang/Float;ILjava/lang/Object;)V

    .line 158
    .line 159
    .line 160
    goto :goto_2

    .line 161
    :cond_6
    if-eqz v1, :cond_7

    .line 162
    .line 163
    invoke-static {v1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 164
    .line 165
    .line 166
    :cond_7
    :goto_2
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 167
    .line 168
    move/from16 v1, p6

    .line 169
    .line 170
    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 171
    .line 172
    .line 173
    const/4 v1, 0x1

    .line 174
    add-int/lit8 v2, p8, -0x1

    .line 175
    .line 176
    if-eq v8, v2, :cond_8

    .line 177
    .line 178
    const/high16 v2, 0x41000000    # 8.0f

    .line 179
    .line 180
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 181
    .line 182
    .line 183
    move-result v2

    .line 184
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 185
    .line 186
    .line 187
    :cond_8
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    .line 189
    .line 190
    add-int/lit8 v0, v8, 0x1

    .line 191
    .line 192
    if-ne v0, v10, :cond_9

    .line 193
    .line 194
    move/from16 v0, p4

    .line 195
    .line 196
    if-le v0, v10, :cond_9

    .line 197
    .line 198
    const v0, 0x7f0a1856

    .line 199
    .line 200
    .line 201
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    check-cast v0, Landroid/widget/TextView;

    .line 206
    .line 207
    if-eqz v0, :cond_9

    .line 208
    .line 209
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 210
    .line 211
    .line 212
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 213
    .line 214
    .line 215
    move-result v2

    .line 216
    sub-int/2addr v2, v10

    .line 217
    add-int/2addr v2, v1

    .line 218
    new-instance v1, Ljava/lang/StringBuilder;

    .line 219
    .line 220
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    .line 222
    .line 223
    const-string v3, "+"

    .line 224
    .line 225
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    .line 227
    .line 228
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 229
    .line 230
    .line 231
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v1

    .line 235
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    .line 237
    .line 238
    :cond_9
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇O8〇〇o(Ljava/lang/String;)Z

    .line 243
    .line 244
    .line 245
    move-result v0

    .line 246
    const-wide/16 v1, 0x0

    .line 247
    .line 248
    if-eqz v0, :cond_a

    .line 249
    .line 250
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->OO8oO0o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 259
    .line 260
    .line 261
    move-result v3

    .line 262
    if-eqz v3, :cond_a

    .line 263
    .line 264
    new-instance v3, Landroid/util/Pair;

    .line 265
    .line 266
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 267
    .line 268
    .line 269
    move-result-object v4

    .line 270
    invoke-direct {v3, v0, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {v9, v3}, Lcom/intsig/camscanner/datastruct/DocItem;->ooo〇8oO(Landroid/util/Pair;)V

    .line 274
    .line 275
    .line 276
    :cond_a
    if-nez p7, :cond_d

    .line 277
    .line 278
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 279
    .line 280
    .line 281
    move-result-object v0

    .line 282
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    .line 283
    .line 284
    .line 285
    move-result v0

    .line 286
    if-eqz v0, :cond_c

    .line 287
    .line 288
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v0

    .line 292
    if-eqz v0, :cond_c

    .line 293
    .line 294
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 295
    .line 296
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 297
    .line 298
    .line 299
    move-result-object v3

    .line 300
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 301
    .line 302
    .line 303
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 304
    .line 305
    .line 306
    move-result-object v0

    .line 307
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 308
    .line 309
    .line 310
    move-result v3

    .line 311
    if-eqz v3, :cond_b

    .line 312
    .line 313
    new-instance v3, Landroid/util/Pair;

    .line 314
    .line 315
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 316
    .line 317
    .line 318
    move-result-object v1

    .line 319
    invoke-direct {v3, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 320
    .line 321
    .line 322
    goto :goto_3

    .line 323
    :cond_b
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    .line 324
    .line 325
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 326
    .line 327
    .line 328
    move-result-wide v1

    .line 329
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 330
    .line 331
    .line 332
    move-result-object v1

    .line 333
    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    .line 335
    .line 336
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 337
    .line 338
    .line 339
    move-result-object v0

    .line 340
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 341
    .line 342
    .line 343
    goto :goto_4

    .line 344
    :cond_c
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 345
    .line 346
    .line 347
    move-result-object v0

    .line 348
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇O8〇〇o(Ljava/lang/String;)Z

    .line 349
    .line 350
    .line 351
    move-result v0

    .line 352
    if-eqz v0, :cond_d

    .line 353
    .line 354
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 355
    .line 356
    .line 357
    move-result-object v0

    .line 358
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->OO8oO0o〇(Ljava/lang/String;)Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v0

    .line 362
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 363
    .line 364
    .line 365
    move-result v3

    .line 366
    if-eqz v3, :cond_d

    .line 367
    .line 368
    new-instance v3, Landroid/util/Pair;

    .line 369
    .line 370
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 371
    .line 372
    .line 373
    move-result-object v1

    .line 374
    invoke-direct {v3, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 375
    .line 376
    .line 377
    :goto_3
    move-object v1, v3

    .line 378
    goto :goto_5

    .line 379
    :cond_d
    :goto_4
    move-object/from16 v1, p7

    .line 380
    .line 381
    :goto_5
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    .line 382
    .line 383
    .line 384
    move-result-wide v3

    .line 385
    invoke-virtual/range {p3 .. p3}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 386
    .line 387
    .line 388
    move-result-object v5

    .line 389
    move-object v0, p0

    .line 390
    move-object v2, v11

    .line 391
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8O〇(Landroid/util/Pair;Landroid/widget/ImageView;JLjava/lang/String;)V

    .line 392
    .line 393
    .line 394
    if-eqz v12, :cond_e

    .line 395
    .line 396
    invoke-direct {p0, v12, v9}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO〇(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 397
    .line 398
    .line 399
    :cond_e
    if-eqz v13, :cond_f

    .line 400
    .line 401
    const/4 v3, 0x0

    .line 402
    const/4 v4, 0x4

    .line 403
    const/4 v5, 0x0

    .line 404
    move-object v0, p0

    .line 405
    move-object v1, v13

    .line 406
    move-object/from16 v2, p3

    .line 407
    .line 408
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇000〇〇08(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;IILjava/lang/Object;)V

    .line 409
    .line 410
    .line 411
    :cond_f
    return-void
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
.end method

.method public static final synthetic 〇oOO8O8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Lcom/intsig/camscanner/datastruct/DocItem;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Lcom/intsig/camscanner/datastruct/DocItem;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇〇o8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo80:Landroidx/collection/LruCache;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroidx/collection/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_3

    .line 4
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    move-result v1

    const-wide/16 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5
    sget-object v1, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 6
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 7
    new-instance v4, Landroid/util/Pair;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v4, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 8
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/datastruct/DocItem;->ooo〇8oO(Landroid/util/Pair;)V

    goto :goto_0

    .line 9
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    goto :goto_0

    .line 11
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇O8〇〇o(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 12
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/camscanner/util/SDStorageManager;->OO8oO0o〇(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 14
    new-instance v4, Landroid/util/Pair;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v4, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 15
    invoke-virtual {p2, v4}, Lcom/intsig/camscanner/datastruct/DocItem;->ooo〇8oO(Landroid/util/Pair;)V

    .line 16
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    move-result-object v1

    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_8

    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O8o〇0()Landroid/widget/LinearLayout;

    move-result-object v1

    if-nez v1, :cond_4

    return-void

    .line 18
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O8o〇0()Landroid/widget/LinearLayout;

    move-result-object v1

    if-nez v1, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 19
    :goto_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_6

    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 22
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_2

    .line 23
    :cond_6
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v2

    .line 24
    :cond_7
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O8o〇0()Landroid/widget/LinearLayout;

    move-result-object p1

    .line 25
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    move-result v0

    .line 26
    invoke-direct {p0, p2, p1, v2, v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0O8OO088(Lcom/intsig/camscanner/datastruct/DocItem;Landroid/widget/LinearLayout;Ljava/util/ArrayList;I)V

    return-void

    .line 27
    :cond_8
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    move-result-object v1

    sget-object v3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8()Lcom/intsig/view/NinePhotoView;

    move-result-object p1

    if-eqz p1, :cond_c

    .line 29
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 30
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_9

    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_3

    .line 34
    :cond_9
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "docItem.docThumbPaths: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00()Ljava/util/ArrayList;

    move-result-object v2

    .line 36
    :cond_b
    :goto_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    move-result v0

    invoke-direct {p0, p2, p1, v2, v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo〇O(Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/view/NinePhotoView;Ljava/util/ArrayList;I)V

    .line 37
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_c
    if-nez v2, :cond_d

    .line 38
    sget-object p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    const-string p2, "docViewMode = TimeLineMode but ninePhotoView is NULL"

    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    return-void

    .line 39
    :cond_e
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    move-result-object v1

    if-nez v1, :cond_f

    return-void

    .line 40
    :cond_f
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    move-result-object v1

    if-nez v1, :cond_10

    goto :goto_4

    :cond_10
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 41
    :goto_4
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_12

    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oo8ooo8O:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O08000()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 44
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/collection/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    move-result-object v0

    goto :goto_5

    :cond_11
    move-object v0, v2

    goto :goto_5

    .line 45
    :cond_12
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇O8〇〇o()Landroid/util/Pair;

    move-result-object v0

    .line 46
    :goto_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo8Oo00oo()Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    move-result-wide v6

    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-object v4, v0

    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8O〇(Landroid/util/Pair;Landroid/widget/ImageView;JLjava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OOO〇O0()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_26

    .line 48
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    move-result v3

    const/16 v4, 0x7b

    const/high16 v5, 0x41e00000    # 28.0f

    const/high16 v6, 0x41800000    # 16.0f

    if-ne v3, v4, :cond_18

    .line 49
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    move-result v0

    const v3, 0x7f080e7b

    if-eqz v0, :cond_15

    .line 50
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_13

    goto :goto_6

    .line 51
    :cond_13
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const v3, 0x7f080e7c

    goto :goto_7

    .line 52
    :cond_14
    :goto_6
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_7

    .line 53
    :cond_15
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_16

    goto :goto_7

    :cond_16
    const v3, 0x7f080730

    .line 54
    :cond_17
    :goto_7
    invoke-direct {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto/16 :goto_d

    .line 55
    :cond_18
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    move-result v3

    const/16 v4, 0x7c

    if-ne v3, v4, :cond_1e

    .line 56
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    move-result v0

    const v3, 0x7f080e7a

    if-eqz v0, :cond_1b

    .line 57
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_19

    goto :goto_8

    .line 58
    :cond_19
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const v3, 0x7f080e77

    goto :goto_a

    .line 59
    :cond_1a
    :goto_8
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    goto :goto_a

    .line 60
    :cond_1b
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_1c

    goto :goto_9

    :cond_1c
    const v3, 0x7f08072e

    goto :goto_a

    .line 61
    :cond_1d
    :goto_9
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 62
    :goto_a
    invoke-direct {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto/16 :goto_d

    .line 63
    :cond_1e
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_21

    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v3

    if-nez v3, :cond_21

    .line 64
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_1f

    goto :goto_b

    :cond_1f
    const v0, 0x7f080716

    .line 65
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto :goto_d

    :cond_20
    :goto_b
    const v0, 0x7f080717

    .line 66
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto :goto_d

    .line 67
    :cond_21
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇O8〇〇o(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_25

    if-eqz v0, :cond_22

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    :cond_22
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 68
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v0

    if-eqz v0, :cond_23

    goto :goto_c

    :cond_23
    const v0, 0x7f08070c

    .line 69
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto :goto_d

    :cond_24
    :goto_c
    const v0, 0x7f08070d

    .line 70
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo(Landroid/widget/ImageView;ILjava/lang/Float;)V

    goto :goto_d

    :cond_25
    const/4 v0, 0x0

    .line 71
    invoke-static {v1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 72
    :cond_26
    :goto_d
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 73
    invoke-direct {p0, v0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oO〇(Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 74
    :cond_27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0ooO()Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_28

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p2

    .line 75
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇000〇〇08(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Landroid/widget/ImageView;Lcom/intsig/camscanner/datastruct/DocItem;IILjava/lang/Object;)V

    :cond_28
    return-void
.end method

.method private final 〇〇〇0〇〇0(Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Ljava/util/Map$Entry;

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8〇o(Ljava/lang/String;)Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    if-eqz v1, :cond_0

    .line 42
    .line 43
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->oo〇()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    new-instance v4, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v5, "getDocsThumbFilePaths fileType:"

    .line 59
    .line 60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v2, ",officeFirstPageId:"

    .line 67
    .line 68
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    sget-object v2, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o0ooO()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 95
    .line 96
    .line 97
    move-result v3

    .line 98
    if-nez v3, :cond_0

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v3

    .line 104
    new-instance v4, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v5, "down pdf thumb docSyncId: "

    .line 110
    .line 111
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v3, " \uff0cofficeThumbPath\uff1a"

    .line 118
    .line 119
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v2

    .line 129
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8o:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 133
    .line 134
    new-instance v1, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;

    .line 135
    .line 136
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 137
    .line 138
    .line 139
    move-result-wide v5

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 141
    .line 142
    .line 143
    move-result-wide v7

    .line 144
    const/4 v9, 0x0

    .line 145
    iget-object v10, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 146
    .line 147
    const/4 v11, 0x4

    .line 148
    const/4 v12, 0x0

    .line 149
    move-object v4, v1

    .line 150
    invoke-direct/range {v4 .. v12}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadDocImgRequestTaskData;-><init>(JJILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 151
    .line 152
    .line 153
    const/4 v5, 0x0

    .line 154
    const/4 v6, 0x0

    .line 155
    const/4 v7, 0x6

    .line 156
    const/4 v8, 0x0

    .line 157
    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->o800o8O(Lcom/intsig/camscanner/tsapp/request/RequestTask;Lcom/intsig/camscanner/tsapp/request/RequestTaskData;ZZILjava/lang/Object;)V

    .line 158
    .line 159
    .line 160
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8o:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 161
    .line 162
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 163
    .line 164
    .line 165
    move-result-wide v1

    .line 166
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇oOO8O8(J)V

    .line 167
    .line 168
    .line 169
    sget-object v0, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;->〇8o8o〇:Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;

    .line 170
    .line 171
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;->〇080()Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8o:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 176
    .line 177
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/request/RequestTaskClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tsapp/request/RequestTask;)V

    .line 178
    .line 179
    .line 180
    goto/16 :goto_0

    .line 181
    .line 182
    :cond_1
    return-void
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public final O0o〇〇Oo()Z
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o〇〇8080()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Iterable;

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    const/4 v2, 0x0

    .line 15
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    if-eqz v3, :cond_1

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    check-cast v3, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80〇808〇O()Z

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    if-eqz v4, :cond_0

    .line 32
    .line 33
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 34
    .line 35
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 36
    .line 37
    .line 38
    move-result-wide v5

    .line 39
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    if-nez v3, :cond_0

    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 53
    .line 54
    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    if-ne v2, v0, :cond_2

    .line 59
    .line 60
    const/4 v1, 0x1

    .line 61
    :cond_2
    return v1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final OOO()Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOo8o〇O(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter$OnItemQuickFunClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Ooo()Ljava/util/concurrent/CopyOnWriteArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/CopyOnWriteArraySet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 11
    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v0, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O8oOo80()Landroidx/fragment/app/Fragment;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-direct {p2, p1, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;-><init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;Landroidx/fragment/app/Fragment;Z)V

    .line 38
    .line 39
    .line 40
    return-object p2
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final O〇0()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o〇〇8080()Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Ljava/lang/Iterable;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80〇808〇O()Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-eqz v2, :cond_0

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OO〇00〇8oO:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 42
    .line 43
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 44
    .line 45
    .line 46
    move-result-wide v3

    .line 47
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 48
    .line 49
    .line 50
    move-result-object v3

    .line 51
    invoke-virtual {v2, v3}, Ljava/util/concurrent/CopyOnWriteArraySet;->contains(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-nez v2, :cond_0

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 58
    .line 59
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 63
    .line 64
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 65
    .line 66
    .line 67
    move-result-wide v3

    .line 68
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final o8(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇08O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o8oO〇()Ljava/lang/Long;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇oO:Ljava/lang/Long;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const v0, 0x7f0d0434

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    const v0, 0x7f0d043e

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const v0, 0x7f0d043d

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 44
    .line 45
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_4

    .line 50
    .line 51
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    const v0, 0x7f0d0437

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_3
    const v0, 0x7f0d0436

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_4
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 68
    .line 69
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    if-eqz v1, :cond_5

    .line 74
    .line 75
    const v0, 0x7f0d043a

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_5
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 80
    .line 81
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    if-eqz v1, :cond_6

    .line 86
    .line 87
    const v0, 0x7f0d0435

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_6
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 92
    .line 93
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-eqz v0, :cond_7

    .line 98
    .line 99
    const v0, 0x7f0d0442

    .line 100
    .line 101
    .line 102
    :goto_0
    return v0

    .line 103
    :cond_7
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 104
    .line 105
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 106
    .line 107
    .line 108
    throw v0
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final oo(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 7
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 25
    .line 26
    .line 27
    move-result-wide v2

    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 29
    .line 30
    .line 31
    move-result-wide v4

    .line 32
    cmp-long v6, v2, v4

    .line 33
    .line 34
    if-nez v6, :cond_0

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 44
    .line 45
    .line 46
    move-result-wide v1

    .line 47
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const/4 v0, 0x0

    .line 57
    :goto_0
    if-nez v0, :cond_2

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 60
    .line 61
    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 67
    .line 68
    .line 69
    move-result-wide v1

    .line 70
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    .line 76
    .line 77
    :cond_2
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final ooo0〇O88O(Ljava/lang/Long;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇oO:Ljava/lang/Long;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇o()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "null cannot be cast to non-null type java.text.SimpleDateFormat"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Ljava/text/SimpleDateFormat;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toLocalizedPattern()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 25
    .line 26
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    const-string v0, "pattern"

    .line 33
    .line 34
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const-string v2, "yyyy"

    .line 38
    .line 39
    const-string v3, "yy"

    .line 40
    .line 41
    const/4 v4, 0x0

    .line 42
    const/4 v5, 0x4

    .line 43
    const/4 v6, 0x0

    .line 44
    invoke-static/range {v1 .. v6}, Lkotlin/text/StringsKt;->〇oOO8O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v1, " HH:mm"

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o8oOOo:Ljava/text/SimpleDateFormat;

    .line 66
    .line 67
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/DocMultiEntity;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇08O8o〇0(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇08O8o〇0(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/DocMultiEntity;)V
    .locals 18
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/DocMultiEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "helper"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "item"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 2
    move-object v5, v1

    check-cast v5, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 3
    move-object v6, v2

    check-cast v6, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 4
    sget-object v7, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    iget-object v8, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const-string v9, "helper.itemView"

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->Oo08(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;Landroid/view/View;)V

    .line 5
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo()Landroid/widget/TextView;

    move-result-object v1

    .line 6
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    move-result-object v7

    sget-object v8, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇8()I

    move-result v7

    if-ne v7, v9, :cond_1

    .line 7
    sget-object v7, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v7}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v7

    const v12, 0x7f080cd8

    invoke-static {v7, v12}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_0

    const/16 v12, 0x23

    .line 8
    invoke-static {v12}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    move-result v12

    float-to-int v12, v12

    const/16 v13, 0xd

    invoke-static {v13}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    move-result v13

    float-to-int v13, v13

    invoke-virtual {v7, v11, v11, v12, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 9
    new-instance v12, Lcom/intsig/camscanner/view/CenterSpaceImageSpan;

    const/high16 v13, 0x40800000    # 4.0f

    invoke-static {v13}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    move-result v13

    invoke-direct {v12, v7, v13, v11}, Lcom/intsig/camscanner/view/CenterSpaceImageSpan;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 10
    new-instance v7, Landroid/text/SpannableString;

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 11
    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v13

    sub-int/2addr v13, v10

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v14

    const/16 v15, 0x21

    invoke-virtual {v7, v12, v13, v14, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 12
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    sget-object v7, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    move-object v7, v8

    :goto_0
    if-nez v7, :cond_2

    .line 14
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 15
    :cond_1
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    :cond_2
    :goto_1
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0OO8〇0(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 17
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o8(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 18
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o08O()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    .line 19
    :cond_3
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O〇Oooo〇〇(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    goto :goto_3

    .line 20
    :cond_4
    :goto_2
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O〇O〇oO()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v5, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o80ooO(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Ljava/lang/String;)V

    .line 21
    :goto_3
    iget-object v1, v5, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 22
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O000()Z

    move-result v7

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3e99999a    # 0.3f

    if-eqz v7, :cond_5

    const/high16 v7, 0x3f800000    # 1.0f

    goto :goto_4

    :cond_5
    const v7, 0x3e99999a    # 0.3f

    :goto_4
    invoke-virtual {v1, v7}, Landroid/view/View;->setAlpha(F)V

    .line 23
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O000()Z

    move-result v7

    invoke-virtual {v1, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 24
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    if-nez v1, :cond_6

    goto :goto_5

    :cond_6
    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 25
    :goto_5
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0()Landroid/widget/TextView;

    move-result-object v1

    if-nez v1, :cond_7

    goto :goto_6

    :cond_7
    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 26
    :goto_6
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    move-result-object v1

    if-nez v1, :cond_8

    goto :goto_7

    :cond_8
    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 27
    :goto_7
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O000()Landroid/widget/TextView;

    move-result-object v1

    if-nez v1, :cond_9

    goto :goto_8

    :cond_9
    invoke-virtual {v1, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 28
    :goto_8
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0000OOO()Landroid/widget/CheckBox;

    move-result-object v1

    .line 29
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇0OOo〇0()Landroid/widget/LinearLayout;

    move-result-object v7

    .line 30
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇〇0〇〇0()Landroid/widget/ImageView;

    move-result-object v14

    if-nez v14, :cond_a

    goto :goto_a

    .line 31
    :cond_a
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80〇808〇O()Z

    move-result v15

    if-eqz v15, :cond_b

    goto :goto_9

    :cond_b
    const v12, 0x3e99999a    # 0.3f

    :goto_9
    invoke-virtual {v14, v12}, Landroid/view/View;->setAlpha(F)V

    .line 32
    :goto_a
    iget-object v12, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8o08O8O:Landroid/content/Context;

    .line 33
    instance-of v13, v12, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    if-eqz v13, :cond_d

    .line 34
    check-cast v12, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    invoke-virtual {v12}, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;->OoO〇OOo8o()Lcom/intsig/camscanner/movecopyactivity/action/IAction;

    move-result-object v12

    instance-of v12, v12, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInAction;

    if-eqz v12, :cond_c

    .line 35
    invoke-static {v7, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 36
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇0OOo〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    goto :goto_b

    .line 37
    :cond_c
    invoke-static {v7, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    :goto_b
    if-eqz v14, :cond_17

    .line 38
    invoke-static {v14, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto/16 :goto_d

    .line 39
    :cond_d
    instance-of v13, v12, Lcom/intsig/camscanner/searchactivity/SearchActivity;

    if-eqz v13, :cond_e

    .line 40
    invoke-static {v7, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    if-eqz v14, :cond_17

    .line 41
    invoke-static {v14, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto/16 :goto_d

    .line 42
    :cond_e
    instance-of v13, v12, Lcom/intsig/camscanner/doc2officeactivity/Doc2OfficeActivity;

    if-eqz v13, :cond_f

    const/4 v13, 0x1

    goto :goto_c

    .line 43
    :cond_f
    instance-of v13, v12, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    :goto_c
    if-eqz v13, :cond_15

    .line 44
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooOO(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 45
    invoke-static {v7}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    if-eqz v14, :cond_10

    .line 46
    invoke-static {v14}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    .line 47
    :cond_10
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-static {v1, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto :goto_d

    .line 48
    :cond_11
    iget-object v12, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v12}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->Oo〇()Z

    move-result v12

    if-eqz v12, :cond_13

    if-eqz v14, :cond_12

    .line 49
    invoke-static {v14, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 50
    :cond_12
    invoke-static {v7, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 51
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-static {v1, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto :goto_d

    .line 52
    :cond_13
    invoke-static {v7, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 53
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇0OOo〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/CompoundButton;->setChecked(Z)V

    if-eqz v14, :cond_14

    .line 54
    invoke-static {v14}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O00(Landroid/view/View;)V

    .line 55
    :cond_14
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v1

    if-eqz v1, :cond_17

    invoke-static {v1, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto :goto_d

    .line 56
    :cond_15
    instance-of v1, v12, Lcom/intsig/camscanner/targetdir/TargetDirActivity;

    if-eqz v1, :cond_17

    .line 57
    invoke-static {v7, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 58
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    move-result-object v1

    sget-object v7, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    if-eqz v14, :cond_17

    .line 59
    invoke-static {v14, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    goto :goto_d

    :cond_16
    if-eqz v14, :cond_17

    .line 60
    invoke-static {v14, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 61
    :cond_17
    :goto_d
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    move-result v1

    invoke-direct {v0, v5, v1, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooO〇00O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;ILcom/intsig/camscanner/datastruct/DocItem;)V

    .line 62
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇o(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 63
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;

    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocItemProviderNewView;->〇080()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 64
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->oO()J

    move-result-wide v1

    goto :goto_e

    .line 65
    :cond_18
    iget v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇08O:I

    if-gt v1, v10, :cond_19

    .line 66
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    move-result-wide v1

    goto :goto_e

    :cond_19
    const/4 v2, 0x3

    if-gt v1, v2, :cond_1a

    .line 67
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    move-result-wide v1

    goto :goto_e

    .line 68
    :cond_1a
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    move-result-wide v1

    .line 69
    :goto_e
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o88〇OO08〇()Landroid/widget/TextView;

    move-result-object v7

    if-nez v7, :cond_1b

    goto :goto_f

    :cond_1b
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇08O8o〇0()I

    move-result v12

    invoke-direct {v0, v1, v2, v12}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇8〇0〇o〇O(JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    :goto_f
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8ooOoo〇()Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v1, 0x0

    goto :goto_10

    :cond_1c
    const/4 v1, 0x4

    :goto_10
    new-array v2, v9, [Landroid/view/View;

    .line 71
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O08000()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v7

    aput-object v7, v2, v11

    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8oO〇()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v7

    aput-object v7, v2, v10

    .line 72
    invoke-static {v1, v2}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 73
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8ooOoo〇()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 74
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 75
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->〇O00()J

    move-result-wide v12

    invoke-direct {v2, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 76
    invoke-virtual {v1, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v7, 0x5

    .line 77
    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 78
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O08000()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v7

    if-nez v7, :cond_1d

    goto :goto_11

    :cond_1d
    new-array v9, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v9, v11

    invoke-static {v9, v10}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    const-string v9, "%02d"

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v9, "format(this, *args)"

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :goto_11
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8oO〇()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v1

    if-nez v1, :cond_1e

    goto :goto_12

    :cond_1e
    add-int/2addr v2, v10

    invoke-static {v2}, Lcom/intsig/camscanner/mainmenu/folder/scenario/ScenarioDirUtilKt;->〇080(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :cond_1f
    :goto_12
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇oOO8O8()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_20

    new-instance v2, LOOo/〇〇808〇;

    invoke-direct {v2, v0, v6}, LOOo/〇〇808〇;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;Lcom/intsig/camscanner/datastruct/DocItem;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    :cond_20
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇0(Lcom/intsig/camscanner/datastruct/DocItem;)Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->OoO8()Z

    move-result v1

    if-eqz v1, :cond_21

    const/4 v1, 0x1

    goto :goto_13

    :cond_21
    const/4 v1, 0x0

    :goto_13
    if-eqz v1, :cond_22

    .line 82
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0O:Ljava/util/Set;

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 83
    sget-object v2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->o88O〇8()I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇O8o08O(I)V

    .line 84
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O0O:Ljava/util/Set;

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_22
    const/16 v2, 0x8

    if-eqz v1, :cond_23

    const/4 v7, 0x0

    goto :goto_14

    :cond_23
    const/16 v7, 0x8

    :goto_14
    new-array v9, v10, [Landroid/view/View;

    .line 85
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇oOO8O8()Landroid/view/View;

    move-result-object v12

    aput-object v12, v9, v11

    .line 86
    invoke-static {v7, v9}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    if-eqz v1, :cond_24

    .line 87
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0〇oo()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_24
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oo〇()Lcom/intsig/view/TagLinearLayout;

    move-result-object v1

    if-eqz v1, :cond_26

    .line 89
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O〇O〇oO()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_25

    const-string v7, "|"

    filled-new-array {v7}, [Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x6

    const/16 v17, 0x0

    invoke-static/range {v12 .. v17}, Lkotlin/text/StringsKt;->Oo(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v8

    :cond_25
    invoke-direct {v0, v8, v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O8O〇88oO0(Ljava/util/List;Lcom/intsig/view/TagLinearLayout;)V

    .line 90
    :cond_26
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->o〇00O:Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇008〇oo()Lcom/intsig/camscanner/adapter/QueryInterface;

    move-result-object v1

    if-eqz v1, :cond_2a

    .line 91
    invoke-interface {v1}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇o00〇〇Oo()I

    move-result v7

    if-ne v7, v10, :cond_2a

    invoke-interface {v1}, Lcom/intsig/camscanner/adapter/QueryInterface;->〇080()[Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_29

    array-length v7, v7

    if-nez v7, :cond_27

    const/4 v7, 0x1

    goto :goto_15

    :cond_27
    const/4 v7, 0x0

    :goto_15
    if-eqz v7, :cond_28

    goto :goto_16

    :cond_28
    const/4 v10, 0x0

    :cond_29
    :goto_16
    if-nez v10, :cond_2a

    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7, v1, v6, v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->O〇OO(Landroid/content/Context;Lcom/intsig/camscanner/adapter/QueryInterface;Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;)V

    .line 93
    :cond_2a
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->OOo0O(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 94
    invoke-direct {v0, v5, v6}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->Oo8Oo00oo(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 95
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo〇8oO()Landroid/widget/TextView;

    move-result-object v1

    if-nez v1, :cond_2b

    goto :goto_18

    :cond_2b
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->oO00OOO()Z

    move-result v5

    if-eqz v5, :cond_2c

    goto :goto_17

    :cond_2c
    const/16 v11, 0x8

    .line 96
    :goto_17
    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    .line 97
    :goto_18
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->〇〇o〇:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "convert cost time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final 〇O〇80o08O()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇0o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo〇8o008:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->oOo0:Ljava/util/Set;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/16 v0, 0xb

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;I)V
    .locals 3
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "viewHolder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->〇〇8O0〇8(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;I)V

    .line 7
    .line 8
    .line 9
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    if-eqz p2, :cond_0

    .line 13
    .line 14
    move-object v1, p1

    .line 15
    check-cast v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    move-object v1, v0

    .line 19
    :goto_0
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;->ooo0〇〇O:Landroid/view/View$OnClickListener;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    .line 31
    .line 32
    :cond_1
    if-eqz p2, :cond_2

    .line 33
    .line 34
    move-object v1, p1

    .line 35
    check-cast v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    move-object v1, v0

    .line 39
    :goto_1
    if-eqz v1, :cond_3

    .line 40
    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0()Landroid/widget/TextView;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-eqz v1, :cond_3

    .line 46
    .line 47
    new-instance v2, LOOo/〇O00;

    .line 48
    .line 49
    invoke-direct {v2, p0}, LOOo/〇O00;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    .line 54
    .line 55
    :cond_3
    if-eqz p2, :cond_4

    .line 56
    .line 57
    move-object v1, p1

    .line 58
    check-cast v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_4
    move-object v1, v0

    .line 62
    :goto_2
    if-eqz v1, :cond_5

    .line 63
    .line 64
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O8OO088()Landroid/widget/TextView;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    if-eqz v1, :cond_5

    .line 69
    .line 70
    new-instance v2, LOOo/〇〇8O0〇8;

    .line 71
    .line 72
    invoke-direct {v2, p0}, LOOo/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    :cond_5
    if-eqz p2, :cond_6

    .line 79
    .line 80
    move-object v0, p1

    .line 81
    check-cast v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;

    .line 82
    .line 83
    :cond_6
    if-eqz v0, :cond_7

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O000()Landroid/widget/TextView;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    if-eqz p1, :cond_7

    .line 90
    .line 91
    new-instance p2, LOOo/〇0〇O0088o;

    .line 92
    .line 93
    invoke-direct {p2, p0}, LOOo/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    .line 98
    .line 99
    :cond_7
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

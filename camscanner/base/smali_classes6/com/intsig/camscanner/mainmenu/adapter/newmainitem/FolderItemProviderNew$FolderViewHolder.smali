.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "FolderItemProviderNew.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FolderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Landroid/widget/TextView;

.field private final o0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Landroid/widget/TextView;

.field private oOo0:Landroid/widget/ImageView;

.field private oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Landroid/widget/CheckBox;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Landroid/widget/LinearLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Landroid/widget/ImageView;

.field private 〇OOo8〇0:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 13
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docAdapter"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o8O〇O()Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o0:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;

    .line 19
    .line 20
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 21
    .line 22
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const-string v2, "binding.clRoot"

    .line 27
    .line 28
    const-string v3, "binding.ivFolderBottomIcon"

    .line 29
    .line 30
    const-string v4, "binding.llFolderCheckbox"

    .line 31
    .line 32
    const-string v5, "binding.checkboxFolder"

    .line 33
    .line 34
    const-string v6, "binding.ivFolderOpeMore"

    .line 35
    .line 36
    const-string v7, "binding.tvDocNum"

    .line 37
    .line 38
    const-string v8, "binding.tvFolderName"

    .line 39
    .line 40
    const-string v9, "binding.ivFolderIcon"

    .line 41
    .line 42
    const-string v10, "bind(convertView)"

    .line 43
    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 47
    .line 48
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 49
    .line 50
    .line 51
    move-result p2

    .line 52
    if-eqz p2, :cond_0

    .line 53
    .line 54
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 62
    .line 63
    invoke-static {p2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 67
    .line 68
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 69
    .line 70
    invoke-static {p2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 74
    .line 75
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 76
    .line 77
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 81
    .line 82
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 83
    .line 84
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 88
    .line 89
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 90
    .line 91
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 95
    .line 96
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 97
    .line 98
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 102
    .line 103
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 104
    .line 105
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 109
    .line 110
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 111
    .line 112
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 116
    .line 117
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->oOo0:Landroid/widget/TextView;

    .line 118
    .line 119
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 120
    .line 121
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 122
    .line 123
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 124
    .line 125
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeUpgradeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 126
    .line 127
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 128
    .line 129
    goto/16 :goto_3

    .line 130
    .line 131
    :cond_0
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    invoke-static {p1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 139
    .line 140
    invoke-static {p2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 144
    .line 145
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 146
    .line 147
    invoke-static {p2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 151
    .line 152
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 153
    .line 154
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 158
    .line 159
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 160
    .line 161
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 165
    .line 166
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 167
    .line 168
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 172
    .line 173
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 174
    .line 175
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 179
    .line 180
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 181
    .line 182
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 186
    .line 187
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 188
    .line 189
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    .line 191
    .line 192
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 193
    .line 194
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 195
    .line 196
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 197
    .line 198
    goto/16 :goto_3

    .line 199
    .line 200
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 201
    .line 202
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 203
    .line 204
    .line 205
    move-result v1

    .line 206
    const/4 v11, 0x1

    .line 207
    if-eqz v1, :cond_2

    .line 208
    .line 209
    const/4 v1, 0x1

    .line 210
    goto :goto_0

    .line 211
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 212
    .line 213
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 214
    .line 215
    .line 216
    move-result v1

    .line 217
    :goto_0
    if-eqz v1, :cond_3

    .line 218
    .line 219
    const/4 v1, 0x1

    .line 220
    goto :goto_1

    .line 221
    :cond_3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 222
    .line 223
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 224
    .line 225
    .line 226
    move-result v1

    .line 227
    :goto_1
    if-eqz v1, :cond_6

    .line 228
    .line 229
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 230
    .line 231
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 232
    .line 233
    .line 234
    move-result-object v0

    .line 235
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 236
    .line 237
    .line 238
    move-result-object v0

    .line 239
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 244
    .line 245
    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 246
    .line 247
    sget-object v12, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 248
    .line 249
    invoke-virtual {v12, v0, p2}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇o00〇〇Oo(ILcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)Landroid/view/ViewGroup$MarginLayoutParams;

    .line 250
    .line 251
    .line 252
    move-result-object p2

    .line 253
    invoke-virtual {v1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    .line 255
    .line 256
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 257
    .line 258
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 259
    .line 260
    .line 261
    move-result v0

    .line 262
    if-eqz v0, :cond_4

    .line 263
    .line 264
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    invoke-static {p1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 272
    .line 273
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 277
    .line 278
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 279
    .line 280
    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 284
    .line 285
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->oOo0:Landroid/widget/TextView;

    .line 286
    .line 287
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 291
    .line 292
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->〇0O:Landroid/widget/ImageView;

    .line 293
    .line 294
    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    .line 296
    .line 297
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 298
    .line 299
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 300
    .line 301
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    .line 303
    .line 304
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 305
    .line 306
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 307
    .line 308
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    .line 310
    .line 311
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 312
    .line 313
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 314
    .line 315
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    .line 317
    .line 318
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 319
    .line 320
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 321
    .line 322
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 326
    .line 327
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 328
    .line 329
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo0:Landroid/widget/ImageView;

    .line 330
    .line 331
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 332
    .line 333
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 334
    .line 335
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeUpgradeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 336
    .line 337
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 338
    .line 339
    goto :goto_2

    .line 340
    :cond_4
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;

    .line 341
    .line 342
    .line 343
    move-result-object p1

    .line 344
    invoke-static {p1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    .line 346
    .line 347
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 348
    .line 349
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 350
    .line 351
    .line 352
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 353
    .line 354
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 355
    .line 356
    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    .line 358
    .line 359
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 360
    .line 361
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 362
    .line 363
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    .line 365
    .line 366
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 367
    .line 368
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 369
    .line 370
    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    .line 372
    .line 373
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 374
    .line 375
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 376
    .line 377
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    .line 379
    .line 380
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 381
    .line 382
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 383
    .line 384
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 385
    .line 386
    .line 387
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 388
    .line 389
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 390
    .line 391
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    .line 393
    .line 394
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 395
    .line 396
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 397
    .line 398
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    .line 400
    .line 401
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 402
    .line 403
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 404
    .line 405
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo0:Landroid/widget/ImageView;

    .line 406
    .line 407
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 408
    .line 409
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 410
    .line 411
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 412
    .line 413
    .line 414
    move-result p1

    .line 415
    if-eqz p1, :cond_7

    .line 416
    .line 417
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 418
    .line 419
    const/high16 v0, 0x41400000    # 12.0f

    .line 420
    .line 421
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 422
    .line 423
    .line 424
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 425
    .line 426
    invoke-virtual {p1, v11}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 427
    .line 428
    .line 429
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 430
    .line 431
    .line 432
    move-result p1

    .line 433
    if-eqz p1, :cond_5

    .line 434
    .line 435
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 436
    .line 437
    const p2, 0x7f081199

    .line 438
    .line 439
    .line 440
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 441
    .line 442
    .line 443
    goto :goto_3

    .line 444
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 445
    .line 446
    const p2, 0x7f08119a    # 1.808664E38f

    .line 447
    .line 448
    .line 449
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 450
    .line 451
    .line 452
    goto :goto_3

    .line 453
    :cond_6
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 454
    .line 455
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 456
    .line 457
    .line 458
    move-result p2

    .line 459
    if-eqz p2, :cond_8

    .line 460
    .line 461
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;

    .line 462
    .line 463
    .line 464
    move-result-object p1

    .line 465
    invoke-static {p1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 466
    .line 467
    .line 468
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 469
    .line 470
    invoke-static {p2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 471
    .line 472
    .line 473
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 474
    .line 475
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 476
    .line 477
    invoke-static {p2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 478
    .line 479
    .line 480
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 481
    .line 482
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 483
    .line 484
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 485
    .line 486
    .line 487
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 488
    .line 489
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 490
    .line 491
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    .line 493
    .line 494
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 495
    .line 496
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->OO:Landroid/widget/CheckBox;

    .line 497
    .line 498
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 499
    .line 500
    .line 501
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 502
    .line 503
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 504
    .line 505
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    .line 507
    .line 508
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 509
    .line 510
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 511
    .line 512
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 513
    .line 514
    .line 515
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 516
    .line 517
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 518
    .line 519
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 520
    .line 521
    .line 522
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 523
    .line 524
    :cond_7
    :goto_3
    return-void

    .line 525
    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 526
    .line 527
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 528
    .line 529
    .line 530
    throw p1
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method


# virtual methods
.method public final O8ooOoo〇()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8〇o()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo〇8o008:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇0O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->oOo0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Landroid/widget/CheckBox;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇080OO8〇0:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00〇8()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderItemProviderNew$FolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

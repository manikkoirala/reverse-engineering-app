.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;
.super Lcom/intsig/camscanner/mainmenu/adapter/GlideClearViewHolder;
.source "DocItemProviderNew.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O0O:Lcom/intsig/view/NinePhotoView;

.field private O88O:Landroidx/appcompat/widget/AppCompatImageView;

.field private O8o08O8O:Landroid/widget/ImageView;

.field private OO:Landroid/widget/ImageView;

.field private OO〇00〇8oO:Landroid/widget/ImageView;

.field private Oo0〇Ooo:Landroid/widget/TextView;

.field private Oo80:Landroid/widget/TextView;

.field private Ooo08:Landroid/view/View;

.field private O〇08oOOO0:Landroid/widget/TextView;

.field private O〇o88o08〇:Landroid/widget/ImageView;

.field private final o0:Landroidx/fragment/app/Fragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0OoOOo0:Landroid/widget/TextView;

.field private o8o:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private o8oOOo:Landroid/view/View;

.field private o8〇OO:Landroid/view/View;

.field private o8〇OO0〇0o:Landroid/widget/TextView;

.field private oO00〇o:Landroid/widget/TextView;

.field private oOO0880O:Landroid/view/View;

.field private oOO〇〇:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private oOo0:Landroid/widget/LinearLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Landroid/widget/CheckBox;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO〇8O8oOo:Landroid/widget/TextView;

.field private oo8ooo8O:Landroidx/appcompat/widget/AppCompatImageView;

.field private ooO:Landroid/widget/ImageView;

.field private ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private o〇00O:Landroid/widget/TextView;

.field private o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

.field private o〇o〇Oo88:Landroid/widget/TextView;

.field private 〇00O0:Landroid/view/View;

.field private 〇080OO8〇0:Landroid/widget/ImageView;

.field private 〇08O〇00〇o:Landroid/widget/ImageView;

.field private 〇08〇o0O:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private 〇0O:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O〇O00O:Landroid/view/ViewGroup;

.field private 〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

.field private 〇OO8ooO8〇:Landroid/widget/TextView;

.field private final 〇OOo8〇0:Z

.field private 〇OO〇00〇0O:Landroid/widget/TextView;

.field private 〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

.field private 〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

.field private 〇〇08O:Landroid/widget/LinearLayout;

.field private 〇〇o〇:Landroid/widget/TextView;

.field private 〇〇〇0o〇〇0:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;Landroidx/fragment/app/Fragment;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroidx/fragment/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docViewMode"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "mFragment"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/adapter/GlideClearViewHolder;-><init>(Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0:Landroidx/fragment/app/Fragment;

    .line 20
    .line 21
    iput-boolean p4, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OOo8〇0:Z

    .line 22
    .line 23
    sget-object p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 24
    .line 25
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result p3

    .line 29
    const-string v0, "binding.tvDocName"

    .line 30
    .line 31
    const-string v1, "binding.checkboxDoc"

    .line 32
    .line 33
    const-string v2, "binding.llDocCheckbox"

    .line 34
    .line 35
    const-string v3, "bind(convertView)"

    .line 36
    .line 37
    if-eqz p3, :cond_2

    .line 38
    .line 39
    if-eqz p4, :cond_0

    .line 40
    .line 41
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->o8〇OO0〇0o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 49
    .line 50
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 51
    .line 52
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 53
    .line 54
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 55
    .line 56
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 57
    .line 58
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 59
    .line 60
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 61
    .line 62
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 63
    .line 64
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 65
    .line 66
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 67
    .line 68
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 69
    .line 70
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 74
    .line 75
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 76
    .line 77
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 78
    .line 79
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->oOo0:Landroid/widget/ImageView;

    .line 80
    .line 81
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 82
    .line 83
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->OO〇00〇8oO:Landroid/widget/LinearLayout;

    .line 84
    .line 85
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 89
    .line 90
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocBackupListModeDocTypeBinding;->OO:Landroid/widget/CheckBox;

    .line 91
    .line 92
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 96
    .line 97
    goto/16 :goto_0

    .line 98
    .line 99
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 100
    .line 101
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 102
    .line 103
    .line 104
    move-result p2

    .line 105
    if-eqz p2, :cond_1

    .line 106
    .line 107
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 115
    .line 116
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 117
    .line 118
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇〇08O:Landroid/widget/ImageView;

    .line 119
    .line 120
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 121
    .line 122
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 123
    .line 124
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 125
    .line 126
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇08〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 127
    .line 128
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 132
    .line 133
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 134
    .line 135
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 136
    .line 137
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 138
    .line 139
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 143
    .line 144
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇O〇〇O8:Landroid/widget/LinearLayout;

    .line 145
    .line 146
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 150
    .line 151
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->O0O:Landroid/widget/ImageView;

    .line 152
    .line 153
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 154
    .line 155
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->Oo80:Landroid/widget/TextView;

    .line 156
    .line 157
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 158
    .line 159
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 160
    .line 161
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 162
    .line 163
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇080OO8〇0:Lcom/intsig/view/TagLinearLayout;

    .line 164
    .line 165
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 166
    .line 167
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇o0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 168
    .line 169
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 170
    .line 171
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->o8o:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 172
    .line 173
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08〇o0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 174
    .line 175
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇00O0:Landroid/widget/TextView;

    .line 176
    .line 177
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇o〇:Landroid/widget/TextView;

    .line 178
    .line 179
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->O〇08oOOO0:Landroid/widget/TextView;

    .line 180
    .line 181
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo80:Landroid/widget/TextView;

    .line 182
    .line 183
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 184
    .line 185
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇o88o08〇:Landroid/widget/ImageView;

    .line 186
    .line 187
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00O0:Landroid/view/View;

    .line 188
    .line 189
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 190
    .line 191
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo08:Landroid/view/View;

    .line 192
    .line 193
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->o〇oO:Landroid/widget/TextView;

    .line 194
    .line 195
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OO8ooO8〇:Landroid/widget/TextView;

    .line 196
    .line 197
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->oOo0:Landroid/widget/ImageView;

    .line 198
    .line 199
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO:Landroid/view/View;

    .line 200
    .line 201
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 202
    .line 203
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO〇8O8oOo:Landroid/widget/TextView;

    .line 204
    .line 205
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->oOO〇〇:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 206
    .line 207
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O〇O00O:Landroid/view/ViewGroup;

    .line 208
    .line 209
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->o8〇OO:Landroid/widget/TextView;

    .line 210
    .line 211
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0OoOOo0:Landroid/widget/TextView;

    .line 212
    .line 213
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->〇OO8ooO8〇:Landroid/widget/TextView;

    .line 214
    .line 215
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 216
    .line 217
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 218
    .line 219
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO00〇o:Landroid/widget/TextView;

    .line 220
    .line 221
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->ooO:Landroid/view/View;

    .line 222
    .line 223
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOO0880O:Landroid/view/View;

    .line 224
    .line 225
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeUpgradeBinding;->Ooo08:Landroid/widget/TextView;

    .line 226
    .line 227
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇08oOOO0:Landroid/widget/TextView;

    .line 228
    .line 229
    goto/16 :goto_0

    .line 230
    .line 231
    :cond_1
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatImageView;

    .line 239
    .line 240
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 241
    .line 242
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 243
    .line 244
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 245
    .line 246
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 247
    .line 248
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 249
    .line 250
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 251
    .line 252
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 256
    .line 257
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 258
    .line 259
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 260
    .line 261
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 262
    .line 263
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 267
    .line 268
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇〇08O:Landroid/widget/LinearLayout;

    .line 269
    .line 270
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 274
    .line 275
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 276
    .line 277
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 278
    .line 279
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->o〇oO:Landroid/widget/TextView;

    .line 280
    .line 281
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 282
    .line 283
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->oOo0:Landroid/widget/ImageView;

    .line 284
    .line 285
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 286
    .line 287
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->o〇00O:Lcom/intsig/view/TagLinearLayout;

    .line 288
    .line 289
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 290
    .line 291
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->O0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 292
    .line 293
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 294
    .line 295
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇o0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 296
    .line 297
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08〇o0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 298
    .line 299
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 300
    .line 301
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇o〇:Landroid/widget/TextView;

    .line 302
    .line 303
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->Oo80:Landroid/widget/TextView;

    .line 304
    .line 305
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo80:Landroid/widget/TextView;

    .line 306
    .line 307
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 308
    .line 309
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇o88o08〇:Landroid/widget/ImageView;

    .line 310
    .line 311
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00O0:Landroid/view/View;

    .line 312
    .line 313
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 314
    .line 315
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo08:Landroid/view/View;

    .line 316
    .line 317
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->oOO〇〇:Landroid/widget/TextView;

    .line 318
    .line 319
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OO8ooO8〇:Landroid/widget/TextView;

    .line 320
    .line 321
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 322
    .line 323
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO:Landroid/view/View;

    .line 324
    .line 325
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇08〇o0O:Landroid/widget/TextView;

    .line 326
    .line 327
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO〇8O8oOo:Landroid/widget/TextView;

    .line 328
    .line 329
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 330
    .line 331
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O〇O00O:Landroid/view/ViewGroup;

    .line 332
    .line 333
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 334
    .line 335
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0OoOOo0:Landroid/widget/TextView;

    .line 336
    .line 337
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->O〇08oOOO0:Landroid/widget/TextView;

    .line 338
    .line 339
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 340
    .line 341
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->O88O:Landroid/widget/TextView;

    .line 342
    .line 343
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO00〇o:Landroid/widget/TextView;

    .line 344
    .line 345
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->o8〇OO:Landroid/view/View;

    .line 346
    .line 347
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOO0880O:Landroid/view/View;

    .line 348
    .line 349
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeDocTypeBinding;->〇00O0:Landroid/widget/TextView;

    .line 350
    .line 351
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇08oOOO0:Landroid/widget/TextView;

    .line 352
    .line 353
    goto/16 :goto_0

    .line 354
    .line 355
    :cond_2
    sget-object p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 356
    .line 357
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 358
    .line 359
    .line 360
    move-result p3

    .line 361
    if-eqz p3, :cond_4

    .line 362
    .line 363
    sget-object p2, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;

    .line 364
    .line 365
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/adapter/DocItemLayoutSetUtil;->O8()Z

    .line 366
    .line 367
    .line 368
    move-result p2

    .line 369
    const/4 p3, 0x2

    .line 370
    const/4 p4, 0x0

    .line 371
    if-eqz p2, :cond_3

    .line 372
    .line 373
    iget-object p2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 374
    .line 375
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 376
    .line 377
    .line 378
    move-result-object p2

    .line 379
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 380
    .line 381
    .line 382
    move-result-object p2

    .line 383
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 384
    .line 385
    .line 386
    move-result-object p2

    .line 387
    iget p2, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 388
    .line 389
    iget-object v4, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 390
    .line 391
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 392
    .line 393
    invoke-static {v5, p2, p4, p3, p4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇o〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;ILcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ILjava/lang/Object;)Landroid/view/ViewGroup$MarginLayoutParams;

    .line 394
    .line 395
    .line 396
    move-result-object p2

    .line 397
    invoke-virtual {v4, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 398
    .line 399
    .line 400
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;

    .line 401
    .line 402
    .line 403
    move-result-object p1

    .line 404
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    .line 406
    .line 407
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 408
    .line 409
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 410
    .line 411
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->〇0O:Landroid/widget/ImageView;

    .line 412
    .line 413
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 414
    .line 415
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 416
    .line 417
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 418
    .line 419
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 420
    .line 421
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO:Landroid/view/View;

    .line 422
    .line 423
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 424
    .line 425
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    .line 427
    .line 428
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 429
    .line 430
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 431
    .line 432
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 433
    .line 434
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 435
    .line 436
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    .line 438
    .line 439
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 440
    .line 441
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->oOo0:Landroid/widget/LinearLayout;

    .line 442
    .line 443
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 444
    .line 445
    .line 446
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 447
    .line 448
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 449
    .line 450
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 451
    .line 452
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 453
    .line 454
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 455
    .line 456
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->OO:Lcom/intsig/view/TagLinearLayout;

    .line 457
    .line 458
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 459
    .line 460
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->OO〇00〇8oO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 461
    .line 462
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 463
    .line 464
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeUpgradeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 465
    .line 466
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 467
    .line 468
    goto/16 :goto_0

    .line 469
    .line 470
    :cond_3
    iget-object p2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 471
    .line 472
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 473
    .line 474
    .line 475
    move-result-object p2

    .line 476
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 477
    .line 478
    .line 479
    move-result-object p2

    .line 480
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 481
    .line 482
    .line 483
    move-result-object p2

    .line 484
    iget p2, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 485
    .line 486
    iget-object v4, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 487
    .line 488
    sget-object v5, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 489
    .line 490
    invoke-static {v5, p2, p4, p3, p4}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇o〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;ILcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ILjava/lang/Object;)Landroid/view/ViewGroup$MarginLayoutParams;

    .line 491
    .line 492
    .line 493
    move-result-object p2

    .line 494
    invoke-virtual {v4, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 495
    .line 496
    .line 497
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;

    .line 498
    .line 499
    .line 500
    move-result-object p1

    .line 501
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    .line 503
    .line 504
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 505
    .line 506
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 507
    .line 508
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 509
    .line 510
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 511
    .line 512
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 513
    .line 514
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 515
    .line 516
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 517
    .line 518
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO:Landroid/view/View;

    .line 519
    .line 520
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 521
    .line 522
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    .line 524
    .line 525
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 526
    .line 527
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 528
    .line 529
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 530
    .line 531
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 532
    .line 533
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 534
    .line 535
    .line 536
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 537
    .line 538
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->oOo0:Landroid/widget/LinearLayout;

    .line 539
    .line 540
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 541
    .line 542
    .line 543
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 544
    .line 545
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 546
    .line 547
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 548
    .line 549
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 550
    .line 551
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 552
    .line 553
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->OO:Lcom/intsig/view/TagLinearLayout;

    .line 554
    .line 555
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 556
    .line 557
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->OO〇00〇8oO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 558
    .line 559
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 560
    .line 561
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeDocTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 562
    .line 563
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 564
    .line 565
    goto/16 :goto_0

    .line 566
    .line 567
    :cond_4
    sget-object p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 568
    .line 569
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 570
    .line 571
    .line 572
    move-result p3

    .line 573
    if-eqz p3, :cond_5

    .line 574
    .line 575
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;

    .line 576
    .line 577
    .line 578
    move-result-object p1

    .line 579
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 580
    .line 581
    .line 582
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 583
    .line 584
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇08O:Landroid/widget/LinearLayout;

    .line 585
    .line 586
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 587
    .line 588
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 589
    .line 590
    .line 591
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 592
    .line 593
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->OO:Landroid/widget/CheckBox;

    .line 594
    .line 595
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    .line 597
    .line 598
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 599
    .line 600
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 601
    .line 602
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 603
    .line 604
    .line 605
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 606
    .line 607
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 608
    .line 609
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 610
    .line 611
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 612
    .line 613
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 614
    .line 615
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->〇08O〇00〇o:Lcom/intsig/view/TagLinearLayout;

    .line 616
    .line 617
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 618
    .line 619
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeDocTypeBinding;->〇0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 620
    .line 621
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 622
    .line 623
    goto/16 :goto_0

    .line 624
    .line 625
    :cond_5
    sget-object p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$CardBagMode;

    .line 626
    .line 627
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 628
    .line 629
    .line 630
    move-result p3

    .line 631
    if-eqz p3, :cond_6

    .line 632
    .line 633
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;

    .line 634
    .line 635
    .line 636
    move-result-object p1

    .line 637
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 638
    .line 639
    .line 640
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 641
    .line 642
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 643
    .line 644
    .line 645
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 646
    .line 647
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->OO:Landroid/widget/CheckBox;

    .line 648
    .line 649
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 650
    .line 651
    .line 652
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 653
    .line 654
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 655
    .line 656
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 657
    .line 658
    .line 659
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 660
    .line 661
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->〇0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 662
    .line 663
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 664
    .line 665
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 666
    .line 667
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooO:Landroid/widget/ImageView;

    .line 668
    .line 669
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 670
    .line 671
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OO〇00〇0O:Landroid/widget/TextView;

    .line 672
    .line 673
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 674
    .line 675
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 676
    .line 677
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 678
    .line 679
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇〇0o〇〇0:Landroid/widget/ImageView;

    .line 680
    .line 681
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocCardBagModeDocTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 682
    .line 683
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 684
    .line 685
    goto :goto_0

    .line 686
    :cond_6
    sget-object p3, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$TimeLineMode;

    .line 687
    .line 688
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 689
    .line 690
    .line 691
    move-result p2

    .line 692
    if-eqz p2, :cond_7

    .line 693
    .line 694
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;

    .line 695
    .line 696
    .line 697
    move-result-object p1

    .line 698
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 699
    .line 700
    .line 701
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;

    .line 702
    .line 703
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 704
    .line 705
    const-string p3, "binding.ilTimeLineDefaul\u2026ddleView.atvTimelineTitle"

    .line 706
    .line 707
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 708
    .line 709
    .line 710
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 711
    .line 712
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;

    .line 713
    .line 714
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 715
    .line 716
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8oOOo:Landroid/view/View;

    .line 717
    .line 718
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 719
    .line 720
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 721
    .line 722
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->o〇00O:Landroid/widget/CheckBox;

    .line 723
    .line 724
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 725
    .line 726
    .line 727
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 728
    .line 729
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 730
    .line 731
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 732
    .line 733
    .line 734
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 735
    .line 736
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 737
    .line 738
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O88O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 739
    .line 740
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;

    .line 741
    .line 742
    iget-object p3, p2, Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;->o〇00O:Lcom/intsig/view/NinePhotoView;

    .line 743
    .line 744
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O:Lcom/intsig/view/NinePhotoView;

    .line 745
    .line 746
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 747
    .line 748
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 749
    .line 750
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 751
    .line 752
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 753
    .line 754
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇0O:Lcom/intsig/camscanner/databinding/ItemTimeLineOfficeMiddleViewBinding;

    .line 755
    .line 756
    iget-object p4, p3, Lcom/intsig/camscanner/databinding/ItemTimeLineOfficeMiddleViewBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 757
    .line 758
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 759
    .line 760
    iget-object p3, p3, Lcom/intsig/camscanner/databinding/ItemTimeLineOfficeMiddleViewBinding;->OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 761
    .line 762
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 763
    .line 764
    invoke-virtual {p2}, Lcom/intsig/camscanner/databinding/ItemTimeLineDefaultMiddleViewBinding;->〇080()Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 765
    .line 766
    .line 767
    move-result-object p2

    .line 768
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOO〇〇:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 769
    .line 770
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocTimeLineModeDocTypeBinding;->〇0O:Lcom/intsig/camscanner/databinding/ItemTimeLineOfficeMiddleViewBinding;

    .line 771
    .line 772
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ItemTimeLineOfficeMiddleViewBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 773
    .line 774
    .line 775
    move-result-object p1

    .line 776
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 777
    .line 778
    :goto_0
    return-void

    .line 779
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 780
    .line 781
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 782
    .line 783
    .line 784
    throw p1
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method


# virtual methods
.method public final O000()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO00〇o:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O08000()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O0O8OO088()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇o〇Oo88:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O0o〇〇Oo()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8O〇()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇08oOOO0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8ooOoo〇()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O〇o88o08〇:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8〇o()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO8oO0o〇()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇o〇:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇00O0:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo8Oo00oo()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Ooo()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O88O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇O〇oO()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0O0()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo80:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o8()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o88〇OO08〇()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o8oO〇()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO()Landroidx/appcompat/widget/LinearLayoutCompat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇08〇o0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO00OOO()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OO〇00〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final ooo〇8oO()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oO〇8O8oOo:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇()Lcom/intsig/view/TagLinearLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇8〇oO〇〇8o:Lcom/intsig/view/TagLinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0OOo〇0()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8oOO88()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇〇0o〇〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇O()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->ooo0〇〇O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o0OoOOo0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Landroid/widget/CheckBox;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00〇8()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->Ooo08:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O8o〇0()Landroid/widget/LinearLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇〇08O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8()Lcom/intsig/view/NinePhotoView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->O0O:Lcom/intsig/view/NinePhotoView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇OO8ooO8〇:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8〇0〇o〇O()Landroidx/appcompat/widget/LinearLayoutCompat;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOO〇〇:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O〇80o08O()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o〇00O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8〇OO:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o0O0O8()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->oOO0880O:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->o8oOOo:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇0o()Landroid/view/ViewGroup;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->〇0O〇O00O:Landroid/view/ViewGroup;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocItemProviderNew$DocViewHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

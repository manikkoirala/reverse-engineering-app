.class public final Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "TeamFolderItemProviderNew.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TeamFolderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroid/widget/LinearLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Landroid/widget/ImageView;

.field private 〇OOo8〇0:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;)V
    .locals 12
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docViewMode"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$ListMode;

    .line 15
    .line 16
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const-string v1, "binding.clRoot"

    .line 21
    .line 22
    const-string v2, "binding.ivFolderBottomIcon"

    .line 23
    .line 24
    const-string v3, "binding.llFolderCheckbox"

    .line 25
    .line 26
    const-string v4, "binding.ivFolderOpeMore"

    .line 27
    .line 28
    const-string v5, "binding.tvDocNum"

    .line 29
    .line 30
    const-string v6, "binding.tvFolderName"

    .line 31
    .line 32
    const-string v7, "binding.ivFolderIcon"

    .line 33
    .line 34
    const-string v8, "bind(convertView)"

    .line 35
    .line 36
    if-eqz v0, :cond_0

    .line 37
    .line 38
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-static {p1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 46
    .line 47
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 51
    .line 52
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o0:Landroid/widget/TextView;

    .line 58
    .line 59
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 60
    .line 61
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 65
    .line 66
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 67
    .line 68
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 72
    .line 73
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 74
    .line 75
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o〇00O:Landroid/widget/LinearLayout;

    .line 79
    .line 80
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 81
    .line 82
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 86
    .line 87
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 88
    .line 89
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 93
    .line 94
    goto/16 :goto_0

    .line 95
    .line 96
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$GridMode;

    .line 97
    .line 98
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-eqz v0, :cond_1

    .line 103
    .line 104
    iget-object p2, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 105
    .line 106
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 107
    .line 108
    .line 109
    move-result-object p2

    .line 110
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 111
    .line 112
    .line 113
    move-result-object p2

    .line 114
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 115
    .line 116
    .line 117
    move-result-object p2

    .line 118
    iget p2, p2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 119
    .line 120
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 121
    .line 122
    sget-object v9, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager;->〇o〇:Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;

    .line 123
    .line 124
    const/4 v10, 0x2

    .line 125
    const/4 v11, 0x0

    .line 126
    invoke-static {v9, p2, v11, v10, v11}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;->〇o〇(Lcom/intsig/camscanner/mainmenu/adapter/MainDocLayoutManager$Companion;ILcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;ILjava/lang/Object;)Landroid/view/ViewGroup$MarginLayoutParams;

    .line 127
    .line 128
    .line 129
    move-result-object p2

    .line 130
    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    .line 132
    .line 133
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    invoke-static {p1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 141
    .line 142
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 146
    .line 147
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 148
    .line 149
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o0:Landroid/widget/TextView;

    .line 153
    .line 154
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 155
    .line 156
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 160
    .line 161
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 162
    .line 163
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 167
    .line 168
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 169
    .line 170
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    .line 172
    .line 173
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o〇00O:Landroid/widget/LinearLayout;

    .line 174
    .line 175
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 176
    .line 177
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 181
    .line 182
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 183
    .line 184
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 188
    .line 189
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocGridModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 190
    .line 191
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇0O:Landroid/widget/ImageView;

    .line 192
    .line 193
    goto :goto_0

    .line 194
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/DocViewMode$LargePicMode;

    .line 195
    .line 196
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 197
    .line 198
    .line 199
    move-result p2

    .line 200
    if-eqz p2, :cond_2

    .line 201
    .line 202
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    invoke-static {p1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    .line 208
    .line 209
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 210
    .line 211
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    .line 213
    .line 214
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 215
    .line 216
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 217
    .line 218
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o0:Landroid/widget/TextView;

    .line 222
    .line 223
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 224
    .line 225
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 229
    .line 230
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇0O:Landroid/widget/ImageView;

    .line 231
    .line 232
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 236
    .line 237
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 238
    .line 239
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o〇00O:Landroid/widget/LinearLayout;

    .line 243
    .line 244
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 245
    .line 246
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 250
    .line 251
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocLargepicModeFolderTypeBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 252
    .line 253
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 257
    .line 258
    goto :goto_0

    .line 259
    :cond_2
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;

    .line 260
    .line 261
    .line 262
    move-result-object p1

    .line 263
    invoke-static {p1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 267
    .line 268
    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 272
    .line 273
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo0:Landroid/widget/TextView;

    .line 274
    .line 275
    invoke-static {p2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o0:Landroid/widget/TextView;

    .line 279
    .line 280
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 281
    .line 282
    invoke-static {p2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    .line 284
    .line 285
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 286
    .line 287
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 288
    .line 289
    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 293
    .line 294
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 295
    .line 296
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    .line 298
    .line 299
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o〇00O:Landroid/widget/LinearLayout;

    .line 300
    .line 301
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->o〇00O:Landroid/widget/ImageView;

    .line 302
    .line 303
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    .line 305
    .line 306
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 307
    .line 308
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemMaindocListModeFolderTypeBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 309
    .line 310
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 314
    .line 315
    :goto_0
    return-void
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method


# virtual methods
.method public final O8ooOoo〇()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->o〇00O:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇080OO8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->OO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/TeamFolderItemProviderNew$TeamFolderViewHolder;->〇0O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

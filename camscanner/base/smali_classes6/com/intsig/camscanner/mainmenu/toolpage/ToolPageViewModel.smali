.class public final Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "ToolPageViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 10
    .line 11
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 12
    .line 13
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 17
    .line 18
    const-string p1, "adTab"

    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->OO:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O8oOo80(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/backup/BackUpExp;->〇080()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BACKUP:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "import"

    .line 14
    .line 15
    const-string v2, "doc_backup"

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, -0x1

    .line 5
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(II)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 9
    .line 10
    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇00(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-object v0
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇O〇oO(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BUY_DEVICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    if-ne p1, v0, :cond_1

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_text:Ljava/lang/String;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-nez v0, :cond_2

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getStringRes()I

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string p1, "app.getString(cellEnum.stringRes)"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getStringRes()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string p1, "{\n            app.getStr\u2026Enum.stringRes)\n        }"

    .line 46
    .line 47
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    :cond_2
    :goto_1
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o0ooO()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f1303bc

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_CARD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 17
    .line 18
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "id_card"

    .line 23
    .line 24
    const-string v3, "scan_process"

    .line 25
    .line 26
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 34
    .line 35
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const-string v2, "image_to_text"

    .line 40
    .line 41
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    sget-object v1, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-eqz v1, :cond_0

    .line 55
    .line 56
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ERASE_PAPER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 57
    .line 58
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    const-string v2, "test_paper"

    .line 63
    .line 64
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->enableImageRestore()Z

    .line 76
    .line 77
    .line 78
    move-result v1

    .line 79
    if-eqz v1, :cond_1

    .line 80
    .line 81
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 82
    .line 83
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    const-string v2, "image_restore"

    .line 88
    .line 89
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 97
    .line 98
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    const-string v2, "id_photo"

    .line 103
    .line 104
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 105
    .line 106
    .line 107
    move-result-object v1

    .line 108
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oooo8o0〇()Z

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    if-eqz v1, :cond_2

    .line 116
    .line 117
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 118
    .line 119
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    const-string v2, "excel_ocr"

    .line 124
    .line 125
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    .line 131
    .line 132
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TRANSLATE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 133
    .line 134
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 135
    .line 136
    .line 137
    move-result-object v1

    .line 138
    const-string v2, "translate"

    .line 139
    .line 140
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 141
    .line 142
    .line 143
    move-result-object v1

    .line 144
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    .line 146
    .line 147
    invoke-static {}, Lcom/intsig/advertisement/util/CommonUtil;->〇80〇808〇O()I

    .line 148
    .line 149
    .line 150
    move-result v1

    .line 151
    if-nez v1, :cond_3

    .line 152
    .line 153
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TOPIC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 154
    .line 155
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    const-string v2, "scan_exam"

    .line 160
    .line 161
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    .line 167
    .line 168
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->forbidNcnnLibForBookScene()Z

    .line 173
    .line 174
    .line 175
    move-result v1

    .line 176
    if-nez v1, :cond_4

    .line 177
    .line 178
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BOOK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 179
    .line 180
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 181
    .line 182
    .line 183
    move-result-object v1

    .line 184
    const-string v2, "scan_book"

    .line 185
    .line 186
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 187
    .line 188
    .line 189
    move-result-object v1

    .line 190
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    :cond_4
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 194
    .line 195
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 196
    .line 197
    .line 198
    move-result-object v1

    .line 199
    const-string v2, "scan_ppt"

    .line 200
    .line 201
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 202
    .line 203
    .line 204
    move-result-object v1

    .line 205
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    .line 207
    .line 208
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 209
    .line 210
    .line 211
    move-result-object v1

    .line 212
    iget-object v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->write_pad_config:Lcom/intsig/tsapp/sync/AppConfigJson$WritePad;

    .line 213
    .line 214
    const/4 v2, 0x1

    .line 215
    if-eqz v1, :cond_5

    .line 216
    .line 217
    iget v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson$WritePad;->func_switch:I

    .line 218
    .line 219
    if-ne v1, v2, :cond_5

    .line 220
    .line 221
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WritePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 222
    .line 223
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 224
    .line 225
    .line 226
    move-result-object v1

    .line 227
    const-string v4, "wacom"

    .line 228
    .line 229
    invoke-virtual {p0, v1, v3, v4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 230
    .line 231
    .line 232
    move-result-object v1

    .line 233
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    .line 235
    .line 236
    :cond_5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇〇0〇0o8(Ljava/util/List;)V

    .line 237
    .line 238
    .line 239
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 240
    .line 241
    .line 242
    move-result-object v1

    .line 243
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOpenBankCardJournal()Z

    .line 244
    .line 245
    .line 246
    move-result v1

    .line 247
    if-eqz v1, :cond_6

    .line 248
    .line 249
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 250
    .line 251
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 252
    .line 253
    .line 254
    move-result-object v1

    .line 255
    const-string v4, "bank_card_journal"

    .line 256
    .line 257
    invoke-virtual {p0, v1, v3, v4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 258
    .line 259
    .line 260
    move-result-object v1

    .line 261
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    .line 263
    .line 264
    :cond_6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->ooo8o〇o〇(Ljava/util/List;)V

    .line 265
    .line 266
    .line 267
    const v1, 0x7f130f6b

    .line 268
    .line 269
    .line 270
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 271
    .line 272
    .line 273
    move-result-object v1

    .line 274
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    .line 276
    .line 277
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 278
    .line 279
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 280
    .line 281
    .line 282
    move-result-object v1

    .line 283
    const-string v4, "import_pic"

    .line 284
    .line 285
    invoke-virtual {p0, v1, v3, v4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 286
    .line 287
    .line 288
    move-result-object v1

    .line 289
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    .line 291
    .line 292
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 293
    .line 294
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 295
    .line 296
    .line 297
    move-result-object v1

    .line 298
    const-string v3, "import_doc"

    .line 299
    .line 300
    const-string v4, "doc_process"

    .line 301
    .line 302
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 303
    .line 304
    .line 305
    move-result-object v1

    .line 306
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    .line 308
    .line 309
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8oOo80(Ljava/util/List;)V

    .line 310
    .line 311
    .line 312
    const v1, 0x7f130a20

    .line 313
    .line 314
    .line 315
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 316
    .line 317
    .line 318
    move-result-object v1

    .line 319
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    .line 321
    .line 322
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 323
    .line 324
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 325
    .line 326
    .line 327
    move-result-object v1

    .line 328
    const-string v3, "to_word"

    .line 329
    .line 330
    const-string v5, "format_conversion"

    .line 331
    .line 332
    invoke-virtual {p0, v1, v5, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 333
    .line 334
    .line 335
    move-result-object v1

    .line 336
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    .line 338
    .line 339
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 340
    .line 341
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 342
    .line 343
    .line 344
    move-result-object v1

    .line 345
    const-string v3, "to_excel"

    .line 346
    .line 347
    invoke-virtual {p0, v1, v5, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 348
    .line 349
    .line 350
    move-result-object v1

    .line 351
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    .line 353
    .line 354
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 355
    .line 356
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 357
    .line 358
    .line 359
    move-result-object v1

    .line 360
    const-string v3, "to_ppt"

    .line 361
    .line 362
    invoke-virtual {p0, v1, v5, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 363
    .line 364
    .line 365
    move-result-object v1

    .line 366
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    .line 368
    .line 369
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 370
    .line 371
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 372
    .line 373
    .line 374
    move-result-object v1

    .line 375
    const-string v3, "doc_to_pic"

    .line 376
    .line 377
    invoke-virtual {p0, v1, v5, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 378
    .line 379
    .line 380
    move-result-object v1

    .line 381
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    .line 383
    .line 384
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 385
    .line 386
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 387
    .line 388
    .line 389
    move-result-object v1

    .line 390
    const-string v3, "doc_to_long_pic"

    .line 391
    .line 392
    invoke-virtual {p0, v1, v5, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 393
    .line 394
    .line 395
    move-result-object v1

    .line 396
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 397
    .line 398
    .line 399
    const v1, 0x7f1309a1

    .line 400
    .line 401
    .line 402
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 403
    .line 404
    .line 405
    move-result-object v1

    .line 406
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    .line 408
    .line 409
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 410
    .line 411
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 412
    .line 413
    .line 414
    move-result-object v1

    .line 415
    const-string v3, "doc_signature"

    .line 416
    .line 417
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 418
    .line 419
    .line 420
    move-result-object v1

    .line 421
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    .line 423
    .line 424
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 425
    .line 426
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 427
    .line 428
    .line 429
    move-result-object v1

    .line 430
    const-string v3, "add_watermark"

    .line 431
    .line 432
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 433
    .line 434
    .line 435
    move-result-object v1

    .line 436
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    .line 438
    .line 439
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 440
    .line 441
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 442
    .line 443
    .line 444
    move-result-object v1

    .line 445
    const-string v3, "doc_merge"

    .line 446
    .line 447
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 448
    .line 449
    .line 450
    move-result-object v1

    .line 451
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 452
    .line 453
    .line 454
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 455
    .line 456
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 457
    .line 458
    .line 459
    move-result-object v1

    .line 460
    const-string v3, "doc_pic_up"

    .line 461
    .line 462
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 463
    .line 464
    .line 465
    move-result-object v1

    .line 466
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    .line 468
    .line 469
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 470
    .line 471
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 472
    .line 473
    .line 474
    move-result-object v1

    .line 475
    const-string v3, "doc_revise"

    .line 476
    .line 477
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 478
    .line 479
    .line 480
    move-result-object v1

    .line 481
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482
    .line 483
    .line 484
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 485
    .line 486
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 487
    .line 488
    .line 489
    move-result-object v1

    .line 490
    const-string v3, "doc_encrypt"

    .line 491
    .line 492
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 493
    .line 494
    .line 495
    move-result-object v1

    .line 496
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    .line 498
    .line 499
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->〇080()Z

    .line 500
    .line 501
    .line 502
    move-result v1

    .line 503
    if-eqz v1, :cond_7

    .line 504
    .line 505
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 506
    .line 507
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 508
    .line 509
    .line 510
    move-result-object v1

    .line 511
    const-string v3, "cs_pdf"

    .line 512
    .line 513
    invoke-virtual {p0, v1, v4, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 514
    .line 515
    .line 516
    move-result-object v1

    .line 517
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    .line 519
    .line 520
    :cond_7
    const v1, 0x7f131e7f

    .line 521
    .line 522
    .line 523
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 524
    .line 525
    .line 526
    move-result-object v1

    .line 527
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    .line 529
    .line 530
    sget-object v1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 531
    .line 532
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 533
    .line 534
    .line 535
    move-result v1

    .line 536
    if-ne v1, v2, :cond_8

    .line 537
    .line 538
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 539
    .line 540
    goto :goto_0

    .line 541
    :cond_8
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 542
    .line 543
    :goto_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 544
    .line 545
    .line 546
    move-result-object v1

    .line 547
    const-string v2, "qr_code"

    .line 548
    .line 549
    const-string v3, "other"

    .line 550
    .line 551
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 552
    .line 553
    .line 554
    move-result-object v1

    .line 555
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 556
    .line 557
    .line 558
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇O80ooo()Z

    .line 559
    .line 560
    .line 561
    move-result v1

    .line 562
    if-eqz v1, :cond_9

    .line 563
    .line 564
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 565
    .line 566
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 567
    .line 568
    .line 569
    move-result-object v1

    .line 570
    const-string v2, "innovation_lab"

    .line 571
    .line 572
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 573
    .line 574
    .line 575
    move-result-object v1

    .line 576
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o80oO()Z

    .line 577
    .line 578
    .line 579
    move-result v2

    .line 580
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 581
    .line 582
    .line 583
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 584
    .line 585
    .line 586
    :cond_9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇80〇808〇O(Ljava/util/List;)V

    .line 587
    .line 588
    .line 589
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oO8o(Ljava/util/List;)V

    .line 590
    .line 591
    .line 592
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o〇〇0〇88(Ljava/util/List;)V

    .line 593
    .line 594
    .line 595
    return-object v0
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final o8o〇〇0O(I)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/List;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v1

    .line 22
    :goto_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v2, 0x0

    .line 30
    if-ge p1, v0, :cond_3

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 33
    .line 34
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, Ljava/util/List;

    .line 39
    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    move-object v1, p1

    .line 47
    check-cast v1, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;

    .line 48
    .line 49
    :cond_1
    const/4 p1, 0x1

    .line 50
    if-eqz v1, :cond_2

    .line 51
    .line 52
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    const/4 v1, 0x2

    .line 57
    if-ne v0, v1, :cond_2

    .line 58
    .line 59
    const/4 v0, 0x1

    .line 60
    goto :goto_1

    .line 61
    :cond_2
    const/4 v0, 0x0

    .line 62
    :goto_1
    if-eqz v0, :cond_3

    .line 63
    .line 64
    return p1

    .line 65
    :cond_3
    return v2
    .line 66
    .line 67
.end method

.method private final oO()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇〇〇0〇〇0()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->showScanTools()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o〇0OOo〇0()Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0ooO()Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    :goto_0
    move-object v1, v0

    .line 32
    check-cast v1, Ljava/util/Collection;

    .line 33
    .line 34
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    xor-int/lit8 v1, v1, 0x1

    .line 39
    .line 40
    if-eqz v1, :cond_2

    .line 41
    .line 42
    const v1, 0x7f1303d2

    .line 43
    .line 44
    .line 45
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    :cond_2
    return-object v0
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oO8o(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->Oooo8o0〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->〇O00()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SMART_ERASE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 14
    .line 15
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "scan_process"

    .line 20
    .line 21
    const-string v2, "smart_remove"

    .line 22
    .line 23
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->〇o00〇〇Oo()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    xor-int/lit8 v1, v1, 0x1

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 34
    .line 35
    .line 36
    const/4 v1, 0x4

    .line 37
    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x1

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇O00(Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final ooo8o〇o〇(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/invoice/InvoiceExp;->〇080()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INVOICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "scan_process"

    .line 14
    .line 15
    const-string v2, "invoice"

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o〇0OOo〇0()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇8o8o〇(Ljava/util/List;)V

    .line 7
    .line 8
    .line 9
    const v1, 0x7f130afb

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 20
    .line 21
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "image_to_text"

    .line 26
    .line 27
    const-string v3, "scan_process"

    .line 28
    .line 29
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_0

    .line 43
    .line 44
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ERASE_PAPER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 45
    .line 46
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v2, "test_paper"

    .line 51
    .line 52
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->enableImageRestore()Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-eqz v1, :cond_1

    .line 68
    .line 69
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 70
    .line 71
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    const-string v2, "image_restore"

    .line 76
    .line 77
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 85
    .line 86
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    const-string v2, "id_photo"

    .line 91
    .line 92
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oooo8o0〇()Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    if-eqz v1, :cond_2

    .line 104
    .line 105
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 106
    .line 107
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    const-string v2, "excel_ocr"

    .line 112
    .line 113
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TRANSLATE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 121
    .line 122
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    const-string v2, "translate"

    .line 127
    .line 128
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOpenBankCardJournal()Z

    .line 140
    .line 141
    .line 142
    move-result v1

    .line 143
    if-eqz v1, :cond_3

    .line 144
    .line 145
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 146
    .line 147
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    const-string v2, "bank_card_journal"

    .line 152
    .line 153
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    .line 159
    .line 160
    :cond_3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->ooo8o〇o〇(Ljava/util/List;)V

    .line 161
    .line 162
    .line 163
    const v1, 0x7f130f6b

    .line 164
    .line 165
    .line 166
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .line 172
    .line 173
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 174
    .line 175
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 176
    .line 177
    .line 178
    move-result-object v1

    .line 179
    const-string v2, "import_pic"

    .line 180
    .line 181
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 182
    .line 183
    .line 184
    move-result-object v1

    .line 185
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    .line 187
    .line 188
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 189
    .line 190
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    const-string v2, "import_doc"

    .line 195
    .line 196
    const-string v3, "doc_process"

    .line 197
    .line 198
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    .line 204
    .line 205
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8oOo80(Ljava/util/List;)V

    .line 206
    .line 207
    .line 208
    const v1, 0x7f130a20

    .line 209
    .line 210
    .line 211
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 212
    .line 213
    .line 214
    move-result-object v1

    .line 215
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    .line 217
    .line 218
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 219
    .line 220
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 221
    .line 222
    .line 223
    move-result-object v1

    .line 224
    const-string v2, "to_word"

    .line 225
    .line 226
    const-string v4, "format_conversion"

    .line 227
    .line 228
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 229
    .line 230
    .line 231
    move-result-object v1

    .line 232
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    .line 234
    .line 235
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 236
    .line 237
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 238
    .line 239
    .line 240
    move-result-object v1

    .line 241
    const-string v2, "to_excel"

    .line 242
    .line 243
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 244
    .line 245
    .line 246
    move-result-object v1

    .line 247
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    .line 249
    .line 250
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 251
    .line 252
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 253
    .line 254
    .line 255
    move-result-object v1

    .line 256
    const-string v2, "doc_to_pic"

    .line 257
    .line 258
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 259
    .line 260
    .line 261
    move-result-object v1

    .line 262
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    .line 264
    .line 265
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 266
    .line 267
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    const-string v2, "doc_to_long_pic"

    .line 272
    .line 273
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 274
    .line 275
    .line 276
    move-result-object v1

    .line 277
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    .line 279
    .line 280
    const v1, 0x7f1309a1

    .line 281
    .line 282
    .line 283
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 284
    .line 285
    .line 286
    move-result-object v1

    .line 287
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    .line 289
    .line 290
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 291
    .line 292
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 293
    .line 294
    .line 295
    move-result-object v1

    .line 296
    const-string v2, "doc_signature"

    .line 297
    .line 298
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 299
    .line 300
    .line 301
    move-result-object v1

    .line 302
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    .line 304
    .line 305
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 306
    .line 307
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 308
    .line 309
    .line 310
    move-result-object v1

    .line 311
    const-string v2, "add_watermark"

    .line 312
    .line 313
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 314
    .line 315
    .line 316
    move-result-object v1

    .line 317
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    .line 319
    .line 320
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 321
    .line 322
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 323
    .line 324
    .line 325
    move-result-object v1

    .line 326
    const-string v2, "doc_merge"

    .line 327
    .line 328
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 329
    .line 330
    .line 331
    move-result-object v1

    .line 332
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    .line 334
    .line 335
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 336
    .line 337
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 338
    .line 339
    .line 340
    move-result-object v1

    .line 341
    const-string v2, "doc_pic_up"

    .line 342
    .line 343
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 344
    .line 345
    .line 346
    move-result-object v1

    .line 347
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    .line 349
    .line 350
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 351
    .line 352
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 353
    .line 354
    .line 355
    move-result-object v1

    .line 356
    const-string v2, "doc_revise"

    .line 357
    .line 358
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 359
    .line 360
    .line 361
    move-result-object v1

    .line 362
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    .line 364
    .line 365
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 366
    .line 367
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 368
    .line 369
    .line 370
    move-result-object v1

    .line 371
    const-string v2, "doc_encrypt"

    .line 372
    .line 373
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 374
    .line 375
    .line 376
    move-result-object v1

    .line 377
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    .line 379
    .line 380
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->〇080()Z

    .line 381
    .line 382
    .line 383
    move-result v1

    .line 384
    if-eqz v1, :cond_4

    .line 385
    .line 386
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 387
    .line 388
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 389
    .line 390
    .line 391
    move-result-object v1

    .line 392
    const-string v2, "cs_pdf"

    .line 393
    .line 394
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 395
    .line 396
    .line 397
    move-result-object v1

    .line 398
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    .line 400
    .line 401
    :cond_4
    const v1, 0x7f131e7f

    .line 402
    .line 403
    .line 404
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 405
    .line 406
    .line 407
    move-result-object v1

    .line 408
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    .line 410
    .line 411
    sget-object v1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 412
    .line 413
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 414
    .line 415
    .line 416
    move-result v1

    .line 417
    const/4 v2, 0x1

    .line 418
    if-ne v1, v2, :cond_5

    .line 419
    .line 420
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 421
    .line 422
    goto :goto_0

    .line 423
    :cond_5
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 424
    .line 425
    :goto_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 426
    .line 427
    .line 428
    move-result-object v1

    .line 429
    const-string v2, "qr_code"

    .line 430
    .line 431
    const-string v3, "other"

    .line 432
    .line 433
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 434
    .line 435
    .line 436
    move-result-object v1

    .line 437
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    .line 439
    .line 440
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇O80ooo()Z

    .line 441
    .line 442
    .line 443
    move-result v1

    .line 444
    if-eqz v1, :cond_6

    .line 445
    .line 446
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 447
    .line 448
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 449
    .line 450
    .line 451
    move-result-object v1

    .line 452
    const-string v2, "innovation_lab"

    .line 453
    .line 454
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 455
    .line 456
    .line 457
    move-result-object v1

    .line 458
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o80oO()Z

    .line 459
    .line 460
    .line 461
    move-result v2

    .line 462
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 463
    .line 464
    .line 465
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 466
    .line 467
    .line 468
    :cond_6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇80〇808〇O(Ljava/util/List;)V

    .line 469
    .line 470
    .line 471
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oO8o(Ljava/util/List;)V

    .line 472
    .line 473
    .line 474
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o〇〇0〇88(Ljava/util/List;)V

    .line 475
    .line 476
    .line 477
    return-object v0
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final o〇〇0〇88(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->Oooo8o0〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->COUNT_NUMBER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "scan_process"

    .line 14
    .line 15
    const-string v2, "count_mode"

    .line 16
    .line 17
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x4

    .line 22
    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇00(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    invoke-direct {v0, p0, p1, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;I)V

    .line 5
    .line 6
    .line 7
    const/16 v1, 0x25a

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇〇8O0〇8(I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getTitle()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-nez v1, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getTitle()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇0〇O0088o(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-nez v1, :cond_1

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->o800o8O(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇80〇808〇O(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/printer/PrintUtil;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PRINTER_DOC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "print_file"

    .line 14
    .line 15
    const-string v2, "smart_print"

    .line 16
    .line 17
    invoke-virtual {p0, v0, v2, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BUY_DEVICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 25
    .line 26
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v1, "buy_printer"

    .line 31
    .line 32
    invoke-virtual {p0, v0, v2, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8o8O〇O()Z
    .locals 13

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->APPLICATION_TAB:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇〇8O0〇8(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    if-eqz v3, :cond_8

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    check-cast v2, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 24
    .line 25
    invoke-virtual {v2}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getCategoryLocation()I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    if-gez v3, :cond_0

    .line 30
    .line 31
    const/4 v3, 0x0

    .line 32
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 33
    .line 34
    invoke-virtual {v4}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    check-cast v4, Ljava/util/List;

    .line 42
    .line 43
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    const/4 v5, -0x1

    .line 48
    const/4 v6, 0x1

    .line 49
    const/4 v7, -0x1

    .line 50
    const/4 v8, -0x1

    .line 51
    const/4 v9, -0x1

    .line 52
    const/4 v10, 0x1

    .line 53
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 54
    .line 55
    .line 56
    move-result v11

    .line 57
    if-eqz v11, :cond_6

    .line 58
    .line 59
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v11

    .line 63
    check-cast v11, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;

    .line 64
    .line 65
    add-int/2addr v7, v6

    .line 66
    invoke-interface {v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 67
    .line 68
    .line 69
    move-result v12

    .line 70
    if-ne v12, v6, :cond_3

    .line 71
    .line 72
    add-int/lit8 v12, v7, 0x1

    .line 73
    .line 74
    invoke-direct {p0, v12}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o8o〇〇0O(I)Z

    .line 75
    .line 76
    .line 77
    move-result v12

    .line 78
    if-eqz v12, :cond_3

    .line 79
    .line 80
    add-int/lit8 v8, v8, 0x1

    .line 81
    .line 82
    if-ne v8, v3, :cond_1

    .line 83
    .line 84
    const-string v12, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.toolpage.entity.ToolPageItem"

    .line 85
    .line 86
    invoke-static {v11, v12}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    check-cast v11, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 90
    .line 91
    invoke-virtual {v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇8o8o〇()Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v11

    .line 95
    invoke-virtual {v2}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getCategoryName()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v12

    .line 99
    invoke-static {v12, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 100
    .line 101
    .line 102
    move-result v11

    .line 103
    if-eqz v11, :cond_2

    .line 104
    .line 105
    const/4 v10, 0x0

    .line 106
    goto :goto_1

    .line 107
    :cond_2
    if-gez v7, :cond_4

    .line 108
    .line 109
    const/4 v5, 0x0

    .line 110
    goto :goto_2

    .line 111
    :cond_3
    if-ne v8, v3, :cond_5

    .line 112
    .line 113
    invoke-direct {p0, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o8o〇〇0O(I)Z

    .line 114
    .line 115
    .line 116
    move-result v11

    .line 117
    if-eqz v11, :cond_5

    .line 118
    .line 119
    add-int/lit8 v9, v9, 0x1

    .line 120
    .line 121
    invoke-virtual {v2}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getIconLocation()I

    .line 122
    .line 123
    .line 124
    move-result v11

    .line 125
    if-ne v9, v11, :cond_1

    .line 126
    .line 127
    :cond_4
    move v5, v7

    .line 128
    goto :goto_2

    .line 129
    :cond_5
    if-le v8, v3, :cond_1

    .line 130
    .line 131
    add-int/lit8 v5, v7, -0x1

    .line 132
    .line 133
    :cond_6
    :goto_2
    if-gez v5, :cond_7

    .line 134
    .line 135
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 136
    .line 137
    invoke-virtual {v3}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 138
    .line 139
    .line 140
    move-result-object v3

    .line 141
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 142
    .line 143
    .line 144
    check-cast v3, Ljava/util/List;

    .line 145
    .line 146
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 147
    .line 148
    .line 149
    move-result v3

    .line 150
    add-int/lit8 v5, v3, -0x1

    .line 151
    .line 152
    :cond_7
    const-string v3, "adData"

    .line 153
    .line 154
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-direct {p0, v5, v10, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo(IZLcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 158
    .line 159
    .line 160
    const/4 v2, 0x1

    .line 161
    goto/16 :goto_0

    .line 162
    .line 163
    :cond_8
    return v2
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇8o8o〇(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->scan_tools_sheet:Lcom/intsig/tsapp/sync/AppConfigJson$ScanToolsSheetEntity;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanToolsSheetEntity;->scan_service:Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    new-instance v7, Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 16
    .line 17
    .line 18
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->contract:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    const/4 v5, 0x4

    .line 22
    const/4 v6, 0x0

    .line 23
    move-object v1, p0

    .line 24
    move-object v2, v7

    .line 25
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->screenshot:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 29
    .line 30
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->ticket:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 34
    .line 35
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->draft:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 39
    .line 40
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->blackboard:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 44
    .line 45
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->references:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 49
    .line 50
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->certificates:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 54
    .line 55
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 56
    .line 57
    .line 58
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->exercises:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 59
    .line 60
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 61
    .line 62
    .line 63
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->card_photo:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 64
    .line 65
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 66
    .line 67
    .line 68
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->image_quality:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 69
    .line 70
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 71
    .line 72
    .line 73
    iget-object v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->others:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 74
    .line 75
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0, v7, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇O00(Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;Z)V

    .line 77
    .line 78
    .line 79
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->ppt:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 80
    .line 81
    move-object v1, p0

    .line 82
    move-object v2, v7

    .line 83
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 84
    .line 85
    .line 86
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->administrative:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 87
    .line 88
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 89
    .line 90
    .line 91
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->case_file:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 92
    .line 93
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 94
    .line 95
    .line 96
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->legal_papers:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 97
    .line 98
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 99
    .line 100
    .line 101
    iget-object v3, v0, Lcom/intsig/tsapp/sync/AppConfigJson$ScanServiceEntity;->drawing:Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;

    .line 102
    .line 103
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oo88o8O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;ZILjava/lang/Object;)V

    .line 104
    .line 105
    .line 106
    invoke-interface {v7}, Ljava/util/List;->size()I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    if-lez v0, :cond_0

    .line 111
    .line 112
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$addScanServiceBtn$1$1;

    .line 113
    .line 114
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel$addScanServiceBtn$1$1;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-static {v7, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 118
    .line 119
    .line 120
    const v0, 0x7f1303bc

    .line 121
    .line 122
    .line 123
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    .line 129
    .line 130
    invoke-interface {p1, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 131
    .line 132
    .line 133
    :cond_0
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇8o〇〇8080()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->APPLICATION_TAB_TOP:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇O00(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_1

    .line 9
    .line 10
    iget-boolean v0, v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->isClickClose:Z

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolBannerAdItem;

    .line 15
    .line 16
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolBannerAdItem;-><init>(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;I)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Ljava/util/List;

    .line 26
    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    :cond_0
    const/4 v0, 0x1

    .line 33
    return v0

    .line 34
    :cond_1
    return v2
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇O00(Ljava/util/List;Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;",
            "Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;",
            "Z)V"
        }
    .end annotation

    .line 1
    if-eqz p2, :cond_a

    .line 2
    .line 3
    iget-object v0, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->pic:Ljava/lang/String;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    goto :goto_1

    .line 18
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 19
    :goto_1
    if-nez v0, :cond_a

    .line 20
    .line 21
    iget-object v0, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->title:Ljava/lang/String;

    .line 22
    .line 23
    if-eqz v0, :cond_3

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_2

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_2
    const/4 v0, 0x0

    .line 33
    goto :goto_3

    .line 34
    :cond_3
    :goto_2
    const/4 v0, 0x1

    .line 35
    :goto_3
    if-nez v0, :cond_a

    .line 36
    .line 37
    iget-object v0, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->bgcolor:Ljava/lang/String;

    .line 38
    .line 39
    if-eqz v0, :cond_5

    .line 40
    .line 41
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_4

    .line 46
    .line 47
    goto :goto_4

    .line 48
    :cond_4
    const/4 v0, 0x0

    .line 49
    goto :goto_5

    .line 50
    :cond_5
    :goto_4
    const/4 v0, 0x1

    .line 51
    :goto_5
    if-nez v0, :cond_a

    .line 52
    .line 53
    iget-object v0, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->dplink:Ljava/lang/String;

    .line 54
    .line 55
    if-eqz v0, :cond_6

    .line 56
    .line 57
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    if-nez v0, :cond_7

    .line 62
    .line 63
    :cond_6
    const/4 v1, 0x1

    .line 64
    :cond_7
    if-eqz v1, :cond_8

    .line 65
    .line 66
    goto :goto_6

    .line 67
    :cond_8
    if-eqz p3, :cond_9

    .line 68
    .line 69
    iget-object v4, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->title:Ljava/lang/String;

    .line 70
    .line 71
    if-eqz v4, :cond_9

    .line 72
    .line 73
    const-string v0, "title"

    .line 74
    .line 75
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    sget-object v2, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->Companion:Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;

    .line 79
    .line 80
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 81
    .line 82
    const/4 v5, 0x0

    .line 83
    const/4 v6, 0x4

    .line 84
    const/4 v7, 0x0

    .line 85
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 86
    .line 87
    .line 88
    :cond_9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 89
    .line 90
    const/4 v1, 0x3

    .line 91
    const/16 v2, 0x191

    .line 92
    .line 93
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(II)V

    .line 94
    .line 95
    .line 96
    iget-object v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->pic:Ljava/lang/String;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->o800o8O(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    iget-object v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->title:Ljava/lang/String;

    .line 102
    .line 103
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇0〇O0088o(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    iget-object v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->bgcolor:Ljava/lang/String;

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇O〇(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    iget v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->index:I

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇O888o0o(I)V

    .line 114
    .line 115
    .line 116
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o0:Landroid/app/Application;

    .line 117
    .line 118
    iget-object v2, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->title:Ljava/lang/String;

    .line 119
    .line 120
    iget-object v3, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->dplink:Ljava/lang/String;

    .line 121
    .line 122
    invoke-virtual {p0, v1, p3, v2, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8〇o(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object p3

    .line 126
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇O00(Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    const-string p3, "scan_serve"

    .line 130
    .line 131
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->O〇8O8〇008(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    iget p2, p2, Lcom/intsig/tsapp/sync/AppConfigJson$ScanKitBoxEntity;->index:I

    .line 135
    .line 136
    new-instance p3, Ljava/lang/StringBuilder;

    .line 137
    .line 138
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p2

    .line 148
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->O8ooOoo〇(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    .line 153
    .line 154
    nop

    .line 155
    :cond_a
    :goto_6
    return-void
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final 〇oo(IZLcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 3

    .line 1
    invoke-virtual {p3}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "insertTabAdCategory>>> ad.id = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "ToolPageViewMode"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p3}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0oO〇oo00(Ljava/lang/String;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇00(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 43
    .line 44
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Ljava/util/List;

    .line 49
    .line 50
    if-eqz v1, :cond_1

    .line 51
    .line 52
    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 53
    .line 54
    .line 55
    :cond_1
    if-eqz p2, :cond_2

    .line 56
    .line 57
    new-instance p2, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 58
    .line 59
    const/4 v0, 0x1

    .line 60
    const/4 v1, -0x1

    .line 61
    invoke-direct {p2, v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(II)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p3}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getCategoryName()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p3

    .line 68
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇00(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->OO:Ljava/lang/String;

    .line 72
    .line 73
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->o〇O8〇〇o(Ljava/lang/Object;)V

    .line 74
    .line 75
    .line 76
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 77
    .line 78
    invoke-virtual {p3}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    move-result-object p3

    .line 82
    check-cast p3, Ljava/util/List;

    .line 83
    .line 84
    if-eqz p3, :cond_2

    .line 85
    .line 86
    invoke-interface {p3, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 87
    .line 88
    .line 89
    :cond_2
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getCellType()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x2

    .line 8
    invoke-direct {v0, v2, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(II)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getDrawableRes()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->OoO8(I)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O〇O〇oO(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇0〇O0088o(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    .line 27
    if-ne p1, v1, :cond_0

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇〇8〇〇()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-nez p1, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 p1, 0x0

    .line 38
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v2, "cellType = "

    .line 51
    .line 52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string v1, "ToolPageViewMode"

    .line 63
    .line 64
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    return-object v0
.end method

.method private final 〇〇0〇0o8(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->white_board_config:Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;->tool_box_switch:I

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    if-ne v1, v2, :cond_0

    .line 13
    .line 14
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$WhiteBoardConfig;->shooting_mode_switch:I

    .line 15
    .line 16
    if-ne v0, v2, :cond_0

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WhitePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 19
    .line 20
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "scan_process"

    .line 25
    .line 26
    const-string v2, "whiteboard"

    .line 27
    .line 28
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    :cond_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇〇〇0〇〇0()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f130a20

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 17
    .line 18
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "to_word"

    .line 23
    .line 24
    const-string v3, "format_conversion"

    .line 25
    .line 26
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 34
    .line 35
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const-string v2, "to_excel"

    .line 40
    .line 41
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 49
    .line 50
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    const-string v2, "to_ppt"

    .line 55
    .line 56
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 64
    .line 65
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    const-string v2, "doc_to_pic"

    .line 70
    .line 71
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 79
    .line 80
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    const-string v2, "doc_to_long_pic"

    .line 85
    .line 86
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    const v1, 0x7f130f6b

    .line 94
    .line 95
    .line 96
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    .line 102
    .line 103
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 104
    .line 105
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    const-string v2, "import_pic"

    .line 110
    .line 111
    const-string v3, "scan_process"

    .line 112
    .line 113
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    .line 119
    .line 120
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 121
    .line 122
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 123
    .line 124
    .line 125
    move-result-object v1

    .line 126
    const-string v2, "import_doc"

    .line 127
    .line 128
    const-string v4, "doc_process"

    .line 129
    .line 130
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8oOo80(Ljava/util/List;)V

    .line 138
    .line 139
    .line 140
    const v1, 0x7f1309a1

    .line 141
    .line 142
    .line 143
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 151
    .line 152
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    const-string v2, "doc_signature"

    .line 157
    .line 158
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    .line 164
    .line 165
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 166
    .line 167
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 168
    .line 169
    .line 170
    move-result-object v1

    .line 171
    const-string v2, "add_watermark"

    .line 172
    .line 173
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 174
    .line 175
    .line 176
    move-result-object v1

    .line 177
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 181
    .line 182
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 183
    .line 184
    .line 185
    move-result-object v1

    .line 186
    const-string v2, "doc_merge"

    .line 187
    .line 188
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 189
    .line 190
    .line 191
    move-result-object v1

    .line 192
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    .line 194
    .line 195
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 196
    .line 197
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    const-string v2, "doc_pic_up"

    .line 202
    .line 203
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 204
    .line 205
    .line 206
    move-result-object v1

    .line 207
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 211
    .line 212
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 213
    .line 214
    .line 215
    move-result-object v1

    .line 216
    const-string v2, "doc_revise"

    .line 217
    .line 218
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 219
    .line 220
    .line 221
    move-result-object v1

    .line 222
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    .line 224
    .line 225
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 226
    .line 227
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    const-string v2, "doc_encrypt"

    .line 232
    .line 233
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 234
    .line 235
    .line 236
    move-result-object v1

    .line 237
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    .line 239
    .line 240
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->〇080()Z

    .line 241
    .line 242
    .line 243
    move-result v1

    .line 244
    if-eqz v1, :cond_0

    .line 245
    .line 246
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 247
    .line 248
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 249
    .line 250
    .line 251
    move-result-object v1

    .line 252
    const-string v2, "cs_pdf"

    .line 253
    .line 254
    invoke-virtual {p0, v1, v4, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 255
    .line 256
    .line 257
    move-result-object v1

    .line 258
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    .line 260
    .line 261
    :cond_0
    const v1, 0x7f1303bc

    .line 262
    .line 263
    .line 264
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 265
    .line 266
    .line 267
    move-result-object v1

    .line 268
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    .line 270
    .line 271
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_CARD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 272
    .line 273
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 274
    .line 275
    .line 276
    move-result-object v1

    .line 277
    const-string v2, "id_card"

    .line 278
    .line 279
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 280
    .line 281
    .line 282
    move-result-object v1

    .line 283
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    .line 285
    .line 286
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 287
    .line 288
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 289
    .line 290
    .line 291
    move-result-object v1

    .line 292
    const-string v2, "image_to_text"

    .line 293
    .line 294
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 295
    .line 296
    .line 297
    move-result-object v1

    .line 298
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    .line 300
    .line 301
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 302
    .line 303
    .line 304
    move-result-object v1

    .line 305
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->enableImageRestore()Z

    .line 306
    .line 307
    .line 308
    move-result v1

    .line 309
    if-eqz v1, :cond_1

    .line 310
    .line 311
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 312
    .line 313
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 314
    .line 315
    .line 316
    move-result-object v1

    .line 317
    const-string v2, "image_restore"

    .line 318
    .line 319
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 320
    .line 321
    .line 322
    move-result-object v1

    .line 323
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    .line 325
    .line 326
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 327
    .line 328
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 329
    .line 330
    .line 331
    move-result-object v1

    .line 332
    const-string v2, "id_photo"

    .line 333
    .line 334
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 335
    .line 336
    .line 337
    move-result-object v1

    .line 338
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    .line 340
    .line 341
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oooo8o0〇()Z

    .line 342
    .line 343
    .line 344
    move-result v1

    .line 345
    if-eqz v1, :cond_2

    .line 346
    .line 347
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 348
    .line 349
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 350
    .line 351
    .line 352
    move-result-object v1

    .line 353
    const-string v2, "excel_ocr"

    .line 354
    .line 355
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 356
    .line 357
    .line 358
    move-result-object v1

    .line 359
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    .line 361
    .line 362
    :cond_2
    invoke-static {}, Lcom/intsig/advertisement/util/CommonUtil;->〇80〇808〇O()I

    .line 363
    .line 364
    .line 365
    move-result v1

    .line 366
    if-nez v1, :cond_3

    .line 367
    .line 368
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TOPIC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 369
    .line 370
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 371
    .line 372
    .line 373
    move-result-object v1

    .line 374
    const-string v2, "scan_exam"

    .line 375
    .line 376
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 377
    .line 378
    .line 379
    move-result-object v1

    .line 380
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    .line 382
    .line 383
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 384
    .line 385
    .line 386
    move-result-object v1

    .line 387
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->forbidNcnnLibForBookScene()Z

    .line 388
    .line 389
    .line 390
    move-result v1

    .line 391
    if-nez v1, :cond_4

    .line 392
    .line 393
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BOOK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 394
    .line 395
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 396
    .line 397
    .line 398
    move-result-object v1

    .line 399
    const-string v2, "scan_book"

    .line 400
    .line 401
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 402
    .line 403
    .line 404
    move-result-object v1

    .line 405
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    .line 407
    .line 408
    :cond_4
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 409
    .line 410
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 411
    .line 412
    .line 413
    move-result-object v1

    .line 414
    const-string v2, "scan_ppt"

    .line 415
    .line 416
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 417
    .line 418
    .line 419
    move-result-object v1

    .line 420
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    .line 422
    .line 423
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇〇0〇0o8(Ljava/util/List;)V

    .line 424
    .line 425
    .line 426
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 427
    .line 428
    .line 429
    move-result-object v1

    .line 430
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isOpenBankCardJournal()Z

    .line 431
    .line 432
    .line 433
    move-result v1

    .line 434
    if-eqz v1, :cond_5

    .line 435
    .line 436
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 437
    .line 438
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 439
    .line 440
    .line 441
    move-result-object v1

    .line 442
    const-string v2, "bank_card_journal"

    .line 443
    .line 444
    invoke-virtual {p0, v1, v3, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 445
    .line 446
    .line 447
    move-result-object v1

    .line 448
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 449
    .line 450
    .line 451
    :cond_5
    const v1, 0x7f131e7f

    .line 452
    .line 453
    .line 454
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->O8ooOoo〇(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 455
    .line 456
    .line 457
    move-result-object v1

    .line 458
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    .line 460
    .line 461
    invoke-static {}, Lcom/intsig/camscanner/settings/workflow/WorkflowUtils;->〇o00〇〇Oo()Z

    .line 462
    .line 463
    .line 464
    move-result v1

    .line 465
    const-string v2, "other"

    .line 466
    .line 467
    if-eqz v1, :cond_6

    .line 468
    .line 469
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WORK_FLOW:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 470
    .line 471
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 472
    .line 473
    .line 474
    move-result-object v1

    .line 475
    const-string v3, "workflows"

    .line 476
    .line 477
    invoke-virtual {p0, v1, v2, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 478
    .line 479
    .line 480
    move-result-object v1

    .line 481
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 482
    .line 483
    .line 484
    :cond_6
    sget-object v1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 485
    .line 486
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 487
    .line 488
    .line 489
    move-result v1

    .line 490
    const/4 v3, 0x1

    .line 491
    if-ne v1, v3, :cond_7

    .line 492
    .line 493
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 494
    .line 495
    goto :goto_0

    .line 496
    :cond_7
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 497
    .line 498
    :goto_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 499
    .line 500
    .line 501
    move-result-object v1

    .line 502
    const-string v3, "qr_code"

    .line 503
    .line 504
    invoke-virtual {p0, v1, v2, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 505
    .line 506
    .line 507
    move-result-object v1

    .line 508
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 509
    .line 510
    .line 511
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0〇O80ooo()Z

    .line 512
    .line 513
    .line 514
    move-result v1

    .line 515
    if-eqz v1, :cond_8

    .line 516
    .line 517
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 518
    .line 519
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇oo〇(Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 520
    .line 521
    .line 522
    move-result-object v1

    .line 523
    const-string v3, "innovation_lab"

    .line 524
    .line 525
    invoke-virtual {p0, v1, v2, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 526
    .line 527
    .line 528
    move-result-object v1

    .line 529
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o80oO()Z

    .line 530
    .line 531
    .line 532
    move-result v2

    .line 533
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 534
    .line 535
    .line 536
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    .line 538
    .line 539
    :cond_8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oO8o(Ljava/util/List;)V

    .line 540
    .line 541
    .line 542
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->o〇〇0〇88(Ljava/util/List;)V

    .line 543
    .line 544
    .line 545
    return-object v0
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method


# virtual methods
.method public final O8〇o(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz p3, :cond_1

    .line 9
    .line 10
    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v2, 0x0

    .line 18
    goto :goto_1

    .line 19
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 20
    :goto_1
    if-nez v2, :cond_6

    .line 21
    .line 22
    if-eqz p4, :cond_3

    .line 23
    .line 24
    invoke-interface {p4}, Ljava/lang/CharSequence;->length()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_2

    .line 29
    .line 30
    goto :goto_2

    .line 31
    :cond_2
    const/4 v0, 0x0

    .line 32
    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 33
    .line 34
    goto :goto_3

    .line 35
    :cond_4
    new-instance v0, Lcom/intsig/tianshu/ParamsBuilder;

    .line 36
    .line 37
    invoke-direct {v0}, Lcom/intsig/tianshu/ParamsBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    if-eqz p2, :cond_5

    .line 41
    .line 42
    sget-object v2, Lcom/intsig/camscanner/capture/scene/CaptureSceneData;->Companion:Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;

    .line 43
    .line 44
    const/4 v5, 0x0

    .line 45
    const/4 v6, 0x4

    .line 46
    const/4 v7, 0x0

    .line 47
    move-object v3, p1

    .line 48
    move-object v4, p3

    .line 49
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData$Companion;Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-eqz p1, :cond_5

    .line 54
    .line 55
    invoke-static {p1, v1}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->Oo08(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;Z)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    if-eqz p1, :cond_5

    .line 60
    .line 61
    const-string p2, "capture_scene_json"

    .line 62
    .line 63
    invoke-virtual {v0, p2, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 64
    .line 65
    .line 66
    :cond_5
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 67
    .line 68
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    const-string p2, "capture_function_entrance"

    .line 73
    .line 74
    invoke-virtual {v0, p2, p1}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 75
    .line 76
    .line 77
    const-string p1, "cs_internal"

    .line 78
    .line 79
    const-string p2, "true"

    .line 80
    .line 81
    invoke-virtual {v0, p1, p2}, Lcom/intsig/tianshu/ParamsBuilder;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/tianshu/ParamsBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0, p4}, Lcom/intsig/tianshu/ParamsBuilder;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    const-string p2, "paramsBuilder.buildWithPath(deeplink)"

    .line 89
    .line 90
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    return-object p1

    .line 94
    :cond_6
    :goto_3
    const-string p1, ""

    .line 95
    .line 96
    return-object p1
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public final Oo0oOo〇0(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "item"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇080()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "trackAction>>> type = "

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "ToolPageViewMode"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    const/16 v1, 0x25a

    .line 37
    .line 38
    if-ne v0, v1, :cond_0

    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇O8o08O()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    const-string v1, ""

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    if-ne v0, v2, :cond_1

    .line 53
    .line 54
    move-object v0, v1

    .line 55
    goto :goto_0

    .line 56
    :cond_1
    if-nez v0, :cond_a

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇O8o08O()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->OO0o〇〇()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 67
    .line 68
    .line 69
    move-result v3

    .line 70
    if-ne v3, v2, :cond_2

    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_2
    if-nez v3, :cond_9

    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->OO0o〇〇()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    :goto_1
    if-eqz v1, :cond_7

    .line 80
    .line 81
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    sparse-switch p1, :sswitch_data_0

    .line 86
    .line 87
    .line 88
    goto :goto_2

    .line 89
    :sswitch_0
    const-string p1, "import_doc"

    .line 90
    .line 91
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-eqz p1, :cond_7

    .line 96
    .line 97
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolImportFile:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :sswitch_1
    const-string p1, "doc_revise"

    .line 101
    .line 102
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    if-nez p1, :cond_3

    .line 107
    .line 108
    goto :goto_2

    .line 109
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfSort:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 110
    .line 111
    goto :goto_3

    .line 112
    :sswitch_2
    const-string p1, "doc_pic_up"

    .line 113
    .line 114
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    move-result p1

    .line 118
    if-nez p1, :cond_4

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_4
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfExtract:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 122
    .line 123
    goto :goto_3

    .line 124
    :sswitch_3
    const-string p1, "doc_merge"

    .line 125
    .line 126
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 127
    .line 128
    .line 129
    move-result p1

    .line 130
    if-nez p1, :cond_5

    .line 131
    .line 132
    goto :goto_2

    .line 133
    :cond_5
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileMerge:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 134
    .line 135
    goto :goto_3

    .line 136
    :sswitch_4
    const-string p1, "doc_signature"

    .line 137
    .line 138
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    if-nez p1, :cond_6

    .line 143
    .line 144
    goto :goto_2

    .line 145
    :cond_6
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileSignature:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 146
    .line 147
    goto :goto_3

    .line 148
    :cond_7
    :goto_2
    const/4 p1, 0x0

    .line 149
    :goto_3
    new-instance v2, Lorg/json/JSONObject;

    .line 150
    .line 151
    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 152
    .line 153
    .line 154
    const-string v3, "from"

    .line 155
    .line 156
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    .line 158
    .line 159
    const-string v0, "type"

    .line 160
    .line 161
    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 162
    .line 163
    .line 164
    if-eqz p1, :cond_8

    .line 165
    .line 166
    const-string v0, "refer_source"

    .line 167
    .line 168
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->getCsPdfTrack()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    invoke-virtual {v2, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 173
    .line 174
    .line 175
    :cond_8
    const-string p1, "CSMainApplication"

    .line 176
    .line 177
    const-string v0, "select"

    .line 178
    .line 179
    invoke-static {p1, v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 180
    .line 181
    .line 182
    return-void

    .line 183
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 184
    .line 185
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 186
    .line 187
    .line 188
    throw p1

    .line 189
    :cond_a
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 190
    .line 191
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 192
    .line 193
    .line 194
    throw p1

    .line 195
    :sswitch_data_0
    .sparse-switch
        -0x31b0d8f -> :sswitch_4
        0x12b2c271 -> :sswitch_3
        0x48f58657 -> :sswitch_2
        0x4c2fa1df -> :sswitch_1
        0x7eb1cd9e -> :sswitch_0
    .end sparse-switch
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "<this>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "from"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "type"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->O〇8O8〇008(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->O8ooOoo〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final O〇8oOo8O()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const/4 v1, 0x0

    .line 17
    const/4 v2, 0x0

    .line 18
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    const/4 v4, 0x1

    .line 23
    if-eqz v3, :cond_3

    .line 24
    .line 25
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    check-cast v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;

    .line 30
    .line 31
    instance-of v5, v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;

    .line 32
    .line 33
    if-eqz v5, :cond_1

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 36
    .line 37
    .line 38
    :goto_1
    const/4 v2, 0x1

    .line 39
    goto :goto_0

    .line 40
    :cond_1
    instance-of v5, v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolBannerAdItem;

    .line 41
    .line 42
    if-eqz v5, :cond_2

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 45
    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_2
    instance-of v5, v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 49
    .line 50
    if-eqz v5, :cond_0

    .line 51
    .line 52
    invoke-interface {v3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    if-ne v5, v4, :cond_0

    .line 57
    .line 58
    check-cast v3, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 59
    .line 60
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->OO0o〇〇〇〇0()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    instance-of v5, v3, Ljava/lang/String;

    .line 65
    .line 66
    if-eqz v5, :cond_0

    .line 67
    .line 68
    check-cast v3, Ljava/lang/CharSequence;

    .line 69
    .line 70
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->OO:Ljava/lang/String;

    .line 71
    .line 72
    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 73
    .line 74
    .line 75
    move-result v3

    .line 76
    if-eqz v3, :cond_0

    .line 77
    .line 78
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇8o〇〇8080()Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇8o8O〇O()Z

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    if-nez v2, :cond_4

    .line 91
    .line 92
    if-nez v3, :cond_4

    .line 93
    .line 94
    if-eqz v0, :cond_5

    .line 95
    .line 96
    :cond_4
    const/4 v1, 0x1

    .line 97
    :cond_5
    return v1
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final oo〇(Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;I)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "item"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    if-lt p2, v0, :cond_7

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Ljava/util/List;

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v1, -0x1

    .line 25
    :goto_0
    if-ge p2, v1, :cond_7

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 28
    .line 29
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    check-cast v1, Ljava/util/List;

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    if-eqz v1, :cond_1

    .line 37
    .line 38
    add-int/lit8 v3, p2, -0x1

    .line 39
    .line 40
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    check-cast v1, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    move-object v1, v2

    .line 48
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 49
    .line 50
    invoke-virtual {v3}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    check-cast v3, Ljava/util/List;

    .line 55
    .line 56
    if-eqz v3, :cond_2

    .line 57
    .line 58
    add-int/2addr p2, v0

    .line 59
    invoke-interface {v3, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    move-object v2, p2

    .line 64
    check-cast v2, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;

    .line 65
    .line 66
    :cond_2
    const/4 p2, 0x0

    .line 67
    if-eqz v1, :cond_3

    .line 68
    .line 69
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-ne v3, v0, :cond_3

    .line 74
    .line 75
    const/4 v3, 0x1

    .line 76
    goto :goto_2

    .line 77
    :cond_3
    const/4 v3, 0x0

    .line 78
    :goto_2
    if-eqz v3, :cond_5

    .line 79
    .line 80
    if-eqz v2, :cond_4

    .line 81
    .line 82
    invoke-interface {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇080()I

    .line 87
    .line 88
    .line 89
    move-result v3

    .line 90
    if-ne v2, v3, :cond_4

    .line 91
    .line 92
    const/4 p2, 0x1

    .line 93
    :cond_4
    if-nez p2, :cond_5

    .line 94
    .line 95
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 96
    .line 97
    invoke-virtual {p2}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object p2

    .line 101
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 102
    .line 103
    .line 104
    check-cast p2, Ljava/util/List;

    .line 105
    .line 106
    invoke-interface {p2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 107
    .line 108
    .line 109
    :cond_5
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 110
    .line 111
    invoke-virtual {p2}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 116
    .line 117
    .line 118
    check-cast p2, Ljava/util/List;

    .line 119
    .line 120
    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;->〇o00〇〇Oo()I

    .line 124
    .line 125
    .line 126
    move-result p2

    .line 127
    const/16 v1, 0x25a

    .line 128
    .line 129
    if-ne p2, v1, :cond_6

    .line 130
    .line 131
    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;->o〇〇0〇()Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO08〇(Ljava/lang/String;Z)V

    .line 142
    .line 143
    .line 144
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 145
    .line 146
    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    invoke-virtual {p1, p2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 151
    .line 152
    .line 153
    :cond_7
    return-void
    .line 154
.end method

.method public final o〇8oOO88()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/IToolPageStyle;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageViewModel;->oO()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

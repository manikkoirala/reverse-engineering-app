.class public final Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;
.super Ljava/lang/Object;
.source "ToolFunctionControl.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;,
        Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$ImportGalleryInterface;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OoO8:Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static o800o8O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static 〇O888o0o:Z


# instance fields
.field private O8:Lcom/intsig/lifecycle/LifecycleHandler;

.field private OO0o〇〇:Z

.field private OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private Oo08:J

.field private Oooo8o0〇:Z

.field private oO80:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

.field private o〇0:Z

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

.field private 〇80〇808〇O:Z

.field private 〇8o8o〇:Z

.field private 〇O00:Z

.field private 〇O8o08O:Ljava/lang/String;

.field private final 〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Ljava/lang/String;

.field private 〇〇808〇:I

.field private 〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

.field private final 〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OoO8:Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o800o8O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dataItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o〇:Ljava/lang/String;

    const/4 p2, -0x1

    .line 5
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇808〇:I

    .line 6
    new-instance p2, Lcom/intsig/lifecycle/LifecycleHandler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/intsig/lifecycle/LifecycleHandler;-><init>(Landroid/os/Looper;Landroidx/lifecycle/LifecycleOwner;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O8:Lcom/intsig/lifecycle/LifecycleHandler;

    .line 7
    new-instance p1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$mStatusListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$mStatusListener$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;

    .line 8
    new-instance p1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$mOfficeStatusListener$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$mOfficeStatusListener$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 9
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;)V

    return-void
.end method

.method private final O000(I)Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    const/16 v0, 0x69

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    if-eq p1, v0, :cond_0

    .line 12
    .line 13
    const/16 v0, 0xcd

    .line 14
    .line 15
    if-eq p1, v0, :cond_0

    .line 16
    .line 17
    const/16 v0, 0xcf

    .line 18
    .line 19
    if-eq p1, v0, :cond_0

    .line 20
    .line 21
    const/16 v0, 0x25b

    .line 22
    .line 23
    if-eq p1, v0, :cond_0

    .line 24
    .line 25
    const/16 v0, 0x25f

    .line 26
    .line 27
    if-eq p1, v0, :cond_0

    .line 28
    .line 29
    packed-switch p1, :pswitch_data_0

    .line 30
    .line 31
    .line 32
    packed-switch p1, :pswitch_data_1

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    :pswitch_0
    const/4 v1, 0x1

    .line 37
    :cond_1
    :goto_0
    return v1

    .line 38
    nop

    .line 39
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    :pswitch_data_1
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O08000(Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V
    .locals 8

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "goPdfToOffice:path:"

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ToolFunctionControl"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    const/4 v5, 0x0

    .line 28
    move-object v2, v0

    .line 29
    move-object v4, p2

    .line 30
    move-object v6, p1

    .line 31
    move-object v7, p4

    .line 32
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 36
    .line 37
    const/4 p2, 0x1

    .line 38
    const/4 p4, 0x0

    .line 39
    if-nez p3, :cond_0

    .line 40
    .line 41
    iput-boolean p4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O00:Z

    .line 42
    .line 43
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 44
    .line 45
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 46
    .line 47
    .line 48
    move-result p3

    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 50
    .line 51
    invoke-direct {p0, p3, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8(ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    new-array p2, p2, [Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 58
    .line 59
    new-instance p3, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 60
    .line 61
    const/4 v0, 0x0

    .line 62
    invoke-direct {p3, p1, v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    aput-object p3, p2, p4

    .line 66
    .line 67
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;

    .line 72
    .line 73
    const/4 v5, 0x0

    .line 74
    const/4 v6, 0x0

    .line 75
    const/4 v7, 0x0

    .line 76
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportOfficeByPath(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;ZZZ)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 81
    .line 82
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 83
    .line 84
    .line 85
    move-result p3

    .line 86
    const/16 v0, 0x65

    .line 87
    .line 88
    if-eq p3, v0, :cond_2

    .line 89
    .line 90
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 91
    .line 92
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 93
    .line 94
    .line 95
    move-result p3

    .line 96
    const/16 v0, 0x66

    .line 97
    .line 98
    if-ne p3, v0, :cond_1

    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 102
    .line 103
    if-eqz p1, :cond_4

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->O8()V

    .line 106
    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_2
    :goto_0
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 110
    .line 111
    const-wide/16 v2, 0x0

    .line 112
    .line 113
    cmp-long p3, v0, v2

    .line 114
    .line 115
    if-nez p3, :cond_3

    .line 116
    .line 117
    new-instance p3, Ljava/util/ArrayList;

    .line 118
    .line 119
    invoke-direct {p3, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    .line 121
    .line 122
    new-instance p2, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 123
    .line 124
    const-string v0, ""

    .line 125
    .line 126
    invoke-direct {p2, p1, v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    .line 131
    .line 132
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 133
    .line 134
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;

    .line 137
    .line 138
    invoke-static {p1, p3, p2, v0, p4}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportByPath(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;Z)V

    .line 139
    .line 140
    .line 141
    goto :goto_1

    .line 142
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 143
    .line 144
    if-eqz p1, :cond_4

    .line 145
    .line 146
    iget-boolean p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇0:Z

    .line 147
    .line 148
    invoke-virtual {p1, v0, v1, p2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->〇o〇(JZ)V

    .line 149
    .line 150
    .line 151
    :cond_4
    :goto_1
    return-void
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O0o〇〇Oo(IZ)Z
    .locals 3

    .line 1
    const/16 v0, 0xc9

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eq p1, v0, :cond_0

    .line 6
    .line 7
    const/16 v0, 0x25f

    .line 8
    .line 9
    if-ne p1, v0, :cond_1

    .line 10
    .line 11
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_2

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-eqz p1, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    const/4 p1, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_2
    :goto_0
    const/4 p1, 0x1

    .line 27
    :goto_1
    if-eqz p1, :cond_5

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 30
    .line 31
    if-eqz p1, :cond_3

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->isSupportImportOffice()Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-ne p1, v2, :cond_3

    .line 38
    .line 39
    const/4 p1, 0x1

    .line 40
    goto :goto_2

    .line 41
    :cond_3
    const/4 p1, 0x0

    .line 42
    :goto_2
    if-nez p1, :cond_4

    .line 43
    .line 44
    if-eqz p2, :cond_5

    .line 45
    .line 46
    :cond_4
    return v1

    .line 47
    :cond_5
    return v2
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic O8(Lcom/intsig/app/AlertDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O8ooOoo〇(Lcom/intsig/app/AlertDialog;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O8ooOoo〇(Lcom/intsig/app/AlertDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "$dialog"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static synthetic O8〇o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/util/List;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oo〇(Ljava/util/List;Ljava/lang/String;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;I)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O000(I)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0o〇〇〇〇0()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O888o0o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OO8oO0o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "ToolFunctionControl"

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string p1, "activity isFinish"

    .line 12
    .line 13
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 22
    .line 23
    .line 24
    move-result-wide v2

    .line 25
    iput-wide v2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 28
    .line 29
    if-nez v0, :cond_2

    .line 30
    .line 31
    const-wide/16 v4, 0x0

    .line 32
    .line 33
    cmp-long v0, v2, v4

    .line 34
    .line 35
    if-gtz v0, :cond_1

    .line 36
    .line 37
    const-string p1, "finishPdfImport -->  pdfToOfficeUtils == null"

    .line 38
    .line 39
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-void

    .line 43
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 44
    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 46
    .line 47
    const/4 v5, 0x0

    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 49
    .line 50
    .line 51
    move-result-object v6

    .line 52
    sget-object v7, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 53
    .line 54
    move-object v2, v0

    .line 55
    move-object v4, p2

    .line 56
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V

    .line 57
    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 60
    .line 61
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getOriginalPath()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v2, "checkBeforeToNextPage uriL"

    .line 75
    .line 76
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string p2, ", path:"

    .line 83
    .line 84
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 98
    .line 99
    if-eqz p1, :cond_3

    .line 100
    .line 101
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 102
    .line 103
    const/4 p2, 0x1

    .line 104
    invoke-virtual {p1, v0, v1, p2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->〇o〇(JZ)V

    .line 105
    .line 106
    .line 107
    :cond_3
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final OOO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v0, "CSPdfPackage"

    .line 9
    .line 10
    const-string v1, "merge_success"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x2

    .line 16
    const/4 v1, 0x0

    .line 17
    const/4 v2, 0x0

    .line 18
    invoke-static {p0, p1, v2, v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;IILjava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic OOO〇O0(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/Long;Landroid/net/Uri;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇〇0〇(Ljava/lang/Long;Landroid/net/Uri;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final Oo(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "intent"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo〇o(Landroid/content/Intent;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OOO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OoO8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$checkOpenCsPdf$1;

    .line 21
    .line 22
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$checkOpenCsPdf$1;-><init>(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function2;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    sget-object v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$checkOpenCsPdf$2;->o0:Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$checkOpenCsPdf$2;

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇080(Lkotlin/jvm/functions/Function0;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇()Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇0〇O0088o()V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const-string v0, "/pdf/toolpage"

    .line 48
    .line 49
    invoke-virtual {p1, v0}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final Ooo8〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/Long;)V
    .locals 2

    .line 1
    if-nez p1, :cond_1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "ToolFunctionControl"

    .line 7
    .line 8
    const-string p2, "start2LongImageStitchActivity data == null"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    goto :goto_2

    .line 14
    :cond_1
    :goto_0
    const-string v0, "CSPdfPackage"

    .line 15
    .line 16
    const-string v1, "transfer_long_pic_success"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    if-eqz p1, :cond_2

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 28
    .line 29
    .line 30
    move-result-wide p1

    .line 31
    goto :goto_1

    .line 32
    :cond_2
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 36
    .line 37
    .line 38
    move-result-wide p1

    .line 39
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 46
    .line 47
    invoke-static {p2, p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->o0ooO(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    invoke-static {p2}, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;->〇80〇808〇O(Landroid/content/Context;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    const/4 v1, 0x1

    .line 58
    invoke-static {p2, p1, v0, v1}, Lcom/intsig/camscanner/imagestitch/LongImageStitchActivity;->O〇〇O80o8(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    new-instance p2, Lcom/intsig/result/GetActivityResult;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    invoke-direct {p2, v0}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 67
    .line 68
    .line 69
    const/16 v0, 0x68

    .line 70
    .line 71
    invoke-virtual {p2, p1, v0}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    new-instance p2, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$start2LongImageStitchActivity$1;

    .line 76
    .line 77
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$start2LongImageStitchActivity$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1, p2}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 81
    .line 82
    .line 83
    :goto_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;JLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o0O0(JLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇0(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->〇080:Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 18
    .line 19
    .line 20
    move-result-wide v2

    .line 21
    const/4 p1, 0x0

    .line 22
    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/intsig/camscanner/translate_new/util/TranslateNewHelper;->o〇0(Landroidx/fragment/app/FragmentActivity;JZ)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    const-string p1, "ToolFunctionControl"

    .line 27
    .line 28
    const-string v0, "start2Translate data == null"

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    const-string p4, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p4, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇00(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method static synthetic O〇O〇oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p5, 0x8

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    sget-object p4, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 6
    .line 7
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O08000(Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method private final o0O0(JLjava/lang/String;)V
    .locals 6

    .line 1
    invoke-static {p3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    if-nez p3, :cond_0

    .line 6
    .line 7
    const-string p1, "ToolFunctionControl"

    .line 8
    .line 9
    const-string p2, "saveAsWaterMarkPdf -> dstPath is not exist"

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-static {p3}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    new-instance v3, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$saveAsWaterMarkPdf$1;

    .line 24
    .line 25
    const/4 p3, 0x0

    .line 26
    invoke-direct {v3, p1, p2, p0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$saveAsWaterMarkPdf$1;-><init>(JLcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    const/4 v4, 0x3

    .line 30
    const/4 v5, 0x0

    .line 31
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o8()Lcom/intsig/camscanner/ppt/PPTImportHelper;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ppt/PPTImportHelper;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$getPptImportHelper$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$getPptImportHelper$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/ppt/PPTImportHelper;-><init>(Lcom/intsig/camscanner/ppt/PPTImportInterface;)V

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o8oO〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x25c

    .line 8
    .line 9
    if-eq v0, v1, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8o0〇()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/16 v1, 0xc9

    .line 24
    .line 25
    if-eq v0, v1, :cond_1

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    const/4 v1, 0x2

    .line 34
    if-ne v0, v1, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 40
    :goto_1
    return v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p4, 0x2

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    and-int/lit8 p4, p4, 0x4

    .line 7
    .line 8
    if-eqz p4, :cond_1

    .line 9
    .line 10
    const/4 p3, 0x1

    .line 11
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇08O8o〇0(Landroid/content/Intent;ZZ)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final oO00OOO(I)Z
    .locals 1

    .line 1
    const/16 v0, 0xcf

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    :goto_0
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oo〇(Ljava/util/List;Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 4
    .line 5
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 13
    .line 14
    .line 15
    move-result-wide v1

    .line 16
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput-boolean p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    new-instance v1, LO88O/Oo08;

    .line 27
    .line 28
    invoke-direct {v1, p0}, LO88O/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 29
    .line 30
    .line 31
    sget-object v2, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o800o8O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 32
    .line 33
    invoke-static {p1, v0, v1, v2}, Lcom/intsig/camscanner/securitymark/SecurityMarkActivity;->O〇080〇o0(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/securitymark/SecurityMarkActivity$PrepareIntentCallBack;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const-string p1, "ToolFunctionControl"

    .line 38
    .line 39
    const-string v0, "data == null"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final ooo0〇O88O(JZ)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x0

    .line 9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startESignSelectAcceptorActivity$1;

    .line 10
    .line 11
    const/4 v9, 0x0

    .line 12
    move-object v4, v0

    .line 13
    move v5, p3

    .line 14
    move-object v6, p0

    .line 15
    move-wide v7, p1

    .line 16
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startESignSelectAcceptorActivity$1;-><init>(ZLcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;JLkotlin/coroutines/Continuation;)V

    .line 17
    .line 18
    .line 19
    const/4 v5, 0x3

    .line 20
    const/4 v6, 0x0

    .line 21
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final ooo〇8oO(IZ)Z
    .locals 3

    .line 1
    const/16 v0, 0xc9

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ne p1, v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O000(I)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-eqz p1, :cond_2

    .line 24
    .line 25
    :cond_1
    const/4 p1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_5

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 31
    .line 32
    if-eqz p1, :cond_3

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->isSupportImportOffice()Z

    .line 35
    .line 36
    .line 37
    move-result p1

    .line 38
    if-ne p1, v2, :cond_3

    .line 39
    .line 40
    const/4 p1, 0x1

    .line 41
    goto :goto_1

    .line 42
    :cond_3
    const/4 p1, 0x0

    .line 43
    :goto_1
    if-nez p1, :cond_4

    .line 44
    .line 45
    if-eqz p2, :cond_5

    .line 46
    .line 47
    :cond_4
    return v1

    .line 48
    :cond_5
    return v2
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final oo〇(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x65

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    if-eq v0, v1, :cond_8

    .line 11
    .line 12
    const/16 v1, 0x66

    .line 13
    .line 14
    if-eq v0, v1, :cond_7

    .line 15
    .line 16
    const/16 v1, 0x69

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    const/4 v4, 0x1

    .line 20
    if-eq v0, v1, :cond_6

    .line 21
    .line 22
    const/16 v1, 0xd1

    .line 23
    .line 24
    if-eq v0, v1, :cond_5

    .line 25
    .line 26
    const/16 v1, 0x230

    .line 27
    .line 28
    const-string v5, "ToolFunctionControl"

    .line 29
    .line 30
    if-eq v0, v1, :cond_4

    .line 31
    .line 32
    packed-switch v0, :pswitch_data_0

    .line 33
    .line 34
    .line 35
    :try_start_0
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 36
    .line 37
    .line 38
    move-result-object p3

    .line 39
    invoke-virtual {p3}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 40
    .line 41
    .line 42
    move-result-object p3

    .line 43
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_0

    .line 48
    .line 49
    const-string v0, "type"

    .line 50
    .line 51
    invoke-virtual {p3, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 52
    .line 53
    .line 54
    :cond_0
    const-string p2, "CSPdfImport"

    .line 55
    .line 56
    const-string v0, "import"

    .line 57
    .line 58
    invoke-static {p2, v0, p3}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :catch_0
    move-exception p2

    .line 63
    invoke-static {v5, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 64
    .line 65
    .line 66
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 67
    .line 68
    .line 69
    move-result p2

    .line 70
    sub-int/2addr p2, v4

    .line 71
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    check-cast p2, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 76
    .line 77
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 78
    .line 79
    .line 80
    move-result-object p3

    .line 81
    invoke-static {p3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 82
    .line 83
    .line 84
    move-result-wide v0

    .line 85
    invoke-static {v2, v4, v3}, Lcom/intsig/camscanner/experiment/DocImportOptExp;->〇o00〇〇Oo(ZILjava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    move-result p3

    .line 89
    if-eqz p3, :cond_1

    .line 90
    .line 91
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-ne p1, v4, :cond_2

    .line 96
    .line 97
    :cond_1
    const/4 v2, 0x1

    .line 98
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;

    .line 99
    .line 100
    if-eqz p1, :cond_3

    .line 101
    .line 102
    const-wide/16 v3, -0x1

    .line 103
    .line 104
    cmp-long p3, v0, v3

    .line 105
    .line 106
    if-eqz p3, :cond_3

    .line 107
    .line 108
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 109
    .line 110
    .line 111
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    .line 117
    .line 118
    goto/16 :goto_1

    .line 119
    .line 120
    :cond_3
    if-eqz v2, :cond_9

    .line 121
    .line 122
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    invoke-virtual {p2}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getPageCount()I

    .line 127
    .line 128
    .line 129
    move-result p2

    .line 130
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->ooOO(Landroid/net/Uri;I)V

    .line 131
    .line 132
    .line 133
    goto/16 :goto_1

    .line 134
    .line 135
    :pswitch_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 136
    .line 137
    .line 138
    move-result p2

    .line 139
    sub-int/2addr p2, v4

    .line 140
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 145
    .line 146
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇00O0O0(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 147
    .line 148
    .line 149
    goto/16 :goto_1

    .line 150
    .line 151
    :pswitch_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 152
    .line 153
    .line 154
    move-result p2

    .line 155
    sub-int/2addr p2, v4

    .line 156
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 161
    .line 162
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0O〇Oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 163
    .line 164
    .line 165
    goto/16 :goto_1

    .line 166
    .line 167
    :pswitch_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 168
    .line 169
    .line 170
    move-result p2

    .line 171
    sub-int/2addr p2, v4

    .line 172
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 177
    .line 178
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OOO8o〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 179
    .line 180
    .line 181
    goto :goto_1

    .line 182
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇80o08O(Ljava/util/List;)V

    .line 183
    .line 184
    .line 185
    goto :goto_1

    .line 186
    :pswitch_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 187
    .line 188
    .line 189
    move-result p2

    .line 190
    sub-int/2addr p2, v4

    .line 191
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 192
    .line 193
    .line 194
    move-result-object p1

    .line 195
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 196
    .line 197
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :pswitch_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 202
    .line 203
    .line 204
    move-result p2

    .line 205
    sub-int/2addr p2, v4

    .line 206
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 207
    .line 208
    .line 209
    move-result-object p1

    .line 210
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 211
    .line 212
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇o(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 213
    .line 214
    .line 215
    goto :goto_1

    .line 216
    :cond_4
    :try_start_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 217
    .line 218
    .line 219
    move-result-object p1

    .line 220
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 221
    .line 222
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 227
    .line 228
    .line 229
    move-result-wide p1

    .line 230
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->ooo0〇O88O(JZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 231
    .line 232
    .line 233
    goto :goto_1

    .line 234
    :catch_1
    move-exception p1

    .line 235
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 236
    .line 237
    .line 238
    goto :goto_1

    .line 239
    :cond_5
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 240
    .line 241
    .line 242
    move-result-object p1

    .line 243
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 244
    .line 245
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O〇0(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V

    .line 246
    .line 247
    .line 248
    goto :goto_1

    .line 249
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 250
    .line 251
    .line 252
    move-result p2

    .line 253
    sub-int/2addr p2, v4

    .line 254
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 259
    .line 260
    const/4 p2, 0x2

    .line 261
    invoke-static {p0, p1, v3, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇000O0(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/Long;ILjava/lang/Object;)V

    .line 262
    .line 263
    .line 264
    goto :goto_1

    .line 265
    :cond_7
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 266
    .line 267
    .line 268
    move-result-object p1

    .line 269
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 270
    .line 271
    const-string p2, "EXCEL"

    .line 272
    .line 273
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO8oO0o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    goto :goto_1

    .line 277
    :cond_8
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 278
    .line 279
    .line 280
    move-result-object p1

    .line 281
    check-cast p1, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 282
    .line 283
    const-string p2, "WORD"

    .line 284
    .line 285
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO8oO0o〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/String;)V

    .line 286
    .line 287
    .line 288
    :cond_9
    :goto_1
    return-void

    .line 289
    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇00(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇8(ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;
    .locals 2

    .line 1
    if-nez p2, :cond_0

    .line 2
    .line 3
    new-instance p2, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-direct {p2, v0, v1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;-><init>(Ljava/lang/String;Z)V

    .line 8
    .line 9
    .line 10
    :cond_0
    const/16 v0, 0x69

    .line 11
    .line 12
    if-eq p1, v0, :cond_1

    .line 13
    .line 14
    const/16 v0, 0xc9

    .line 15
    .line 16
    if-eq p1, v0, :cond_1

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇00()Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->setImportOriginPdf(Z)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇00〇8()Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;->setImportOriginPdf(Z)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-object p2
.end method

.method private final o〇O8〇〇o()V
    .locals 3

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    const-string v1, "dealQrCode>>>"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-wide/16 v1, -0x2

    .line 26
    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    sget-object v1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    if-eqz v1, :cond_0

    .line 38
    .line 39
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 43
    .line 44
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_QR_CODE_ONLY:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const v1, 0x138d5

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇o(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 16

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    const-string v0, "ENTRANCE_CAPTURE_MODE_SIGNATURE"

    .line 4
    .line 5
    const-string v2, "ToolFunctionControl"

    .line 6
    .line 7
    if-eqz p1, :cond_5

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-virtual {v3}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    if-eqz v3, :cond_1

    .line 18
    .line 19
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    if-eqz v3, :cond_0

    .line 24
    .line 25
    :try_start_0
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 30
    .line 31
    .line 32
    move-result-object v5

    .line 33
    invoke-static {v5, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    invoke-static {v3, v4, v0, v5}, Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;->〇O〇(JLjava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object v5, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    invoke-static {v5, v3, v4, v0}, Lcom/intsig/camscanner/newsign/signtype/SelectSignTypeHelper;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    move-exception v0

    .line 47
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 48
    .line 49
    .line 50
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_4

    .line 56
    .line 57
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 58
    .line 59
    .line 60
    move-result-wide v3

    .line 61
    const-wide/16 v5, 0x0

    .line 62
    .line 63
    cmp-long v0, v3, v5

    .line 64
    .line 65
    if-ltz v0, :cond_2

    .line 66
    .line 67
    iget-boolean v0, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇80〇808〇O:Z

    .line 68
    .line 69
    if-eqz v0, :cond_2

    .line 70
    .line 71
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    const v7, 0x7f130805

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 85
    .line 86
    .line 87
    move-result-wide v7

    .line 88
    cmp-long v0, v7, v5

    .line 89
    .line 90
    if-ltz v0, :cond_2

    .line 91
    .line 92
    invoke-static {v3, v4, v7, v8}, Lcom/intsig/camscanner/app/DBUtil;->OOo88OOo(JJ)J

    .line 93
    .line 94
    .line 95
    :cond_2
    iget-object v0, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO80:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 96
    .line 97
    if-nez v0, :cond_3

    .line 98
    .line 99
    sget-object v0, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_HOME_TAB:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 100
    .line 101
    :cond_3
    iget-object v3, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 102
    .line 103
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 104
    .line 105
    .line 106
    move-result-object v4

    .line 107
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 112
    .line 113
    .line 114
    move-result-object v5

    .line 115
    const-string v6, "other_app"

    .line 116
    .line 117
    const/4 v7, 0x1

    .line 118
    const/4 v8, 0x1

    .line 119
    const/4 v9, 0x1

    .line 120
    const/4 v10, 0x0

    .line 121
    const/4 v11, 0x0

    .line 122
    const/4 v12, 0x0

    .line 123
    const/4 v13, 0x0

    .line 124
    const/16 v14, 0x780

    .line 125
    .line 126
    const/4 v15, 0x0

    .line 127
    invoke-static/range {v3 .. v15}, Lcom/intsig/camscanner/pdf/signature/tab/SignatureEntranceUtil;->〇〇808〇(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/Integer;Ljava/lang/String;ZZZZLjava/lang/String;ZZILjava/lang/Object;)V

    .line 128
    .line 129
    .line 130
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_4
    const/4 v0, 0x0

    .line 134
    :goto_1
    if-nez v0, :cond_6

    .line 135
    .line 136
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    new-instance v3, Ljava/lang/StringBuilder;

    .line 141
    .line 142
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .line 144
    .line 145
    const-string v4, "start2Signature: uri = "

    .line 146
    .line 147
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    const-string v0, " "

    .line 154
    .line 155
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v0

    .line 162
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    goto :goto_2

    .line 166
    :cond_5
    const-string v0, "data == null"

    .line 167
    .line 168
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    :cond_6
    :goto_2
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final o〇〇0〇(Ljava/lang/Long;Landroid/net/Uri;)V
    .locals 33

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 4
    .line 5
    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 6
    .line 7
    .line 8
    move-object/from16 v0, p1

    .line 9
    .line 10
    iput-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 11
    .line 12
    const-string v3, "ToolFunctionControl"

    .line 13
    .line 14
    if-eqz p2, :cond_0

    .line 15
    .line 16
    :try_start_0
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 17
    .line 18
    .line 19
    move-result-wide v4

    .line 20
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iput-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    new-instance v4, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v5, "finishOriginPdfImport error\uff1a"

    .line 34
    .line 35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    :goto_0
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 49
    .line 50
    if-eqz v0, :cond_8

    .line 51
    .line 52
    check-cast v0, Ljava/lang/Number;

    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 55
    .line 56
    .line 57
    move-result-wide v4

    .line 58
    const-wide/16 v6, 0x0

    .line 59
    .line 60
    cmp-long v0, v4, v6

    .line 61
    .line 62
    if-gtz v0, :cond_1

    .line 63
    .line 64
    goto/16 :goto_1

    .line 65
    .line 66
    :cond_1
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 67
    .line 68
    iget-object v4, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 69
    .line 70
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-string v6, "finishOriginPdfImport docId\uff1a"

    .line 80
    .line 81
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    const-string v0, ", functionType:"

    .line 88
    .line 89
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    iget-object v0, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    const/16 v3, 0x69

    .line 109
    .line 110
    const/4 v4, 0x0

    .line 111
    if-eq v0, v3, :cond_7

    .line 112
    .line 113
    const/16 v3, 0xcd

    .line 114
    .line 115
    if-eq v0, v3, :cond_6

    .line 116
    .line 117
    const/16 v3, 0xcf

    .line 118
    .line 119
    if-eq v0, v3, :cond_5

    .line 120
    .line 121
    const/16 v3, 0xca

    .line 122
    .line 123
    if-eq v0, v3, :cond_4

    .line 124
    .line 125
    const/16 v3, 0xcb

    .line 126
    .line 127
    if-eq v0, v3, :cond_2

    .line 128
    .line 129
    packed-switch v0, :pswitch_data_0

    .line 130
    .line 131
    .line 132
    sget-object v5, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;

    .line 133
    .line 134
    iget-object v6, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 135
    .line 136
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 137
    .line 138
    check-cast v0, Ljava/lang/Number;

    .line 139
    .line 140
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 141
    .line 142
    .line 143
    move-result-wide v7

    .line 144
    const/4 v9, 0x0

    .line 145
    const/4 v10, 0x1

    .line 146
    const/4 v11, 0x0

    .line 147
    const/4 v12, 0x0

    .line 148
    const/4 v13, 0x0

    .line 149
    const/16 v14, 0x70

    .line 150
    .line 151
    const/4 v15, 0x0

    .line 152
    invoke-static/range {v5 .. v15}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;->O8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;Landroid/app/Activity;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 153
    .line 154
    .line 155
    goto/16 :goto_1

    .line 156
    .line 157
    :pswitch_0
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 158
    .line 159
    check-cast v0, Ljava/lang/Number;

    .line 160
    .line 161
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 162
    .line 163
    .line 164
    move-result-wide v2

    .line 165
    const-string v0, "PPT"

    .line 166
    .line 167
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Ooo(JLjava/lang/String;)V

    .line 168
    .line 169
    .line 170
    goto/16 :goto_1

    .line 171
    .line 172
    :pswitch_1
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 173
    .line 174
    check-cast v0, Ljava/lang/Number;

    .line 175
    .line 176
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 177
    .line 178
    .line 179
    move-result-wide v2

    .line 180
    const-string v0, "EXCEL"

    .line 181
    .line 182
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Ooo(JLjava/lang/String;)V

    .line 183
    .line 184
    .line 185
    goto/16 :goto_1

    .line 186
    .line 187
    :pswitch_2
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 188
    .line 189
    check-cast v0, Ljava/lang/Number;

    .line 190
    .line 191
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 192
    .line 193
    .line 194
    move-result-wide v2

    .line 195
    const-string v0, "WORD"

    .line 196
    .line 197
    invoke-virtual {v1, v2, v3, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Ooo(JLjava/lang/String;)V

    .line 198
    .line 199
    .line 200
    goto/16 :goto_1

    .line 201
    .line 202
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇O〇()Z

    .line 203
    .line 204
    .line 205
    move-result v0

    .line 206
    if-nez v0, :cond_3

    .line 207
    .line 208
    return-void

    .line 209
    :cond_3
    iget-object v0, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 210
    .line 211
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 212
    .line 213
    .line 214
    move-result-object v5

    .line 215
    const/4 v6, 0x0

    .line 216
    const/4 v7, 0x0

    .line 217
    new-instance v8, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$finishOriginPdfImport$1;

    .line 218
    .line 219
    invoke-direct {v8, v2, v1, v4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$finishOriginPdfImport$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lkotlin/coroutines/Continuation;)V

    .line 220
    .line 221
    .line 222
    const/4 v9, 0x3

    .line 223
    const/4 v10, 0x0

    .line 224
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 225
    .line 226
    .line 227
    goto :goto_1

    .line 228
    :cond_4
    sget-object v11, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;

    .line 229
    .line 230
    iget-object v12, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 231
    .line 232
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 233
    .line 234
    check-cast v0, Ljava/lang/Number;

    .line 235
    .line 236
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 237
    .line 238
    .line 239
    move-result-wide v13

    .line 240
    const-string v15, "from_pdf_signature"

    .line 241
    .line 242
    const/16 v16, 0x1

    .line 243
    .line 244
    const/16 v17, 0x0

    .line 245
    .line 246
    const/16 v18, 0x0

    .line 247
    .line 248
    const/16 v19, 0x0

    .line 249
    .line 250
    const/16 v20, 0x70

    .line 251
    .line 252
    const/16 v21, 0x0

    .line 253
    .line 254
    invoke-static/range {v11 .. v21}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;->O8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;Landroid/app/Activity;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 255
    .line 256
    .line 257
    goto :goto_1

    .line 258
    :cond_5
    sget-object v22, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;

    .line 259
    .line 260
    iget-object v0, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 261
    .line 262
    iget-object v2, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 263
    .line 264
    check-cast v2, Ljava/lang/Number;

    .line 265
    .line 266
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    .line 267
    .line 268
    .line 269
    move-result-wide v24

    .line 270
    const-string v26, "from_pdf_encryption"

    .line 271
    .line 272
    const/16 v27, 0x1

    .line 273
    .line 274
    const/16 v28, 0x0

    .line 275
    .line 276
    const/16 v29, 0x0

    .line 277
    .line 278
    const/16 v30, 0x0

    .line 279
    .line 280
    const/16 v31, 0x70

    .line 281
    .line 282
    const/16 v32, 0x0

    .line 283
    .line 284
    move-object/from16 v23, v0

    .line 285
    .line 286
    invoke-static/range {v22 .. v32}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;->O8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;Landroid/app/Activity;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 287
    .line 288
    .line 289
    goto :goto_1

    .line 290
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;

    .line 291
    .line 292
    iget-object v3, v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 293
    .line 294
    iget-object v2, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 295
    .line 296
    check-cast v2, Ljava/lang/Number;

    .line 297
    .line 298
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    .line 299
    .line 300
    .line 301
    move-result-wide v4

    .line 302
    const-string v6, "from_pdf_extract"

    .line 303
    .line 304
    const/4 v7, 0x1

    .line 305
    const/4 v8, 0x0

    .line 306
    const/4 v9, 0x0

    .line 307
    const/4 v10, 0x0

    .line 308
    const/16 v11, 0x70

    .line 309
    .line 310
    const/4 v12, 0x0

    .line 311
    move-object v2, v0

    .line 312
    invoke-static/range {v2 .. v12}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;->O8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;Landroid/app/Activity;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 313
    .line 314
    .line 315
    goto :goto_1

    .line 316
    :cond_7
    iget-object v0, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 317
    .line 318
    check-cast v0, Ljava/lang/Long;

    .line 319
    .line 320
    invoke-direct {v1, v4, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Ooo8〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/Long;)V

    .line 321
    .line 322
    .line 323
    :cond_8
    :goto_1
    return-void

    .line 324
    nop

    .line 325
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇00(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v1, p1

    move-object/from16 v0, p2

    .line 1
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    const-string v3, "ToolFunctionControl"

    if-eqz v2, :cond_0

    const-string v0, "activity error occur."

    .line 2
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 3
    :cond_0
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-static {v2}, Lcom/intsig/util/PermissionUtil;->O8ooOoo〇(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o8oO〇()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "need storage permission support."

    .line 5
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 7
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    move-result-object v3

    .line 8
    new-instance v4, LO88O/〇080;

    invoke-direct {v4, v6, v1, v0}, LO88O/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    return-void

    .line 9
    :cond_1
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 10
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    move-result v2

    const/16 v4, 0xcf

    if-ne v2, v4, :cond_2

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 11
    sget-object v4, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->PDF_ADD_PASSWORD:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 12
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 13
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_PASSWORD:Lcom/intsig/camscanner/purchase/entity/Function;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    move-result-object v1

    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    const-string v0, "PDF_ENCRYPTION purchaseVip"

    .line 16
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 17
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v2

    invoke-virtual {v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    move-result v2

    const-string v4, "CLICK_TOOL_FROM_TOOLPAGE"

    const-string v5, "cs_main_application"

    if-eqz v2, :cond_5

    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    move-result v2

    const/16 v7, 0xca

    if-ne v2, v7, :cond_5

    const-string v1, "CLICK_TOOL_FROM_HOME"

    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 19
    sget-object v0, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity;->O0O:Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;

    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v2, "cs_home_tab"

    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 20
    :cond_3
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21
    sget-object v0, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity;->O0O:Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;

    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v0, v1, v5}, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 22
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity;->O0O:Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;

    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v0, v1, v5}, Lcom/intsig/camscanner/newsign/transitionpage/ESignTransitionActivity$Companion;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)V

    :goto_0
    return-void

    .line 23
    :cond_5
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    move-result v2

    const/4 v7, 0x2

    const-string v8, "CSHome"

    if-eq v2, v7, :cond_29

    const/16 v7, 0xd0

    const/4 v9, 0x0

    if-eq v2, v7, :cond_27

    const/16 v7, 0x191

    if-eq v2, v7, :cond_26

    const/16 v7, 0x192

    if-eq v2, v7, :cond_25

    const/16 v7, 0x26c

    if-eq v2, v7, :cond_24

    const/16 v7, 0x26d

    if-eq v2, v7, :cond_23

    const/4 v7, 0x4

    const/4 v10, 0x1

    const/4 v11, 0x0

    packed-switch v2, :pswitch_data_0

    packed-switch v2, :pswitch_data_1

    const v0, 0x138d5

    const-wide/16 v3, -0x2

    packed-switch v2, :pswitch_data_2

    if-eqz v1, :cond_6

    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    :goto_1
    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 24
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇oo〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;ZLcom/intsig/camscanner/newsign/CsImportUsage;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 25
    :pswitch_0
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_WORD:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 26
    :pswitch_1
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    move-result-object v0

    const-string v1, "/backup/main"

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    move-result-object v0

    const-string v1, "arg_from_part"

    .line 28
    invoke-virtual {v0, v1, v5}, Lcom/alibaba/android/arouter/facade/Postcard;->withString(Ljava/lang/String;Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    goto/16 :goto_b

    .line 30
    :pswitch_2
    new-instance v1, Lcom/intsig/camscanner/app/StartCameraBuilder;

    invoke-direct {v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 31
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 32
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 33
    invoke-virtual {v1, v3, v4}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 34
    sget-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->INVOICE:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_INVOICE:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 36
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    goto/16 :goto_b

    .line 38
    :pswitch_3
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇080()I

    move-result v2

    if-ne v2, v7, :cond_7

    .line 39
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇80〇808〇O()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 40
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v2, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 41
    invoke-static {v10}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->〇〇8O0〇8(Z)V

    if-eqz v1, :cond_7

    .line 42
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    :cond_7
    const-string v1, "tab_count_mode"

    .line 43
    invoke-static {v8, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    new-instance v1, Lcom/intsig/camscanner/app/StartCameraBuilder;

    invoke-direct {v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 45
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 46
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 47
    invoke-virtual {v1, v3, v4}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 48
    sget-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->COUNT_NUMBER:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 49
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_COUNT_NUMBER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v1

    .line 50
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    goto/16 :goto_b

    .line 52
    :pswitch_4
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇80〇808〇O()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 53
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 54
    invoke-static {v10}, Lcom/intsig/camscanner/settings/workflow/WorkflowUtils;->〇o〇(Z)V

    if-eqz v1, :cond_8

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 56
    :cond_8
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 57
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v1, "cs_application"

    invoke-static {v0, v1}, Lcom/intsig/camscanner/settings/workflow/WorkflowUtils;->〇080(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_b

    .line 58
    :cond_9
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-static {v0, v9}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->〇o00〇〇Oo(Landroid/content/Context;Lcom/intsig/tsapp/account/model/LoginMainArgs;)Landroid/content/Intent;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/intsig/result/GetActivityResult;

    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-direct {v1, v2}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    const/16 v2, 0x70

    .line 60
    invoke-virtual {v1, v0, v2}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$3;

    invoke-direct {v1, v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$3;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    invoke-virtual {v0, v1}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    goto/16 :goto_b

    .line 62
    :pswitch_5
    instance-of v0, v1, Lcom/intsig/camscanner/mainmenu/mainpage/KingKongAdapter;

    if-eqz v0, :cond_a

    const-string v0, "tab_smart_remove"

    .line 63
    invoke-static {v8, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_a
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇080()I

    move-result v0

    if-ne v0, v7, :cond_b

    .line 65
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇80〇808〇O()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 66
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 67
    invoke-static {v10}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->OoO8(Z)V

    if-eqz v1, :cond_c

    .line 68
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 69
    :cond_b
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->Oooo8o0〇()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 70
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 71
    invoke-static {v10}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->OoO8(Z)V

    if-eqz v1, :cond_c

    .line 72
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 73
    :cond_c
    :goto_2
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    sget-object v1, Lcom/intsig/camscanner/innovationlab/InnovationLabActivity;->〇〇08O:Lcom/intsig/camscanner/innovationlab/InnovationLabActivity$Companion;

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/innovationlab/InnovationLabActivity$Companion;->〇080(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_b

    .line 74
    :pswitch_6
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->Oooo8o0〇()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 75
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    if-eqz v1, :cond_d

    .line 76
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 77
    :cond_d
    invoke-static {v11}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo〇Oo〇O00(Z)V

    .line 78
    :cond_e
    new-instance v0, Lcom/intsig/webview/data/WebArgs;

    invoke-direct {v0}, Lcom/intsig/webview/data/WebArgs;-><init>()V

    .line 79
    invoke-virtual {v0, v11}, Lcom/intsig/webview/data/WebArgs;->〇oOO8O8(Z)V

    .line 80
    invoke-virtual {v0, v11}, Lcom/intsig/webview/data/WebArgs;->〇0000OOO(Z)V

    .line 81
    iget-object v12, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v13, ""

    invoke-static {}, Lcom/intsig/utils/WebUrlUtils;->〇0〇O0088o()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v17, v0

    invoke-static/range {v12 .. v17}, Lcom/intsig/webview/util/WebUtil;->〇O〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLcom/intsig/webview/data/WebArgs;)V

    goto/16 :goto_b

    :pswitch_7
    const-string v0, "BUY_DEVICE"

    .line 82
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v0

    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

    if-eqz v0, :cond_15

    .line 84
    iget-object v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_link:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_f

    goto :goto_3

    :cond_f
    const/4 v1, 0x0

    goto :goto_4

    :cond_10
    :goto_3
    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_11

    const-string v0, "toolbox_link is empty"

    .line 85
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 86
    :cond_11
    iget-object v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_mini_app:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_12

    goto :goto_5

    :cond_12
    const/4 v10, 0x0

    :cond_13
    :goto_5
    if-nez v10, :cond_14

    .line 87
    iget-object v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_mini_app:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "toolbox_mini_app = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const v2, 0x7f131fa6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity.getString(R.str\u2026ll_open_wx_small_routine)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v2, Lcom/intsig/app/AlertDialog;

    iget-object v4, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-direct {v2, v4}, Lcom/intsig/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 90
    invoke-virtual {v2, v1}, Lcom/intsig/app/AlertDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {v2, v11}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 92
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const v4, 0x7f13057e

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 93
    new-instance v4, LO88O/〇o00〇〇Oo;

    invoke-direct {v4, v2}, LO88O/〇o00〇〇Oo;-><init>(Lcom/intsig/app/AlertDialog;)V

    const/4 v5, -0x2

    invoke-virtual {v2, v5, v1, v4}, Lcom/intsig/app/AlertDialog;->〇〇8O0〇8(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 94
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const v4, 0x7f131db8

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    new-instance v4, LO88O/〇o〇;

    invoke-direct {v4, v2, v6, v0}, LO88O/〇o〇;-><init>(Lcom/intsig/app/AlertDialog;Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;)V

    const/4 v0, -0x1

    invoke-virtual {v2, v0, v1, v4}, Lcom/intsig/app/AlertDialog;->〇〇8O0〇8(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 96
    invoke-virtual {v2}, Lcom/intsig/app/AlertDialog;->show()V

    goto :goto_6

    .line 97
    :cond_14
    iget-object v1, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_link:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "toolbox_link="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_link:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 99
    :goto_6
    sget-object v9, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_15
    if-nez v9, :cond_2a

    const-string v0, "printer_buy_entry is null"

    .line 100
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    :pswitch_8
    const-string v0, "PRINTER_DOC"

    .line 101
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/intsig/camscanner/printer/PrintUtil;->〇080:Lcom/intsig/camscanner/printer/PrintUtil;

    .line 103
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 104
    new-instance v2, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$4;

    invoke-direct {v2, v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$4;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    const/16 v3, 0x25b

    .line 105
    invoke-direct {v6, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O000(I)Z

    move-result v3

    .line 106
    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/intsig/camscanner/printer/PrintUtil;->〇〇8O0〇8(Landroid/app/Activity;Ljava/lang/String;Lcom/intsig/camscanner/printer/contract/PreparePrintDataCallback;Z)V

    goto/16 :goto_b

    .line 107
    :pswitch_9
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.toolpage.entity.ToolTabAdItem"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;

    .line 108
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolTabAdItem;->〇00〇8(Landroid/content/Context;)V

    goto/16 :goto_b

    .line 109
    :pswitch_a
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇O8〇〇o()V

    .line 110
    instance-of v0, v1, Lcom/intsig/camscanner/mainmenu/mainpage/KingKongAdapter;

    if-eqz v0, :cond_16

    .line 111
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;

    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodeLogAgent;->〇〇888()V

    .line 112
    :cond_16
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    move-result v0

    const/16 v2, 0x260

    if-ne v0, v2, :cond_2a

    .line 113
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 114
    invoke-static {v11}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo0O00〇80(Z)V

    if-eqz v1, :cond_2a

    .line 115
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    goto/16 :goto_b

    .line 116
    :pswitch_b
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->BANK_CARD_JOURNAL:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 117
    :pswitch_c
    invoke-direct/range {p0 .. p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0000OOO(Lcom/chad/library/adapter/base/BaseQuickAdapter;)V

    goto/16 :goto_b

    .line 118
    :pswitch_d
    iget v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇808〇:I

    const-string v2, ""

    const-string v3, "cs_tool_page"

    const-string v5, "cs_home"

    if-eqz v1, :cond_18

    if-eq v1, v10, :cond_17

    move-object/from16 v22, v2

    goto :goto_7

    :cond_17
    move-object/from16 v22, v3

    goto :goto_7

    :cond_18
    move-object/from16 v22, v5

    :goto_7
    if-eqz v1, :cond_1a

    if-eq v1, v10, :cond_19

    goto :goto_8

    :cond_19
    const-string v2, "import_pic_cs_tool_page"

    goto :goto_8

    :cond_1a
    const-string v2, "import_pic_cs_home"

    :goto_8
    move-object/from16 v23, v2

    .line 119
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportPicOptExp;->〇080()Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowingWechatMiniImport()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 120
    sget-object v11, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OoO8:Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;

    .line 121
    iget-object v12, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v13, ""

    const-wide/16 v14, -0x2

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v24, 0xf0

    const/16 v25, 0x0

    .line 122
    invoke-static/range {v11 .. v25}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;->OO0o〇〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;JZLcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$ImportGalleryInterface;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/newsign/CsImportUsage;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 123
    :cond_1b
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowingWechatMiniImport()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 124
    sget-object v11, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;->O8o08O8O:Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;

    const-string v12, ""

    const-wide/16 v13, -0x2

    .line 125
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    move-object v15, v3

    goto :goto_9

    :cond_1c
    move-object v15, v5

    :goto_9
    const/16 v16, 0x0

    const/16 v19, 0x8

    const/16 v20, 0x0

    move-object/from16 v17, v22

    move-object/from16 v18, v23

    .line 126
    invoke-static/range {v11 .. v20}, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;->〇80〇808〇O(Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;Ljava/lang/String;JLjava/lang/String;Lcom/intsig/camscanner/newsign/CsImportUsage;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;

    move-result-object v0

    .line 127
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v1

    const-string v2, "activity.supportFragmentManager.beginTransaction()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 129
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_b

    .line 130
    :cond_1d
    sget-object v11, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OoO8:Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;

    .line 131
    iget-object v12, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v13, ""

    const-wide/16 v14, -0x2

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v24, 0x1f0

    const/16 v25, 0x0

    .line 132
    invoke-static/range {v11 .. v25}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;->OO0o〇〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$Companion;Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;JZLcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$ImportGalleryInterface;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/newsign/CsImportUsage;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 133
    :pswitch_e
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->PPT:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 134
    :pswitch_f
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->forbidNcnnLibForBookScene()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 135
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    :cond_1e
    const-string v0, "startToolCellFunction: click but ncnn forbidden"

    .line 136
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 137
    :pswitch_10
    sget-object v0, Lcom/intsig/camscanner/paper/CamExamGuideManager;->〇080:Lcom/intsig/camscanner/paper/CamExamGuideManager;

    sget-object v1, Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;->CAM_EXAM_GUIDE_ENTRANCE_TYPE_TOOL_BOX:Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/paper/CamExamGuideManager;->〇080(Lcom/intsig/camscanner/paper/CamExamGuideManager$CamExamGuideType;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 138
    sget-object v0, Lcom/intsig/camscanner/paper/CamExamGuideBottomDialog;->〇080OO8〇0:Lcom/intsig/camscanner/paper/CamExamGuideBottomDialog$Companion;

    .line 139
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    .line 140
    new-instance v2, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$2;

    invoke-direct {v2, v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealToolCellFunction$2;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 141
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/paper/CamExamGuideBottomDialog$Companion;->〇080(Landroidx/fragment/app/FragmentManager;Lcom/intsig/camscanner/paper/CamExamGuideBottomDialog$CallBack;)V

    goto/16 :goto_b

    .line 142
    :cond_1f
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 143
    :pswitch_11
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 144
    :pswitch_12
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 145
    :pswitch_13
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇080()I

    move-result v0

    if-ne v0, v7, :cond_20

    .line 146
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 147
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇88()Z

    move-result v0

    if-nez v0, :cond_21

    .line 148
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8o〇o()V

    goto :goto_a

    .line 149
    :cond_20
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇oo〇(Z)V

    .line 150
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇80O()V

    :cond_21
    :goto_a
    if-eqz v1, :cond_22

    .line 151
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 152
    :cond_22
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 153
    :pswitch_14
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    .line 154
    :pswitch_15
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto/16 :goto_b

    :cond_23
    const-string v0, "onclick white pad"

    .line 155
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 157
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 158
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN_TOOLBOX:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 159
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    goto :goto_b

    :cond_24
    const-string v0, "onclick  write pad"

    .line 162
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 164
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 165
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN_TOOLBOX:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 166
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 167
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_WRITING_BOARD:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    goto :goto_b

    .line 169
    :cond_25
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->TRANSLATE:Lcom/intsig/camscanner/capture/CaptureMode;

    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V

    goto :goto_b

    .line 170
    :cond_26
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->O8()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇0OOo〇0(Ljava/lang/String;)V

    goto :goto_b

    .line 171
    :cond_27
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->O8()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 172
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    invoke-static {v9, v9}, Lcom/intsig/camscanner/pdf/kit/ShareToCsPdfUtil;->〇080(Ljava/lang/String;Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_b

    .line 173
    :cond_28
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    const-string v1, "com.intsig.cspdf"

    const-string v2, "gp_cs_cs_tools_icon"

    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/IntentUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_b

    :cond_29
    const-string v0, " KingKongToolKit check to cs pdf"

    .line 174
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->KingKongToolKit:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    invoke-direct {v6, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OoO8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)V

    const-string v1, "refer_source"

    .line 176
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->getCsPdfTrack()Ljava/lang/String;

    move-result-object v0

    const-string v2, "tab_pdf_package"

    invoke-static {v8, v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a
    :goto_b
    return-void

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x259
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x260
        :pswitch_a
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final 〇0000OOO(Lcom/chad/library/adapter/base/BaseQuickAdapter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    const-string v1, "dealTopicPaper>>>"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 12
    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O〇88O8O()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o0o()V

    .line 21
    .line 22
    .line 23
    :cond_0
    if-eqz p1, :cond_1

    .line 24
    .line 25
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 26
    .line 27
    .line 28
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 29
    .line 30
    invoke-direct {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    const-wide/16 v0, -0x2

    .line 46
    .line 47
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    sget-object v0, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC_PAPER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
.end method

.method static synthetic 〇000O0(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/Long;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Ooo8〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;Ljava/lang/Long;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final 〇00O0O0(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 5

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    const-string v1, "batch handle images finish, go to view doc"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v0, Landroid/content/Intent;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    const-class v3, Lcom/intsig/camscanner/DocumentActivity;

    .line 19
    .line 20
    const-string v4, "android.intent.action.VIEW"

    .line 21
    .line 22
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 23
    .line 24
    .line 25
    const-string v1, "constant_doc_enc_green_channel"

    .line 26
    .line 27
    const/4 v2, 0x1

    .line 28
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getPageCount()I

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    if-le v1, v2, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const/4 v2, 0x0

    .line 39
    :goto_0
    const-string v1, "constant_show_batch_process_tips"

    .line 40
    .line 41
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    const-string v1, "constant_doc_enc_green_channel_ori_path"

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getOriginalPath()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_1
    const-string p1, "data == null"

    .line 60
    .line 61
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :goto_1
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8(Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V
    .locals 8

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    const-string v1, "goPdfToOffice:uri"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    move-object v2, v0

    .line 14
    move-object v4, p2

    .line 15
    move-object v6, p1

    .line 16
    move-object v7, p4

    .line 17
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V

    .line 18
    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 21
    .line 22
    const/4 p2, 0x0

    .line 23
    if-nez p3, :cond_0

    .line 24
    .line 25
    iput-boolean p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O00:Z

    .line 26
    .line 27
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 28
    .line 29
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 30
    .line 31
    .line 32
    move-result p3

    .line 33
    iget-object p4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 34
    .line 35
    invoke-direct {p0, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8(ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    const/4 p3, 0x1

    .line 42
    new-array p3, p3, [Landroid/net/Uri;

    .line 43
    .line 44
    aput-object p1, p3, p2

    .line 45
    .line 46
    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;

    .line 51
    .line 52
    const/4 v4, 0x0

    .line 53
    const/4 v5, 0x1

    .line 54
    const/4 v6, 0x0

    .line 55
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportOfficeByUri(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;ZZZ)V

    .line 56
    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 60
    .line 61
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 62
    .line 63
    .line 64
    move-result p3

    .line 65
    const/16 p4, 0x65

    .line 66
    .line 67
    if-eq p3, p4, :cond_2

    .line 68
    .line 69
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 70
    .line 71
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 72
    .line 73
    .line 74
    move-result p3

    .line 75
    const/16 p4, 0x66

    .line 76
    .line 77
    if-ne p3, p4, :cond_1

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 81
    .line 82
    if-eqz p1, :cond_4

    .line 83
    .line 84
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->O8()V

    .line 85
    .line 86
    .line 87
    goto :goto_1

    .line 88
    :cond_2
    :goto_0
    iget-wide p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 89
    .line 90
    const-wide/16 v0, 0x0

    .line 91
    .line 92
    cmp-long v2, p3, v0

    .line 93
    .line 94
    if-nez v2, :cond_3

    .line 95
    .line 96
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 97
    .line 98
    iget-object p4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;

    .line 101
    .line 102
    invoke-static {p3, p1, p4, v0, p2}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportByUri(Landroid/content/Context;Landroid/net/Uri;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;Z)V

    .line 103
    .line 104
    .line 105
    goto :goto_1

    .line 106
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 107
    .line 108
    if-eqz p1, :cond_4

    .line 109
    .line 110
    iget-boolean p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇0:Z

    .line 111
    .line 112
    invoke-virtual {p1, p3, p4, p2}, Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;->〇o〇(JZ)V

    .line 113
    .line 114
    .line 115
    :cond_4
    :goto_1
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O00:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p5, 0x8

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    sget-object p4, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->PDF_TOOLS:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 6
    .line 7
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8(Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method public static synthetic 〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;IILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, -0x1

    .line 6
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->ooOO(Landroid/net/Uri;I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic 〇O00(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O888o0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O888o0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Context;Ljava/util/ArrayList;ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;ZZILjava/lang/Object;)V
    .locals 7

    .line 1
    and-int/lit8 p8, p7, 0x8

    .line 2
    .line 3
    if-eqz p8, :cond_0

    .line 4
    .line 5
    const/4 p4, 0x0

    .line 6
    :cond_0
    move-object v4, p4

    .line 7
    and-int/lit8 p4, p7, 0x10

    .line 8
    .line 9
    const/4 p8, 0x0

    .line 10
    if-eqz p4, :cond_1

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    goto :goto_0

    .line 14
    :cond_1
    move v5, p5

    .line 15
    :goto_0
    and-int/lit8 p4, p7, 0x20

    .line 16
    .line 17
    if-eqz p4, :cond_2

    .line 18
    .line 19
    const/4 v6, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_2
    move v6, p6

    .line 22
    :goto_1
    move-object v0, p0

    .line 23
    move-object v1, p1

    .line 24
    move-object v2, p2

    .line 25
    move v3, p3

    .line 26
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o800o8O(Landroid/content/Context;Ljava/util/ArrayList;ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;ZZ)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇888:Lcom/intsig/camscanner/pdf/office/PdfToOfficeMain;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo08:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇oOO8O8(Lcom/intsig/app/AlertDialog;Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p3, "$dialog"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$this_run"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 17
    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;->〇o00〇〇Oo()Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    iget-object p1, p1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    iget-object p3, p2, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_mini_app:Ljava/lang/String;

    .line 26
    .line 27
    iget-object p2, p2, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->toolbox_link:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {p0, p1, p3, p2}, Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;->o〇0(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;ZLcom/intsig/camscanner/newsign/CsImportUsage;ILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p5, p4, 0x1

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    and-int/lit8 p5, p4, 0x2

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    if-eqz p5, :cond_1

    .line 10
    .line 11
    const/4 p2, 0x0

    .line 12
    :cond_1
    and-int/lit8 p4, p4, 0x4

    .line 13
    .line 14
    if-eqz p4, :cond_2

    .line 15
    .line 16
    new-instance p3, Lcom/intsig/camscanner/newsign/CsImportUsage;

    .line 17
    .line 18
    invoke-direct {p3, v0}, Lcom/intsig/camscanner/newsign/CsImportUsage;-><init>(I)V

    .line 19
    .line 20
    .line 21
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oo88o8O(Lcom/chad/library/adapter/base/BaseQuickAdapter;ZLcom/intsig/camscanner/newsign/CsImportUsage;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic 〇o〇(Lcom/intsig/app/AlertDialog;Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇oOO8O8(Lcom/intsig/app/AlertDialog;Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic 〇〇0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p4, 0x2

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 6
    .line 7
    :cond_0
    and-int/lit8 p4, p4, 0x4

    .line 8
    .line 9
    if-eqz p4, :cond_1

    .line 10
    .line 11
    sget-object p3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 12
    .line 13
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇〇0〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    sput-object p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o800o8O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/Long;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇〇0〇(Ljava/lang/Long;Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final O0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO0o〇〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O0O8OO088(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇808〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O8O〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final OOO8o〇〇(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 5

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    const-string v1, "batch handle images finish, go to view doc"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v0, Landroid/content/Intent;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    const-class v3, Lcom/intsig/camscanner/DocumentActivity;

    .line 19
    .line 20
    const-string v4, "android.intent.action.VIEW"

    .line 21
    .line 22
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->isCsDoc()Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT_CS_DOC:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 39
    .line 40
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    :goto_0
    const-string v1, "constant_doc_edit_type"

    .line 45
    .line 46
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 52
    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_1
    const-string p1, "data == null"

    .line 56
    .line 57
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :goto_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final OOo8o〇O(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "onToolCellFunctionClicked"

    .line 2
    .line 3
    const-string v1, "ToolFunctionControl"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string p1, "activity error occur."

    .line 17
    .line 18
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    const-string v0, "CLICK_TOOL_FROM_TOOLPAGE"

    .line 23
    .line 24
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/4 v1, 0x0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    packed-switch v0, :pswitch_data_0

    .line 38
    .line 39
    .line 40
    :pswitch_0
    goto :goto_0

    .line 41
    :pswitch_1
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfSort:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :pswitch_2
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfExtract:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :pswitch_3
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileMerge:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :pswitch_4
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileSignature:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :pswitch_5
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolImportFile:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 54
    .line 55
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 56
    .line 57
    sget-object v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇O8o08O:Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$Companion;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_2

    .line 64
    .line 65
    new-instance v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 66
    .line 67
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 68
    .line 69
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    new-instance v2, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startToolCellFunction$1;

    .line 77
    .line 78
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startToolCellFunction$1;-><init>(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function2;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startToolCellFunction$2;

    .line 86
    .line 87
    invoke-direct {v1, p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startToolCellFunction$2;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇080(Lkotlin/jvm/functions/Function0;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇()Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇0〇O0088o()V

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇00(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    :goto_1
    return-void

    .line 106
    nop

    .line 107
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final Oo8Oo00oo(I)I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceType"
        }
    .end annotation

    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation

    .line 1
    const/16 v0, 0xcc

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const p1, 0x7f1307fa

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 p1, -0x1

    .line 10
    :goto_0
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Ooo(JLjava/lang/String;)V
    .locals 10
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$localOriginPdf2Office$1;

    .line 15
    .line 16
    const/4 v9, 0x0

    .line 17
    move-object v4, v0

    .line 18
    move-wide v5, p1

    .line 19
    move-object v7, p0

    .line 20
    move-object v8, p3

    .line 21
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$localOriginPdf2Office$1;-><init>(JLcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 22
    .line 23
    .line 24
    const/4 v5, 0x3

    .line 25
    const/4 v6, 0x0

    .line 26
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oo〇O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO80:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo〇o(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "intent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/result/GetActivityResult;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    invoke-direct {v0, v1}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 11
    .line 12
    .line 13
    const/16 v1, 0x67

    .line 14
    .line 15
    invoke-virtual {v0, p1, v1}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startActivityForAddWatermark$1;

    .line 20
    .line 21
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$startActivityForAddWatermark$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o800o8O(Landroid/content/Context;Ljava/util/ArrayList;ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;ZZ)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;",
            ">;I",
            "Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;",
            "ZZ)V"
        }
    .end annotation

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "dataList"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    :cond_0
    if-nez p5, :cond_1

    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O00:Z

    .line 27
    .line 28
    invoke-direct {p0, p3, p4}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8(ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 33
    .line 34
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;

    .line 35
    .line 36
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO00OOO(I)Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    const/4 v5, 0x1

    .line 41
    move-object v1, p2

    .line 42
    move v6, p6

    .line 43
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportOfficeByPath(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;ZZZ)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object p5, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;

    .line 48
    .line 49
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO00OOO(I)Z

    .line 50
    .line 51
    .line 52
    move-result p3

    .line 53
    invoke-static {p1, p2, p4, p5, p3}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportByPath(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;Z)V

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public final o88〇OO08〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8o8o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o8O〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oooo8o0〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final oo88o8O(Lcom/chad/library/adapter/base/BaseQuickAdapter;ZLcom/intsig/camscanner/newsign/CsImportUsage;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;Z",
            "Lcom/intsig/camscanner/newsign/CsImportUsage;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v3, p3

    .line 4
    .line 5
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 8
    .line 9
    .line 10
    move-result v5

    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "dealPdfRelative functionType == "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "ToolFunctionControl"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/16 v0, 0x25f

    .line 34
    .line 35
    const/4 v7, 0x1

    .line 36
    const/4 v8, 0x0

    .line 37
    if-nez p2, :cond_1

    .line 38
    .line 39
    if-eq v0, v5, :cond_1

    .line 40
    .line 41
    iget-boolean v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO0o〇〇:Z

    .line 42
    .line 43
    if-eqz v1, :cond_0

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    const/4 v1, 0x0

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 49
    :goto_1
    invoke-virtual {v6, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇80(I)Z

    .line 50
    .line 51
    .line 52
    move-result v12

    .line 53
    invoke-virtual {v6, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo8Oo00oo(I)I

    .line 54
    .line 55
    .line 56
    move-result v13

    .line 57
    invoke-direct {v6, v5, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->ooo〇8oO(IZ)Z

    .line 58
    .line 59
    .line 60
    move-result v4

    .line 61
    invoke-direct {v6, v5, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O0o〇〇Oo(IZ)Z

    .line 62
    .line 63
    .line 64
    move-result v18

    .line 65
    const/4 v1, 0x3

    .line 66
    new-array v1, v1, [Ljava/lang/Integer;

    .line 67
    .line 68
    const/16 v2, 0xc9

    .line 69
    .line 70
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 71
    .line 72
    .line 73
    move-result-object v9

    .line 74
    aput-object v9, v1, v8

    .line 75
    .line 76
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 77
    .line 78
    .line 79
    move-result-object v9

    .line 80
    aput-object v9, v1, v7

    .line 81
    .line 82
    const/16 v9, 0x68

    .line 83
    .line 84
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 85
    .line 86
    .line 87
    move-result-object v9

    .line 88
    const/4 v10, 0x2

    .line 89
    aput-object v9, v1, v10

    .line 90
    .line 91
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 92
    .line 93
    .line 94
    move-result-object v1

    .line 95
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 96
    .line 97
    .line 98
    move-result-object v9

    .line 99
    invoke-interface {v1, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    const-string v15, "INTENT_FUNCTION_USAGE"

    .line 104
    .line 105
    if-eqz v1, :cond_5

    .line 106
    .line 107
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 108
    .line 109
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    if-ne v1, v2, :cond_2

    .line 114
    .line 115
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 116
    .line 117
    invoke-virtual {v1, v8}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 118
    .line 119
    .line 120
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;

    .line 121
    .line 122
    invoke-virtual {v1, v8}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->OO0o〇〇〇〇0(Z)V

    .line 123
    .line 124
    .line 125
    if-eqz p1, :cond_2

    .line 126
    .line 127
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 128
    .line 129
    .line 130
    :cond_2
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 131
    .line 132
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 133
    .line 134
    .line 135
    move-result v1

    .line 136
    if-ne v1, v0, :cond_3

    .line 137
    .line 138
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 139
    .line 140
    invoke-static {v0, v8}, Lcom/intsig/camscanner/util/MainMenuTipsChecker;->〇80〇808〇O(Landroid/content/Context;Z)Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    if-eqz v0, :cond_3

    .line 145
    .line 146
    new-instance v0, Ljava/util/ArrayList;

    .line 147
    .line 148
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .line 150
    .line 151
    iget-object v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 152
    .line 153
    invoke-static {v1, v7}, Lcom/intsig/camscanner/util/MainMenuTipsChecker;->〇80〇808〇O(Landroid/content/Context;Z)Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    if-eqz v1, :cond_4

    .line 158
    .line 159
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇80〇808〇O()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v1

    .line 163
    if-eqz v1, :cond_4

    .line 164
    .line 165
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    .line 167
    .line 168
    goto :goto_2

    .line 169
    :cond_3
    const/4 v0, 0x0

    .line 170
    :cond_4
    :goto_2
    move-object v10, v0

    .line 171
    iget-object v9, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 172
    .line 173
    const-string v11, "cs_tool_page"

    .line 174
    .line 175
    iget-boolean v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8o8o〇:Z

    .line 176
    .line 177
    iget-boolean v1, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oooo8o0〇:Z

    .line 178
    .line 179
    iget-object v2, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 180
    .line 181
    move/from16 v14, v18

    .line 182
    .line 183
    move-object v7, v15

    .line 184
    move v15, v0

    .line 185
    move/from16 v16, v1

    .line 186
    .line 187
    move-object/from16 v17, v2

    .line 188
    .line 189
    invoke-static/range {v9 .. v17}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->oO80(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;ZIZZZLcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Landroid/content/Intent;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 194
    .line 195
    .line 196
    goto :goto_5

    .line 197
    :cond_5
    move-object v7, v15

    .line 198
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 199
    .line 200
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 201
    .line 202
    .line 203
    move-result v0

    .line 204
    const/16 v1, 0xcc

    .line 205
    .line 206
    if-eq v0, v1, :cond_6

    .line 207
    .line 208
    packed-switch v0, :pswitch_data_0

    .line 209
    .line 210
    .line 211
    const/16 v19, 0x0

    .line 212
    .line 213
    goto :goto_3

    .line 214
    :pswitch_0
    const/16 v19, 0x1

    .line 215
    .line 216
    :goto_3
    const/16 v20, 0x0

    .line 217
    .line 218
    goto :goto_4

    .line 219
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 220
    .line 221
    const/4 v1, 0x5

    .line 222
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->Oooo8o0〇(I)V

    .line 223
    .line 224
    .line 225
    const/16 v19, 0x0

    .line 226
    .line 227
    const/16 v20, 0x1

    .line 228
    .line 229
    :goto_4
    iget-object v14, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 230
    .line 231
    const-string v15, "cs_tool_page"

    .line 232
    .line 233
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 234
    .line 235
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 236
    .line 237
    .line 238
    move-result v0

    .line 239
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇80(I)Z

    .line 240
    .line 241
    .line 242
    move-result v16

    .line 243
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 244
    .line 245
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 246
    .line 247
    .line 248
    move-result v0

    .line 249
    invoke-virtual {v6, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo8Oo00oo(I)I

    .line 250
    .line 251
    .line 252
    move-result v17

    .line 253
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 254
    .line 255
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 256
    .line 257
    .line 258
    move-result v0

    .line 259
    invoke-direct {v6, v0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O000(I)Z

    .line 260
    .line 261
    .line 262
    move-result v21

    .line 263
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 264
    .line 265
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->Oo08()Ljava/lang/String;

    .line 266
    .line 267
    .line 268
    move-result-object v22

    .line 269
    invoke-static/range {v14 .. v22}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryActivity;->o〇08oO80o(Landroid/content/Context;Ljava/lang/String;ZIZZZZLjava/lang/String;)Landroid/content/Intent;

    .line 270
    .line 271
    .line 272
    move-result-object v0

    .line 273
    const-string v1, "getIntentDocAndPdf(\n    \u2026em.iconDesc\n            )"

    .line 274
    .line 275
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 279
    .line 280
    .line 281
    :goto_5
    move-object v2, v0

    .line 282
    iget-object v7, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 283
    .line 284
    new-instance v9, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealPdfRelative$2;

    .line 285
    .line 286
    move-object v0, v9

    .line 287
    move-object/from16 v1, p0

    .line 288
    .line 289
    move-object/from16 v3, p3

    .line 290
    .line 291
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl$dealPdfRelative$2;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;Lcom/intsig/camscanner/newsign/CsImportUsage;ZI)V

    .line 292
    .line 293
    .line 294
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 295
    .line 296
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 297
    .line 298
    .line 299
    move-result v0

    .line 300
    const/16 v1, 0x67

    .line 301
    .line 302
    if-ne v0, v1, :cond_7

    .line 303
    .line 304
    const/4 v8, 0x1

    .line 305
    :cond_7
    const-string v0, "other"

    .line 306
    .line 307
    const-string v1, "cs_main_application"

    .line 308
    .line 309
    invoke-static {v7, v9, v8, v0, v1}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 310
    .line 311
    .line 312
    return-void

    .line 313
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final ooOO(Landroid/net/Uri;I)V
    .locals 4

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    if-eqz p1, :cond_2

    .line 4
    .line 5
    const-string v1, "batch handle images finish, go to view doc"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v0, Landroid/content/Intent;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    const-class v2, Lcom/intsig/camscanner/DocumentActivity;

    .line 15
    .line 16
    const-string v3, "android.intent.action.VIEW"

    .line 17
    .line 18
    invoke-direct {v0, v3, p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    if-le p2, p1, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 p1, 0x0

    .line 26
    :goto_0
    const-string p2, "constant_show_batch_process_tips"

    .line 27
    .line 28
    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o〇:Ljava/lang/String;

    .line 32
    .line 33
    if-eqz p1, :cond_1

    .line 34
    .line 35
    const-string p2, "EXTRA_LOTTERY_VALUE"

    .line 36
    .line 37
    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    .line 39
    .line 40
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_2
    const-string p1, "data == null"

    .line 47
    .line 48
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :goto_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final o〇0OOo〇0(Ljava/lang/String;)V
    .locals 4

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    const-string v2, "Uri.parse(it) ?: return"

    .line 13
    .line 14
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    new-instance v2, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v3, "deeplink uri = "

    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    invoke-static {p1, v1}, Lcom/intsig/router/CSRouterManager;->O8(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :catch_0
    move-exception p1

    .line 44
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    .line 46
    .line 47
    :cond_1
    :goto_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o〇8oOO88(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1
    const-string v0, "uris"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const/4 v2, 0x1

    .line 15
    const/4 v3, 0x0

    .line 16
    if-eqz v1, :cond_2

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Landroid/net/Uri;

    .line 23
    .line 24
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    invoke-static {v4, v1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->isPdfUri(Landroid/content/Context;Landroid/net/Uri;)I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/4 v4, -0x1

    .line 31
    if-ne v1, v4, :cond_0

    .line 32
    .line 33
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    const p3, 0x7f130378

    .line 38
    .line 39
    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 43
    .line 44
    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 49
    .line 50
    const v0, 0x7f13096c

    .line 51
    .line 52
    .line 53
    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p3

    .line 57
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 62
    .line 63
    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p3

    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 68
    .line 69
    new-array v1, v2, [Ljava/lang/Object;

    .line 70
    .line 71
    aput-object p2, v1, v3

    .line 72
    .line 73
    const p2, 0x7f130b27

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    invoke-static {p1, p3, p2}, Lcom/intsig/camscanner/app/DialogUtils;->o〇〇0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void

    .line 84
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 85
    .line 86
    .line 87
    move-result p2

    .line 88
    if-nez p2, :cond_3

    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 91
    .line 92
    .line 93
    move-result p2

    .line 94
    if-eqz p2, :cond_4

    .line 95
    .line 96
    :cond_3
    if-nez p3, :cond_4

    .line 97
    .line 98
    iput-boolean v2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O00:Z

    .line 99
    .line 100
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 101
    .line 102
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 103
    .line 104
    .line 105
    move-result p2

    .line 106
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 107
    .line 108
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8(ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 113
    .line 114
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇〇8O0〇8:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;

    .line 115
    .line 116
    const/4 v4, 0x0

    .line 117
    const/4 v5, 0x0

    .line 118
    const/4 v6, 0x1

    .line 119
    move-object v1, p1

    .line 120
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportOfficeByUri(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$ImportStatusListener;ZZZ)V

    .line 121
    .line 122
    .line 123
    goto :goto_1

    .line 124
    :cond_4
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 125
    .line 126
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 127
    .line 128
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O〇:Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;

    .line 129
    .line 130
    invoke-static {p2, p1, p3, v0, v3}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->checkTypeAndImportByUri(Landroid/content/Context;Ljava/util/List;Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$ImportStatusListener;Z)V

    .line 131
    .line 132
    .line 133
    :goto_1
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final o〇O(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "entity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o8()Lcom/intsig/camscanner/ppt/PPTImportHelper;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/ppt/PPTImportHelper;->〇〇〇0〇〇0(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇80〇808〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00〇8()Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O8o〇0(Landroid/content/Intent;ZZ)V
    .locals 9

    .line 1
    const-string v1, "ToolFunctionControl"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string v0, "data is null"

    .line 6
    .line 7
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v3, "goDocImport onlyImgPdf:"

    .line 17
    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v3, ", isImgPdf:"

    .line 25
    .line 26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    const-string v1, "EXTRA_LOTTERY_VALUE"

    .line 40
    .line 41
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const/4 v2, 0x1

    .line 46
    const/4 v4, 0x0

    .line 47
    if-eqz v1, :cond_2

    .line 48
    .line 49
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 50
    .line 51
    .line 52
    move-result v6

    .line 53
    if-nez v6, :cond_1

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const/4 v6, 0x0

    .line 57
    goto :goto_1

    .line 58
    :cond_2
    :goto_0
    const/4 v6, 0x1

    .line 59
    :goto_1
    if-nez v6, :cond_3

    .line 60
    .line 61
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o〇:Ljava/lang/String;

    .line 62
    .line 63
    :cond_3
    invoke-static {p1}, Lcom/intsig/camscanner/pdfengine/core/PdfImportHelper;->getUrisFromIntent(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v6, "uris"

    .line 68
    .line 69
    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 73
    .line 74
    .line 75
    move-result v6

    .line 76
    xor-int/2addr v6, v2

    .line 77
    const/16 v7, 0xd1

    .line 78
    .line 79
    const/4 v8, 0x0

    .line 80
    if-eqz v6, :cond_5

    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-eq v0, v7, :cond_4

    .line 89
    .line 90
    const-string v2, "uris[0]"

    .line 91
    .line 92
    packed-switch v0, :pswitch_data_0

    .line 93
    .line 94
    .line 95
    invoke-virtual {p0, v1, v8, p2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8oOO88(Ljava/util/List;Ljava/lang/String;Z)V

    .line 96
    .line 97
    .line 98
    goto/16 :goto_4

    .line 99
    .line 100
    :pswitch_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    move-object v1, v0

    .line 108
    check-cast v1, Landroid/net/Uri;

    .line 109
    .line 110
    const-string v2, "PPT"

    .line 111
    .line 112
    const/4 v4, 0x0

    .line 113
    const/16 v5, 0x8

    .line 114
    .line 115
    const/4 v6, 0x0

    .line 116
    move-object v0, p0

    .line 117
    move v3, p3

    .line 118
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 119
    .line 120
    .line 121
    goto/16 :goto_4

    .line 122
    .line 123
    :pswitch_1
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    move-object v1, v0

    .line 131
    check-cast v1, Landroid/net/Uri;

    .line 132
    .line 133
    const-string v2, "EXCEL"

    .line 134
    .line 135
    const/4 v4, 0x0

    .line 136
    const/16 v5, 0x8

    .line 137
    .line 138
    const/4 v6, 0x0

    .line 139
    move-object v0, p0

    .line 140
    move v3, p3

    .line 141
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 142
    .line 143
    .line 144
    goto/16 :goto_4

    .line 145
    .line 146
    :pswitch_2
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    move-object v1, v0

    .line 154
    check-cast v1, Landroid/net/Uri;

    .line 155
    .line 156
    const-string v2, "WORD"

    .line 157
    .line 158
    const/4 v4, 0x0

    .line 159
    const/16 v5, 0x8

    .line 160
    .line 161
    const/4 v6, 0x0

    .line 162
    move-object v0, p0

    .line 163
    move v3, p3

    .line 164
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/net/Uri;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 165
    .line 166
    .line 167
    goto/16 :goto_4

    .line 168
    .line 169
    :cond_4
    invoke-virtual {p0, v1, v8, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇8oOO88(Ljava/util/List;Ljava/lang/String;Z)V

    .line 170
    .line 171
    .line 172
    goto/16 :goto_4

    .line 173
    .line 174
    :cond_5
    const-string v1, "intent_result_path_list"

    .line 175
    .line 176
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    if-eqz v0, :cond_a

    .line 181
    .line 182
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    if-lez v1, :cond_a

    .line 187
    .line 188
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 189
    .line 190
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 191
    .line 192
    .line 193
    move-result v1

    .line 194
    if-eq v1, v7, :cond_8

    .line 195
    .line 196
    const-string v2, "dataList[0].path"

    .line 197
    .line 198
    packed-switch v1, :pswitch_data_1

    .line 199
    .line 200
    .line 201
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 202
    .line 203
    .line 204
    move-result v1

    .line 205
    if-nez v1, :cond_6

    .line 206
    .line 207
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 208
    .line 209
    .line 210
    move-result v1

    .line 211
    if-nez v1, :cond_6

    .line 212
    .line 213
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 214
    .line 215
    .line 216
    move-result-object v1

    .line 217
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 218
    .line 219
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getMime()I

    .line 220
    .line 221
    .line 222
    move-result v1

    .line 223
    const/4 v2, 0x2

    .line 224
    if-ne v1, v2, :cond_6

    .line 225
    .line 226
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    const-string v1, "dataList[0]"

    .line 231
    .line 232
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    check-cast v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 236
    .line 237
    const-string v1, "local"

    .line 238
    .line 239
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->o〇O(Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;Ljava/lang/String;)V

    .line 240
    .line 241
    .line 242
    return-void

    .line 243
    :cond_6
    new-instance v2, Ljava/util/ArrayList;

    .line 244
    .line 245
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .line 247
    .line 248
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 253
    .line 254
    .line 255
    move-result v1

    .line 256
    if-eqz v1, :cond_7

    .line 257
    .line 258
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 259
    .line 260
    .line 261
    move-result-object v1

    .line 262
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 263
    .line 264
    new-instance v3, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 265
    .line 266
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object v1

    .line 270
    invoke-direct {v3, v1, v8}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    .line 275
    .line 276
    goto :goto_2

    .line 277
    :cond_7
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 278
    .line 279
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 280
    .line 281
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 282
    .line 283
    .line 284
    move-result v3

    .line 285
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 286
    .line 287
    const/4 v6, 0x0

    .line 288
    const/16 v7, 0x20

    .line 289
    .line 290
    const/4 v8, 0x0

    .line 291
    move-object v0, p0

    .line 292
    move v5, p2

    .line 293
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O888o0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Context;Ljava/util/ArrayList;ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;ZZILjava/lang/Object;)V

    .line 294
    .line 295
    .line 296
    goto/16 :goto_4

    .line 297
    .line 298
    :pswitch_3
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    check-cast v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 303
    .line 304
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 305
    .line 306
    .line 307
    move-result-object v1

    .line 308
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    .line 310
    .line 311
    const-string v2, "PPT"

    .line 312
    .line 313
    const/4 v4, 0x0

    .line 314
    const/16 v5, 0x8

    .line 315
    .line 316
    const/4 v6, 0x0

    .line 317
    move-object v0, p0

    .line 318
    move v3, p3

    .line 319
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O〇O〇oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 320
    .line 321
    .line 322
    goto :goto_4

    .line 323
    :pswitch_4
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 324
    .line 325
    .line 326
    move-result-object v0

    .line 327
    check-cast v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 328
    .line 329
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 330
    .line 331
    .line 332
    move-result-object v1

    .line 333
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 334
    .line 335
    .line 336
    const-string v2, "EXCEL"

    .line 337
    .line 338
    const/4 v4, 0x0

    .line 339
    const/16 v5, 0x8

    .line 340
    .line 341
    const/4 v6, 0x0

    .line 342
    move-object v0, p0

    .line 343
    move v3, p3

    .line 344
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O〇O〇oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 345
    .line 346
    .line 347
    goto :goto_4

    .line 348
    :pswitch_5
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 349
    .line 350
    .line 351
    move-result-object v0

    .line 352
    check-cast v0, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 353
    .line 354
    invoke-virtual {v0}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 355
    .line 356
    .line 357
    move-result-object v1

    .line 358
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    .line 360
    .line 361
    const-string v2, "WORD"

    .line 362
    .line 363
    const/4 v4, 0x0

    .line 364
    const/16 v5, 0x8

    .line 365
    .line 366
    const/4 v6, 0x0

    .line 367
    move-object v0, p0

    .line 368
    move v3, p3

    .line 369
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->O〇O〇oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Ljava/lang/String;Ljava/lang/String;ZLcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;ILjava/lang/Object;)V

    .line 370
    .line 371
    .line 372
    goto :goto_4

    .line 373
    :cond_8
    new-instance v2, Ljava/util/ArrayList;

    .line 374
    .line 375
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 376
    .line 377
    .line 378
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 379
    .line 380
    .line 381
    move-result-object v0

    .line 382
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 383
    .line 384
    .line 385
    move-result v1

    .line 386
    if-eqz v1, :cond_9

    .line 387
    .line 388
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 389
    .line 390
    .line 391
    move-result-object v1

    .line 392
    check-cast v1, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;

    .line 393
    .line 394
    new-instance v3, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;

    .line 395
    .line 396
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/pdf/PdfGalleryFileEntity;->getPath()Ljava/lang/String;

    .line 397
    .line 398
    .line 399
    move-result-object v1

    .line 400
    invoke-direct {v3, v1, v8}, Lcom/intsig/camscanner/pdfengine/entity/PdfPathImportEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    .line 402
    .line 403
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    .line 405
    .line 406
    goto :goto_3

    .line 407
    :cond_9
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 408
    .line 409
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 410
    .line 411
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 412
    .line 413
    .line 414
    move-result v3

    .line 415
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 416
    .line 417
    const/4 v5, 0x1

    .line 418
    const/4 v6, 0x0

    .line 419
    const/16 v7, 0x20

    .line 420
    .line 421
    const/4 v8, 0x0

    .line 422
    move-object v0, p0

    .line 423
    invoke-static/range {v0 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O888o0o(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Context;Ljava/util/ArrayList;ILcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;ZZILjava/lang/Object;)V

    .line 424
    .line 425
    .line 426
    :cond_a
    :goto_4
    return-void

    .line 427
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    :pswitch_data_1
    .packed-switch 0x65
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final 〇0O〇Oo(Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;)V
    .locals 4

    .line 1
    const-string v0, "ToolFunctionControl"

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const-string v1, "PDF_PAGE_ADJUST, go to view doc"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v0, Landroid/content/Intent;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    const-class v2, Lcom/intsig/camscanner/DocumentActivity;

    .line 19
    .line 20
    const-string v3, "android.intent.action.VIEW"

    .line 21
    .line 22
    invoke-direct {v0, v3, p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 23
    .line 24
    .line 25
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/EditType;->MOVE:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const-string v1, "constant_doc_edit_type"

    .line 32
    .line 33
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const-string p1, "data == null"

    .line 43
    .line 44
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇80(I)Z
    .locals 1

    .line 1
    const/16 v0, 0xca

    .line 2
    .line 3
    if-eq p1, v0, :cond_0

    .line 4
    .line 5
    const/16 v0, 0xcb

    .line 6
    .line 7
    if-eq p1, v0, :cond_0

    .line 8
    .line 9
    packed-switch p1, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    packed-switch p1, :pswitch_data_1

    .line 13
    .line 14
    .line 15
    const/4 p1, 0x0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    :pswitch_0
    const/4 p1, 0x1

    .line 18
    :goto_0
    return p1

    .line 19
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 20
    :pswitch_data_1
    .packed-switch 0xcd
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final 〇O〇80o08O(Ljava/util/List;)V
    .locals 20
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;",
            ">;)V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "docMsgList"

    .line 4
    .line 5
    move-object/from16 v2, p1

    .line 6
    .line 7
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const-string v1, "ToolFunctionControl"

    .line 11
    .line 12
    const-string v3, "mergeDocs"

    .line 13
    .line 14
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v7, 0x0

    .line 18
    new-instance v10, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    sget-object v5, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 30
    .line 31
    const-string v1, "title"

    .line 32
    .line 33
    const-string v3, "type"

    .line 34
    .line 35
    const-string v6, "_id"

    .line 36
    .line 37
    const-string v8, "_data"

    .line 38
    .line 39
    filled-new-array {v6, v8, v1, v3}, [Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v6

    .line 43
    const/4 v8, 0x0

    .line 44
    const/4 v9, 0x0

    .line 45
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 50
    .line 51
    .line 52
    move-result-object v3

    .line 53
    const/4 v4, 0x0

    .line 54
    const/4 v13, 0x0

    .line 55
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 56
    .line 57
    .line 58
    move-result v5

    .line 59
    const/4 v6, 0x1

    .line 60
    if-eqz v5, :cond_1

    .line 61
    .line 62
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    check-cast v5, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 67
    .line 68
    invoke-virtual {v5}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->isCsDoc()Z

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    if-eqz v5, :cond_0

    .line 73
    .line 74
    const/4 v13, 0x1

    .line 75
    goto :goto_0

    .line 76
    :cond_1
    if-eqz v1, :cond_5

    .line 77
    .line 78
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 79
    .line 80
    .line 81
    move-result v3

    .line 82
    if-eqz v3, :cond_4

    .line 83
    .line 84
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    .line 85
    .line 86
    .line 87
    move-result-wide v7

    .line 88
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    if-eqz v5, :cond_2

    .line 97
    .line 98
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 99
    .line 100
    .line 101
    move-result-object v5

    .line 102
    check-cast v5, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;

    .line 103
    .line 104
    invoke-virtual {v5}, Lcom/intsig/camscanner/pdfengine/core/LocalPdfImportProcessor$FinalDocMsg;->getUri()Landroid/net/Uri;

    .line 105
    .line 106
    .line 107
    move-result-object v5

    .line 108
    invoke-static {v5}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 109
    .line 110
    .line 111
    move-result-wide v11

    .line 112
    cmp-long v5, v11, v7

    .line 113
    .line 114
    if-nez v5, :cond_3

    .line 115
    .line 116
    new-instance v5, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 117
    .line 118
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v17

    .line 122
    const/4 v9, 0x2

    .line 123
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v18

    .line 127
    const/4 v9, 0x3

    .line 128
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    .line 129
    .line 130
    .line 131
    move-result v19

    .line 132
    move-object v14, v5

    .line 133
    move-wide v15, v7

    .line 134
    invoke-direct/range {v14 .. v19}, Lcom/intsig/camscanner/datastruct/DocumentListItem;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    .line 139
    .line 140
    goto :goto_1

    .line 141
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 142
    .line 143
    .line 144
    :cond_5
    new-instance v1, Lcom/intsig/camscanner/merge/MergeDocumentsTask;

    .line 145
    .line 146
    iget-object v9, v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 147
    .line 148
    sget-object v11, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇o00〇〇Oo:Ljava/lang/String;

    .line 149
    .line 150
    new-array v2, v6, [Ljava/lang/Object;

    .line 151
    .line 152
    invoke-static {}, Lcom/intsig/camscanner/util/DocDirNameFormat;->Oo08()Ljava/text/SimpleDateFormat;

    .line 153
    .line 154
    .line 155
    move-result-object v3

    .line 156
    new-instance v5, Ljava/util/Date;

    .line 157
    .line 158
    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v3

    .line 165
    aput-object v3, v2, v4

    .line 166
    .line 167
    const v3, 0x7f1307fb

    .line 168
    .line 169
    .line 170
    invoke-virtual {v9, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v12

    .line 174
    const-wide/16 v14, -0x2

    .line 175
    .line 176
    new-instance v2, LO88O/O8;

    .line 177
    .line 178
    invoke-direct {v2, v0}, LO88O/O8;-><init>(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;)V

    .line 179
    .line 180
    .line 181
    move-object v8, v1

    .line 182
    move-object/from16 v16, v2

    .line 183
    .line 184
    invoke-direct/range {v8 .. v16}, Lcom/intsig/camscanner/merge/MergeDocumentsTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ZJLcom/intsig/camscanner/merge/MergeDocumentsTask$PostListener;)V

    .line 185
    .line 186
    .line 187
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 188
    .line 189
    .line 190
    move-result-object v2

    .line 191
    new-array v3, v4, [Ljava/lang/Integer;

    .line 192
    .line 193
    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 194
    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇o()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o0O0O8(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OO0o〇〇〇〇0:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇o8(Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0〇O0088o:Lcom/intsig/camscanner/pdfengine/core/PdfImportParentEntity;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇〇0〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/capture/SupportCaptureModeOption;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mode"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "functionEntrance"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "supportCaptureModeOption"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    const-wide/16 v0, -0x2

    .line 32
    .line 33
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

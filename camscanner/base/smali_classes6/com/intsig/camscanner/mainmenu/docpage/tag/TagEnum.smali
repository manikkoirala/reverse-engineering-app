.class public final enum Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;
.super Ljava/lang/Enum;
.source "TagEnum.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Accountancy8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Architecture1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Architecture2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Business9:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Cards7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final Companion:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final enum Daily1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Daily2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Daily3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Daily4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Daily5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Education6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Government1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Government2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Government3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Interest1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Interest2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Interest3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Interest4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Medical1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Medical2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Medical3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Medical4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Medical5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

.field public static final enum Personals6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;


# instance fields
.field private final category:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final code:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final tagName:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;
    .locals 3

    .line 1
    const/16 v0, 0x37

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Architecture1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Architecture2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    const/16 v1, 0x18

    .line 142
    .line 143
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 144
    .line 145
    aput-object v2, v0, v1

    .line 146
    .line 147
    const/16 v1, 0x19

    .line 148
    .line 149
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 150
    .line 151
    aput-object v2, v0, v1

    .line 152
    .line 153
    const/16 v1, 0x1a

    .line 154
    .line 155
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 156
    .line 157
    aput-object v2, v0, v1

    .line 158
    .line 159
    const/16 v1, 0x1b

    .line 160
    .line 161
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 162
    .line 163
    aput-object v2, v0, v1

    .line 164
    .line 165
    const/16 v1, 0x1c

    .line 166
    .line 167
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 168
    .line 169
    aput-object v2, v0, v1

    .line 170
    .line 171
    const/16 v1, 0x1d

    .line 172
    .line 173
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 174
    .line 175
    aput-object v2, v0, v1

    .line 176
    .line 177
    const/16 v1, 0x1e

    .line 178
    .line 179
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 180
    .line 181
    aput-object v2, v0, v1

    .line 182
    .line 183
    const/16 v1, 0x1f

    .line 184
    .line 185
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 186
    .line 187
    aput-object v2, v0, v1

    .line 188
    .line 189
    const/16 v1, 0x20

    .line 190
    .line 191
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 192
    .line 193
    aput-object v2, v0, v1

    .line 194
    .line 195
    const/16 v1, 0x21

    .line 196
    .line 197
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 198
    .line 199
    aput-object v2, v0, v1

    .line 200
    .line 201
    const/16 v1, 0x22

    .line 202
    .line 203
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 204
    .line 205
    aput-object v2, v0, v1

    .line 206
    .line 207
    const/16 v1, 0x23

    .line 208
    .line 209
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 210
    .line 211
    aput-object v2, v0, v1

    .line 212
    .line 213
    const/16 v1, 0x24

    .line 214
    .line 215
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 216
    .line 217
    aput-object v2, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x25

    .line 220
    .line 221
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 222
    .line 223
    aput-object v2, v0, v1

    .line 224
    .line 225
    const/16 v1, 0x26

    .line 226
    .line 227
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 228
    .line 229
    aput-object v2, v0, v1

    .line 230
    .line 231
    const/16 v1, 0x27

    .line 232
    .line 233
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 234
    .line 235
    aput-object v2, v0, v1

    .line 236
    .line 237
    const/16 v1, 0x28

    .line 238
    .line 239
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 240
    .line 241
    aput-object v2, v0, v1

    .line 242
    .line 243
    const/16 v1, 0x29

    .line 244
    .line 245
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 246
    .line 247
    aput-object v2, v0, v1

    .line 248
    .line 249
    const/16 v1, 0x2a

    .line 250
    .line 251
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 252
    .line 253
    aput-object v2, v0, v1

    .line 254
    .line 255
    const/16 v1, 0x2b

    .line 256
    .line 257
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 258
    .line 259
    aput-object v2, v0, v1

    .line 260
    .line 261
    const/16 v1, 0x2c

    .line 262
    .line 263
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 264
    .line 265
    aput-object v2, v0, v1

    .line 266
    .line 267
    const/16 v1, 0x2d

    .line 268
    .line 269
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business9:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 270
    .line 271
    aput-object v2, v0, v1

    .line 272
    .line 273
    const/16 v1, 0x2e

    .line 274
    .line 275
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 276
    .line 277
    aput-object v2, v0, v1

    .line 278
    .line 279
    const/16 v1, 0x2f

    .line 280
    .line 281
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 282
    .line 283
    aput-object v2, v0, v1

    .line 284
    .line 285
    const/16 v1, 0x30

    .line 286
    .line 287
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 288
    .line 289
    aput-object v2, v0, v1

    .line 290
    .line 291
    const/16 v1, 0x31

    .line 292
    .line 293
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 294
    .line 295
    aput-object v2, v0, v1

    .line 296
    .line 297
    const/16 v1, 0x32

    .line 298
    .line 299
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 300
    .line 301
    aput-object v2, v0, v1

    .line 302
    .line 303
    const/16 v1, 0x33

    .line 304
    .line 305
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 306
    .line 307
    aput-object v2, v0, v1

    .line 308
    .line 309
    const/16 v1, 0x34

    .line 310
    .line 311
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 312
    .line 313
    aput-object v2, v0, v1

    .line 314
    .line 315
    const/16 v1, 0x35

    .line 316
    .line 317
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 318
    .line 319
    aput-object v2, v0, v1

    .line 320
    .line 321
    const/16 v1, 0x36

    .line 322
    .line 323
    sget-object v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 324
    .line 325
    aput-object v2, v0, v1

    .line 326
    .line 327
    return-object v0
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static constructor <clinit>()V
    .locals 13

    .line 1
    new-instance v6, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 2
    .line 3
    const-string v1, "Interest1"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "INT101"

    .line 7
    .line 8
    const-string v4, "\u6c7d\u8f66\u7167"

    .line 9
    .line 10
    const-string v5, "interest"

    .line 11
    .line 12
    move-object v0, v6

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sput-object v6, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 19
    .line 20
    const-string v8, "Interest2"

    .line 21
    .line 22
    const/4 v9, 0x1

    .line 23
    const-string v10, "INT102"

    .line 24
    .line 25
    const-string v11, "\u7d20\u63cf"

    .line 26
    .line 27
    const-string v12, "interest"

    .line 28
    .line 29
    move-object v7, v0

    .line 30
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 34
    .line 35
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 36
    .line 37
    const-string v2, "Interest3"

    .line 38
    .line 39
    const/4 v3, 0x2

    .line 40
    const-string v4, "INT103"

    .line 41
    .line 42
    const-string v5, "\u7ed8\u753b"

    .line 43
    .line 44
    const-string v6, "interest"

    .line 45
    .line 46
    move-object v1, v0

    .line 47
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 51
    .line 52
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 53
    .line 54
    const-string v8, "Interest4"

    .line 55
    .line 56
    const/4 v9, 0x3

    .line 57
    const-string v10, "INT104"

    .line 58
    .line 59
    const-string v11, "\u4e66\u6cd5"

    .line 60
    .line 61
    const-string v12, "interest"

    .line 62
    .line 63
    move-object v7, v0

    .line 64
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Interest4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 68
    .line 69
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 70
    .line 71
    const-string v2, "Daily1"

    .line 72
    .line 73
    const/4 v3, 0x4

    .line 74
    const-string v4, "DAI101"

    .line 75
    .line 76
    const v1, 0x7f1319ff

    .line 77
    .line 78
    .line 79
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v5

    .line 83
    const-string v6, "daily"

    .line 84
    .line 85
    move-object v1, v0

    .line 86
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 92
    .line 93
    const-string v8, "Daily2"

    .line 94
    .line 95
    const/4 v9, 0x5

    .line 96
    const-string v10, "DAI102"

    .line 97
    .line 98
    const v1, 0x7f131a01

    .line 99
    .line 100
    .line 101
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v11

    .line 105
    const-string v12, "daily"

    .line 106
    .line 107
    move-object v7, v0

    .line 108
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 112
    .line 113
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 114
    .line 115
    const-string v2, "Daily3"

    .line 116
    .line 117
    const/4 v3, 0x6

    .line 118
    const-string v4, "DAI103"

    .line 119
    .line 120
    const v1, 0x7f130f88

    .line 121
    .line 122
    .line 123
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v5

    .line 127
    const-string v6, "daily"

    .line 128
    .line 129
    move-object v1, v0

    .line 130
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 134
    .line 135
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 136
    .line 137
    const-string v8, "Daily4"

    .line 138
    .line 139
    const/4 v9, 0x7

    .line 140
    const-string v10, "DAI104"

    .line 141
    .line 142
    const v1, 0x7f131a05

    .line 143
    .line 144
    .line 145
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v11

    .line 149
    const-string v12, "daily"

    .line 150
    .line 151
    move-object v7, v0

    .line 152
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 156
    .line 157
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 158
    .line 159
    const-string v2, "Daily5"

    .line 160
    .line 161
    const/16 v3, 0x8

    .line 162
    .line 163
    const-string v4, "DAI105"

    .line 164
    .line 165
    const v1, 0x7f131a07

    .line 166
    .line 167
    .line 168
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v5

    .line 172
    const-string v6, "daily"

    .line 173
    .line 174
    move-object v1, v0

    .line 175
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Daily5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 179
    .line 180
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 181
    .line 182
    const-string v8, "Education1"

    .line 183
    .line 184
    const/16 v9, 0x9

    .line 185
    .line 186
    const-string v10, "EDU101"

    .line 187
    .line 188
    const v1, 0x7f130f87

    .line 189
    .line 190
    .line 191
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v11

    .line 195
    const-string v12, "education"

    .line 196
    .line 197
    move-object v7, v0

    .line 198
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 202
    .line 203
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 204
    .line 205
    const-string v2, "Education2"

    .line 206
    .line 207
    const/16 v3, 0xa

    .line 208
    .line 209
    const-string v4, "EDU102"

    .line 210
    .line 211
    const v1, 0x7f13126c

    .line 212
    .line 213
    .line 214
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v5

    .line 218
    const-string v6, "education"

    .line 219
    .line 220
    move-object v1, v0

    .line 221
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 225
    .line 226
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 227
    .line 228
    const-string v8, "Education3"

    .line 229
    .line 230
    const/16 v9, 0xb

    .line 231
    .line 232
    const-string v10, "EDU103"

    .line 233
    .line 234
    const v1, 0x7f1319f9

    .line 235
    .line 236
    .line 237
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object v11

    .line 241
    const-string v12, "education"

    .line 242
    .line 243
    move-object v7, v0

    .line 244
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 248
    .line 249
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 250
    .line 251
    const-string v2, "Education4"

    .line 252
    .line 253
    const/16 v3, 0xc

    .line 254
    .line 255
    const-string v4, "EDU104"

    .line 256
    .line 257
    const-string v5, "\u5f55\u53d6\u901a\u77e5\u4e66"

    .line 258
    .line 259
    const-string v6, "education"

    .line 260
    .line 261
    move-object v1, v0

    .line 262
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    .line 264
    .line 265
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 266
    .line 267
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 268
    .line 269
    const-string v8, "Education5"

    .line 270
    .line 271
    const/16 v9, 0xd

    .line 272
    .line 273
    const-string v10, "EDU105"

    .line 274
    .line 275
    const-string v11, "\u8bfe\u7a0b\u8868"

    .line 276
    .line 277
    const-string v12, "education"

    .line 278
    .line 279
    move-object v7, v0

    .line 280
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .line 282
    .line 283
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 284
    .line 285
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 286
    .line 287
    const-string v2, "Education6"

    .line 288
    .line 289
    const/16 v3, 0xe

    .line 290
    .line 291
    const-string v4, "EDU106"

    .line 292
    .line 293
    const v1, 0x7f131805

    .line 294
    .line 295
    .line 296
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 297
    .line 298
    .line 299
    move-result-object v5

    .line 300
    const-string v6, "education"

    .line 301
    .line 302
    move-object v1, v0

    .line 303
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    .line 305
    .line 306
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Education6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 307
    .line 308
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 309
    .line 310
    const-string v8, "Architecture1"

    .line 311
    .line 312
    const/16 v9, 0xf

    .line 313
    .line 314
    const-string v10, "ARC101"

    .line 315
    .line 316
    const-string v11, "\u6280\u672f\u56fe\u7eb8"

    .line 317
    .line 318
    const-string v12, "architecture"

    .line 319
    .line 320
    move-object v7, v0

    .line 321
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    .line 323
    .line 324
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Architecture1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 325
    .line 326
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 327
    .line 328
    const-string v2, "Architecture2"

    .line 329
    .line 330
    const/16 v3, 0x10

    .line 331
    .line 332
    const-string v4, "ARC102"

    .line 333
    .line 334
    const-string v5, "\u6237\u578b\u56fe"

    .line 335
    .line 336
    const-string v6, "architecture"

    .line 337
    .line 338
    move-object v1, v0

    .line 339
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    .line 341
    .line 342
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Architecture2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 343
    .line 344
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 345
    .line 346
    const-string v8, "Cards1"

    .line 347
    .line 348
    const/16 v9, 0x11

    .line 349
    .line 350
    const-string v10, "CAR101"

    .line 351
    .line 352
    const v1, 0x7f131a00

    .line 353
    .line 354
    .line 355
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 356
    .line 357
    .line 358
    move-result-object v11

    .line 359
    const-string v12, "cards"

    .line 360
    .line 361
    move-object v7, v0

    .line 362
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    .line 364
    .line 365
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 366
    .line 367
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 368
    .line 369
    const-string v2, "Cards2"

    .line 370
    .line 371
    const/16 v3, 0x12

    .line 372
    .line 373
    const-string v4, "CAR102"

    .line 374
    .line 375
    const v1, 0x7f131a04

    .line 376
    .line 377
    .line 378
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v5

    .line 382
    const-string v6, "cards"

    .line 383
    .line 384
    move-object v1, v0

    .line 385
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    .line 387
    .line 388
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 389
    .line 390
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 391
    .line 392
    const-string v8, "Cards3"

    .line 393
    .line 394
    const/16 v9, 0x13

    .line 395
    .line 396
    const-string v10, "CAR103"

    .line 397
    .line 398
    const v1, 0x7f131a03

    .line 399
    .line 400
    .line 401
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 402
    .line 403
    .line 404
    move-result-object v11

    .line 405
    const-string v12, "cards"

    .line 406
    .line 407
    move-object v7, v0

    .line 408
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    .line 410
    .line 411
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 412
    .line 413
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 414
    .line 415
    const-string v2, "Cards4"

    .line 416
    .line 417
    const/16 v3, 0x14

    .line 418
    .line 419
    const-string v4, "CAR104"

    .line 420
    .line 421
    const v1, 0x7f13126e

    .line 422
    .line 423
    .line 424
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 425
    .line 426
    .line 427
    move-result-object v5

    .line 428
    const-string v6, "cards"

    .line 429
    .line 430
    move-object v1, v0

    .line 431
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    .line 433
    .line 434
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 435
    .line 436
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 437
    .line 438
    const-string v8, "Cards5"

    .line 439
    .line 440
    const/16 v9, 0x15

    .line 441
    .line 442
    const-string v10, "CAR105"

    .line 443
    .line 444
    const v1, 0x7f130f86

    .line 445
    .line 446
    .line 447
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 448
    .line 449
    .line 450
    move-result-object v11

    .line 451
    const-string v12, "cards"

    .line 452
    .line 453
    move-object v7, v0

    .line 454
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 458
    .line 459
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 460
    .line 461
    const-string v2, "Cards6"

    .line 462
    .line 463
    const/16 v3, 0x16

    .line 464
    .line 465
    const-string v4, "CAR106"

    .line 466
    .line 467
    const v1, 0x7f130f89

    .line 468
    .line 469
    .line 470
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 471
    .line 472
    .line 473
    move-result-object v5

    .line 474
    const-string v6, "cards"

    .line 475
    .line 476
    move-object v1, v0

    .line 477
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    .line 479
    .line 480
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 481
    .line 482
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 483
    .line 484
    const-string v8, "Cards7"

    .line 485
    .line 486
    const/16 v9, 0x17

    .line 487
    .line 488
    const-string v10, "CAR107"

    .line 489
    .line 490
    const v1, 0x7f13126f

    .line 491
    .line 492
    .line 493
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 494
    .line 495
    .line 496
    move-result-object v11

    .line 497
    const-string v12, "cards"

    .line 498
    .line 499
    move-object v7, v0

    .line 500
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    .line 502
    .line 503
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Cards7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 504
    .line 505
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 506
    .line 507
    const-string v2, "Accountancy1"

    .line 508
    .line 509
    const/16 v3, 0x18

    .line 510
    .line 511
    const-string v4, "ACC101"

    .line 512
    .line 513
    const-string v5, "\u53d1\u7968"

    .line 514
    .line 515
    const-string v6, "accountancy"

    .line 516
    .line 517
    move-object v1, v0

    .line 518
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    .line 520
    .line 521
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 522
    .line 523
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 524
    .line 525
    const-string v8, "Accountancy2"

    .line 526
    .line 527
    const/16 v9, 0x19

    .line 528
    .line 529
    const-string v10, "ACC102"

    .line 530
    .line 531
    const v1, 0x7f131a02

    .line 532
    .line 533
    .line 534
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 535
    .line 536
    .line 537
    move-result-object v11

    .line 538
    const-string v12, "accountancy"

    .line 539
    .line 540
    move-object v7, v0

    .line 541
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    .line 543
    .line 544
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 545
    .line 546
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 547
    .line 548
    const-string v2, "Accountancy3"

    .line 549
    .line 550
    const/16 v3, 0x1a

    .line 551
    .line 552
    const-string v4, "ACC103"

    .line 553
    .line 554
    const-string v5, "\u91c7\u8d2d\u5355"

    .line 555
    .line 556
    const-string v6, "accountancy"

    .line 557
    .line 558
    move-object v1, v0

    .line 559
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    .line 561
    .line 562
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 563
    .line 564
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 565
    .line 566
    const-string v8, "Accountancy4"

    .line 567
    .line 568
    const/16 v9, 0x1b

    .line 569
    .line 570
    const-string v10, "ACC104"

    .line 571
    .line 572
    const-string v11, "\u4fdd\u9669\u5355"

    .line 573
    .line 574
    const-string v12, "accountancy"

    .line 575
    .line 576
    move-object v7, v0

    .line 577
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    .line 579
    .line 580
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 581
    .line 582
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 583
    .line 584
    const-string v2, "Accountancy5"

    .line 585
    .line 586
    const/16 v3, 0x1c

    .line 587
    .line 588
    const-string v4, "ACC105"

    .line 589
    .line 590
    const-string v5, "\u94f6\u884c\u6d41\u6c34"

    .line 591
    .line 592
    const-string v6, "accountancy"

    .line 593
    .line 594
    move-object v1, v0

    .line 595
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    .line 597
    .line 598
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 599
    .line 600
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 601
    .line 602
    const-string v8, "Accountancy6"

    .line 603
    .line 604
    const/16 v9, 0x1d

    .line 605
    .line 606
    const-string v10, "ACC106"

    .line 607
    .line 608
    const-string v11, "\u62a5\u4ef7\u5355"

    .line 609
    .line 610
    const-string v12, "accountancy"

    .line 611
    .line 612
    move-object v7, v0

    .line 613
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    .line 615
    .line 616
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 617
    .line 618
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 619
    .line 620
    const-string v2, "Accountancy7"

    .line 621
    .line 622
    const/16 v3, 0x1e

    .line 623
    .line 624
    const-string v4, "ACC107"

    .line 625
    .line 626
    const-string v5, "\u63d0\u8d27\u5355"

    .line 627
    .line 628
    const-string v6, "accountancy"

    .line 629
    .line 630
    move-object v1, v0

    .line 631
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    .line 633
    .line 634
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 635
    .line 636
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 637
    .line 638
    const-string v8, "Accountancy8"

    .line 639
    .line 640
    const/16 v9, 0x1f

    .line 641
    .line 642
    const-string v10, "ACC108"

    .line 643
    .line 644
    const-string v11, "\u5de5\u8d44\u53d1\u653e\u660e\u7ec6"

    .line 645
    .line 646
    const-string v12, "accountancy"

    .line 647
    .line 648
    move-object v7, v0

    .line 649
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    .line 651
    .line 652
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Accountancy8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 653
    .line 654
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 655
    .line 656
    const-string v2, "Medical1"

    .line 657
    .line 658
    const/16 v3, 0x20

    .line 659
    .line 660
    const-string v4, "MED101"

    .line 661
    .line 662
    const v1, 0x7f130f8a

    .line 663
    .line 664
    .line 665
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 666
    .line 667
    .line 668
    move-result-object v5

    .line 669
    const-string v6, "medical"

    .line 670
    .line 671
    move-object v1, v0

    .line 672
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    .line 674
    .line 675
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 676
    .line 677
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 678
    .line 679
    const-string v8, "Medical2"

    .line 680
    .line 681
    const/16 v9, 0x21

    .line 682
    .line 683
    const-string v10, "MED102"

    .line 684
    .line 685
    const-string v11, "\u75c5\u4f8b"

    .line 686
    .line 687
    const-string v12, "medical"

    .line 688
    .line 689
    move-object v7, v0

    .line 690
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    .line 692
    .line 693
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 694
    .line 695
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 696
    .line 697
    const-string v2, "Medical3"

    .line 698
    .line 699
    const/16 v3, 0x22

    .line 700
    .line 701
    const-string v4, "MED103"

    .line 702
    .line 703
    const-string v5, "\u836f\u54c1\u8bf4\u660e\u4e66"

    .line 704
    .line 705
    const-string v6, "medical"

    .line 706
    .line 707
    move-object v1, v0

    .line 708
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    .line 710
    .line 711
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 712
    .line 713
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 714
    .line 715
    const-string v8, "Medical4"

    .line 716
    .line 717
    const/16 v9, 0x23

    .line 718
    .line 719
    const-string v10, "MED104"

    .line 720
    .line 721
    const-string v11, "\u4f53\u68c0\u5355"

    .line 722
    .line 723
    const-string v12, "medical"

    .line 724
    .line 725
    move-object v7, v0

    .line 726
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    .line 728
    .line 729
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 730
    .line 731
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 732
    .line 733
    const-string v2, "Medical5"

    .line 734
    .line 735
    const/16 v3, 0x24

    .line 736
    .line 737
    const-string v4, "MED105"

    .line 738
    .line 739
    const-string v5, "\u68c0\u9a8c\u5355"

    .line 740
    .line 741
    const-string v6, "medical"

    .line 742
    .line 743
    move-object v1, v0

    .line 744
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    .line 746
    .line 747
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Medical5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 748
    .line 749
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 750
    .line 751
    const-string v8, "Business1"

    .line 752
    .line 753
    const/16 v9, 0x25

    .line 754
    .line 755
    const-string v10, "BUS101"

    .line 756
    .line 757
    const v1, 0x7f131a06

    .line 758
    .line 759
    .line 760
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 761
    .line 762
    .line 763
    move-result-object v11

    .line 764
    const-string v12, "business"

    .line 765
    .line 766
    move-object v7, v0

    .line 767
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    .line 769
    .line 770
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 771
    .line 772
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 773
    .line 774
    const-string v2, "Business2"

    .line 775
    .line 776
    const/16 v3, 0x26

    .line 777
    .line 778
    const-string v4, "BUS102"

    .line 779
    .line 780
    const-string v5, "PPT"

    .line 781
    .line 782
    const-string v6, "business"

    .line 783
    .line 784
    move-object v1, v0

    .line 785
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    .line 787
    .line 788
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 789
    .line 790
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 791
    .line 792
    const-string v8, "Business3"

    .line 793
    .line 794
    const/16 v9, 0x27

    .line 795
    .line 796
    const-string v10, "BUS103"

    .line 797
    .line 798
    const-string v11, "\u540d\u7247"

    .line 799
    .line 800
    const-string v12, "business"

    .line 801
    .line 802
    move-object v7, v0

    .line 803
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    .line 805
    .line 806
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 807
    .line 808
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 809
    .line 810
    const-string v2, "Business4"

    .line 811
    .line 812
    const/16 v3, 0x28

    .line 813
    .line 814
    const-string v4, "BUS104"

    .line 815
    .line 816
    const v1, 0x7f131270

    .line 817
    .line 818
    .line 819
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 820
    .line 821
    .line 822
    move-result-object v5

    .line 823
    const-string v6, "business"

    .line 824
    .line 825
    move-object v1, v0

    .line 826
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 827
    .line 828
    .line 829
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 830
    .line 831
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 832
    .line 833
    const-string v8, "Business5"

    .line 834
    .line 835
    const/16 v9, 0x29

    .line 836
    .line 837
    const-string v10, "BUS105"

    .line 838
    .line 839
    const-string v11, "\u7b7e\u5230\u8868"

    .line 840
    .line 841
    const-string v12, "business"

    .line 842
    .line 843
    move-object v7, v0

    .line 844
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    .line 846
    .line 847
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 848
    .line 849
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 850
    .line 851
    const-string v2, "Business6"

    .line 852
    .line 853
    const/16 v3, 0x2a

    .line 854
    .line 855
    const-string v4, "BUS106"

    .line 856
    .line 857
    const-string v5, "\u8003\u52e4\u660e\u7ec6"

    .line 858
    .line 859
    const-string v6, "business"

    .line 860
    .line 861
    move-object v1, v0

    .line 862
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    .line 864
    .line 865
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 866
    .line 867
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 868
    .line 869
    const-string v8, "Business7"

    .line 870
    .line 871
    const/16 v9, 0x2b

    .line 872
    .line 873
    const-string v10, "BUS107"

    .line 874
    .line 875
    const-string v11, "\u7533\u8bf7\u4e66"

    .line 876
    .line 877
    const-string v12, "business"

    .line 878
    .line 879
    move-object v7, v0

    .line 880
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    .line 882
    .line 883
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business7:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 884
    .line 885
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 886
    .line 887
    const-string v2, "Business8"

    .line 888
    .line 889
    const/16 v3, 0x2c

    .line 890
    .line 891
    const-string v4, "BUS108"

    .line 892
    .line 893
    const-string v5, "\u627f\u8bfa\u4e66"

    .line 894
    .line 895
    const-string v6, "business"

    .line 896
    .line 897
    move-object v1, v0

    .line 898
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    .line 900
    .line 901
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business8:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 902
    .line 903
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 904
    .line 905
    const-string v8, "Business9"

    .line 906
    .line 907
    const/16 v9, 0x2d

    .line 908
    .line 909
    const-string v10, "BUS109"

    .line 910
    .line 911
    const-string v11, "\u516c\u544a\u58f0\u660e"

    .line 912
    .line 913
    const-string v12, "business"

    .line 914
    .line 915
    move-object v7, v0

    .line 916
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    .line 918
    .line 919
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Business9:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 920
    .line 921
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 922
    .line 923
    const-string v2, "Personals1"

    .line 924
    .line 925
    const/16 v3, 0x2e

    .line 926
    .line 927
    const-string v4, "PER101"

    .line 928
    .line 929
    const v1, 0x7f130f85

    .line 930
    .line 931
    .line 932
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 933
    .line 934
    .line 935
    move-result-object v5

    .line 936
    const-string v6, "personals"

    .line 937
    .line 938
    move-object v1, v0

    .line 939
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    .line 941
    .line 942
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 943
    .line 944
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 945
    .line 946
    const-string v8, "Personals2"

    .line 947
    .line 948
    const/16 v9, 0x2f

    .line 949
    .line 950
    const-string v10, "PER102"

    .line 951
    .line 952
    const v1, 0x7f13126d

    .line 953
    .line 954
    .line 955
    invoke-static {v1}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 956
    .line 957
    .line 958
    move-result-object v11

    .line 959
    const-string v12, "personals"

    .line 960
    .line 961
    move-object v7, v0

    .line 962
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    .line 964
    .line 965
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 966
    .line 967
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 968
    .line 969
    const-string v2, "Personals3"

    .line 970
    .line 971
    const/16 v3, 0x30

    .line 972
    .line 973
    const-string v4, "PER103"

    .line 974
    .line 975
    const-string v5, "\u6bd5\u4e1a\u8bc1"

    .line 976
    .line 977
    const-string v6, "personals"

    .line 978
    .line 979
    move-object v1, v0

    .line 980
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    .line 982
    .line 983
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 984
    .line 985
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 986
    .line 987
    const-string v8, "Personals4"

    .line 988
    .line 989
    const/16 v9, 0x31

    .line 990
    .line 991
    const-string v10, "PER104"

    .line 992
    .line 993
    const-string v11, "\u5b66\u4f4d\u8bc1"

    .line 994
    .line 995
    const-string v12, "personals"

    .line 996
    .line 997
    move-object v7, v0

    .line 998
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    .line 1000
    .line 1001
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals4:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1002
    .line 1003
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1004
    .line 1005
    const-string v2, "Personals5"

    .line 1006
    .line 1007
    const/16 v3, 0x32

    .line 1008
    .line 1009
    const-string v4, "PER105"

    .line 1010
    .line 1011
    const-string v5, "\u7b80\u5386"

    .line 1012
    .line 1013
    const-string v6, "personals"

    .line 1014
    .line 1015
    move-object v1, v0

    .line 1016
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    .line 1018
    .line 1019
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals5:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1020
    .line 1021
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1022
    .line 1023
    const-string v8, "Personals6"

    .line 1024
    .line 1025
    const/16 v9, 0x33

    .line 1026
    .line 1027
    const-string v10, "PER106"

    .line 1028
    .line 1029
    const-string v11, "\u5728\u79bb\u804c\u8bc1\u660e"

    .line 1030
    .line 1031
    const-string v12, "personals"

    .line 1032
    .line 1033
    move-object v7, v0

    .line 1034
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    .line 1036
    .line 1037
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Personals6:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1038
    .line 1039
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1040
    .line 1041
    const-string v2, "Government1"

    .line 1042
    .line 1043
    const/16 v3, 0x34

    .line 1044
    .line 1045
    const-string v4, "GOV101"

    .line 1046
    .line 1047
    const-string v5, "\u6cd5\u5f8b\u6587\u4ef6"

    .line 1048
    .line 1049
    const-string v6, "personals"

    .line 1050
    .line 1051
    move-object v1, v0

    .line 1052
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    .line 1054
    .line 1055
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government1:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1056
    .line 1057
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1058
    .line 1059
    const-string v8, "Government2"

    .line 1060
    .line 1061
    const/16 v9, 0x35

    .line 1062
    .line 1063
    const-string v10, "GOV102"

    .line 1064
    .line 1065
    const-string v11, "\u7ea2\u5934\u6587\u4ef6"

    .line 1066
    .line 1067
    const-string v12, "personals"

    .line 1068
    .line 1069
    move-object v7, v0

    .line 1070
    invoke-direct/range {v7 .. v12}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    .line 1072
    .line 1073
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government2:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1074
    .line 1075
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1076
    .line 1077
    const-string v2, "Government3"

    .line 1078
    .line 1079
    const/16 v3, 0x36

    .line 1080
    .line 1081
    const-string v4, "GOV103"

    .line 1082
    .line 1083
    const-string v5, "\u5ba1\u6279\u6587\u4ef6"

    .line 1084
    .line 1085
    const-string v6, "personals"

    .line 1086
    .line 1087
    move-object v1, v0

    .line 1088
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1089
    .line 1090
    .line 1091
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Government3:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1092
    .line 1093
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->$values()[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1094
    .line 1095
    .line 1096
    move-result-object v0

    .line 1097
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->$VALUES:[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 1098
    .line 1099
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;

    .line 1100
    .line 1101
    const/4 v1, 0x0

    .line 1102
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 1103
    .line 1104
    .line 1105
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Companion:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;

    .line 1106
    .line 1107
    return-void
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->code:Ljava/lang/String;

    .line 5
    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->tagName:Ljava/lang/String;

    .line 7
    .line 8
    iput-object p5, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->category:Ljava/lang/String;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final getTagEnumByName(Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->Companion:Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum$Companion;->〇080(Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->$VALUES:[Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getCategory()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->category:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCode()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->code:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getString(I)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getTagName()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagEnum;->tagName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

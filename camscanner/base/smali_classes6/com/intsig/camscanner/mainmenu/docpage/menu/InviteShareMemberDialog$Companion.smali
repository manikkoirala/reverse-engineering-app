.class public final Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$Companion;
.super Ljava/lang/Object;
.source "InviteShareMemberDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$Companion$InviteShareMemberBundleEntity;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$Companion$InviteShareMemberBundleEntity;Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$ActionListener;)V
    .locals 2
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$Companion$InviteShareMemberBundleEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$ActionListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "extraData"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "callback"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog;-><init>()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog;->〇8O0880(Lcom/intsig/camscanner/mainmenu/docpage/menu/InviteShareMemberDialog$ActionListener;)V

    .line 22
    .line 23
    .line 24
    new-instance p3, Landroid/os/Bundle;

    .line 25
    .line 26
    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 27
    .line 28
    .line 29
    const-string v1, "extra_key_invite_share_dialog_data"

    .line 30
    .line 31
    invoke-virtual {p3, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p3}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    const-string p2, "InviteShareMemberDialog"

    .line 42
    .line 43
    invoke-virtual {v0, p1, p2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

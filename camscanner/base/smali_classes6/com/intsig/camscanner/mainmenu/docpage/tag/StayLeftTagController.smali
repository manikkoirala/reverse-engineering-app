.class public final Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;
.super Ljava/lang/Object;
.source "StayLeftTagController.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;,
        Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lcom/chad/library/adapter/base/BaseQuickAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;",
            "Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "StayLeftTagController::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇0O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binding"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "tagChangeCallBack"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o0:Landroid/content/Context;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->OO:Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;

    .line 24
    .line 25
    new-instance p3, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 31
    .line 32
    new-instance p3, Landroid/util/LongSparseArray;

    .line 33
    .line 34
    invoke-direct {p3}, Landroid/util/LongSparseArray;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o〇00O:Landroid/util/LongSparseArray;

    .line 38
    .line 39
    iget-object p3, p2, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 40
    .line 41
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    .line 43
    .line 44
    new-instance p3, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$1;

    .line 45
    .line 46
    invoke-direct {p3}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$1;-><init>()V

    .line 47
    .line 48
    .line 49
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->O8o08O8O:Lcom/chad/library/adapter/base/BaseQuickAdapter;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/〇080;

    .line 52
    .line 53
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/docpage/tag/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p3, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 57
    .line 58
    .line 59
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 60
    .line 61
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 62
    .line 63
    invoke-direct {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p1, "<anonymous parameter 1>"

    .line 12
    .line 13
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string p1, "CSMain"

    .line 17
    .line 18
    const-string p2, "fixed_label_click"

    .line 19
    .line 20
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->O8(I)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public final O8(I)V
    .locals 5

    .line 1
    if-ltz p1, :cond_3

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->O8o08O8O:Lcom/chad/library/adapter/base/BaseQuickAdapter;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-lt p1, v0, :cond_0

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->O8o08O8O:Lcom/chad/library/adapter/base/BaseQuickAdapter;

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItem(I)Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    check-cast p1, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 25
    .line 26
    .line 27
    move-result-wide v0

    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    cmp-long v4, v0, v2

    .line 33
    .line 34
    if-nez v4, :cond_1

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o〇00O:Landroid/util/LongSparseArray;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 40
    .line 41
    .line 42
    move-result-wide v1

    .line 43
    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Ljava/lang/Integer;

    .line 48
    .line 49
    if-nez v0, :cond_2

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇o〇(Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;I)V

    .line 58
    .line 59
    .line 60
    :cond_3
    :goto_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final Oo08(Lcom/intsig/camscanner/mainmenu/docpage/tag/TagsInfo;)V
    .locals 7
    .param p1    # Lcom/intsig/camscanner/mainmenu/docpage/tag/TagsInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "tagsInfo"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagsInfo;->〇080()Ljava/util/ArrayList;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagsInfo;->〇o00〇〇Oo()Landroid/util/LongSparseArray;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o〇00O:Landroid/util/LongSparseArray;

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->O8o08O8O:Lcom/chad/library/adapter/base/BaseQuickAdapter;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 29
    .line 30
    invoke-virtual {p1, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const/4 v0, 0x0

    .line 40
    const/4 v1, 0x0

    .line 41
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    if-eqz v2, :cond_2

    .line 46
    .line 47
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    check-cast v2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 54
    .line 55
    .line 56
    move-result-wide v3

    .line 57
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 58
    .line 59
    .line 60
    move-result-wide v5

    .line 61
    cmp-long v2, v3, v5

    .line 62
    .line 63
    if-nez v2, :cond_0

    .line 64
    .line 65
    const/4 v2, 0x1

    .line 66
    goto :goto_1

    .line 67
    :cond_0
    const/4 v2, 0x0

    .line 68
    :goto_1
    if-eqz v2, :cond_1

    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_2
    const/4 v1, -0x1

    .line 75
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 76
    .line 77
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 78
    .line 79
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const-string v0, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    .line 84
    .line 85
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 89
    .line 90
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    if-lt v0, v1, :cond_3

    .line 95
    .line 96
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-le v0, v1, :cond_4

    .line 101
    .line 102
    :cond_3
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPosition(I)V

    .line 103
    .line 104
    .line 105
    :cond_4
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    const v0, 0x7f0a0839

    .line 21
    .line 22
    .line 23
    if-ne p1, v0, :cond_2

    .line 24
    .line 25
    sget-object p1, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇0O:Ljava/lang/String;

    .line 26
    .line 27
    const-string v0, "User Operation:  add tag"

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->o0:Landroid/content/Context;

    .line 33
    .line 34
    invoke-static {p1}, Lcom/intsig/camscanner/app/DialogUtils;->O8ooOoo〇(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    :cond_2
    :goto_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o〇0(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->〇080()Landroid/widget/LinearLayout;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o〇(Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;I)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "tagItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O08o〇(J)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->OO:Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;

    .line 27
    .line 28
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController$TagChangeCallBack;->〇080(Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;I)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public final 〇〇888(Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/tag/StayLeftTagController;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutMainDocStayLeftTagListBinding;->〇080()Landroid/widget/LinearLayout;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "binding.root"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

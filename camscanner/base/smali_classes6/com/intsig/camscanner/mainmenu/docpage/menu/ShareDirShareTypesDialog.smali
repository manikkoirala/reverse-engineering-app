.class public final Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialog;
.source "ShareDirShareTypesDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;,
        Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO〇00〇8oO:Ljava/lang/String;

.field public static final oOo0:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

.field private OO:Ljava/lang/Long;

.field private final o0:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Z

.field private o〇00O:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private final 〇0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo0:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/Long;Lcom/intsig/camscanner/datastruct/FolderItem;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "mContext"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const v0, 0x7f14014d

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;I)V

    .line 10
    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 15
    .line 16
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO:Ljava/lang/Long;

    .line 17
    .line 18
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 19
    .line 20
    iput-object p5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o〇00O:Lkotlin/jvm/functions/Function2;

    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setCanceledOnTouchOutside(Z)V

    .line 24
    .line 25
    .line 26
    sget-object p1, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$mClickLimit$2;->o0:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$mClickLimit$2;

    .line 27
    .line 28
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇0O:Lkotlin/Lazy;

    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oO80(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    .line 7
    .line 8
    .line 9
    const-string p0, "CSFolderInvitePop"

    .line 10
    .line 11
    const-string p1, "close"

    .line 12
    .line 13
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo08()Lcom/intsig/utils/ClickLimit;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-mClickLimit>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Lcom/intsig/utils/ClickLimit;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final Oooo8o0〇(Lcom/intsig/camscanner/share/type/BaseShare;)V
    .locals 3

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareDirectLink;

    .line 2
    .line 3
    const-string v1, "CSFolderInvitePop"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string p1, "copy_link"

    .line 8
    .line 9
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 14
    .line 15
    if-eqz v0, :cond_4

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast p1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    const-string p1, "wechat"

    .line 36
    .line 37
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_2

    .line 56
    .line 57
    const-string p1, "qq"

    .line 58
    .line 59
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WHATS_APP:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->getPkgName()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/intsig/camscanner/share/type/BaseShare;->OO0o〇〇()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    if-eqz p1, :cond_3

    .line 78
    .line 79
    const-string p1, "whatsapp"

    .line 80
    .line 81
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_3
    const-string p1, "more"

    .line 86
    .line 87
    invoke-static {v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    :cond_4
    :goto_0
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final oO80(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/widget/CompoundButton;Z)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->Oo08()Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-nez p1, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iput-boolean p2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo〇8o008:Z

    .line 18
    .line 19
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇〇808〇(Z)V

    .line 20
    .line 21
    .line 22
    if-eqz p2, :cond_1

    .line 23
    .line 24
    const-string p1, "public"

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const-string p1, "private"

    .line 28
    .line 29
    :goto_0
    const-string v0, "CSFolderInvitePop"

    .line 30
    .line 31
    const-string v1, "doc_status"

    .line 32
    .line 33
    const-string v2, "type"

    .line 34
    .line 35
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 39
    .line 40
    if-eqz p0, :cond_2

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/datastruct/FolderItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p0

    .line 46
    if-eqz p0, :cond_2

    .line 47
    .line 48
    sget-object p1, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo0:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;

    .line 49
    .line 50
    invoke-virtual {p1, p0, p2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;->〇o00〇〇Oo(Ljava/lang/String;Z)V

    .line 51
    .line 52
    .line 53
    :cond_2
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇0()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/share/type/BaseShare;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    new-array v2, v1, [Ljava/lang/Object;

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 8
    .line 9
    aput-object v4, v2, v3

    .line 10
    .line 11
    const v3, 0x7f131013

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v2, "mContext.getString(R.string.cs_617_share87, link)"

    .line 19
    .line 20
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v2, Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    const v4, 0x7f080c08

    .line 33
    .line 34
    .line 35
    const/4 v5, 0x0

    .line 36
    if-ne v3, v1, :cond_0

    .line 37
    .line 38
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 39
    .line 40
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const v3, 0x7f080c41

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 49
    .line 50
    .line 51
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    const v6, 0x7f1317ae

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 69
    .line 70
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 74
    .line 75
    sget-object v6, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->EMAIL:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 76
    .line 77
    invoke-static {v3, v1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 78
    .line 79
    .line 80
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 81
    .line 82
    const v6, 0x7f130136

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareDirectLink;

    .line 96
    .line 97
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 98
    .line 99
    invoke-direct {v1, v3, v0}, Lcom/intsig/camscanner/share/type/ShareDirectLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 109
    .line 110
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 111
    .line 112
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 116
    .line 117
    sget-object v3, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WHATS_APP:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 118
    .line 119
    invoke-static {v0, v1, v3}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 123
    .line 124
    const v3, 0x7f1307eb

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_0
    if-nez v3, :cond_1

    .line 139
    .line 140
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 141
    .line 142
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 143
    .line 144
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 148
    .line 149
    sget-object v6, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->WE_CHAT:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 150
    .line 151
    invoke-static {v3, v1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 152
    .line 153
    .line 154
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 155
    .line 156
    const v6, 0x7f130fef

    .line 157
    .line 158
    .line 159
    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    .line 168
    .line 169
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 170
    .line 171
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 172
    .line 173
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 174
    .line 175
    .line 176
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 177
    .line 178
    sget-object v6, Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;->QQ:Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;

    .line 179
    .line 180
    invoke-static {v3, v1, v6}, Lcom/intsig/camscanner/share/ShareHelper;->o08oOO(Landroid/content/Context;Lcom/intsig/camscanner/share/type/BaseShare;Lcom/intsig/camscanner/share/ShareAppCompatibleEnum;)V

    .line 181
    .line 182
    .line 183
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 184
    .line 185
    const v6, 0x7f130ff0

    .line 186
    .line 187
    .line 188
    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v3

    .line 192
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    .line 197
    .line 198
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareDirectLink;

    .line 199
    .line 200
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 201
    .line 202
    invoke-direct {v1, v3, v0}, Lcom/intsig/camscanner/share/type/ShareDirectLink;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    .line 210
    .line 211
    new-instance v1, Lcom/intsig/camscanner/share/type/ShareTextCharacter;

    .line 212
    .line 213
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 214
    .line 215
    invoke-direct {v1, v3, v5, v0}, Lcom/intsig/camscanner/share/type/ShareTextCharacter;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    const v0, 0x7f080c30

    .line 219
    .line 220
    .line 221
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 222
    .line 223
    .line 224
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 225
    .line 226
    const v3, 0x7f130848

    .line 227
    .line 228
    .line 229
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    :cond_1
    :goto_0
    return-object v2
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p1, "<anonymous parameter 1>"

    .line 12
    .line 13
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/utils/FastClickUtil;->〇080()Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 24
    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO:Ljava/lang/Long;

    .line 28
    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    iget-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo〇8o008:Z

    .line 32
    .line 33
    if-nez p1, :cond_1

    .line 34
    .line 35
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇8o8o〇(I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o〇00O:Lkotlin/jvm/functions/Function2;

    .line 40
    .line 41
    if-eqz p1, :cond_2

    .line 42
    .line 43
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo〇8o008:Z

    .line 48
    .line 49
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    invoke-interface {p1, p2, p0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇8o8o〇(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o〇0()Ljava/util/ArrayList;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatDialog;->dismiss()V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 22
    .line 23
    if-eqz v1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/camscanner/share/type/BaseShare;

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const/4 v1, 0x0

    .line 39
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 40
    .line 41
    .line 42
    move-result v2

    .line 43
    if-eqz v2, :cond_2

    .line 44
    .line 45
    const/4 v2, 0x1

    .line 46
    if-ne p1, v2, :cond_2

    .line 47
    .line 48
    new-instance p1, Lcom/intsig/camscanner/share/channel/item/EmailShareChannel;

    .line 49
    .line 50
    invoke-direct {p1}, Lcom/intsig/camscanner/share/channel/item/EmailShareChannel;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/ShareHelper;->Oo08(Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->Oooo8o0〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇808〇(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-ne p1, v1, :cond_6

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->〇080OO8〇0:Landroidx/appcompat/widget/SwitchCompat;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    move-object p1, v0

    .line 13
    :goto_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    .line 17
    .line 18
    .line 19
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 20
    .line 21
    if-eqz p1, :cond_2

    .line 22
    .line 23
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 24
    .line 25
    goto :goto_2

    .line 26
    :cond_2
    move-object p1, v0

    .line 27
    :goto_2
    if-nez p1, :cond_3

    .line 28
    .line 29
    goto :goto_3

    .line 30
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 31
    .line 32
    const v2, 0x7f131786

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 43
    .line 44
    if-eqz p1, :cond_4

    .line 45
    .line 46
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 47
    .line 48
    :cond_4
    if-nez v0, :cond_5

    .line 49
    .line 50
    goto :goto_8

    .line 51
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    const v1, 0x7f131787

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    .line 62
    .line 63
    goto :goto_8

    .line 64
    :cond_6
    if-nez p1, :cond_d

    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 67
    .line 68
    if-eqz p1, :cond_7

    .line 69
    .line 70
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->〇080OO8〇0:Landroidx/appcompat/widget/SwitchCompat;

    .line 71
    .line 72
    goto :goto_4

    .line 73
    :cond_7
    move-object p1, v0

    .line 74
    :goto_4
    if-nez p1, :cond_8

    .line 75
    .line 76
    goto :goto_5

    .line 77
    :cond_8
    const/4 v1, 0x0

    .line 78
    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    .line 79
    .line 80
    .line 81
    :goto_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 82
    .line 83
    if-eqz p1, :cond_9

    .line 84
    .line 85
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 86
    .line 87
    goto :goto_6

    .line 88
    :cond_9
    move-object p1, v0

    .line 89
    :goto_6
    if-nez p1, :cond_a

    .line 90
    .line 91
    goto :goto_7

    .line 92
    :cond_a
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 93
    .line 94
    const v2, 0x7f131789

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    .line 103
    .line 104
    :goto_7
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 105
    .line 106
    if-eqz p1, :cond_b

    .line 107
    .line 108
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 109
    .line 110
    :cond_b
    if-nez v0, :cond_c

    .line 111
    .line 112
    goto :goto_8

    .line 113
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 114
    .line 115
    const v1, 0x7f131788

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    .line 124
    .line 125
    :cond_d
    :goto_8
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇〇888()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v0, v1

    .line 10
    :goto_0
    if-nez v0, :cond_1

    .line 11
    .line 12
    goto :goto_1

    .line 13
    :cond_1
    new-instance v2, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 14
    .line 15
    invoke-direct {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 16
    .line 17
    .line 18
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 19
    .line 20
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    const v4, 0x7f0601e1

    .line 25
    .line 26
    .line 27
    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    invoke-virtual {v2, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 40
    .line 41
    .line 42
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 43
    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 47
    .line 48
    goto :goto_2

    .line 49
    :cond_2
    move-object v0, v1

    .line 50
    :goto_2
    if-nez v0, :cond_3

    .line 51
    .line 52
    goto :goto_3

    .line 53
    :cond_3
    new-instance v2, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 54
    .line 55
    invoke-direct {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 56
    .line 57
    .line 58
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 59
    .line 60
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    const/16 v5, 0x8

    .line 65
    .line 66
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 67
    .line 68
    .line 69
    move-result v4

    .line 70
    int-to-float v4, v4

    .line 71
    invoke-virtual {v2, v4}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    const v4, 0x7f0601e0

    .line 76
    .line 77
    .line 78
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 83
    .line 84
    .line 85
    move-result v3

    .line 86
    invoke-virtual {v2, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    invoke-virtual {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 95
    .line 96
    .line 97
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 98
    .line 99
    const/4 v2, 0x0

    .line 100
    if-eqz v0, :cond_6

    .line 101
    .line 102
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    if-eqz v0, :cond_4

    .line 107
    .line 108
    sget-object v1, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->oOo0:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;

    .line 109
    .line 110
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$Companion;->〇080(Ljava/lang/String;)Z

    .line 111
    .line 112
    .line 113
    move-result v0

    .line 114
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇〇808〇(Z)V

    .line 115
    .line 116
    .line 117
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 118
    .line 119
    :cond_4
    if-nez v1, :cond_5

    .line 120
    .line 121
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇〇808〇(Z)V

    .line 122
    .line 123
    .line 124
    :cond_5
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 125
    .line 126
    :cond_6
    if-nez v1, :cond_7

    .line 127
    .line 128
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇〇808〇(Z)V

    .line 129
    .line 130
    .line 131
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 132
    .line 133
    if-eqz v0, :cond_8

    .line 134
    .line 135
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->〇080OO8〇0:Landroidx/appcompat/widget/SwitchCompat;

    .line 136
    .line 137
    if-eqz v0, :cond_8

    .line 138
    .line 139
    new-instance v1, LOo8/o〇0;

    .line 140
    .line 141
    invoke-direct {v1, p0}, LOo8/o〇0;-><init>(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 145
    .line 146
    .line 147
    :cond_8
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final OO0o〇〇(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "newLink"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇8o8o〇(I)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const p1, 0x7f0d0227

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(I)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {v0, p1, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v2, 0x0

    .line 25
    invoke-virtual {v0, v2}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setHideable(Z)V

    .line 26
    .line 27
    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 33
    .line 34
    new-instance p1, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 35
    .line 36
    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 42
    .line 43
    if-eqz p1, :cond_0

    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    move-object p1, v1

    .line 49
    :goto_0
    if-nez p1, :cond_1

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    new-instance v0, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 53
    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    const/4 v4, 0x4

    .line 57
    invoke-direct {v0, v3, v4}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 61
    .line 62
    .line 63
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 64
    .line 65
    if-eqz p1, :cond_2

    .line 66
    .line 67
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 68
    .line 69
    goto :goto_2

    .line 70
    :cond_2
    move-object p1, v1

    .line 71
    :goto_2
    if-nez p1, :cond_3

    .line 72
    .line 73
    goto :goto_3

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 75
    .line 76
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 77
    .line 78
    .line 79
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o〇0()Ljava/util/ArrayList;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 84
    .line 85
    if-eqz v0, :cond_4

    .line 86
    .line 87
    invoke-virtual {v0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 88
    .line 89
    .line 90
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog$ShareDirAdapter;

    .line 91
    .line 92
    if-eqz p1, :cond_5

    .line 93
    .line 94
    new-instance v0, LOo8/O8;

    .line 95
    .line 96
    invoke-direct {v0, p0}, LOo8/O8;-><init>(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 100
    .line 101
    .line 102
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 103
    .line 104
    if-eqz p1, :cond_6

    .line 105
    .line 106
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 107
    .line 108
    if-eqz p1, :cond_6

    .line 109
    .line 110
    new-instance v0, LOo8/Oo08;

    .line 111
    .line 112
    invoke-direct {v0, p0}, LOo8/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    .line 117
    .line 118
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO:Ljava/lang/Long;

    .line 119
    .line 120
    if-eqz p1, :cond_7

    .line 121
    .line 122
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 123
    .line 124
    .line 125
    move-result-wide v3

    .line 126
    goto :goto_4

    .line 127
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 128
    .line 129
    .line 130
    move-result-wide v3

    .line 131
    const-wide/32 v5, 0xf731400

    .line 132
    .line 133
    .line 134
    add-long/2addr v3, v5

    .line 135
    :goto_4
    invoke-static {}, Lcom/intsig/camscanner/util/DocDirNameFormat;->o〇0()Ljava/text/SimpleDateFormat;

    .line 136
    .line 137
    .line 138
    move-result-object p1

    .line 139
    new-instance v0, Ljava/util/Date;

    .line 140
    .line 141
    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    sget-object v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 149
    .line 150
    new-instance v3, Ljava/lang/StringBuilder;

    .line 151
    .line 152
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .line 154
    .line 155
    const-string v4, "expire timeStr == "

    .line 156
    .line 157
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v3

    .line 167
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇080OO8〇0:Lcom/intsig/camscanner/databinding/DialogShareDirBinding;

    .line 171
    .line 172
    if-eqz v0, :cond_8

    .line 173
    .line 174
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogShareDirBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 175
    .line 176
    :cond_8
    if-nez v1, :cond_9

    .line 177
    .line 178
    goto :goto_5

    .line 179
    :cond_9
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->o0:Landroidx/fragment/app/FragmentActivity;

    .line 182
    .line 183
    const v3, 0x7f130fee

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    const-string v3, "mContext.getString(R.string.cs_617_share02)"

    .line 191
    .line 192
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    const/4 v3, 0x1

    .line 196
    new-array v4, v3, [Ljava/lang/Object;

    .line 197
    .line 198
    aput-object p1, v4, v2

    .line 199
    .line 200
    invoke-static {v4, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object p1

    .line 208
    const-string v0, "format(format, *args)"

    .line 209
    .line 210
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    .line 215
    .line 216
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->〇〇888()V

    .line 217
    .line 218
    .line 219
    return-void
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSFolderInvitePop"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public show()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirShareTypesDialog;->OO:Ljava/lang/Long;

    .line 7
    .line 8
    new-instance v2, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v3, "show expireTimeInMills:"

    .line 14
    .line 15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    .line 33
    .line 34
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const-string v2, "win.attributes"

    .line 39
    .line 40
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const/4 v2, -0x1

    .line 44
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 45
    .line 46
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 49
    .line 50
    .line 51
    :cond_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public final Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;
.super Lcom/intsig/camscanner/view/CsBottomItemsDialog;
.source "MainDocMoreMenu.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;,
        Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇O〇〇O8:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private o8oOOo:Z

.field private final ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8〇oO〇〇8o:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇O〇〇O8:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;)V
    .locals 8
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mainDocFragment"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "mIMenuOperation"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    const/4 v4, 0x0

    .line 18
    const/4 v5, 0x0

    .line 19
    const/16 v6, 0xe

    .line 20
    .line 21
    const/4 v7, 0x0

    .line 22
    move-object v1, p0

    .line 23
    move-object v2, p1

    .line 24
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;-><init>(Landroid/content/Context;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 25
    .line 26
    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 28
    .line 29
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 30
    .line 31
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o8o8〇o()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇8o8o〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OoO8()Lcom/intsig/menu/MenuItem;
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOo(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const v1, 0x7f080507

    .line 10
    .line 11
    .line 12
    const v2, 0x7f130537

    .line 13
    .line 14
    .line 15
    const/16 v3, 0x21

    .line 16
    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    const/4 v4, 0x1

    .line 20
    if-eq v0, v4, :cond_1

    .line 21
    .line 22
    const/4 v4, 0x2

    .line 23
    if-eq v0, v4, :cond_0

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v4

    .line 31
    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-direct {v0, v3, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 40
    .line 41
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const v2, 0x7f130538

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const v2, 0x7f080505

    .line 53
    .line 54
    .line 55
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 60
    .line 61
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const v2, 0x7f131c6d

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    const v2, 0x7f080506

    .line 73
    .line 74
    .line 75
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_2
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 80
    .line 81
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-direct {v0, v3, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 90
    .line 91
    .line 92
    :goto_0
    return-object v0
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o800o8O(I)Z
    .locals 0

    .line 1
    packed-switch p1, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    goto :goto_0

    .line 6
    :pswitch_0
    const/4 p1, 0x1

    .line 7
    :goto_0
    return p1

    .line 8
    nop

    .line 9
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oo88o8O(I)Z
    .locals 1

    .line 1
    const/16 v0, 0x22

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x1

    .line 8
    :goto_0
    return p1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0〇O0088o(Lcom/intsig/camscanner/datastruct/FolderItem;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/menu/MenuTypeItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderMenuItemControl;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderMenuItemControl;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    invoke-virtual {v1, p1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderMenuItemControl;->o〇0(Lcom/intsig/camscanner/datastruct/FolderItem;Landroid/content/Context;)Lcom/intsig/menu/MenuItem;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o8o8〇o()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->o〇0()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const-string v3, "folder_invite_show"

    .line 32
    .line 33
    invoke-static {v2, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-virtual {v1, p1, v2}, Lcom/intsig/camscanner/mainmenu/adapter/newmainitem/FolderMenuItemControl;->Oo08(Lcom/intsig/camscanner/datastruct/FolderItem;Landroid/content/Context;)Lcom/intsig/menu/MenuItem;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    if-eqz v1, :cond_1

    .line 45
    .line 46
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o8o8〇o()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->o〇0()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    const-string v2, "folder_cooperate_detail_show"

    .line 60
    .line 61
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    if-lez v1, :cond_2

    .line 69
    .line 70
    new-instance v1, Lcom/intsig/camscanner/view/MenuGroupTitle;

    .line 71
    .line 72
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    const v3, 0x7f131006

    .line 77
    .line 78
    .line 79
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    const-string v3, "mContext.getString(R.string.cs_617_share61)"

    .line 84
    .line 85
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    const/4 v3, 0x1

    .line 89
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oO()Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    invoke-direct {v1, v2, v3, p1}, Lcom/intsig/camscanner/view/MenuGroupTitle;-><init>(Ljava/lang/String;ZZ)V

    .line 94
    .line 95
    .line 96
    const/4 p1, 0x0

    .line 97
    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 98
    .line 99
    .line 100
    new-instance p1, Lcom/intsig/camscanner/view/MenuGroupDivider;

    .line 101
    .line 102
    invoke-direct {p1}, Lcom/intsig/camscanner/view/MenuGroupDivider;-><init>()V

    .line 103
    .line 104
    .line 105
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    :cond_2
    return-object v0
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇O00(I)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const/4 v0, 0x4

    .line 2
    if-eq p1, v0, :cond_9

    .line 3
    .line 4
    const/4 v0, 0x5

    .line 5
    if-eq p1, v0, :cond_8

    .line 6
    .line 7
    const/16 v0, 0x1f

    .line 8
    .line 9
    const-string v1, "MainDocFolderMenu"

    .line 10
    .line 11
    if-eq p1, v0, :cond_7

    .line 12
    .line 13
    packed-switch p1, :pswitch_data_0

    .line 14
    .line 15
    .line 16
    const-string v0, "CSFolderMode"

    .line 17
    .line 18
    const-string v2, "CSFolderMore"

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    const/4 v4, 0x0

    .line 22
    packed-switch p1, :pswitch_data_1

    .line 23
    .line 24
    .line 25
    new-instance v2, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v5, "User Operation:  click changeViewMode to "

    .line 31
    .line 32
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->o800o8O(I)Z

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-eqz v2, :cond_a

    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇O〇(I)I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    const/4 v5, 0x0

    .line 64
    if-ltz v2, :cond_0

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    const/4 v3, 0x0

    .line 68
    :goto_0
    if-eqz v3, :cond_1

    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_1
    move-object p1, v4

    .line 72
    :goto_1
    if-eqz p1, :cond_a

    .line 73
    .line 74
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 79
    .line 80
    if-eqz v2, :cond_3

    .line 81
    .line 82
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 83
    .line 84
    sget-object v4, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080:Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;

    .line 85
    .line 86
    invoke-virtual {v4, p1}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->OoO8(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v4

    .line 90
    invoke-virtual {v3, v0, v4}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo0〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    if-eqz v0, :cond_2

    .line 98
    .line 99
    invoke-static {v0, p1}, Lcom/intsig/camscanner/app/ScenarioDBUtilKt;->O8(Ljava/lang/String;I)I

    .line 100
    .line 101
    .line 102
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oO00OOO(I)V

    .line 107
    .line 108
    .line 109
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 110
    .line 111
    invoke-virtual {v2, v0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇0〇OO〇O0O(II)V

    .line 112
    .line 113
    .line 114
    sget-object v4, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 115
    .line 116
    :cond_3
    if-nez v4, :cond_4

    .line 117
    .line 118
    const-string p1, "click changeViewMode but mCurrentFolderItem NULL"

    .line 119
    .line 120
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    :cond_4
    iput-boolean v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->o8oOOo:Z

    .line 124
    .line 125
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->〇〇808〇()V

    .line 126
    .line 127
    .line 128
    goto/16 :goto_2

    .line 129
    .line 130
    :pswitch_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 131
    .line 132
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇8o8o〇()V

    .line 133
    .line 134
    .line 135
    goto/16 :goto_2

    .line 136
    .line 137
    :pswitch_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 138
    .line 139
    const-string v0, "move_copy"

    .line 140
    .line 141
    invoke-virtual {p1, v2, v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo0〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 145
    .line 146
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->oO80()V

    .line 147
    .line 148
    .line 149
    goto/16 :goto_2

    .line 150
    .line 151
    :pswitch_2
    const-string p1, "User Operation:  onclick menu_switch_dir_view_mode"

    .line 152
    .line 153
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    iput-boolean v3, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->o8oOOo:Z

    .line 157
    .line 158
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 159
    .line 160
    const-string v1, "view"

    .line 161
    .line 162
    invoke-virtual {p1, v2, v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo0〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 166
    .line 167
    const/4 v1, 0x2

    .line 168
    invoke-static {p1, v0, v4, v1, v4}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o0〇O8〇0o(Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->〇〇808〇()V

    .line 172
    .line 173
    .line 174
    goto/16 :goto_2

    .line 175
    .line 176
    :pswitch_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 177
    .line 178
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇o00〇〇Oo()V

    .line 179
    .line 180
    .line 181
    goto/16 :goto_2

    .line 182
    .line 183
    :pswitch_4
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 184
    .line 185
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇80〇808〇O()V

    .line 186
    .line 187
    .line 188
    goto :goto_2

    .line 189
    :pswitch_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 190
    .line 191
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->Oo08()V

    .line 192
    .line 193
    .line 194
    goto :goto_2

    .line 195
    :pswitch_6
    const-string p1, "User Operation:  onclick importPdfFromLocal"

    .line 196
    .line 197
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 201
    .line 202
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->OO0o〇〇〇〇0()V

    .line 203
    .line 204
    .line 205
    goto :goto_2

    .line 206
    :pswitch_7
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 207
    .line 208
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->o〇0()V

    .line 209
    .line 210
    .line 211
    goto :goto_2

    .line 212
    :pswitch_8
    sget-boolean p1, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇o〇:Z

    .line 213
    .line 214
    if-eqz p1, :cond_6

    .line 215
    .line 216
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 217
    .line 218
    .line 219
    move-result p1

    .line 220
    if-eqz p1, :cond_5

    .line 221
    .line 222
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 223
    .line 224
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->O8()V

    .line 225
    .line 226
    .line 227
    goto :goto_2

    .line 228
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 229
    .line 230
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 231
    .line 232
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OFFLINE_FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 233
    .line 234
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 235
    .line 236
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 237
    .line 238
    .line 239
    invoke-static {p1, v0}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 240
    .line 241
    .line 242
    goto :goto_2

    .line 243
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 244
    .line 245
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->O8()V

    .line 246
    .line 247
    .line 248
    goto :goto_2

    .line 249
    :cond_7
    const-string p1, "User Operation: buy"

    .line 250
    .line 251
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    .line 253
    .line 254
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 255
    .line 256
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇〇888()V

    .line 257
    .line 258
    .line 259
    goto :goto_2

    .line 260
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 261
    .line 262
    if-eqz p1, :cond_a

    .line 263
    .line 264
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 265
    .line 266
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o08O80O(Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 267
    .line 268
    .line 269
    goto :goto_2

    .line 270
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 271
    .line 272
    if-eqz p1, :cond_a

    .line 273
    .line 274
    const-string v0, "CSDirectory"

    .line 275
    .line 276
    const-string v1, "folder_invite_click"

    .line 277
    .line 278
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 282
    .line 283
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 284
    .line 285
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->O〇8Oo(Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/Boolean;)V

    .line 286
    .line 287
    .line 288
    :cond_a
    :goto_2
    return-void

    .line 289
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    :pswitch_data_1
    .packed-switch 0x21
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇O〇(I)I
    .locals 0

    .line 1
    packed-switch p1, :pswitch_data_0

    .line 2
    .line 3
    .line 4
    const/4 p1, -0x1

    .line 5
    goto :goto_0

    .line 6
    :pswitch_0
    const/4 p1, 0x4

    .line 7
    goto :goto_0

    .line 8
    :pswitch_1
    const/4 p1, 0x3

    .line 9
    goto :goto_0

    .line 10
    :pswitch_2
    const/4 p1, 0x2

    .line 11
    goto :goto_0

    .line 12
    :pswitch_3
    const/4 p1, 0x1

    .line 13
    goto :goto_0

    .line 14
    :pswitch_4
    const/4 p1, 0x0

    .line 15
    :goto_0
    return p1

    .line 16
    nop

    .line 17
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/menu/MenuItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x5

    .line 7
    new-array v1, v1, [Ljava/lang/Integer;

    .line 8
    .line 9
    const/16 v2, 0x64

    .line 10
    .line 11
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const/4 v3, 0x0

    .line 16
    aput-object v2, v1, v3

    .line 17
    .line 18
    const/16 v2, 0x65

    .line 19
    .line 20
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    const/4 v4, 0x1

    .line 25
    aput-object v2, v1, v4

    .line 26
    .line 27
    const/16 v2, 0x66

    .line 28
    .line 29
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    const/4 v5, 0x2

    .line 34
    aput-object v2, v1, v5

    .line 35
    .line 36
    const/16 v2, 0x67

    .line 37
    .line 38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 39
    .line 40
    .line 41
    move-result-object v2

    .line 42
    const/4 v5, 0x3

    .line 43
    aput-object v2, v1, v5

    .line 44
    .line 45
    const/16 v2, 0x68

    .line 46
    .line 47
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    const/4 v5, 0x4

    .line 52
    aput-object v2, v1, v5

    .line 53
    .line 54
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-eqz v2, :cond_3

    .line 67
    .line 68
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    check-cast v2, Ljava/lang/Number;

    .line 73
    .line 74
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 75
    .line 76
    .line 77
    move-result v2

    .line 78
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇O〇(I)I

    .line 79
    .line 80
    .line 81
    move-result v5

    .line 82
    new-instance v6, Lcom/intsig/menu/MenuItem;

    .line 83
    .line 84
    invoke-static {v5}, Lcom/intsig/camscanner/mainmenu/folder/scenario/ScenarioDirUtilKt;->O8(I)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v7

    .line 88
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 89
    .line 90
    if-eqz v8, :cond_0

    .line 91
    .line 92
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 93
    .line 94
    .line 95
    move-result v8

    .line 96
    if-ne v8, v5, :cond_0

    .line 97
    .line 98
    const/4 v8, 0x1

    .line 99
    goto :goto_1

    .line 100
    :cond_0
    const/4 v8, 0x0

    .line 101
    :goto_1
    invoke-static {v5, v8}, Lcom/intsig/camscanner/mainmenu/folder/scenario/ScenarioDirUtilKt;->〇o00〇〇Oo(IZ)I

    .line 102
    .line 103
    .line 104
    move-result v8

    .line 105
    invoke-direct {v6, v2, v7, v8}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 106
    .line 107
    .line 108
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 109
    .line 110
    if-eqz v2, :cond_1

    .line 111
    .line 112
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    if-ne v2, v5, :cond_1

    .line 117
    .line 118
    const/4 v2, 0x1

    .line 119
    goto :goto_2

    .line 120
    :cond_1
    const/4 v2, 0x0

    .line 121
    :goto_2
    if-eqz v2, :cond_2

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    const v5, 0x7f060123

    .line 128
    .line 129
    .line 130
    invoke-static {v2, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 131
    .line 132
    .line 133
    move-result v2

    .line 134
    invoke-virtual {v6, v2}, Lcom/intsig/menu/MenuItem;->〇O00(I)V

    .line 135
    .line 136
    .line 137
    :cond_2
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_3
    return-object v0
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public OO0o〇〇(II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->oo88o8O(I)Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->dismiss()V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇O00(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 2
    .line 3
    const v1, 0x7f13066b

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "mActivity.getString(R.string.cs_511_more)"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/menu/MenuTypeItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->o8oOOo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇8O0〇8()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0

    .line 10
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 16
    .line 17
    const v2, 0x7f080917

    .line 18
    .line 19
    .line 20
    const v3, 0x7f1307e0

    .line 21
    .line 22
    .line 23
    const/16 v4, 0x10

    .line 24
    .line 25
    const v5, 0x7f080902

    .line 26
    .line 27
    .line 28
    const v6, 0x7f1301b5

    .line 29
    .line 30
    .line 31
    const/16 v7, 0xf

    .line 32
    .line 33
    if-eqz v1, :cond_d

    .line 34
    .line 35
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇0〇O0088o(Lcom/intsig/camscanner/datastruct/FolderItem;)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    check-cast v1, Ljava/util/Collection;

    .line 40
    .line 41
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 42
    .line 43
    .line 44
    move-result v8

    .line 45
    const/4 v9, 0x1

    .line 46
    xor-int/2addr v8, v9

    .line 47
    if-eqz v8, :cond_1

    .line 48
    .line 49
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 50
    .line 51
    .line 52
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 57
    .line 58
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->O〇O〇oO()Z

    .line 59
    .line 60
    .line 61
    move-result v8

    .line 62
    if-nez v8, :cond_2

    .line 63
    .line 64
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 65
    .line 66
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    .line 67
    .line 68
    .line 69
    move-result v8

    .line 70
    if-nez v8, :cond_2

    .line 71
    .line 72
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 73
    .line 74
    invoke-interface {v8}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇o〇()Z

    .line 75
    .line 76
    .line 77
    move-result v8

    .line 78
    if-nez v8, :cond_2

    .line 79
    .line 80
    new-instance v8, Lcom/intsig/menu/MenuItem;

    .line 81
    .line 82
    iget-object v10, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 83
    .line 84
    const v11, 0x7f13021e

    .line 85
    .line 86
    .line 87
    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v10

    .line 91
    const v11, 0x7f080bb1

    .line 92
    .line 93
    .line 94
    const/16 v12, 0xe

    .line 95
    .line 96
    invoke-direct {v8, v12, v10, v11}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 97
    .line 98
    .line 99
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    .line 101
    .line 102
    :cond_2
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 103
    .line 104
    invoke-virtual {v8}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    .line 105
    .line 106
    .line 107
    move-result v8

    .line 108
    if-nez v8, :cond_3

    .line 109
    .line 110
    new-instance v8, Lcom/intsig/menu/MenuItem;

    .line 111
    .line 112
    iget-object v10, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 113
    .line 114
    invoke-virtual {v10, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v6

    .line 118
    invoke-direct {v8, v7, v6, v5}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 119
    .line 120
    .line 121
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    .line 123
    .line 124
    new-instance v5, Lcom/intsig/menu/MenuItem;

    .line 125
    .line 126
    iget-object v6, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 127
    .line 128
    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v3

    .line 132
    invoke-direct {v5, v4, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 133
    .line 134
    .line 135
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    :cond_3
    const/4 v2, 0x0

    .line 139
    const/4 v3, 0x0

    .line 140
    if-eqz v1, :cond_4

    .line 141
    .line 142
    new-instance v4, Lcom/intsig/menu/MenuItem;

    .line 143
    .line 144
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 145
    .line 146
    const v6, 0x7f131039

    .line 147
    .line 148
    .line 149
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v5

    .line 153
    iget-object v6, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 154
    .line 155
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 156
    .line 157
    .line 158
    move-result v6

    .line 159
    const/4 v7, 0x2

    .line 160
    invoke-static {v6, v3, v7, v2}, Lcom/intsig/camscanner/mainmenu/folder/scenario/ScenarioDirUtilKt;->〇o〇(IZILjava/lang/Object;)I

    .line 161
    .line 162
    .line 163
    move-result v6

    .line 164
    const/16 v7, 0x22

    .line 165
    .line 166
    invoke-direct {v4, v7, v5, v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 167
    .line 168
    .line 169
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 170
    .line 171
    invoke-virtual {v5}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 172
    .line 173
    .line 174
    move-result v5

    .line 175
    invoke-static {v5}, Lcom/intsig/camscanner/mainmenu/folder/scenario/ScenarioDirUtilKt;->O8(I)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v5

    .line 179
    invoke-virtual {v4, v5}, Lcom/intsig/menu/MenuItem;->OO0o〇〇(Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    :cond_4
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 186
    .line 187
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/FolderItem;->o8oO〇()Z

    .line 188
    .line 189
    .line 190
    move-result v4

    .line 191
    if-nez v4, :cond_5

    .line 192
    .line 193
    if-eqz v1, :cond_5

    .line 194
    .line 195
    new-instance v4, Lcom/intsig/menu/MenuItem;

    .line 196
    .line 197
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 198
    .line 199
    const v6, 0x7f130889

    .line 200
    .line 201
    .line 202
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v5

    .line 206
    const v6, 0x7f080a12

    .line 207
    .line 208
    .line 209
    const/16 v7, 0x23

    .line 210
    .line 211
    invoke-direct {v4, v7, v5, v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 212
    .line 213
    .line 214
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    .line 216
    .line 217
    :cond_5
    if-eqz v1, :cond_7

    .line 218
    .line 219
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->O0O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 220
    .line 221
    invoke-virtual {v4}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇8〇0〇o〇O()Z

    .line 222
    .line 223
    .line 224
    move-result v4

    .line 225
    if-eqz v4, :cond_7

    .line 226
    .line 227
    new-instance v4, Lcom/intsig/menu/MenuItem;

    .line 228
    .line 229
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 230
    .line 231
    const v6, 0x7f131130

    .line 232
    .line 233
    .line 234
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object v5

    .line 238
    const v6, 0x7f080787

    .line 239
    .line 240
    .line 241
    const/16 v7, 0x24

    .line 242
    .line 243
    invoke-direct {v4, v7, v5, v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 244
    .line 245
    .line 246
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 247
    .line 248
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo8O8o80()Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 249
    .line 250
    .line 251
    move-result-object v5

    .line 252
    if-eqz v5, :cond_6

    .line 253
    .line 254
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->〇8o〇〇8080()Ljava/util/List;

    .line 255
    .line 256
    .line 257
    move-result-object v5

    .line 258
    if-eqz v5, :cond_6

    .line 259
    .line 260
    check-cast v5, Ljava/util/Collection;

    .line 261
    .line 262
    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    .line 263
    .line 264
    .line 265
    move-result v5

    .line 266
    xor-int/2addr v5, v9

    .line 267
    goto :goto_0

    .line 268
    :cond_6
    const/4 v5, 0x1

    .line 269
    :goto_0
    invoke-virtual {v4, v5}, Lcom/intsig/menu/MenuItem;->〇〇808〇(Z)V

    .line 270
    .line 271
    .line 272
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    .line 274
    .line 275
    :cond_7
    if-nez v1, :cond_8

    .line 276
    .line 277
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->OoO8()Lcom/intsig/menu/MenuItem;

    .line 278
    .line 279
    .line 280
    move-result-object v1

    .line 281
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    :cond_8
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇〇08O:Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;

    .line 285
    .line 286
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu$IMenuOperation;->〇080()I

    .line 287
    .line 288
    .line 289
    move-result v1

    .line 290
    const/4 v4, 0x4

    .line 291
    if-eq v1, v4, :cond_9

    .line 292
    .line 293
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 294
    .line 295
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 296
    .line 297
    const v5, 0x7f130229

    .line 298
    .line 299
    .line 300
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 301
    .line 302
    .line 303
    move-result-object v4

    .line 304
    const v5, 0x7f080c96

    .line 305
    .line 306
    .line 307
    const/16 v6, 0x11

    .line 308
    .line 309
    invoke-direct {v1, v6, v4, v5}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 310
    .line 311
    .line 312
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    .line 314
    .line 315
    :cond_9
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 316
    .line 317
    instance-of v4, v1, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;

    .line 318
    .line 319
    if-eqz v4, :cond_a

    .line 320
    .line 321
    move-object v2, v1

    .line 322
    check-cast v2, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;

    .line 323
    .line 324
    :cond_a
    if-eqz v2, :cond_b

    .line 325
    .line 326
    invoke-interface {v2}, Lcom/intsig/camscanner/mainmenu/docpage/inter/DocTypeActivity;->〇080O0()Z

    .line 327
    .line 328
    .line 329
    move-result v1

    .line 330
    if-ne v1, v9, :cond_b

    .line 331
    .line 332
    goto :goto_1

    .line 333
    :cond_b
    const/4 v9, 0x0

    .line 334
    :goto_1
    if-eqz v9, :cond_c

    .line 335
    .line 336
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 337
    .line 338
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 339
    .line 340
    const v3, 0x7f130227

    .line 341
    .line 342
    .line 343
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 344
    .line 345
    .line 346
    move-result-object v2

    .line 347
    const v3, 0x7f080bb2

    .line 348
    .line 349
    .line 350
    const/16 v4, 0x12

    .line 351
    .line 352
    invoke-direct {v1, v4, v2, v3}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 353
    .line 354
    .line 355
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    .line 357
    .line 358
    :cond_c
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 359
    .line 360
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇O8〇〇o()Z

    .line 361
    .line 362
    .line 363
    move-result v1

    .line 364
    if-nez v1, :cond_e

    .line 365
    .line 366
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->O08000()Z

    .line 367
    .line 368
    .line 369
    move-result v1

    .line 370
    if-nez v1, :cond_e

    .line 371
    .line 372
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 373
    .line 374
    const/16 v3, 0x1f

    .line 375
    .line 376
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 377
    .line 378
    .line 379
    move-result-object v2

    .line 380
    const v4, 0x7f131c65

    .line 381
    .line 382
    .line 383
    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 384
    .line 385
    .line 386
    move-result-object v4

    .line 387
    const v5, 0x7f0809ec

    .line 388
    .line 389
    .line 390
    const/4 v6, 0x0

    .line 391
    const/4 v7, -0x1

    .line 392
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 393
    .line 394
    .line 395
    move-result-object v2

    .line 396
    const v8, 0x7f0606af

    .line 397
    .line 398
    .line 399
    invoke-static {v2, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 400
    .line 401
    .line 402
    move-result v8

    .line 403
    move-object v2, v1

    .line 404
    invoke-direct/range {v2 .. v8}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;IZII)V

    .line 405
    .line 406
    .line 407
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    .line 409
    .line 410
    goto :goto_2

    .line 411
    :cond_d
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 412
    .line 413
    iget-object v8, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 414
    .line 415
    invoke-virtual {v8, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 416
    .line 417
    .line 418
    move-result-object v6

    .line 419
    invoke-direct {v1, v7, v6, v5}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 420
    .line 421
    .line 422
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    .line 424
    .line 425
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 426
    .line 427
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/docpage/menu/MainDocMoreMenu;->〇8〇oO〇〇8o:Landroid/app/Activity;

    .line 428
    .line 429
    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v3

    .line 433
    invoke-direct {v1, v4, v3, v2}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 434
    .line 435
    .line 436
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    .line 438
    .line 439
    :cond_e
    :goto_2
    return-object v0
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

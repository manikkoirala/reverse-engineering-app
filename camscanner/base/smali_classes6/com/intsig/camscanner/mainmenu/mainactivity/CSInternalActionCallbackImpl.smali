.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;
.super Ljava/lang/Object;
.source "CSInternalActionCallbackImpl.kt"

# interfaces
.implements Lcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    sget-object v2, Lcom/intsig/camscanner/web/PARAMATER_KEY;->position:Lcom/intsig/camscanner/web/PARAMATER_KEY;

    .line 8
    .line 9
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    check-cast v1, Ljava/lang/String;

    .line 14
    .line 15
    sget-object v2, Lcom/intsig/camscanner/web/PARAMATER_VALUE;->main:Lcom/intsig/camscanner/web/PARAMATER_VALUE;

    .line 16
    .line 17
    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-static {v2, v1, v3}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x0

    .line 27
    if-nez v1, :cond_0

    .line 28
    .line 29
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const-string v3, "position`s value is not main"

    .line 36
    .line 37
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return v2

    .line 41
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-nez v1, :cond_1

    .line 46
    .line 47
    const/4 v4, -0x1

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    sget-object v4, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl$WhenMappings;->〇o00〇〇Oo:[I

    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    aget v4, v4, v5

    .line 56
    .line 57
    :goto_0
    packed-switch v4, :pswitch_data_0

    .line 58
    .line 59
    .line 60
    sget-object v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 61
    .line 62
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    new-instance v4, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v5, "function is "

    .line 76
    .line 77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    const/4 v3, 0x0

    .line 91
    goto/16 :goto_3

    .line 92
    .line 93
    :pswitch_0
    iget-object v5, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 94
    .line 95
    sget-object v6, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 96
    .line 97
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇000()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 98
    .line 99
    .line 100
    move-result-object v7

    .line 101
    sget-object v8, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_MIXED:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 102
    .line 103
    const/4 v9, 0x0

    .line 104
    const/4 v10, 0x0

    .line 105
    const/4 v11, 0x0

    .line 106
    const/4 v12, 0x0

    .line 107
    const/16 v13, 0x78

    .line 108
    .line 109
    const/4 v14, 0x0

    .line 110
    invoke-static/range {v5 .. v14}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 111
    .line 112
    .line 113
    const-string v1, "CSTransferResultFolder"

    .line 114
    .line 115
    const-string v2, "camera"

    .line 116
    .line 117
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    goto/16 :goto_3

    .line 121
    .line 122
    :pswitch_1
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 123
    .line 124
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->IMAGE_RESTORE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 125
    .line 126
    const/4 v6, 0x0

    .line 127
    const/4 v7, 0x0

    .line 128
    const/4 v8, 0x0

    .line 129
    const/4 v9, 0x0

    .line 130
    const/4 v10, 0x0

    .line 131
    const/4 v11, 0x0

    .line 132
    const/16 v12, 0x7e

    .line 133
    .line 134
    const/4 v13, 0x0

    .line 135
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 136
    .line 137
    .line 138
    goto/16 :goto_3

    .line 139
    .line 140
    :pswitch_2
    iget-object v14, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 141
    .line 142
    sget-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 143
    .line 144
    const/16 v16, 0x0

    .line 145
    .line 146
    const/16 v17, 0x0

    .line 147
    .line 148
    const/16 v18, 0x0

    .line 149
    .line 150
    const/16 v19, 0x0

    .line 151
    .line 152
    const/16 v20, 0x0

    .line 153
    .line 154
    const/16 v21, 0x0

    .line 155
    .line 156
    const/16 v22, 0x7e

    .line 157
    .line 158
    const/16 v23, 0x0

    .line 159
    .line 160
    invoke-static/range {v14 .. v23}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 161
    .line 162
    .line 163
    goto/16 :goto_3

    .line 164
    .line 165
    :pswitch_3
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 166
    .line 167
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 168
    .line 169
    const/4 v6, 0x0

    .line 170
    const/4 v7, 0x0

    .line 171
    const/4 v8, 0x0

    .line 172
    const/4 v9, 0x0

    .line 173
    const/4 v10, 0x0

    .line 174
    const/4 v11, 0x0

    .line 175
    const/16 v12, 0x7e

    .line 176
    .line 177
    const/4 v13, 0x0

    .line 178
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 179
    .line 180
    .line 181
    goto/16 :goto_3

    .line 182
    .line 183
    :pswitch_4
    iget-object v14, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 184
    .line 185
    sget-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 186
    .line 187
    const/16 v16, 0x0

    .line 188
    .line 189
    const/16 v17, 0x0

    .line 190
    .line 191
    const/16 v18, 0x0

    .line 192
    .line 193
    const/16 v19, 0x0

    .line 194
    .line 195
    const/16 v20, 0x0

    .line 196
    .line 197
    const/16 v21, 0x0

    .line 198
    .line 199
    const/16 v22, 0x7e

    .line 200
    .line 201
    const/16 v23, 0x0

    .line 202
    .line 203
    invoke-static/range {v14 .. v23}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 204
    .line 205
    .line 206
    goto/16 :goto_3

    .line 207
    .line 208
    :pswitch_5
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 209
    .line 210
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 211
    .line 212
    const/4 v6, 0x0

    .line 213
    const/4 v7, 0x0

    .line 214
    const/4 v8, 0x0

    .line 215
    const/4 v9, 0x0

    .line 216
    const/4 v10, 0x0

    .line 217
    const/4 v11, 0x0

    .line 218
    const/16 v12, 0x7e

    .line 219
    .line 220
    const/4 v13, 0x0

    .line 221
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 222
    .line 223
    .line 224
    goto/16 :goto_3

    .line 225
    .line 226
    :pswitch_6
    iget-object v14, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 227
    .line 228
    sget-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 229
    .line 230
    const/16 v16, 0x0

    .line 231
    .line 232
    const/16 v17, 0x0

    .line 233
    .line 234
    const/16 v18, 0x0

    .line 235
    .line 236
    const/16 v19, 0x0

    .line 237
    .line 238
    const/16 v20, 0x0

    .line 239
    .line 240
    const/16 v21, 0x0

    .line 241
    .line 242
    const/16 v22, 0x7e

    .line 243
    .line 244
    const/16 v23, 0x0

    .line 245
    .line 246
    invoke-static/range {v14 .. v23}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 247
    .line 248
    .line 249
    goto/16 :goto_3

    .line 250
    .line 251
    :pswitch_7
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 252
    .line 253
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->PPT:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 254
    .line 255
    const/4 v6, 0x0

    .line 256
    const/4 v7, 0x0

    .line 257
    const/4 v8, 0x0

    .line 258
    const/4 v9, 0x0

    .line 259
    const/4 v10, 0x0

    .line 260
    const/4 v11, 0x0

    .line 261
    const/16 v12, 0x7e

    .line 262
    .line 263
    const/4 v13, 0x0

    .line 264
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 265
    .line 266
    .line 267
    goto/16 :goto_3

    .line 268
    .line 269
    :pswitch_8
    iget-object v14, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 270
    .line 271
    sget-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 272
    .line 273
    const/16 v16, 0x0

    .line 274
    .line 275
    const/16 v17, 0x0

    .line 276
    .line 277
    const/16 v18, 0x0

    .line 278
    .line 279
    const/16 v19, 0x0

    .line 280
    .line 281
    const/16 v20, 0x0

    .line 282
    .line 283
    const/16 v21, 0x0

    .line 284
    .line 285
    const/16 v22, 0x7e

    .line 286
    .line 287
    const/16 v23, 0x0

    .line 288
    .line 289
    invoke-static/range {v14 .. v23}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 290
    .line 291
    .line 292
    goto/16 :goto_3

    .line 293
    .line 294
    :pswitch_9
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 295
    .line 296
    sget-object v5, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 297
    .line 298
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇000()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 299
    .line 300
    .line 301
    move-result-object v6

    .line 302
    sget-object v7, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 303
    .line 304
    const/4 v8, 0x1

    .line 305
    const/4 v9, 0x0

    .line 306
    const/4 v10, 0x0

    .line 307
    const/4 v11, 0x0

    .line 308
    const/16 v12, 0x70

    .line 309
    .line 310
    const/4 v13, 0x0

    .line 311
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 312
    .line 313
    .line 314
    goto :goto_3

    .line 315
    :pswitch_a
    const-string v1, "CSTaskCenter"

    .line 316
    .line 317
    const-string v2, "web_login"

    .line 318
    .line 319
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    .line 321
    .line 322
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 323
    .line 324
    .line 325
    move-result-object v1

    .line 326
    sget-object v2, Lcom/intsig/camscanner/web/PARAMATER_KEY;->logAgent:Lcom/intsig/camscanner/web/PARAMATER_KEY;

    .line 327
    .line 328
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    .line 330
    .line 331
    move-result-object v1

    .line 332
    check-cast v1, Ljava/lang/String;

    .line 333
    .line 334
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 335
    .line 336
    sget-object v2, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 337
    .line 338
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 339
    .line 340
    .line 341
    move-result v2

    .line 342
    if-eqz v2, :cond_2

    .line 343
    .line 344
    sget-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 345
    .line 346
    goto :goto_1

    .line 347
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 348
    .line 349
    :goto_1
    move-object v5, v2

    .line 350
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 351
    .line 352
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇000()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 353
    .line 354
    .line 355
    move-result-object v6

    .line 356
    sget-object v7, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_QR_CODE_ONLY:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 357
    .line 358
    const/4 v8, 0x0

    .line 359
    const-string v2, "taskCenter"

    .line 360
    .line 361
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 362
    .line 363
    .line 364
    move-result v1

    .line 365
    if-eqz v1, :cond_3

    .line 366
    .line 367
    const v1, 0x138d7

    .line 368
    .line 369
    .line 370
    const v9, 0x138d7

    .line 371
    .line 372
    .line 373
    goto :goto_2

    .line 374
    :cond_3
    const v1, 0x138d0

    .line 375
    .line 376
    .line 377
    const v9, 0x138d0

    .line 378
    .line 379
    .line 380
    :goto_2
    const/4 v10, 0x0

    .line 381
    const/4 v11, 0x0

    .line 382
    const/16 v12, 0x60

    .line 383
    .line 384
    const/4 v13, 0x0

    .line 385
    invoke-static/range {v4 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 386
    .line 387
    .line 388
    goto :goto_3

    .line 389
    :pswitch_b
    iget-object v14, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 390
    .line 391
    sget-object v15, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 392
    .line 393
    const/16 v16, 0x0

    .line 394
    .line 395
    const/16 v17, 0x0

    .line 396
    .line 397
    const/16 v18, 0x0

    .line 398
    .line 399
    const/16 v19, 0x0

    .line 400
    .line 401
    const/16 v20, 0x0

    .line 402
    .line 403
    const/16 v21, 0x0

    .line 404
    .line 405
    const/16 v22, 0x7e

    .line 406
    .line 407
    const/16 v23, 0x0

    .line 408
    .line 409
    invoke-static/range {v14 .. v23}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 410
    .line 411
    .line 412
    :goto_3
    return v3

    .line 413
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const/4 v0, -0x1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl$WhenMappings;->〇o00〇〇Oo:[I

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    aget v0, v1, v0

    .line 16
    .line 17
    :goto_0
    const/4 v1, 0x1

    .line 18
    if-eq v0, v1, :cond_5

    .line 19
    .line 20
    const/4 v2, 0x2

    .line 21
    if-eq v0, v2, :cond_3

    .line 22
    .line 23
    const/4 p1, 0x3

    .line 24
    if-eq v0, p1, :cond_2

    .line 25
    .line 26
    const/4 p1, 0x4

    .line 27
    if-eq v0, p1, :cond_1

    .line 28
    .line 29
    const/4 v1, 0x0

    .line 30
    goto :goto_1

    .line 31
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string v0, "fold module create by web"

    .line 38
    .line 39
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    sget-object p2, Lcom/intsig/camscanner/web/PARAMATER_KEY;->tpl_id:Lcom/intsig/camscanner/web/PARAMATER_KEY;

    .line 47
    .line 48
    invoke-virtual {p1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    check-cast p1, Ljava/lang/String;

    .line 53
    .line 54
    if-eqz p1, :cond_7

    .line 55
    .line 56
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 57
    .line 58
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO〇O0O(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    const-string p2, "create folder by web"

    .line 69
    .line 70
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_1

    .line 74
    :cond_3
    invoke-static {p1}, Lcom/intsig/camscanner/app/DBUtil;->O8oOo80(Landroid/content/Context;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 79
    .line 80
    .line 81
    move-result p2

    .line 82
    if-eqz p2, :cond_4

    .line 83
    .line 84
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    const-string p2, "current account has not offline folder"

    .line 91
    .line 92
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_4
    invoke-static {p1}, Lcom/intsig/camscanner/app/DBUtil;->Oo0oOo〇0(Landroid/content/Context;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    if-eqz p1, :cond_7

    .line 101
    .line 102
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 103
    .line 104
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8ooOO(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    goto :goto_1

    .line 112
    :cond_5
    const-string p2, "dir_mycard"

    .line 113
    .line 114
    invoke-static {p1, p2}, Lcom/intsig/camscanner/app/DBUtil;->〇o8OO0(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    if-nez p1, :cond_6

    .line 119
    .line 120
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 121
    .line 122
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object p1

    .line 126
    const-string p2, "current account has not certification folder"

    .line 127
    .line 128
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    goto :goto_1

    .line 132
    :cond_6
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 133
    .line 134
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8ooOO(Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    :cond_7
    :goto_1
    return v1
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 4
    .param p1    # Lcom/intsig/camscanner/web/UrlEntity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "urlEntity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "doNativeAction"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->o〇0()Lcom/intsig/camscanner/web/MODULE;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const/4 v3, 0x0

    .line 32
    if-eqz v1, :cond_3

    .line 33
    .line 34
    if-nez v2, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl$WhenMappings;->〇080:[I

    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    aget v1, v2, v1

    .line 44
    .line 45
    const/4 v2, 0x1

    .line 46
    if-eq v1, v2, :cond_2

    .line 47
    .line 48
    const/4 v2, 0x2

    .line 49
    if-eq v1, v2, :cond_1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/web/UrlEntity;)Z

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    goto :goto_0

    .line 57
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;->〇o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/web/UrlEntity;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    :cond_3
    :goto_0
    return v3
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;
.super Ljava/lang/Object;
.source "VipIconManager.kt"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SwitchIntDef"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oO80:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:I

.field private Oo08:Z

.field private o〇0:Z

.field private final 〇080:Lcom/airbnb/lottie/LottieAnimationView;

.field private final 〇o00〇〇Oo:Landroid/widget/TextView;

.field private 〇o〇:Z

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->oO80:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/airbnb/lottie/LottieAnimationView;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO0o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x18

    .line 6
    .line 7
    packed-switch v0, :pswitch_data_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :pswitch_0
    const/16 v0, 0x30

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-static {v0}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇8o8o〇(I)V

    .line 22
    .line 23
    .line 24
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-static {v0}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->oO80(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :pswitch_1
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    invoke-static {v0}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇8o8o〇(I)V

    .line 45
    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    invoke-static {v0}, Lkotlin/math/MathKt;->〇o00〇〇Oo(F)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->oO80(I)V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void

    .line 59
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oooo8o0〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OoO8(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇080()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o00〇〇Oo:Landroid/widget/TextView;

    .line 21
    .line 22
    if-eqz v0, :cond_2

    .line 23
    .line 24
    const/4 v1, 0x1

    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    new-instance p1, L〇080OO8〇0/〇o00〇〇Oo;

    .line 32
    .line 33
    invoke-direct {p1, v0}, L〇080OO8〇0/〇o00〇〇Oo;-><init>(Landroid/widget/TextView;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 37
    .line 38
    .line 39
    :cond_2
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final Oooo8o0〇(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o800o8O(Landroid/widget/TextView;)V
    .locals 2

    .line 1
    const-string v0, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/widget/TextView;->getLineCount()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x2

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/high16 v0, 0x40e00000    # 7.0f

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    int-to-float v0, v0

    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {p0, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o〇0(I)Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;

    .line 2
    .line 3
    const/4 v2, 0x0

    .line 4
    const/4 v3, 0x0

    .line 5
    const/4 v4, 0x0

    .line 6
    const/4 v5, 0x0

    .line 7
    const/4 v6, 0x0

    .line 8
    const/16 v7, 0x3e

    .line 9
    .line 10
    const/4 v8, 0x0

    .line 11
    move-object v0, v9

    .line 12
    move v1, p1

    .line 13
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;-><init>(IIIIILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v9}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->OO0o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, v9}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v9}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇8o8o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 23
    .line 24
    .line 25
    return-object v9
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇080(Landroid/widget/TextView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o800o8O(Landroid/widget/TextView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x13

    .line 6
    .line 7
    const-string v2, "VipIconManager"

    .line 8
    .line 9
    if-ne v0, v1, :cond_2

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->web_premium_outline_style_3:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebOutlineProducts;

    .line 20
    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const v3, 0x7f1318b5

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const-string v4, "csApplication.getString(R.string.cs_647_icon3)"

    .line 36
    .line 37
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :try_start_0
    const-string v4, "%s"

    .line 41
    .line 42
    const/4 v5, 0x2

    .line 43
    const/4 v6, 0x0

    .line 44
    const/4 v7, 0x0

    .line 45
    invoke-static {v1, v4, v7, v5, v6}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-eqz v1, :cond_1

    .line 50
    .line 51
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const/4 v4, 0x1

    .line 56
    new-array v4, v4, [Ljava/lang/Object;

    .line 57
    .line 58
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WebOutlineProducts;->main_save_percent:I

    .line 59
    .line 60
    new-instance v5, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    const-string v0, "%"

    .line 69
    .line 70
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    aput-object v0, v4, v7

    .line 78
    .line 79
    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    const-string v1, "csApplication.getString(\u2026cts.main_save_percent}%\")"

    .line 84
    .line 85
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    :cond_1
    const-string v0, ""

    .line 98
    .line 99
    :goto_0
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇〇888(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇080()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    .line 107
    .line 108
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .line 110
    .line 111
    const-string v1, "set description : "

    .line 112
    .line 113
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final 〇O00(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$this_run"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇888:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oo08:Z

    .line 16
    .line 17
    if-nez p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O888o0o(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "update lottie\uff1a "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "VipIconManager"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇808〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇O〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->OoO8(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f12002f

    .line 6
    .line 7
    .line 8
    const v2, 0x7f0808af

    .line 9
    .line 10
    .line 11
    const v3, 0x7f12002b

    .line 12
    .line 13
    .line 14
    const v4, 0x7f0808ab

    .line 15
    .line 16
    .line 17
    packed-switch v0, :pswitch_data_0

    .line 18
    .line 19
    .line 20
    goto/16 :goto_0

    .line 21
    .line 22
    :pswitch_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->oO80()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇80〇808〇O()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 36
    .line 37
    .line 38
    goto/16 :goto_0

    .line 39
    .line 40
    :pswitch_1
    const v0, 0x7f080689

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 44
    .line 45
    .line 46
    goto/16 :goto_0

    .line 47
    .line 48
    :pswitch_2
    const v0, 0x7f080da1

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 52
    .line 53
    .line 54
    const v0, 0x7f120026

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 58
    .line 59
    .line 60
    goto/16 :goto_0

    .line 61
    .line 62
    :pswitch_3
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 66
    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :pswitch_4
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 74
    .line 75
    .line 76
    goto/16 :goto_0

    .line 77
    .line 78
    :pswitch_5
    const v0, 0x7f0808b0

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 82
    .line 83
    .line 84
    const v0, 0x7f120030

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_0

    .line 91
    .line 92
    :pswitch_6
    const v0, 0x7f0808b1

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 96
    .line 97
    .line 98
    const v0, 0x7f120031

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :pswitch_7
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :pswitch_8
    const v0, 0x7f0808ad

    .line 113
    .line 114
    .line 115
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 116
    .line 117
    .line 118
    const v0, 0x7f12002d

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 122
    .line 123
    .line 124
    goto :goto_0

    .line 125
    :pswitch_9
    const v0, 0x7f0808ac

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 129
    .line 130
    .line 131
    const v0, 0x7f12002c

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :pswitch_a
    const v0, 0x7f0808ae

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 142
    .line 143
    .line 144
    const v0, 0x7f12002e

    .line 145
    .line 146
    .line 147
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 148
    .line 149
    .line 150
    goto :goto_0

    .line 151
    :pswitch_b
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 152
    .line 153
    .line 154
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 155
    .line 156
    .line 157
    goto :goto_0

    .line 158
    :pswitch_c
    const v0, 0x7f0808a9

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 162
    .line 163
    .line 164
    const v0, 0x7f120029

    .line 165
    .line 166
    .line 167
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 168
    .line 169
    .line 170
    goto :goto_0

    .line 171
    :pswitch_d
    const v0, 0x7f0808a8

    .line 172
    .line 173
    .line 174
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 175
    .line 176
    .line 177
    const v0, 0x7f120028

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 181
    .line 182
    .line 183
    goto :goto_0

    .line 184
    :pswitch_e
    const v0, 0x7f0808aa

    .line 185
    .line 186
    .line 187
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 188
    .line 189
    .line 190
    const v0, 0x7f12002a

    .line 191
    .line 192
    .line 193
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 194
    .line 195
    .line 196
    goto :goto_0

    .line 197
    :pswitch_f
    const v0, 0x7f0808a7

    .line 198
    .line 199
    .line 200
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇80〇808〇O(I)V

    .line 201
    .line 202
    .line 203
    const v0, 0x7f120027

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->OO0o〇〇〇〇0(I)V

    .line 207
    .line 208
    .line 209
    :goto_0
    return-void

    .line 210
    nop

    .line 211
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇O〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "show lottie\uff1a"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "VipIconManager"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->Oo08()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_0

    .line 32
    .line 33
    const-string p1, "show lottie fail, no resource"

    .line 34
    .line 35
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 40
    .line 41
    const/16 v2, 0x17

    .line 42
    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eq v2, v0, :cond_1

    .line 50
    .line 51
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇0000OOO()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-nez v0, :cond_2

    .line 56
    .line 57
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->O8:I

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 60
    .line 61
    .line 62
    move-result v3

    .line 63
    if-ne v0, v3, :cond_2

    .line 64
    .line 65
    const-string p1, "lottie is showing"

    .line 66
    .line 67
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return-void

    .line 71
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    iput v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->O8:I

    .line 76
    .line 77
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    const-string v3, "hasShowVipIconLottie, use common lottie "

    .line 82
    .line 83
    if-ne v2, v0, :cond_4

    .line 84
    .line 85
    sget-object v0, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->Oo8Oo00oo()Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-eqz v0, :cond_3

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇0O〇(I)Z

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    if-eqz v0, :cond_5

    .line 102
    .line 103
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 104
    .line 105
    .line 106
    move-result v0

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    .line 108
    .line 109
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇o〇()I

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oooo8o0〇(I)V

    .line 130
    .line 131
    .line 132
    return-void

    .line 133
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0888()Z

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    if-eqz v0, :cond_5

    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->O8()I

    .line 140
    .line 141
    .line 142
    move-result v0

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    .line 144
    .line 145
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇o〇()I

    .line 162
    .line 163
    .line 164
    move-result p1

    .line 165
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oooo8o0〇(I)V

    .line 166
    .line 167
    .line 168
    return-void

    .line 169
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 170
    .line 171
    if-eqz v0, :cond_6

    .line 172
    .line 173
    const-string v2, "prepare lottie"

    .line 174
    .line 175
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇o〇()I

    .line 179
    .line 180
    .line 181
    move-result v1

    .line 182
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oooo8o0〇(I)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->Oo08()I

    .line 186
    .line 187
    .line 188
    move-result v1

    .line 189
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 190
    .line 191
    .line 192
    const/4 v1, 0x1

    .line 193
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    .line 194
    .line 195
    .line 196
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$updateLottie$1$1;

    .line 197
    .line 198
    invoke-direct {v2, p1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager$updateLottie$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;)V

    .line 199
    .line 200
    .line 201
    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/LottieAnimationView;->O8(Landroid/animation/Animator$AnimatorListener;)V

    .line 202
    .line 203
    .line 204
    new-instance p1, L〇080OO8〇0/〇080;

    .line 205
    .line 206
    invoke-direct {p1, p0, v0}, L〇080OO8〇0/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Lcom/airbnb/lottie/LottieAnimationView;)V

    .line 207
    .line 208
    .line 209
    const-wide/16 v2, 0x1f4

    .line 210
    .line 211
    invoke-virtual {v0, p1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 212
    .line 213
    .line 214
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 215
    .line 216
    :cond_6
    return-void
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇O00(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Lcom/airbnb/lottie/LottieAnimationView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇808〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->o〇0()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇o00〇〇Oo()I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->OoO8(Landroid/view/View;II)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇808〇(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;->〇o〇()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oooo8o0〇(I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final OO0o〇〇〇〇0()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇888:Z

    .line 3
    .line 4
    const-string v0, "onResume"

    .line 5
    .line 6
    const-string v1, "VipIconManager"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iget-boolean v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 16
    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    iget-boolean v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oo08:Z

    .line 20
    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    const-string v2, "resume anim"

    .line 24
    .line 25
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o〇0:Z

    .line 29
    .line 30
    if-eqz v1, :cond_0

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇〇8O0〇8()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 37
    .line 38
    .line 39
    :goto_0
    const/4 v0, 0x0

    .line 40
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oo08:Z

    .line 41
    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oO80()V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇888:Z

    .line 3
    .line 4
    const-string v0, "onPause"

    .line 5
    .line 6
    const-string v1, "VipIconManager"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-boolean v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 16
    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    const-string v2, "pause anim"

    .line 20
    .line 21
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇〇808〇()V

    .line 25
    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->Oo08:Z

    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇0〇O0088o(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-nez v0, :cond_5

    .line 16
    .line 17
    const/4 v0, -0x1

    .line 18
    if-ne p1, v0, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 22
    .line 23
    const/4 v2, 0x1

    .line 24
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 28
    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    if-eqz p1, :cond_4

    .line 37
    .line 38
    const/4 v0, 0x2

    .line 39
    const v2, 0x7f0812f6

    .line 40
    .line 41
    .line 42
    if-eq p1, v0, :cond_3

    .line 43
    .line 44
    const/4 v0, 0x3

    .line 45
    if-eq p1, v0, :cond_2

    .line 46
    .line 47
    packed-switch p1, :pswitch_data_0

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o〇0(I)Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇8O0〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o〇0(I)Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇O888o0o(Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconStyleModel;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :pswitch_2
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 68
    .line 69
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 74
    .line 75
    invoke-virtual {p1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    const-string p1, "CSMain"

    .line 80
    .line 81
    const-string v0, "renew_education_show"

    .line 82
    .line 83
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 87
    .line 88
    invoke-virtual {p1, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 93
    .line 94
    const v0, 0x7f0812f5

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 98
    .line 99
    .line 100
    :goto_0
    return-void

    .line 101
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 102
    .line 103
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 104
    .line 105
    .line 106
    const-string p1, "VipIconManager"

    .line 107
    .line 108
    const-string v0, "special account or edu account, do not show the icon"

    .line 109
    .line 110
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    return-void

    .line 114
    nop

    .line 115
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇80〇808〇O()V
    .locals 2

    .line 1
    const-string v0, "VipIconManager"

    .line 2
    .line 3
    const-string v1, "\u91ca\u653elottie\u52a8\u753b"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇o〇:Z

    .line 10
    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->o〇0:Z

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇080:Lcom/airbnb/lottie/LottieAnimationView;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇〇888()V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
.end method

.method public final 〇〇888(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pageId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇o〇()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const/4 p1, -0x1

    .line 13
    goto/16 :goto_1

    .line 14
    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇o〇()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    const/16 p1, 0x19

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->o8()Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    const/16 p1, 0x17

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇080()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    const/16 p1, 0x18

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o〇8()Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-eqz v0, :cond_4

    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->O8ooOoo〇()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    goto :goto_1

    .line 59
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇o〇()Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_5

    .line 64
    .line 65
    const/16 p1, 0x9

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->Oo8Oo00oo()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    const/4 v1, 0x2

    .line 73
    const-string v2, "type"

    .line 74
    .line 75
    const-string v3, "vip_icon_show"

    .line 76
    .line 77
    if-eqz v0, :cond_6

    .line 78
    .line 79
    const-string v0, "discount"

    .line 80
    .line 81
    invoke-static {p1, v3, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    :goto_0
    const/4 p1, 0x2

    .line 85
    goto :goto_1

    .line 86
    :cond_6
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->O8ooOoo〇()Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    const-string v4, "24or48_activity"

    .line 91
    .line 92
    if-eqz v0, :cond_7

    .line 93
    .line 94
    invoke-static {p1, v3, v2, v4}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-nez v0, :cond_8

    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/camscanner/purchase/FavorableManager;->〇080()Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    if-eqz v0, :cond_8

    .line 109
    .line 110
    invoke-static {p1, v3, v2, v4}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    const/4 p1, 0x3

    .line 114
    goto :goto_1

    .line 115
    :cond_8
    const-string v0, "normal_vip"

    .line 116
    .line 117
    invoke-static {p1, v3, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    const/4 p1, 0x0

    .line 121
    :goto_1
    return p1
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

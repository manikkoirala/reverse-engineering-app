.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;
.super Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction;
.source "MainActViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ESignLinkQueryAction"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Ljava/lang/String;

.field private final 〇080:Lcom/intsig/utils/CsResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/newsign/data/sync/ESignLinkQueryRes;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/lang/String;

.field private final 〇o〇:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/intsig/utils/CsResult;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/newsign/data/sync/ESignLinkQueryRes;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    .line 1
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;-><init>(Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final O8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 14
    .line 15
    iget-object v3, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 16
    .line 17
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 25
    .line 26
    iget-object v3, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-nez v1, :cond_3

    .line 33
    .line 34
    return v2

    .line 35
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 36
    .line 37
    iget-object v3, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 38
    .line 39
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-nez v1, :cond_4

    .line 44
    .line 45
    return v2

    .line 46
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    .line 47
    .line 48
    iget-object p1, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    .line 49
    .line 50
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-nez p1, :cond_5

    .line 55
    .line 56
    return v2

    .line 57
    :cond_5
    return v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    mul-int/lit8 v0, v0, 0x1f

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    :goto_0
    add-int/2addr v0, v1

    .line 21
    mul-int/lit8 v0, v0, 0x1f

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 24
    .line 25
    if-nez v1, :cond_1

    .line 26
    .line 27
    const/4 v1, 0x0

    .line 28
    goto :goto_1

    .line 29
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    :goto_1
    add-int/2addr v0, v1

    .line 34
    mul-int/lit8 v0, v0, 0x1f

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    .line 37
    .line 38
    if-nez v1, :cond_2

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    :goto_2
    add-int/2addr v0, v2

    .line 46
    return v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public toString()Ljava/lang/String;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->O8:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v4, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v5, "ESignLinkQueryAction(result="

    .line 15
    .line 16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v0, ", encryptId="

    .line 23
    .line 24
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v0, ", sid="

    .line 31
    .line 32
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    const-string v0, ", url="

    .line 39
    .line 40
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    const-string v0, ")"

    .line 47
    .line 48
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()Lcom/intsig/utils/CsResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/newsign/data/sync/ESignLinkQueryRes;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇080:Lcom/intsig/utils/CsResult;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryAction;->〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

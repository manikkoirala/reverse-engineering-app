.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;
.super Ljava/lang/Object;
.source "MainBtmBarController.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;,
        Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇〇〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8o8o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO80:Landroid/widget/EditText;

.field private o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

.field private final 〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Landroidx/viewpager2/widget/ViewPager2;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇888:Lcom/intsig/camscanner/launch/CsApplication;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "MainBtmBarController::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;Landroidx/viewpager2/widget/ViewPager2;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Landroidx/viewpager2/widget/ViewPager2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "editBtmBar"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "normalBtmBar"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "viewpager2"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "mainActivity"

    .line 17
    .line 18
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "mFragment"

    .line 22
    .line 23
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 30
    .line 31
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 32
    .line 33
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o〇:Landroidx/viewpager2/widget/ViewPager2;

    .line 34
    .line 35
    iput-object p4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    iput-object p5, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 38
    .line 39
    sget-object p1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇〇888:Lcom/intsig/camscanner/launch/CsApplication;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o8O〇()V

    .line 48
    .line 49
    .line 50
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 51
    .line 52
    new-instance p2, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$mExtractImageLoading$2;

    .line 53
    .line 54
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$mExtractImageLoading$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 55
    .line 56
    .line 57
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇80〇808〇O:Lkotlin/Lazy;

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final O000()Ljava/util/ArrayList;
    .locals 22
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x7

    .line 4
    new-array v2, v1, [Ljava/lang/Integer;

    .line 5
    .line 6
    const/16 v3, 0x18

    .line 7
    .line 8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    const/4 v4, 0x0

    .line 13
    aput-object v3, v2, v4

    .line 14
    .line 15
    const/16 v5, 0x10

    .line 16
    .line 17
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v5

    .line 21
    const/4 v6, 0x1

    .line 22
    aput-object v5, v2, v6

    .line 23
    .line 24
    const/16 v7, 0x11

    .line 25
    .line 26
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object v7

    .line 30
    const/4 v8, 0x2

    .line 31
    aput-object v7, v2, v8

    .line 32
    .line 33
    const/16 v9, 0x12

    .line 34
    .line 35
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v9

    .line 39
    const/4 v10, 0x3

    .line 40
    aput-object v9, v2, v10

    .line 41
    .line 42
    const/16 v11, 0x13

    .line 43
    .line 44
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 45
    .line 46
    .line 47
    move-result-object v11

    .line 48
    const/4 v12, 0x4

    .line 49
    aput-object v11, v2, v12

    .line 50
    .line 51
    const/16 v11, 0x14

    .line 52
    .line 53
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 54
    .line 55
    .line 56
    move-result-object v11

    .line 57
    const/4 v13, 0x5

    .line 58
    aput-object v11, v2, v13

    .line 59
    .line 60
    const/16 v11, 0x15

    .line 61
    .line 62
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 63
    .line 64
    .line 65
    move-result-object v11

    .line 66
    const/4 v14, 0x6

    .line 67
    aput-object v11, v2, v14

    .line 68
    .line 69
    new-array v15, v1, [Ljava/lang/Integer;

    .line 70
    .line 71
    const v16, 0x7f131348

    .line 72
    .line 73
    .line 74
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 75
    .line 76
    .line 77
    move-result-object v16

    .line 78
    aput-object v16, v15, v4

    .line 79
    .line 80
    const v16, 0x7f1302e6

    .line 81
    .line 82
    .line 83
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 84
    .line 85
    .line 86
    move-result-object v16

    .line 87
    aput-object v16, v15, v6

    .line 88
    .line 89
    const v16, 0x7f1302ea

    .line 90
    .line 91
    .line 92
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v16

    .line 96
    aput-object v16, v15, v8

    .line 97
    .line 98
    const v16, 0x7f130133

    .line 99
    .line 100
    .line 101
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v16

    .line 105
    aput-object v16, v15, v10

    .line 106
    .line 107
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 108
    .line 109
    .line 110
    move-result-object v16

    .line 111
    invoke-virtual/range {v16 .. v16}, Lcom/intsig/tsapp/sync/AppConfigJson;->isSendFaxOn()Z

    .line 112
    .line 113
    .line 114
    move-result v16

    .line 115
    if-eqz v16, :cond_0

    .line 116
    .line 117
    const v16, 0x7f13022a

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_0
    const v16, 0x7f130541

    .line 122
    .line 123
    .line 124
    :goto_0
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 125
    .line 126
    .line 127
    move-result-object v16

    .line 128
    aput-object v16, v15, v12

    .line 129
    .line 130
    const v16, 0x7f13033e

    .line 131
    .line 132
    .line 133
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 134
    .line 135
    .line 136
    move-result-object v16

    .line 137
    aput-object v16, v15, v13

    .line 138
    .line 139
    const v16, 0x7f131dbc

    .line 140
    .line 141
    .line 142
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 143
    .line 144
    .line 145
    move-result-object v16

    .line 146
    aput-object v16, v15, v14

    .line 147
    .line 148
    new-array v1, v1, [Ljava/lang/Integer;

    .line 149
    .line 150
    const v16, 0x7f0806ef

    .line 151
    .line 152
    .line 153
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 154
    .line 155
    .line 156
    move-result-object v16

    .line 157
    aput-object v16, v1, v4

    .line 158
    .line 159
    const v16, 0x7f080762

    .line 160
    .line 161
    .line 162
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 163
    .line 164
    .line 165
    move-result-object v17

    .line 166
    aput-object v17, v1, v6

    .line 167
    .line 168
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 169
    .line 170
    .line 171
    move-result-object v16

    .line 172
    aput-object v16, v1, v8

    .line 173
    .line 174
    const v8, 0x7f08093c

    .line 175
    .line 176
    .line 177
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 178
    .line 179
    .line 180
    move-result-object v8

    .line 181
    aput-object v8, v1, v10

    .line 182
    .line 183
    const v8, 0x7f080d73

    .line 184
    .line 185
    .line 186
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 187
    .line 188
    .line 189
    move-result-object v8

    .line 190
    aput-object v8, v1, v12

    .line 191
    .line 192
    const v8, 0x7f080749

    .line 193
    .line 194
    .line 195
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 196
    .line 197
    .line 198
    move-result-object v8

    .line 199
    aput-object v8, v1, v13

    .line 200
    .line 201
    const v8, 0x7f0804e5

    .line 202
    .line 203
    .line 204
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 205
    .line 206
    .line 207
    move-result-object v8

    .line 208
    aput-object v8, v1, v14

    .line 209
    .line 210
    new-instance v8, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;

    .line 211
    .line 212
    const/16 v17, 0x0

    .line 213
    .line 214
    const/16 v18, 0x0

    .line 215
    .line 216
    const/16 v19, 0x0

    .line 217
    .line 218
    const/16 v20, 0x7

    .line 219
    .line 220
    const/16 v21, 0x0

    .line 221
    .line 222
    move-object/from16 v16, v8

    .line 223
    .line 224
    invoke-direct/range {v16 .. v21}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;-><init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v8, v2, v15, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->tansTabList([Ljava/lang/Integer;[Ljava/lang/Integer;[Ljava/lang/Integer;)Ljava/util/ArrayList;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    new-instance v2, Ljava/util/ArrayList;

    .line 232
    .line 233
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 234
    .line 235
    .line 236
    iget-object v8, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 237
    .line 238
    const/4 v10, 0x0

    .line 239
    const-string v12, "mMainBottomListener"

    .line 240
    .line 241
    if-nez v8, :cond_1

    .line 242
    .line 243
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    move-object v8, v10

    .line 247
    :cond_1
    invoke-interface {v8}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇8o8o〇()Ljava/util/Set;

    .line 248
    .line 249
    .line 250
    move-result-object v8

    .line 251
    iget-object v13, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 252
    .line 253
    if-nez v13, :cond_2

    .line 254
    .line 255
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    move-object v13, v10

    .line 259
    :cond_2
    invoke-interface {v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 260
    .line 261
    .line 262
    move-result-object v13

    .line 263
    instance-of v13, v13, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 264
    .line 265
    if-eqz v13, :cond_4

    .line 266
    .line 267
    iget-object v13, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 268
    .line 269
    if-nez v13, :cond_3

    .line 270
    .line 271
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 272
    .line 273
    .line 274
    move-object v13, v10

    .line 275
    :cond_3
    invoke-interface {v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->o〇0()Z

    .line 276
    .line 277
    .line 278
    move-result v13

    .line 279
    if-eqz v13, :cond_5

    .line 280
    .line 281
    :cond_4
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    :cond_5
    invoke-interface {v8}, Ljava/util/Set;->size()I

    .line 285
    .line 286
    .line 287
    move-result v3

    .line 288
    if-le v3, v6, :cond_6

    .line 289
    .line 290
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    .line 292
    .line 293
    :cond_6
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 294
    .line 295
    if-nez v3, :cond_7

    .line 296
    .line 297
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 298
    .line 299
    .line 300
    goto :goto_1

    .line 301
    :cond_7
    move-object v10, v3

    .line 302
    :goto_1
    invoke-interface {v10}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇o00〇〇Oo()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 303
    .line 304
    .line 305
    move-result-object v3

    .line 306
    if-eqz v3, :cond_8

    .line 307
    .line 308
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇8()Z

    .line 309
    .line 310
    .line 311
    move-result v10

    .line 312
    if-ne v10, v6, :cond_8

    .line 313
    .line 314
    const/4 v10, 0x1

    .line 315
    goto :goto_2

    .line 316
    :cond_8
    const/4 v10, 0x0

    .line 317
    :goto_2
    if-eqz v10, :cond_9

    .line 318
    .line 319
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    .line 321
    .line 322
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    .line 324
    .line 325
    goto :goto_4

    .line 326
    :cond_9
    iget-object v10, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 327
    .line 328
    invoke-virtual {v10}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 329
    .line 330
    .line 331
    move-result-object v10

    .line 332
    check-cast v8, Ljava/lang/Iterable;

    .line 333
    .line 334
    new-instance v11, Ljava/util/ArrayList;

    .line 335
    .line 336
    const/16 v12, 0xa

    .line 337
    .line 338
    invoke-static {v8, v12}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 339
    .line 340
    .line 341
    move-result v12

    .line 342
    invoke-direct {v11, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 343
    .line 344
    .line 345
    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 346
    .line 347
    .line 348
    move-result-object v8

    .line 349
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    .line 350
    .line 351
    .line 352
    move-result v12

    .line 353
    if-eqz v12, :cond_a

    .line 354
    .line 355
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    move-result-object v12

    .line 359
    check-cast v12, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 360
    .line 361
    invoke-virtual {v12}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 362
    .line 363
    .line 364
    move-result-wide v12

    .line 365
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 366
    .line 367
    .line 368
    move-result-object v12

    .line 369
    invoke-interface {v11, v12}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 370
    .line 371
    .line 372
    goto :goto_3

    .line 373
    :cond_a
    invoke-static {v10, v11}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇8〇0〇o〇O(Landroid/content/Context;Ljava/util/List;)Z

    .line 374
    .line 375
    .line 376
    move-result v8

    .line 377
    if-eqz v8, :cond_b

    .line 378
    .line 379
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    .line 381
    .line 382
    goto :goto_4

    .line 383
    :cond_b
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    .line 385
    .line 386
    :goto_4
    if-eqz v3, :cond_c

    .line 387
    .line 388
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/FolderItem;->oO()Z

    .line 389
    .line 390
    .line 391
    move-result v3

    .line 392
    if-ne v3, v6, :cond_c

    .line 393
    .line 394
    const/4 v4, 0x1

    .line 395
    :cond_c
    if-eqz v4, :cond_d

    .line 396
    .line 397
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    .line 399
    .line 400
    :cond_d
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 401
    .line 402
    .line 403
    move-result-object v3

    .line 404
    const-string v4, "tansTabList.iterator()"

    .line 405
    .line 406
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 407
    .line 408
    .line 409
    :cond_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 410
    .line 411
    .line 412
    move-result v4

    .line 413
    if-eqz v4, :cond_10

    .line 414
    .line 415
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 416
    .line 417
    .line 418
    move-result-object v4

    .line 419
    const-string v5, "tansListIterator.next()"

    .line 420
    .line 421
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    check-cast v4, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;

    .line 425
    .line 426
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 427
    .line 428
    .line 429
    move-result-object v5

    .line 430
    :cond_f
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 431
    .line 432
    .line 433
    move-result v6

    .line 434
    if-eqz v6, :cond_e

    .line 435
    .line 436
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 437
    .line 438
    .line 439
    move-result-object v6

    .line 440
    check-cast v6, Ljava/lang/Number;

    .line 441
    .line 442
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 443
    .line 444
    .line 445
    move-result v6

    .line 446
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->getMItemId()I

    .line 447
    .line 448
    .line 449
    move-result v7

    .line 450
    if-ne v6, v7, :cond_f

    .line 451
    .line 452
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 453
    .line 454
    .line 455
    goto :goto_5

    .line 456
    :cond_10
    return-object v1
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final O08000(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    if-eqz p2, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 9
    .line 10
    if-nez p2, :cond_1

    .line 11
    .line 12
    const-string p2, "mMainBottomListener"

    .line 13
    .line 14
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 p2, 0x0

    .line 18
    :cond_1
    invoke-interface {p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    instance-of p2, p2, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 23
    .line 24
    if-eqz p2, :cond_2

    .line 25
    .line 26
    const-string p2, "cs_home"

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    const-string p2, "cs_main"

    .line 30
    .line 31
    :goto_0
    const-string v0, "convert_to_pic"

    .line 32
    .line 33
    const-string v1, "from_part"

    .line 34
    .line 35
    const-string v2, "CSSelectModeMore"

    .line 36
    .line 37
    invoke-static {v2, v0, v1, p2}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 41
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Ljava/util/Collection;

    .line 49
    .line 50
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 51
    .line 52
    .line 53
    new-instance v1, LO8o08O8O/OO0o〇〇;

    .line 54
    .line 55
    invoke-direct {v1, p0, p1}, LO8o08O8O/OO0o〇〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;)V

    .line 56
    .line 57
    .line 58
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final O0O8OO088(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 2

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$docItems"

    .line 7
    .line 8
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance p3, Landroid/content/Intent;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-class v1, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 16
    .line 17
    invoke-direct {p3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "sourceFolderParentSyncId"

    .line 21
    .line 22
    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    const-string p1, "docItems"

    .line 26
    .line 27
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    const-string p1, "fromPart"

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const-string p1, "ACTION_DOCS_MOVE"

    .line 40
    .line 41
    invoke-virtual {p3, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 45
    .line 46
    const/16 p1, 0x81

    .line 47
    .line 48
    invoke-virtual {p0, p3, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final O0o〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docs"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOO(Ljava/util/List;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O8(Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo〇O(Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O8O〇(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    check-cast v1, Ljava/util/Collection;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    new-instance v2, LO8o08O8O/〇〇808〇;

    .line 15
    .line 16
    invoke-direct {v2, p0, p2, p1}, LO8o08O8O/〇〇808〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 17
    .line 18
    .line 19
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooo0〇O88O(Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O8〇o()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Ljava/lang/Iterable;

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    move-object v2, v1

    .line 22
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-static {v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 v1, 0x0

    .line 36
    :goto_0
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 37
    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    const/4 v0, 0x1

    .line 41
    goto :goto_1

    .line 42
    :cond_2
    const/4 v0, 0x0

    .line 43
    :goto_1
    return v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooo〇8oO(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final OO8oO0o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 7
    .line 8
    if-nez p0, :cond_0

    .line 9
    .line 10
    const-string p0, "mMainBottomListener"

    .line 11
    .line 12
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    :cond_0
    invoke-interface {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method private final OOO(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: share"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x1

    .line 13
    if-le v0, v1, :cond_0

    .line 14
    .line 15
    const-string v0, "batch"

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string v0, "single"

    .line 19
    .line 20
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    const-string v3, "select_list_share"

    .line 25
    .line 26
    const-string v4, "from"

    .line 27
    .line 28
    invoke-static {v2, v3, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    sget-object v0, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 32
    .line 33
    move-object v2, p1

    .line 34
    check-cast v2, Ljava/lang/Iterable;

    .line 35
    .line 36
    new-instance v3, Ljava/util/ArrayList;

    .line 37
    .line 38
    const/16 v4, 0xa

    .line 39
    .line 40
    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    .line 46
    .line 47
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 52
    .line 53
    .line 54
    move-result v5

    .line 55
    if-eqz v5, :cond_1

    .line 56
    .line 57
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v5

    .line 61
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 62
    .line 63
    iget-wide v5, v5, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 64
    .line 65
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    invoke-interface {v3, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_1
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇80〇808〇O(Ljava/util/List;)Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    const/4 v3, 0x0

    .line 78
    if-nez v0, :cond_3

    .line 79
    .line 80
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    if-ne v0, v1, :cond_2

    .line 85
    .line 86
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 97
    .line 98
    iget-wide v1, p1, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 99
    .line 100
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇〇0〇(Landroid/content/Context;J)I

    .line 101
    .line 102
    .line 103
    move-result p1

    .line 104
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 109
    .line 110
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 115
    .line 116
    const-string v0, "id_mode_batch"

    .line 117
    .line 118
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    :goto_2
    return-void

    .line 122
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    .line 123
    .line 124
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .line 126
    .line 127
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 128
    .line 129
    .line 130
    move-result-object v4

    .line 131
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 132
    .line 133
    .line 134
    move-result v5

    .line 135
    if-eqz v5, :cond_4

    .line 136
    .line 137
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 138
    .line 139
    .line 140
    move-result-object v5

    .line 141
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 142
    .line 143
    iget-wide v5, v5, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 144
    .line 145
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 146
    .line 147
    .line 148
    move-result-object v5

    .line 149
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    goto :goto_3

    .line 153
    :cond_4
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v4

    .line 157
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 158
    .line 159
    iget-object v4, v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 160
    .line 161
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 162
    .line 163
    .line 164
    move-result v4

    .line 165
    if-eqz v4, :cond_10

    .line 166
    .line 167
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 168
    .line 169
    .line 170
    move-result v4

    .line 171
    if-le v4, v1, :cond_c

    .line 172
    .line 173
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 174
    .line 175
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->OO0o〇〇〇〇0()Z

    .line 176
    .line 177
    .line 178
    move-result p1

    .line 179
    if-eqz p1, :cond_b

    .line 180
    .line 181
    instance-of p1, v2, Ljava/util/Collection;

    .line 182
    .line 183
    if-eqz p1, :cond_5

    .line 184
    .line 185
    move-object p1, v2

    .line 186
    check-cast p1, Ljava/util/Collection;

    .line 187
    .line 188
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 189
    .line 190
    .line 191
    move-result p1

    .line 192
    if-eqz p1, :cond_5

    .line 193
    .line 194
    goto :goto_6

    .line 195
    :cond_5
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 200
    .line 201
    .line 202
    move-result v2

    .line 203
    if-eqz v2, :cond_9

    .line 204
    .line 205
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 206
    .line 207
    .line 208
    move-result-object v2

    .line 209
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 210
    .line 211
    iget-object v4, v2, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 212
    .line 213
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 214
    .line 215
    .line 216
    move-result v4

    .line 217
    if-eqz v4, :cond_8

    .line 218
    .line 219
    iget-object v2, v2, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 220
    .line 221
    invoke-static {v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 222
    .line 223
    .line 224
    move-result v2

    .line 225
    if-eqz v2, :cond_7

    .line 226
    .line 227
    goto :goto_4

    .line 228
    :cond_7
    const/4 v2, 0x0

    .line 229
    goto :goto_5

    .line 230
    :cond_8
    :goto_4
    const/4 v2, 0x1

    .line 231
    :goto_5
    if-nez v2, :cond_6

    .line 232
    .line 233
    const/4 v1, 0x0

    .line 234
    :cond_9
    :goto_6
    if-eqz v1, :cond_a

    .line 235
    .line 236
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 237
    .line 238
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 239
    .line 240
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->o0ooO(Landroidx/appcompat/app/AppCompatActivity;Ljava/util/ArrayList;)V

    .line 241
    .line 242
    .line 243
    goto/16 :goto_7

    .line 244
    .line 245
    :cond_a
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 246
    .line 247
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 248
    .line 249
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 250
    .line 251
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O08〇oO8〇()Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object v2

    .line 255
    invoke-virtual {p1, v1, v0, v2}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇0000OOO(Landroidx/appcompat/app/AppCompatActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    goto/16 :goto_7

    .line 259
    .line 260
    :cond_b
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 261
    .line 262
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 263
    .line 264
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 265
    .line 266
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O08〇oO8〇()Ljava/lang/String;

    .line 267
    .line 268
    .line 269
    move-result-object v2

    .line 270
    invoke-virtual {p1, v1, v0, v2}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇0000OOO(Landroidx/appcompat/app/AppCompatActivity;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    goto/16 :goto_7

    .line 274
    .line 275
    :cond_c
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 276
    .line 277
    .line 278
    move-result-object v1

    .line 279
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 280
    .line 281
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 282
    .line 283
    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 284
    .line 285
    .line 286
    move-result v1

    .line 287
    if-eqz v1, :cond_e

    .line 288
    .line 289
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 290
    .line 291
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->OO0o〇〇〇〇0()Z

    .line 292
    .line 293
    .line 294
    move-result v1

    .line 295
    if-eqz v1, :cond_d

    .line 296
    .line 297
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 298
    .line 299
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 300
    .line 301
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->o0ooO(Landroidx/appcompat/app/AppCompatActivity;Ljava/util/ArrayList;)V

    .line 302
    .line 303
    .line 304
    goto :goto_7

    .line 305
    :cond_d
    sget-object v0, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 306
    .line 307
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 308
    .line 309
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 310
    .line 311
    .line 312
    move-result-object v2

    .line 313
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 314
    .line 315
    iget-wide v4, v2, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 316
    .line 317
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 318
    .line 319
    .line 320
    move-result-object v2

    .line 321
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 322
    .line 323
    .line 324
    move-result-object p1

    .line 325
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 326
    .line 327
    iget-object p1, p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 328
    .line 329
    invoke-virtual {v0, v1, v2, p1}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->Oo8Oo00oo(Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/Long;Ljava/lang/String;)V

    .line 330
    .line 331
    .line 332
    goto :goto_7

    .line 333
    :cond_e
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 334
    .line 335
    .line 336
    move-result-object v0

    .line 337
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 338
    .line 339
    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 340
    .line 341
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oo〇(Ljava/lang/String;)Z

    .line 342
    .line 343
    .line 344
    move-result v0

    .line 345
    if-eqz v0, :cond_f

    .line 346
    .line 347
    sget-object v0, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 348
    .line 349
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 350
    .line 351
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 352
    .line 353
    .line 354
    move-result-object p1

    .line 355
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 356
    .line 357
    iget-wide v2, p1, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 358
    .line 359
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->OoO8(Landroidx/appcompat/app/AppCompatActivity;J)V

    .line 360
    .line 361
    .line 362
    goto :goto_7

    .line 363
    :cond_f
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 364
    .line 365
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 366
    .line 367
    .line 368
    move-result-object v0

    .line 369
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 370
    .line 371
    iget-wide v5, v0, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 372
    .line 373
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 374
    .line 375
    .line 376
    move-result-object p1

    .line 377
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 378
    .line 379
    iget-object v7, p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 380
    .line 381
    const-string p1, "docs[0].docSyncId"

    .line 382
    .line 383
    invoke-static {v7, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    .line 385
    .line 386
    const/4 v8, 0x0

    .line 387
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 388
    .line 389
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O08〇oO8〇()Ljava/lang/String;

    .line 390
    .line 391
    .line 392
    move-result-object v9

    .line 393
    const/16 v10, 0x8

    .line 394
    .line 395
    const/4 v11, 0x0

    .line 396
    invoke-static/range {v4 .. v11}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->o〇〇0〇(Landroidx/appcompat/app/AppCompatActivity;JLjava/lang/String;ZLjava/lang/String;ILjava/lang/Object;)V

    .line 397
    .line 398
    .line 399
    :goto_7
    return-void

    .line 400
    :cond_10
    sget-object v4, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 401
    .line 402
    invoke-virtual {v4}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->OO0o〇〇〇〇0()Z

    .line 403
    .line 404
    .line 405
    move-result v4

    .line 406
    if-eqz v4, :cond_14

    .line 407
    .line 408
    instance-of v4, v2, Ljava/util/Collection;

    .line 409
    .line 410
    if-eqz v4, :cond_11

    .line 411
    .line 412
    move-object v4, v2

    .line 413
    check-cast v4, Ljava/util/Collection;

    .line 414
    .line 415
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    .line 416
    .line 417
    .line 418
    move-result v4

    .line 419
    if-eqz v4, :cond_11

    .line 420
    .line 421
    goto :goto_8

    .line 422
    :cond_11
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 423
    .line 424
    .line 425
    move-result-object v2

    .line 426
    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 427
    .line 428
    .line 429
    move-result v4

    .line 430
    if-eqz v4, :cond_13

    .line 431
    .line 432
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 433
    .line 434
    .line 435
    move-result-object v4

    .line 436
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 437
    .line 438
    iget-object v4, v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 439
    .line 440
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇oOO8O8(Ljava/lang/String;)Z

    .line 441
    .line 442
    .line 443
    move-result v4

    .line 444
    if-eqz v4, :cond_12

    .line 445
    .line 446
    const/4 v3, 0x1

    .line 447
    :cond_13
    :goto_8
    if-eqz v3, :cond_14

    .line 448
    .line 449
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 450
    .line 451
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 452
    .line 453
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->o0ooO(Landroidx/appcompat/app/AppCompatActivity;Ljava/util/ArrayList;)V

    .line 454
    .line 455
    .line 456
    return-void

    .line 457
    :cond_14
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 458
    .line 459
    new-instance v3, LO8o08O8O/〇〇888;

    .line 460
    .line 461
    invoke-direct {v3, p0, p1}, LO8o08O8O/〇〇888;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V

    .line 462
    .line 463
    .line 464
    invoke-static {v2, v0, v1, v3}, Lcom/intsig/camscanner/share/ShareHelper;->Ooo8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 465
    .line 466
    .line 467
    return-void
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method private static final OOO8o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Landroid/net/Uri;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V
    .locals 7

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$uri"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "$docItem"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "$newTitle"

    .line 17
    .line 18
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v0, "_data"

    .line 28
    .line 29
    const-string v2, "sync_doc_id"

    .line 30
    .line 31
    filled-new-array {v0, v2}, [Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    const/4 v4, 0x0

    .line 36
    const/4 v5, 0x0

    .line 37
    const/4 v6, 0x0

    .line 38
    move-object v2, p1

    .line 39
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const/4 v0, 0x0

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    if-eqz v1, :cond_0

    .line 51
    .line 52
    const/4 v0, 0x0

    .line 53
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 58
    .line 59
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 60
    .line 61
    const/4 v3, 0x1

    .line 62
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    const/4 v4, 0x3

    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 68
    .line 69
    .line 70
    move-result-wide v5

    .line 71
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 72
    .line 73
    .line 74
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 75
    .line 76
    .line 77
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 78
    .line 79
    .line 80
    move-result-wide v1

    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 82
    .line 83
    invoke-static {v1, v2, p3, v0, p1}, Lcom/intsig/camscanner/util/Util;->o8O0(JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 87
    .line 88
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 89
    .line 90
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 91
    .line 92
    .line 93
    move-result-wide v0

    .line 94
    invoke-virtual {p1, p3, v0, v1, p4}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇00〇8(Landroid/content/Context;JI)V

    .line 95
    .line 96
    .line 97
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 98
    .line 99
    new-instance p2, LO8o08O8O/〇8o8o〇;

    .line 100
    .line 101
    invoke-direct {p2, p0}, LO8o08O8O/〇8o8o〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1, p2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final OOO〇O0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$selectDocItemIds"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 12
    .line 13
    .line 14
    const-string p2, "CSAddShortcutPop"

    .line 15
    .line 16
    const-string p3, "confirm"

    .line 17
    .line 18
    invoke-static {p2, p3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o(Ljava/util/Set;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final OOo8o〇O(ZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docItem"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "newTitle"

    .line 12
    .line 13
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "onTitleChanged newTitle="

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {p5}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    .line 48
    return-void

    .line 49
    :cond_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    iget-object v2, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 54
    .line 55
    const/4 v5, 0x0

    .line 56
    new-instance v6, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$1$1;

    .line 57
    .line 58
    invoke-direct {v6, p1, p3, p5}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$1$2;

    .line 62
    .line 63
    invoke-direct {v7, p5, p4, p3, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$1$2;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 64
    .line 65
    .line 66
    move-object v3, p2

    .line 67
    move-object v4, p5

    .line 68
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final Oo()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showMoreDialog$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showMoreDialog$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/thread/CustomAsyncTask;->〇O8o08O(Ljava/lang/String;)Lcom/intsig/thread/CustomAsyncTask;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/thread/CustomAsyncTask;->Oo08()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OO8oO0o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final Oo8Oo00oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;)V
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Ljava/lang/Integer;

    .line 3
    .line 4
    const/16 v1, 0x10

    .line 5
    .line 6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/4 v2, 0x0

    .line 11
    aput-object v1, v0, v2

    .line 12
    .line 13
    const/16 v1, 0x11

    .line 14
    .line 15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x1

    .line 20
    aput-object v1, v0, v2

    .line 21
    .line 22
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->getMItemId()I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇08O8o〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;Ljava/util/Set;Ljava/util/Set;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainLockHandler;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 55
    .line 56
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$doHandleItemClick$1;

    .line 57
    .line 58
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$doHandleItemClick$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainLockHandler;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainLockHandler$IDocUnlockListener;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainLockHandler;->〇o〇(Ljava/util/Set;)V

    .line 69
    .line 70
    .line 71
    :goto_0
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic OoO8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final Oo〇O(Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;I)V
    .locals 8

    .line 1
    const-string p2, "$docIds"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    if-eqz p2, :cond_1

    .line 20
    .line 21
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    check-cast p2, Ljava/lang/Long;

    .line 26
    .line 27
    new-instance v6, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 30
    .line 31
    .line 32
    new-instance v7, Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .line 36
    .line 37
    iget-object v0, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 38
    .line 39
    const-string v1, "id"

    .line 40
    .line 41
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 45
    .line 46
    .line 47
    move-result-wide v1

    .line 48
    const-string v3, "page_num ASC"

    .line 49
    .line 50
    move-object v4, v6

    .line 51
    move-object v5, v7

    .line 52
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/db/dao/ImageDao;->ooo0〇O88O(Landroid/content/Context;JLjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    if-lez p2, :cond_0

    .line 60
    .line 61
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    .line 62
    .line 63
    .line 64
    move-result p2

    .line 65
    if-lez p2, :cond_0

    .line 66
    .line 67
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    .line 68
    .line 69
    .line 70
    move-result p2

    .line 71
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-ne p2, v0, :cond_0

    .line 76
    .line 77
    iget-object p2, p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 78
    .line 79
    invoke-static {p2, v6, v7}, Lcom/intsig/camscanner/control/ShareControl;->〇O(Landroid/app/Activity;Ljava/util/List;Ljava/util/ArrayList;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    return-void
    .line 84
    .line 85
.end method

.method private final O〇0(Z)V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showCopyMoveDialog>>> isShowMove = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇080:Lcom/intsig/camscanner/scenariodir/DocManualOperations;

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    new-instance v4, Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Ljava/util/Collection;

    .line 34
    .line 35
    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 36
    .line 37
    .line 38
    new-instance v5, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Ljava/util/Collection;

    .line 45
    .line 46
    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v7

    .line 53
    move v6, p1

    .line 54
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;ZLjava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇O(Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O〇O〇oO()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mMainBottomListener"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 16
    .line 17
    const-string v2, "cs_home"

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    return-object v2

    .line 22
    :cond_1
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 23
    .line 24
    if-eqz v1, :cond_3

    .line 25
    .line 26
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o8o8〇o()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    const-string v0, "cs_main"

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "cs_directory"

    .line 42
    .line 43
    :goto_0
    return-object v0

    .line 44
    :cond_3
    return-object v2
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o0O0(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    new-instance v1, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    check-cast v2, Ljava/util/Collection;

    .line 10
    .line 11
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 12
    .line 13
    .line 14
    new-instance v2, LO8o08O8O/Oo08;

    .line 15
    .line 16
    invoke-direct {v2, p0, p2, p1}, LO8o08O8O/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 17
    .line 18
    .line 19
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0ooO(IZI)V
    .locals 1

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 4
    .line 5
    const-string p2, "doBtmItemAction>>> in share dir, no permission"

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    if-eqz p3, :cond_0

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p1, p3}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void

    .line 22
    :cond_1
    if-eqz p1, :cond_7

    .line 23
    .line 24
    const/4 p2, 0x1

    .line 25
    if-eq p1, p2, :cond_5

    .line 26
    .line 27
    const/4 p2, 0x2

    .line 28
    if-eq p1, p2, :cond_4

    .line 29
    .line 30
    const/4 p2, 0x3

    .line 31
    if-eq p1, p2, :cond_3

    .line 32
    .line 33
    const/4 p2, 0x4

    .line 34
    if-eq p1, p2, :cond_2

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    const-string p2, "select_list_more"

    .line 42
    .line 43
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    const-string p1, "file_more"

    .line 47
    .line 48
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8〇o()Z

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    sget-object p2, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 56
    .line 57
    new-instance p3, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    const-string v0, "doBtmItemAction: more contains office doc: "

    .line 63
    .line 64
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo()V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    const-string p2, "select_list_delete"

    .line 86
    .line 87
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    const-string p1, "delete"

    .line 91
    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇O〇80o08O()V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o88〇OO08〇()V

    .line 100
    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    const-string p2, "select_list_move_copy"

    .line 108
    .line 109
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    const-string p1, "move_copy"

    .line 113
    .line 114
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 118
    .line 119
    if-nez p1, :cond_6

    .line 120
    .line 121
    const-string p1, "mMainBottomListener"

    .line 122
    .line 123
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    const/4 p1, 0x0

    .line 127
    :cond_6
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇080()Z

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇0(Z)V

    .line 132
    .line 133
    .line 134
    goto :goto_0

    .line 135
    :cond_7
    const-string p1, "share"

    .line 136
    .line 137
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇000O0()V

    .line 141
    .line 142
    .line 143
    :goto_0
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final o8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 7
    .line 8
    if-nez p0, :cond_0

    .line 9
    .line 10
    const-string p0, "mMainBottomListener"

    .line 11
    .line 12
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 p0, 0x0

    .line 16
    :cond_0
    invoke-interface {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method public static final synthetic o800o8O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Ljava/util/Set;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o88〇OO08〇()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-le v0, v1, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "select_list_merge"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v0, "merge"

    .line 22
    .line 23
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmMergeHandler;

    .line 27
    .line 28
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmMergeHandler;-><init>()V

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 34
    .line 35
    if-nez v2, :cond_0

    .line 36
    .line 37
    const-string v2, "mMainBottomListener"

    .line 38
    .line 39
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/4 v2, 0x0

    .line 43
    :cond_0
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmMergeHandler;->〇O〇(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;)Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmMergeHandler;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmMergeHandler;->OO0o〇〇()V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    const-string v1, "select_list_rename"

    .line 56
    .line 57
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const-string v0, "CSFileRename"

    .line 61
    .line 62
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    check-cast v0, Ljava/lang/Iterable;

    .line 70
    .line 71
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇O(Ljava/lang/Iterable;)Ljava/util/List;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const/4 v1, 0x0

    .line 76
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 81
    .line 82
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o0O0O8(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 83
    .line 84
    .line 85
    :goto_0
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o8O〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$initEditBtmBar$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$initEditBtmBar$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->OO0o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout$IPageChangeCallback;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o8oO〇()Lcom/intsig/app/BaseProgressDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇80〇808〇O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oO(Ljava/util/Set;Ljava/util/Set;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    check-cast p2, Ljava/util/Collection;

    .line 4
    .line 5
    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 6
    .line 7
    .line 8
    check-cast p1, Ljava/lang/Iterable;

    .line 9
    .line 10
    new-instance p2, Ljava/util/ArrayList;

    .line 11
    .line 12
    const/16 v1, 0xa

    .line 13
    .line 14
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 19
    .line 20
    .line 21
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 36
    .line 37
    new-instance v8, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 40
    .line 41
    .line 42
    move-result-wide v3

    .line 43
    const-string v5, ""

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 50
    .line 51
    .line 52
    move-result v7

    .line 53
    move-object v2, v8

    .line 54
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/datastruct/DocumentListItem;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 55
    .line 56
    .line 57
    invoke-interface {p2, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    .line 62
    .line 63
    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 64
    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 67
    .line 68
    new-instance v1, LO8o08O8O/〇O8o08O;

    .line 69
    .line 70
    invoke-direct {v1, p0, v0, p1}, LO8o08O8O/〇O8o08O;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 71
    .line 72
    .line 73
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final oO00OOO()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mMainBottomListener"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇8o8o〇()Ljava/util/Set;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O0O8OO088(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->CERTIFICATE_CS_LIST:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->idType(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-static {p2, p1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final ooo0〇O88O(Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    new-instance v3, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v4, "showRenameDlg id:"

    .line 15
    .line 16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v1, ", title:"

    .line 23
    .line 24
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    move-object/from16 v11, p2

    .line 28
    .line 29
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, v6, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 40
    .line 41
    if-nez v0, :cond_0

    .line 42
    .line 43
    const-string v0, "mMainBottomListener"

    .line 44
    .line 45
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇o00〇〇Oo()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const/4 v1, 0x0

    .line 54
    if-eqz v0, :cond_1

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oO()Z

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    const/4 v2, 0x1

    .line 61
    if-ne v0, v2, :cond_1

    .line 62
    .line 63
    const/4 v1, 0x1

    .line 64
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v8

    .line 68
    iget-object v7, v6, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 69
    .line 70
    const v9, 0x7f131e5a

    .line 71
    .line 72
    .line 73
    const/4 v10, 0x0

    .line 74
    new-instance v13, LO8o08O8O/〇o00〇〇Oo;

    .line 75
    .line 76
    move-object v0, v13

    .line 77
    move-object/from16 v2, p0

    .line 78
    .line 79
    move-object v3, v8

    .line 80
    move-object/from16 v4, p1

    .line 81
    .line 82
    move-object/from16 v5, p2

    .line 83
    .line 84
    invoke-direct/range {v0 .. v5}, LO8o08O8O/〇o00〇〇Oo;-><init>(ZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    new-instance v14, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$2;

    .line 88
    .line 89
    invoke-direct {v14, v6}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$showRenameDlg$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 93
    .line 94
    .line 95
    move-result-wide v15

    .line 96
    move-object/from16 v11, p2

    .line 97
    .line 98
    move-object/from16 v12, p3

    .line 99
    .line 100
    invoke-static/range {v7 .. v16}, Lcom/intsig/camscanner/app/DialogUtils;->o88〇OO08〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 101
    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final ooo〇8oO(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$docs"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    new-instance v1, LO8o08O8O/oO80;

    .line 14
    .line 15
    invoke-direct {v1, p0, p1}, LO8o08O8O/oO80;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V

    .line 16
    .line 17
    .line 18
    new-instance p1, LO8o08O8O/〇80〇808〇O;

    .line 19
    .line 20
    invoke-direct {p1, p0}, LO8o08O8O/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->O0〇0(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareDismiss;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$selectDocItemIds"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "CSAddShortcutPop"

    .line 12
    .line 13
    const-string v0, "not_tips"

    .line 14
    .line 15
    invoke-static {p3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇〇88〇88()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o(Ljava/util/Set;)V

    .line 22
    .line 23
    .line 24
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Landroid/net/Uri;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOO8o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Landroid/net/Uri;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final o〇0OOo〇0(ZLjava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mMainBottomListener"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇o00〇〇Oo()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 v3, 0x0

    .line 24
    :goto_0
    const/16 v4, 0x65

    .line 25
    .line 26
    if-ne v3, v4, :cond_3

    .line 27
    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    goto :goto_1

    .line 35
    :cond_2
    const/4 v0, 0x4

    .line 36
    :goto_1
    sget-object v4, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080:Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;

    .line 37
    .line 38
    invoke-virtual {v4, v3, v0}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080(II)V

    .line 39
    .line 40
    .line 41
    goto :goto_2

    .line 42
    :cond_3
    const-string v0, "CSMain"

    .line 43
    .line 44
    const-string v3, "lock"

    .line 45
    .line 46
    invoke-static {v0, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    :goto_2
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmLockHandler;

    .line 50
    .line 51
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmLockHandler;-><init>()V

    .line 52
    .line 53
    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 55
    .line 56
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 57
    .line 58
    if-nez v4, :cond_4

    .line 59
    .line 60
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    goto :goto_3

    .line 64
    :cond_4
    move-object v1, v4

    .line 65
    :goto_3
    invoke-virtual {v0, v3, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmLockHandler;->o〇0(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;)Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmLockHandler;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0, p2, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmLockHandler;->Oo08(Ljava/util/Set;Z)V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o〇8(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    check-cast p1, Ljava/util/Collection;

    .line 6
    .line 7
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    new-instance v2, LO8o08O8O/o〇0;

    .line 12
    .line 13
    invoke-direct {v2, p0}, LO8o08O8O/o〇0;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1, p1, v2}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->oo88o8O(Landroidx/fragment/app/FragmentActivity;Ljava/util/List;Lcom/intsig/callback/Callback0;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method private final o〇O()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mMainBottomListener"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->OO0o〇〇〇〇0()Ljava/util/Set;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇O8〇〇o(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇80()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇〇0〇(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8o0o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o(Ljava/util/Set;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    const-string v0, "CSAddShortcutPop"

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    const v1, 0x7f131e57

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const v1, 0x7f13133a

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    new-instance v1, LO8o08O8O/〇o〇;

    .line 41
    .line 42
    invoke-direct {v1, p0, p1}, LO8o08O8O/〇o〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;)V

    .line 43
    .line 44
    .line 45
    const v2, 0x7f131e36

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    new-instance v1, LO8o08O8O/O8;

    .line 53
    .line 54
    invoke-direct {v1, p0, p1}, LO8o08O8O/O8;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;)V

    .line 55
    .line 56
    .line 57
    const p1, 0x7f13111e

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, p1, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    const/4 v0, 0x0

    .line 65
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 74
    .line 75
    .line 76
    :cond_1
    :goto_0
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final 〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Ljava/util/ArrayList;I)V
    .locals 2

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$docItems"

    .line 7
    .line 8
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance p3, Landroid/content/Intent;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-class v1, Lcom/intsig/camscanner/movecopyactivity/MoveCopyActivity;

    .line 16
    .line 17
    invoke-direct {p3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "sourceFolderParentSyncId"

    .line 21
    .line 22
    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    const-string p1, "docItems"

    .line 26
    .line 27
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    const-string p1, "fromPart"

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const-string p1, "ACTION_DOCS_COPY"

    .line 40
    .line 41
    invoke-virtual {p3, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 45
    .line 46
    const/16 p1, 0x82

    .line 47
    .line 48
    invoke-virtual {p0, p3, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇00()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0000OOO(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mMainBottomListener"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇O〇0()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇80o(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇00〇8()Z
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Ljava/lang/Iterable;

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    move-object v2, v1

    .line 22
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-static {v2}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const/4 v1, 0x0

    .line 36
    :goto_0
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 37
    .line 38
    if-eqz v1, :cond_2

    .line 39
    .line 40
    const/4 v0, 0x1

    .line 41
    goto :goto_1

    .line 42
    :cond_2
    const/4 v0, 0x0

    .line 43
    :goto_1
    return v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O0o〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0O〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 16
    .line 17
    if-nez p0, :cond_1

    .line 18
    .line 19
    const-string p0, "mMainBottomListener"

    .line 20
    .line 21
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const/4 p0, 0x0

    .line 25
    :cond_1
    invoke-interface {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo08:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 4

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$docIds"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$documentListItems"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance p3, Landroid/content/Intent;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    const-class v1, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 21
    .line 22
    const-string v2, "android.intent.action.SEND"

    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    invoke-direct {p3, v2, v3, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    const-string v1, "SEND_TYPE"

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    if-le v0, v2, :cond_0

    .line 36
    .line 37
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 38
    .line 39
    const-string v0, "User Operation: upload docs"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    const/16 p1, 0xb

    .line 45
    .line 46
    invoke-virtual {p3, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    .line 48
    .line 49
    const-string p1, "ids"

    .line 50
    .line 51
    invoke-virtual {p3, p1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    if-ne p2, v2, :cond_1

    .line 60
    .line 61
    sget-object p2, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 62
    .line 63
    const-string v0, "User Operation: upload_print_fax doc"

    .line 64
    .line 65
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const/16 p2, 0xa

    .line 69
    .line 70
    invoke-virtual {p3, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    .line 72
    .line 73
    const/4 p2, 0x0

    .line 74
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    const-string p2, "docIds[0]"

    .line 79
    .line 80
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    check-cast p1, Ljava/lang/Number;

    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    .line 86
    .line 87
    .line 88
    move-result-wide p1

    .line 89
    const-string v0, "doc_id"

    .line 90
    .line 91
    invoke-virtual {p3, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 92
    .line 93
    .line 94
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 95
    .line 96
    invoke-virtual {p1, p3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 97
    .line 98
    .line 99
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 100
    .line 101
    if-nez p0, :cond_2

    .line 102
    .line 103
    const-string p0, "mMainBottomListener"

    .line 104
    .line 105
    invoke-static {p0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_2
    move-object v3, p0

    .line 110
    :goto_1
    invoke-interface {v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 111
    .line 112
    .line 113
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇80()Ljava/util/ArrayList;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;",
            ">;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x6

    .line 4
    new-array v2, v1, [Ljava/lang/Integer;

    .line 5
    .line 6
    const/16 v3, 0x19

    .line 7
    .line 8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    const/4 v4, 0x0

    .line 13
    aput-object v3, v2, v4

    .line 14
    .line 15
    const/16 v5, 0x18

    .line 16
    .line 17
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v5

    .line 21
    const/4 v6, 0x1

    .line 22
    aput-object v5, v2, v6

    .line 23
    .line 24
    const/16 v7, 0x10

    .line 25
    .line 26
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object v7

    .line 30
    const/4 v8, 0x2

    .line 31
    aput-object v7, v2, v8

    .line 32
    .line 33
    const/16 v9, 0x11

    .line 34
    .line 35
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 36
    .line 37
    .line 38
    move-result-object v9

    .line 39
    const/4 v10, 0x3

    .line 40
    aput-object v9, v2, v10

    .line 41
    .line 42
    const/16 v11, 0x12

    .line 43
    .line 44
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 45
    .line 46
    .line 47
    move-result-object v11

    .line 48
    const/4 v12, 0x4

    .line 49
    aput-object v11, v2, v12

    .line 50
    .line 51
    const/16 v11, 0x15

    .line 52
    .line 53
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 54
    .line 55
    .line 56
    move-result-object v11

    .line 57
    const/4 v13, 0x5

    .line 58
    aput-object v11, v2, v13

    .line 59
    .line 60
    new-array v14, v1, [Ljava/lang/Integer;

    .line 61
    .line 62
    const v15, 0x7f1307ff

    .line 63
    .line 64
    .line 65
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 66
    .line 67
    .line 68
    move-result-object v15

    .line 69
    aput-object v15, v14, v4

    .line 70
    .line 71
    const v15, 0x7f131348

    .line 72
    .line 73
    .line 74
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 75
    .line 76
    .line 77
    move-result-object v15

    .line 78
    aput-object v15, v14, v6

    .line 79
    .line 80
    const v15, 0x7f1302e6

    .line 81
    .line 82
    .line 83
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 84
    .line 85
    .line 86
    move-result-object v15

    .line 87
    aput-object v15, v14, v8

    .line 88
    .line 89
    const v15, 0x7f1302ea

    .line 90
    .line 91
    .line 92
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 93
    .line 94
    .line 95
    move-result-object v15

    .line 96
    aput-object v15, v14, v10

    .line 97
    .line 98
    const v15, 0x7f130133

    .line 99
    .line 100
    .line 101
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v15

    .line 105
    aput-object v15, v14, v12

    .line 106
    .line 107
    const v15, 0x7f131dbc

    .line 108
    .line 109
    .line 110
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 111
    .line 112
    .line 113
    move-result-object v15

    .line 114
    aput-object v15, v14, v13

    .line 115
    .line 116
    new-array v1, v1, [Ljava/lang/Integer;

    .line 117
    .line 118
    const v15, 0x7f080e4a

    .line 119
    .line 120
    .line 121
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 122
    .line 123
    .line 124
    move-result-object v15

    .line 125
    aput-object v15, v1, v4

    .line 126
    .line 127
    const v15, 0x7f0806ef

    .line 128
    .line 129
    .line 130
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 131
    .line 132
    .line 133
    move-result-object v15

    .line 134
    aput-object v15, v1, v6

    .line 135
    .line 136
    const v15, 0x7f080762

    .line 137
    .line 138
    .line 139
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 140
    .line 141
    .line 142
    move-result-object v15

    .line 143
    aput-object v15, v1, v8

    .line 144
    .line 145
    aput-object v15, v1, v10

    .line 146
    .line 147
    const v8, 0x7f08093c

    .line 148
    .line 149
    .line 150
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 151
    .line 152
    .line 153
    move-result-object v8

    .line 154
    aput-object v8, v1, v12

    .line 155
    .line 156
    const v8, 0x7f0804e5

    .line 157
    .line 158
    .line 159
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 160
    .line 161
    .line 162
    move-result-object v8

    .line 163
    aput-object v8, v1, v13

    .line 164
    .line 165
    iget-object v8, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 166
    .line 167
    const/4 v10, 0x0

    .line 168
    const-string v12, "mMainBottomListener"

    .line 169
    .line 170
    if-nez v8, :cond_0

    .line 171
    .line 172
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    move-object v8, v10

    .line 176
    :cond_0
    invoke-interface {v8}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇8o8o〇()Ljava/util/Set;

    .line 177
    .line 178
    .line 179
    move-result-object v8

    .line 180
    invoke-interface {v8}, Ljava/util/Set;->size()I

    .line 181
    .line 182
    .line 183
    move-result v13

    .line 184
    if-ne v13, v6, :cond_1

    .line 185
    .line 186
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇00〇8()Z

    .line 187
    .line 188
    .line 189
    move-result v13

    .line 190
    if-eqz v13, :cond_1

    .line 191
    .line 192
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O08000()Z

    .line 193
    .line 194
    .line 195
    move-result v13

    .line 196
    if-eqz v13, :cond_1

    .line 197
    .line 198
    const/4 v4, 0x1

    .line 199
    :cond_1
    new-instance v13, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;

    .line 200
    .line 201
    const/16 v16, 0x0

    .line 202
    .line 203
    const/16 v17, 0x0

    .line 204
    .line 205
    const/16 v18, 0x0

    .line 206
    .line 207
    const/16 v19, 0x7

    .line 208
    .line 209
    const/16 v20, 0x0

    .line 210
    .line 211
    move-object v15, v13

    .line 212
    invoke-direct/range {v15 .. v20}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;-><init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 213
    .line 214
    .line 215
    invoke-virtual {v13, v2, v14, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->tansTabList([Ljava/lang/Integer;[Ljava/lang/Integer;[Ljava/lang/Integer;)Ljava/util/ArrayList;

    .line 216
    .line 217
    .line 218
    move-result-object v1

    .line 219
    new-instance v2, Ljava/util/ArrayList;

    .line 220
    .line 221
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 222
    .line 223
    .line 224
    if-nez v4, :cond_2

    .line 225
    .line 226
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    .line 228
    .line 229
    :cond_2
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 230
    .line 231
    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 232
    .line 233
    .line 234
    move-result-object v3

    .line 235
    move-object v4, v8

    .line 236
    check-cast v4, Ljava/lang/Iterable;

    .line 237
    .line 238
    new-instance v13, Ljava/util/ArrayList;

    .line 239
    .line 240
    const/16 v14, 0xa

    .line 241
    .line 242
    invoke-static {v4, v14}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 243
    .line 244
    .line 245
    move-result v14

    .line 246
    invoke-direct {v13, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 247
    .line 248
    .line 249
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 250
    .line 251
    .line 252
    move-result-object v4

    .line 253
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 254
    .line 255
    .line 256
    move-result v14

    .line 257
    if-eqz v14, :cond_3

    .line 258
    .line 259
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 260
    .line 261
    .line 262
    move-result-object v14

    .line 263
    check-cast v14, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 264
    .line 265
    invoke-virtual {v14}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 266
    .line 267
    .line 268
    move-result-wide v14

    .line 269
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 270
    .line 271
    .line 272
    move-result-object v14

    .line 273
    invoke-interface {v13, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 274
    .line 275
    .line 276
    goto :goto_0

    .line 277
    :cond_3
    invoke-static {v3, v13}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇8〇0〇o〇O(Landroid/content/Context;Ljava/util/List;)Z

    .line 278
    .line 279
    .line 280
    move-result v3

    .line 281
    if-eqz v3, :cond_4

    .line 282
    .line 283
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    .line 285
    .line 286
    goto :goto_1

    .line 287
    :cond_4
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    .line 289
    .line 290
    :goto_1
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 291
    .line 292
    if-nez v3, :cond_5

    .line 293
    .line 294
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 295
    .line 296
    .line 297
    move-object v3, v10

    .line 298
    :cond_5
    invoke-interface {v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 299
    .line 300
    .line 301
    move-result-object v3

    .line 302
    instance-of v3, v3, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 303
    .line 304
    if-eqz v3, :cond_7

    .line 305
    .line 306
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 307
    .line 308
    if-nez v3, :cond_6

    .line 309
    .line 310
    invoke-static {v12}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 311
    .line 312
    .line 313
    goto :goto_2

    .line 314
    :cond_6
    move-object v10, v3

    .line 315
    :goto_2
    invoke-interface {v10}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->o〇0()Z

    .line 316
    .line 317
    .line 318
    move-result v3

    .line 319
    if-eqz v3, :cond_8

    .line 320
    .line 321
    :cond_7
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    .line 323
    .line 324
    :cond_8
    invoke-interface {v8}, Ljava/util/Set;->size()I

    .line 325
    .line 326
    .line 327
    move-result v3

    .line 328
    if-le v3, v6, :cond_9

    .line 329
    .line 330
    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    .line 332
    .line 333
    :cond_9
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 334
    .line 335
    .line 336
    move-result-object v3

    .line 337
    const-string v4, "tansTabList.iterator()"

    .line 338
    .line 339
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    .line 341
    .line 342
    :cond_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 343
    .line 344
    .line 345
    move-result v4

    .line 346
    if-eqz v4, :cond_c

    .line 347
    .line 348
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 349
    .line 350
    .line 351
    move-result-object v4

    .line 352
    const-string v5, "tansListIterator.next()"

    .line 353
    .line 354
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 355
    .line 356
    .line 357
    check-cast v4, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;

    .line 358
    .line 359
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 360
    .line 361
    .line 362
    move-result-object v5

    .line 363
    :cond_b
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 364
    .line 365
    .line 366
    move-result v6

    .line 367
    if-eqz v6, :cond_a

    .line 368
    .line 369
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 370
    .line 371
    .line 372
    move-result-object v6

    .line 373
    check-cast v6, Ljava/lang/Number;

    .line 374
    .line 375
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 376
    .line 377
    .line 378
    move-result v6

    .line 379
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->getMItemId()I

    .line 380
    .line 381
    .line 382
    move-result v7

    .line 383
    if-ne v6, v7, :cond_b

    .line 384
    .line 385
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 386
    .line 387
    .line 388
    goto :goto_3

    .line 389
    :cond_c
    return-object v1
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇8〇0〇o〇O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/ArrayList;I)V
    .locals 7

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$arrayList"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-virtual {p2}, Landroid/app/Activity;->isFinishing()Z

    .line 14
    .line 15
    .line 16
    move-result p2

    .line 17
    if-nez p2, :cond_0

    .line 18
    .line 19
    const/4 p2, 0x0

    .line 20
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 27
    .line 28
    .line 29
    move-result-wide p1

    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    const/4 v2, 0x0

    .line 37
    const/4 v3, 0x0

    .line 38
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$extractImages$1$1;

    .line 39
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-direct {v4, p0, p1, p2, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$extractImages$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;JLkotlin/coroutines/Continuation;)V

    .line 42
    .line 43
    .line 44
    const/4 v5, 0x3

    .line 45
    const/4 v6, 0x0

    .line 46
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 47
    .line 48
    .line 49
    :cond_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇O(Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v3

    .line 11
    new-instance v4, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v5, "saveWithNewTitle id:"

    .line 17
    .line 18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v1, " title:"

    .line 25
    .line 26
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v1, ", newTitle:"

    .line 33
    .line 34
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const-string v0, "CSFileRename"

    .line 48
    .line 49
    const-string v1, "finish"

    .line 50
    .line 51
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 57
    .line 58
    .line 59
    move-result-wide v1

    .line 60
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 61
    .line 62
    .line 63
    move-result-object v5

    .line 64
    const-string v0, "withAppendedId(Documents\u2026.CONTENT_URI, docItem.id)"

    .line 65
    .line 66
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    new-instance v0, LO8o08O8O/OO0o〇〇〇〇0;

    .line 70
    .line 71
    move-object v3, v0

    .line 72
    move-object v4, p0

    .line 73
    move-object v6, p1

    .line 74
    move-object v7, p2

    .line 75
    move v8, p3

    .line 76
    invoke-direct/range {v3 .. v8}, LO8o08O8O/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Landroid/net/Uri;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo8Oo00oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O888o0o(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Ljava/util/Set;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0O〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;IZI)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o0ooO(IZI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇O〇80o08O()V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇080:Lcom/intsig/camscanner/scenariodir/DocManualOperations;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    new-instance v2, Ljava/util/ArrayList;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    const-string v5, "mMainBottomListener"

    .line 11
    .line 12
    if-nez v3, :cond_0

    .line 13
    .line 14
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    move-object v3, v4

    .line 18
    :cond_0
    invoke-interface {v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->OO0o〇〇〇〇0()Ljava/util/Set;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    check-cast v3, Ljava/util/Collection;

    .line 23
    .line 24
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 25
    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 28
    .line 29
    if-nez v3, :cond_1

    .line 30
    .line 31
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    move-object v4, v3

    .line 36
    :goto_0
    invoke-interface {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇0000OOO()Z

    .line 37
    .line 38
    .line 39
    move-result v3

    .line 40
    const/4 v4, 0x0

    .line 41
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$go2Delete$1;

    .line 42
    .line 43
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$go2Delete$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 44
    .line 45
    .line 46
    const/16 v6, 0x8

    .line 47
    .line 48
    const/4 v7, 0x0

    .line 49
    invoke-static/range {v0 .. v7}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->oo88o8O(Lcom/intsig/camscanner/scenariodir/DocManualOperations;Landroid/app/Activity;Ljava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇o(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: add shortcut"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMain"

    .line 9
    .line 10
    const-string v1, "addshortcut"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    check-cast p1, Ljava/lang/Iterable;

    .line 16
    .line 17
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->oO(Ljava/lang/Iterable;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    check-cast p1, Ljava/lang/Number;

    .line 22
    .line 23
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    const-string v3, "mMainBottomListener"

    .line 37
    .line 38
    if-nez v1, :cond_0

    .line 39
    .line 40
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    move-object v1, v2

    .line 44
    :cond_0
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇0000OOO()Z

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->〇〇00OO(Landroid/content/Context;Ljava/lang/Long;Z)V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 52
    .line 53
    if-nez p1, :cond_1

    .line 54
    .line 55
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    move-object v2, p1

    .line 60
    :goto_0
    invoke-interface {v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇o00〇〇Oo(ZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOo8o〇O(ZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O000()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0o(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: save to gallery"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    check-cast p1, Ljava/util/Collection;

    .line 9
    .line 10
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const-string v0, "null cannot be cast to non-null type java.util.ArrayList<kotlin.Long>"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    check-cast p1, Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇〇o8(Ljava/util/ArrayList;)V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 25
    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    const-string p1, "mMainBottomListener"

    .line 29
    .line 30
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/4 p1, 0x0

    .line 34
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8〇o()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOO〇O0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;Ljava/util/Set;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o8oO〇()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇o8(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    new-instance v1, LO8o08O8O/Oooo8o0〇;

    .line 4
    .line 5
    invoke-direct {v1, p1, p0}, LO8o08O8O/Oooo8o0〇;-><init>(Ljava/util/ArrayList;Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 6
    .line 7
    .line 8
    invoke-static {v0, p1, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇0〇〇0(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: multi tag"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    check-cast p1, Ljava/util/Collection;

    .line 11
    .line 12
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->OOO8o〇〇(Ljava/util/Collection;)[J

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-static {v0, p1}, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;->〇080(Landroidx/fragment/app/FragmentActivity;[J)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 20
    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    const-string p1, "mMainBottomListener"

    .line 24
    .line 25
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/4 p1, 0x0

    .line 29
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->O8()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final O0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Ooo()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mMainBottomListener"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇80〇808〇O()Landroidx/fragment/app/Fragment;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 16
    .line 17
    const-string v2, "CSHome"

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    return-object v2

    .line 22
    :cond_1
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 23
    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    check-cast v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o8o8〇o()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->o〇0()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    return-object v0

    .line 37
    :cond_2
    return-object v2
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final Ooo8〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainBtmListener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo〇o(Z)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move/from16 v1, p1

    .line 4
    .line 5
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 9
    .line 10
    .line 11
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 12
    .line 13
    invoke-static {v2, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 14
    .line 15
    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o〇:Landroidx/viewpager2/widget/ViewPager2;

    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    const/4 v5, 0x0

    .line 22
    const/4 v6, 0x0

    .line 23
    const/high16 v1, 0x42400000    # 48.0f

    .line 24
    .line 25
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 26
    .line 27
    .line 28
    move-result v7

    .line 29
    const/4 v8, 0x7

    .line 30
    const/4 v9, 0x0

    .line 31
    invoke-static/range {v3 .. v9}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    iget-object v10, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o〇:Landroidx/viewpager2/widget/ViewPager2;

    .line 36
    .line 37
    const/4 v11, 0x0

    .line 38
    const/4 v12, 0x0

    .line 39
    const/4 v13, 0x0

    .line 40
    const/4 v14, 0x0

    .line 41
    const/4 v15, 0x7

    .line 42
    const/16 v16, 0x0

    .line 43
    .line 44
    invoke-static/range {v10 .. v16}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final oo()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇oOO8O8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8oOO88()Landroid/widget/EditText;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO80:Landroid/widget/EditText;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇o(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/DocumentListItem;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    iput v1, v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo0:I

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    iput-wide v1, v0, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput-object v1, v0, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    .line 36
    .line 37
    .line 38
    move-result-wide v1

    .line 39
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;->〇080OO8〇0:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;->〇0O:Ljava/lang/String;

    .line 54
    .line 55
    sget-object v1, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 58
    .line 59
    .line 60
    move-result-wide v2

    .line 61
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇O8o08O(J)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-nez v1, :cond_0

    .line 66
    .line 67
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 68
    .line 69
    sget-object v1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 70
    .line 71
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 76
    .line 77
    .line 78
    move-result-wide v2

    .line 79
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇〇0〇(Landroid/content/Context;J)I

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_0
    const/4 p1, 0x1

    .line 92
    new-array p1, p1, [Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 93
    .line 94
    const/4 v1, 0x0

    .line 95
    aput-object v0, p1, v1

    .line 96
    .line 97
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOO(Ljava/util/List;)V

    .line 102
    .line 103
    .line 104
    :goto_0
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇000O0()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "! mMainBottomListener.isInitialized"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇O()Ljava/util/Set;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇8o8o〇:Ljava/lang/String;

    .line 24
    .line 25
    const-string v1, "mSelectDocItemIds.isEmpty()"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO00OOO()Ljava/util/Set;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Ljava/lang/Iterable;

    .line 36
    .line 37
    new-instance v1, Ljava/util/ArrayList;

    .line 38
    .line 39
    const/16 v2, 0xa

    .line 40
    .line 41
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    .line 47
    .line 48
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_2

    .line 57
    .line 58
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 63
    .line 64
    new-instance v3, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 65
    .line 66
    invoke-direct {v3}, Lcom/intsig/camscanner/datastruct/DocumentListItem;-><init>()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇oOO8O8()I

    .line 70
    .line 71
    .line 72
    move-result v4

    .line 73
    iput v4, v3, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo0:I

    .line 74
    .line 75
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 76
    .line 77
    .line 78
    move-result-wide v4

    .line 79
    iput-wide v4, v3, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 80
    .line 81
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v4

    .line 85
    iput-object v4, v3, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 86
    .line 87
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->oo88o8O()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    iput-object v4, v3, Lcom/intsig/camscanner/datastruct/DocumentListItem;->oOo〇8o008:Ljava/lang/String;

    .line 92
    .line 93
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->〇00〇8()J

    .line 94
    .line 95
    .line 96
    move-result-wide v4

    .line 97
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v4

    .line 101
    iput-object v4, v3, Lcom/intsig/camscanner/datastruct/DocumentListItem;->〇080OO8〇0:Ljava/lang/String;

    .line 102
    .line 103
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v2

    .line 111
    iput-object v2, v3, Lcom/intsig/camscanner/datastruct/DocumentListItem;->〇0O:Ljava/lang/String;

    .line 112
    .line 113
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_2
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->OOO(Ljava/util/List;)V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇00O0O0(Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO80:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇08O8o〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "tabItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "selectDocItems"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "selectDocItemIds"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/TabItem;->getMItemId()I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    const-string v0, "null cannot be cast to non-null type java.util.ArrayList<kotlin.Long>"

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    const/4 v2, 0x1

    .line 24
    const-string v3, "encrypt"

    .line 25
    .line 26
    const-string v4, "from_part"

    .line 27
    .line 28
    const-string v5, "CSSelectModeMore"

    .line 29
    .line 30
    packed-switch p1, :pswitch_data_0

    .line 31
    .line 32
    .line 33
    goto/16 :goto_0

    .line 34
    .line 35
    :pswitch_0
    new-instance p1, Ljava/util/ArrayList;

    .line 36
    .line 37
    move-object p3, p2

    .line 38
    check-cast p3, Ljava/util/Collection;

    .line 39
    .line 40
    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 41
    .line 42
    .line 43
    check-cast p2, Ljava/lang/Iterable;

    .line 44
    .line 45
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->oO(Ljava/lang/Iterable;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    check-cast p2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 50
    .line 51
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O08000(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :pswitch_1
    const-string p1, "delete_history"

    .line 61
    .line 62
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇8(Ljava/util/Set;)V

    .line 66
    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :pswitch_2
    new-instance p1, Ljava/util/ArrayList;

    .line 71
    .line 72
    move-object p3, p2

    .line 73
    check-cast p3, Ljava/util/Collection;

    .line 74
    .line 75
    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 76
    .line 77
    .line 78
    check-cast p2, Ljava/lang/Iterable;

    .line 79
    .line 80
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->oO(Ljava/lang/Iterable;)Ljava/lang/Object;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    check-cast p2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 85
    .line 86
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o0O0(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    goto/16 :goto_0

    .line 94
    .line 95
    :pswitch_3
    new-instance p1, Ljava/util/ArrayList;

    .line 96
    .line 97
    move-object p3, p2

    .line 98
    check-cast p3, Ljava/util/Collection;

    .line 99
    .line 100
    invoke-direct {p1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 101
    .line 102
    .line 103
    check-cast p2, Ljava/lang/Iterable;

    .line 104
    .line 105
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->oO(Ljava/lang/Iterable;)Ljava/lang/Object;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    check-cast p2, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 110
    .line 111
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p2

    .line 115
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O8O〇(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto/16 :goto_0

    .line 119
    .line 120
    :pswitch_4
    const-string p1, "add_shortcut"

    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 123
    .line 124
    .line 125
    move-result-object p2

    .line 126
    invoke-static {v5, p1, v4, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇〇0〇(Ljava/util/Set;)V

    .line 130
    .line 131
    .line 132
    goto/16 :goto_0

    .line 133
    .line 134
    :pswitch_5
    move-object p1, p3

    .line 135
    check-cast p1, Ljava/util/Collection;

    .line 136
    .line 137
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 138
    .line 139
    .line 140
    move-result-object p1

    .line 141
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    check-cast p1, Ljava/util/ArrayList;

    .line 145
    .line 146
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object p2

    .line 150
    const-string v0, "save_to_gallery"

    .line 151
    .line 152
    invoke-static {v5, v0, v4, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    sget-object p2, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 156
    .line 157
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->o800o8O(Ljava/util/List;)Z

    .line 158
    .line 159
    .line 160
    move-result p2

    .line 161
    if-eqz p2, :cond_1

    .line 162
    .line 163
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 164
    .line 165
    .line 166
    move-result p2

    .line 167
    if-ne p2, v2, :cond_0

    .line 168
    .line 169
    sget-object p2, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 170
    .line 171
    invoke-virtual {p2}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 172
    .line 173
    .line 174
    move-result-object p2

    .line 175
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    const-string p3, "docItemIds[0]"

    .line 180
    .line 181
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    check-cast p1, Ljava/lang/Number;

    .line 185
    .line 186
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    .line 187
    .line 188
    .line 189
    move-result-wide v0

    .line 190
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇〇0〇(Landroid/content/Context;J)I

    .line 191
    .line 192
    .line 193
    move-result p1

    .line 194
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object p1

    .line 198
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 199
    .line 200
    invoke-direct {p0, p2, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    goto :goto_0

    .line 204
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 205
    .line 206
    const-string p2, "id_mode_batch"

    .line 207
    .line 208
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooOO(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    goto :goto_0

    .line 212
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇〇0o(Ljava/util/Set;)V

    .line 216
    .line 217
    .line 218
    goto :goto_0

    .line 219
    :pswitch_6
    move-object p1, p3

    .line 220
    check-cast p1, Ljava/util/Collection;

    .line 221
    .line 222
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 223
    .line 224
    .line 225
    move-result-object p1

    .line 226
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    check-cast p1, Ljava/util/ArrayList;

    .line 230
    .line 231
    const-string p1, "upload_fax"

    .line 232
    .line 233
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-static {v5, p1, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oO(Ljava/util/Set;Ljava/util/Set;)V

    .line 241
    .line 242
    .line 243
    goto :goto_0

    .line 244
    :pswitch_7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    const-string p2, "label"

    .line 249
    .line 250
    invoke-static {v5, p2, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    .line 252
    .line 253
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇0000OOO(Ljava/lang/String;)V

    .line 254
    .line 255
    .line 256
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇〇〇0〇〇0(Ljava/util/Set;)V

    .line 257
    .line 258
    .line 259
    goto :goto_0

    .line 260
    :pswitch_8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object p1

    .line 264
    invoke-static {v5, v3, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    invoke-direct {p0, v1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0OOo〇0(ZLjava/util/Set;)V

    .line 268
    .line 269
    .line 270
    goto :goto_0

    .line 271
    :pswitch_9
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O〇O〇oO()Ljava/lang/String;

    .line 272
    .line 273
    .line 274
    move-result-object p1

    .line 275
    invoke-static {v5, v3, v4, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    invoke-direct {p0, v2, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0OOo〇0(ZLjava/util/Set;)V

    .line 279
    .line 280
    .line 281
    :goto_0
    return-void

    .line 282
    nop

    .line 283
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final 〇o0O0O8(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->ooo0〇O88O(Lcom/intsig/camscanner/datastruct/DocItem;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇oOO8O8()V
    .locals 13

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mMainBottomListener"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇o〇()[Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;

    .line 17
    .line 18
    if-nez v3, :cond_1

    .line 19
    .line 20
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    move-object v1, v3

    .line 25
    :goto_0
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController$IMainBottomEditListener;->〇8o8o〇()Ljava/util/Set;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    new-instance v2, Ljava/util/ArrayList;

    .line 30
    .line 31
    array-length v3, v0

    .line 32
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 33
    .line 34
    .line 35
    array-length v3, v0

    .line 36
    const/4 v4, 0x0

    .line 37
    const/4 v5, 0x0

    .line 38
    :goto_1
    if-ge v5, v3, :cond_2

    .line 39
    .line 40
    aget-object v6, v0, v5

    .line 41
    .line 42
    invoke-virtual {v6}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->getTextResId()I

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v6

    .line 50
    invoke-interface {v2, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    add-int/lit8 v5, v5, 0x1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_2
    new-array v3, v4, [Ljava/lang/Integer;

    .line 57
    .line 58
    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    move-object v6, v2

    .line 63
    check-cast v6, [Ljava/lang/Integer;

    .line 64
    .line 65
    new-instance v2, Ljava/util/ArrayList;

    .line 66
    .line 67
    array-length v3, v0

    .line 68
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 69
    .line 70
    .line 71
    array-length v3, v0

    .line 72
    const/4 v5, 0x0

    .line 73
    :goto_2
    if-ge v5, v3, :cond_3

    .line 74
    .line 75
    aget-object v7, v0, v5

    .line 76
    .line 77
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->getImageResId()I

    .line 78
    .line 79
    .line 80
    move-result v7

    .line 81
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object v7

    .line 85
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    add-int/lit8 v5, v5, 0x1

    .line 89
    .line 90
    goto :goto_2

    .line 91
    :cond_3
    new-array v3, v4, [Ljava/lang/Integer;

    .line 92
    .line 93
    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object v2

    .line 97
    move-object v8, v2

    .line 98
    check-cast v8, [Ljava/lang/Integer;

    .line 99
    .line 100
    new-instance v2, Ljava/util/ArrayList;

    .line 101
    .line 102
    array-length v3, v0

    .line 103
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 104
    .line 105
    .line 106
    array-length v3, v0

    .line 107
    const/4 v5, 0x0

    .line 108
    :goto_3
    if-ge v5, v3, :cond_4

    .line 109
    .line 110
    aget-object v7, v0, v5

    .line 111
    .line 112
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->isSetGray()Z

    .line 113
    .line 114
    .line 115
    move-result v7

    .line 116
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 117
    .line 118
    .line 119
    move-result-object v7

    .line 120
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    add-int/lit8 v5, v5, 0x1

    .line 124
    .line 125
    goto :goto_3

    .line 126
    :cond_4
    new-array v3, v4, [Ljava/lang/Boolean;

    .line 127
    .line 128
    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    move-object v9, v2

    .line 133
    check-cast v9, [Ljava/lang/Boolean;

    .line 134
    .line 135
    new-instance v2, Ljava/util/ArrayList;

    .line 136
    .line 137
    array-length v3, v0

    .line 138
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .line 140
    .line 141
    array-length v3, v0

    .line 142
    const/4 v5, 0x0

    .line 143
    :goto_4
    if-ge v5, v3, :cond_5

    .line 144
    .line 145
    aget-object v7, v0, v5

    .line 146
    .line 147
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->getToastResId()I

    .line 148
    .line 149
    .line 150
    move-result v7

    .line 151
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 152
    .line 153
    .line 154
    move-result-object v7

    .line 155
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    add-int/lit8 v5, v5, 0x1

    .line 159
    .line 160
    goto :goto_4

    .line 161
    :cond_5
    new-array v3, v4, [Ljava/lang/Integer;

    .line 162
    .line 163
    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    move-object v11, v2

    .line 168
    check-cast v11, [Ljava/lang/Integer;

    .line 169
    .line 170
    new-instance v2, Ljava/util/ArrayList;

    .line 171
    .line 172
    array-length v3, v0

    .line 173
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 174
    .line 175
    .line 176
    array-length v3, v0

    .line 177
    const/4 v5, 0x0

    .line 178
    :goto_5
    if-ge v5, v3, :cond_6

    .line 179
    .line 180
    aget-object v7, v0, v5

    .line 181
    .line 182
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->isShow()Z

    .line 183
    .line 184
    .line 185
    move-result v7

    .line 186
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 187
    .line 188
    .line 189
    move-result-object v7

    .line 190
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    add-int/lit8 v5, v5, 0x1

    .line 194
    .line 195
    goto :goto_5

    .line 196
    :cond_6
    new-array v3, v4, [Ljava/lang/Boolean;

    .line 197
    .line 198
    invoke-interface {v2, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 199
    .line 200
    .line 201
    move-result-object v2

    .line 202
    move-object v10, v2

    .line 203
    check-cast v10, [Ljava/lang/Boolean;

    .line 204
    .line 205
    new-instance v2, Ljava/util/ArrayList;

    .line 206
    .line 207
    array-length v3, v0

    .line 208
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 209
    .line 210
    .line 211
    array-length v3, v0

    .line 212
    const/4 v5, 0x0

    .line 213
    :goto_6
    if-ge v5, v3, :cond_7

    .line 214
    .line 215
    aget-object v7, v0, v5

    .line 216
    .line 217
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;->getDotTextItem()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;

    .line 218
    .line 219
    .line 220
    move-result-object v7

    .line 221
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 222
    .line 223
    .line 224
    add-int/lit8 v5, v5, 0x1

    .line 225
    .line 226
    goto :goto_6

    .line 227
    :cond_7
    new-array v0, v4, [Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;

    .line 228
    .line 229
    invoke-interface {v2, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    move-object v12, v0

    .line 234
    check-cast v12, [Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;

    .line 235
    .line 236
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 237
    .line 238
    move-object v7, v8

    .line 239
    invoke-virtual/range {v5 .. v12}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->Oooo8o0〇([Ljava/lang/Integer;[Ljava/lang/Integer;[Ljava/lang/Integer;[Ljava/lang/Boolean;[Ljava/lang/Boolean;[Ljava/lang/Integer;[Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;)V

    .line 240
    .line 241
    .line 242
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 243
    .line 244
    check-cast v1, Ljava/util/Collection;

    .line 245
    .line 246
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 247
    .line 248
    .line 249
    move-result v1

    .line 250
    xor-int/lit8 v1, v1, 0x1

    .line 251
    .line 252
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->setItemEnabled(Z)V

    .line 253
    .line 254
    .line 255
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 256
    .line 257
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇80〇808〇O()V

    .line 258
    .line 259
    .line 260
    return-void
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

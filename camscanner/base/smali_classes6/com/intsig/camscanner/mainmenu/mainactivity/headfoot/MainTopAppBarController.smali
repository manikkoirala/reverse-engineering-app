.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;
.super Ljava/lang/Object;
.source "MainTopAppBarController.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainDocFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binding"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/FragmentMainDocPageBinding;

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController$mTopListMenu$2;

    .line 19
    .line 20
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController$mTopListMenu$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;)V

    .line 21
    .line 22
    .line 23
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇o〇:Lkotlin/Lazy;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇080()Lcom/intsig/menu/PopupListMenu;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇o〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/menu/PopupListMenu;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇o00〇〇Oo()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇(Landroid/view/View;Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mAnchor"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "clickCallback"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080()Lcom/intsig/menu/PopupListMenu;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupListMenu;->o〇O8〇〇o(Z)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080()Lcom/intsig/menu/PopupListMenu;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const/16 v2, 0x82

    .line 34
    .line 35
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    int-to-float v1, v1

    .line 40
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupListMenu;->〇O888o0o(F)V

    .line 41
    .line 42
    .line 43
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080()Lcom/intsig/menu/PopupListMenu;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    invoke-virtual {v0, p2}, Lcom/intsig/menu/PopupListMenu;->o800o8O(Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopAppBarController;->〇080()Lcom/intsig/menu/PopupListMenu;

    .line 53
    .line 54
    .line 55
    move-result-object p2

    .line 56
    if-eqz p2, :cond_3

    .line 57
    .line 58
    const/4 v0, 0x5

    .line 59
    invoke-virtual {p2, p1, v0}, Lcom/intsig/menu/PopupListMenu;->O〇8O8〇008(Landroid/view/View;I)V

    .line 60
    .line 61
    .line 62
    :cond_3
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MainFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$WhenMappings;,
        Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$VipIconShaker;,
        Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$UnsubscribeScaffoldMeRedDot;,
        Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$Companion;,
        Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$MainHomeBottomIndexChangeEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final o0OoOOo0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇0O〇O00O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field public O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

.field private final O88O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

.field private OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

.field private OO〇00〇8oO:Z

.field private final Oo0〇Ooo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo80:Z

.field private Ooo08:Z

.field private O〇08oOOO0:Ljava/lang/String;

.field private O〇o88o08〇:I

.field private o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

.field private o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

.field private final o8oOOo:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Z

.field private oOO〇〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private oOo0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field private final oOo〇8o008:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO〇8O8oOo:I

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooO:Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;

.field private ooo0〇〇O:Ljava/lang/String;

.field private final o〇00O:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroidx/fragment/app/Fragment;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇00O0:Ljava/lang/String;

.field private 〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

.field private 〇08〇o0O:Ljava/lang/String;

.field private 〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

.field private 〇OO8ooO8〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

.field private 〇OO〇00〇0O:Landroid/widget/Toast;

.field private final 〇O〇〇O8:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;

.field private 〇〇08O:Lcom/intsig/camscanner/signin/SignInIconAnimation;

.field private 〇〇o〇:J

.field private 〇〇〇0o〇〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O〇O00O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "MainFragment::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Landroid/util/SparseArray;

    .line 5
    .line 6
    const/4 v1, 0x4

    .line 7
    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mCloudSpaceReward$2;->o0:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mCloudSpaceReward$2;

    .line 13
    .line 14
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOo〇8o008:Lkotlin/Lazy;

    .line 19
    .line 20
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mGiftTipControl$2;

    .line 21
    .line 22
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mGiftTipControl$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8oOOo:Lkotlin/Lazy;

    .line 30
    .line 31
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 32
    .line 33
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mainTopBarController$2;

    .line 34
    .line 35
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mainTopBarController$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 36
    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇O〇〇O8:Lkotlin/Lazy;

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$1;

    .line 45
    .line 46
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 47
    .line 48
    .line 49
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$2;

    .line 50
    .line 51
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 52
    .line 53
    .line 54
    invoke-static {v0, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    const-class v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 59
    .line 60
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$3;

    .line 65
    .line 66
    invoke-direct {v3, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 67
    .line 68
    .line 69
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$4;

    .line 70
    .line 71
    const/4 v5, 0x0

    .line 72
    invoke-direct {v4, v5, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 73
    .line 74
    .line 75
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$5;

    .line 76
    .line 77
    invoke-direct {v5, p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 78
    .line 79
    .line 80
    invoke-static {p0, v2, v3, v4, v5}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O88O:Lkotlin/Lazy;

    .line 85
    .line 86
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$csLinkParser$2;

    .line 87
    .line 88
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$csLinkParser$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 89
    .line 90
    .line 91
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$docShutterGuidePopClient$2;

    .line 98
    .line 99
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$docShutterGuidePopClient$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 100
    .line 101
    .line 102
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇oO:Lkotlin/Lazy;

    .line 107
    .line 108
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mLoadingDialog$2;

    .line 109
    .line 110
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$mLoadingDialog$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 111
    .line 112
    .line 113
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO:Lkotlin/Lazy;

    .line 118
    .line 119
    new-instance v0, Ljava/util/ArrayList;

    .line 120
    .line 121
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .line 123
    .line 124
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo0〇Ooo:Ljava/util/ArrayList;

    .line 125
    .line 126
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    const/high16 v1, 0x43440000    # 196.0f

    .line 135
    .line 136
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 137
    .line 138
    .line 139
    move-result v1

    .line 140
    sub-int/2addr v0, v1

    .line 141
    iput v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO〇8O8oOo:I

    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O008o8oo(Lcom/intsig/utils/CsResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/MainActAction$ESignLinkQueryDownloadData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v1, 0x0

    .line 2
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$1;

    .line 3
    .line 4
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 5
    .line 6
    .line 7
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$2;

    .line 8
    .line 9
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 10
    .line 11
    .line 12
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$3;

    .line 13
    .line 14
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryDownloadResult$3;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 15
    .line 16
    .line 17
    const/4 v5, 0x1

    .line 18
    const/4 v6, 0x0

    .line 19
    move-object v0, p1

    .line 20
    invoke-static/range {v0 .. v6}, Lcom/intsig/utils/CsResultKt;->〇o00〇〇Oo(Lcom/intsig/utils/CsResult;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O008oO0(Landroid/content/Intent;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "from_router"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-string v0, "uri"

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Landroid/net/Uri;

    .line 18
    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 22
    .line 23
    new-instance v1, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v2, "router from out uri = "

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v0, p1}, Lcom/intsig/router/CSRouterManager;->O8(Landroid/content/Context;Landroid/net/Uri;)V

    .line 48
    .line 49
    .line 50
    :cond_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O00OoO〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/util/PermissionUtil;->O8ooOoo〇(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 18
    .line 19
    const-string v1, "cn, not request storage permission auto"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 26
    .line 27
    const-string v1, "not cn, request storage permission auto"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇〇o8O()V

    .line 33
    .line 34
    .line 35
    :cond_1
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O00〇o00(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo0O〇8800(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    new-instance v1, Lo〇00O/oo88o8O;

    .line 18
    .line 19
    invoke-direct {v1, p0, p1}, Lo〇00O/oo88o8O;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 20
    .line 21
    .line 22
    invoke-static {p2, v0, v1}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->〇〇o0〇8()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O08〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O0O0〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇o88o08〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O0oO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x3

    .line 12
    if-eq v0, v1, :cond_2

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->〇〇808〇(Landroid/content/Context;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8〇o〇o()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇o00〇〇Oo()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 43
    .line 44
    const/4 v2, 0x1

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O〇(IZ)V

    .line 46
    .line 47
    .line 48
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 49
    .line 50
    const-string v1, "go checkEduInfo"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-static {v0}, Lcom/intsig/camscanner/edu/EduGroupHelper;->〇o〇(Landroid/content/Context;)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O0〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO8o〇08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O00〇o00(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O0〇O80ooo(Landroid/os/Bundle;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    const/4 v2, 0x2

    .line 5
    const/4 v3, 0x1

    .line 6
    const/4 v4, 0x0

    .line 7
    const/4 v5, 0x0

    .line 8
    const-string v6, "mPagerAdapter"

    .line 9
    .line 10
    if-eqz p1, :cond_4

    .line 11
    .line 12
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 13
    .line 14
    if-nez v7, :cond_0

    .line 15
    .line 16
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    move-object v7, v5

    .line 20
    :cond_0
    invoke-virtual {v7, v4}, Lcom/intsig/adapter/BaseViewPager2Adapter;->〇O〇(I)Landroidx/fragment/app/Fragment;

    .line 21
    .line 22
    .line 23
    move-result-object v7

    .line 24
    check-cast v7, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 25
    .line 26
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 27
    .line 28
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 29
    .line 30
    if-nez v7, :cond_1

    .line 31
    .line 32
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    move-object v7, v5

    .line 36
    :cond_1
    invoke-virtual {v7, v3}, Lcom/intsig/adapter/BaseViewPager2Adapter;->〇O〇(I)Landroidx/fragment/app/Fragment;

    .line 37
    .line 38
    .line 39
    move-result-object v7

    .line 40
    check-cast v7, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 41
    .line 42
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 43
    .line 44
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 45
    .line 46
    if-nez v7, :cond_2

    .line 47
    .line 48
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    move-object v7, v5

    .line 52
    :cond_2
    invoke-virtual {v7, v2}, Lcom/intsig/adapter/BaseViewPager2Adapter;->〇O〇(I)Landroidx/fragment/app/Fragment;

    .line 53
    .line 54
    .line 55
    move-result-object v7

    .line 56
    check-cast v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 57
    .line 58
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 59
    .line 60
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 61
    .line 62
    if-nez v7, :cond_3

    .line 63
    .line 64
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    move-object v7, v5

    .line 68
    :cond_3
    invoke-virtual {v7, v1}, Lcom/intsig/adapter/BaseViewPager2Adapter;->〇O〇(I)Landroidx/fragment/app/Fragment;

    .line 69
    .line 70
    .line 71
    move-result-object v7

    .line 72
    check-cast v7, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 73
    .line 74
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 75
    .line 76
    :cond_4
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 77
    .line 78
    if-nez v7, :cond_5

    .line 79
    .line 80
    sget-object v7, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->oO00〇o:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment$Companion;

    .line 81
    .line 82
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment$Companion;->〇o00〇〇Oo()Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 87
    .line 88
    :cond_5
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 89
    .line 90
    if-nez v7, :cond_6

    .line 91
    .line 92
    sget-object v8, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment$Companion;

    .line 93
    .line 94
    const/4 v9, 0x0

    .line 95
    const/4 v10, 0x0

    .line 96
    const/4 v11, 0x0

    .line 97
    const/4 v12, 0x0

    .line 98
    const/4 v13, 0x0

    .line 99
    const/4 v14, 0x0

    .line 100
    const/16 v15, 0x3f

    .line 101
    .line 102
    const/16 v16, 0x0

    .line 103
    .line 104
    invoke-static/range {v8 .. v16}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment$Companion;->〇o〇(Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment$Companion;Lcom/intsig/camscanner/datastruct/FolderItem;ZZZZZILjava/lang/Object;)Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 105
    .line 106
    .line 107
    move-result-object v7

    .line 108
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 109
    .line 110
    :cond_6
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 111
    .line 112
    if-nez v7, :cond_9

    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇0o〇o8()Z

    .line 115
    .line 116
    .line 117
    move-result v7

    .line 118
    if-nez v7, :cond_8

    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->o〇0()Z

    .line 121
    .line 122
    .line 123
    move-result v7

    .line 124
    if-eqz v7, :cond_7

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_7
    sget-object v7, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment$Companion;

    .line 128
    .line 129
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 130
    .line 131
    .line 132
    move-result-object v7

    .line 133
    goto :goto_1

    .line 134
    :cond_8
    :goto_0
    sget-object v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;

    .line 135
    .line 136
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 137
    .line 138
    .line 139
    move-result-object v7

    .line 140
    :goto_1
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 141
    .line 142
    :cond_9
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 143
    .line 144
    if-nez v7, :cond_a

    .line 145
    .line 146
    sget-object v7, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;

    .line 147
    .line 148
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 149
    .line 150
    .line 151
    move-result-object v7

    .line 152
    iput-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 153
    .line 154
    :cond_a
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 155
    .line 156
    if-eqz v7, :cond_b

    .line 157
    .line 158
    new-instance v8, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$resetFragment$1;

    .line 159
    .line 160
    invoke-direct {v8, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$resetFragment$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v7, v8}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->O8o0〇(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment$OnMainHomeFragmentCallback;)V

    .line 164
    .line 165
    .line 166
    :cond_b
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 167
    .line 168
    iget-object v8, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 169
    .line 170
    invoke-virtual {v7, v4, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 171
    .line 172
    .line 173
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 174
    .line 175
    iget-object v7, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 176
    .line 177
    invoke-virtual {v4, v3, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 178
    .line 179
    .line 180
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 181
    .line 182
    iget-object v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 183
    .line 184
    invoke-virtual {v3, v2, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 185
    .line 186
    .line 187
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 188
    .line 189
    iget-object v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 190
    .line 191
    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 192
    .line 193
    .line 194
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 195
    .line 196
    if-nez v1, :cond_c

    .line 197
    .line 198
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    goto :goto_2

    .line 202
    :cond_c
    move-object v5, v1

    .line 203
    :goto_2
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 204
    .line 205
    invoke-virtual {v5, v1}, Lcom/intsig/adapter/BaseViewPager2Adapter;->〇O00(Landroid/util/SparseArray;)V

    .line 206
    .line 207
    .line 208
    return-void
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final O80(Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Oo〇o(Z)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇()Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const/4 v0, 0x0

    .line 13
    if-nez p1, :cond_2

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 p1, 0x0

    .line 37
    :goto_0
    if-eqz p1, :cond_1

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    goto :goto_2

    .line 42
    :cond_2
    :goto_1
    const/4 p1, 0x1

    .line 43
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v1, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇o00〇〇Oo(ZZ)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->O8(I)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇o〇(Z)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇()Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_3

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 71
    .line 72
    if-eqz p1, :cond_3

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->O008o8oo()V

    .line 75
    .line 76
    .line 77
    :cond_3
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final O80OO()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇o〇:Landroid/view/ViewStub;

    .line 6
    .line 7
    const-string v1, "mBinding.viewStubQrScanGuide"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 14
    .line 15
    .line 16
    iput-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Ooo08:Z

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const v1, 0x7f0a1a52

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 34
    .line 35
    new-instance v1, Lo〇00O/OoO8;

    .line 36
    .line 37
    invoke-direct {v1, p0}, Lo〇00O/OoO8;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8〇8O()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const v1, 0x7f0a0b75

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    check-cast v0, Landroid/widget/LinearLayout;

    .line 62
    .line 63
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    const v2, 0x7f0a10db

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    check-cast v1, Lcom/intsig/comm/widget/CustomTextView;

    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 81
    .line 82
    .line 83
    move-result-object v2

    .line 84
    const v3, 0x7f131287

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    .line 93
    .line 94
    sget-object v2, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->TOP:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 95
    .line 96
    invoke-virtual {v1, v2}, Lcom/intsig/comm/widget/CustomTextView;->setArrowDirection(Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;)V

    .line 97
    .line 98
    .line 99
    const/4 v2, 0x0

    .line 100
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    new-instance v2, Lo〇00O/o800o8O;

    .line 108
    .line 109
    invoke-direct {v2, p0, v1}, Lo〇00O/o800o8O;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/comm/widget/CustomTextView;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 113
    .line 114
    .line 115
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o0〇(Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V
    .locals 8

    .line 1
    and-int/lit8 v0, p8, 0x2

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p0

    .line 6
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOo0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v0, p0

    .line 10
    move-object v1, p2

    .line 11
    :goto_0
    and-int/lit8 v2, p8, 0x4

    .line 12
    .line 13
    if-eqz v2, :cond_1

    .line 14
    .line 15
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    move-object v2, p3

    .line 19
    :goto_1
    and-int/lit8 v3, p8, 0x8

    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    if-eqz v3, :cond_2

    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    goto :goto_2

    .line 26
    :cond_2
    move v3, p4

    .line 27
    :goto_2
    and-int/lit8 v5, p8, 0x10

    .line 28
    .line 29
    if-eqz v5, :cond_3

    .line 30
    .line 31
    const v5, 0x138d0

    .line 32
    .line 33
    .line 34
    goto :goto_3

    .line 35
    :cond_3
    move v5, p5

    .line 36
    :goto_3
    and-int/lit8 v6, p8, 0x20

    .line 37
    .line 38
    if-eqz v6, :cond_4

    .line 39
    .line 40
    const/4 v6, -0x1

    .line 41
    goto :goto_4

    .line 42
    :cond_4
    move v6, p6

    .line 43
    :goto_4
    and-int/lit8 v7, p8, 0x40

    .line 44
    .line 45
    if-eqz v7, :cond_5

    .line 46
    .line 47
    goto :goto_5

    .line 48
    :cond_5
    move v4, p7

    .line 49
    :goto_5
    move-object p2, p0

    .line 50
    move-object p3, p1

    .line 51
    move-object p4, v1

    .line 52
    move-object p5, v2

    .line 53
    move p6, v3

    .line 54
    move p7, v5

    .line 55
    move/from16 p8, v6

    .line 56
    .line 57
    move/from16 p9, v4

    .line 58
    .line 59
    invoke-virtual/range {p2 .. p9}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZ)V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
.end method

.method private final O8O(Landroid/content/Intent;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "autoGoToLoginPage intent == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    const-string v0, "key_auto_go_to_login_page"

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-nez p1, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 22
    .line 23
    const-string v0, "autoGoToLoginPage"

    .line 24
    .line 25
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/16 p1, 0x64

    .line 29
    .line 30
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O8o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    const-string v0, "screenshot_click"

    .line 11
    .line 12
    invoke-static {p2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo0O〇8800(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O8o0〇(Landroid/content/Intent;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    if-eqz v1, :cond_1

    .line 6
    .line 7
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 8
    .line 9
    const-string v3, "batch handle images finish, go to view doc"

    .line 10
    .line 11
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v2, "view_doc_uri"

    .line 15
    .line 16
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    check-cast v2, Landroid/net/Uri;

    .line 21
    .line 22
    const-string v3, "view_pdf_doc"

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    sget-object v5, Lcom/intsig/camscanner/util/CONSTANT;->〇8o8o〇:Ljava/lang/String;

    .line 30
    .line 31
    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 32
    .line 33
    .line 34
    move-result v5

    .line 35
    if-eqz v3, :cond_0

    .line 36
    .line 37
    if-eqz v2, :cond_0

    .line 38
    .line 39
    sget-object v6, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity;->ooo0〇〇O:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;

    .line 40
    .line 41
    iget-object v7, v0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    const-string v1, "mActivity"

    .line 44
    .line 45
    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 49
    .line 50
    .line 51
    move-result-wide v8

    .line 52
    const/4 v10, 0x0

    .line 53
    const/4 v11, 0x0

    .line 54
    const/4 v12, 0x0

    .line 55
    const/4 v13, 0x0

    .line 56
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 57
    .line 58
    .line 59
    move-result-object v14

    .line 60
    const/16 v15, 0x3c

    .line 61
    .line 62
    const/16 v16, 0x0

    .line 63
    .line 64
    invoke-static/range {v6 .. v16}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;->O8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewActivity$Companion;Landroid/app/Activity;JLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    new-instance v3, Landroid/content/Intent;

    .line 69
    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 71
    .line 72
    .line 73
    move-result-object v6

    .line 74
    const-class v7, Lcom/intsig/camscanner/DocumentActivity;

    .line 75
    .line 76
    const-string v8, "android.intent.action.VIEW"

    .line 77
    .line 78
    invoke-direct {v3, v8, v2, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    .line 80
    .line 81
    const-string v2, "extra_offline_folder"

    .line 82
    .line 83
    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 84
    .line 85
    .line 86
    const/4 v2, 0x0

    .line 87
    const-string v6, "extra_folder_id"

    .line 88
    .line 89
    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    .line 91
    .line 92
    sget-object v2, Lcom/intsig/camscanner/util/CONSTANT;->〇8o8o〇:Ljava/lang/String;

    .line 93
    .line 94
    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    .line 96
    .line 97
    const-string v2, "constant_is_show_doc_location"

    .line 98
    .line 99
    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v3}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 107
    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 111
    .line 112
    const-string v2, "data == null"

    .line 113
    .line 114
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    :goto_0
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic O8〇8〇O80(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8〇〇()Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O8〇o0〇〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇o〇:Landroid/view/ViewStub;

    .line 11
    .line 12
    const-string v0, "mBinding.viewStubQrScanGuide"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 19
    .line 20
    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Ooo08:Z

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO0o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x1

    .line 13
    if-ne v1, v4, :cond_4

    .line 14
    .line 15
    sget-object v1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->oo88o8O()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oO0o()V

    .line 30
    .line 31
    .line 32
    :cond_1
    return-void

    .line 33
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShareDirOpen()Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_5

    .line 42
    .line 43
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O0o〇〇o()Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    if-nez v1, :cond_5

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-eqz v0, :cond_5

    .line 58
    .line 59
    new-instance v0, Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;

    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 62
    .line 63
    const-string v4, "mActivity"

    .line 64
    .line 65
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 69
    .line 70
    if-eqz v4, :cond_3

    .line 71
    .line 72
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    if-eqz v4, :cond_3

    .line 77
    .line 78
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇8o8o〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    :cond_3
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkShowGuideWhenClickDocTab$1;

    .line 83
    .line 84
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkShowGuideWhenClickDocTab$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 85
    .line 86
    .line 87
    invoke-direct {v0, v1, p0, v2, v4}, Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;-><init>(Landroid/content/Context;Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/datastruct/FolderItem;Lkotlin/jvm/functions/Function2;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v3}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setCancelable(Z)V

    .line 91
    .line 92
    .line 93
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooO:Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;->show()V

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_4
    if-nez v1, :cond_5

    .line 100
    .line 101
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 102
    .line 103
    const-string v1, "checkShowGuideWhenClickDocTab isReduceSeniorDirGuide:true"

    .line 104
    .line 105
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl;->〇〇888()Z

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-eqz v0, :cond_5

    .line 113
    .line 114
    sget-object v0, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->〇080:Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;

    .line 115
    .line 116
    invoke-static {v0, v3, v4, v2}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->〇O888o0o(Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;ZILjava/lang/Object;)V

    .line 117
    .line 118
    .line 119
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    if-eqz v0, :cond_5

    .line 124
    .line 125
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->〇〇08〇0oo0()V

    .line 126
    .line 127
    .line 128
    :cond_5
    :goto_0
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final OO0〇O()V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initMsgReceive$callbackFlow$1;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initMsgReceive$callbackFlow$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 5
    .line 6
    .line 7
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->oO80(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v3, "viewLifecycleOwner"

    .line 16
    .line 17
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-static {v2}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 21
    .line 22
    .line 23
    move-result-object v4

    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initMsgReceive$1;

    .line 27
    .line 28
    invoke-direct {v7, v0, p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initMsgReceive$1;-><init>(Lkotlinx/coroutines/flow/Flow;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 29
    .line 30
    .line 31
    const/4 v8, 0x3

    .line 32
    const/4 v9, 0x0

    .line 33
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO8〇O8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->OooO〇()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OOO0o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$this_run"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OOOo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$this_run"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OO〇80oO〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO〇00〇8oO:Lcom/airbnb/lottie/LottieAnimationView;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 14
    .line 15
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;-><init>(Lcom/airbnb/lottie/LottieAnimationView;Landroid/widget/TextView;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OooO〇080(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Oo0O〇8800(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "mBinding.mainIncludeScreenshot.root"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->getType()Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v1, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;->SCREENSHOT:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;

    .line 25
    .line 26
    if-ne v0, v1, :cond_0

    .line 27
    .line 28
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇80〇808〇O()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    new-instance v2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "record conceal screenshot, path = "

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇80〇808〇O()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooO0(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :cond_0
    const/4 p1, 0x0

    .line 62
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 65
    .line 66
    if-eqz p1, :cond_1

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->o〇〇8〇〇()V

    .line 69
    .line 70
    .line 71
    :cond_1
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final OoOO〇()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->oo()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    const/4 v2, 0x1

    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇o00〇〇Oo(ZZ)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo0O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇O8〇〇o8〇(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OooO〇080(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p2

    .line 6
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇080OO8〇0:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇080OO8〇0:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->o〇00O:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇080OO8〇0:I

    .line 32
    .line 33
    const/4 v3, 0x1

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    iget-wide v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇08O〇00〇o:J

    .line 39
    .line 40
    iget-wide v6, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->OO:J

    .line 41
    .line 42
    iget-object p1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 43
    .line 44
    check-cast p1, Ljava/lang/String;

    .line 45
    .line 46
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->o0:Ljava/lang/Object;

    .line 47
    .line 48
    check-cast v2, Ljava/lang/String;

    .line 49
    .line 50
    invoke-static {p2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    move-object p2, p1

    .line 54
    move-object p1, v2

    .line 55
    goto :goto_1

    .line 56
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 57
    .line 58
    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 59
    .line 60
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    throw p1

    .line 64
    :cond_2
    invoke-static {p2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 65
    .line 66
    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 68
    .line 69
    .line 70
    move-result-wide v4

    .line 71
    const-wide/16 v6, 0x7530

    .line 72
    .line 73
    const/4 p2, 0x0

    .line 74
    :cond_3
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 75
    .line 76
    .line 77
    move-result-wide v8

    .line 78
    sub-long/2addr v8, v4

    .line 79
    cmp-long v2, v8, v6

    .line 80
    .line 81
    if-gez v2, :cond_9

    .line 82
    .line 83
    const/4 v2, 0x0

    .line 84
    if-eqz p2, :cond_5

    .line 85
    .line 86
    invoke-static {p2}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 87
    .line 88
    .line 89
    move-result v8

    .line 90
    if-eqz v8, :cond_4

    .line 91
    .line 92
    goto :goto_2

    .line 93
    :cond_4
    const/4 v8, 0x0

    .line 94
    goto :goto_3

    .line 95
    :cond_5
    :goto_2
    const/4 v8, 0x1

    .line 96
    :goto_3
    if-eqz v8, :cond_9

    .line 97
    .line 98
    invoke-static {p1}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object p2

    .line 102
    if-eqz p2, :cond_6

    .line 103
    .line 104
    invoke-static {p2}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 105
    .line 106
    .line 107
    move-result v8

    .line 108
    if-eqz v8, :cond_7

    .line 109
    .line 110
    :cond_6
    const/4 v2, 0x1

    .line 111
    :cond_7
    if-nez v2, :cond_8

    .line 112
    .line 113
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 114
    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 116
    .line 117
    .line 118
    move-result-wide v0

    .line 119
    sub-long/2addr v0, v4

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    .line 121
    .line 122
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    .line 124
    .line 125
    const-string v3, "showAddToShareDirResult costTime:"

    .line 126
    .line 127
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    goto :goto_4

    .line 141
    :cond_8
    iput-object p1, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->o0:Ljava/lang/Object;

    .line 142
    .line 143
    iput-object p2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 144
    .line 145
    iput-wide v6, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->OO:J

    .line 146
    .line 147
    iput-wide v4, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇08O〇00〇o:J

    .line 148
    .line 149
    iput v3, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$suspendQueryDirSyncId$1;->〇080OO8〇0:I

    .line 150
    .line 151
    const-wide/16 v8, 0x7d0

    .line 152
    .line 153
    invoke-static {v8, v9, v0}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    move-result-object v2

    .line 157
    if-ne v2, v1, :cond_3

    .line 158
    .line 159
    return-object v1

    .line 160
    :cond_9
    :goto_4
    return-object p2
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final Oo〇〇〇〇()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 10
    .line 11
    const/4 v2, 0x1

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇080()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-ne v0, v2, :cond_1

    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/4 v0, 0x0

    .line 23
    :goto_0
    if-eqz v0, :cond_2

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 26
    .line 27
    const-string v2, "cs bluetooth has other dialog show"

    .line 28
    .line 29
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return v1

    .line 33
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOo8o〇O()J

    .line 34
    .line 35
    .line 36
    move-result-wide v3

    .line 37
    invoke-static {v3, v4}, Lcom/intsig/utils/DateTimeUtil;->OoO8(J)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_3

    .line 42
    .line 43
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 44
    .line 45
    const-string v2, "cs bluetooth has showed"

    .line 46
    .line 47
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return v1

    .line 51
    :cond_3
    invoke-static {}, Lcom/intsig/utils/CommonUtil;->〇O00()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-nez v0, :cond_4

    .line 56
    .line 57
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 58
    .line 59
    const-string v2, "cs bluetooth not connected"

    .line 60
    .line 61
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return v1

    .line 65
    :cond_4
    return v2
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇00o08(Landroid/content/Intent;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "openSpecDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p1, :cond_2

    .line 9
    .line 10
    const-string v1, "MainActivity.intent.open.dialog"

    .line 11
    .line 12
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-nez p1, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 p1, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 28
    :goto_1
    if-eqz p1, :cond_2

    .line 29
    .line 30
    const-string p1, "openSpecDialog  isNullOrEmpty"

    .line 31
    .line 32
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_2
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇80O80O〇0(Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/utils/CsResult;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O008o8oo(Lcom/intsig/utils/CsResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O〇8〇008(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇O〇88O8O()V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "viewLifecycleOwner"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$subscribeUi$1;

    .line 15
    .line 16
    const/4 v3, 0x0

    .line 17
    invoke-direct {v2, p0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$subscribeUi$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v2}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    const/4 v5, 0x0

    .line 35
    const/4 v6, 0x0

    .line 36
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$subscribeUi$2;

    .line 37
    .line 38
    invoke-direct {v7, p0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$subscribeUi$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 39
    .line 40
    .line 41
    const/4 v8, 0x3

    .line 42
    const/4 v9, 0x0

    .line 43
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O〇o8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/view/AnimatedImageView;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$position"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "$this_apply"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0888(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Landroid/graphics/RectF;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    if-eqz p0, :cond_0

    .line 21
    .line 22
    invoke-virtual {p2, p0}, Lcom/intsig/view/AnimatedImageView;->setTargetDrawingRectF(Landroid/graphics/RectF;)V

    .line 23
    .line 24
    .line 25
    sget-object p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/intsig/view/AnimatedImageView;->getTargetDrawingRectF()Landroid/graphics/RectF;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    new-instance p2, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v0, "change bound---- relevance-animation "

    .line 37
    .line 38
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    :cond_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O〇〇O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇08O:Lcom/intsig/camscanner/signin/SignInIconAnimation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/signin/SignInIconAnimation;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO〇00〇8oO:Lcom/airbnb/lottie/LottieAnimationView;

    .line 12
    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/signin/SignInIconAnimation;-><init>(Landroid/view/View;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇08O:Lcom/intsig/camscanner/signin/SignInIconAnimation;

    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇08O:Lcom/intsig/camscanner/signin/SignInIconAnimation;

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    const/4 v2, 0x0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/signin/SignInIconAnimation;->O8()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    const/4 v2, 0x1

    .line 31
    :cond_1
    if-eqz v2, :cond_2

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇08O:Lcom/intsig/camscanner/signin/SignInIconAnimation;

    .line 34
    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/signin/SignInIconAnimation;->Oo08(I)V

    .line 38
    .line 39
    .line 40
    :cond_2
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOOO0(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇〇o8O()V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO0〇0o:Z

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_3

    .line 14
    .line 15
    sget-object v0, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇080:Lcom/intsig/camscanner/docimport/util/DocImportHelper;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->〇O00()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_2

    .line 22
    .line 23
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 24
    .line 25
    const-string v2, "checkPermissionThrottle, check storage permission"

    .line 26
    .line 27
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/docimport/util/DocImportHelper;->o800o8O()V

    .line 31
    .line 32
    .line 33
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 34
    .line 35
    const/16 v1, 0x1e

    .line 36
    .line 37
    if-lt v0, v1, :cond_1

    .line 38
    .line 39
    sget-object v2, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;->〇0O:Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;

    .line 40
    .line 41
    const/4 v3, 0x1

    .line 42
    const/4 v4, 0x1

    .line 43
    const/4 v5, 0x0

    .line 44
    const/4 v6, 0x0

    .line 45
    const/16 v7, 0xc

    .line 46
    .line 47
    const/4 v8, 0x0

    .line 48
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog$Companion;ZZZLjava/lang/String;ILjava/lang/Object;)Lcom/intsig/camscanner/docimport/permission/AllDocPermissionRequestDialog;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkPermissionThrottle$1$1;

    .line 53
    .line 54
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkPermissionThrottle$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/docimport/permission/BaseAllDocPermissionRequestDialog;->o880(Lcom/intsig/camscanner/docimport/util/FileManagerPermissionCheckUtil$IPermissionRequestCallback;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    const-string v2, "childFragmentManager"

    .line 65
    .line 66
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    const-string v2, "AllDocPermissionRequestDialog"

    .line 70
    .line 71
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇00O()V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_2
    const/4 v0, 0x0

    .line 80
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO0〇0o:Z

    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇00O()V

    .line 84
    .line 85
    .line 86
    :goto_0
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/comm/widget/CustomTextView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o〇OO80oO(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/comm/widget/CustomTextView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o088O8800()Landroid/graphics/Rect;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;->getMFabButton()Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o0(Landroid/view/View;)Landroid/graphics/Rect;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o088〇〇()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->O8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;->after_buy_force_login:I

    .line 28
    .line 29
    if-lez v0, :cond_1

    .line 30
    .line 31
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;->force_login_times:I

    .line 36
    .line 37
    if-lez v0, :cond_1

    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOO8o〇〇()J

    .line 40
    .line 41
    .line 42
    move-result-wide v0

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0O〇Oo()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    sget-object v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 48
    .line 49
    new-instance v4, Ljava/lang/StringBuilder;

    .line 50
    .line 51
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v5, "startBindPhone time="

    .line 55
    .line 56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v5, ",times="

    .line 63
    .line 64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const-wide/16 v3, 0x0

    .line 78
    .line 79
    const/4 v5, 0x1

    .line 80
    cmp-long v6, v0, v3

    .line 81
    .line 82
    if-eqz v6, :cond_0

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/utils/DateTimeUtil;->〇80〇808〇O()J

    .line 85
    .line 86
    .line 87
    move-result-wide v3

    .line 88
    invoke-static {v0, v1, v3, v4, v5}, Lcom/intsig/utils/DateTimeUtil;->〇〇8O0〇8(JJI)Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-eqz v0, :cond_1

    .line 93
    .line 94
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;

    .line 95
    .line 96
    .line 97
    move-result-object v0

    .line 98
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;->force_login_times:I

    .line 99
    .line 100
    if-ge v2, v0, :cond_1

    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 103
    .line 104
    invoke-static {v0, v5}, Lcom/intsig/tsapp/account/login_task/BindPhoneActivity;->o〇o8〇〇O(Landroid/app/Activity;Z)V

    .line 105
    .line 106
    .line 107
    add-int/2addr v2, v5

    .line 108
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇0o8〇(I)V

    .line 109
    .line 110
    .line 111
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ForceInfo;->force_login_times:I

    .line 116
    .line 117
    if-ne v2, v0, :cond_1

    .line 118
    .line 119
    invoke-static {}, Lcom/intsig/utils/DateTimeUtil;->〇80〇808〇O()J

    .line 120
    .line 121
    .line 122
    move-result-wide v0

    .line 123
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo0o08o0o(J)V

    .line 124
    .line 125
    .line 126
    const/4 v0, 0x0

    .line 127
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇0o8〇(I)V

    .line 128
    .line 129
    .line 130
    :cond_1
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o0O0O〇〇〇0(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇〇()Ljava/util/Set;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o〇(Z)V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O〇oO〇oo8o()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o〇(Z)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O88〇〇o0O()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_2

    .line 34
    .line 35
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O0o〇(Z)V

    .line 36
    .line 37
    .line 38
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->O88〇〇o0O()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇o〇(Z)V

    .line 47
    .line 48
    .line 49
    :cond_3
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o0OO(II)V
    .locals 2

    .line 1
    const/4 v0, 0x3

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    if-nez p2, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 p1, 0x0

    .line 9
    :goto_0
    sget-object p2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v1, "isFromMeToHome = "

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇()V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇oO〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0o〇〇〇8o(Landroid/view/View;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 v0, 0x3

    .line 4
    new-array v1, v0, [F

    .line 5
    .line 6
    fill-array-data v1, :array_0

    .line 7
    .line 8
    .line 9
    const-string v2, "scaleX"

    .line 10
    .line 11
    invoke-static {p1, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    new-array v0, v0, [F

    .line 16
    .line 17
    fill-array-data v0, :array_1

    .line 18
    .line 19
    .line 20
    const-string v2, "scaleY"

    .line 21
    .line 22
    invoke-static {p1, v2, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    new-instance v0, Landroid/animation/AnimatorSet;

    .line 27
    .line 28
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 29
    .line 30
    .line 31
    const-wide/16 v2, 0x1f4

    .line 32
    .line 33
    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 34
    .line 35
    .line 36
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    .line 37
    .line 38
    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1, p1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 52
    .line 53
    .line 54
    :cond_0
    return-void

    .line 55
    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f99999a    # 1.2f
        0x3f800000    # 1.0f
    .end array-data

    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    :array_1
    .array-data 4
        0x3f000000    # 0.5f
        0x3f99999a    # 1.2f
        0x3f800000    # 1.0f
    .end array-data
    .line 66
    .line 67
.end method

.method public static final synthetic o0〇〇00(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o8〇8(Ljava/util/ArrayList;JLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o0〇〇00〇o()Lcom/intsig/camscanner/attention/RewardAPI;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOo〇8o008:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/attention/RewardAPI;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇o88o08〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o88(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(J)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇8o00(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o88o88(I)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz p1, :cond_3

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    const/4 v2, 0x1

    .line 9
    if-eq p1, v1, :cond_0

    .line 10
    .line 11
    const/4 v1, 0x3

    .line 12
    if-eq p1, v1, :cond_0

    .line 13
    .line 14
    packed-switch p1, :pswitch_data_0

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :pswitch_0
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇O〇(Landroid/app/Activity;Z)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 29
    .line 30
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/util/CnUnsubscribeScaffoldConfig;->oO80(Landroid/app/Activity;Z)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :pswitch_2
    sget-object p1, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 35
    .line 36
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->O〇8O8〇008(Landroidx/fragment/app/FragmentActivity;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 45
    .line 46
    if-nez p1, :cond_1

    .line 47
    .line 48
    return-void

    .line 49
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->Oo8Oo00oo()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    const-string v1, "type"

    .line 54
    .line 55
    const-string v3, "vip_icon_click"

    .line 56
    .line 57
    if-eqz p1, :cond_2

    .line 58
    .line 59
    const-string p1, "discount"

    .line 60
    .line 61
    invoke-static {v0, v3, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    new-instance p1, Lcom/intsig/utils/IntentBuilder;

    .line 65
    .line 66
    invoke-direct {p1}, Lcom/intsig/utils/IntentBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p1, p0}, Lcom/intsig/utils/IntentBuilder;->〇O8o08O(Landroidx/fragment/app/Fragment;)Lcom/intsig/utils/IntentBuilder;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const-class v0, Lcom/intsig/camscanner/purchase/activity/GPRenewalRedeemActivity;

    .line 74
    .line 75
    invoke-virtual {p1, v0}, Lcom/intsig/utils/IntentBuilder;->〇〇888(Ljava/lang/Class;)Lcom/intsig/utils/IntentBuilder;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/utils/IntentBuilder;->〇80〇808〇O()V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->O8ooOoo〇()Z

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    if-eqz p1, :cond_4

    .line 88
    .line 89
    const-string p1, "24or48_activity"

    .line 90
    .line 91
    invoke-static {v0, v3, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-static {p1, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o0ooO(Landroid/app/Activity;Z)V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_3
    :pswitch_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    invoke-virtual {v1, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->Oo0oOo〇0(Ljava/lang/String;I)V

    .line 107
    .line 108
    .line 109
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇O〇0O0〇()V

    .line 110
    .line 111
    .line 112
    :cond_4
    :goto_0
    return-void

    .line 113
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o88oo〇O()Lcom/intsig/camscanner/util/CsLinkParser;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/util/CsLinkParser;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o8O〇008(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILcom/intsig/camscanner/purchase/track/FunctionEntrance;ZILjava/lang/Object;)V
    .locals 7

    .line 1
    and-int/lit8 p8, p7, 0x2

    .line 2
    .line 3
    if-eqz p8, :cond_0

    .line 4
    .line 5
    sget-object p2, Lcom/intsig/camscanner/capture/CaptureMode;->NONE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 6
    .line 7
    :cond_0
    move-object v2, p2

    .line 8
    and-int/lit8 p2, p7, 0x4

    .line 9
    .line 10
    if-eqz p2, :cond_1

    .line 11
    .line 12
    sget-object p3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 13
    .line 14
    :cond_1
    move-object v3, p3

    .line 15
    and-int/lit8 p2, p7, 0x8

    .line 16
    .line 17
    if-eqz p2, :cond_2

    .line 18
    .line 19
    const/4 p4, -0x1

    .line 20
    const/4 v4, -0x1

    .line 21
    goto :goto_0

    .line 22
    :cond_2
    move v4, p4

    .line 23
    :goto_0
    and-int/lit8 p2, p7, 0x10

    .line 24
    .line 25
    if-eqz p2, :cond_3

    .line 26
    .line 27
    iget-object p5, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOo0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    :cond_3
    move-object v5, p5

    .line 30
    and-int/lit8 p2, p7, 0x20

    .line 31
    .line 32
    if-eqz p2, :cond_4

    .line 33
    .line 34
    const/4 p6, 0x0

    .line 35
    const/4 v6, 0x0

    .line 36
    goto :goto_1

    .line 37
    :cond_4
    move v6, p6

    .line 38
    :goto_1
    move-object v0, p0

    .line 39
    move-object v1, p1

    .line 40
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0o0(Landroid/view/View;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILcom/intsig/camscanner/purchase/track/FunctionEntrance;Z)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
.end method

.method private static final o8o0o8(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V
    .locals 3

    .line 1
    const-string v0, "$this_apply"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$position"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "this$0"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "end relevance-animation--"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const/4 v2, 0x0

    .line 36
    if-eqz p2, :cond_0

    .line 37
    .line 38
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;->getAd_id()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    move-object p2, v2

    .line 44
    :goto_0
    invoke-virtual {p0, v1, p2}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇00(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 45
    .line 46
    .line 47
    move-result-object p0

    .line 48
    invoke-static {p0}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdUtil;->〇〇8O0〇8(Lcom/intsig/advertisement/record/operation/AdIdRecord;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 52
    .line 53
    .line 54
    move-result-object p0

    .line 55
    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    .line 56
    .line 57
    .line 58
    move-result p0

    .line 59
    if-nez p0, :cond_7

    .line 60
    .line 61
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 62
    .line 63
    .line 64
    move-result-object p0

    .line 65
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 66
    .line 67
    .line 68
    move-result p0

    .line 69
    if-eqz p0, :cond_1

    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_1
    iget-object p0, p3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 73
    .line 74
    const/4 p2, 0x1

    .line 75
    if-eqz p0, :cond_2

    .line 76
    .line 77
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 78
    .line 79
    .line 80
    move-result p0

    .line 81
    if-ne p0, p2, :cond_2

    .line 82
    .line 83
    const/4 p0, 0x1

    .line 84
    goto :goto_1

    .line 85
    :cond_2
    const/4 p0, 0x0

    .line 86
    :goto_1
    if-eqz p0, :cond_7

    .line 87
    .line 88
    iget-object p0, p3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 89
    .line 90
    if-eqz p0, :cond_3

    .line 91
    .line 92
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 93
    .line 94
    .line 95
    move-result p0

    .line 96
    if-ne p0, p2, :cond_3

    .line 97
    .line 98
    const/4 v0, 0x1

    .line 99
    :cond_3
    if-eqz v0, :cond_4

    .line 100
    .line 101
    goto :goto_2

    .line 102
    :cond_4
    sget-object p0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->DOC_LIST_ICON:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 103
    .line 104
    if-ne p1, p0, :cond_6

    .line 105
    .line 106
    iget-object p0, p3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 107
    .line 108
    if-eqz p0, :cond_5

    .line 109
    .line 110
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->o〇o0oOO8()Landroid/view/View;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    :cond_5
    invoke-direct {p3, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0o〇〇〇8o(Landroid/view/View;)V

    .line 115
    .line 116
    .line 117
    :cond_6
    invoke-direct {p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8o0o0(Landroid/graphics/Bitmap;)V

    .line 118
    .line 119
    .line 120
    :cond_7
    :goto_2
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final o8〇O〇0O0〇()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: upgrade to premium"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipManager;->〇〇808〇()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v1, "responseVipClick, open cs x pdf web purchase page"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 24
    .line 25
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->MAIN_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 26
    .line 27
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 30
    .line 31
    .line 32
    const/4 v2, 0x4

    .line 33
    const/4 v3, 0x0

    .line 34
    const/4 v4, 0x0

    .line 35
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipManager;->OoO8(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;IILjava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 44
    .line 45
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->MAIN_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 46
    .line 47
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 48
    .line 49
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 50
    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇〇〇0〇〇0(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final oO0o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-static {p2}, Lcom/intsig/util/PermissionUtil;->O8ooOoo〇(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    if-eqz p2, :cond_1

    .line 15
    .line 16
    if-eqz p3, :cond_0

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO8o〇08〇()V

    .line 19
    .line 20
    .line 21
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 22
    .line 23
    const-string p3, "click mainpage screenshot after grant permission"

    .line 24
    .line 25
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p2

    .line 32
    const-string p3, "screenshot_click"

    .line 33
    .line 34
    invoke-static {p2, p3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8ooo(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final oO8o〇08〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->OoO8(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/SDStorageManager;->oO80(Landroid/content/Context;Z)V

    .line 16
    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-wide/16 v1, 0x0

    .line 23
    .line 24
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo0〇o〇(Landroid/content/Context;J)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->OO0o〇〇〇〇0(Landroid/content/Context;)Z

    .line 32
    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OoO〇OOo8o()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oOO8oo0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/scenario/SeniorFolderMainGuideControl;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "CSHome"

    .line 8
    .line 9
    const-string v1, "folder_red_dot"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    invoke-virtual {v0, v1, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O〇(IZ)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oOOO0(Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "viewLifecycleOwner"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$openShareDirAfterSync$1;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-direct {v5, p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$openShareDirAfterSync$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 20
    .line 21
    .line 22
    const/4 v6, 0x3

    .line 23
    const/4 v7, 0x0

    .line 24
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOO0o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oo0O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0o0oO〇〇0()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oo8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->ooo〇8oO()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 22
    .line 23
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 24
    .line 25
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oo8〇〇()Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8oOOo:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oooO888(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O8Oo(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final oooo800〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O8o08O8O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO0〇0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o0o8(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic o〇O8OO(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;IZ)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p6}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇OOO〇〇(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;IZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/utils/CsResult;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0o(Lcom/intsig/utils/CsResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇o8〇〇O(Landroid/content/Intent;)V
    .locals 16

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "intent == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    :goto_0
    move-object/from16 v2, p0

    .line 13
    .line 14
    goto/16 :goto_2

    .line 15
    .line 16
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const-string v2, "MainMenuActivity.intent.home.tab"

    .line 21
    .line 22
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    if-eqz v2, :cond_2

    .line 27
    .line 28
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    const-string v2, "MainMenuActivity.intent.open.doc"

    .line 40
    .line 41
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    const/4 v3, 0x0

    .line 46
    const-wide/16 v4, -0x1

    .line 47
    .line 48
    if-eqz v2, :cond_3

    .line 49
    .line 50
    const-string v1, "doc_id"

    .line 51
    .line 52
    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 53
    .line 54
    .line 55
    move-result-wide v0

    .line 56
    cmp-long v2, v0, v4

    .line 57
    .line 58
    if-lez v2, :cond_0

    .line 59
    .line 60
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 61
    .line 62
    new-instance v4, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string v5, "handleFolderIntent INTENT_OPEN_DOC :"

    .line 68
    .line 69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    const-string v4, "viewLifecycleOwner"

    .line 87
    .line 88
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-static {v2}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 92
    .line 93
    .line 94
    move-result-object v5

    .line 95
    const/4 v6, 0x0

    .line 96
    const/4 v7, 0x0

    .line 97
    new-instance v8, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$handleFolderIntent$1;

    .line 98
    .line 99
    move-object/from16 v2, p0

    .line 100
    .line 101
    invoke-direct {v8, v0, v1, v2, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$handleFolderIntent$1;-><init>(JLcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 102
    .line 103
    .line 104
    const/4 v9, 0x3

    .line 105
    const/4 v10, 0x0

    .line 106
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 107
    .line 108
    .line 109
    goto/16 :goto_2

    .line 110
    .line 111
    :cond_3
    move-object/from16 v2, p0

    .line 112
    .line 113
    const-string v6, "MainMenuActivity.intent.open.team_folder"

    .line 114
    .line 115
    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 116
    .line 117
    .line 118
    move-result v6

    .line 119
    if-nez v6, :cond_9

    .line 120
    .line 121
    const-string v6, "MainMenuActivity.intent.folder.shortcut"

    .line 122
    .line 123
    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    move-result v6

    .line 127
    const-string v7, "MainMenuActivity.intent.open.folder.sync"

    .line 128
    .line 129
    if-nez v6, :cond_4

    .line 130
    .line 131
    const-string v6, "MainMenuActivity.intent.open.folder"

    .line 132
    .line 133
    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    if-nez v6, :cond_4

    .line 138
    .line 139
    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 140
    .line 141
    .line 142
    move-result v6

    .line 143
    if-eqz v6, :cond_9

    .line 144
    .line 145
    :cond_4
    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    const-string v6, "doWithDirShortCut uri == null"

    .line 150
    .line 151
    if-eqz v1, :cond_7

    .line 152
    .line 153
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    if-eqz v1, :cond_5

    .line 158
    .line 159
    const-string v1, "MainMenuActivity.intent.open.folder.syncId"

    .line 160
    .line 161
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    sget-object v6, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 166
    .line 167
    new-instance v7, Ljava/lang/StringBuilder;

    .line 168
    .line 169
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .line 171
    .line 172
    const-string v8, "handleFolderIntent folderSyncId:"

    .line 173
    .line 174
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 181
    .line 182
    .line 183
    move-result-object v7

    .line 184
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 188
    .line 189
    .line 190
    move-result-object v6

    .line 191
    invoke-virtual {v6}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    .line 192
    .line 193
    .line 194
    move-result-object v6

    .line 195
    invoke-virtual {v6, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 199
    .line 200
    .line 201
    goto :goto_1

    .line 202
    :cond_5
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 203
    .line 204
    invoke-static {v1, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    :goto_1
    const-string v1, "MainMenuActivity.intent.open.docId"

    .line 208
    .line 209
    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 210
    .line 211
    .line 212
    move-result-wide v8

    .line 213
    cmp-long v3, v8, v4

    .line 214
    .line 215
    if-lez v3, :cond_9

    .line 216
    .line 217
    sget-object v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 218
    .line 219
    new-instance v4, Ljava/lang/StringBuilder;

    .line 220
    .line 221
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .line 223
    .line 224
    const-string v5, "handleFolderIntent openDocId:"

    .line 225
    .line 226
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v4

    .line 236
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 240
    .line 241
    .line 242
    move-result-object v3

    .line 243
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 244
    .line 245
    invoke-virtual {v3}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 246
    .line 247
    .line 248
    move-result v3

    .line 249
    const/4 v4, 0x1

    .line 250
    if-eq v3, v4, :cond_6

    .line 251
    .line 252
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 253
    .line 254
    .line 255
    move-result-object v3

    .line 256
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 257
    .line 258
    invoke-virtual {v3, v4}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 259
    .line 260
    .line 261
    :cond_6
    const/4 v3, -0x1

    .line 262
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    .line 264
    .line 265
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 266
    .line 267
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 268
    .line 269
    .line 270
    move-result-object v1

    .line 271
    invoke-static {v1, v8, v9}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oO00OOO(Landroid/content/Context;J)Z

    .line 272
    .line 273
    .line 274
    move-result v10

    .line 275
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    invoke-static {v0, v8, v9}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v11

    .line 283
    sget-object v6, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇080:Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;

    .line 284
    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 286
    .line 287
    .line 288
    move-result-object v7

    .line 289
    const/4 v12, 0x0

    .line 290
    const/4 v13, 0x0

    .line 291
    const/16 v14, 0x20

    .line 292
    .line 293
    const/4 v15, 0x0

    .line 294
    invoke-static/range {v6 .. v15}, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇O00(Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;Landroid/app/Activity;JZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 295
    .line 296
    .line 297
    goto :goto_2

    .line 298
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 299
    .line 300
    .line 301
    move-result-object v1

    .line 302
    if-eqz v1, :cond_8

    .line 303
    .line 304
    sget-object v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 305
    .line 306
    new-instance v5, Ljava/lang/StringBuilder;

    .line 307
    .line 308
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 309
    .line 310
    .line 311
    const-string v6, "handleFolderIntent uri:"

    .line 312
    .line 313
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    .line 315
    .line 316
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 317
    .line 318
    .line 319
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 320
    .line 321
    .line 322
    move-result-object v5

    .line 323
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    .line 325
    .line 326
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 327
    .line 328
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 329
    .line 330
    .line 331
    move-result-object v4

    .line 332
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 333
    .line 334
    .line 335
    move-result-wide v5

    .line 336
    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DirDao;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 337
    .line 338
    .line 339
    move-result-object v1

    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 341
    .line 342
    .line 343
    move-result-object v4

    .line 344
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    .line 345
    .line 346
    .line 347
    move-result-object v4

    .line 348
    invoke-virtual {v4, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 349
    .line 350
    .line 351
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 352
    .line 353
    .line 354
    goto :goto_2

    .line 355
    :cond_8
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 356
    .line 357
    invoke-static {v0, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    .line 359
    .line 360
    :cond_9
    :goto_2
    return-void
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public static final synthetic o〇oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00o〇O8()Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->〇〇o0〇8()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo8O8o80()Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇00〇〇〇o〇8(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 22

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    sget-boolean v2, Lcom/intsig/wxapi/WXEntryActivity;->o〇00O:Z

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-eqz v2, :cond_0

    .line 9
    .line 10
    sput-boolean v3, Lcom/intsig/wxapi/WXEntryActivity;->o〇00O:Z

    .line 11
    .line 12
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-static {v2}, Lcom/intsig/camscanner/util/CSWebJumpProtocol;->〇O〇(Landroid/app/Activity;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    if-eqz v0, :cond_6

    .line 20
    .line 21
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    if-eqz v2, :cond_6

    .line 26
    .line 27
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    if-eqz v2, :cond_6

    .line 32
    .line 33
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    const v5, -0x203bdb11

    .line 38
    .line 39
    .line 40
    const-string v6, "mini_program_doc_report_info"

    .line 41
    .line 42
    const-string v7, "mini_program_doc_from"

    .line 43
    .line 44
    const-string v8, "constant_is_show_doc_location"

    .line 45
    .line 46
    if-eq v4, v5, :cond_5

    .line 47
    .line 48
    const v5, 0x66509644

    .line 49
    .line 50
    .line 51
    if-eq v4, v5, :cond_3

    .line 52
    .line 53
    const v3, 0x6650a645

    .line 54
    .line 55
    .line 56
    if-eq v4, v3, :cond_1

    .line 57
    .line 58
    goto/16 :goto_0

    .line 59
    .line 60
    :cond_1
    const-string v3, "MainMenuActivity.intent.import.miniprogram.mul"

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    if-nez v2, :cond_2

    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 71
    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    const-string v1, "is from miniProgramDoc Mul"

    .line 81
    .line 82
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    const v2, 0x7f130692

    .line 97
    .line 98
    .line 99
    invoke-static {v1, v2}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 100
    .line 101
    .line 102
    invoke-direct/range {p0 .. p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o0〇(Landroid/content/Intent;)V

    .line 103
    .line 104
    .line 105
    goto/16 :goto_0

    .line 106
    .line 107
    :cond_3
    const-string v4, "MainMenuActivity.intent.import.miniprogram.img"

    .line 108
    .line 109
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    if-nez v2, :cond_4

    .line 114
    .line 115
    goto/16 :goto_0

    .line 116
    .line 117
    :cond_4
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 118
    .line 119
    new-instance v4, Ljava/lang/StringBuilder;

    .line 120
    .line 121
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .line 123
    .line 124
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    const-string v1, "is from miniProgramDoc Img"

    .line 128
    .line 129
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    const-string v1, "other_share_in_img_pic_list"

    .line 140
    .line 141
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 142
    .line 143
    .line 144
    move-result-object v10

    .line 145
    const-string v1, "other_share_in_img_pic_title"

    .line 146
    .line 147
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v18

    .line 151
    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 152
    .line 153
    .line 154
    move-result v19

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 156
    .line 157
    .line 158
    move-result-object v9

    .line 159
    const-wide/16 v11, -0x1

    .line 160
    .line 161
    const-wide/16 v13, -0x2

    .line 162
    .line 163
    const/16 v15, 0x7c

    .line 164
    .line 165
    const/16 v16, 0x0

    .line 166
    .line 167
    const/16 v17, 0x0

    .line 168
    .line 169
    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v20

    .line 173
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    move-object/from16 v21, v0

    .line 178
    .line 179
    check-cast v21, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 180
    .line 181
    invoke-static/range {v9 .. v21}, Lcom/intsig/camscanner/app/IntentUtil;->OoO8(Landroid/app/Activity;Ljava/util/ArrayList;JJILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;)V

    .line 182
    .line 183
    .line 184
    goto :goto_0

    .line 185
    :cond_5
    const-string v4, "MainMenuActivity.intent.import.miniprogram"

    .line 186
    .line 187
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 188
    .line 189
    .line 190
    move-result v2

    .line 191
    if-eqz v2, :cond_6

    .line 192
    .line 193
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 194
    .line 195
    new-instance v4, Ljava/lang/StringBuilder;

    .line 196
    .line 197
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .line 199
    .line 200
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    const-string v1, "is from miniProgramDoc"

    .line 204
    .line 205
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 209
    .line 210
    .line 211
    move-result-object v1

    .line 212
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    .line 214
    .line 215
    const-string v1, "mini_program_doc_pic_list"

    .line 216
    .line 217
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 218
    .line 219
    .line 220
    move-result-object v10

    .line 221
    const-string v1, "mini_program_doc_title"

    .line 222
    .line 223
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 224
    .line 225
    .line 226
    move-result-object v18

    .line 227
    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 228
    .line 229
    .line 230
    move-result v19

    .line 231
    const-string v1, "mini_program_doc_id"

    .line 232
    .line 233
    const-wide/16 v2, -0x2

    .line 234
    .line 235
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 236
    .line 237
    .line 238
    move-result-wide v13

    .line 239
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 240
    .line 241
    .line 242
    move-result-object v9

    .line 243
    const-wide/16 v11, -0x1

    .line 244
    .line 245
    const/16 v15, 0x7c

    .line 246
    .line 247
    invoke-static {}, Lcom/intsig/camscanner/external_import/ExternalImportHelper;->O8()Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object v16

    .line 251
    const/16 v17, 0x0

    .line 252
    .line 253
    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v20

    .line 257
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 258
    .line 259
    .line 260
    move-result-object v0

    .line 261
    move-object/from16 v21, v0

    .line 262
    .line 263
    check-cast v21, Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;

    .line 264
    .line 265
    invoke-static/range {v9 .. v21}, Lcom/intsig/camscanner/app/IntentUtil;->OoO8(Landroid/app/Activity;Ljava/util/ArrayList;JJILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/intsig/camscanner/miniprogram/NewDocReportInfo;)V

    .line 266
    .line 267
    .line 268
    :cond_6
    :goto_0
    return-void
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇0888(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Landroid/graphics/RectF;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$WhenMappings;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eq p1, v0, :cond_3

    .line 12
    .line 13
    const/4 v0, 0x2

    .line 14
    if-eq p1, v0, :cond_2

    .line 15
    .line 16
    const/4 v0, 0x3

    .line 17
    if-eq p1, v0, :cond_1

    .line 18
    .line 19
    :cond_0
    move-object p1, v1

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 22
    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->O088O()Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    goto :goto_0

    .line 30
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 31
    .line 32
    if-eqz p1, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->〇00o〇O8()Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    goto :goto_0

    .line 39
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 40
    .line 41
    if-eqz p1, :cond_0

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->o〇o0oOO8()Landroid/view/View;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    :goto_0
    if-eqz p1, :cond_5

    .line 48
    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_4

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_4
    move-object p1, v1

    .line 57
    :goto_1
    if-eqz p1, :cond_5

    .line 58
    .line 59
    new-instance v0, Landroid/graphics/Rect;

    .line 60
    .line 61
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 65
    .line 66
    .line 67
    new-instance v1, Landroid/graphics/RectF;

    .line 68
    .line 69
    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 70
    .line 71
    .line 72
    :cond_5
    return-object v1
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOOo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇08O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇0O8Oo(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V
    .locals 8

    .line 1
    const-string v0, "$this_apply"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$position"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "this$0"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "start relevance-animation--"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Lo〇00O/〇O888o0o;

    .line 24
    .line 25
    move-object v2, v0

    .line 26
    move-object v3, p0

    .line 27
    move-object v4, p1

    .line 28
    move-object v5, p2

    .line 29
    move-object v6, p3

    .line 30
    move-object v7, p4

    .line 31
    invoke-direct/range {v2 .. v7}, Lo〇00O/〇O888o0o;-><init>(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, v0}, Lcom/intsig/view/AnimatedImageView;->oO80(Lcom/intsig/callback/Callback0;)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final 〇0o(Lcom/intsig/utils/CsResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/AddUseToShareDirResult;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v1, 0x0

    .line 2
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$1;

    .line 3
    .line 4
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 5
    .line 6
    .line 7
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$2;

    .line 8
    .line 9
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 10
    .line 11
    .line 12
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$3;

    .line 13
    .line 14
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showAddToShareDirResult$3;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 15
    .line 16
    .line 17
    const/4 v5, 0x1

    .line 18
    const/4 v6, 0x0

    .line 19
    move-object v0, p1

    .line 20
    invoke-static/range {v0 .. v6}, Lcom/intsig/utils/CsResultKt;->〇o00〇〇Oo(Lcom/intsig/utils/CsResult;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇0o0oO〇〇0()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇〇00〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o088〇〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->Oo08()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇080:Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v2, 0x0

    .line 32
    const/4 v3, 0x0

    .line 33
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkShowScanGuide$1;

    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkShowScanGuide$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 37
    .line 38
    .line 39
    const/4 v5, 0x3

    .line 40
    const/4 v6, 0x0

    .line 41
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O80OO()V

    .line 46
    .line 47
    .line 48
    :cond_1
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0o88O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/16 p1, 0x8c

    .line 7
    .line 8
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇0o88Oo〇()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppSwitch;->〇〇888(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "viewLifecycleOwner"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    const/4 v3, 0x0

    .line 26
    const/4 v4, 0x0

    .line 27
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkPayOrder$1;

    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkPayOrder$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 31
    .line 32
    .line 33
    const/4 v6, 0x3

    .line 34
    const/4 v7, 0x0

    .line 35
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0oO(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$recyclerView"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$this_run"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 17
    .line 18
    .line 19
    move-result-object p3

    .line 20
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 21
    .line 22
    .line 23
    move-result p3

    .line 24
    const/4 v0, 0x1

    .line 25
    if-ne p3, v0, :cond_0

    .line 26
    .line 27
    const/4 p0, 0x0

    .line 28
    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 29
    .line 30
    .line 31
    const-string p0, "CSMain"

    .line 32
    .line 33
    const-string p1, "back_to_top_click"

    .line 34
    .line 35
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 40
    .line 41
    .line 42
    move-result-object p0

    .line 43
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O8o08O(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 44
    .line 45
    .line 46
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0oO(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇0o〇o()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇80〇808〇O()V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initTabLayout$1;

    .line 17
    .line 18
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initTabLayout$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->OO0o〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout$IPageChangeCallback;)V

    .line 22
    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8〇OO0〇0o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;

    .line 31
    .line 32
    const-string v1, "mBinding.mainBottomEditModeTab"

    .line 33
    .line 34
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    iget-object v4, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 42
    .line 43
    const-string v1, "mBinding.mainBottomTab"

    .line 44
    .line 45
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    iget-object v5, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 53
    .line 54
    const-string v1, "mBinding.mainViewpager"

    .line 55
    .line 56
    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 60
    .line 61
    .line 62
    move-result-object v6

    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 64
    .line 65
    .line 66
    move-result-object v7

    .line 67
    move-object v2, v0

    .line 68
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;Landroidx/viewpager2/widget/ViewPager2;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8o80O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇ooO8Ooo〇(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇0〇o8〇()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 10
    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v3, "intent == "

    .line 17
    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8O(Landroid/content/Intent;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇o8〇〇O(Landroid/content/Intent;)V

    .line 35
    .line 36
    .line 37
    const-string v1, "onCreate"

    .line 38
    .line 39
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇00〇〇〇o〇8(Landroid/content/Intent;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o88oo〇O()Lcom/intsig/camscanner/util/CsLinkParser;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/util/CsLinkParser;->〇〇808〇(Landroid/content/Intent;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇00o08(Landroid/content/Intent;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O008oO0(Landroid/content/Intent;)V

    .line 53
    .line 54
    .line 55
    sget-object v1, Lcom/intsig/camscanner/purchase/push/PurchasePushManager;->〇080:Lcom/intsig/camscanner/purchase/push/PurchasePushManager;

    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/purchase/push/PurchasePushManager;->〇o00〇〇Oo(Landroid/content/Intent;Landroid/app/Activity;)V

    .line 62
    .line 63
    .line 64
    sget-object v1, Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;->〇080:Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;

    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;->o〇0(Landroid/content/Intent;Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;)V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇80O()Lcom/intsig/app/BaseProgressDialog;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8〇OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "<get-mLoadingDialog>(...)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇80O80O〇0(Lcom/intsig/utils/CsResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/newsign/data/sync/ESignLinkQueryRes;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onESignLinkQueryResult == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$1;

    .line 25
    .line 26
    invoke-direct {v4, p0, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$2;

    .line 30
    .line 31
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 32
    .line 33
    .line 34
    new-instance v6, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$3;

    .line 35
    .line 36
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onESignLinkQueryResult$3;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 37
    .line 38
    .line 39
    const/4 v7, 0x1

    .line 40
    const/4 v8, 0x0

    .line 41
    move-object v2, p1

    .line 42
    invoke-static/range {v2 .. v8}, Lcom/intsig/utils/CsResultKt;->〇o00〇〇Oo(Lcom/intsig/utils/CsResult;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇8O(I)V
    .locals 20

    .line 1
    move/from16 v0, p1

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "goNexPageByGpDropCnl process = "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    if-eq v0, v1, :cond_3

    .line 27
    .line 28
    const/4 v2, 0x2

    .line 29
    if-eq v0, v2, :cond_2

    .line 30
    .line 31
    const/4 v3, 0x3

    .line 32
    if-eq v0, v3, :cond_1

    .line 33
    .line 34
    const/4 v3, 0x4

    .line 35
    if-eq v0, v3, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    .line 39
    .line 40
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    new-instance v6, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 45
    .line 46
    const/4 v3, 0x0

    .line 47
    const/4 v10, 0x0

    .line 48
    invoke-direct {v6, v3, v2, v1, v10}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 49
    .line 50
    .line 51
    const/4 v7, 0x0

    .line 52
    const/4 v8, 0x4

    .line 53
    const/4 v9, 0x0

    .line 54
    move-object v4, v0

    .line 55
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v10, v10}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->OOo8o〇O(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    const-string v2, "selfsearch_channel"

    .line 71
    .line 72
    invoke-static {v1, v2, v2}, Lcom/intsig/camscanner/web/UrlUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-static {v0, v1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_2
    const/4 v3, 0x0

    .line 81
    sget-object v4, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 82
    .line 83
    const/4 v5, 0x0

    .line 84
    const/4 v6, 0x0

    .line 85
    const/4 v7, 0x0

    .line 86
    const/4 v8, 0x1

    .line 87
    const/16 v9, 0x1c

    .line 88
    .line 89
    const/4 v10, 0x0

    .line 90
    move-object/from16 v2, p0

    .line 91
    .line 92
    invoke-static/range {v2 .. v10}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8O〇008(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILcom/intsig/camscanner/purchase/track/FunctionEntrance;ZILjava/lang/Object;)V

    .line 93
    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_3
    const/4 v12, 0x0

    .line 97
    sget-object v13, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_SINGLE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 98
    .line 99
    const/4 v14, 0x0

    .line 100
    const/4 v15, 0x0

    .line 101
    const/16 v16, 0x0

    .line 102
    .line 103
    const/16 v17, 0x1

    .line 104
    .line 105
    const/16 v18, 0x1c

    .line 106
    .line 107
    const/16 v19, 0x0

    .line 108
    .line 109
    move-object/from16 v11, p0

    .line 110
    .line 111
    invoke-static/range {v11 .. v19}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8O〇008(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILcom/intsig/camscanner/purchase/track/FunctionEntrance;ZILjava/lang/Object;)V

    .line 112
    .line 113
    .line 114
    :goto_0
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇8O0880(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8o0o0(Landroid/graphics/Bitmap;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v0, v3, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->OOOo〇(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;ZILjava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇()V

    .line 16
    .line 17
    .line 18
    :cond_1
    if-eqz p1, :cond_2

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 21
    .line 22
    .line 23
    :cond_2
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8ooo(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/4 v2, 0x1

    .line 17
    const-string v3, "Screenshots"

    .line 18
    .line 19
    const/4 v4, 0x0

    .line 20
    const v5, 0x7f13046b

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇80〇808〇O()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v6

    .line 27
    const/4 v7, 0x1

    .line 28
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/app/IntentUtil;->〇8o8o〇(Landroid/content/Context;ZLjava/lang/String;ZILjava/lang/String;Z)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    new-instance v0, Lcom/intsig/result/GetActivityResult;

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-direct {v0, v1}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 39
    .line 40
    .line 41
    const/16 v1, 0x7d

    .line 42
    .line 43
    invoke-virtual {v0, p1, v1}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$go2GalleryScreenshotAfterGetStoragePermission$1;

    .line 48
    .line 49
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$go2GalleryScreenshotAfterGetStoragePermission$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO0o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇8〇8o00(J)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "delete successfully docId "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇〇8o(Landroid/content/Intent;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const-string v1, "create_open_card_folder"

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 18
    .line 19
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-nez p1, :cond_1

    .line 24
    .line 25
    return-void

    .line 26
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const-string v1, "viewLifecycleOwner"

    .line 31
    .line 32
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createOpenCardFolder$1;

    .line 40
    .line 41
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createOpenCardFolder$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Landroid/util/SparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO8〇O8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8〇8000(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOO〇〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8〇8O0oO()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇O8〇〇o8〇(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)Z
    .locals 10

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->getType()Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    sget-object v2, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;->SCREENSHOT:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsType;

    .line 10
    .line 11
    if-eq v1, v2, :cond_1

    .line 12
    .line 13
    return v0

    .line 14
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇08〇0oo0(Ljava/lang/Integer;)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-nez v1, :cond_2

    .line 35
    .line 36
    return v0

    .line 37
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 38
    .line 39
    if-eqz v1, :cond_4

    .line 40
    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇80〇808〇O()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-eqz v1, :cond_4

    .line 46
    .line 47
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 48
    .line 49
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    if-nez v2, :cond_3

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    const-string v3, "screenshot_show"

    .line 60
    .line 61
    invoke-static {v2, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_3
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooo0〇〇O:Ljava/lang/String;

    .line 65
    .line 66
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o088O8800()Landroid/graphics/Rect;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    if-nez v1, :cond_5

    .line 71
    .line 72
    return v0

    .line 73
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 82
    .line 83
    sub-int/2addr v2, v1

    .line 84
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 85
    .line 86
    new-instance v3, Ljava/lang/StringBuilder;

    .line 87
    .line 88
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    .line 90
    .line 91
    const-string v4, "paddingBottomOrigin = "

    .line 92
    .line 93
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const-string v4, "\tpaddingBottom = "

    .line 100
    .line 101
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 115
    .line 116
    if-eqz v3, :cond_6

    .line 117
    .line 118
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->o〇〇8〇〇()V

    .line 119
    .line 120
    .line 121
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 122
    .line 123
    .line 124
    move-result-object v3

    .line 125
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 126
    .line 127
    invoke-virtual {v3}, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 128
    .line 129
    .line 130
    move-result-object v3

    .line 131
    invoke-virtual {v3, v0, v0, v0, v2}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 135
    .line 136
    .line 137
    move-result-object v2

    .line 138
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 139
    .line 140
    invoke-virtual {v2}, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 141
    .line 142
    .line 143
    move-result-object v2

    .line 144
    const-string v3, "mBinding.mainIncludeScreenshot.root"

    .line 145
    .line 146
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    const/4 v3, 0x1

    .line 150
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 154
    .line 155
    .line 156
    move-result-object v2

    .line 157
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 158
    .line 159
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 160
    .line 161
    const v4, 0x7f130429

    .line 162
    .line 163
    .line 164
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 165
    .line 166
    .line 167
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 168
    .line 169
    .line 170
    move-result-object v2

    .line 171
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 172
    .line 173
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 174
    .line 175
    const-string v4, "mBinding.mainIncludeScreenshot.tvBottomTipsTitle"

    .line 176
    .line 177
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-static {v2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 184
    .line 185
    .line 186
    move-result-object v2

    .line 187
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 188
    .line 189
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->OO:Landroid/widget/ImageView;

    .line 190
    .line 191
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$2;

    .line 192
    .line 193
    invoke-direct {v4}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$2;-><init>()V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v2, v4}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 200
    .line 201
    .line 202
    move-result-object v2

    .line 203
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 204
    .line 205
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->OO:Landroid/widget/ImageView;

    .line 206
    .line 207
    new-instance v4, Lo〇00O/o〇〇0〇;

    .line 208
    .line 209
    invoke-direct {v4, p0, p1}, Lo〇00O/o〇〇0〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    .line 214
    .line 215
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 216
    .line 217
    .line 218
    move-result-object v2

    .line 219
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 220
    .line 221
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->o〇00O:Landroid/widget/RelativeLayout;

    .line 222
    .line 223
    new-instance v4, Lo〇00O/OOO〇O0;

    .line 224
    .line 225
    invoke-direct {v4, p0, p1}, Lo〇00O/OOO〇O0;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 226
    .line 227
    .line 228
    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    .line 230
    .line 231
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 232
    .line 233
    .line 234
    move-result-object v2

    .line 235
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 236
    .line 237
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 238
    .line 239
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 240
    .line 241
    .line 242
    move-result-object v4

    .line 243
    invoke-virtual {v4}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 244
    .line 245
    .line 246
    move-result-object v4

    .line 247
    if-nez v4, :cond_7

    .line 248
    .line 249
    const-string p1, "showScreenshotRBTips getActivity return null"

    .line 250
    .line 251
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    .line 253
    .line 254
    goto/16 :goto_1

    .line 255
    .line 256
    :cond_7
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇〇888()Ljava/lang/String;

    .line 257
    .line 258
    .line 259
    move-result-object v1

    .line 260
    if-eqz v1, :cond_8

    .line 261
    .line 262
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 263
    .line 264
    .line 265
    move-result v1

    .line 266
    if-nez v1, :cond_9

    .line 267
    .line 268
    :cond_8
    const/4 v0, 0x1

    .line 269
    :cond_9
    if-eqz v0, :cond_a

    .line 270
    .line 271
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 272
    .line 273
    .line 274
    move-result-object v0

    .line 275
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->〇O888o0o(Landroidx/fragment/app/Fragment;)Lcom/bumptech/glide/RequestManager;

    .line 276
    .line 277
    .line 278
    move-result-object v0

    .line 279
    const v1, 0x7f080abb

    .line 280
    .line 281
    .line 282
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 283
    .line 284
    .line 285
    move-result-object v1

    .line 286
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->Oooo8o0〇(Ljava/lang/Integer;)Lcom/bumptech/glide/RequestBuilder;

    .line 287
    .line 288
    .line 289
    move-result-object v0

    .line 290
    new-instance v1, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 291
    .line 292
    invoke-direct {v1}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;-><init>()V

    .line 293
    .line 294
    .line 295
    invoke-virtual {v1}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;->o〇0()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 296
    .line 297
    .line 298
    move-result-object v1

    .line 299
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->o8O0(Lcom/bumptech/glide/TransitionOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 300
    .line 301
    .line 302
    move-result-object v0

    .line 303
    invoke-virtual {v0, v2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 304
    .line 305
    .line 306
    goto :goto_0

    .line 307
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 308
    .line 309
    .line 310
    move-result-object v0

    .line 311
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->〇O888o0o(Landroidx/fragment/app/Fragment;)Lcom/bumptech/glide/RequestManager;

    .line 312
    .line 313
    .line 314
    move-result-object v0

    .line 315
    invoke-virtual {p1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;->〇〇888()Ljava/lang/String;

    .line 316
    .line 317
    .line 318
    move-result-object v1

    .line 319
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 320
    .line 321
    .line 322
    move-result-object v0

    .line 323
    new-instance v1, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 324
    .line 325
    invoke-direct {v1}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;-><init>()V

    .line 326
    .line 327
    .line 328
    invoke-virtual {v1}, Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;->o〇0()Lcom/bumptech/glide/load/resource/drawable/DrawableTransitionOptions;

    .line 329
    .line 330
    .line 331
    move-result-object v1

    .line 332
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->o8O0(Lcom/bumptech/glide/TransitionOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 333
    .line 334
    .line 335
    move-result-object v0

    .line 336
    invoke-virtual {v0, v2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 337
    .line 338
    .line 339
    :goto_0
    const-string v0, "it"

    .line 340
    .line 341
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 342
    .line 343
    .line 344
    invoke-static {v2}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    .line 345
    .line 346
    .line 347
    move-result v0

    .line 348
    if-eqz v0, :cond_b

    .line 349
    .line 350
    invoke-virtual {v2}, Landroid/view/View;->isLayoutRequested()Z

    .line 351
    .line 352
    .line 353
    move-result v0

    .line 354
    if-nez v0, :cond_b

    .line 355
    .line 356
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 357
    .line 358
    .line 359
    move-result-object v0

    .line 360
    const-string v1, "viewLifecycleOwner"

    .line 361
    .line 362
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    .line 364
    .line 365
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 366
    .line 367
    .line 368
    move-result-object v4

    .line 369
    const/4 v5, 0x0

    .line 370
    const/4 v6, 0x0

    .line 371
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$5$1$1;

    .line 372
    .line 373
    const/4 v0, 0x0

    .line 374
    invoke-direct {v7, p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$5$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;Lkotlin/coroutines/Continuation;)V

    .line 375
    .line 376
    .line 377
    const/4 v8, 0x3

    .line 378
    const/4 v9, 0x0

    .line 379
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 380
    .line 381
    .line 382
    goto :goto_1

    .line 383
    :cond_b
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$lambda$37$$inlined$doOnLayout$1;

    .line 384
    .line 385
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$showScreenshotRBTips$lambda$37$$inlined$doOnLayout$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 386
    .line 387
    .line 388
    invoke-virtual {v2, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 389
    .line 390
    .line 391
    :goto_1
    return v3
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method private final 〇OoO0o0()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇080()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇Oo〇O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇80O()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O〇〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇〇8〇〇(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/view/AnimatedImageView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇o8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/view/AnimatedImageView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o88〇O(Landroid/content/Intent;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_7

    .line 2
    .line 3
    const-string v0, "MainActivity.intent.open.tab"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const/4 v0, 0x0

    .line 10
    const/4 v1, 0x1

    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    if-nez v2, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v2, 0x0

    .line 21
    goto :goto_1

    .line 22
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 23
    :goto_1
    if-eqz v2, :cond_2

    .line 24
    .line 25
    return-void

    .line 26
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    sparse-switch v2, :sswitch_data_0

    .line 31
    .line 32
    .line 33
    goto :goto_2

    .line 34
    :sswitch_0
    const-string v0, "me_page"

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-nez p1, :cond_3

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 48
    .line 49
    const/4 v0, 0x3

    .line 50
    invoke-virtual {p1, v0}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 51
    .line 52
    .line 53
    goto :goto_2

    .line 54
    :sswitch_1
    const-string v0, "main_doc"

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    if-nez p1, :cond_4

    .line 61
    .line 62
    goto :goto_2

    .line 63
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 68
    .line 69
    invoke-virtual {p1, v1}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 70
    .line 71
    .line 72
    goto :goto_2

    .line 73
    :sswitch_2
    const-string v1, "main_home"

    .line 74
    .line 75
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    if-nez p1, :cond_5

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 87
    .line 88
    invoke-virtual {p1, v0}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 89
    .line 90
    .line 91
    goto :goto_2

    .line 92
    :sswitch_3
    const-string v0, "tool_page"

    .line 93
    .line 94
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result p1

    .line 98
    if-nez p1, :cond_6

    .line 99
    .line 100
    goto :goto_2

    .line 101
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 106
    .line 107
    const/4 v0, 0x2

    .line 108
    invoke-virtual {p1, v0}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 109
    .line 110
    .line 111
    :cond_7
    :goto_2
    return-void

    .line 112
    nop

    .line 113
    :sswitch_data_0
    .sparse-switch
        -0x18cd0f2a -> :sswitch_3
        -0xefbac5b -> :sswitch_2
        -0x7bca4e -> :sswitch_1
        0x37cfccd6 -> :sswitch_0
    .end sparse-switch
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇o8〇8(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-class v2, Lcom/intsig/camscanner/BatchModeActivity;

    .line 8
    .line 9
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    const-string v1, "BatchModeActivity.intent.doc.id"

    .line 13
    .line 14
    const-wide/16 v2, -0x1

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    const-string v1, "BatchModeActivity.intent.tag.id"

    .line 20
    .line 21
    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 22
    .line 23
    .line 24
    const-string p2, "BatchModeActivity.intent.uris"

    .line 25
    .line 26
    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 27
    .line 28
    .line 29
    const-string p1, "BatchModeActivity.need.go.to.doc"

    .line 30
    .line 31
    const/4 p2, 0x0

    .line 32
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 33
    .line 34
    .line 35
    const-string p1, "BatchModeActivity.specific.setting"

    .line 36
    .line 37
    const/4 p3, 0x1

    .line 38
    invoke-virtual {v0, p1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 39
    .line 40
    .line 41
    const-string p1, "BatchModeActivity.specific.need.trim"

    .line 42
    .line 43
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    .line 45
    .line 46
    const-string p1, "BatchModeActivity.specific.enhance.mode"

    .line 47
    .line 48
    const/4 p2, -0x1

    .line 49
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 50
    .line 51
    .line 52
    const-string p1, "BatchModeActivity.specific.doc.name.id"

    .line 53
    .line 54
    const p2, 0x7f13046b

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 58
    .line 59
    .line 60
    const/4 p1, 0x0

    .line 61
    const-string p2, "team_token_id"

    .line 62
    .line 63
    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    .line 65
    .line 66
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    if-nez p1, :cond_0

    .line 71
    .line 72
    const-string p1, "extra_folder_id"

    .line 73
    .line 74
    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    :cond_0
    new-instance p1, Lcom/intsig/result/GetActivityResult;

    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    invoke-direct {p1, p2}, Lcom/intsig/result/GetActivityResult;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 84
    .line 85
    .line 86
    const/16 p2, 0x7e

    .line 87
    .line 88
    invoke-virtual {p1, v0, p2}, Lcom/intsig/result/GetActivityResult;->startActivityForResult(Landroid/content/Intent;I)Lcom/intsig/result/GetActivityResult;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    new-instance p2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$go2BatchModeActivityScreenshot$1;

    .line 93
    .line 94
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$go2BatchModeActivityScreenshot$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, p2}, Lcom/intsig/result/GetActivityResult;->〇8o8o〇(Lcom/intsig/result/OnForResultCallback;)Lcom/intsig/result/GetActivityResult;

    .line 98
    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOO〇〇:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oOO80o()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇00〇8oO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇080()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇00〇8oO:Z

    .line 24
    .line 25
    const/16 v0, 0x7b

    .line 26
    .line 27
    invoke-static {p0, v0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 28
    .line 29
    .line 30
    :cond_1
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oO〇(I)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Ooo08:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const-string v0, "mBinding.viewStubQrScanGuide"

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇o〇:Landroid/view/ViewStub;

    .line 14
    .line 15
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇o〇:Landroid/view/ViewStub;

    .line 28
    .line 29
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 34
    .line 35
    .line 36
    :cond_1
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇oO〇08o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo0O〇8800(Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇ooO8Ooo〇(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p3, "$this_run"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object p3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "User Operation: go integral reward"

    .line 14
    .line 15
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    sget-object p3, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;

    .line 19
    .line 20
    const-string v0, "CSHomeOperationIcon"

    .line 21
    .line 22
    invoke-virtual {p3, v0, p0}, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;->〇o〇(Ljava/lang/String;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 30
    .line 31
    if-eqz p0, :cond_0

    .line 32
    .line 33
    const/4 p1, 0x0

    .line 34
    invoke-virtual {p0, p1}, Lcom/intsig/view/ImageViewDot;->Oo08(Z)V

    .line 35
    .line 36
    .line 37
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇〇888()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇ooO〇000(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 10

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 10
    .line 11
    const-string v2, "main nav icon on show"

    .line 12
    .line 13
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v1, "it"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v8

    .line 29
    const/4 v9, 0x0

    .line 30
    if-eqz v8, :cond_0

    .line 31
    .line 32
    const-string v2, "pic"

    .line 33
    .line 34
    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const-string v3, "."

    .line 38
    .line 39
    const/4 v4, 0x0

    .line 40
    const/4 v5, 0x0

    .line 41
    const/4 v6, 0x6

    .line 42
    const/4 v7, 0x0

    .line 43
    move-object v2, v8

    .line 44
    invoke-static/range {v2 .. v7}, Lkotlin/text/StringsKt;->ooo〇8oO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    invoke-virtual {v8, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    const-string v3, "this as java.lang.String).substring(startIndex)"

    .line 53
    .line 54
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    move-object v2, v9

    .line 59
    :goto_0
    const-string v3, ".gif"

    .line 60
    .line 61
    invoke-static {v3, v2, v1}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    if-eqz v2, :cond_1

    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v3

    .line 75
    invoke-static {v2, v3, v0, v9, v9}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdUtil;->〇oo〇(Landroid/content/Context;Ljava/lang/String;Landroid/widget/ImageView;Lcom/bumptech/glide/request/RequestOptions;Lcom/bumptech/glide/request/RequestListener;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    invoke-static {v2}, Lcom/bumptech/glide/Glide;->〇O888o0o(Landroidx/fragment/app/Fragment;)Lcom/bumptech/glide/RequestManager;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v3

    .line 91
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 92
    .line 93
    .line 94
    move-result-object v2

    .line 95
    invoke-virtual {v2, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 96
    .line 97
    .line 98
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    sget-object v3, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->MAIN_NAV_ICON:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 103
    .line 104
    invoke-static {v2, v3, p1}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    new-instance v3, Lo〇00O/〇0〇O0088o;

    .line 109
    .line 110
    invoke-direct {v3, p1, p0, v2}, Lo〇00O/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;)V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getSpotShowSwitch()I

    .line 117
    .line 118
    .line 119
    move-result v3

    .line 120
    if-ne v3, v1, :cond_2

    .line 121
    .line 122
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->Oo08()Ljava/lang/Boolean;

    .line 123
    .line 124
    .line 125
    move-result-object v3

    .line 126
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    if-nez v3, :cond_2

    .line 131
    .line 132
    const/4 v3, 0x1

    .line 133
    goto :goto_2

    .line 134
    :cond_2
    const/4 v3, 0x0

    .line 135
    :goto_2
    invoke-virtual {v0, v3}, Lcom/intsig/view/ImageViewDot;->Oo08(Z)V

    .line 136
    .line 137
    .line 138
    iget-boolean v0, p1, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->hasNotifyShow:Z

    .line 139
    .line 140
    if-nez v0, :cond_3

    .line 141
    .line 142
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇80〇808〇O()V

    .line 143
    .line 144
    .line 145
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;

    .line 146
    .line 147
    const-string v2, "CSHomeOperationIcon"

    .line 148
    .line 149
    invoke-virtual {v0, v2, p1}, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;->o〇0(Ljava/lang/String;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 150
    .line 151
    .line 152
    iput-boolean v1, p1, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->hasNotifyShow:Z

    .line 153
    .line 154
    :cond_3
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0o88O(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇o〇OO80oO(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/comm/widget/CustomTextView;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x2

    .line 7
    new-array v1, v0, [I

    .line 8
    .line 9
    new-array v2, v0, [I

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 12
    .line 13
    .line 14
    move-result-object v3

    .line 15
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 16
    .line 17
    invoke-virtual {v3, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 21
    .line 22
    .line 23
    const/4 v3, 0x0

    .line 24
    aget v1, v1, v3

    .line 25
    .line 26
    aget v2, v2, v3

    .line 27
    .line 28
    sub-int/2addr v1, v2

    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 34
    .line 35
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 36
    .line 37
    .line 38
    move-result p0

    .line 39
    div-int/2addr p0, v0

    .line 40
    add-int/2addr v1, p0

    .line 41
    invoke-virtual {p1, v1}, Lcom/intsig/comm/widget/CustomTextView;->setArrowMarginLeft(I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsEntity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8o0OOOo()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-wide/16 v1, 0x0

    .line 6
    .line 7
    const-string v3, "MIAN_HOME_LOGO_SHOW_TIME"

    .line 8
    .line 9
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->OO0o〇〇〇〇0(Ljava/lang/String;J)J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide v4

    .line 17
    invoke-static {v0, v1, v4, v5}, Lcom/intsig/utils/DateTimeUtil;->〇〇808〇(JJ)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 34
    .line 35
    const v1, 0x7f0806da

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 47
    .line 48
    const v1, 0x7f0806db

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 52
    .line 53
    .line 54
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇08O:Landroid/widget/TextView;

    .line 59
    .line 60
    const/4 v1, 0x0

    .line 61
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 69
    .line 70
    const-string v1, "mBinding.ivCsLogo"

    .line 71
    .line 72
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    const/4 v2, 0x1

    .line 76
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 84
    .line 85
    .line 86
    move-result-wide v4

    .line 87
    invoke-virtual {v0, v3, v4, v5}, Lcom/intsig/utils/PreferenceUtil;->OoO8(Ljava/lang/String;J)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 95
    .line 96
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    const/4 v2, 0x0

    .line 101
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    const-wide/16 v2, 0x9c4

    .line 106
    .line 107
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initLogoOnSearch$1;

    .line 112
    .line 113
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initLogoOnSearch$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 121
    .line 122
    .line 123
    move-result-object v2

    .line 124
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 125
    .line 126
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-static {v2}, Landroidx/core/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    .line 130
    .line 131
    .line 132
    move-result v1

    .line 133
    if-nez v1, :cond_1

    .line 134
    .line 135
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇O8〇8O0oO()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    const-string v2, "onDestroyView, call animation cancel"

    .line 140
    .line 141
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 145
    .line 146
    .line 147
    goto :goto_1

    .line 148
    :cond_1
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initLogoOnSearch$lambda$27$$inlined$doOnDetach$1;

    .line 149
    .line 150
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initLogoOnSearch$lambda$27$$inlined$doOnDetach$1;-><init>(Landroid/view/View;Landroid/view/ViewPropertyAnimator;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v2, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 154
    .line 155
    .line 156
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 157
    .line 158
    .line 159
    :cond_2
    return-void
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O80OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooo800〇〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇〇0(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇〇00(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OO(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇OOO〇〇(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;IZ)V
    .locals 11

    .line 1
    move-object v0, p0

    .line 2
    move-object v10, p1

    .line 3
    const-string v1, "this$0"

    .line 4
    .line 5
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v1, "$captureMode"

    .line 9
    .line 10
    move-object v2, p2

    .line 11
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v1, v10, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mClickLimit:Lcom/intsig/utils/ClickLimit;

    .line 17
    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-virtual {v1, p0}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_0

    .line 25
    .line 26
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 27
    .line 28
    const-string v1, "click camera too fast"

    .line 29
    .line 30
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 35
    .line 36
    const-string v1, "User Operation: camera"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/util/TimeLogger;->〇80〇808〇O()V

    .line 42
    .line 43
    .line 44
    const/4 v4, 0x0

    .line 45
    const/4 v5, 0x0

    .line 46
    const/16 v8, 0x18

    .line 47
    .line 48
    const/4 v9, 0x0

    .line 49
    move-object v0, p1

    .line 50
    move-object v1, p2

    .line 51
    move-object v2, p3

    .line 52
    move-object v3, p4

    .line 53
    move/from16 v6, p5

    .line 54
    .line 55
    move/from16 v7, p6

    .line 56
    .line 57
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O888Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOo00()Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇〇888()V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method public static final synthetic 〇〇〇O〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO:Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O08o(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "encryptId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "sid"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "url"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "handleESignFromWeb encryptId == "

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v2, " ,sid == "

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->o〇8oOO88(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final O08〇oO8〇()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇80〇808〇O()Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 6
    .line 7
    const-string v2, "cs_home"

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    return-object v2

    .line 12
    :cond_0
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    const-string v0, "cs_main"

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_1
    return-object v2
    .line 20
    .line 21
.end method

.method public final O0o0(Landroid/view/View;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ILcom/intsig/camscanner/purchase/track/FunctionEntrance;Z)V
    .locals 13
    .param p2    # Lcom/intsig/camscanner/capture/CaptureMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "captureMode"

    .line 2
    .line 3
    move-object v4, p2

    .line 4
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/business/folders/OfflineFolder;

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/business/folders/OfflineFolder;-><init>(Landroid/app/Activity;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 21
    .line 22
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x0

    .line 27
    const/4 v9, 0x1

    .line 28
    move-object v10, p0

    .line 29
    if-ne v1, v9, :cond_1

    .line 30
    .line 31
    iget-object v1, v10, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 32
    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->〇o〇88〇8()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    goto :goto_0

    .line 40
    :cond_0
    const/4 v1, 0x0

    .line 41
    :goto_0
    if-eqz v1, :cond_1

    .line 42
    .line 43
    const/4 v11, 0x1

    .line 44
    goto :goto_1

    .line 45
    :cond_1
    const/4 v11, 0x0

    .line 46
    :goto_1
    new-instance v12, Lo〇00O/〇0000OOO;

    .line 47
    .line 48
    move-object v1, v12

    .line 49
    move-object v2, p1

    .line 50
    move-object v3, p0

    .line 51
    move-object v4, p2

    .line 52
    move-object/from16 v5, p5

    .line 53
    .line 54
    move-object/from16 v6, p3

    .line 55
    .line 56
    move/from16 v7, p4

    .line 57
    .line 58
    move/from16 v8, p6

    .line 59
    .line 60
    invoke-direct/range {v1 .. v8}, Lo〇00O/〇0000OOO;-><init>(Landroid/view/View;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;IZ)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v11, v9, v12}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->o〇0(ZILcom/intsig/camscanner/business/folders/OfflineFolder$OnUsesTipsListener;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public final O0〇8〇()Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :goto_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O88()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    if-eq v0, v1, :cond_1

    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_0

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 27
    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->O〇o8()V

    .line 31
    .line 32
    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(I)V

    .line 40
    .line 41
    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final O8oO0(Landroidx/recyclerview/widget/RecyclerView;Landroidx/appcompat/widget/Toolbar;F)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/appcompat/widget/Toolbar;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "fragmentToolbar"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇080()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_5

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O88O:Landroidx/appcompat/widget/Toolbar;

    .line 22
    .line 23
    if-nez v0, :cond_0

    .line 24
    .line 25
    goto :goto_3

    .line 26
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_1

    .line 31
    .line 32
    const/4 v0, 0x1

    .line 33
    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    .line 35
    :goto_0
    if-eqz v0, :cond_4

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->〇080()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    if-eqz p2, :cond_2

    .line 46
    .line 47
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇oO:Landroid/view/View;

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_2
    const/4 p2, 0x0

    .line 51
    :goto_1
    if-nez p2, :cond_3

    .line 52
    .line 53
    goto :goto_2

    .line 54
    :cond_3
    invoke-virtual {p2, p3}, Landroid/view/View;->setElevation(F)V

    .line 55
    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_4
    invoke-virtual {p2, p3}, Landroid/view/View;->setElevation(F)V

    .line 59
    .line 60
    .line 61
    :goto_2
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇Oo(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 62
    .line 63
    .line 64
    :cond_5
    :goto_3
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final O8〇()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO00〇0o〇〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    if-eqz v0, :cond_2

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇08O:Landroid/widget/TextView;

    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const v1, 0x7f1301b1

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇08O:Landroid/widget/TextView;

    .line 41
    .line 42
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    .line 44
    .line 45
    :goto_2
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final OOo00()Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO〇000()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOo0:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OoO〇OOo8o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->〇8o0o0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇00O()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkStoragePermission$1;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$checkStoragePermission$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-static {v0, v1, v2}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇0o8o8〇()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->O〇0o8〇()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇0o8〇()Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇〇888()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    :cond_0
    return-object v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇O〇〇O8:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇88()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->o8o〇〇0O(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    const-string v1, "mBinding.mainBottomTab"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O88O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇oo8O80()Lcom/intsig/view/AnimatedImageView;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇OOo8〇0:Lcom/intsig/view/AnimatedImageView;

    .line 6
    .line 7
    const-string v1, "mBinding.aivMask"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    const/4 v1, 0x0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const v3, 0x7f0a0ab0

    .line 25
    .line 26
    .line 27
    if-ne v2, v3, :cond_3

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 30
    .line 31
    const-string v2, "click vip icon"

    .line 32
    .line 33
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    if-nez p1, :cond_2

    .line 41
    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    :cond_2
    check-cast p1, Ljava/lang/Integer;

    .line 47
    .line 48
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 49
    .line 50
    .line 51
    move-result p1

    .line 52
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o88o88(I)V

    .line 53
    .line 54
    .line 55
    goto/16 :goto_b

    .line 56
    .line 57
    :cond_3
    :goto_1
    const-string v2, "CSHome"

    .line 58
    .line 59
    if-nez v0, :cond_4

    .line 60
    .line 61
    goto :goto_3

    .line 62
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    const v4, 0x7f0a09f6

    .line 67
    .line 68
    .line 69
    if-ne v3, v4, :cond_6

    .line 70
    .line 71
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 72
    .line 73
    const-string v0, "click qr scan"

    .line 74
    .line 75
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    new-instance p1, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 79
    .line 80
    invoke-direct {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o0ooO(Landroidx/fragment/app/Fragment;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 88
    .line 89
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    sget-object v0, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    if-eqz v0, :cond_5

    .line 100
    .line 101
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 102
    .line 103
    goto :goto_2

    .line 104
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 105
    .line 106
    :goto_2
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    sget-object v0, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_QR_CODE_ONLY:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 111
    .line 112
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    const v0, 0x138d8

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 124
    .line 125
    .line 126
    const-string p1, "qr"

    .line 127
    .line 128
    invoke-static {v2, p1}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    goto/16 :goto_b

    .line 132
    .line 133
    :cond_6
    :goto_3
    const/4 v3, 0x1

    .line 134
    if-nez v0, :cond_7

    .line 135
    .line 136
    goto :goto_5

    .line 137
    :cond_7
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 138
    .line 139
    .line 140
    move-result v4

    .line 141
    const v5, 0x7f0a088f

    .line 142
    .line 143
    .line 144
    if-ne v4, v5, :cond_a

    .line 145
    .line 146
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 147
    .line 148
    const-string v2, "User Operation: go sns"

    .line 149
    .line 150
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    invoke-static {v2}, Lcom/intsig/camscanner/attention/RewardAPI;->getType(Landroid/content/Context;)I

    .line 162
    .line 163
    .line 164
    move-result v2

    .line 165
    if-nez v2, :cond_8

    .line 166
    .line 167
    const-string v2, "wechat"

    .line 168
    .line 169
    goto :goto_4

    .line 170
    :cond_8
    const-string v2, "facebook"

    .line 171
    .line 172
    :goto_4
    const-string v4, "cloud_icon_click"

    .line 173
    .line 174
    const-string v5, "type"

    .line 175
    .line 176
    invoke-static {v0, v4, v5, v2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0〇〇00〇o()Lcom/intsig/camscanner/attention/RewardAPI;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    if-eqz v0, :cond_9

    .line 184
    .line 185
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 186
    .line 187
    .line 188
    move-result-object v2

    .line 189
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v4

    .line 193
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/camscanner/attention/RewardAPI;->〇o00〇〇Oo(Landroid/app/Activity;ZLjava/lang/String;)V

    .line 194
    .line 195
    .line 196
    :cond_9
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 197
    .line 198
    .line 199
    goto/16 :goto_b

    .line 200
    .line 201
    :cond_a
    :goto_5
    if-nez v0, :cond_b

    .line 202
    .line 203
    goto :goto_6

    .line 204
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 205
    .line 206
    .line 207
    move-result p1

    .line 208
    const v1, 0x7f0a0943

    .line 209
    .line 210
    .line 211
    if-ne p1, v1, :cond_c

    .line 212
    .line 213
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 214
    .line 215
    const-string v0, "User Operation: go integral reward"

    .line 216
    .line 217
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    .line 219
    .line 220
    const-string p1, "sign_now"

    .line 221
    .line 222
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 226
    .line 227
    .line 228
    move-result-object p1

    .line 229
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 230
    .line 231
    .line 232
    move-result-object v0

    .line 233
    invoke-static {v0}, Lcom/intsig/camscanner/web/UrlUtil;->o〇0OOo〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    invoke-static {p1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    goto/16 :goto_b

    .line 241
    .line 242
    :cond_c
    :goto_6
    if-nez v0, :cond_d

    .line 243
    .line 244
    goto :goto_7

    .line 245
    :cond_d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 246
    .line 247
    .line 248
    move-result p1

    .line 249
    const v1, 0x7f0a093a

    .line 250
    .line 251
    .line 252
    if-ne p1, v1, :cond_e

    .line 253
    .line 254
    const-string p1, "free_icon"

    .line 255
    .line 256
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    .line 258
    .line 259
    sget-object p1, Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;->〇080:Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;

    .line 260
    .line 261
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;->Oo08(Landroidx/fragment/app/FragmentActivity;)V

    .line 266
    .line 267
    .line 268
    goto/16 :goto_b

    .line 269
    .line 270
    :cond_e
    :goto_7
    if-nez v0, :cond_f

    .line 271
    .line 272
    goto :goto_9

    .line 273
    :cond_f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 274
    .line 275
    .line 276
    move-result p1

    .line 277
    const v1, 0x7f0a0d7c

    .line 278
    .line 279
    .line 280
    if-ne p1, v1, :cond_11

    .line 281
    .line 282
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 283
    .line 284
    const-string v0, "click search"

    .line 285
    .line 286
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .line 288
    .line 289
    new-instance p1, Landroid/content/Intent;

    .line 290
    .line 291
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 292
    .line 293
    .line 294
    move-result-object v0

    .line 295
    const-class v1, Lcom/intsig/camscanner/searchactivity/SearchActivity;

    .line 296
    .line 297
    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 301
    .line 302
    .line 303
    move-result-object v0

    .line 304
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 305
    .line 306
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 307
    .line 308
    .line 309
    move-result v0

    .line 310
    const-string v1, "search"

    .line 311
    .line 312
    if-nez v0, :cond_10

    .line 313
    .line 314
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    .line 316
    .line 317
    const-string v0, "cs_home"

    .line 318
    .line 319
    goto :goto_8

    .line 320
    :cond_10
    const-string v0, "CSMain"

    .line 321
    .line 322
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    const-string v0, "cs_main"

    .line 326
    .line 327
    :goto_8
    const-string v1, "intent_from_part"

    .line 328
    .line 329
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    .line 331
    .line 332
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 333
    .line 334
    .line 335
    goto/16 :goto_b

    .line 336
    .line 337
    :cond_11
    :goto_9
    if-nez v0, :cond_12

    .line 338
    .line 339
    goto :goto_a

    .line 340
    :cond_12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 341
    .line 342
    .line 343
    move-result p1

    .line 344
    const v1, 0x7f0a0a2b

    .line 345
    .line 346
    .line 347
    if-ne p1, v1, :cond_15

    .line 348
    .line 349
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 350
    .line 351
    .line 352
    move-result-object p1

    .line 353
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 354
    .line 355
    .line 356
    move-result-object p1

    .line 357
    const-string v0, "select_list_cancel"

    .line 358
    .line 359
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 363
    .line 364
    .line 365
    move-result-object p1

    .line 366
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 367
    .line 368
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 369
    .line 370
    .line 371
    move-result p1

    .line 372
    if-eqz p1, :cond_14

    .line 373
    .line 374
    if-eq p1, v3, :cond_13

    .line 375
    .line 376
    goto :goto_b

    .line 377
    :cond_13
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 378
    .line 379
    if-eqz p1, :cond_19

    .line 380
    .line 381
    invoke-virtual {p1}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->onBackPressed()Z

    .line 382
    .line 383
    .line 384
    goto :goto_b

    .line 385
    :cond_14
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 386
    .line 387
    if-eqz p1, :cond_19

    .line 388
    .line 389
    invoke-virtual {p1}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->onBackPressed()Z

    .line 390
    .line 391
    .line 392
    goto :goto_b

    .line 393
    :cond_15
    :goto_a
    if-nez v0, :cond_16

    .line 394
    .line 395
    goto :goto_b

    .line 396
    :cond_16
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 397
    .line 398
    .line 399
    move-result p1

    .line 400
    const v0, 0x7f0a177f

    .line 401
    .line 402
    .line 403
    if-ne p1, v0, :cond_19

    .line 404
    .line 405
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 406
    .line 407
    .line 408
    move-result-object p1

    .line 409
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->Ooo()Ljava/lang/String;

    .line 410
    .line 411
    .line 412
    move-result-object p1

    .line 413
    const-string v0, "select_list_select_all"

    .line 414
    .line 415
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    .line 417
    .line 418
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 419
    .line 420
    .line 421
    move-result-object p1

    .line 422
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 423
    .line 424
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 425
    .line 426
    .line 427
    move-result p1

    .line 428
    if-eqz p1, :cond_18

    .line 429
    .line 430
    if-eq p1, v3, :cond_17

    .line 431
    .line 432
    goto :goto_b

    .line 433
    :cond_17
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 434
    .line 435
    if-eqz p1, :cond_19

    .line 436
    .line 437
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->oOoO8OO〇()V

    .line 438
    .line 439
    .line 440
    goto :goto_b

    .line 441
    :cond_18
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 442
    .line 443
    if-eqz p1, :cond_19

    .line 444
    .line 445
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->Oo〇〇〇〇()V

    .line 446
    .line 447
    .line 448
    :cond_19
    :goto_b
    return-void
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public enableToolbar()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeLifecycleObserver;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeLifecycleObserver;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 19
    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl$Companion;

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;

    .line 32
    .line 33
    sget-object v0, Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;->〇080:Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/password_identify/PasswordIdentifyManager;->oo88o8O()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooo008()V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0o〇o()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOo00()Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇O〇〇O8:Landroid/widget/RelativeLayout;

    .line 53
    .line 54
    const-string v2, "mBinding.rlRootMain"

    .line 55
    .line 56
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->OO0o〇〇〇〇0(Landroid/view/View;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO0〇O()V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0oO()V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oOO8oo0()V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇80oO〇()V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0o88Oo〇()V

    .line 75
    .line 76
    .line 77
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;

    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/CSInternalActionCallbackImpl;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 84
    .line 85
    .line 86
    invoke-static {v0}, Lcom/intsig/camscanner/web/WebAction;->o80ooO(Lcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;)V

    .line 87
    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/InnerNoviceListener;

    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/InnerNoviceListener;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oooo8o0〇(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0〇o8〇()V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇oo()V

    .line 113
    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->o〇O()V

    .line 120
    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o088〇〇()V

    .line 123
    .line 124
    .line 125
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 126
    .line 127
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 132
    .line 133
    .line 134
    move-result-object v2

    .line 135
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->Oo08()V

    .line 139
    .line 140
    .line 141
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 142
    .line 143
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇o〇OoO8()V

    .line 144
    .line 145
    .line 146
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0〇O80ooo(Landroid/os/Bundle;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    const-string v0, "viewLifecycleOwner"

    .line 154
    .line 155
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    const/4 v2, 0x0

    .line 163
    const/4 v3, 0x0

    .line 164
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initialize$2;

    .line 165
    .line 166
    const/4 p1, 0x0

    .line 167
    invoke-direct {v4, p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initialize$2;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 168
    .line 169
    .line 170
    const/4 v5, 0x3

    .line 171
    const/4 v6, 0x0

    .line 172
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 173
    .line 174
    .line 175
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    const/4 v0, 0x5

    .line 180
    new-array v0, v0, [Landroid/view/View;

    .line 181
    .line 182
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇〇08O:Landroid/widget/TextView;

    .line 183
    .line 184
    const/4 v2, 0x0

    .line 185
    aput-object v1, v0, v2

    .line 186
    .line 187
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO〇00〇8oO:Lcom/airbnb/lottie/LottieAnimationView;

    .line 188
    .line 189
    const/4 v3, 0x1

    .line 190
    aput-object v1, v0, v3

    .line 191
    .line 192
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 193
    .line 194
    const/4 v4, 0x2

    .line 195
    aput-object v1, v0, v4

    .line 196
    .line 197
    const/4 v1, 0x3

    .line 198
    iget-object v5, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 199
    .line 200
    aput-object v5, v0, v1

    .line 201
    .line 202
    const/4 v1, 0x4

    .line 203
    iget-object v5, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 204
    .line 205
    aput-object v5, v0, v1

    .line 206
    .line 207
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 208
    .line 209
    .line 210
    new-array v0, v4, [Landroid/view/View;

    .line 211
    .line 212
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo0:Landroid/widget/ImageView;

    .line 213
    .line 214
    aput-object v1, v0, v2

    .line 215
    .line 216
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOO〇〇:Landroid/widget/TextView;

    .line 217
    .line 218
    aput-object p1, v0, v3

    .line 219
    .line 220
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 221
    .line 222
    .line 223
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O〇88O8O()V

    .line 224
    .line 225
    .line 226
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo0o0o8()V

    .line 227
    .line 228
    .line 229
    sget-object p1, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->〇080:Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;

    .line 230
    .line 231
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 232
    .line 233
    .line 234
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 235
    .line 236
    .line 237
    move-result-wide v0

    .line 238
    iget-wide v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇o〇:J

    .line 239
    .line 240
    sub-long/2addr v0, v2

    .line 241
    new-instance p1, Ljava/lang/StringBuilder;

    .line 242
    .line 243
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 244
    .line 245
    .line 246
    const-string v2, "MainFragment from onAttach to init finish == "

    .line 247
    .line 248
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 252
    .line 253
    .line 254
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 255
    .line 256
    .line 257
    move-result-object p1

    .line 258
    const-string v0, "STARTDISPLAY"

    .line 259
    .line 260
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    return-void
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public interceptBackPressed()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v1, "null cannot be cast to non-null type com.intsig.mvp.fragment.BaseChangeFragment"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    check-cast v0, Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->onBackPressed()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    const/4 v1, 0x1

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    return v1

    .line 32
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->〇O〇()V

    .line 37
    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/camscanner/service/UploadUtils;->〇o〇()Z

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 47
    .line 48
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    const-string v2, "cs_main"

    .line 53
    .line 54
    if-eqz v0, :cond_4

    .line 55
    .line 56
    if-eq v0, v1, :cond_3

    .line 57
    .line 58
    const/4 v3, 0x2

    .line 59
    if-eq v0, v3, :cond_2

    .line 60
    .line 61
    const/4 v3, 0x3

    .line 62
    if-eq v0, v3, :cond_1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_1
    const-string v2, "cs_me"

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    const-string v2, "cs_application"

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_3
    const-string v2, "cs_home"

    .line 72
    .line 73
    :cond_4
    :goto_0
    const-string v0, "exit"

    .line 74
    .line 75
    const-string v3, "from_part"

    .line 76
    .line 77
    const-string v4, "CSTab"

    .line 78
    .line 79
    invoke-static {v4, v0, v3, v2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;

    .line 83
    .line 84
    if-eqz v0, :cond_5

    .line 85
    .line 86
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainHomeAdBackControl;->〇o〇()V

    .line 87
    .line 88
    .line 89
    :cond_5
    return v1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final o00o0O〇〇o(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇08〇o0O:Lcom/intsig/camscanner/view/SpaceStatusBarView;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o0〇OO008O(IZ)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "openTab: index="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    if-ltz p1, :cond_0

    .line 25
    .line 26
    const/4 v2, 0x4

    .line 27
    if-ge p1, v2, :cond_0

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    :cond_0
    if-eqz v1, :cond_1

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 37
    .line 38
    invoke-virtual {v0, p1, p2}, Landroidx/viewpager2/widget/ViewPager2;->setCurrentItem(IZ)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    .line 43
    .line 44
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 45
    .line 46
    .line 47
    const-string v1, "openTab: BUT ERROR index="

    .line 48
    .line 49
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final o8o0(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    return-object v0

    .line 11
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    .line 12
    .line 13
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 17
    .line 18
    .line 19
    iget p1, v0, Landroid/graphics/Rect;->top:I

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o00〇〇Oo()Lcom/intsig/camscanner/util/StatusBarHelper;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o〇()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    sub-int/2addr p1, v1

    .line 30
    iput p1, v0, Landroid/graphics/Rect;->top:I

    .line 31
    .line 32
    iget p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o00〇〇Oo()Lcom/intsig/camscanner/util/StatusBarHelper;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/StatusBarHelper;->〇o〇()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    sub-int/2addr p1, v1

    .line 43
    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 44
    .line 45
    :cond_1
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o8oo0OOO()Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    if-eq v1, v0, :cond_0

    .line 16
    .line 17
    return v2

    .line 18
    :cond_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇00〇8oO:Z

    .line 19
    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    return v2

    .line 23
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const-string v3, "PermissionFragment"

    .line 28
    .line 29
    invoke-virtual {v1, v3}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-eqz v1, :cond_2

    .line 34
    .line 35
    instance-of v3, v1, Lcom/intsig/permission/PermissionFragment;

    .line 36
    .line 37
    if-eqz v3, :cond_2

    .line 38
    .line 39
    move-object v3, v1

    .line 40
    check-cast v3, Lcom/intsig/permission/PermissionFragment;

    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/intsig/permission/PermissionFragment;->oOo〇08〇()Z

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-eqz v3, :cond_2

    .line 47
    .line 48
    check-cast v1, Lcom/intsig/permission/PermissionFragment;

    .line 49
    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/permission/PermissionFragment;->oO〇oo(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .line 52
    .line 53
    return v2

    .line 54
    :catch_0
    move-exception v1

    .line 55
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 56
    .line 57
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 58
    .line 59
    .line 60
    :cond_2
    return v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oO8(Z)I
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return p1

    .line 5
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 6
    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    if-eqz p1, :cond_1

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇8o8o〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const/4 p1, 0x0

    .line 21
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->oO80()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    if-eqz p1, :cond_2

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇80〇808〇O()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    goto :goto_1

    .line 34
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOo(Landroid/content/Context;)I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    :goto_1
    return p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final oO88〇0O8O(JILandroid/net/Uri;)V
    .locals 4
    .param p4    # Landroid/net/Uri;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "uri"

    .line 2
    .line 3
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Landroid/content/Intent;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const-class v2, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 13
    .line 14
    const-string v3, "com.intsig.camscanner.NEW_DOC"

    .line 15
    .line 16
    invoke-direct {v0, v3, p4, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 17
    .line 18
    .line 19
    const-string p4, "scanner_image_src"

    .line 20
    .line 21
    invoke-virtual {v0, p4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 22
    .line 23
    .line 24
    const-string p3, "tag_id"

    .line 25
    .line 26
    invoke-virtual {v0, p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 27
    .line 28
    .line 29
    const-string p1, "extra_folder_id"

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇0o8〇()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    .line 37
    .line 38
    const/16 p1, 0x80

    .line 39
    .line 40
    invoke-virtual {p0, v0, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final oOO〇0o8〇()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OoOO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO〇O0O(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "templateId"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createTemplateFolder$1;

    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-direct {v1, p1, p0, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createTemplateFolder$1;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-static {}, Lcom/intsig/camscanner/control/ShareControl;->o〇8()Lcom/intsig/camscanner/control/ShareControl;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/intsig/camscanner/control/ShareControl;->〇0000OOO(IILandroid/content/Intent;)V

    const/16 v1, 0x66

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    if-eq p1, v1, :cond_e

    const/16 v0, 0x92

    const-string v1, "mActivity"

    if-eq p1, v0, :cond_d

    const v0, 0x186a1

    const/4 v5, 0x0

    if-eq p1, v0, :cond_b

    const/16 v0, 0x7b

    if-eq p1, v0, :cond_8

    const/16 v0, 0x7c

    if-eq p1, v0, :cond_6

    const/16 v0, 0x8e

    if-eq p1, v0, :cond_5

    const/16 v0, 0x8f

    if-eq p1, v0, :cond_4

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto/16 :goto_2

    .line 4
    :pswitch_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇00O0:Ljava/lang/String;

    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇08oOOO0:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇O80o8OO(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5
    :pswitch_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->〇〇0o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc8

    .line 7
    invoke-static {v0, v5, v1, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇8(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;I)V

    goto/16 :goto_2

    :pswitch_2
    const/16 v0, 0x1f4

    if-ne v0, p2, :cond_0

    .line 8
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    iget-object v7, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    new-instance v8, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-direct {v8, v3, v4, v2, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v0

    .line 10
    invoke-direct/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v7, p3

    .line 11
    invoke-static/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_0
    const/16 v0, 0xe8

    if-ne v0, p2, :cond_1

    .line 12
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    iget-object v7, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v8, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    const/16 v1, 0xd1

    invoke-direct {v8, v3, v1, v2, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v0

    .line 14
    invoke-direct/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v7, p3

    .line 15
    invoke-static/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_1
    const/16 v0, 0x8d

    if-ne v0, p2, :cond_14

    .line 16
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    iget-object v7, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v8, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    const/16 v1, 0xca

    invoke-direct {v8, v3, v1, v2, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v0

    .line 18
    invoke-direct/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 19
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_CS_SCAN:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->Oo〇O(Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;)V

    .line 20
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->〇0(Z)V

    .line 21
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, v0

    move-object v7, p3

    .line 22
    invoke-static/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_2
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x6

    const/4 v11, 0x0

    move-object v6, v0

    move-object v7, p3

    .line 23
    invoke-static/range {v6 .. v11}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    goto/16 :goto_2

    .line 24
    :pswitch_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->o〇8oOO88()Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 25
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    goto/16 :goto_2

    :pswitch_4
    if-ne p2, v4, :cond_14

    if-eqz p3, :cond_14

    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    move-result-object v0

    const-string v1, "targetDirSyncId"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    goto/16 :goto_2

    :pswitch_5
    if-eqz p3, :cond_3

    :try_start_0
    const-string v0, "extra_folder_id"

    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇0o8〇()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extra_offline_folder"

    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo〇O0o〇()Z

    move-result v1

    .line 29
    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    const-string v0, "import_pic"

    .line 30
    invoke-static {v0}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0, p3}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    .line 32
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_4
    if-ne p2, v4, :cond_14

    if-eqz p3, :cond_14

    .line 33
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    iget-object v6, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    invoke-direct {v1, v3, v4, v2, v5}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, v0

    move-object v3, v6

    move-object v4, v1

    move v6, v7

    move-object v7, v8

    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v3, p3

    .line 34
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    goto/16 :goto_2

    .line 35
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇0〇O0088o(Landroidx/appcompat/app/AppCompatActivity;)V

    goto/16 :goto_2

    :cond_6
    if-ne p2, v4, :cond_14

    .line 36
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object v0

    const v1, 0x7f130692

    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    if-eqz p3, :cond_7

    const-string v0, "view_doc_uri"

    .line 37
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_7

    .line 38
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 39
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->o〇8oOO88(J)Lcom/intsig/camscanner/datastruct/DocItem;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 40
    sget-object v3, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    new-instance v5, Lo〇00O/Oooo8o0〇;

    invoke-direct {v5, v0, v1}, Lo〇00O/Oooo8o0〇;-><init>(J)V

    invoke-virtual {v3, v4, v2, v5}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->oo88o8O(Landroidx/fragment/app/FragmentActivity;Ljava/util/List;Lcom/intsig/callback/Callback0;)V

    .line 41
    :cond_7
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o0〇(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 42
    :cond_8
    iput-boolean v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OO〇00〇8oO:Z

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇080()Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_9

    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 45
    :cond_9
    invoke-static {}, Lcom/intsig/camscanner/util/SwitchControl;->〇080()Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_a

    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O00OoO〇()V

    :cond_a
    :goto_0
    return-void

    .line 47
    :cond_b
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooO:Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;

    if-eqz v0, :cond_14

    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    move-result-object v5

    :cond_c
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/dialog/docpage/ShareDirGuideDialog;->〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/FolderStackManager;)V

    goto/16 :goto_2

    .line 48
    :cond_d
    sget-object v0, Lcom/intsig/tsapp/account/login_task/GoogleLoginControl;->O8:Lcom/intsig/tsapp/account/login_task/GoogleLoginControl$Companion;

    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v2, p3}, Lcom/intsig/tsapp/account/login_task/GoogleLoginControl$Companion;->oO80(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 49
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ON SYS_CAMERA RESULT: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eq p2, v4, :cond_f

    return-void

    .line 50
    :cond_f
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult() mTmpPhotoFile "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 52
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_10

    goto :goto_1

    :cond_10
    const/4 v2, 0x0

    :cond_11
    :goto_1
    const v3, 0x7f130085

    if-nez v2, :cond_13

    .line 53
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 55
    new-instance v0, Ljava/io/File;

    .line 56
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    move-result-object v1

    const-string v4, ".jpg"

    .line 57
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    :try_start_1
    invoke-static {v2, v0}, Lcom/intsig/utils/FileUtil;->Oo08(Ljava/io/File;Ljava/io/File;)V

    .line 60
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇o0oOO8()J

    move-result-wide v1

    .line 61
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇〇8O0〇8(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v4, "getFileUriFromFilePath(targetFile)"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x2

    .line 62
    invoke-virtual {p0, v1, v2, v4, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO88〇0O8O(JILandroid/net/Uri;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 63
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v1, v3}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 64
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_12
    const-string v1, "tempFile is not exists"

    .line 65
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 66
    :cond_13
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0, v3}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 67
    :cond_14
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    if-eqz v0, :cond_15

    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o00〇〇Oo(IILandroid/content/Intent;)V

    :cond_15
    return-void

    :pswitch_data_0
    .packed-switch 0x80
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x8a
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 7
    .line 8
    .line 9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    iput-wide v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇o〇:J

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/launch/LaunchEvent;->〇〇888()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const/4 v0, 0x0

    .line 23
    if-eqz p1, :cond_0

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o808o8o08()Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-nez p1, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x1

    .line 32
    :cond_0
    if-eqz v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->OooO〇()V

    .line 39
    .line 40
    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onBackFromCardDetailMoveCopy(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "cardDetailMovecopyEvent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇〇〇0〇〇0()Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;->〇080()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onCaptureImportFileReceived(Lcom/intsig/camscanner/eventbus/ImportFileEvent;)V
    .locals 8
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity$Companion;->〇080()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "onCaptureImportFileReceived"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    const-string v1, "mActivity"

    .line 19
    .line 20
    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v4, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    const/4 v2, 0x0

    .line 27
    const/4 v5, 0x0

    .line 28
    const/4 v6, -0x1

    .line 29
    invoke-direct {v4, v5, v6, v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 30
    .line 31
    .line 32
    const/4 v5, 0x0

    .line 33
    const/4 v6, 0x4

    .line 34
    const/4 v7, 0x0

    .line 35
    move-object v2, v0

    .line 36
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/ImportFileEvent;->〇080()Landroid/content/Intent;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    const/4 v4, 0x1

    .line 44
    const/4 v5, 0x0

    .line 45
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;->oO(Lcom/intsig/camscanner/mainmenu/toolpage/ToolFunctionControl;Landroid/content/Intent;ZZILjava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/experiment/GalleryCacheExp;->〇080()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    const/4 v0, 0x0

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    const/4 v3, 0x0

    .line 23
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onCreate$1;

    .line 24
    .line 25
    invoke-direct {v4, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onCreate$1;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 26
    .line 27
    .line 28
    const/4 v5, 0x2

    .line 29
    const/4 v6, 0x0

    .line 30
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 31
    .line 32
    .line 33
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-eqz p1, :cond_1

    .line 38
    .line 39
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    const/4 v3, 0x0

    .line 48
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onCreate$2;

    .line 49
    .line 50
    invoke-direct {v4, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onCreate$2;-><init>(Lkotlin/coroutines/Continuation;)V

    .line 51
    .line 52
    .line 53
    const/4 v5, 0x2

    .line 54
    const/4 v6, 0x0

    .line 55
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 56
    .line 57
    .line 58
    :cond_1
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 7
    .line 8
    .line 9
    move-result-object p3

    .line 10
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->O〇〇O80o8()Lcom/intsig/camscanner/mainmenu/mainactivity/asyncinflate/AsyncInflater;

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->provideLayoutResourceId()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/asyncinflate/AsyncInflater;->〇o00〇〇Oo(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p3

    .line 22
    const/4 v0, 0x0

    .line 23
    if-nez p3, :cond_0

    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->provideLayoutResourceId()I

    .line 26
    .line 27
    .line 28
    move-result p3

    .line 29
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    :cond_0
    if-eqz p3, :cond_1

    .line 34
    .line 35
    :try_start_0
    invoke-static {p3}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 36
    .line 37
    .line 38
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    goto :goto_0

    .line 40
    :catch_0
    move-exception p3

    .line 41
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 42
    .line 43
    invoke-static {v1, p3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 44
    .line 45
    .line 46
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇o00〇〇Oo(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    goto :goto_0

    .line 51
    :cond_1
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇o00〇〇Oo(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 56
    .line 57
    if-eqz p1, :cond_2

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    goto :goto_1

    .line 64
    :cond_2
    const/4 p1, 0x0

    .line 65
    :goto_1
    return-object p1
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroy()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onDestroy"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08O()Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CommonUtil;->〇o〇(Landroid/app/Activity;ZZ)V

    .line 27
    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o00〇88〇08:Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment$Companion;

    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment$Companion;->Oo08(Z)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onDestroyView"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/web/WebAction;->o80ooO(Lcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oooo8o0〇(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 23
    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇80〇808〇O()V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7

    .line 1
    const/16 v0, 0x18

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo〇〇〇〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    if-eqz v2, :cond_0

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o800()V

    .line 18
    .line 19
    .line 20
    sget-object v1, Lcom/intsig/camscanner/capture/writeboard/CsBluetoothDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/capture/writeboard/CsBluetoothDialog$Companion;

    .line 21
    .line 22
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onKeyDown$1$1;

    .line 23
    .line 24
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onKeyDown$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 25
    .line 26
    .line 27
    const/4 v4, 0x0

    .line 28
    const/4 v5, 0x4

    .line 29
    const/4 v6, 0x0

    .line 30
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/capture/writeboard/CsBluetoothDialog$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/writeboard/CsBluetoothDialog$Companion;Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    const/4 p1, 0x1

    .line 34
    return p1

    .line 35
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    return p1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->oO80()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final onReceiveDirShareEventFromQr(Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle$DuuidJson;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle$DuuidJson;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "duuidJson"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p1, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle$DuuidJson;->duuid:Ljava/lang/String;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/camscanner/capture/qrcode/QRCodeResultHandle$DuuidJson;->sid:Ljava/lang/String;

    .line 9
    .line 10
    invoke-virtual {p0, v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇O80o8OO(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final onReceiveLoginFinish(Lcom/intsig/tsapp/account/model/LoginFinishEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onReceiveLoginFinish: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 24
    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->〇0o88O()V

    .line 28
    .line 29
    .line 30
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 31
    .line 32
    if-eqz p1, :cond_1

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->O8()V

    .line 35
    .line 36
    .line 37
    :cond_1
    const-wide/16 v0, -0x2

    .line 38
    .line 39
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O08o〇(J)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 43
    .line 44
    if-eqz p1, :cond_2

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;->o00o0O〇〇o()V

    .line 47
    .line 48
    .line 49
    :cond_2
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onReceiveUnsubscribeScaffoldMeRedDot(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$UnsubscribeScaffoldMeRedDot;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$UnsubscribeScaffoldMeRedDot;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "redDot"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 11
    .line 12
    const/4 v0, 0x3

    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->〇O〇(IZ)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onResume"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v1, "CSMain"

    .line 12
    .line 13
    invoke-static {v1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0〇8o〇()V

    .line 17
    .line 18
    .line 19
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/GpDropCnlConfiguration;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/GpDropCnlConfiguration;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/GpDropCnlConfiguration;->〇080(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;)Lkotlin/Pair;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Ljava/lang/Boolean;

    .line 34
    .line 35
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    const/4 v3, 0x1

    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    invoke-virtual {v1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    check-cast v1, Ljava/lang/Number;

    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8O(I)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OoO0o0()Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-eqz v1, :cond_2

    .line 61
    .line 62
    iget-boolean v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo80:Z

    .line 63
    .line 64
    if-eqz v1, :cond_1

    .line 65
    .line 66
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 67
    .line 68
    const/16 v2, 0x21

    .line 69
    .line 70
    if-ge v1, v2, :cond_2

    .line 71
    .line 72
    :cond_1
    iput-boolean v3, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo80:Z

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O00OoO〇()V

    .line 75
    .line 76
    .line 77
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇oOO80o()V

    .line 78
    .line 79
    .line 80
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0OOo〇0()V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOO〇0o8〇()I

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-ne v1, v3, :cond_3

    .line 92
    .line 93
    const-string v1, "addOneCloudGift() isSendOneCloudGift() = 1"

    .line 94
    .line 95
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    invoke-static {}, Lcom/intsig/camscanner/guide/markguide/GpGuideMarkControl;->O8()V

    .line 99
    .line 100
    .line 101
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog;->O8o08O8O:Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;

    .line 102
    .line 103
    const-string v1, "main"

    .line 104
    .line 105
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gallery/ImportSourceSelectDialog$Companion;->OO0o〇〇(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    if-eqz v0, :cond_4

    .line 113
    .line 114
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->Oo0o08o0o()V

    .line 115
    .line 116
    .line 117
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇oO08〇o0()Z

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-nez v0, :cond_5

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0〇8〇()Z

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    if-eqz v0, :cond_6

    .line 128
    .line 129
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/experiment/GalleryCacheExp;->〇o00〇〇Oo()Z

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    if-eqz v0, :cond_6

    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 136
    .line 137
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onResume$1;

    .line 138
    .line 139
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$onResume$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 140
    .line 141
    .line 142
    invoke-static {v0, v3, v1}, Lcom/intsig/camscanner/util/MainMenuTipsChecker;->〇8o8o〇(Landroid/app/Activity;ILcom/intsig/camscanner/util/MainMenuTipsChecker$MainTipsListener;)V

    .line 143
    .line 144
    .line 145
    :cond_6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;->oo88o8O()V

    .line 150
    .line 151
    .line 152
    sget-object v0, Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayManager;->〇080:Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayManager$Companion;

    .line 153
    .line 154
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayManager$Companion;->Oo08()V

    .line 155
    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 158
    .line 159
    if-eqz v0, :cond_7

    .line 160
    .line 161
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->OO0o〇〇〇〇0()V

    .line 162
    .line 163
    .line 164
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 165
    .line 166
    .line 167
    move-result-wide v0

    .line 168
    iget-wide v2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇o〇:J

    .line 169
    .line 170
    sub-long/2addr v0, v2

    .line 171
    new-instance v2, Ljava/lang/StringBuilder;

    .line 172
    .line 173
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 174
    .line 175
    .line 176
    const-string v3, "MainFragment from onAttach to onResume finish =="

    .line 177
    .line 178
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 182
    .line 183
    .line 184
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    const-string v1, "STARTDISPLAY"

    .line 189
    .line 190
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-static {}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->OO0o〇〇〇〇0()Ljava/lang/Long;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    if-eqz v0, :cond_8

    .line 198
    .line 199
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 200
    .line 201
    .line 202
    move-result-wide v0

    .line 203
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 204
    .line 205
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 206
    .line 207
    .line 208
    move-result-object v3

    .line 209
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇O〇〇O8:Landroid/widget/RelativeLayout;

    .line 210
    .line 211
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 212
    .line 213
    .line 214
    move-result-object v0

    .line 215
    invoke-static {v2, v3, v0}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->o800o8O(Landroidx/fragment/app/FragmentActivity;Landroid/view/View;Ljava/lang/Long;)V

    .line 216
    .line 217
    .line 218
    :cond_8
    sget-object v0, Lcom/intsig/camscanner/tools/FontTools;->〇080:Lcom/intsig/camscanner/tools/FontTools;

    .line 219
    .line 220
    sget-object v1, Lcom/intsig/camscanner/tools/FontTools$FontEnum;->MUYAO:Lcom/intsig/camscanner/tools/FontTools$FontEnum;

    .line 221
    .line 222
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/FontTools;->〇080(Lcom/intsig/camscanner/tools/FontTools$FontEnum;)V

    .line 223
    .line 224
    .line 225
    return-void
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "outState"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    const-string v0, "restore_camera_img_path"

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 19
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v2, "onSaveInstanceState mTmpPhotoFilePath: "

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onStart"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇O800oo()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActViewModel;->〇8o〇〇8080(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇88()V

    .line 23
    .line 24
    .line 25
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->Oo80:Z

    .line 26
    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OoO0o0()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 36
    .line 37
    const/16 v1, 0x21

    .line 38
    .line 39
    if-lt v0, v1, :cond_0

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O00OoO〇()V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final onSyncResult(Lcom/intsig/camscanner/eventbus/SyncEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "CSMain"

    .line 2
    .line 3
    const-string v1, "synchron_success"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    if-nez p1, :cond_1

    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/SyncEvent;->〇080()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    const/4 v1, 0x0

    .line 27
    if-eqz v0, :cond_3

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO〇00〇0O:Landroid/widget/Toast;

    .line 30
    .line 31
    if-eqz p1, :cond_2

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/widget/Toast;->cancel()V

    .line 34
    .line 35
    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO〇00〇0O:Landroid/widget/Toast;

    .line 37
    .line 38
    :cond_2
    return-void

    .line 39
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/SyncEvent;->〇o00〇〇Oo()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-nez p1, :cond_4

    .line 44
    .line 45
    return-void

    .line 46
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 51
    .line 52
    invoke-virtual {p1}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    const/4 v0, 0x1

    .line 57
    if-eq p1, v0, :cond_5

    .line 58
    .line 59
    return-void

    .line 60
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_6

    .line 69
    .line 70
    return-void

    .line 71
    :cond_6
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO〇00〇0O:Landroid/widget/Toast;

    .line 72
    .line 73
    const/4 v0, 0x0

    .line 74
    const v2, 0x7f130649

    .line 75
    .line 76
    .line 77
    if-eqz p1, :cond_7

    .line 78
    .line 79
    invoke-virtual {p1, v2}, Landroid/widget/Toast;->setText(I)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, v0}, Landroid/widget/Toast;->setDuration(I)V

    .line 83
    .line 84
    .line 85
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 86
    .line 87
    :cond_7
    if-nez v1, :cond_8

    .line 88
    .line 89
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO〇00〇0O:Landroid/widget/Toast;

    .line 102
    .line 103
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO〇00〇0O:Landroid/widget/Toast;

    .line 104
    .line 105
    if-eqz p1, :cond_9

    .line 106
    .line 107
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :catch_0
    move-exception p1

    .line 112
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 113
    .line 114
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    :cond_9
    :goto_0
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final onSystemMsgReceived(Lcom/intsig/camscanner/mainmenu/mepage/entity/SystemMsgEvent;)V
    .locals 2
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onSystemMsgReceived"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "restore_camera_img_path"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇08〇o0O:Ljava/lang/String;

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 17
    .line 18
    new-instance v1, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v2, "onViewStateRestored mTmpPhotoFilePath: "

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onVipIconShake(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$VipIconShaker;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$VipIconShaker;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "shaker"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇〇O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "mainBtmBarController"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final ooo008()V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->MAIN_NAV_ICON:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇O00(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "mBinding.ivQrScan"

    .line 8
    .line 9
    const-string v2, "mBinding.ivIntegralReward"

    .line 10
    .line 11
    const-string v3, "mBinding.ivIconOpe"

    .line 12
    .line 13
    const-string v4, "mBinding.ivCloudReward"

    .line 14
    .line 15
    const/4 v5, 0x1

    .line 16
    const/4 v6, 0x0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v7

    .line 23
    iget-object v7, v7, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 24
    .line 25
    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {v7, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 36
    .line 37
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-static {v3, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 48
    .line 49
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {v3, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 60
    .line 61
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-static {v2, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇ooO〇000(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_0

    .line 71
    .line 72
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->right_top_point_enter_switch:I

    .line 77
    .line 78
    if-ne v0, v5, :cond_1

    .line 79
    .line 80
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 81
    .line 82
    .line 83
    move-result v0

    .line 84
    if-nez v0, :cond_1

    .line 85
    .line 86
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o0ooO()Ljava/lang/Boolean;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    if-nez v0, :cond_1

    .line 95
    .line 96
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 97
    .line 98
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇〇888()Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-nez v0, :cond_1

    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 109
    .line 110
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    invoke-static {v0, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 121
    .line 122
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 133
    .line 134
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 145
    .line 146
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 157
    .line 158
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 159
    .line 160
    .line 161
    move-result-object v1

    .line 162
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0〇(Landroid/content/Context;)Z

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    if-eqz v1, :cond_3

    .line 167
    .line 168
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 169
    .line 170
    .line 171
    move-result-object v1

    .line 172
    invoke-static {v1}, Lcom/bumptech/glide/Glide;->〇O888o0o(Landroidx/fragment/app/Fragment;)Lcom/bumptech/glide/RequestManager;

    .line 173
    .line 174
    .line 175
    move-result-object v1

    .line 176
    const v2, 0x7f080467

    .line 177
    .line 178
    .line 179
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 180
    .line 181
    .line 182
    move-result-object v2

    .line 183
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/RequestManager;->Oooo8o0〇(Ljava/lang/Integer;)Lcom/bumptech/glide/RequestBuilder;

    .line 184
    .line 185
    .line 186
    move-result-object v1

    .line 187
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 188
    .line 189
    .line 190
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    const-string v2, "viewLifecycleOwner"

    .line 195
    .line 196
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    invoke-static {v1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 200
    .line 201
    .line 202
    move-result-object v1

    .line 203
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initToolbar$1$1;

    .line 204
    .line 205
    const/4 v3, 0x0

    .line 206
    invoke-direct {v2, v0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initToolbar$1$1;-><init>(Landroidx/appcompat/widget/AppCompatImageView;Lkotlin/coroutines/Continuation;)V

    .line 207
    .line 208
    .line 209
    invoke-virtual {v1, v2}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 210
    .line 211
    .line 212
    goto/16 :goto_0

    .line 213
    .line 214
    :cond_1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 215
    .line 216
    .line 217
    move-result v0

    .line 218
    if-eqz v0, :cond_2

    .line 219
    .line 220
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o088〇〇()Z

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    if-eqz v0, :cond_2

    .line 225
    .line 226
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 231
    .line 232
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    invoke-static {v0, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 243
    .line 244
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    .line 246
    .line 247
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 248
    .line 249
    .line 250
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 251
    .line 252
    .line 253
    move-result-object v0

    .line 254
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 255
    .line 256
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    .line 258
    .line 259
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 260
    .line 261
    .line 262
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 263
    .line 264
    .line 265
    move-result-object v0

    .line 266
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 267
    .line 268
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 272
    .line 273
    .line 274
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 275
    .line 276
    .line 277
    move-result-object v0

    .line 278
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 279
    .line 280
    new-instance v1, Lo〇00O/〇oo〇;

    .line 281
    .line 282
    invoke-direct {v1, p0}, Lo〇00O/〇oo〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 286
    .line 287
    .line 288
    goto :goto_0

    .line 289
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0〇〇00〇o()Lcom/intsig/camscanner/attention/RewardAPI;

    .line 290
    .line 291
    .line 292
    move-result-object v0

    .line 293
    if-eqz v0, :cond_3

    .line 294
    .line 295
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 296
    .line 297
    .line 298
    move-result-object v0

    .line 299
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o08〇808(Landroid/content/Context;)Z

    .line 300
    .line 301
    .line 302
    move-result v0

    .line 303
    if-nez v0, :cond_3

    .line 304
    .line 305
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 306
    .line 307
    .line 308
    move-result-object v0

    .line 309
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 310
    .line 311
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 312
    .line 313
    .line 314
    invoke-static {v0, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 318
    .line 319
    .line 320
    move-result-object v0

    .line 321
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o〇00O:Lcom/intsig/view/ImageViewDot;

    .line 322
    .line 323
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    .line 325
    .line 326
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 327
    .line 328
    .line 329
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 330
    .line 331
    .line 332
    move-result-object v0

    .line 333
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 334
    .line 335
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    .line 337
    .line 338
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 339
    .line 340
    .line 341
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 342
    .line 343
    .line 344
    move-result-object v0

    .line 345
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 346
    .line 347
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 348
    .line 349
    .line 350
    invoke-static {v0, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 351
    .line 352
    .line 353
    const-string v0, "CSHome"

    .line 354
    .line 355
    const-string v1, "cloud_icon_show"

    .line 356
    .line 357
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    .line 359
    .line 360
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;->〇080:Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;

    .line 361
    .line 362
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/manager/IdShareActivityManager;->〇〇888()Z

    .line 363
    .line 364
    .line 365
    move-result v0

    .line 366
    if-eqz v0, :cond_4

    .line 367
    .line 368
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 369
    .line 370
    .line 371
    move-result-object v0

    .line 372
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 373
    .line 374
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 375
    .line 376
    .line 377
    const/16 v1, 0x8

    .line 378
    .line 379
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 380
    .line 381
    .line 382
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 383
    .line 384
    .line 385
    move-result-object v0

    .line 386
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O8o08O8O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 387
    .line 388
    const-string v1, "mBinding.ivIdShareActivity"

    .line 389
    .line 390
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 394
    .line 395
    .line 396
    new-array v0, v5, [Landroid/view/View;

    .line 397
    .line 398
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 399
    .line 400
    .line 401
    move-result-object v1

    .line 402
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O8o08O8O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 403
    .line 404
    aput-object v1, v0, v6

    .line 405
    .line 406
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 407
    .line 408
    .line 409
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 410
    .line 411
    .line 412
    move-result-object v0

    .line 413
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O8o08O8O:Lcom/airbnb/lottie/LottieAnimationView;

    .line 414
    .line 415
    new-instance v1, Lo〇00O/o〇O8〇〇o;

    .line 416
    .line 417
    invoke-direct {v1, p0}, Lo〇00O/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 418
    .line 419
    .line 420
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 421
    .line 422
    .line 423
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇8o0OOOo()V

    .line 424
    .line 425
    .line 426
    return-void
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public final oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.mainactivity.MainActivity"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final ooooo0O()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇80〇808〇O()Lcom/intsig/mvp/fragment/BaseChangeFragment;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 6
    .line 7
    const-string v2, "CSHome"

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    return-object v2

    .line 12
    :cond_0
    instance-of v0, v0, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    const-string v0, "CSMain"

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_1
    return-object v2
    .line 20
    .line 21
.end method

.method public final oo〇O0o〇()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇80〇808〇O()Z

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    :cond_0
    return v2
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇O80o8OO(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "handleDirShare duuid = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, " ,sid = "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    const/4 v2, 0x1

    .line 33
    if-eqz p1, :cond_1

    .line 34
    .line 35
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    if-nez v3, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v3, 0x0

    .line 43
    goto :goto_1

    .line 44
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 45
    :goto_1
    if-nez v3, :cond_6

    .line 46
    .line 47
    if-eqz p2, :cond_2

    .line 48
    .line 49
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    if-nez v3, :cond_3

    .line 54
    .line 55
    :cond_2
    const/4 v1, 0x1

    .line 56
    :cond_3
    if-eqz v1, :cond_4

    .line 57
    .line 58
    goto :goto_2

    .line 59
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-nez v1, :cond_5

    .line 68
    .line 69
    const-string v1, "handleDirShare, not login"

    .line 70
    .line 71
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇00O0:Ljava/lang/String;

    .line 75
    .line 76
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇08oOOO0:Ljava/lang/String;

    .line 77
    .line 78
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 79
    .line 80
    iget-object p2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 81
    .line 82
    invoke-direct {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    .line 84
    .line 85
    const p2, 0x7f131d10

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    const p2, 0x7f13038c

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    new-instance p2, Lo〇00O/〇oOO8O8;

    .line 100
    .line 101
    invoke-direct {p2, p0}, Lo〇00O/〇oOO8O8;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 102
    .line 103
    .line 104
    const v0, 0x7f130074

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1, v0, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 116
    .line 117
    .line 118
    return-void

    .line 119
    :cond_5
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 120
    .line 121
    const-string v1, "mActivity"

    .line 122
    .line 123
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$joinShareDir$2;

    .line 127
    .line 128
    invoke-direct {v1, p1, p0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$joinShareDir$2;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const-string p1, "other"

    .line 132
    .line 133
    invoke-static {v0, v1, p1, p1}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇O〇(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    :cond_6
    :goto_2
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final o〇Oo(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 5
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->oO80(I)Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_3

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0〇8〇()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const v3, 0x7f131755

    .line 24
    .line 25
    .line 26
    if-eqz v2, :cond_2

    .line 27
    .line 28
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->computeVerticalScrollOffset()I

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    iget v4, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oO〇8O8oOo:I

    .line 33
    .line 34
    if-lt v2, v4, :cond_0

    .line 35
    .line 36
    const v2, 0x7f08049a

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setImage(I)V

    .line 40
    .line 41
    .line 42
    const v2, 0x7f131b91

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setText(I)V

    .line 46
    .line 47
    .line 48
    new-instance v2, Lo〇00O/〇00;

    .line 49
    .line 50
    invoke-direct {v2, p0, p1, v0}, Lo〇00O/〇00;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    .line 55
    .line 56
    iget p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇0o〇〇0:I

    .line 57
    .line 58
    if-nez p1, :cond_1

    .line 59
    .line 60
    iput v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇0o〇〇0:I

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_0
    const p1, 0x7f080cb8

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setImage(I)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setText(I)V

    .line 70
    .line 71
    .line 72
    new-instance p1, Lo〇00O/O〇8O8〇008;

    .line 73
    .line 74
    invoke-direct {p1, p0, v0}, Lo〇00O/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    .line 79
    .line 80
    const/4 p1, 0x0

    .line 81
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇0o〇〇0:I

    .line 82
    .line 83
    :cond_1
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇0o〇〇0:I

    .line 84
    .line 85
    if-ne p1, v1, :cond_3

    .line 86
    .line 87
    const-string p1, "CSMain"

    .line 88
    .line 89
    const-string v0, "back_to_top_show"

    .line 90
    .line 91
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const/4 p1, 0x2

    .line 95
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇〇〇0o〇〇0:I

    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_2
    const p1, 0x7f080cb9

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setImage(I)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;->setText(I)V

    .line 105
    .line 106
    .line 107
    new-instance p1, Lo〇00O/O8ooOoo〇;

    .line 108
    .line 109
    invoke-direct {p1, p0, v0}, Lo〇00O/O8ooOoo〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabView;)V

    .line 110
    .line 111
    .line 112
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    .line 114
    .line 115
    :cond_3
    :goto_1
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final o〇OoO0(ZLjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "tip"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 7
    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v2, "checkShowGiftMarketingTip\tisShow="

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->O0()Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-nez p1, :cond_0

    .line 40
    .line 41
    const/4 p1, 0x1

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const/4 p1, 0x0

    .line 44
    :goto_0
    if-ne p1, v0, :cond_1

    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8〇〇()Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;->O8(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_1
    if-nez p1, :cond_2

    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8〇〇()Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeGiftTipControl;->〇o〇()V

    .line 61
    .line 62
    .line 63
    :cond_2
    :goto_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final o〇o0oOO8()J
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x1

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const-wide/16 v0, -0x2

    .line 20
    .line 21
    :goto_0
    return-wide v0
.end method

.method public final o〇oO08〇o0()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/CommonBottomTabLayout;->getCurrentPosition()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇8〇〇(Z)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇O〇〇O8:Landroid/widget/RelativeLayout;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;->getMFabButton()Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/4 v2, 0x0

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-nez v1, :cond_0

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    :cond_0
    if-eqz v2, :cond_1

    .line 28
    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOo00()Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string v1, "rootView"

    .line 36
    .line 37
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->OO0o〇〇〇〇0(Landroid/view/View;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->OOo00()Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇8o8o〇()V

    .line 49
    .line 50
    .line 51
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02b8

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final remoteOpenFolder(Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;)V
    .locals 4
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/targetdir/TargetDirActivity;->O0O:Lcom/intsig/camscanner/targetdir/TargetDirActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/targetdir/TargetDirActivity$Companion;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 10
    .line 11
    const-string v0, "remoteOpenFolder: but TargetDirActivity holding!"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    if-eqz p1, :cond_3

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;->〇080()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;->〇080()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/DirDao;->oO80(Landroid/content/Context;Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;->〇080()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    const-string v2, "remoteOpenFolder ignore teamDirSyncId = "

    .line 58
    .line 59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-void

    .line 73
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;->〇080()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    new-instance v2, Ljava/lang/StringBuilder;

    .line 80
    .line 81
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .line 83
    .line 84
    const-string v3, "remoteOpenFolder openFolder = "

    .line 85
    .line 86
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;->〇080()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8ooOO(Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    return-void

    .line 107
    :cond_3
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 108
    .line 109
    const-string v0, "remoteOpenFolder event isEmpty"

    .line 110
    .line 111
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇0〇(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/datastruct/DocItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "docItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 9
    .line 10
    .line 11
    move-result-wide v1

    .line 12
    new-instance p1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v3, "selectDoc id == "

    .line 18
    .line 19
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo88()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;->〇oOO8O8()V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇00o〇O8()Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8()Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    if-eqz p1, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;->oo0O〇0〇〇〇()Ljava/util/Set;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    invoke-interface {v1}, Ljava/util/Set;->size()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    goto :goto_0

    .line 60
    :cond_0
    const/4 v1, 0x0

    .line 61
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainTopBarController;->O8(I)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0O0O〇〇〇0(Lcom/intsig/camscanner/mainmenu/adapter/MainDocAdapter;)V

    .line 65
    .line 66
    .line 67
    return-void
.end method

.method public final 〇0〇8o〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooooo0O()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇〇888(Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o:Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;

    .line 16
    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/viewmanager/VipIconManager;->〇0〇O0088o(I)V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇80〇(Landroid/graphics/Bitmap;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;)V
    .locals 8

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_5

    .line 3
    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;->getPosition()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    :cond_0
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    if-nez v3, :cond_1

    .line 15
    .line 16
    return-void

    .line 17
    :cond_1
    const-string v0, "CsAdUtil.getOperationPos\u2026Info?.position) ?: return"

    .line 18
    .line 19
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇oo8O80()Lcom/intsig/view/AnimatedImageView;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    new-instance v1, Lo〇00O/〇〇808〇;

    .line 27
    .line 28
    invoke-direct {v1, p0}, Lo〇00O/〇〇808〇;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 29
    .line 30
    .line 31
    const-wide/16 v4, 0x32

    .line 32
    .line 33
    invoke-virtual {v0, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 34
    .line 35
    .line 36
    sget-object v1, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->DOC_LIST_ICON:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 37
    .line 38
    if-ne v3, v1, :cond_2

    .line 39
    .line 40
    new-instance v1, Lo〇00O/〇O00;

    .line 41
    .line 42
    invoke-direct {v1, p0, v3, v0}, Lo〇00O/〇O00;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/view/AnimatedImageView;)V

    .line 43
    .line 44
    .line 45
    const-wide/16 v4, 0x64

    .line 46
    .line 47
    invoke-virtual {v0, v1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 48
    .line 49
    .line 50
    :cond_2
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0888(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Landroid/graphics/RectF;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lcom/intsig/view/AnimatedImageView;->setTargetDrawingRectF(Landroid/graphics/RectF;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/view/AnimatedImageView;->getTargetDrawingRectF()Landroid/graphics/RectF;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    if-nez v1, :cond_3

    .line 62
    .line 63
    return-void

    .line 64
    :cond_3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/view/AnimatedImageView;->getTargetDrawingRectF()Landroid/graphics/RectF;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    new-instance v4, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v5, "begin show animal---- relevance-animation "

    .line 76
    .line 77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    const/4 v1, 0x1

    .line 91
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0, p1}, Lcom/intsig/view/SafeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    .line 96
    .line 97
    sget-object v2, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$WhenMappings;->〇080:[I

    .line 98
    .line 99
    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    aget v2, v2, v4

    .line 104
    .line 105
    if-ne v2, v1, :cond_4

    .line 106
    .line 107
    const/4 v1, 0x2

    .line 108
    :cond_4
    invoke-virtual {v0, v1}, Lcom/intsig/view/AnimatedImageView;->setAnimalType(I)V

    .line 109
    .line 110
    .line 111
    const-wide/16 v1, 0x1f4

    .line 112
    .line 113
    invoke-virtual {v0, v1, v2}, Lcom/intsig/view/AnimatedImageView;->setAnimDuration(J)V

    .line 114
    .line 115
    .line 116
    new-instance v7, Lo〇00O/〇〇8O0〇8;

    .line 117
    .line 118
    move-object v1, v7

    .line 119
    move-object v2, v0

    .line 120
    move-object v4, p2

    .line 121
    move-object v5, p0

    .line 122
    move-object v6, p1

    .line 123
    invoke-direct/range {v1 .. v6}, Lo〇00O/〇〇8O0〇8;-><init>(Lcom/intsig/view/AnimatedImageView;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/advertisement/adapters/sources/cs/PositionRelevance;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Landroid/graphics/Bitmap;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v7}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 127
    .line 128
    .line 129
    goto :goto_0

    .line 130
    :cond_5
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 131
    .line 132
    const-string p2, "relevance-animation no bitmapForScale"

    .line 133
    .line 134
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8o0o0(Landroid/graphics/Bitmap;)V

    .line 138
    .line 139
    .line 140
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 141
    .line 142
    :goto_0
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇80〇808〇O()Lcom/intsig/mvp/fragment/BaseChangeFragment;
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroidx/viewpager2/widget/ViewPager2;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇00O:Landroid/util/SparseArray;

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string v1, "null cannot be cast to non-null type com.intsig.mvp.fragment.BaseChangeFragment"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    check-cast v0, Lcom/intsig/mvp/fragment/BaseChangeFragment;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .line 24
    return-object v0

    .line 25
    :catch_0
    move-exception v0

    .line 26
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    return-object v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇88(Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O80(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8o80O(Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/MainBtmBarController;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8oo0oO0()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇0O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8o08O8O:Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8ooOO(Ljava/lang/String;)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->〇0〇0(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception p1

    .line 10
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 11
    .line 12
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8〇0O〇()Landroid/graphics/Rect;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->O0O:Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutScreenshotImportBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o8o0(Landroid/view/View;)Landroid/graphics/Rect;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8〇o〇OoO8()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo〇〇oO()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/adapter/BaseViewPager2Adapter;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->o8oOOo:Landroidx/viewpager2/widget/ViewPager2;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setUserInputEnabled(Z)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseViewPager2Adapter;

    .line 23
    .line 24
    if-nez v1, :cond_0

    .line 25
    .line 26
    const-string v1, "mPagerAdapter"

    .line 27
    .line 28
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/4 v1, 0x0

    .line 32
    :cond_0
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 33
    .line 34
    .line 35
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initViewPager$1$1;

    .line 36
    .line 37
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$initViewPager$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroidx/viewpager2/widget/ViewPager2;->registerOnPageChangeCallback(Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇8〇〇8〇8()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O088O()Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->oo〇O〇o〇8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    return v0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇88(Ljava/util/ArrayList;)V
    .locals 3
    .param p1    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "dirTypes"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v1, "viewLifecycleOwner"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createSeniorFolders$1;

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    invoke-direct {v1, p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment$createSeniorFolders$1;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZ)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isResumed()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->〇〇888()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 29
    .line 30
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o0ooO(Landroidx/fragment/app/Fragment;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇o0oOO8()J

    .line 42
    .line 43
    .line 44
    move-result-wide v2

    .line 45
    invoke-virtual {p2, v2, v3}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-virtual {p1, p4}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇0〇O0088o(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    invoke-virtual {p1, p5}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    const/4 p2, 0x1

    .line 70
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇8o8o〇(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    const/16 p2, 0x66

    .line 75
    .line 76
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O〇8O8〇008(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oo8()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->oo〇(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p1, p6}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇〇〇0(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    invoke-virtual {p1, p7}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O00(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 93
    .line 94
    .line 95
    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method public final 〇〇08〇0oo0(Ljava/lang/Integer;)Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇0O:Landroid/widget/ImageView;

    .line 6
    .line 7
    const-string v1, "mBinding.ivOpeMore"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    if-nez p1, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-nez v3, :cond_1

    .line 25
    .line 26
    return v0

    .line 27
    :cond_1
    :goto_0
    if-nez p1, :cond_2

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-ne p1, v0, :cond_5

    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇O8〇〇o()Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_3

    .line 41
    .line 42
    return v2

    .line 43
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;

    .line 44
    .line 45
    if-nez p1, :cond_4

    .line 46
    .line 47
    return v2

    .line 48
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocHostFragment;->o00〇88〇08()Lcom/intsig/camscanner/mainmenu/FolderStackManager;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/FolderStackManager;->OO0o〇〇〇〇0()Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8oo8888()Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentCsMainBinding;->〇0O:Landroid/widget/ImageView;

    .line 61
    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 66
    .line 67
    .line 68
    return p1

    .line 69
    :cond_5
    :goto_1
    return v2
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇〇O(Landroid/content/Intent;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o0OoOOo0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onNewIntent"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8O(Landroid/content/Intent;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o〇o8〇〇O(Landroid/content/Intent;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇00〇〇〇o〇8(Landroid/content/Intent;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->o88oo〇O()Lcom/intsig/camscanner/util/CsLinkParser;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/util/CsLinkParser;->〇〇808〇(Landroid/content/Intent;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const-string v0, "MainMenuActivity"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/comm/util/AppLaunchSourceStatistic;->〇o00〇〇Oo(Landroid/content/Intent;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o88〇O(Landroid/content/Intent;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇00o08(Landroid/content/Intent;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O008oO0(Landroid/content/Intent;)V

    .line 36
    .line 37
    .line 38
    sget-object v0, Lcom/intsig/camscanner/purchase/push/PurchasePushManager;->〇080:Lcom/intsig/camscanner/purchase/push/PurchasePushManager;

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/purchase/push/PurchasePushManager;->〇o00〇〇Oo(Landroid/content/Intent;Landroid/app/Activity;)V

    .line 45
    .line 46
    .line 47
    sget-object v0, Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;->〇080:Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;

    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/purchase/push/MainPushShortMsgControl;->o〇0(Landroid/content/Intent;Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;)V

    .line 54
    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇8〇〇8o(Landroid/content/Intent;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

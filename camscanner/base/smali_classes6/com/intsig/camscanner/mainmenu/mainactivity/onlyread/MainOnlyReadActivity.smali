.class public final Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "MainOnlyReadActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic O88O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final oOO〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment;

.field private o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment;

.field private ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;

.field private final 〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O88O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "MainOnlyReadActivity::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const v0, 0x7f0a0671

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {p0, v0, p1, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->O〇8〇008(ILandroidx/fragment/app/Fragment;Z)V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final O〇080〇o0()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇o0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity$Companion;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O88O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const v1, 0x7f0a1642

    .line 21
    .line 22
    .line 23
    if-ne v0, v1, :cond_2

    .line 24
    .line 25
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 26
    .line 27
    const-string v0, "first"

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;

    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    if-eqz p1, :cond_a

    .line 42
    .line 43
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 44
    .line 45
    if-eqz p1, :cond_a

    .line 46
    .line 47
    const v0, 0x7f080a5e

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_5

    .line 54
    .line 55
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    const v1, 0x7f0a1644

    .line 63
    .line 64
    .line 65
    if-ne v0, v1, :cond_4

    .line 66
    .line 67
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 68
    .line 69
    const-string v0, "second"

    .line 70
    .line 71
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇〇08O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment;

    .line 75
    .line 76
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V

    .line 77
    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    if-eqz p1, :cond_a

    .line 84
    .line 85
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 86
    .line 87
    if-eqz p1, :cond_a

    .line 88
    .line 89
    const v0, 0x7f080a5f

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 93
    .line 94
    .line 95
    goto/16 :goto_5

    .line 96
    .line 97
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    const v1, 0x7f0a1645

    .line 105
    .line 106
    .line 107
    if-ne v0, v1, :cond_6

    .line 108
    .line 109
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 110
    .line 111
    const-string v0, "third"

    .line 112
    .line 113
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment;

    .line 117
    .line 118
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    if-eqz p1, :cond_a

    .line 126
    .line 127
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 128
    .line 129
    if-eqz p1, :cond_a

    .line 130
    .line 131
    const v0, 0x7f080a60

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 135
    .line 136
    .line 137
    goto :goto_5

    .line 138
    :cond_6
    :goto_3
    if-nez p1, :cond_7

    .line 139
    .line 140
    goto :goto_4

    .line 141
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    const v1, 0x7f0a1643

    .line 146
    .line 147
    .line 148
    if-ne v0, v1, :cond_8

    .line 149
    .line 150
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 151
    .line 152
    const-string v0, "four"

    .line 153
    .line 154
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment;

    .line 158
    .line 159
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V

    .line 160
    .line 161
    .line 162
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 163
    .line 164
    .line 165
    move-result-object p1

    .line 166
    if-eqz p1, :cond_a

    .line 167
    .line 168
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 169
    .line 170
    if-eqz p1, :cond_a

    .line 171
    .line 172
    const v0, 0x7f080a61

    .line 173
    .line 174
    .line 175
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 176
    .line 177
    .line 178
    goto :goto_5

    .line 179
    :cond_8
    :goto_4
    if-nez p1, :cond_9

    .line 180
    .line 181
    goto :goto_5

    .line 182
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 183
    .line 184
    .line 185
    move-result p1

    .line 186
    const v0, 0x7f0a1641

    .line 187
    .line 188
    .line 189
    if-ne p1, v0, :cond_a

    .line 190
    .line 191
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->oOO〇〇:Ljava/lang/String;

    .line 192
    .line 193
    const-string v0, "capture"

    .line 194
    .line 195
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .line 197
    .line 198
    new-instance p1, Landroid/content/Intent;

    .line 199
    .line 200
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 201
    .line 202
    const-class v1, Lcom/intsig/camscanner/ModelCaptureActivity;

    .line 203
    .line 204
    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 205
    .line 206
    .line 207
    const/16 v0, 0x3e9

    .line 208
    .line 209
    invoke-virtual {p0, p1, v0}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 210
    .line 211
    .line 212
    :cond_a
    :goto_5
    return-void
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    const p1, 0x7f0602f5

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-static {p0, v0, v0, p1}, Lcom/intsig/utils/StatusBarUtil;->〇o00〇〇Oo(Landroid/app/Activity;ZZI)V

    .line 10
    .line 11
    .line 12
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment$Companion;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;

    .line 19
    .line 20
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment$Companion;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->〇〇08O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadSecondFragment;

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment$Companion;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadThirdFragment;

    .line 35
    .line 36
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment;->o0:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment$Companion;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFourFragment;

    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->ooo0〇〇O:Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/OnlyReadFirstFragment;

    .line 45
    .line 46
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O0〇(Lcom/intsig/mvp/fragment/BaseChangeFragment;)V

    .line 47
    .line 48
    .line 49
    const/4 p1, 0x5

    .line 50
    new-array p1, p1, [Landroid/view/View;

    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const/4 v2, 0x0

    .line 57
    if-eqz v1, :cond_0

    .line 58
    .line 59
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->o〇00O:Landroid/widget/TextView;

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    move-object v1, v2

    .line 63
    :goto_0
    const/4 v3, 0x0

    .line 64
    aput-object v1, p1, v3

    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    if-eqz v1, :cond_1

    .line 71
    .line 72
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_1
    move-object v1, v2

    .line 76
    :goto_1
    aput-object v1, p1, v0

    .line 77
    .line 78
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    if-eqz v0, :cond_2

    .line 83
    .line 84
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->〇0O:Landroid/widget/TextView;

    .line 85
    .line 86
    goto :goto_2

    .line 87
    :cond_2
    move-object v0, v2

    .line 88
    :goto_2
    const/4 v1, 0x2

    .line 89
    aput-object v0, p1, v1

    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-eqz v0, :cond_3

    .line 96
    .line 97
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_3
    move-object v0, v2

    .line 101
    :goto_3
    const/4 v1, 0x3

    .line 102
    aput-object v0, p1, v1

    .line 103
    .line 104
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/onlyread/MainOnlyReadActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    if-eqz v0, :cond_4

    .line 109
    .line 110
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityMainOnlyReadBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 111
    .line 112
    :cond_4
    const/4 v0, 0x4

    .line 113
    aput-object v2, p1, v0

    .line 114
    .line 115
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O8〇8O0oO([Landroid/view/View;)V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/16 p3, 0x3e9

    .line 5
    .line 6
    if-ne p1, p3, :cond_2

    .line 7
    .line 8
    const/4 p1, -0x1

    .line 9
    if-eq p2, p1, :cond_1

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    if-eq p2, p1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->〇O888o0o(Landroid/content/Context;)Landroid/content/Intent;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-string p2, "PATTERN_EX_ONLY_BE_READ_DATA"

    .line 24
    .line 25
    invoke-virtual {p1, p2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 32
    .line 33
    .line 34
    :cond_2
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const/4 p1, 0x1

    .line 5
    return p1

    .line 6
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d0084

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

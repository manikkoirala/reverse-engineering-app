.class public final Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "SuperDirGuideDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;,
        Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;,
        Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;,
        Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic o〇00O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private OO:Lcom/intsig/callback/DialogDismissListener;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/utils/ClickLimit;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o〇00O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O0〇0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    const-string v0, "null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    const-string v0, "dialog.behavior"

    .line 13
    .line 14
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x3

    .line 18
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o00〇88〇08()Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    if-eqz v1, :cond_2

    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const-string v3, "growth_record"

    .line 17
    .line 18
    const-string v4, "it.getString(R.string.cs_618_timefolder_title)"

    .line 19
    .line 20
    const-string v8, "certificate"

    .line 21
    .line 22
    const-string v9, "it.getString(R.string.cs_618_idcardfolder_title)"

    .line 23
    .line 24
    const v11, 0x7f0807ed

    .line 25
    .line 26
    .line 27
    const/16 v12, 0x69

    .line 28
    .line 29
    const-string v13, "briefcase"

    .line 30
    .line 31
    const/16 v14, 0x66

    .line 32
    .line 33
    const v15, 0x7f0807d7

    .line 34
    .line 35
    .line 36
    const/4 v5, 0x1

    .line 37
    if-ne v2, v5, :cond_0

    .line 38
    .line 39
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 40
    .line 41
    invoke-direct {v2, v14}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 42
    .line 43
    .line 44
    const v14, 0x7f0807f3

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v14}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, v15}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 51
    .line 52
    .line 53
    const v6, 0x7f131272

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v6

    .line 60
    const-string v7, "it.getString(R.string.cs_628_apply_title)"

    .line 61
    .line 62
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    const v6, 0x7f131271

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v7

    .line 75
    const-string v10, "it.getString(R.string.cs_628_apply_content02)"

    .line 76
    .line 77
    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v2, v7}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2, v13}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    .line 91
    .line 92
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 93
    .line 94
    const/16 v7, 0x6a

    .line 95
    .line 96
    invoke-direct {v2, v7}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {v2, v14}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2, v15}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 103
    .line 104
    .line 105
    const v7, 0x7f131541

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v7

    .line 112
    const-string v13, "it.getString(R.string.cs_633_work_info)"

    .line 113
    .line 114
    invoke-static {v7, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v2, v7}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v6

    .line 124
    invoke-static {v6, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 131
    .line 132
    .line 133
    const-string v6, "work_file"

    .line 134
    .line 135
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    .line 140
    .line 141
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 142
    .line 143
    invoke-direct {v2, v12}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 144
    .line 145
    .line 146
    const v6, 0x7f0807ea

    .line 147
    .line 148
    .line 149
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v2, v11}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 153
    .line 154
    .line 155
    const v6, 0x7f131057

    .line 156
    .line 157
    .line 158
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 159
    .line 160
    .line 161
    move-result-object v6

    .line 162
    invoke-static {v6, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    const v6, 0x7f131284

    .line 169
    .line 170
    .line 171
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 172
    .line 173
    .line 174
    move-result-object v6

    .line 175
    const-string v7, "it.getString(R.string.cs\u20268_idcardfolder_content02)"

    .line 176
    .line 177
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 193
    .line 194
    const/16 v5, 0x65

    .line 195
    .line 196
    invoke-direct {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 197
    .line 198
    .line 199
    const v5, 0x7f0807f7

    .line 200
    .line 201
    .line 202
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 203
    .line 204
    .line 205
    const v5, 0x7f0807e3

    .line 206
    .line 207
    .line 208
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 209
    .line 210
    .line 211
    const v5, 0x7f131099

    .line 212
    .line 213
    .line 214
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v5

    .line 218
    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    const v4, 0x7f1312c7

    .line 225
    .line 226
    .line 227
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object v1

    .line 231
    const-string v4, "it.getString(R.string.cs_628_timefolder_content02)"

    .line 232
    .line 233
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    const/4 v1, 0x0

    .line 240
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 241
    .line 242
    .line 243
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    .line 248
    .line 249
    goto/16 :goto_0

    .line 250
    .line 251
    :cond_0
    if-nez v2, :cond_1

    .line 252
    .line 253
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 254
    .line 255
    invoke-direct {v2, v14}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 256
    .line 257
    .line 258
    const v6, 0x7f0807d8

    .line 259
    .line 260
    .line 261
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 262
    .line 263
    .line 264
    invoke-virtual {v2, v15}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 265
    .line 266
    .line 267
    const v6, 0x7f1310b0

    .line 268
    .line 269
    .line 270
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v6

    .line 274
    const-string v7, "it.getString(R.string.cs_618_workfolder_title)"

    .line 275
    .line 276
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    .line 278
    .line 279
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    const v6, 0x7f1310af

    .line 283
    .line 284
    .line 285
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v6

    .line 289
    const-string v7, "it.getString(R.string.cs_618_workfolder_content)"

    .line 290
    .line 291
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 295
    .line 296
    .line 297
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 298
    .line 299
    .line 300
    invoke-virtual {v2, v13}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 301
    .line 302
    .line 303
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    .line 305
    .line 306
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 307
    .line 308
    invoke-direct {v2, v12}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 309
    .line 310
    .line 311
    const v6, 0x7f0807ee

    .line 312
    .line 313
    .line 314
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {v2, v11}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 318
    .line 319
    .line 320
    const v6, 0x7f131057

    .line 321
    .line 322
    .line 323
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 324
    .line 325
    .line 326
    move-result-object v6

    .line 327
    invoke-static {v6, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 328
    .line 329
    .line 330
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 331
    .line 332
    .line 333
    const v6, 0x7f131056

    .line 334
    .line 335
    .line 336
    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 337
    .line 338
    .line 339
    move-result-object v6

    .line 340
    const-string v7, "it.getString(R.string.cs_618_idcardfolder_content)"

    .line 341
    .line 342
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    .line 344
    .line 345
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 346
    .line 347
    .line 348
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 349
    .line 350
    .line 351
    invoke-virtual {v2, v8}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 352
    .line 353
    .line 354
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    .line 356
    .line 357
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 358
    .line 359
    const/16 v5, 0x65

    .line 360
    .line 361
    invoke-direct {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 362
    .line 363
    .line 364
    const v5, 0x7f0807e4

    .line 365
    .line 366
    .line 367
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 368
    .line 369
    .line 370
    const v5, 0x7f0807e3

    .line 371
    .line 372
    .line 373
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 374
    .line 375
    .line 376
    const v5, 0x7f131099

    .line 377
    .line 378
    .line 379
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 380
    .line 381
    .line 382
    move-result-object v5

    .line 383
    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    .line 385
    .line 386
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 387
    .line 388
    .line 389
    const v4, 0x7f131098

    .line 390
    .line 391
    .line 392
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 393
    .line 394
    .line 395
    move-result-object v4

    .line 396
    const-string v5, "it.getString(R.string.cs_618_timefolder_content)"

    .line 397
    .line 398
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    .line 400
    .line 401
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 402
    .line 403
    .line 404
    const/4 v4, 0x0

    .line 405
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 406
    .line 407
    .line 408
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 409
    .line 410
    .line 411
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    .line 413
    .line 414
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 415
    .line 416
    const/16 v3, 0x68

    .line 417
    .line 418
    invoke-direct {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 419
    .line 420
    .line 421
    const v3, 0x7f0807e7

    .line 422
    .line 423
    .line 424
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 425
    .line 426
    .line 427
    const v3, 0x7f0807e6

    .line 428
    .line 429
    .line 430
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 431
    .line 432
    .line 433
    const v3, 0x7f131035

    .line 434
    .line 435
    .line 436
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 437
    .line 438
    .line 439
    move-result-object v3

    .line 440
    const-string v4, "it.getString(R.string.cs_618_familyfolder_title)"

    .line 441
    .line 442
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    .line 444
    .line 445
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 446
    .line 447
    .line 448
    const v3, 0x7f131034

    .line 449
    .line 450
    .line 451
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 452
    .line 453
    .line 454
    move-result-object v3

    .line 455
    const-string v4, "it.getString(R.string.cs_618_familyfolder_content)"

    .line 456
    .line 457
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    .line 459
    .line 460
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 461
    .line 462
    .line 463
    const/4 v3, 0x0

    .line 464
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 465
    .line 466
    .line 467
    const-string v3, "family_file"

    .line 468
    .line 469
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 470
    .line 471
    .line 472
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    .line 474
    .line 475
    new-instance v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 476
    .line 477
    const/16 v3, 0x67

    .line 478
    .line 479
    invoke-direct {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;-><init>(I)V

    .line 480
    .line 481
    .line 482
    const v3, 0x7f0807f1

    .line 483
    .line 484
    .line 485
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->OO0o〇〇〇〇0(I)V

    .line 486
    .line 487
    .line 488
    const v3, 0x7f0807f0

    .line 489
    .line 490
    .line 491
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇80〇808〇O(I)V

    .line 492
    .line 493
    .line 494
    const v3, 0x7f13105c

    .line 495
    .line 496
    .line 497
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 498
    .line 499
    .line 500
    move-result-object v3

    .line 501
    const-string v4, "it.getString(R.string.cs_618_ideafolder_title)"

    .line 502
    .line 503
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 504
    .line 505
    .line 506
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇8o8o〇(Ljava/lang/String;)V

    .line 507
    .line 508
    .line 509
    const v3, 0x7f13105b

    .line 510
    .line 511
    .line 512
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 513
    .line 514
    .line 515
    move-result-object v1

    .line 516
    const-string v3, "it.getString(R.string.cs_618_ideafolder_content)"

    .line 517
    .line 518
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 519
    .line 520
    .line 521
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->oO80(Ljava/lang/String;)V

    .line 522
    .line 523
    .line 524
    const/4 v1, 0x0

    .line 525
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 526
    .line 527
    .line 528
    const-string v1, "ideas"

    .line 529
    .line 530
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇O8o08O(Ljava/lang/String;)V

    .line 531
    .line 532
    .line 533
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    .line 535
    .line 536
    goto :goto_0

    .line 537
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 538
    .line 539
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 540
    .line 541
    .line 542
    throw v0

    .line 543
    :cond_2
    :goto_0
    return-object v0
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final o880(Ljava/util/List;I)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1
    check-cast p1, Ljava/lang/Iterable;

    .line 2
    .line 3
    new-instance v0, Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6
    .line 7
    .line 8
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    const/4 v2, 0x0

    .line 17
    if-eqz v1, :cond_2

    .line 18
    .line 19
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    move-object v3, v1

    .line 24
    check-cast v3, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇o00〇〇Oo()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    if-ne v3, p2, :cond_1

    .line 31
    .line 32
    const/4 v2, 0x1

    .line 33
    :cond_1
    if-eqz v2, :cond_0

    .line 34
    .line 35
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    .line 40
    .line 41
    const/16 p2, 0xa

    .line 42
    .line 43
    invoke-static {v0, p2}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 44
    .line 45
    .line 46
    move-result p2

    .line 47
    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    .line 49
    .line 50
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 55
    .line 56
    .line 57
    move-result v0

    .line 58
    const-string v1, "0"

    .line 59
    .line 60
    if-eqz v0, :cond_4

    .line 61
    .line 62
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    check-cast v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->o〇0()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-eqz v0, :cond_3

    .line 73
    .line 74
    const-string v1, "1"

    .line 75
    .line 76
    :cond_3
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 81
    .line 82
    .line 83
    move-result p2

    .line 84
    if-eqz p2, :cond_5

    .line 85
    .line 86
    goto :goto_2

    .line 87
    :cond_5
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    move-object v1, p1

    .line 92
    check-cast v1, Ljava/lang/String;

    .line 93
    .line 94
    :goto_2
    return-object v1
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic oOo〇08〇(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->O0〇0(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇088O(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final oooO888()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o〇00O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final o〇0〇o(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$adapter"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 12
    .line 13
    const-wide/16 v1, 0x12c

    .line 14
    .line 15
    invoke-virtual {v0, p2, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    if-nez p2, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->dismissAllowingStateLoss()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    check-cast p2, Ljava/lang/Iterable;

    .line 30
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object p2

    .line 40
    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_2

    .line 45
    .line 46
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    move-object v2, v1

    .line 51
    check-cast v2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->o〇0()Z

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-eqz v2, :cond_1

    .line 58
    .line 59
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    new-instance p2, Ljava/util/ArrayList;

    .line 64
    .line 65
    const/16 v1, 0xa

    .line 66
    .line 67
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 72
    .line 73
    .line 74
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    if-eqz v1, :cond_3

    .line 83
    .line 84
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    check-cast v1, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 89
    .line 90
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇o00〇〇Oo()I

    .line 91
    .line 92
    .line 93
    move-result v1

    .line 94
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇8〇80o(Ljava/util/List;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 110
    .line 111
    .line 112
    move-result-object p0

    .line 113
    instance-of p1, p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 114
    .line 115
    const/4 v0, 0x0

    .line 116
    if-eqz p1, :cond_4

    .line 117
    .line 118
    check-cast p0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_4
    move-object p0, v0

    .line 122
    :goto_2
    if-eqz p0, :cond_5

    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->〇oO88o()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    :cond_5
    if-eqz v0, :cond_6

    .line 129
    .line 130
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->〇o〇88(Ljava/util/ArrayList;)V

    .line 131
    .line 132
    .line 133
    :cond_6
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final 〇088O(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$view"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "$adapter"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "<anonymous parameter 0>"

    .line 17
    .line 18
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string p3, "<anonymous parameter 1>"

    .line 22
    .line 23
    invoke-static {p4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object p3, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 27
    .line 28
    const-wide/16 v0, 0x12c

    .line 29
    .line 30
    invoke-virtual {p3, p1, v0, v1}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_0

    .line 35
    .line 36
    const-string p0, "SuperDirGuideDialog"

    .line 37
    .line 38
    const-string p1, "click item too fast"

    .line 39
    .line 40
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_0
    invoke-virtual {p2, p5}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItem(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    check-cast p1, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->o〇0()Z

    .line 51
    .line 52
    .line 53
    move-result p3

    .line 54
    xor-int/lit8 p3, p3, 0x1

    .line 55
    .line 56
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇〇888(Z)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p2, p5}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->oooO888()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    if-eqz p1, :cond_1

    .line 67
    .line 68
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;->o〇00O:Landroid/widget/TextView;

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_1
    const/4 p1, 0x0

    .line 72
    :goto_0
    if-nez p1, :cond_2

    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_2
    invoke-virtual {p2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇o〇88〇8(Ljava/util/List;)Z

    .line 80
    .line 81
    .line 82
    move-result p0

    .line 83
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 84
    .line 85
    .line 86
    :goto_1
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇80o(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x6

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    const/16 v2, 0x65

    .line 7
    .line 8
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    const-string v3, "growth_record"

    .line 13
    .line 14
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 15
    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    aput-object v1, v0, v2

    .line 19
    .line 20
    new-instance v1, Landroid/util/Pair;

    .line 21
    .line 22
    const/16 v2, 0x66

    .line 23
    .line 24
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    const-string v3, "briefcase"

    .line 29
    .line 30
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    aput-object v1, v0, v2

    .line 35
    .line 36
    new-instance v1, Landroid/util/Pair;

    .line 37
    .line 38
    const/16 v2, 0x67

    .line 39
    .line 40
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    const-string v3, "ideas"

    .line 45
    .line 46
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    const/4 v2, 0x2

    .line 50
    aput-object v1, v0, v2

    .line 51
    .line 52
    new-instance v1, Landroid/util/Pair;

    .line 53
    .line 54
    const/16 v2, 0x68

    .line 55
    .line 56
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const-string v3, "family_file"

    .line 61
    .line 62
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    const/4 v2, 0x3

    .line 66
    aput-object v1, v0, v2

    .line 67
    .line 68
    new-instance v1, Landroid/util/Pair;

    .line 69
    .line 70
    const/16 v2, 0x69

    .line 71
    .line 72
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    const-string v3, "certificate"

    .line 77
    .line 78
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 79
    .line 80
    .line 81
    const/4 v2, 0x4

    .line 82
    aput-object v1, v0, v2

    .line 83
    .line 84
    new-instance v1, Landroid/util/Pair;

    .line 85
    .line 86
    const/16 v2, 0x6a

    .line 87
    .line 88
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o880(Ljava/util/List;I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    const-string v2, "work_file"

    .line 93
    .line 94
    invoke-direct {v1, v2, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    const/4 p1, 0x5

    .line 98
    aput-object v1, v0, p1

    .line 99
    .line 100
    const-string p1, "CSAdvancedFolderGuidePop"

    .line 101
    .line 102
    const-string v1, "next"

    .line 103
    .line 104
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final 〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    const-wide/16 v1, 0x12c

    .line 9
    .line 10
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-nez p1, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    const-string p1, "CSAdvancedFolderGuidePop"

    .line 18
    .line 19
    const-string v0, "close"

    .line 20
    .line 21
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->dismissAllowingStateLoss()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o〇0〇o(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o〇88〇8(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
            ">;)Z"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->o〇0()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    const/4 p1, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_1
    const/4 p1, 0x0

    .line 26
    :goto_0
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final 〇〇o0〇8()Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$Companion;->〇080()Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dismissAllowingStateLoss()V
    .locals 2

    .line 1
    :try_start_0
    invoke-super {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    const-string v1, "SuperDirGuideDialog"

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    .line 10
    .line 11
    :goto_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140150

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d034b

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    const-string v0, "SuperDirGuideDialog"

    .line 5
    .line 6
    const-string v1, "onDestroyView"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->OO:Lcom/intsig/callback/DialogDismissListener;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/callback/DialogDismissListener;->dismiss()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSAdvancedFolderGuidePop"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    const-string p2, "SuperDirGuideDialog"

    .line 10
    .line 11
    const-string v0, "onViewCreated"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    if-eqz p2, :cond_0

    .line 21
    .line 22
    new-instance v0, L〇08O〇00〇o/〇O8o08O;

    .line 23
    .line 24
    invoke-direct {v0}, L〇08O〇00〇o/〇O8o08O;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->o00〇88〇08()Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-direct {p2, v0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;-><init>(Ljava/util/List;)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->oooO888()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    if-eqz v0, :cond_1

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 46
    .line 47
    if-eqz v0, :cond_1

    .line 48
    .line 49
    new-instance v1, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 50
    .line 51
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-direct {v1, v2}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 62
    .line 63
    .line 64
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->oooO888()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    if-eqz v0, :cond_2

    .line 69
    .line 70
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 71
    .line 72
    if-eqz v0, :cond_2

    .line 73
    .line 74
    new-instance v1, L〇08O〇00〇o/OO0o〇〇;

    .line 75
    .line 76
    invoke-direct {v1, p0}, L〇08O〇00〇o/OO0o〇〇;-><init>(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    .line 81
    .line 82
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->oooO888()Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    if-eqz v0, :cond_3

    .line 87
    .line 88
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentSuperDirGuideBinding;->o〇00O:Landroid/widget/TextView;

    .line 89
    .line 90
    if-eqz v0, :cond_3

    .line 91
    .line 92
    new-instance v1, L〇08O〇00〇o/Oooo8o0〇;

    .line 93
    .line 94
    invoke-direct {v1, p0, p2}, L〇08O〇00〇o/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    .line 99
    .line 100
    :cond_3
    new-instance v0, L〇08O〇00〇o/〇〇808〇;

    .line 101
    .line 102
    invoke-direct {v0, p0, p1, p2}, L〇08O〇00〇o/〇〇808〇;-><init>(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;Landroid/view/View;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {p2, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 106
    .line 107
    .line 108
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;->OO:Lcom/intsig/callback/DialogDismissListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

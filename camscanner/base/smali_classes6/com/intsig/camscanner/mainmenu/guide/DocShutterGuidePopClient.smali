.class public final Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;
.super Ljava/lang/Object;
.source "DocShutterGuidePopClient.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private Oo08:Z

.field private final 〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->oooO8〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private final O8(Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;)Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient$createOnGlobalLayoutListener$1;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient$createOnGlobalLayoutListener$1;

    .line 2
    .line 3
    invoke-direct {v0, p2, p1, p0, p3}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient$createOnGlobalLayoutListener$1;-><init>(Lcom/intsig/comm/widget/CustomTextView;Landroid/view/View;Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO80()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    const v1, 0x7f0a1a52

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    new-instance v2, L〇08O〇00〇o/OO0o〇〇〇〇0;

    .line 13
    .line 14
    invoke-direct {v2, p0}, L〇08O〇00〇o/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;

    .line 21
    .line 22
    invoke-direct {v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;-><init>()V

    .line 23
    .line 24
    .line 25
    sget-object v2, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->BOTTOM_RIGHT:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇〇808〇(Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;)V

    .line 28
    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 31
    .line 32
    const v3, 0x7f1300f5

    .line 33
    .line 34
    .line 35
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->OoO8(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 43
    .line 44
    const/16 v3, 0x10

    .line 45
    .line 46
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇0〇O0088o(I)V

    .line 51
    .line 52
    .line 53
    const v2, 0x7f0a10db

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const-string v3, "rootGuide.findViewById(R.id.shutter_bg_tips)"

    .line 61
    .line 62
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    check-cast v2, Lcom/intsig/comm/widget/CustomTextView;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->OO0o〇〇()Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Lcom/intsig/comm/widget/CustomTextView;->setArrowDirection(Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->Oooo8o0〇()Ljava/lang/CharSequence;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    if-eqz v3, :cond_1

    .line 86
    .line 87
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 88
    .line 89
    if-eqz v3, :cond_0

    .line 90
    .line 91
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 92
    .line 93
    .line 94
    move-result-object v3

    .line 95
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 96
    .line 97
    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 98
    .line 99
    .line 100
    :cond_0
    invoke-direct {p0, v0, v2, v1}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8(Landroid/view/View;Lcom/intsig/comm/widget/CustomTextView;Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;)Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient$createOnGlobalLayoutListener$1;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 105
    .line 106
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 111
    .line 112
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    .line 116
    .line 117
    .line 118
    :cond_1
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇〇888()V

    .line 7
    .line 8
    .line 9
    const-string p0, "CSHome"

    .line 10
    .line 11
    const-string p1, "scan_bubble_close"

    .line 12
    .line 13
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->Oo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final OO0o〇〇〇〇0(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "rootView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇8〇()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/16 v1, 0x8

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 15
    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void

    .line 23
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇8〇()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v2, 0x2

    .line 28
    if-ne v0, v2, :cond_9

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/camscanner/mainmenu/guide/DocCaptureGuideType;->〇080:Lcom/intsig/camscanner/mainmenu/guide/DocCaptureGuideType$Companion;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/guide/DocCaptureGuideType$Companion;->〇080()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_9

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O〇8O0O80〇()Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/widget/MainBottomTabLayout;->getMFabButton()Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const/4 v2, 0x0

    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-nez v0, :cond_2

    .line 56
    .line 57
    const/4 v0, 0x1

    .line 58
    goto :goto_1

    .line 59
    :cond_2
    const/4 v0, 0x0

    .line 60
    :goto_1
    if-nez v0, :cond_3

    .line 61
    .line 62
    goto :goto_5

    .line 63
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 64
    .line 65
    invoke-static {v0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇0(Landroid/content/Context;)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-lez v0, :cond_5

    .line 70
    .line 71
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 72
    .line 73
    if-nez p1, :cond_4

    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_4
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 77
    .line 78
    .line 79
    :goto_2
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O88〇o〇(I)V

    .line 80
    .line 81
    .line 82
    return-void

    .line 83
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 84
    .line 85
    if-nez v0, :cond_8

    .line 86
    .line 87
    const v0, 0x7f0a1a67

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    if-nez v0, :cond_6

    .line 95
    .line 96
    goto :goto_3

    .line 97
    :cond_6
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    .line 99
    .line 100
    :goto_3
    const v0, 0x7f0a0ce4

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 108
    .line 109
    if-nez p1, :cond_7

    .line 110
    .line 111
    goto :goto_4

    .line 112
    :cond_7
    const/4 v0, 0x4

    .line 113
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 114
    .line 115
    .line 116
    :cond_8
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->oO80()V

    .line 117
    .line 118
    .line 119
    return-void

    .line 120
    :cond_9
    :goto_5
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 121
    .line 122
    if-nez p1, :cond_a

    .line 123
    .line 124
    goto :goto_6

    .line 125
    :cond_a
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    .line 127
    .line 128
    :goto_6
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final Oo08()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 11
    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇o〇:Landroid/view/View;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const v2, 0x7f0a1212

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    move-object v0, v1

    .line 31
    :goto_0
    if-eqz v0, :cond_3

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    if-nez v2, :cond_2

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 47
    .line 48
    .line 49
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->O8:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 50
    .line 51
    nop

    .line 52
    :cond_3
    :goto_1
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇〇888()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O88〇o〇(I)V

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/guide/DocShutterGuidePopClient;->〇8o8o〇()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;
.super Lcom/chad/library/adapter/base/BaseQuickAdapter;
.source "SuperDirGuideDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SuperDirInnerAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
        "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
        "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const v0, 0x7f0d04c0

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;-><init>(ILjava/util/List;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected O0OO8〇0(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "holder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v1

    .line 22
    :goto_0
    if-nez v0, :cond_1

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->Oo08()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    .line 31
    .line 32
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 39
    .line 40
    :cond_2
    if-nez v1, :cond_3

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇080()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    .line 49
    .line 50
    :goto_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    if-eqz v0, :cond_4

    .line 55
    .line 56
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 57
    .line 58
    if-eqz v0, :cond_4

    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->O8()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    .line 66
    .line 67
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_5

    .line 72
    .line 73
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;->o〇00O:Landroid/widget/ImageView;

    .line 74
    .line 75
    if-eqz v0, :cond_5

    .line 76
    .line 77
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->〇o〇()I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 82
    .line 83
    .line 84
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;->〇00()Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    if-eqz p1, :cond_7

    .line 89
    .line 90
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemSuperDirLinearBinding;->OO:Landroid/widget/ImageView;

    .line 91
    .line 92
    if-eqz p1, :cond_7

    .line 93
    .line 94
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;->o〇0()Z

    .line 95
    .line 96
    .line 97
    move-result p2

    .line 98
    if-eqz p2, :cond_6

    .line 99
    .line 100
    const p2, 0x7f08079a

    .line 101
    .line 102
    .line 103
    goto :goto_3

    .line 104
    :cond_6
    const p2, 0x7f080e4d

    .line 105
    .line 106
    .line 107
    :goto_3
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 108
    .line 109
    .line 110
    :cond_7
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirInnerAdapter;->O0OO8〇0(Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearProvider;Lcom/intsig/camscanner/mainmenu/guide/SuperDirGuideDialog$SuperDirLinearItem;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

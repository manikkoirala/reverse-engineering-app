.class public final Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;
.super Ljava/lang/Object;
.source "ToolPageV2Repo.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇080(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;",
            ">;"
        }
    .end annotation

    .line 1
    const-string v0, "ToolPageV2Repo"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    :try_start_0
    const-class v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgServerData;

    .line 5
    .line 6
    invoke-static {p1, v2}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgServerData;

    .line 11
    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    const-string p1, "data is null"

    .line 15
    .line 16
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-object v1

    .line 20
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgServerData;->getApplication()Ljava/util/ArrayList;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    if-eqz v2, :cond_2

    .line 25
    .line 26
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 v2, 0x0

    .line 34
    goto :goto_1

    .line 35
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 36
    :goto_1
    if-eqz v2, :cond_3

    .line 37
    .line 38
    const-string p1, "data.list is null or empty"

    .line 39
    .line 40
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-object v1

    .line 44
    :cond_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgServerData;->getApplication()Ljava/util/ArrayList;

    .line 45
    .line 46
    .line 47
    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    return-object p1

    .line 49
    :catch_0
    move-exception p1

    .line 50
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .line 52
    .line 53
    return-object v1
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇o00〇〇Oo()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇〇888()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇080(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 14
    .line 15
    .line 16
    move-result v3

    .line 17
    if-eqz v3, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v3, 0x0

    .line 21
    goto :goto_1

    .line 22
    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 23
    :goto_1
    if-eqz v3, :cond_2

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇o〇()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇080(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .line 37
    .line 38
    :try_start_0
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const/4 v4, 0x0

    .line 46
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    if-eqz v5, :cond_13

    .line 51
    .line 52
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    add-int/lit8 v6, v4, 0x1

    .line 57
    .line 58
    if-gez v4, :cond_3

    .line 59
    .line 60
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 61
    .line 62
    .line 63
    :cond_3
    check-cast v5, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;

    .line 64
    .line 65
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_language_key()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    invoke-static {v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v8

    .line 73
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_name()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object v4

    .line 77
    if-eqz v4, :cond_5

    .line 78
    .line 79
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    if-nez v4, :cond_4

    .line 84
    .line 85
    goto :goto_3

    .line 86
    :cond_4
    const/4 v4, 0x0

    .line 87
    goto :goto_4

    .line 88
    :cond_5
    :goto_3
    const/4 v4, 0x1

    .line 89
    :goto_4
    if-eqz v4, :cond_6

    .line 90
    .line 91
    const-string v4, ""

    .line 92
    .line 93
    goto :goto_5

    .line 94
    :cond_6
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_name()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v4

    .line 98
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 99
    .line 100
    .line 101
    :goto_5
    new-instance v14, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;

    .line 102
    .line 103
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_id()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v7

    .line 107
    if-eqz v7, :cond_8

    .line 108
    .line 109
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    .line 110
    .line 111
    .line 112
    move-result v7

    .line 113
    if-nez v7, :cond_7

    .line 114
    .line 115
    goto :goto_6

    .line 116
    :cond_7
    const/4 v7, 0x0

    .line 117
    goto :goto_7

    .line 118
    :cond_8
    :goto_6
    const/4 v7, 0x1

    .line 119
    :goto_7
    if-eqz v7, :cond_9

    .line 120
    .line 121
    const-string v7, "10152"

    .line 122
    .line 123
    goto :goto_8

    .line 124
    :cond_9
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_id()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v7

    .line 128
    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 129
    .line 130
    .line 131
    :goto_8
    move-object v10, v7

    .line 132
    const/4 v11, 0x0

    .line 133
    const/16 v12, 0x8

    .line 134
    .line 135
    const/4 v13, 0x0

    .line 136
    move-object v7, v14

    .line 137
    move-object v9, v4

    .line 138
    invoke-direct/range {v7 .. v13}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    .line 143
    .line 144
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 145
    .line 146
    .line 147
    move-result-object v7

    .line 148
    if-eqz v7, :cond_b

    .line 149
    .line 150
    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    .line 151
    .line 152
    .line 153
    move-result v7

    .line 154
    if-eqz v7, :cond_a

    .line 155
    .line 156
    goto :goto_9

    .line 157
    :cond_a
    const/4 v7, 0x0

    .line 158
    goto :goto_a

    .line 159
    :cond_b
    :goto_9
    const/4 v7, 0x1

    .line 160
    :goto_a
    if-nez v7, :cond_12

    .line 161
    .line 162
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getClass_style_type()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v7

    .line 166
    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    .line 167
    .line 168
    .line 169
    move-result v8

    .line 170
    packed-switch v8, :pswitch_data_0

    .line 171
    .line 172
    .line 173
    goto/16 :goto_b

    .line 174
    .line 175
    :pswitch_0
    const-string v8, "6"

    .line 176
    .line 177
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    move-result v7

    .line 181
    if-nez v7, :cond_c

    .line 182
    .line 183
    goto/16 :goto_b

    .line 184
    .line 185
    :cond_c
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2LeftTwoRightOneItem;

    .line 186
    .line 187
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2LeftTwoRightOneItem;-><init>()V

    .line 188
    .line 189
    .line 190
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 191
    .line 192
    .line 193
    move-result-object v8

    .line 194
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 195
    .line 196
    .line 197
    move-result-object v5

    .line 198
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 199
    .line 200
    .line 201
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 202
    .line 203
    .line 204
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    goto/16 :goto_c

    .line 211
    .line 212
    :pswitch_1
    const-string v8, "5"

    .line 213
    .line 214
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 215
    .line 216
    .line 217
    move-result v7

    .line 218
    if-nez v7, :cond_d

    .line 219
    .line 220
    goto/16 :goto_b

    .line 221
    .line 222
    :cond_d
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TopThreeBottomTwoItem;

    .line 223
    .line 224
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TopThreeBottomTwoItem;-><init>()V

    .line 225
    .line 226
    .line 227
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 228
    .line 229
    .line 230
    move-result-object v8

    .line 231
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 232
    .line 233
    .line 234
    move-result-object v5

    .line 235
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 239
    .line 240
    .line 241
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    .line 246
    .line 247
    goto/16 :goto_c

    .line 248
    .line 249
    :pswitch_2
    const-string v8, "4"

    .line 250
    .line 251
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 252
    .line 253
    .line 254
    move-result v7

    .line 255
    if-nez v7, :cond_e

    .line 256
    .line 257
    goto/16 :goto_b

    .line 258
    .line 259
    :cond_e
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2AverageTwoItem;

    .line 260
    .line 261
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2AverageTwoItem;-><init>()V

    .line 262
    .line 263
    .line 264
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 265
    .line 266
    .line 267
    move-result-object v8

    .line 268
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 269
    .line 270
    .line 271
    move-result-object v5

    .line 272
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 273
    .line 274
    .line 275
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 276
    .line 277
    .line 278
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    .line 283
    .line 284
    goto/16 :goto_c

    .line 285
    .line 286
    :pswitch_3
    const-string v8, "3"

    .line 287
    .line 288
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 289
    .line 290
    .line 291
    move-result v7

    .line 292
    if-nez v7, :cond_f

    .line 293
    .line 294
    goto :goto_b

    .line 295
    :cond_f
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2FiveDropFirstItem;

    .line 296
    .line 297
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2FiveDropFirstItem;-><init>()V

    .line 298
    .line 299
    .line 300
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 301
    .line 302
    .line 303
    move-result-object v8

    .line 304
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 305
    .line 306
    .line 307
    move-result-object v5

    .line 308
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 309
    .line 310
    .line 311
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 312
    .line 313
    .line 314
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 315
    .line 316
    .line 317
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    .line 319
    .line 320
    goto :goto_c

    .line 321
    :pswitch_4
    const-string v8, "2"

    .line 322
    .line 323
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 324
    .line 325
    .line 326
    move-result v7

    .line 327
    if-nez v7, :cond_10

    .line 328
    .line 329
    goto :goto_b

    .line 330
    :cond_10
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2LeftOneRightTwoItem;

    .line 331
    .line 332
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2LeftOneRightTwoItem;-><init>()V

    .line 333
    .line 334
    .line 335
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 336
    .line 337
    .line 338
    move-result-object v8

    .line 339
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 340
    .line 341
    .line 342
    move-result-object v5

    .line 343
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 344
    .line 345
    .line 346
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 347
    .line 348
    .line 349
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 350
    .line 351
    .line 352
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    .line 354
    .line 355
    goto :goto_c

    .line 356
    :pswitch_5
    const-string v8, "1"

    .line 357
    .line 358
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 359
    .line 360
    .line 361
    move-result v7

    .line 362
    if-nez v7, :cond_11

    .line 363
    .line 364
    goto :goto_b

    .line 365
    :cond_11
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TopThreeBottomOneItem;

    .line 366
    .line 367
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TopThreeBottomOneItem;-><init>()V

    .line 368
    .line 369
    .line 370
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 371
    .line 372
    .line 373
    move-result-object v8

    .line 374
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 375
    .line 376
    .line 377
    move-result-object v5

    .line 378
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 379
    .line 380
    .line 381
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 382
    .line 383
    .line 384
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 385
    .line 386
    .line 387
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    .line 389
    .line 390
    goto :goto_c

    .line 391
    :goto_b
    new-instance v7, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2SquareItem;

    .line 392
    .line 393
    invoke-direct {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2SquareItem;-><init>()V

    .line 394
    .line 395
    .line 396
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 397
    .line 398
    .line 399
    move-result-object v8

    .line 400
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentData;->getContent()Ljava/util/ArrayList;

    .line 401
    .line 402
    .line 403
    move-result-object v5

    .line 404
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 405
    .line 406
    .line 407
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 408
    .line 409
    .line 410
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->O8(Ljava/lang/String;)V

    .line 411
    .line 412
    .line 413
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    .line 415
    .line 416
    :cond_12
    :goto_c
    move v4, v6

    .line 417
    goto/16 :goto_2

    .line 418
    .line 419
    :catch_0
    move-exception v0

    .line 420
    const-string v1, "ToolPageV2Repo"

    .line 421
    .line 422
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 423
    .line 424
    .line 425
    :cond_13
    return-object v3

    .line 426
    nop

    .line 427
    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public final 〇o〇(Lkotlin/jvm/functions/Function0;)V
    .locals 3
    .param p1    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;->〇080()Ljava/util/HashMap;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->oO80(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$queryData$1;

    .line 22
    .line 23
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$queryData$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/tianshu/TianShuAPI;->o8O〇(Ljava/util/HashMap;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

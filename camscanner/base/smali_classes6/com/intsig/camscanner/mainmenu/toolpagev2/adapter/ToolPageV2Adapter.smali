.class public final Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "ToolPageV2Adapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    invoke-direct {p0, v0, v1, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2RecentUseProvider;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2RecentUseProvider;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 12
    .line 13
    .line 14
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2TitleMoreProvider;

    .line 15
    .line 16
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2TitleMoreProvider;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 20
    .line 21
    .line 22
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;

    .line 23
    .line 24
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 28
    .line 29
    .line 30
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2TopThreeBottomOneProvider;

    .line 31
    .line 32
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2TopThreeBottomOneProvider;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 36
    .line 37
    .line 38
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2LeftOneRightTwoProvider;

    .line 39
    .line 40
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2LeftOneRightTwoProvider;-><init>()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 44
    .line 45
    .line 46
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2LeftTwoRightOneProvider;

    .line 47
    .line 48
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2LeftTwoRightOneProvider;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 52
    .line 53
    .line 54
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageFiveDropFirstProvider;

    .line 55
    .line 56
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageFiveDropFirstProvider;-><init>()V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 60
    .line 61
    .line 62
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageAverageTwoProvider;

    .line 63
    .line 64
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageAverageTwoProvider;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 68
    .line 69
    .line 70
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageTopThreeBottomTwoProvider;

    .line 71
    .line 72
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageTopThreeBottomTwoProvider;-><init>()V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 76
    .line 77
    .line 78
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o〇()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

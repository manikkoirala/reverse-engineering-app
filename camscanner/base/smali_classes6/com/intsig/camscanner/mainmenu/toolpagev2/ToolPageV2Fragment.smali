.class public final Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;
.super Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;
.source "ToolPageV2Fragment.kt"

# interfaces
.implements Lcom/chad/library/adapter/base/listener/OnItemChildClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic O8o08O8O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:I

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->O8o08O8O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/BaseToolPageFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$2;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 35
    .line 36
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$3;

    .line 41
    .line 42
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$4;

    .line 46
    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$5;

    .line 52
    .line 53
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 61
    .line 62
    const/4 v0, 0x5

    .line 63
    new-array v0, v0, [Ljava/lang/String;

    .line 64
    .line 65
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    const v3, 0x7f1303bc

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    const/4 v3, 0x0

    .line 79
    aput-object v2, v0, v3

    .line 80
    .line 81
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    const v3, 0x7f130f6b

    .line 86
    .line 87
    .line 88
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    const/4 v3, 0x1

    .line 93
    aput-object v2, v0, v3

    .line 94
    .line 95
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    const v3, 0x7f130a20

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v2

    .line 106
    const/4 v3, 0x2

    .line 107
    aput-object v2, v0, v3

    .line 108
    .line 109
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    const v3, 0x7f1309a1

    .line 114
    .line 115
    .line 116
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    const/4 v3, 0x3

    .line 121
    aput-object v2, v0, v3

    .line 122
    .line 123
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 124
    .line 125
    .line 126
    move-result-object v1

    .line 127
    const v2, 0x7f1314e7

    .line 128
    .line 129
    .line 130
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    const/4 v2, 0x4

    .line 135
    aput-object v1, v0, v2

    .line 136
    .line 137
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->OO:Ljava/util/ArrayList;

    .line 142
    .line 143
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇〇〇0(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo8o(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;)V
    .locals 6

    .line 1
    const/4 v0, 0x2

    .line 2
    new-array v0, v0, [Landroid/util/Pair;

    .line 3
    .line 4
    new-instance v1, Landroid/util/Pair;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->Oo08()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v2

    .line 10
    const-string v3, "from"

    .line 11
    .line 12
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aput-object v1, v0, v2

    .line 17
    .line 18
    new-instance v1, Landroid/util/Pair;

    .line 19
    .line 20
    const-string v3, "scheme_type"

    .line 21
    .line 22
    const-string v4, "643_new"

    .line 23
    .line 24
    invoke-direct {v1, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    .line 26
    .line 27
    const/4 v3, 0x1

    .line 28
    aput-object v1, v0, v3

    .line 29
    .line 30
    const-string v1, "CSMainApplication"

    .line 31
    .line 32
    const-string v4, "more"

    .line 33
    .line 34
    invoke-static {v1, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 38
    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    new-instance v1, Landroid/os/Bundle;

    .line 42
    .line 43
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v4, "10154"

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v5

    .line 52
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    if-nez v4, :cond_0

    .line 57
    .line 58
    const-string v4, "10153"

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_1

    .line 69
    .line 70
    :cond_0
    const/4 v2, 0x1

    .line 71
    :cond_1
    const-string p1, "key_scroll_to_bottom"

    .line 72
    .line 73
    invoke-virtual {v1, p1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 74
    .line 75
    .line 76
    const p1, 0x7f131c6b

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    const-string v2, "title"

    .line 84
    .line 85
    invoke-virtual {v1, v2, p1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    const-class p1, Lcom/intsig/camscanner/mainmenu/toolpage/ToolPageFragment;

    .line 89
    .line 90
    invoke-static {v0, p1, v1}, Lcom/intsig/comm/router/Routers;->O8(Landroid/content/Context;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 91
    .line 92
    .line 93
    :cond_2
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p4, 0x4

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p3, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic oO〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇O8〇8000(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇08O〇00〇o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/google/android/material/tabs/TabLayout$Tab;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8O0880(Lcom/google/android/material/tabs/TabLayout$Tab;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇088O()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$addListeners$1;

    .line 12
    .line 13
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$addListeners$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$addListeners$2$1;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$addListeners$2$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0oO〇oo00()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$subscribeUI$1;

    .line 10
    .line 11
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$subscribeUI$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;)V

    .line 12
    .line 13
    .line 14
    new-instance v2, Lo8o/O0oo0o0〇;

    .line 15
    .line 16
    invoke-direct {v2, v1}, Lo8o/O0oo0o0〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0ooOOo(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V
    .locals 6

    .line 1
    const-string v0, "ToolPageV2Fragment"

    .line 2
    .line 3
    const-string v1, "handleRealFunction"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    :try_start_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getDeeplink()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    if-eqz v2, :cond_8

    .line 13
    .line 14
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    if-eqz v2, :cond_8

    .line 19
    .line 20
    const-string v3, "parse(deepLink)"

    .line 21
    .line 22
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    if-eqz v3, :cond_8

    .line 28
    .line 29
    const-string v4, "mActivity"

    .line 30
    .line 31
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    if-nez p3, :cond_0

    .line 35
    .line 36
    sget-object v4, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;

    .line 37
    .line 38
    invoke-virtual {v4, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇080(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const-string v4, "from_tool_page"

    .line 46
    .line 47
    const-string v5, "1"

    .line 48
    .line 49
    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-static {v3, v2}, Lcom/intsig/router/CSRouterManager;->O8(Landroid/content/Context;Landroid/net/Uri;)V

    .line 58
    .line 59
    .line 60
    if-nez p3, :cond_1

    .line 61
    .line 62
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 63
    .line 64
    .line 65
    move-result-object p3

    .line 66
    invoke-virtual {p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->o0ooO()V

    .line 67
    .line 68
    .line 69
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getLog_action()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p3

    .line 73
    if-eqz p3, :cond_6

    .line 74
    .line 75
    invoke-virtual {p3}, Ljava/lang/String;->hashCode()I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    sparse-switch v2, :sswitch_data_0

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :sswitch_0
    const-string v2, "import_doc"

    .line 84
    .line 85
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    move-result p3

    .line 89
    if-eqz p3, :cond_6

    .line 90
    .line 91
    sget-object p3, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolImportFile:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :sswitch_1
    const-string v2, "doc_revise"

    .line 95
    .line 96
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 97
    .line 98
    .line 99
    move-result p3

    .line 100
    if-nez p3, :cond_2

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_2
    sget-object p3, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfSort:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 104
    .line 105
    goto :goto_1

    .line 106
    :sswitch_2
    const-string v2, "doc_pic_up"

    .line 107
    .line 108
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    move-result p3

    .line 112
    if-nez p3, :cond_3

    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_3
    sget-object p3, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolPdfExtract:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :sswitch_3
    const-string v2, "doc_merge"

    .line 119
    .line 120
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 121
    .line 122
    .line 123
    move-result p3

    .line 124
    if-nez p3, :cond_4

    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_4
    sget-object p3, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileMerge:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 128
    .line 129
    goto :goto_1

    .line 130
    :sswitch_4
    const-string v2, "doc_signature"

    .line 131
    .line 132
    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    move-result p3

    .line 136
    if-nez p3, :cond_5

    .line 137
    .line 138
    goto :goto_0

    .line 139
    :cond_5
    sget-object p3, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->ToolFileSignature:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 140
    .line 141
    goto :goto_1

    .line 142
    :cond_6
    :goto_0
    const/4 p3, 0x0

    .line 143
    :goto_1
    new-instance v2, Lorg/json/JSONObject;

    .line 144
    .line 145
    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 146
    .line 147
    .line 148
    const-string v3, "from"

    .line 149
    .line 150
    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 151
    .line 152
    .line 153
    const-string p2, "type"

    .line 154
    .line 155
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getLog_action()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object p1

    .line 159
    invoke-virtual {v2, p2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    .line 161
    .line 162
    if-eqz p3, :cond_7

    .line 163
    .line 164
    const-string p1, "refer_source"

    .line 165
    .line 166
    invoke-virtual {p3}, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->getCsPdfTrack()Ljava/lang/String;

    .line 167
    .line 168
    .line 169
    move-result-object p2

    .line 170
    invoke-virtual {v2, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 171
    .line 172
    .line 173
    :cond_7
    const-string p1, "scheme_type"

    .line 174
    .line 175
    const-string p2, "643_new"

    .line 176
    .line 177
    invoke-virtual {v2, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 178
    .line 179
    .line 180
    const-string p1, "CSMainApplication"

    .line 181
    .line 182
    const-string p2, "select"

    .line 183
    .line 184
    invoke-static {p1, p2, v2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    .line 186
    .line 187
    goto :goto_2

    .line 188
    :catch_0
    move-exception p1

    .line 189
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    new-instance p2, Ljava/lang/StringBuilder;

    .line 194
    .line 195
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .line 197
    .line 198
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object p1

    .line 208
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    :cond_8
    :goto_2
    return-void

    .line 212
    nop

    .line 213
    :sswitch_data_0
    .sparse-switch
        -0x31b0d8f -> :sswitch_4
        0x12b2c271 -> :sswitch_3
        0x48f58657 -> :sswitch_2
        0x4c2fa1df -> :sswitch_1
        0x7eb1cd9e -> :sswitch_0
    .end sparse-switch
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final 〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V
    .locals 3

    .line 1
    const-string v0, "handleClickedItem"

    .line 2
    .line 3
    const-string v1, "ToolPageV2Fragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getUnit_id()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v2, "1"

    .line 13
    .line 14
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v2, 0x1

    .line 19
    if-ne v0, v2, :cond_4

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getContent()Ljava/util/ArrayList;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    if-eqz p2, :cond_1

    .line 26
    .line 27
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    const/4 v0, 0x0

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 37
    :goto_1
    xor-int/2addr v0, v2

    .line 38
    const/4 v2, 0x0

    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    goto :goto_2

    .line 42
    :cond_2
    move-object p2, v2

    .line 43
    :goto_2
    if-eqz p2, :cond_3

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getContent()Ljava/util/ArrayList;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getLog_action()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-direct {p0, p2, p1, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇〇O80〇0o(Ljava/util/ArrayList;Ljava/lang/String;Z)V

    .line 57
    .line 58
    .line 59
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 60
    .line 61
    :cond_3
    if-nez v2, :cond_5

    .line 62
    .line 63
    const-string p1, "content is null or empty"

    .line 64
    .line 65
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_4
    if-nez v0, :cond_5

    .line 70
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0ooOOo(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    .line 72
    .line 73
    .line 74
    :cond_5
    :goto_3
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8O0880(Lcom/google/android/material/tabs/TabLayout$Tab;Z)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->getCustomView()Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object p1, v0

    .line 10
    :goto_0
    instance-of v1, p1, Landroid/widget/TextView;

    .line 11
    .line 12
    if-eqz v1, :cond_1

    .line 13
    .line 14
    move-object v0, p1

    .line 15
    check-cast v0, Landroid/widget/TextView;

    .line 16
    .line 17
    :cond_1
    if-eqz v0, :cond_4

    .line 18
    .line 19
    if-eqz p2, :cond_2

    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    invoke-static {p1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    goto :goto_1

    .line 27
    :cond_2
    const/4 p1, 0x0

    .line 28
    invoke-static {p1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    :goto_1
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 33
    .line 34
    .line 35
    if-eqz p2, :cond_3

    .line 36
    .line 37
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const p2, 0x7f060208

    .line 44
    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_3
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    const p2, 0x7f060206

    .line 54
    .line 55
    .line 56
    :goto_2
    invoke-static {p1, p2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 61
    .line 62
    .line 63
    :cond_4
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->O8o08O8O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇08O〇00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O8〇8000(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o08()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->Oo(Lcom/chad/library/adapter/base/listener/OnItemChildClickListener;)V

    .line 7
    .line 8
    .line 9
    const/16 v1, 0x1f

    .line 10
    .line 11
    new-array v1, v1, [I

    .line 12
    .line 13
    fill-array-data v1, :array_0

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇〇8O0〇8([I)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 26
    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    new-instance v2, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 30
    .line 31
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    const/4 v4, 0x1

    .line 34
    const/4 v5, 0x0

    .line 35
    invoke-direct {v2, v3, v4, v5}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void

    .line 45
    :array_0
    .array-data 4
        0x7f0a0d0b
        0x7f0a0d0d
        0x7f0a0d0e
        0x7f0a0d0c
        0x7f0a044c
        0x7f0a0441
        0x7f0a0443
        0x7f0a0442
        0x7f0a0446
        0x7f0a0445
        0x7f0a0444
        0x7f0a0449
        0x7f0a044a
        0x7f0a0447
        0x7f0a0448
        0x7f0a044e
        0x7f0a044f
        0x7f0a0450
        0x7f0a044d
        0x7f0a043e
        0x7f0a043d
        0x7f0a043c
        0x7f0a043f
        0x7f0a0440
        0x7f0a043a
        0x7f0a043b
        0x7f0a0453
        0x7f0a0454
        0x7f0a0455
        0x7f0a0451
        0x7f0a0452
    .end array-data
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;",
            ">;I)",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-le v0, p2, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    .line 18
    .line 19
    return-object p1

    .line 20
    :cond_0
    const/4 p1, 0x0

    .line 21
    return-object p1
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇O80〇0o(Ljava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .line 1
    const-string v0, "ToolPageV2Fragment"

    .line 2
    .line 3
    const-string v1, "showMoreFunctionsDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x2

    .line 9
    new-array v0, v0, [Landroid/util/Pair;

    .line 10
    .line 11
    new-instance v1, Landroid/util/Pair;

    .line 12
    .line 13
    if-nez p2, :cond_0

    .line 14
    .line 15
    const-string v2, ""

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    move-object v2, p2

    .line 19
    :goto_0
    const-string v3, "from"

    .line 20
    .line 21
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    new-instance v1, Landroid/util/Pair;

    .line 28
    .line 29
    const-string v2, "scheme_type"

    .line 30
    .line 31
    const-string v3, "643_new"

    .line 32
    .line 33
    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    aput-object v1, v0, v2

    .line 38
    .line 39
    const-string v1, "CSMainApplication"

    .line 40
    .line 41
    const-string v2, "more"

    .line 42
    .line 43
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 44
    .line 45
    .line 46
    sget-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/dialog/ToolPageMoreFunctionsDialog;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/dialog/ToolPageMoreFunctionsDialog$Companion;

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/dialog/ToolPageMoreFunctionsDialog$Companion;->〇080(Ljava/util/ArrayList;)Lcom/intsig/camscanner/mainmenu/toolpagev2/dialog/ToolPageMoreFunctionsDialog;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$showMoreFunctionsDialog$1;

    .line 53
    .line 54
    invoke-direct {v0, p0, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment$showMoreFunctionsDialog$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Ljava/lang/String;Z)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/dialog/ToolPageMoreFunctionsDialog;->〇o〇88〇8(Lkotlin/jvm/functions/Function1;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    const-string p3, "ToolPageMoreFunctionsDialog"

    .line 65
    .line 66
    invoke-virtual {p1, p2, p3}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0ooOOo(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇〇〇0(Lcom/google/android/material/tabs/TabLayout$Tab;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_a

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->getPosition()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_a

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    goto/16 :goto_3

    .line 18
    .line 19
    :cond_0
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    move-object v0, v2

    .line 32
    :goto_0
    if-nez v0, :cond_2

    .line 33
    .line 34
    return-void

    .line 35
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v3, "tab position = "

    .line 41
    .line 42
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const-string v3, "ToolPageV2Fragment"

    .line 53
    .line 54
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    if-eqz p1, :cond_7

    .line 58
    .line 59
    const/4 v1, 0x1

    .line 60
    if-eq p1, v1, :cond_6

    .line 61
    .line 62
    const/4 v1, 0x2

    .line 63
    if-eq p1, v1, :cond_5

    .line 64
    .line 65
    const/4 v1, 0x3

    .line 66
    if-eq p1, v1, :cond_4

    .line 67
    .line 68
    const/4 v1, 0x4

    .line 69
    if-eq p1, v1, :cond_3

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    const-string v2, "10154"

    .line 77
    .line 78
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    goto :goto_1

    .line 83
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    const-string v2, "10153"

    .line 88
    .line 89
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    goto :goto_1

    .line 94
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    const-string v2, "10155"

    .line 99
    .line 100
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    goto :goto_1

    .line 105
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    const-string v2, "10156"

    .line 110
    .line 111
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    goto :goto_1

    .line 116
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    const-string v2, "10152"

    .line 121
    .line 122
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    :goto_1
    if-eqz v2, :cond_a

    .line 127
    .line 128
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    .line 129
    .line 130
    .line 131
    move-result v1

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    .line 133
    .line 134
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .line 136
    .line 137
    const-string v4, "findTabIndexInList = "

    .line 138
    .line 139
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    const/4 v2, 0x0

    .line 153
    if-nez p1, :cond_8

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 157
    .line 158
    .line 159
    move-result-object p1

    .line 160
    if-eqz p1, :cond_9

    .line 161
    .line 162
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 163
    .line 164
    if-eqz p1, :cond_9

    .line 165
    .line 166
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 167
    .line 168
    .line 169
    move-result v2

    .line 170
    :cond_9
    :goto_2
    invoke-virtual {v0, v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 171
    .line 172
    .line 173
    :cond_a
    :goto_3
    return-void
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇〇〇00()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8〇80o()Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentToolPageV2Binding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    const v1, 0x7f0601e1

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/google/android/material/tabs/TabLayout;->setTabRippleColorResource(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->removeAllTabs()V

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->OO:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    const/4 v3, 0x0

    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    check-cast v2, Ljava/lang/String;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayout;->newTab()Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    const v6, 0x7f0d0756

    .line 52
    .line 53
    .line 54
    invoke-virtual {v5, v6, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    const v5, 0x7f0a182a

    .line 59
    .line 60
    .line 61
    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 62
    .line 63
    .line 64
    move-result-object v5

    .line 65
    check-cast v5, Landroid/widget/TextView;

    .line 66
    .line 67
    const/16 v6, 0x11

    .line 68
    .line 69
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 70
    .line 71
    .line 72
    const/high16 v6, 0x41400000    # 12.0f

    .line 73
    .line 74
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    .line 75
    .line 76
    .line 77
    sget-object v6, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 78
    .line 79
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    .line 84
    .line 85
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 86
    .line 87
    const v6, 0x7f060206

    .line 88
    .line 89
    .line 90
    invoke-static {v2, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v4, v3}, Lcom/google/android/material/tabs/TabLayout$Tab;->setCustomView(Landroid/view/View;)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v4}, Lcom/google/android/material/tabs/TabLayout;->addTab(Lcom/google/android/material/tabs/TabLayout$Tab;)V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/android/material/tabs/TabLayout;->getTabAt(I)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    const/4 v1, 0x1

    .line 109
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇8O0880(Lcom/google/android/material/tabs/TabLayout$Tab;Z)V

    .line 110
    .line 111
    .line 112
    :cond_1
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public O0o〇O0〇(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 10
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onItemChildClick"

    const-string v1, "ToolPageV2Fragment"

    .line 1
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p1, "click too fast."

    .line 3
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 4
    :cond_0
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p3, v0, :cond_1

    const-string p1, "maybe fragment onRestore, sth error"

    .line 5
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 6
    :cond_1
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string p3, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.toolpagev2.entity.BaseToolPageV2Type"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 7
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p2

    const/4 p3, 0x4

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch p2, :pswitch_data_0

    packed-switch p2, :pswitch_data_1

    packed-switch p2, :pswitch_data_2

    goto/16 :goto_1

    .line 8
    :pswitch_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 10
    :pswitch_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 12
    :pswitch_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 14
    :pswitch_3
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0〇0(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 16
    :pswitch_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 18
    :pswitch_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 20
    :pswitch_6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 22
    :pswitch_7
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 24
    :pswitch_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 26
    :pswitch_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 28
    :pswitch_a
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 30
    :pswitch_b
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 32
    :pswitch_c
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 34
    :pswitch_d
    instance-of p2, p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;

    if-eqz p2, :cond_2

    check-cast p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->oO80()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 36
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->Ooo8o(Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;)V

    goto/16 :goto_1

    .line 37
    :pswitch_e
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 39
    :pswitch_f
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 41
    :pswitch_10
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 43
    :pswitch_11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 45
    :pswitch_12
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 47
    :pswitch_13
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 49
    :pswitch_14
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 51
    :pswitch_15
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 53
    :pswitch_16
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 55
    :pswitch_17
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 57
    :pswitch_18
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 59
    :pswitch_19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto/16 :goto_1

    .line 61
    :pswitch_1a
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 62
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_1

    .line 63
    :pswitch_1b
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_1

    .line 65
    :pswitch_1c
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_1

    .line 67
    :pswitch_1d
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p0

    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    goto :goto_1

    .line 69
    :pswitch_1e
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    move-result-object p2

    invoke-direct {p0, p2, v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o〇88〇8(Ljava/util/ArrayList;I)Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->oOoO8OO〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_3
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x7f0a043a
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f0a044c
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f0a0d0b
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string p1, "ToolPageV2Fragment"

    .line 2
    .line 3
    const-string v0, "initialize"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇o08()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇〇〇00()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇088O()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->〇0oO〇oo00()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->o0ooO()V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->O8〇o()V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onResume()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    const-string v0, "scheme_type"

    .line 5
    .line 6
    const-string v1, "643_new"

    .line 7
    .line 8
    const-string v2, "CSMainApplication"

    .line 9
    .line 10
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇〇〇0〇〇0()V

    .line 18
    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇080:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo$Companion;->〇080()Ljava/util/HashMap;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Configuration;->Oo08(Ljava/lang/String;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2Fragment;->o〇O8OO()Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->O8〇o()V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0351

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

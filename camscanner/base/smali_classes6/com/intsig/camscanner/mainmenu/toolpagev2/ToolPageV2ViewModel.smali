.class public final Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "ToolPageV2ViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

.field private 〇08O〇00〇o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->o0:Landroid/app/Application;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;

    .line 12
    .line 13
    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;

    .line 17
    .line 18
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    new-instance p1, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 31
    .line 32
    new-instance p1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$onCLoseBanner$1;

    .line 33
    .line 34
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$onCLoseBanner$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)V

    .line 35
    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->O8o08O8O:Lkotlin/jvm/functions/Function0;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O8ooOoo〇()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇o00〇〇Oo()Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->O8o08O8O:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->O8ooOoo〇()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O8〇o()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$justQueryData$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$justQueryData$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/repo/ToolPageV2Repo;->〇o〇(Lkotlin/jvm/functions/Function0;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "ToolPageV2ViewModel"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    new-instance v3, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$loadCachedData$1;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$loadCachedData$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    const/4 v4, 0x2

    .line 17
    const/4 v5, 0x0

    .line 18
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public final oo88o8O(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "functionType"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 7
    .line 8
    check-cast v0, Ljava/lang/Iterable;

    .line 9
    .line 10
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-eqz v2, :cond_2

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    add-int/lit8 v3, v1, 0x1

    .line 26
    .line 27
    if-gez v1, :cond_0

    .line 28
    .line 29
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 30
    .line 31
    .line 32
    :cond_0
    check-cast v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o〇()I

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    const/4 v5, 0x1

    .line 39
    if-ne v4, v5, :cond_1

    .line 40
    .line 41
    const-string v4, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.toolpagev2.entity.ToolPageV2TitleMoreItem"

    .line 42
    .line 43
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    check-cast v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;

    .line 47
    .line 48
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_1

    .line 57
    .line 58
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    goto :goto_1

    .line 63
    :cond_1
    move v1, v3

    .line 64
    goto :goto_0

    .line 65
    :cond_2
    const/4 p1, 0x0

    .line 66
    :goto_1
    return-object p1
    .line 67
.end method

.method public final oo〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0OOo〇0(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->o〇00O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00()Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->o〇00O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O00()I
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    if-eqz v3, :cond_2

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    add-int/lit8 v4, v2, 0x1

    .line 22
    .line 23
    if-gez v2, :cond_0

    .line 24
    .line 25
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 26
    .line 27
    .line 28
    :cond_0
    check-cast v3, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 29
    .line 30
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o〇()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    const/4 v5, 0x1

    .line 35
    if-ne v3, v5, :cond_1

    .line 36
    .line 37
    move v1, v2

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    move v2, v4

    .line 40
    goto :goto_0

    .line 41
    :cond_2
    :goto_1
    return v1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇oo〇(I)Ljava/lang/Integer;
    .locals 5

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-ltz p1, :cond_0

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 6
    .line 7
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    if-ge p1, v2, :cond_0

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v2, 0x0

    .line 16
    :goto_0
    const/4 v3, 0x0

    .line 17
    if-nez v2, :cond_1

    .line 18
    .line 19
    return-object v3

    .line 20
    :cond_1
    invoke-static {p1, v1}, Lkotlin/ranges/RangesKt;->OO0o〇〇〇〇0(II)Lkotlin/ranges/IntProgression;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-static {p1, v0}, Lkotlin/ranges/RangesKt;->〇8o8o〇(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-eqz v2, :cond_7

    .line 37
    .line 38
    move-object v2, p1

    .line 39
    check-cast v2, Lkotlin/collections/IntIterator;

    .line 40
    .line 41
    invoke-virtual {v2}, Lkotlin/collections/IntIterator;->nextInt()I

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;->〇08O〇00〇o:Ljava/util/List;

    .line 46
    .line 47
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    check-cast v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 52
    .line 53
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇o〇()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-ne v4, v0, :cond_2

    .line 58
    .line 59
    const-string p1, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.toolpagev2.entity.ToolPageV2TitleMoreItem"

    .line 60
    .line 61
    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    check-cast v2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;

    .line 65
    .line 66
    const-string p1, "10152"

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v4

    .line 72
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-eqz p1, :cond_3

    .line 77
    .line 78
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    :goto_1
    move-object v3, p1

    .line 83
    goto :goto_2

    .line 84
    :cond_3
    const-string p1, "10156"

    .line 85
    .line 86
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    if-eqz p1, :cond_4

    .line 95
    .line 96
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    goto :goto_1

    .line 101
    :cond_4
    const-string p1, "10155"

    .line 102
    .line 103
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 108
    .line 109
    .line 110
    move-result p1

    .line 111
    if-eqz p1, :cond_5

    .line 112
    .line 113
    const/4 p1, 0x2

    .line 114
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    goto :goto_1

    .line 119
    :cond_5
    const-string p1, "10153"

    .line 120
    .line 121
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 126
    .line 127
    .line 128
    move-result p1

    .line 129
    if-eqz p1, :cond_6

    .line 130
    .line 131
    const/4 p1, 0x3

    .line 132
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    goto :goto_1

    .line 137
    :cond_6
    const-string p1, "10154"

    .line 138
    .line 139
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2TitleMoreItem;->o〇0()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_7

    .line 148
    .line 149
    const/4 p1, 0x4

    .line 150
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 151
    .line 152
    .line 153
    move-result-object p1

    .line 154
    goto :goto_1

    .line 155
    :cond_7
    :goto_2
    return-object v3
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇〇〇0〇〇0()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->APPLICATION_TAB_TOP:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 2
    .line 3
    iget-boolean v0, v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->isClickClose:Z

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdManager;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/ads/csAd/CsAdManager;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$queryOpeBanner$1;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel$queryOpeBanner$1;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/ToolPageV2ViewModel;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/ads/csAd/CsAdManager;->o〇0(Lcom/intsig/camscanner/ads/adinterface/AdRequestListener;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

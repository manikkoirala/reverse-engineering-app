.class public final Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "ToolPageV2SquareProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "adapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;

    .line 10
    .line 11
    const/4 p1, 0x2

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->O8o08O8O:I

    .line 13
    .line 14
    const p1, 0x7f0d04db

    .line 15
    .line 16
    .line 17
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->〇080OO8〇0:I

    .line 18
    .line 19
    return-void
    .line 20
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;-><init>(Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;)V
    .locals 11
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, p2

    .line 12
    check-cast v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/ToolPageV2SquareItem;

    .line 13
    .line 14
    move-object v0, p1

    .line 15
    check-cast v0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;->〇080()Ljava/util/ArrayList;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    const/4 v1, 0x0

    .line 26
    const/4 v2, 0x0

    .line 27
    move-object v3, v1

    .line 28
    move-object v5, v3

    .line 29
    move-object v6, v5

    .line 30
    move-object v7, v6

    .line 31
    const/4 v4, 0x0

    .line 32
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 33
    .line 34
    .line 35
    move-result v8

    .line 36
    const/4 v9, 0x1

    .line 37
    if-eqz v8, :cond_5

    .line 38
    .line 39
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    .line 41
    .line 42
    move-result-object v8

    .line 43
    add-int/lit8 v10, v4, 0x1

    .line 44
    .line 45
    if-gez v4, :cond_0

    .line 46
    .line 47
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 48
    .line 49
    .line 50
    :cond_0
    check-cast v8, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;

    .line 51
    .line 52
    if-eqz v4, :cond_4

    .line 53
    .line 54
    if-eq v4, v9, :cond_3

    .line 55
    .line 56
    const/4 v9, 0x2

    .line 57
    if-eq v4, v9, :cond_2

    .line 58
    .line 59
    const/4 v9, 0x3

    .line 60
    if-eq v4, v9, :cond_1

    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_1
    move-object v7, v8

    .line 64
    goto :goto_1

    .line 65
    :cond_2
    move-object v6, v8

    .line 66
    goto :goto_1

    .line 67
    :cond_3
    move-object v5, v8

    .line 68
    goto :goto_1

    .line 69
    :cond_4
    move-object v3, v8

    .line 70
    :goto_1
    move v4, v10

    .line 71
    goto :goto_0

    .line 72
    :cond_5
    if-eqz v3, :cond_6

    .line 73
    .line 74
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 79
    .line 80
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 92
    .line 93
    const-string v8, "helper.mBinding.ivTp2SquareTopLeft"

    .line 94
    .line 95
    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getIcon()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v8

    .line 102
    new-instance v10, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$2$1;

    .line 103
    .line 104
    invoke-direct {v10, p1, v3}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$2$1;-><init>(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;)V

    .line 105
    .line 106
    .line 107
    invoke-static {p2, v4, v8, v10}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Util;->〇080(Landroid/content/Context;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 108
    .line 109
    .line 110
    sget-object p2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 111
    .line 112
    goto :goto_2

    .line 113
    :cond_6
    move-object p2, v1

    .line 114
    :goto_2
    const/4 v3, 0x4

    .line 115
    if-nez p2, :cond_7

    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 118
    .line 119
    .line 120
    move-result-object p2

    .line 121
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 122
    .line 123
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 124
    .line 125
    .line 126
    :cond_7
    if-eqz v5, :cond_8

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 129
    .line 130
    .line 131
    move-result-object p2

    .line 132
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 133
    .line 134
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 138
    .line 139
    .line 140
    move-result-object p2

    .line 141
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 142
    .line 143
    .line 144
    move-result-object v4

    .line 145
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 146
    .line 147
    const-string v8, "helper.mBinding.ivTp2SquareTopRight"

    .line 148
    .line 149
    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getIcon()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v8

    .line 156
    new-instance v10, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$4$1;

    .line 157
    .line 158
    invoke-direct {v10, p1, v5}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$4$1;-><init>(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;)V

    .line 159
    .line 160
    .line 161
    invoke-static {p2, v4, v8, v10}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Util;->〇080(Landroid/content/Context;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 162
    .line 163
    .line 164
    sget-object p2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 165
    .line 166
    goto :goto_3

    .line 167
    :cond_8
    move-object p2, v1

    .line 168
    :goto_3
    if-nez p2, :cond_9

    .line 169
    .line 170
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 171
    .line 172
    .line 173
    move-result-object p2

    .line 174
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 175
    .line 176
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 177
    .line 178
    .line 179
    :cond_9
    if-eqz v6, :cond_a

    .line 180
    .line 181
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 182
    .line 183
    .line 184
    move-result-object p2

    .line 185
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 186
    .line 187
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 191
    .line 192
    .line 193
    move-result-object p2

    .line 194
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 195
    .line 196
    .line 197
    move-result-object v4

    .line 198
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 199
    .line 200
    const-string v5, "helper.mBinding.ivTp2SquareBottomLeft"

    .line 201
    .line 202
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getIcon()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v5

    .line 209
    new-instance v8, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$6$1;

    .line 210
    .line 211
    invoke-direct {v8, p1, v6}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$6$1;-><init>(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;)V

    .line 212
    .line 213
    .line 214
    invoke-static {p2, v4, v5, v8}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Util;->〇080(Landroid/content/Context;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 215
    .line 216
    .line 217
    sget-object p2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 218
    .line 219
    goto :goto_4

    .line 220
    :cond_a
    move-object p2, v1

    .line 221
    :goto_4
    if-nez p2, :cond_b

    .line 222
    .line 223
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 224
    .line 225
    .line 226
    move-result-object p2

    .line 227
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 228
    .line 229
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 230
    .line 231
    .line 232
    :cond_b
    if-eqz v7, :cond_c

    .line 233
    .line 234
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 235
    .line 236
    .line 237
    move-result-object p2

    .line 238
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 239
    .line 240
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 241
    .line 242
    .line 243
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 244
    .line 245
    .line 246
    move-result-object p2

    .line 247
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 248
    .line 249
    .line 250
    move-result-object v1

    .line 251
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 252
    .line 253
    const-string v2, "helper.mBinding.ivTp2SquareBottomRight"

    .line 254
    .line 255
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    invoke-virtual {v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;->getIcon()Ljava/lang/String;

    .line 259
    .line 260
    .line 261
    move-result-object v2

    .line 262
    new-instance v4, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$8$1;

    .line 263
    .line 264
    invoke-direct {v4, p1, v7}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$convert$8$1;-><init>(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/serverdata/PageCfgContentItem;)V

    .line 265
    .line 266
    .line 267
    invoke-static {p2, v1, v2, v4}, Lcom/intsig/camscanner/mainmenu/toolpagev2/util/ToolPageV2Util;->〇080(Landroid/content/Context;Landroidx/appcompat/widget/AppCompatImageView;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 268
    .line 269
    .line 270
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 271
    .line 272
    :cond_c
    if-nez v1, :cond_d

    .line 273
    .line 274
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 275
    .line 276
    .line 277
    move-result-object p1

    .line 278
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 279
    .line 280
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 281
    .line 282
    .line 283
    :cond_d
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    .line 284
    .line 285
    .line 286
    move-result p1

    .line 287
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;

    .line 288
    .line 289
    invoke-virtual {p2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 290
    .line 291
    .line 292
    move-result-object p2

    .line 293
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 294
    .line 295
    .line 296
    move-result p2

    .line 297
    sub-int/2addr p2, v9

    .line 298
    if-ne p1, p2, :cond_e

    .line 299
    .line 300
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->o〇00O:Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/ToolPageV2Adapter;

    .line 301
    .line 302
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇8oOO88()Landroidx/recyclerview/widget/RecyclerView;

    .line 303
    .line 304
    .line 305
    move-result-object p1

    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    .line 307
    .line 308
    .line 309
    move-result p1

    .line 310
    int-to-float p1, p1

    .line 311
    const/16 p2, 0x5e

    .line 312
    .line 313
    invoke-static {p2}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 314
    .line 315
    .line 316
    move-result p2

    .line 317
    sub-float/2addr p1, p2

    .line 318
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 319
    .line 320
    .line 321
    move-result-object p2

    .line 322
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 323
    .line 324
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 325
    .line 326
    .line 327
    move-result-object p2

    .line 328
    float-to-int p1, p1

    .line 329
    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 330
    .line 331
    goto :goto_5

    .line 332
    :cond_e
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider$ToolPageV2SquareHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemToolPageV2SquareBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 337
    .line 338
    new-instance p2, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    .line 339
    .line 340
    const/4 v0, -0x1

    .line 341
    const/4 v1, -0x2

    .line 342
    invoke-direct {p2, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 343
    .line 344
    .line 345
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 346
    .line 347
    .line 348
    :goto_5
    return-void
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/toolpagev2/entity/BaseToolPageV2Type;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/toolpagev2/adapter/provider/ToolPageV2SquareProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;
.super Ljava/lang/Object;
.source "TagManagerRouteUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;->〇080:Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final 〇080(Landroidx/fragment/app/FragmentActivity;[J)V
    .locals 2
    .param p0    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # [J
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docIds"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Landroid/content/Intent;

    .line 12
    .line 13
    const-class v1, Lcom/intsig/camscanner/TagSettingActivity;

    .line 14
    .line 15
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 16
    .line 17
    .line 18
    const-string v1, "key_docIds"

    .line 19
    .line 20
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final startActivity(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;->startActivity(Landroid/content/Context;Z)V

    return-void
.end method

.method public final startActivity(Landroid/content/Context;Z)V
    .locals 1

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    if-nez p2, :cond_1

    .line 2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇o0()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-ne p2, v0, :cond_2

    .line 3
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageNewActivity;

    invoke-direct {p2, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_2

    .line 4
    :cond_2
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageActivity;

    invoke-direct {p2, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5
    :goto_2
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 6
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    :goto_3
    if-nez p1, :cond_4

    const-string p1, "TagManagerRouteUtil"

    const-string p2, "startActivity but context is NULL"

    .line 7
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public final startActivityForResult(Landroidx/appcompat/app/AppCompatActivity;I)V
    .locals 2

    if-eqz p1, :cond_1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇o0()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageNewActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4
    :goto_0
    invoke-virtual {p1, v0, p2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 5
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-nez p1, :cond_2

    const-string p1, "TagManagerRouteUtil"

    const-string p2, "startActivityForResult but activity is NULL"

    .line 6
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final startActivityForResult(Landroidx/fragment/app/Fragment;Landroid/content/Context;I)V
    .locals 3

    const-string v0, "TagManagerRouteUtil"

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-eqz p2, :cond_1

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇o0()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 8
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageNewActivity;

    invoke-direct {v1, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 9
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManageActivity;

    invoke-direct {v1, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    :goto_0
    invoke-virtual {p1, v1, p3}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 11
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_1
    if-nez v1, :cond_2

    const-string p1, "startActivityForResult but context is NULL"

    .line 12
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    :cond_2
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_3
    if-nez v1, :cond_4

    const-string p1, "startActivityForResult but fragment is NULL"

    .line 14
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

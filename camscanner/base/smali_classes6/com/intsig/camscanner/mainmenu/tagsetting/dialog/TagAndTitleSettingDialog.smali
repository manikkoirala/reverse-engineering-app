.class public final Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "TagAndTitleSettingDialog.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8〇OO0〇0o:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:[Z

.field private OO:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:J

.field private final oOo0:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Ljava/lang/String;

.field private o〇00O:[Ljava/lang/String;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

.field private 〇08O〇00〇o:[J

.field private 〇0O:Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;

    .line 8
    .line 9
    const-string v0, "_id"

    .line 10
    .line 11
    const-string v1, "title"

    .line 12
    .line 13
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sput-object v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, -0x1

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 7
    .line 8
    new-instance v0, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 14
    .line 15
    const/4 v0, 0x3

    .line 16
    new-array v0, v0, [I

    .line 17
    .line 18
    sget-object v1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    const/4 v3, 0x0

    .line 25
    aput v2, v0, v3

    .line 26
    .line 27
    const/4 v2, 0x1

    .line 28
    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    aput v3, v0, v2

    .line 33
    .line 34
    const/4 v2, 0x2

    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    aput v1, v0, v2

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo0:[I

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final O0O0〇(JLjava/lang/String;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;)V
    .locals 7
    .param p3    # Landroidx/fragment/app/FragmentManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;

    .line 2
    .line 3
    move-wide v1, p0

    .line 4
    move-object v3, p2

    .line 5
    move-object v4, p3

    .line 6
    move-object v5, p4

    .line 7
    move-object v6, p5

    .line 8
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;->〇080(JLjava/lang/String;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8〇8〇O80(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 4
    .line 5
    .line 6
    array-length v0, v0

    .line 7
    new-instance v1, Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 10
    .line 11
    .line 12
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    move v3, v0

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    const-wide/16 v5, 0x0

    .line 22
    .line 23
    const/4 v7, 0x1

    .line 24
    if-eqz v4, :cond_3

    .line 25
    .line 26
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    check-cast v4, Ljava/lang/String;

    .line 31
    .line 32
    const/4 v8, 0x0

    .line 33
    const/4 v9, 0x0

    .line 34
    :goto_1
    if-ge v9, v0, :cond_2

    .line 35
    .line 36
    iget-object v10, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 37
    .line 38
    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    aget-object v10, v10, v9

    .line 42
    .line 43
    invoke-static {v4, v10}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v10

    .line 47
    if-eqz v10, :cond_1

    .line 48
    .line 49
    iget-object v7, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 50
    .line 51
    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    aget-wide v9, v7, v9

    .line 55
    .line 56
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    const/4 v7, 0x0

    .line 64
    goto :goto_2

    .line 65
    :cond_1
    add-int/lit8 v9, v9, 0x1

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_2
    :goto_2
    if-eqz v7, :cond_0

    .line 69
    .line 70
    invoke-direct {p0, v4, v3}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0oO〇oo00(Ljava/lang/String;I)J

    .line 71
    .line 72
    .line 73
    move-result-wide v7

    .line 74
    cmp-long v4, v7, v5

    .line 75
    .line 76
    if-lez v4, :cond_0

    .line 77
    .line 78
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 79
    .line 80
    .line 81
    move-result-object v4

    .line 82
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    add-int/lit8 v3, v3, 0x1

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    new-instance v2, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    const-string v3, "savaTags currentTagIs.size = "

    .line 98
    .line 99
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 103
    .line 104
    .line 105
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    const-string v2, "TagSettingDialog"

    .line 110
    .line 111
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-eqz v0, :cond_a

    .line 123
    .line 124
    invoke-static {p2, v1}, Lcom/intsig/camscanner/app/DBUtil;->〇8O0O808〇(Ljava/util/ArrayList;Ljava/util/List;)V

    .line 125
    .line 126
    .line 127
    new-instance v0, Ljava/util/ArrayList;

    .line 128
    .line 129
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .line 131
    .line 132
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 133
    .line 134
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 135
    .line 136
    .line 137
    move-result-object v3

    .line 138
    :cond_4
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 139
    .line 140
    .line 141
    move-result v4

    .line 142
    if-eqz v4, :cond_5

    .line 143
    .line 144
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 145
    .line 146
    .line 147
    move-result-object v4

    .line 148
    check-cast v4, Ljava/lang/String;

    .line 149
    .line 150
    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 151
    .line 152
    .line 153
    move-result v8

    .line 154
    if-nez v8, :cond_4

    .line 155
    .line 156
    sget-object v8, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 157
    .line 158
    invoke-virtual {v8}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 159
    .line 160
    .line 161
    move-result-object v8

    .line 162
    const-string v9, "rawTag"

    .line 163
    .line 164
    invoke-static {v4, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    invoke-static {v8, v4}, Lcom/intsig/camscanner/db/dao/TagDao;->〇o〇(Landroid/content/Context;Ljava/lang/String;)J

    .line 168
    .line 169
    .line 170
    move-result-wide v8

    .line 171
    cmp-long v4, v8, v5

    .line 172
    .line 173
    if-ltz v4, :cond_4

    .line 174
    .line 175
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 176
    .line 177
    .line 178
    move-result-object v4

    .line 179
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    .line 181
    .line 182
    goto :goto_3

    .line 183
    :cond_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 188
    .line 189
    .line 190
    move-result v3

    .line 191
    if-eqz v3, :cond_7

    .line 192
    .line 193
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 194
    .line 195
    .line 196
    move-result-object v3

    .line 197
    check-cast v3, Ljava/lang/Long;

    .line 198
    .line 199
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 200
    .line 201
    .line 202
    move-result-object v4

    .line 203
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 204
    .line 205
    .line 206
    move-result v5

    .line 207
    if-eqz v5, :cond_6

    .line 208
    .line 209
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 210
    .line 211
    .line 212
    move-result-object v5

    .line 213
    check-cast v5, Ljava/lang/Number;

    .line 214
    .line 215
    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    .line 216
    .line 217
    .line 218
    move-result-wide v5

    .line 219
    const-string v8, "dId"

    .line 220
    .line 221
    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    .line 225
    .line 226
    .line 227
    move-result-wide v8

    .line 228
    invoke-static {v8, v9, v5, v6}, Lcom/intsig/camscanner/util/Util;->〇〇8O0〇8(JJ)V

    .line 229
    .line 230
    .line 231
    goto :goto_4

    .line 232
    :cond_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 233
    .line 234
    .line 235
    move-result p1

    .line 236
    if-gtz p1, :cond_9

    .line 237
    .line 238
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 239
    .line 240
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 241
    .line 242
    .line 243
    move-result p1

    .line 244
    if-eqz p1, :cond_8

    .line 245
    .line 246
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 247
    .line 248
    .line 249
    move-result p1

    .line 250
    if-nez p1, :cond_9

    .line 251
    .line 252
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 253
    .line 254
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 255
    .line 256
    .line 257
    move-result p1

    .line 258
    xor-int/2addr p1, v7

    .line 259
    if-eqz p1, :cond_b

    .line 260
    .line 261
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 262
    .line 263
    .line 264
    move-result p1

    .line 265
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 266
    .line 267
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 268
    .line 269
    .line 270
    move-result v0

    .line 271
    if-le p1, v0, :cond_b

    .line 272
    .line 273
    :cond_9
    const-string p1, "needsync as tags change"

    .line 274
    .line 275
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    sget-object p1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 279
    .line 280
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    const/4 v0, 0x3

    .line 285
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 286
    .line 287
    .line 288
    goto :goto_5

    .line 289
    :cond_a
    sget-object p1, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 290
    .line 291
    invoke-virtual {p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->o〇0()Lcom/intsig/camscanner/launch/CsApplication;

    .line 292
    .line 293
    .line 294
    move-result-object p1

    .line 295
    invoke-static {p1, p2, v1}, Lcom/intsig/camscanner/app/DBUtil;->O8888(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/List;)V

    .line 296
    .line 297
    .line 298
    :cond_b
    :goto_5
    return-void
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final Ooo8o()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 6
    .line 7
    new-instance v2, L〇o0O/Oo08;

    .line 8
    .line 9
    invoke-direct {v2, p0}, L〇o0O/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 13
    .line 14
    .line 15
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 16
    .line 17
    new-instance v2, L〇o0O/o〇0;

    .line 18
    .line 19
    invoke-direct {v2, v0}, L〇o0O/o〇0;-><init>(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 30
    .line 31
    invoke-static {v1, v2}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 35
    .line 36
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 44
    .line 45
    const/4 v3, 0x0

    .line 46
    if-eqz v2, :cond_0

    .line 47
    .line 48
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    goto :goto_0

    .line 53
    :cond_0
    const/4 v2, 0x0

    .line 54
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 55
    .line 56
    .line 57
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 58
    .line 59
    const-string v2, "ivClear"

    .line 60
    .line 61
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 65
    .line 66
    const/4 v4, 0x1

    .line 67
    if-eqz v2, :cond_1

    .line 68
    .line 69
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    if-nez v2, :cond_2

    .line 74
    .line 75
    :cond_1
    const/4 v3, 0x1

    .line 76
    :cond_2
    xor-int/lit8 v2, v3, 0x1

    .line 77
    .line 78
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 79
    .line 80
    .line 81
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 82
    .line 83
    new-instance v2, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$initDocTitleView$1$3;

    .line 84
    .line 85
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$initDocTitleView$1$3;-><init>(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 89
    .line 90
    .line 91
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 92
    .line 93
    new-instance v2, L〇o0O/〇〇888;

    .line 94
    .line 95
    invoke-direct {v2, v0, p0}, L〇o0O/〇〇888;-><init>(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 99
    .line 100
    .line 101
    :cond_3
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇oo(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final o88(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$renameDialogEdit"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$ocrTitleCloud"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object p3, Lcom/intsig/camscanner/app/ArrayUtils;->〇080:Lcom/intsig/camscanner/app/ArrayUtils;

    .line 17
    .line 18
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo0:[I

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->OCR_TITLE_CLOUD:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x1

    .line 27
    invoke-virtual {p3, p0, v2, v1}, Lcom/intsig/camscanner/app/ArrayUtils;->〇080([III)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    invoke-virtual {p1, p0}, Landroid/widget/EditText;->setSelection(I)V

    .line 38
    .line 39
    .line 40
    sget-object p0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇oOO8O8(I)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇o08(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOoO8OO〇()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v1, v2

    .line 14
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 22
    .line 23
    if-eqz v1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/control/TagContainerController;->OoO8()Landroid/widget/AutoCompleteTextView;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    :cond_1
    invoke-static {v0, v2}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇8O0880(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇〇〇00(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0O:Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇O8OO()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sget-object v1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 22
    .line 23
    if-nez v2, :cond_0

    .line 24
    .line 25
    const-string v2, ""

    .line 26
    .line 27
    :cond_0
    move-object v3, v2

    .line 28
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo0:[I

    .line 29
    .line 30
    iget-wide v5, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 31
    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 33
    .line 34
    .line 35
    move-result-object v7

    .line 36
    move-object v2, v0

    .line 37
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->oo〇(Ljava/lang/String;Ljava/lang/String;[IJLandroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-nez v1, :cond_2

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    if-eqz v1, :cond_1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    iget-wide v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 60
    .line 61
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    instance-of v1, v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 70
    .line 71
    if-eqz v1, :cond_3

    .line 72
    .line 73
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 74
    .line 75
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    const-string v4, "null cannot be cast to non-null type androidx.appcompat.app.AppCompatActivity"

    .line 80
    .line 81
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    check-cast v2, Landroidx/appcompat/app/AppCompatActivity;

    .line 85
    .line 86
    const/4 v5, 0x0

    .line 87
    new-instance v6, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$checkNeedRename$1$1;

    .line 88
    .line 89
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$checkNeedRename$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 90
    .line 91
    .line 92
    new-instance v7, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$checkNeedRename$1$2;

    .line 93
    .line 94
    invoke-direct {v7, p0, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$checkNeedRename$1$2;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    move-object v4, v0

    .line 98
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 99
    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOoO8OO〇()V

    .line 103
    .line 104
    .line 105
    :cond_3
    :goto_1
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final o〇oo(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$renameDialogEdit"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "$ocrTitleLocal"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object p3, Lcom/intsig/camscanner/app/ArrayUtils;->〇080:Lcom/intsig/camscanner/app/ArrayUtils;

    .line 17
    .line 18
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo0:[I

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->OCR_TITLE_LOCAL:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x1

    .line 27
    invoke-virtual {p3, p0, v2, v1}, Lcom/intsig/camscanner/app/ArrayUtils;->〇080([III)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 34
    .line 35
    .line 36
    move-result p0

    .line 37
    invoke-virtual {p1, p0}, Landroid/widget/EditText;->setSelection(I)V

    .line 38
    .line 39
    .line 40
    sget-object p0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇oOO8O8(I)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇08O(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇O8〇8000()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0oO〇oo00(Ljava/lang/String;I)J
    .locals 6

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "title"

    .line 7
    .line 8
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v1, "tag_num"

    .line 12
    .line 13
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 18
    .line 19
    .line 20
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 21
    .line 22
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v3, 0x1

    .line 33
    new-array v4, v3, [Ljava/lang/String;

    .line 34
    .line 35
    const/4 v5, 0x0

    .line 36
    aput-object p1, v4, v5

    .line 37
    .line 38
    const-string p1, "title=?"

    .line 39
    .line 40
    invoke-virtual {v1, v2, v0, p1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-ge p1, v3, :cond_0

    .line 45
    .line 46
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 51
    .line 52
    invoke-virtual {p1, p2, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    if-eqz p1, :cond_0

    .line 57
    .line 58
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 59
    .line 60
    .line 61
    move-result-wide p1

    .line 62
    goto :goto_0

    .line 63
    :cond_0
    const-wide/16 p1, -0x1

    .line 64
    .line 65
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 66
    .line 67
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 68
    .line 69
    .line 70
    const-string v1, "insertTag resultId:"

    .line 71
    .line 72
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    const-string v1, "TagSettingDialog"

    .line 83
    .line 84
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    return-wide p1
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇0ooOOo()V
    .locals 13

    .line 1
    const-string v0, "TagSettingDialog"

    .line 2
    .line 3
    const-string v1, "getOneDocumentTag"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 9
    .line 10
    const-wide/16 v2, 0x0

    .line 11
    .line 12
    cmp-long v4, v0, v2

    .line 13
    .line 14
    if-gtz v4, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 20
    .line 21
    .line 22
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget-wide v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 29
    .line 30
    const/4 v4, 0x0

    .line 31
    const/4 v5, 0x4

    .line 32
    const/4 v6, 0x0

    .line 33
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/db/dao/MTagDao;->〇o00〇〇Oo(Landroid/content/Context;JZILjava/lang/Object;)Ljava/util/ArrayList;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    const/4 v2, 0x1

    .line 42
    xor-int/2addr v1, v2

    .line 43
    if-eqz v1, :cond_8

    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_8

    .line 54
    .line 55
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Ljava/lang/Long;

    .line 60
    .line 61
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 62
    .line 63
    if-eqz v3, :cond_1

    .line 64
    .line 65
    array-length v4, v3

    .line 66
    const/4 v5, 0x0

    .line 67
    const/4 v6, 0x0

    .line 68
    const/4 v7, 0x0

    .line 69
    :goto_0
    if-ge v6, v4, :cond_1

    .line 70
    .line 71
    aget-wide v8, v3, v6

    .line 72
    .line 73
    add-int/lit8 v8, v7, 0x1

    .line 74
    .line 75
    iget-object v9, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 76
    .line 77
    const/4 v10, 0x0

    .line 78
    if-eqz v9, :cond_2

    .line 79
    .line 80
    aget-wide v11, v9, v7

    .line 81
    .line 82
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 83
    .line 84
    .line 85
    move-result-object v9

    .line 86
    goto :goto_1

    .line 87
    :cond_2
    move-object v9, v10

    .line 88
    :goto_1
    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result v9

    .line 92
    if-eqz v9, :cond_7

    .line 93
    .line 94
    iget-object v9, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8o08O8O:[Z

    .line 95
    .line 96
    if-eqz v9, :cond_3

    .line 97
    .line 98
    aput-boolean v2, v9, v7

    .line 99
    .line 100
    :cond_3
    iget-object v9, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 101
    .line 102
    if-eqz v9, :cond_4

    .line 103
    .line 104
    aget-object v10, v9, v7

    .line 105
    .line 106
    :cond_4
    if-eqz v10, :cond_6

    .line 107
    .line 108
    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    .line 109
    .line 110
    .line 111
    move-result v7

    .line 112
    if-nez v7, :cond_5

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_5
    const/4 v7, 0x0

    .line 116
    goto :goto_3

    .line 117
    :cond_6
    :goto_2
    const/4 v7, 0x1

    .line 118
    :goto_3
    if-nez v7, :cond_7

    .line 119
    .line 120
    iget-object v7, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 121
    .line 122
    invoke-static {v7, v10}, Lkotlin/collections/CollectionsKt;->o8(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    move-result v7

    .line 126
    if-nez v7, :cond_7

    .line 127
    .line 128
    iget-object v7, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 129
    .line 130
    invoke-static {v10}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    .line 135
    .line 136
    :cond_7
    add-int/lit8 v6, v6, 0x1

    .line 137
    .line 138
    move v7, v8

    .line 139
    goto :goto_0

    .line 140
    :cond_8
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0〇0()Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_3

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/TagContainerController;->〇O888o0o()Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const-string v2, "selectTags"

    .line 11
    .line 12
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    move-object v2, v0

    .line 16
    check-cast v2, Ljava/util/Collection;

    .line 17
    .line 18
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    const/4 v3, 0x1

    .line 23
    xor-int/2addr v2, v3

    .line 24
    if-eqz v2, :cond_2

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    .line 29
    .line 30
    .line 31
    move-result v4

    .line 32
    xor-int/2addr v4, v3

    .line 33
    if-eqz v4, :cond_1

    .line 34
    .line 35
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    if-ne v4, v5, :cond_1

    .line 44
    .line 45
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    const/4 v5, 0x0

    .line 50
    :goto_0
    if-ge v5, v4, :cond_3

    .line 51
    .line 52
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v6

    .line 56
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    if-nez v6, :cond_0

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_1
    :goto_1
    const/4 v1, 0x1

    .line 71
    goto :goto_2

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 73
    .line 74
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 75
    .line 76
    .line 77
    move-result v0

    .line 78
    xor-int/2addr v0, v3

    .line 79
    if-eqz v0, :cond_3

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_3
    :goto_2
    return v1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o88(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final 〇8O0880(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/content/DialogInterface;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    invoke-virtual {p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setCanceledOnTouchOutside(Z)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    const/16 v1, 0x30

    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 29
    .line 30
    .line 31
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string v0, "dialog.behavior"

    .line 36
    .line 37
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setDraggable(Z)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    if-eqz v0, :cond_3

    .line 49
    .line 50
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    const/16 v2, 0x44

    .line 55
    .line 56
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 57
    .line 58
    .line 59
    move-result v2

    .line 60
    sub-int/2addr v1, v2

    .line 61
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    invoke-static {v0}, Lcom/intsig/utils/SystemUiUtil;->〇o〇(Landroid/content/Context;)I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    sub-int/2addr v2, v0

    .line 70
    invoke-virtual {p1, v2}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setPeekHeight(I)V

    .line 71
    .line 72
    .line 73
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 74
    .line 75
    if-eqz p0, :cond_1

    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->〇080()Lcom/intsig/camscanner/view/KeyboardListenerLayout;

    .line 78
    .line 79
    .line 80
    move-result-object p0

    .line 81
    if-eqz p0, :cond_1

    .line 82
    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 84
    .line 85
    .line 86
    move-result-object p0

    .line 87
    goto :goto_0

    .line 88
    :cond_1
    const/4 p0, 0x0

    .line 89
    :goto_0
    if-nez p0, :cond_2

    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_2
    iput v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    .line 94
    :cond_3
    :goto_1
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0O:Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O0o〇〇o(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "TagSettingDialog"

    .line 2
    .line 3
    const-string v1, "savaTags"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 9
    .line 10
    const-wide/16 v2, 0x0

    .line 11
    .line 12
    cmp-long v4, v0, v2

    .line 13
    .line 14
    if-gtz v4, :cond_0

    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 20
    .line 21
    .line 22
    iget-wide v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 23
    .line 24
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 32
    .line 33
    if-eqz v1, :cond_3

    .line 34
    .line 35
    const/4 v2, 0x0

    .line 36
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/control/TagContainerController;->〇8o8o〇(Z)V

    .line 37
    .line 38
    .line 39
    move-object v1, p1

    .line 40
    check-cast v1, Ljava/util/Collection;

    .line 41
    .line 42
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    const/4 v3, 0x1

    .line 47
    xor-int/2addr v1, v3

    .line 48
    if-eqz v1, :cond_1

    .line 49
    .line 50
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8〇8〇O80(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO:Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 57
    .line 58
    .line 59
    move-result p1

    .line 60
    xor-int/2addr p1, v3

    .line 61
    if-eqz p1, :cond_2

    .line 62
    .line 63
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 74
    .line 75
    new-array v4, v3, [Ljava/lang/String;

    .line 76
    .line 77
    iget-wide v5, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 78
    .line 79
    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v5

    .line 83
    aput-object v5, v4, v2

    .line 84
    .line 85
    const-string v2, "document_id=?"

    .line 86
    .line 87
    invoke-virtual {p1, v1, v2, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 88
    .line 89
    .line 90
    const/4 v2, 0x1

    .line 91
    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 92
    .line 93
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 94
    .line 95
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    const/4 v1, 0x3

    .line 100
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 101
    .line 102
    .line 103
    :cond_3
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇〇〇0(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8〇8000()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const-string v0, "name"

    .line 8
    .line 9
    const-string v1, ""

    .line 10
    .line 11
    const-string v2, "CSTitleAndLabel"

    .line 12
    .line 13
    const-string v3, "new_label"

    .line 14
    .line 15
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/TagContainerController;->〇O888o0o()Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "it.selectTags"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇O0o〇〇o(Ljava/util/List;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0O:Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;

    .line 35
    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;->〇o00〇〇Oo()V

    .line 39
    .line 40
    .line 41
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇O8OO()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇Oo〇O()V
    .locals 12

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmp-long v4, v0, v2

    .line 6
    .line 7
    if-gtz v4, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_10

    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/app/ArrayUtils;->〇080:Lcom/intsig/camscanner/app/ArrayUtils;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo0:[I

    .line 19
    .line 20
    iget-wide v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 21
    .line 22
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇0000OOO(Landroid/content/Context;J)I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    const/4 v4, 0x0

    .line 27
    invoke-virtual {v1, v2, v4, v3}, Lcom/intsig/camscanner/app/ArrayUtils;->〇080([III)V

    .line 28
    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 31
    .line 32
    if-eqz v1, :cond_10

    .line 33
    .line 34
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o〇00O:Lcom/intsig/view/FlowLayout;

    .line 35
    .line 36
    if-nez v1, :cond_1

    .line 37
    .line 38
    goto/16 :goto_4

    .line 39
    .line 40
    :cond_1
    const-string v2, "mBinding?.flOcrTitleContainer ?: return"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 46
    .line 47
    if-eqz v2, :cond_10

    .line 48
    .line 49
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 50
    .line 51
    if-nez v2, :cond_2

    .line 52
    .line 53
    goto/16 :goto_4

    .line 54
    .line 55
    :cond_2
    const-string v3, "mBinding?.llOcrTitle ?: return"

    .line 56
    .line 57
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 61
    .line 62
    if-eqz v3, :cond_10

    .line 63
    .line 64
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 65
    .line 66
    if-nez v3, :cond_3

    .line 67
    .line 68
    goto/16 :goto_4

    .line 69
    .line 70
    :cond_3
    const-string v5, "mBinding?.tvEditDocTitle ?: return"

    .line 71
    .line 72
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    sget-object v5, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 76
    .line 77
    iget-wide v6, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 78
    .line 79
    invoke-virtual {v5, v0, v6, v7}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇80〇808〇O(Landroid/content/Context;J)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    iget-wide v7, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 84
    .line 85
    invoke-virtual {v5, v0, v7, v8}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->oO80(Landroid/content/Context;J)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 90
    .line 91
    .line 92
    move-result v7

    .line 93
    if-eqz v7, :cond_4

    .line 94
    .line 95
    const-string v0, ""

    .line 96
    .line 97
    :cond_4
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 98
    .line 99
    .line 100
    move-result v7

    .line 101
    const/4 v8, 0x1

    .line 102
    if-nez v7, :cond_5

    .line 103
    .line 104
    invoke-virtual {v5}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇O〇()Z

    .line 105
    .line 106
    .line 107
    move-result v7

    .line 108
    if-eqz v7, :cond_5

    .line 109
    .line 110
    const/4 v7, 0x1

    .line 111
    goto :goto_0

    .line 112
    :cond_5
    const/4 v7, 0x0

    .line 113
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 114
    .line 115
    .line 116
    move-result v9

    .line 117
    if-nez v9, :cond_6

    .line 118
    .line 119
    invoke-virtual {v5}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->OO0o〇〇()Z

    .line 120
    .line 121
    .line 122
    move-result v5

    .line 123
    if-eqz v5, :cond_6

    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_6
    const/4 v8, 0x0

    .line 127
    :goto_1
    if-nez v7, :cond_7

    .line 128
    .line 129
    if-eqz v8, :cond_8

    .line 130
    .line 131
    :cond_7
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 132
    .line 133
    .line 134
    :cond_8
    const v2, 0x7f0d03f8

    .line 135
    .line 136
    .line 137
    const/4 v5, 0x0

    .line 138
    if-eqz v7, :cond_d

    .line 139
    .line 140
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 141
    .line 142
    .line 143
    move-result-object v7

    .line 144
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 145
    .line 146
    .line 147
    move-result-object v7

    .line 148
    invoke-virtual {v7, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 149
    .line 150
    .line 151
    move-result-object v7

    .line 152
    instance-of v9, v7, Landroid/widget/TextView;

    .line 153
    .line 154
    if-eqz v9, :cond_9

    .line 155
    .line 156
    check-cast v7, Landroid/widget/TextView;

    .line 157
    .line 158
    goto :goto_2

    .line 159
    :cond_9
    move-object v7, v5

    .line 160
    :goto_2
    if-nez v7, :cond_a

    .line 161
    .line 162
    return-void

    .line 163
    :cond_a
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 164
    .line 165
    .line 166
    move-result-object v9

    .line 167
    instance-of v10, v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 168
    .line 169
    if-eqz v10, :cond_b

    .line 170
    .line 171
    check-cast v9, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 172
    .line 173
    goto :goto_3

    .line 174
    :cond_b
    move-object v9, v5

    .line 175
    :goto_3
    if-nez v9, :cond_c

    .line 176
    .line 177
    return-void

    .line 178
    :cond_c
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 179
    .line 180
    .line 181
    move-result-object v10

    .line 182
    const/16 v11, 0x8

    .line 183
    .line 184
    invoke-static {v10, v11}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 185
    .line 186
    .line 187
    move-result v10

    .line 188
    invoke-virtual {v9, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 189
    .line 190
    .line 191
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    .line 193
    .line 194
    invoke-virtual {v7, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    .line 196
    .line 197
    new-instance v9, L〇o0O/〇o〇;

    .line 198
    .line 199
    invoke-direct {v9, p0, v3, v6}, L〇o0O/〇o〇;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    invoke-virtual {v7, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v1, v7}, Lcom/intsig/view/FlowLayout;->addView(Landroid/view/View;)V

    .line 206
    .line 207
    .line 208
    :cond_d
    if-eqz v8, :cond_10

    .line 209
    .line 210
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 211
    .line 212
    .line 213
    move-result-object v6

    .line 214
    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 215
    .line 216
    .line 217
    move-result-object v6

    .line 218
    invoke-virtual {v6, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 219
    .line 220
    .line 221
    move-result-object v2

    .line 222
    instance-of v4, v2, Landroid/widget/TextView;

    .line 223
    .line 224
    if-eqz v4, :cond_e

    .line 225
    .line 226
    move-object v5, v2

    .line 227
    check-cast v5, Landroid/widget/TextView;

    .line 228
    .line 229
    :cond_e
    if-nez v5, :cond_f

    .line 230
    .line 231
    return-void

    .line 232
    :cond_f
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    .line 234
    .line 235
    new-instance v2, L〇o0O/O8;

    .line 236
    .line 237
    invoke-direct {v2, p0, v3, v0}, L〇o0O/O8;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    .line 242
    .line 243
    invoke-virtual {v1, v5}, Lcom/intsig/view/FlowLayout;->addView(Landroid/view/View;)V

    .line 244
    .line 245
    .line 246
    nop

    .line 247
    :cond_10
    :goto_4
    return-void
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final 〇o08(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "$this_apply"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 7
    .line 8
    const-string p1, ""

    .line 9
    .line 10
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)Lcom/intsig/camscanner/control/TagContainerController;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇O80〇0o()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Tag;->〇080:Landroid/net/Uri;

    .line 12
    .line 13
    sget-object v3, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 14
    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x0

    .line 17
    const-string v6, "upper(title_pinyin) ASC"

    .line 18
    .line 19
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const/4 v1, 0x0

    .line 24
    if-eqz v0, :cond_4

    .line 25
    .line 26
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    new-array v3, v2, [J

    .line 31
    .line 32
    iput-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 33
    .line 34
    new-array v3, v2, [Ljava/lang/String;

    .line 35
    .line 36
    iput-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 37
    .line 38
    new-array v2, v2, [Z

    .line 39
    .line 40
    iput-object v2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8o08O8O:[Z

    .line 41
    .line 42
    const/4 v2, 0x0

    .line 43
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 44
    .line 45
    .line 46
    move-result v3

    .line 47
    if-eqz v3, :cond_3

    .line 48
    .line 49
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 50
    .line 51
    if-eqz v3, :cond_0

    .line 52
    .line 53
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 54
    .line 55
    .line 56
    move-result-wide v4

    .line 57
    aput-wide v4, v3, v2

    .line 58
    .line 59
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 60
    .line 61
    if-eqz v3, :cond_1

    .line 62
    .line 63
    const/4 v4, 0x1

    .line 64
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    aput-object v4, v3, v2

    .line 69
    .line 70
    :cond_1
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8o08O8O:[Z

    .line 71
    .line 72
    if-eqz v3, :cond_2

    .line 73
    .line 74
    aput-boolean v1, v3, v2

    .line 75
    .line 76
    :cond_2
    add-int/lit8 v2, v2, 0x1

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_4
    new-array v0, v1, [J

    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇08O〇00〇o:[J

    .line 86
    .line 87
    new-array v0, v1, [Ljava/lang/String;

    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 90
    .line 91
    new-array v0, v1, [Z

    .line 92
    .line 93
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8o08O8O:[Z

    .line 94
    .line 95
    :goto_1
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOoO8OO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Landroid/content/Intent;

    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-class v1, Lcom/intsig/camscanner/settings/DocNameSettingActivity;

    .line 13
    .line 14
    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    .line 16
    .line 17
    const-string v0, "extra_from_template_settings"

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;Landroid/view/View;Z)V
    .locals 0

    .line 1
    const-string p2, "$this_apply"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    if-nez p3, :cond_1

    .line 12
    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 14
    .line 15
    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    invoke-static {p2}, Lkotlin/text/StringsKt;->O0oO008(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 32
    .line 33
    .line 34
    move-result p2

    .line 35
    if-nez p2, :cond_0

    .line 36
    .line 37
    const/4 p2, 0x1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 p2, 0x0

    .line 40
    :goto_0
    if-eqz p2, :cond_1

    .line 41
    .line 42
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->o8〇OO0〇0o:Landroid/widget/AutoCompleteTextView;

    .line 43
    .line 44
    iget-object p1, p1, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 45
    .line 46
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    const v0, 0x7f140192

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-wide/16 v0, -0x1

    .line 16
    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    const-string v2, "extra_doc_id"

    .line 20
    .line 21
    invoke-virtual {p1, v2, v0, v1}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;J)J

    .line 22
    .line 23
    .line 24
    move-result-wide v0

    .line 25
    :cond_0
    iput-wide v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o0:J

    .line 26
    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    if-eqz p1, :cond_1

    .line 32
    .line 33
    const-string v0, "extra_doc_name"

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    goto :goto_0

    .line 40
    :cond_1
    const/4 p1, 0x0

    .line 41
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->oOo〇8o008:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d023d

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    const/4 p1, 0x4

    .line 2
    const/4 p3, 0x0

    .line 3
    if-ne p2, p1, :cond_1

    .line 4
    .line 5
    const-string p1, "TagSettingDialog"

    .line 6
    .line 7
    const-string p2, "KEYCODE_BACK"

    .line 8
    .line 9
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    const/4 p2, 0x1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    invoke-static {p1}, Lcom/intsig/utils/KeyboardUtils;->〇80〇808〇O(Landroid/app/Activity;)Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-ne p1, p2, :cond_0

    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_0
    const/4 p1, 0x0

    .line 28
    :goto_0
    if-eqz p1, :cond_1

    .line 29
    .line 30
    return p2

    .line 31
    :cond_1
    return p3
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    const-string p1, "CSTitleAndLabel"

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/log/LogAgentHelper;->〇0000OOO(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    new-instance p2, L〇o0O/〇080;

    .line 21
    .line 22
    invoke-direct {p2, p0}, L〇o0O/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 26
    .line 27
    .line 28
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇〇O80〇0o()V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇0ooOOo()V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;

    .line 35
    .line 36
    if-eqz p1, :cond_3

    .line 37
    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    const/4 v0, 0x1

    .line 43
    if-eqz p2, :cond_1

    .line 44
    .line 45
    new-instance v1, Lcom/intsig/camscanner/control/TagContainerController;

    .line 46
    .line 47
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->OO:Lcom/intsig/view/FlowLayout;

    .line 48
    .line 49
    invoke-direct {v1, p2, v2, v0}, Lcom/intsig/camscanner/control/TagContainerController;-><init>(Landroid/content/Context;Lcom/intsig/view/FlowLayout;Z)V

    .line 50
    .line 51
    .line 52
    iput-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 53
    .line 54
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    const v3, 0x7f060207

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    const v3, 0x7f0811b9

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/control/TagContainerController;->〇o(II)V

    .line 69
    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 72
    .line 73
    if-eqz v1, :cond_1

    .line 74
    .line 75
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 76
    .line 77
    .line 78
    move-result-object p2

    .line 79
    const v2, 0x7f0601ee

    .line 80
    .line 81
    .line 82
    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 83
    .line 84
    .line 85
    move-result p2

    .line 86
    const v2, 0x7f081091

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, p2, v2}, Lcom/intsig/camscanner/control/TagContainerController;->o8(II)V

    .line 90
    .line 91
    .line 92
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇Oo〇O()V

    .line 93
    .line 94
    .line 95
    iget-object p2, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->〇080OO8〇0:Lcom/intsig/camscanner/control/TagContainerController;

    .line 96
    .line 97
    if-eqz p2, :cond_2

    .line 98
    .line 99
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/control/TagContainerController;->o0ooO(Z)V

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O8o08O8O:[Z

    .line 105
    .line 106
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/control/TagContainerController;->〇O8o08O([Ljava/lang/String;[Z)V

    .line 107
    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->o〇00O:[Ljava/lang/String;

    .line 110
    .line 111
    const v1, 0x7f0a0611

    .line 112
    .line 113
    .line 114
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/control/TagContainerController;->O〇8O8〇008([Ljava/lang/String;I)V

    .line 115
    .line 116
    .line 117
    const/4 v0, 0x0

    .line 118
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/control/TagContainerController;->〇00〇8(Z)V

    .line 119
    .line 120
    .line 121
    new-instance v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$onViewCreated$2$2$1;

    .line 122
    .line 123
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$onViewCreated$2$2$1;-><init>(Lcom/intsig/camscanner/control/TagContainerController;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/control/TagContainerController;->o〇8(Lcom/intsig/camscanner/control/TagContainerController$TagEventCallback;)V

    .line 127
    .line 128
    .line 129
    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 130
    .line 131
    .line 132
    move-result-object p2

    .line 133
    new-instance v0, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$onViewCreated$2$3;

    .line 134
    .line 135
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$onViewCreated$2$3;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 136
    .line 137
    .line 138
    invoke-static {p2, v0}, Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener;->〇o〇(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V

    .line 139
    .line 140
    .line 141
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogTagSettingBinding;->oOo0:Landroid/widget/TextView;

    .line 142
    .line 143
    new-instance p2, L〇o0O/〇o00〇〇Oo;

    .line 144
    .line 145
    invoke-direct {p2, p0}, L〇o0O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->Ooo8o()V

    .line 152
    .line 153
    .line 154
    :cond_3
    return-void
.end method

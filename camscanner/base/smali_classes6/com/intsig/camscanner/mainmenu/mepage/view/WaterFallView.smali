.class public final Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;
.super Landroid/widget/FrameLayout;
.source "WaterFallView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

.field private OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

.field private o0:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/view/CloseCountDownView;

.field private final 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/advertisement/view/AdTagTextView;

.field private 〇0O:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private 〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p2, "WaterFallView"

    .line 2
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o0:Landroid/content/Context;

    const/4 p1, 0x0

    .line 4
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇80〇808〇O()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "ctx"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p2, "WaterFallView"

    .line 7
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o0:Landroid/content/Context;

    const/4 p1, 0x0

    .line 9
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setWillNotDraw(Z)V

    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇80〇808〇O()V

    return-void
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final Oo08()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 4
    .line 5
    const-string v2, "mData"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    move-object v1, v3

    .line 14
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    new-instance v4, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v5, "on show "

    .line 24
    .line 25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sget-object v1, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->MAIN_ME_WATERFALL:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 43
    .line 44
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 49
    .line 50
    if-nez v4, :cond_1

    .line 51
    .line 52
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    move-object v4, v3

    .line 56
    :cond_1
    invoke-virtual {v4}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdCommonBean;->getId()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v0, v1, v2}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇00(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 65
    .line 66
    if-nez v1, :cond_2

    .line 67
    .line 68
    const-string v1, "mediaView"

    .line 69
    .line 70
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    move-object v1, v3

    .line 74
    :cond_2
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView$addClickEvent$1;

    .line 75
    .line 76
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView$addClickEvent$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Lcom/intsig/advertisement/record/operation/AdIdRecord;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setAdClickListener(Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/CsAdListener;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 83
    .line 84
    if-nez v0, :cond_3

    .line 85
    .line 86
    const-string v0, "mRootView"

    .line 87
    .line 88
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    move-object v0, v3

    .line 92
    :cond_3
    new-instance v1, L〇〇08O/〇080;

    .line 93
    .line 94
    invoke-direct {v1, p0}, L〇〇08O/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o〇00O:Lcom/intsig/view/CloseCountDownView;

    .line 101
    .line 102
    if-nez v0, :cond_4

    .line 103
    .line 104
    const-string v0, "mCloseBtnView"

    .line 105
    .line 106
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_4
    move-object v3, v0

    .line 111
    :goto_0
    new-instance v0, L〇〇08O/〇o00〇〇Oo;

    .line 112
    .line 113
    invoke-direct {v0, p0}, L〇〇08O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final o〇0(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "on click root for close"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO0o〇〇〇〇0()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o〇0(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d05d6

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    const v0, 0x7f0a045f

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "findViewById(R.id.cl_waterfall_root)"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 28
    .line 29
    const v0, 0x7f0a19c9

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "findViewById(R.id.v_media)"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 44
    .line 45
    const v0, 0x7f0a1998

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    const-string v1, "findViewById(R.id.v_close)"

    .line 53
    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    check-cast v0, Lcom/intsig/view/CloseCountDownView;

    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o〇00O:Lcom/intsig/view/CloseCountDownView;

    .line 60
    .line 61
    const v0, 0x7f0a0089

    .line 62
    .line 63
    .line 64
    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    const-string v1, "findViewById(R.id.ad_tag)"

    .line 69
    .line 70
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    check-cast v0, Lcom/intsig/advertisement/view/AdTagTextView;

    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇08O〇00〇o:Lcom/intsig/advertisement/view/AdTagTextView;

    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇〇888(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;)Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇888(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "on click btn close"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO0o〇〇〇〇0()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final OO0o〇〇〇〇0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o〇00O:Lcom/intsig/view/CloseCountDownView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mCloseBtnView"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/view/CloseCountDownView;->oO80()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇0O:Lkotlin/jvm/functions/Function0;

    .line 15
    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    :cond_1
    return-void
.end method

.method public final getCallBackClose()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇0O:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO0o〇〇〇〇0()V

    .line 4
    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇08O〇00〇o:Lcom/intsig/advertisement/view/AdTagTextView;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    if-nez p1, :cond_1

    .line 13
    .line 14
    const-string p1, "mAdTagView"

    .line 15
    .line 16
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    move-object p1, v0

    .line 20
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 21
    .line 22
    const-string v2, "mData"

    .line 23
    .line 24
    if-nez v1, :cond_2

    .line 25
    .line 26
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    move-object v1, v0

    .line 30
    :cond_2
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getShow_icon()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    const/4 v3, 0x0

    .line 35
    const/4 v4, 0x1

    .line 36
    if-ne v1, v4, :cond_3

    .line 37
    .line 38
    const/4 v1, 0x1

    .line 39
    goto :goto_0

    .line 40
    :cond_3
    const/4 v1, 0x0

    .line 41
    :goto_0
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 45
    .line 46
    if-nez p1, :cond_4

    .line 47
    .line 48
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    move-object p1, v0

    .line 52
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getLayout()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    const-string v1, "mediaView"

    .line 57
    .line 58
    if-ne p1, v4, :cond_6

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 61
    .line 62
    if-nez p1, :cond_5

    .line 63
    .line 64
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    move-object p1, v0

    .line 68
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o0:Landroid/content/Context;

    .line 73
    .line 74
    invoke-static {v5}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    div-int/lit8 v5, v5, 0x2

    .line 79
    .line 80
    iput v5, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 81
    .line 82
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 83
    .line 84
    if-nez p1, :cond_7

    .line 85
    .line 86
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    move-object p1, v0

    .line 90
    :cond_7
    invoke-virtual {p1, v4}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->〇oo〇(Z)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 94
    .line 95
    if-nez p1, :cond_8

    .line 96
    .line 97
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    move-object p1, v0

    .line 101
    :cond_8
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 102
    .line 103
    if-nez v4, :cond_9

    .line 104
    .line 105
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    move-object v4, v0

    .line 109
    :cond_9
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    .line 110
    .line 111
    const-string v6, "CSadMeStarfallAD"

    .line 112
    .line 113
    invoke-static {p1, v4, v5, v6}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇o00〇〇Oo(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Landroid/widget/ImageView$ScaleType;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 117
    .line 118
    if-nez p1, :cond_a

    .line 119
    .line 120
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    move-object p1, v0

    .line 124
    :cond_a
    invoke-virtual {p1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getClickType()I

    .line 125
    .line 126
    .line 127
    move-result p1

    .line 128
    if-nez p1, :cond_c

    .line 129
    .line 130
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->OO:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 131
    .line 132
    if-nez p1, :cond_b

    .line 133
    .line 134
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    move-object p1, v0

    .line 138
    :cond_b
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 139
    .line 140
    .line 141
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->o〇00O:Lcom/intsig/view/CloseCountDownView;

    .line 142
    .line 143
    if-nez p1, :cond_d

    .line 144
    .line 145
    const-string p1, "mCloseBtnView"

    .line 146
    .line 147
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    move-object p1, v0

    .line 151
    :cond_d
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->O8o08O8O:Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 152
    .line 153
    if-nez v1, :cond_e

    .line 154
    .line 155
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_e
    move-object v0, v1

    .line 160
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getDuration()J

    .line 161
    .line 162
    .line 163
    move-result-wide v0

    .line 164
    long-to-int v1, v0

    .line 165
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView$bindData$1;

    .line 166
    .line 167
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView$bindData$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {p1, v1, v0}, Lcom/intsig/view/CloseCountDownView;->〇8o8o〇(ILkotlin/jvm/functions/Function0;)V

    .line 171
    .line 172
    .line 173
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->Oo08()V

    .line 174
    .line 175
    .line 176
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final setCallBackClose(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/view/WaterFallView;->〇0O:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

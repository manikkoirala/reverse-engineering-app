.class public final Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;
.super Landroid/widget/FrameLayout;
.source "MePageVipRightView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:I

.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Z

.field private final 〇08O〇00〇o:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Z

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 5
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {p2, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇08O〇00〇o:Landroid/os/Handler;

    .line 6
    sget-object p2, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    new-instance p3, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mDefaultAnimatorSet$2;

    invoke-direct {p3, p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mDefaultAnimatorSet$2;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    invoke-static {p2, p3}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p3

    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o〇00O:Lkotlin/Lazy;

    .line 7
    new-instance p3, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLessAnimatorSet$2;

    invoke-direct {p3, p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLessAnimatorSet$2;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    invoke-static {p2, p3}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->O8o08O8O:Lkotlin/Lazy;

    const/4 p2, 0x3

    .line 8
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 9
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇o00〇〇Oo(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Z)Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    move-result-object p1

    const-string p2, "inflate(LayoutInflater.from(context), this, false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 11
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;

    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo0:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)Landroid/animation/AnimatorSet;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇〇808〇()Landroid/animation/AnimatorSet;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇()V
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇0O:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇08O〇00〇o:Landroid/os/Handler;

    .line 5
    .line 6
    new-instance v1, Lo8oOOo/〇080;

    .line 7
    .line 8
    invoke-direct {v1, p0}, Lo8oOOo/〇080;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 9
    .line 10
    .line 11
    const-wide/16 v2, 0x5dc

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMRightTextList()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    iget v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇OOo8〇0:I

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-ltz v1, :cond_0

    .line 13
    .line 14
    if-ge v1, v0, :cond_0

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    :cond_0
    if-nez v2, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 21
    .line 22
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMRightTextList()Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    iget v2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇OOo8〇0:I

    .line 29
    .line 30
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    check-cast v1, Ljava/lang/CharSequence;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇080OO8〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇080OO8〇0:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMAnimatorSet()Landroid/animation/AnimatorSet;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->start()V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private final getMAnimatorSet()Landroid/animation/AnimatorSet;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMDefaultAnimatorSet()Landroid/animation/AnimatorSet;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMLessAnimatorSet()Landroid/animation/AnimatorSet;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :goto_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getMDefaultAnimatorSet()Landroid/animation/AnimatorSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/animation/AnimatorSet;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getMLessAnimatorSet()Landroid/animation/AnimatorSet;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/animation/AnimatorSet;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getMMainColor()I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->〇080:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->〇o00〇〇Oo()Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipResItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipResItem;->〇o00〇〇Oo()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getMRightIconList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->〇080:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->O8()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getMRightTextList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->〇080:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipCardManager;->Oo08()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getVipShadowDrawable()Landroid/graphics/drawable/Drawable;
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMMainColor()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    const/4 v3, 0x0

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v1, 0x0

    .line 20
    :goto_0
    const/4 v4, 0x0

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    move-object v0, v4

    .line 25
    :goto_1
    if-eqz v0, :cond_2

    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 32
    .line 33
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 34
    .line 35
    .line 36
    const v4, 0x3dcccccd    # 0.1f

    .line 37
    .line 38
    .line 39
    invoke-static {v0, v4}, Lcom/intsig/utils/ext/IntExt;->〇080(IF)I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-virtual {v1, v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {v0, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->oo88o8O(Z)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    :cond_2
    return-object v4
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇〇8O0〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇0〇O0088o(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇0〇O0088o(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇080OO8〇0:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇OOo8〇0:I

    .line 11
    .line 12
    add-int/lit8 v0, v0, 0x1

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 15
    .line 16
    rem-int/2addr v0, v1

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇OOo8〇0:I

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO0o〇〇〇〇0()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇80〇808〇O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Ljava/lang/Integer;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 17
    .line 18
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 19
    .line 20
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Ljava/lang/Integer;

    .line 31
    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 39
    .line 40
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 41
    .line 42
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 43
    .line 44
    .line 45
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 46
    .line 47
    const/4 v1, 0x3

    .line 48
    if-ne v0, v1, :cond_2

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 51
    .line 52
    const/4 v1, 0x2

    .line 53
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Ljava/lang/Integer;

    .line 58
    .line 59
    if-eqz v0, :cond_2

    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 66
    .line 67
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 68
    .line 69
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 70
    .line 71
    .line 72
    :cond_2
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇8o8o〇(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇0O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇0O:Z

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMAnimatorSet()Landroid/animation/AnimatorSet;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->cancel()V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇08O〇00〇o:Landroid/os/Handler;

    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇O00()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇80〇808〇O()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO0o〇〇〇〇0()V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 16
    .line 17
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇080OO8〇0:Landroid/view/View;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 23
    .line 24
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇080OO8〇0:Landroid/view/View;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 37
    .line 38
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇0O:Landroid/view/View;

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 46
    .line 47
    const/high16 v1, 0x3f800000    # 1.0f

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 53
    .line 54
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->oOo〇8o008:Landroid/view/View;

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇O8o08O(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇8o8o〇(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇O〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getMRightIconList()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    check-cast v1, Ljava/util/Collection;

    .line 13
    .line 14
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇OOo8〇0:I

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    iput v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 29
    .line 30
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 31
    .line 32
    const-string v2, "mBinding.ivVipRight3"

    .line 33
    .line 34
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget v2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 38
    .line 39
    const/4 v3, 0x1

    .line 40
    const/4 v4, 0x3

    .line 41
    if-ne v2, v4, :cond_0

    .line 42
    .line 43
    const/4 v2, 0x1

    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/4 v2, 0x0

    .line 46
    :goto_0
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 50
    .line 51
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->oOo〇8o008:Landroid/view/View;

    .line 52
    .line 53
    const-string v2, "mBinding.vMask3"

    .line 54
    .line 55
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    iget v2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 59
    .line 60
    if-ne v2, v4, :cond_1

    .line 61
    .line 62
    const/4 v0, 0x1

    .line 63
    :cond_1
    invoke-static {v1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇O00()V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO0o〇〇()V

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO0o〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇808〇()Landroid/animation/AnimatorSet;
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0xc

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    new-instance v1, Landroid/animation/AnimatorSet;

    .line 15
    .line 16
    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 17
    .line 18
    .line 19
    const/4 v2, 0x3

    .line 20
    new-array v3, v2, [Landroid/animation/ObjectAnimator;

    .line 21
    .line 22
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 23
    .line 24
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 25
    .line 26
    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    .line 27
    .line 28
    const/4 v6, 0x2

    .line 29
    new-array v7, v6, [F

    .line 30
    .line 31
    const/4 v8, 0x0

    .line 32
    aput v0, v7, v8

    .line 33
    .line 34
    const/4 v9, 0x1

    .line 35
    const/4 v10, 0x0

    .line 36
    aput v10, v7, v9

    .line 37
    .line 38
    invoke-static {v4, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 39
    .line 40
    .line 41
    move-result-object v4

    .line 42
    aput-object v4, v3, v8

    .line 43
    .line 44
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 45
    .line 46
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇080OO8〇0:Landroid/view/View;

    .line 47
    .line 48
    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    .line 49
    .line 50
    new-array v7, v6, [F

    .line 51
    .line 52
    fill-array-data v7, :array_0

    .line 53
    .line 54
    .line 55
    invoke-static {v4, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    aput-object v4, v3, v9

    .line 60
    .line 61
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 62
    .line 63
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇080OO8〇0:Landroid/view/View;

    .line 64
    .line 65
    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    .line 66
    .line 67
    new-array v7, v6, [F

    .line 68
    .line 69
    aput v0, v7, v8

    .line 70
    .line 71
    aput v10, v7, v9

    .line 72
    .line 73
    invoke-static {v4, v5, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 74
    .line 75
    .line 76
    move-result-object v4

    .line 77
    aput-object v4, v3, v6

    .line 78
    .line 79
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    iget v4, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 84
    .line 85
    if-ne v4, v2, :cond_0

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 88
    .line 89
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 90
    .line 91
    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    .line 92
    .line 93
    new-array v5, v6, [F

    .line 94
    .line 95
    aput v0, v5, v8

    .line 96
    .line 97
    aput v10, v5, v9

    .line 98
    .line 99
    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 107
    .line 108
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇0O:Landroid/view/View;

    .line 109
    .line 110
    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    .line 111
    .line 112
    new-array v5, v6, [F

    .line 113
    .line 114
    aput v0, v5, v8

    .line 115
    .line 116
    aput v10, v5, v9

    .line 117
    .line 118
    invoke-static {v2, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 126
    .line 127
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 128
    .line 129
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    .line 130
    .line 131
    new-array v4, v6, [F

    .line 132
    .line 133
    fill-array-data v4, :array_1

    .line 134
    .line 135
    .line 136
    invoke-static {v0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    .line 142
    .line 143
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 144
    .line 145
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->oOo〇8o008:Landroid/view/View;

    .line 146
    .line 147
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    .line 148
    .line 149
    new-array v4, v6, [F

    .line 150
    .line 151
    fill-array-data v4, :array_2

    .line 152
    .line 153
    .line 154
    invoke-static {v0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    .line 160
    .line 161
    goto :goto_0

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 163
    .line 164
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 165
    .line 166
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    .line 167
    .line 168
    new-array v4, v6, [F

    .line 169
    .line 170
    fill-array-data v4, :array_3

    .line 171
    .line 172
    .line 173
    invoke-static {v0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 174
    .line 175
    .line 176
    move-result-object v0

    .line 177
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 181
    .line 182
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->〇0O:Landroid/view/View;

    .line 183
    .line 184
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    .line 185
    .line 186
    new-array v4, v6, [F

    .line 187
    .line 188
    fill-array-data v4, :array_4

    .line 189
    .line 190
    .line 191
    invoke-static {v0, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    .line 192
    .line 193
    .line 194
    move-result-object v0

    .line 195
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    .line 197
    .line 198
    :goto_0
    instance-of v0, v3, Ljava/util/Collection;

    .line 199
    .line 200
    if-eqz v0, :cond_1

    .line 201
    .line 202
    check-cast v3, Ljava/util/Collection;

    .line 203
    .line 204
    goto :goto_1

    .line 205
    :cond_1
    const/4 v3, 0x0

    .line 206
    :goto_1
    invoke-virtual {v1, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 207
    .line 208
    .line 209
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$initAnimatorSet$lambda$11$$inlined$doOnStart$1;

    .line 210
    .line 211
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$initAnimatorSet$lambda$11$$inlined$doOnStart$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 212
    .line 213
    .line 214
    invoke-virtual {v1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 215
    .line 216
    .line 217
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$initAnimatorSet$lambda$11$$inlined$doOnEnd$1;

    .line 218
    .line 219
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$initAnimatorSet$lambda$11$$inlined$doOnEnd$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {v1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 223
    .line 224
    .line 225
    const-wide/16 v2, 0x1f4

    .line 226
    .line 227
    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 228
    .line 229
    .line 230
    return-object v1

    .line 231
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇〇888(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-interface {p1}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo0:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    if-eqz p1, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo0:Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView$mLifecycleObserver$1;

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇〇8O0〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 11
    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Ljava/lang/Number;

    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->OO:Ljava/util/List;

    .line 24
    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇80〇808〇O()V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 36
    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 38
    .line 39
    new-instance v1, Lo8oOOo/〇o00〇〇Oo;

    .line 40
    .line 41
    invoke-direct {v1, p0}, Lo8oOOo/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;)V

    .line 42
    .line 43
    .line 44
    const-wide/16 v2, 0xfa

    .line 45
    .line 46
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final oO80(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->oOo0:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x1

    .line 14
    const-string v3, "it"

    .line 15
    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->getVipShadowDrawable()Landroid/graphics/drawable/Drawable;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 39
    .line 40
    .line 41
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇O〇()V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->o0:Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;

    .line 45
    .line 46
    iget-object v3, v0, Lcom/intsig/camscanner/databinding/LayoutMePagVipRightViewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 47
    .line 48
    const-string v0, "mBinding.tvVipRight"

    .line 49
    .line 50
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->oOo〇8o008:I

    .line 54
    .line 55
    const/4 v1, 0x3

    .line 56
    if-ne v0, v1, :cond_2

    .line 57
    .line 58
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 59
    .line 60
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    const/16 v1, 0x48

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    const/16 v1, 0x3c

    .line 74
    .line 75
    :goto_1
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    move v4, v0

    .line 80
    const/4 v5, 0x0

    .line 81
    const/4 v6, 0x0

    .line 82
    const/4 v7, 0x0

    .line 83
    const/16 v8, 0xe

    .line 84
    .line 85
    const/4 v9, 0x0

    .line 86
    invoke-static/range {v3 .. v9}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 87
    .line 88
    .line 89
    invoke-static {p0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇〇888(Landroidx/lifecycle/LifecycleOwner;)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;->〇O8o08O(Lcom/intsig/camscanner/mainmenu/mepage/vip/MePageVipRightView;ZILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

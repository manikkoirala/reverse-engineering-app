.class public final Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "MePageIntervalProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;,
        Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->o〇00O:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->O8o08O8O:I

    return-void
.end method

.method public synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/16 p1, 0x8

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const p2, 0x7f0d0449

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;-><init>(II)V

    return-void
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "MePageIntervalProvider"

    .line 7
    .line 8
    const-string v0, "on click left card interval"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "CSMyAccount"

    .line 14
    .line 15
    const-string v0, "point"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-nez p1, :cond_0

    .line 29
    .line 30
    sget-object p1, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 31
    .line 32
    const-string v0, "cs_my_account"

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇80〇808〇O(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$bindLeftInterval$1$1;

    .line 42
    .line 43
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$bindLeftInterval$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;)V

    .line 44
    .line 45
    .line 46
    invoke-static {p1, v0}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇〇808〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;)V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 55
    .line 56
    .line 57
    move-result-object p0

    .line 58
    invoke-static {p0}, Lcom/intsig/camscanner/web/UrlUtil;->o〇0OOo〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p0

    .line 62
    invoke-static {p1, p0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O8〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->oo〇()Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const v2, 0x7f13107e

    .line 10
    .line 11
    .line 12
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$bindRightTurnTable$1;

    .line 22
    .line 23
    invoke-direct {v1, p1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$bindRightTurnTable$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇〇808〇(Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager$TaskInterfaceCallBack;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->o〇〇0〇()Landroid/widget/ImageView;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const v1, 0x7f080d5c

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇oOO8O8()Landroid/view/View;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    new-instance v0, Looo0〇〇O/O8;

    .line 44
    .line 45
    invoke-direct {v0, p0}, Looo0〇〇O/O8;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final OOO〇O0(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->oo〇()Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;->mall_entry_main_paperwork:Ljava/lang/String;

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v1, p2, Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;->mall_entry_vice_paperwork:Ljava/lang/String;

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p2, Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;->mall_entry_url:Ljava/lang/String;

    .line 20
    .line 21
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iget-object p2, p2, Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;->mall_entry_img:Ljava/lang/String;

    .line 36
    .line 37
    invoke-virtual {v0, p2}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->o〇〇0〇()Landroid/widget/ImageView;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {p2, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 46
    .line 47
    .line 48
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇oOO8O8()Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    new-instance p2, Looo0〇〇O/〇o〇;

    .line 53
    .line 54
    invoke-direct {p2, p0}, Looo0〇〇O/〇o〇;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇0000OOO()Landroid/widget/ImageView;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const v1, 0x7f080931

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇00()Landroid/widget/TextView;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const v2, 0x7f1310ce

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->OOO〇O0()Landroid/widget/TextView;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    sget-object v1, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->Oo08()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    const v3, 0x7f1310cf

    .line 64
    .line 65
    .line 66
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v1, " "

    .line 79
    .line 80
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    new-instance v0, Looo0〇〇O/〇o00〇〇Oo;

    .line 98
    .line 99
    invoke-direct {v0, p0}, Looo0〇〇O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    const/16 v0, 0x8

    .line 111
    .line 112
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 113
    .line 114
    .line 115
    :goto_0
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇00〇8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇0000OOO(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "MePageIntervalProvider"

    .line 7
    .line 8
    const-string v0, "on click app mall"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const/4 v3, 0x0

    .line 20
    const/4 v4, 0x0

    .line 21
    const/4 v5, 0x6

    .line 22
    const/4 v6, 0x0

    .line 23
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇O8o08O(Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;Landroid/content/Context;Ljava/lang/String;Lcom/intsig/camscanner/ads/adinterface/OnOpenYouZanListener;ILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇〇0〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->app_mall_business:Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->OOO〇O0(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;Lcom/intsig/tsapp/sync/AppConfigJson$MallBusiness;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->O8〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V

    .line 14
    .line 15
    .line 16
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇〇888()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇oOO8O8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V

    .line 14
    .line 15
    .line 16
    :goto_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇0000OOO(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p2, "$this_apply"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "MePageIntervalProvider"

    .line 7
    .line 8
    const-string v0, "on click left card ope"

    .line 9
    .line 10
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sget-object p2, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;

    .line 14
    .line 15
    const-string v0, "CSMeLeftCardIcon"

    .line 16
    .line 17
    invoke-virtual {p2, v0, p0}, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;->〇o〇(Ljava/lang/String;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇〇888()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇00〇8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "MePageIntervalProvider"

    .line 7
    .line 8
    const-string v0, "on click  turntable"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "CSMyAccount"

    .line 14
    .line 15
    const-string v0, "russian_roulette"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    invoke-static {p0}, Lcom/intsig/camscanner/web/UrlUtil;->〇8〇0〇o〇O(Landroid/content/Context;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p0

    .line 32
    invoke-static {p1, p0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇oOO8O8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->ME_LEFT_CARD:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇O00(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;)Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v3, 0x0

    .line 14
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->OOO〇O0()Landroid/widget/TextView;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getTitle()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇00()Landroid/widget/TextView;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getDescription()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    if-nez v2, :cond_0

    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-static {v2}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-virtual {v1}, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->getPic()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v3

    .line 61
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->〇0000OOO()Landroid/widget/ImageView;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 70
    .line 71
    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    new-instance v2, Looo0〇〇O/〇080;

    .line 89
    .line 90
    invoke-direct {v2, v1, v0}, Looo0〇〇O/〇080;-><init>(Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    .line 95
    .line 96
    iget-boolean p1, v1, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->hasNotifyShow:Z

    .line 97
    .line 98
    if-nez p1, :cond_2

    .line 99
    .line 100
    const/4 p1, 0x1

    .line 101
    iput-boolean p1, v1, Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;->hasNotifyShow:Z

    .line 102
    .line 103
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇80〇808〇O()V

    .line 104
    .line 105
    .line 106
    sget-object p1, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;

    .line 107
    .line 108
    const-string v0, "CSMeLeftCardIcon"

    .line 109
    .line 110
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/operation/middleoperation/commonstyle/OperationLogAgent$Companion;->o〇0(Ljava/lang/String;Lcom/intsig/camscanner/ads/csAd/bean/CsAdDataBean;)V

    .line 111
    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;->O8ooOoo〇()Landroid/view/View;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    const/16 v0, 0x8

    .line 119
    .line 120
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 121
    .line 122
    .line 123
    :cond_2
    :goto_0
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;)V
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance p2, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;

    .line 12
    .line 13
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 14
    .line 15
    const-string v0, "helper.itemView"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;-><init>(Landroid/view/View;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->〇00(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->o〇〇0〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider$CardHolder;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "MePageLoginOptProvider.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8o08O8O:I

.field private oOo〇8o008:Ljava/lang/String;

.field private final o〇00O:I

.field private final 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->o〇00O:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->O8o08O8O:I

    const-string p1, "MePageLoginOptProvider"

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 6
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇0O:Ljava/lang/Boolean;

    return-void
.end method

.method public synthetic constructor <init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/16 p1, 0x10

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const p2, 0x7f0d044b

    .line 1
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;-><init>(II)V

    return-void
.end method

.method private static final O8〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "this$0"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v2, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v3, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v4, "click tvLogin mLastLoginAccount is "

    .line 18
    .line 19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    sget-object v1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene$Companion;

    .line 33
    .line 34
    const/4 v2, 0x1

    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene$Companion;->〇o〇(Z)V

    .line 36
    .line 37
    .line 38
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇o()V

    .line 39
    .line 40
    .line 41
    const-string v1, "type"

    .line 42
    .line 43
    const-string v3, "last_login_account"

    .line 44
    .line 45
    const-string v4, "CSMyAccount"

    .line 46
    .line 47
    const-string v5, "login"

    .line 48
    .line 49
    invoke-static {v4, v5, v1, v3}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o〇0OOo〇0()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-eqz v1, :cond_0

    .line 57
    .line 58
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-static {v0, v2, v4}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇〇888(Landroid/content/Context;ZLjava/lang/String;)V

    .line 63
    .line 64
    .line 65
    goto/16 :goto_2

    .line 66
    .line 67
    :cond_0
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇〇〇0〇〇0()Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-nez v1, :cond_4

    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/tsapp/account/login/login_type/LoginType;->isGoogleLastLogin()Z

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    if-eqz v1, :cond_1

    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_1
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇0O:Ljava/lang/Boolean;

    .line 81
    .line 82
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 83
    .line 84
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    const/4 v2, 0x2

    .line 89
    const/4 v3, 0x0

    .line 90
    if-eqz v1, :cond_3

    .line 91
    .line 92
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    instance-of v4, v1, Landroidx/fragment/app/FragmentActivity;

    .line 97
    .line 98
    if-eqz v4, :cond_2

    .line 99
    .line 100
    check-cast v1, Landroidx/fragment/app/FragmentActivity;

    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_2
    move-object v1, v3

    .line 104
    :goto_0
    if-eqz v1, :cond_5

    .line 105
    .line 106
    sget-object v4, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 107
    .line 108
    iget-object v10, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 109
    .line 110
    new-instance v15, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;

    .line 111
    .line 112
    const/16 v6, 0x2710

    .line 113
    .line 114
    const/4 v7, 0x0

    .line 115
    const/4 v8, 0x0

    .line 116
    const/4 v9, 0x0

    .line 117
    const/4 v11, 0x0

    .line 118
    const/4 v12, 0x0

    .line 119
    const/4 v13, 0x0

    .line 120
    const/4 v14, 0x0

    .line 121
    const/16 v16, 0x1

    .line 122
    .line 123
    const/16 v17, 0xee

    .line 124
    .line 125
    const/16 v18, 0x0

    .line 126
    .line 127
    move-object v5, v15

    .line 128
    move-object/from16 v19, v15

    .line 129
    .line 130
    move/from16 v15, v16

    .line 131
    .line 132
    move/from16 v16, v17

    .line 133
    .line 134
    move-object/from16 v17, v18

    .line 135
    .line 136
    invoke-direct/range {v5 .. v17}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;-><init>(IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 137
    .line 138
    .line 139
    move-object/from16 v5, v19

    .line 140
    .line 141
    invoke-static {v4, v5, v3, v2, v3}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇〇888(Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)Lcom/intsig/tsapp/account/login/LoginBottomDialog;

    .line 142
    .line 143
    .line 144
    move-result-object v2

    .line 145
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    const-string v3, "it.supportFragmentManager"

    .line 150
    .line 151
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    iget-object v0, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 155
    .line 156
    invoke-virtual {v2, v1, v0}, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    goto :goto_2

    .line 160
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    invoke-static {v0, v3, v2, v3}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇O〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)V

    .line 165
    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_4
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇00〇8()V

    .line 169
    .line 170
    .line 171
    :cond_5
    :goto_2
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private static final OOO〇O0(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "this$0"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 9
    .line 10
    const-string v2, "click ivEmailLogin"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇o()V

    .line 16
    .line 17
    .line 18
    const-string v1, "type"

    .line 19
    .line 20
    const-string v2, "email"

    .line 21
    .line 22
    const-string v3, "CSMyAccount"

    .line 23
    .line 24
    const-string v4, "login"

    .line 25
    .line 26
    invoke-static {v3, v4, v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual/range {p0 .. p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    instance-of v2, v1, Landroidx/fragment/app/FragmentActivity;

    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    if-eqz v2, :cond_0

    .line 37
    .line 38
    check-cast v1, Landroidx/fragment/app/FragmentActivity;

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    move-object v1, v3

    .line 42
    :goto_0
    if-eqz v1, :cond_1

    .line 43
    .line 44
    sget-object v2, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 45
    .line 46
    new-instance v15, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;

    .line 47
    .line 48
    const/16 v5, 0x2710

    .line 49
    .line 50
    const/4 v6, 0x0

    .line 51
    const/4 v7, 0x0

    .line 52
    const/4 v8, 0x0

    .line 53
    const/4 v9, 0x0

    .line 54
    const/4 v10, 0x0

    .line 55
    const/4 v11, 0x0

    .line 56
    const/4 v12, 0x0

    .line 57
    const/4 v13, 0x0

    .line 58
    const/4 v14, 0x1

    .line 59
    const/16 v16, 0xfe

    .line 60
    .line 61
    const/16 v17, 0x0

    .line 62
    .line 63
    move-object v4, v15

    .line 64
    move-object/from16 v18, v15

    .line 65
    .line 66
    move/from16 v15, v16

    .line 67
    .line 68
    move-object/from16 v16, v17

    .line 69
    .line 70
    invoke-direct/range {v4 .. v16}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;-><init>(IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 71
    .line 72
    .line 73
    const/4 v4, 0x2

    .line 74
    move-object/from16 v5, v18

    .line 75
    .line 76
    invoke-static {v2, v5, v3, v4, v3}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇〇888(Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)Lcom/intsig/tsapp/account/login/LoginBottomDialog;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    const-string v3, "it.supportFragmentManager"

    .line 85
    .line 86
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    iget-object v0, v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 90
    .line 91
    invoke-virtual {v2, v1, v0}, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_1
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic O〇8O8〇008(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇oOO8O8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇0000OOO(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->o〇〇0〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 4

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click tvOtherLoginWays"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sget-object p1, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene;->OO〇00〇8oO:Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene$Companion;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/tsapp/account/login/login_dialog_scene/MobileNumberInputScene$Companion;->〇o〇(Z)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇o()V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 23
    .line 24
    const/4 v1, 0x2

    .line 25
    const/4 v2, 0x0

    .line 26
    if-eqz p1, :cond_0

    .line 27
    .line 28
    const-string v3, "@"

    .line 29
    .line 30
    invoke-static {p1, v3, v0, v1, v2}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_0

    .line 35
    .line 36
    const/4 p1, 0x1

    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const/4 p1, 0x0

    .line 39
    :goto_0
    if-eqz p1, :cond_2

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    instance-of v0, p1, Landroidx/fragment/app/FragmentActivity;

    .line 46
    .line 47
    if-eqz v0, :cond_1

    .line 48
    .line 49
    check-cast p1, Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    goto :goto_1

    .line 52
    :cond_1
    move-object p1, v2

    .line 53
    :goto_1
    if-eqz p1, :cond_3

    .line 54
    .line 55
    sget-object v0, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 56
    .line 57
    invoke-static {v0, v2, v2, v1, v2}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇〇888(Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion$LoginBottomDialogParams;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)Lcom/intsig/tsapp/account/login/LoginBottomDialog;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 62
    .line 63
    .line 64
    move-result-object p1

    .line 65
    const-string v1, "it.supportFragmentManager"

    .line 66
    .line 67
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 71
    .line 72
    invoke-virtual {v0, p1, p0}, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_2
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 77
    .line 78
    .line 79
    move-result-object p0

    .line 80
    invoke-static {p0, v0, v0}, Lcom/intsig/tsapp/account/login/LoginTranslucentActivity;->〇o〇88〇8(Landroid/content/Context;ZZ)V

    .line 81
    .line 82
    .line 83
    :cond_3
    :goto_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇〇0〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click ivMobileLogin"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "type"

    .line 14
    .line 15
    const-string v0, "mobile"

    .line 16
    .line 17
    const-string v1, "CSMyAccount"

    .line 18
    .line 19
    const-string v2, "login"

    .line 20
    .line 21
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇o()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    const/4 p1, 0x0

    .line 32
    const/4 v0, 0x2

    .line 33
    invoke-static {p0, p1, v0, p1}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇O〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->OOO〇O0(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0000OOO(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click ivGoogleLogin"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "type"

    .line 14
    .line 15
    const-string v0, "google"

    .line 16
    .line 17
    const-string v1, "CSMyAccount"

    .line 18
    .line 19
    const-string v2, "login"

    .line 20
    .line 21
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇00〇8()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇00〇8()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Landroid/app/Activity;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    check-cast v0, Landroid/app/Activity;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v2

    .line 14
    :goto_0
    if-eqz v0, :cond_1

    .line 15
    .line 16
    sget-object v1, Lcom/intsig/tsapp/account/login_task/GoogleLoginControl;->O8:Lcom/intsig/tsapp/account/login_task/GoogleLoginControl$Companion;

    .line 17
    .line 18
    const/16 v3, 0x92

    .line 19
    .line 20
    invoke-virtual {v1, v0, v2, v3}, Lcom/intsig/tsapp/account/login_task/GoogleLoginControl$Companion;->Oo08(Landroid/app/Activity;Landroid/widget/CheckBox;I)V

    .line 21
    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 2
    .line 3
    const-string v1, "cs_my_account"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇80〇808〇O(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->oO80(Z)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇oOO8O8(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇080OO8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "click ivWeChatLogin"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇o()V

    .line 14
    .line 15
    .line 16
    const-string p1, "type"

    .line 17
    .line 18
    const-string v0, "wechat"

    .line 19
    .line 20
    const-string v1, "CSMyAccount"

    .line 21
    .line 22
    const-string v2, "login"

    .line 23
    .line 24
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object p0

    .line 31
    const/4 p1, 0x1

    .line 32
    invoke-static {p0, p1, v1}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇〇888(Landroid/content/Context;ZLjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->O8〇o(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8ooOoo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;)V
    .locals 11
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseCompatLoadingForDrawables"
        }
    .end annotation

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇〇8O0〇8()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 16
    .line 17
    const p2, 0x7f0a0fba

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    check-cast p2, Landroid/widget/RelativeLayout;

    .line 25
    .line 26
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇080:Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/MainCommonUtil;->〇80〇808〇O()Landroid/graphics/drawable/Drawable;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {p2, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 33
    .line 34
    .line 35
    const p2, 0x7f0a188b

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    check-cast p2, Landroid/widget/TextView;

    .line 43
    .line 44
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    const v1, 0x7f060251

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    const v1, 0x7f060208

    .line 71
    .line 72
    .line 73
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 78
    .line 79
    .line 80
    const p2, 0x7f0a0c49

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    check-cast p2, Landroid/widget/LinearLayout;

    .line 88
    .line 89
    const v0, 0x7f0a0986

    .line 90
    .line 91
    .line 92
    invoke-virtual {p1, v0}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    check-cast v0, Landroid/widget/ImageView;

    .line 97
    .line 98
    const v1, 0x7f0a0985

    .line 99
    .line 100
    .line 101
    invoke-virtual {p1, v1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    check-cast v1, Landroid/widget/ImageView;

    .line 106
    .line 107
    const v2, 0x7f0a0984

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    check-cast v2, Landroid/widget/ImageView;

    .line 115
    .line 116
    const v3, 0x7f0a0983

    .line 117
    .line 118
    .line 119
    invoke-virtual {p1, v3}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 120
    .line 121
    .line 122
    move-result-object v3

    .line 123
    check-cast v3, Landroid/widget/ImageView;

    .line 124
    .line 125
    const v4, 0x7f0a0fb4

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1, v4}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 129
    .line 130
    .line 131
    move-result-object v4

    .line 132
    check-cast v4, Landroid/widget/RelativeLayout;

    .line 133
    .line 134
    const v5, 0x7f0a0aa6

    .line 135
    .line 136
    .line 137
    invoke-virtual {p1, v5}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v5

    .line 141
    check-cast v5, Landroid/widget/ImageView;

    .line 142
    .line 143
    const v6, 0x7f0a18ec

    .line 144
    .line 145
    .line 146
    invoke-virtual {p1, v6}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 147
    .line 148
    .line 149
    move-result-object v6

    .line 150
    check-cast v6, Landroid/widget/TextView;

    .line 151
    .line 152
    const v7, 0x7f0a158f

    .line 153
    .line 154
    .line 155
    invoke-virtual {p1, v7}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 156
    .line 157
    .line 158
    move-result-object v7

    .line 159
    check-cast v7, Landroid/widget/TextView;

    .line 160
    .line 161
    const v8, 0x7f0a1590

    .line 162
    .line 163
    .line 164
    invoke-virtual {p1, v8}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 165
    .line 166
    .line 167
    move-result-object p1

    .line 168
    check-cast p1, Landroid/widget/TextView;

    .line 169
    .line 170
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 171
    .line 172
    .line 173
    move-result v8

    .line 174
    if-eqz v8, :cond_1

    .line 175
    .line 176
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 177
    .line 178
    .line 179
    move-result-object v8

    .line 180
    const v9, 0x7f0809cc

    .line 181
    .line 182
    .line 183
    goto :goto_1

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 185
    .line 186
    .line 187
    move-result-object v8

    .line 188
    const v9, 0x7f0809ba

    .line 189
    .line 190
    .line 191
    :goto_1
    invoke-virtual {v8, v9}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 192
    .line 193
    .line 194
    move-result-object v8

    .line 195
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 196
    .line 197
    .line 198
    move-result v9

    .line 199
    if-eqz v9, :cond_2

    .line 200
    .line 201
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 202
    .line 203
    .line 204
    move-result-object v9

    .line 205
    const v10, 0x7f080976

    .line 206
    .line 207
    .line 208
    goto :goto_2

    .line 209
    :cond_2
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 210
    .line 211
    .line 212
    move-result-object v9

    .line 213
    const v10, 0x7f080975

    .line 214
    .line 215
    .line 216
    :goto_2
    invoke-virtual {v9, v10}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 217
    .line 218
    .line 219
    move-result-object v9

    .line 220
    invoke-virtual {p2, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 221
    .line 222
    .line 223
    invoke-virtual {v4, v9}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 224
    .line 225
    .line 226
    invoke-static {}, Lcom/intsig/tsapp/account/util/LoginUtils;->〇o〇()I

    .line 227
    .line 228
    .line 229
    move-result v8

    .line 230
    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 231
    .line 232
    .line 233
    invoke-static {}, Lcom/intsig/tsapp/account/util/LoginUtils;->〇o00〇〇Oo()I

    .line 234
    .line 235
    .line 236
    move-result v8

    .line 237
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 238
    .line 239
    .line 240
    new-instance v8, Looo0〇〇O/Oo08;

    .line 241
    .line 242
    invoke-direct {v8, p0}, Looo0〇〇O/Oo08;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 243
    .line 244
    .line 245
    invoke-virtual {v0, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 246
    .line 247
    .line 248
    new-instance v8, Looo0〇〇O/o〇0;

    .line 249
    .line 250
    invoke-direct {v8, p0}, Looo0〇〇O/o〇0;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 251
    .line 252
    .line 253
    invoke-virtual {v3, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    .line 255
    .line 256
    new-instance v8, Looo0〇〇O/〇〇888;

    .line 257
    .line 258
    invoke-direct {v8, p0}, Looo0〇〇O/〇〇888;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 259
    .line 260
    .line 261
    invoke-virtual {v1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    .line 263
    .line 264
    new-instance v1, Looo0〇〇O/oO80;

    .line 265
    .line 266
    invoke-direct {v1, p0}, Looo0〇〇O/oO80;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 267
    .line 268
    .line 269
    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    .line 271
    .line 272
    new-instance v1, Looo0〇〇O/〇80〇808〇O;

    .line 273
    .line 274
    invoke-direct {v1, p0}, Looo0〇〇O/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 275
    .line 276
    .line 277
    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    .line 279
    .line 280
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇8O0〇8()Z

    .line 281
    .line 282
    .line 283
    move-result p1

    .line 284
    const/16 v1, 0x8

    .line 285
    .line 286
    const/4 v2, 0x0

    .line 287
    if-eqz p1, :cond_3

    .line 288
    .line 289
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 290
    .line 291
    .line 292
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    .line 294
    .line 295
    goto :goto_3

    .line 296
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 297
    .line 298
    .line 299
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->oO80()Z

    .line 300
    .line 301
    .line 302
    move-result p1

    .line 303
    if-eqz p1, :cond_4

    .line 304
    .line 305
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 306
    .line 307
    .line 308
    move-result-object p1

    .line 309
    invoke-static {p1}, Lcom/intsig/utils/AppInstallerUtil;->〇o〇(Landroid/content/Context;)Z

    .line 310
    .line 311
    .line 312
    move-result p1

    .line 313
    if-nez p1, :cond_4

    .line 314
    .line 315
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 316
    .line 317
    .line 318
    goto :goto_3

    .line 319
    :cond_4
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 320
    .line 321
    .line 322
    :goto_3
    new-instance p1, Looo0〇〇O/OO0o〇〇〇〇0;

    .line 323
    .line 324
    invoke-direct {p1, p0}, Looo0〇〇O/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;)V

    .line 325
    .line 326
    .line 327
    invoke-virtual {v7, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    .line 329
    .line 330
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 331
    .line 332
    const/4 v0, 0x1

    .line 333
    if-eqz p1, :cond_6

    .line 334
    .line 335
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 336
    .line 337
    .line 338
    move-result p1

    .line 339
    if-lez p1, :cond_5

    .line 340
    .line 341
    const/4 p1, 0x1

    .line 342
    goto :goto_4

    .line 343
    :cond_5
    const/4 p1, 0x0

    .line 344
    :goto_4
    if-ne p1, v0, :cond_6

    .line 345
    .line 346
    const/4 p1, 0x1

    .line 347
    goto :goto_5

    .line 348
    :cond_6
    const/4 p1, 0x0

    .line 349
    :goto_5
    if-eqz p1, :cond_c

    .line 350
    .line 351
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 352
    .line 353
    invoke-static {p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->Ooo(Ljava/lang/String;)Z

    .line 354
    .line 355
    .line 356
    move-result p1

    .line 357
    if-nez p1, :cond_c

    .line 358
    .line 359
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 360
    .line 361
    .line 362
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 363
    .line 364
    .line 365
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 366
    .line 367
    const/4 p2, 0x2

    .line 368
    const/4 v1, 0x0

    .line 369
    if-eqz p1, :cond_7

    .line 370
    .line 371
    const-string v3, "@"

    .line 372
    .line 373
    invoke-static {p1, v3, v2, p2, v1}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 374
    .line 375
    .line 376
    move-result p1

    .line 377
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 378
    .line 379
    .line 380
    move-result-object p1

    .line 381
    goto :goto_6

    .line 382
    :cond_7
    move-object p1, v1

    .line 383
    :goto_6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->〇0O:Ljava/lang/Boolean;

    .line 384
    .line 385
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇80〇808〇O()Ljava/lang/String;

    .line 386
    .line 387
    .line 388
    move-result-object p1

    .line 389
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 390
    .line 391
    .line 392
    move-result v3

    .line 393
    if-nez v3, :cond_8

    .line 394
    .line 395
    const/4 v3, 0x1

    .line 396
    goto :goto_7

    .line 397
    :cond_8
    const/4 v3, 0x0

    .line 398
    :goto_7
    if-eqz v3, :cond_9

    .line 399
    .line 400
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 401
    .line 402
    invoke-static {v1, p1}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇O888o0o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 403
    .line 404
    .line 405
    move-result-object p1

    .line 406
    :cond_9
    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    .line 408
    .line 409
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 410
    .line 411
    if-eqz p1, :cond_a

    .line 412
    .line 413
    const-string v3, "CSWX"

    .line 414
    .line 415
    invoke-static {p1, v3, v2, p2, v1}, Lkotlin/text/StringsKt;->〇00〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 416
    .line 417
    .line 418
    move-result p1

    .line 419
    if-ne p1, v0, :cond_a

    .line 420
    .line 421
    const/4 p1, 0x1

    .line 422
    goto :goto_8

    .line 423
    :cond_a
    const/4 p1, 0x0

    .line 424
    :goto_8
    if-eqz p1, :cond_d

    .line 425
    .line 426
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->oOo〇8o008:Ljava/lang/String;

    .line 427
    .line 428
    if-eqz p1, :cond_b

    .line 429
    .line 430
    const-string v3, "@camscanner"

    .line 431
    .line 432
    invoke-static {p1, v3, v2, p2, v1}, Lkotlin/text/StringsKt;->〇〇8O0〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 433
    .line 434
    .line 435
    move-result p1

    .line 436
    if-ne p1, v0, :cond_b

    .line 437
    .line 438
    const/4 v2, 0x1

    .line 439
    :cond_b
    if-eqz v2, :cond_d

    .line 440
    .line 441
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 442
    .line 443
    .line 444
    move-result-object p1

    .line 445
    invoke-static {p1}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 446
    .line 447
    .line 448
    move-result-object p1

    .line 449
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇oOO8O8()Ljava/lang/String;

    .line 450
    .line 451
    .line 452
    move-result-object p2

    .line 453
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 454
    .line 455
    .line 456
    move-result-object p1

    .line 457
    new-instance p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 458
    .line 459
    invoke-direct {p2}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 460
    .line 461
    .line 462
    new-instance v0, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;

    .line 463
    .line 464
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 465
    .line 466
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 467
    .line 468
    .line 469
    move-result-object v1

    .line 470
    const/16 v2, 0x64

    .line 471
    .line 472
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 473
    .line 474
    .line 475
    move-result v1

    .line 476
    invoke-direct {v0, v1}, Lcom/bumptech/glide/load/resource/bitmap/RoundedCorners;-><init>(I)V

    .line 477
    .line 478
    .line 479
    invoke-virtual {p2, v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 480
    .line 481
    .line 482
    move-result-object p2

    .line 483
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 484
    .line 485
    .line 486
    move-result-object p1

    .line 487
    new-instance p2, Lcom/bumptech/glide/request/RequestOptions;

    .line 488
    .line 489
    invoke-direct {p2}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 490
    .line 491
    .line 492
    const v0, 0x7f080969

    .line 493
    .line 494
    .line 495
    invoke-virtual {p2, v0}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇8o8o〇(I)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 496
    .line 497
    .line 498
    move-result-object p2

    .line 499
    invoke-virtual {p1, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 500
    .line 501
    .line 502
    move-result-object p1

    .line 503
    invoke-virtual {p1, v5}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 504
    .line 505
    .line 506
    goto :goto_9

    .line 507
    :cond_c
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 508
    .line 509
    .line 510
    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 511
    .line 512
    .line 513
    :cond_d
    :goto_9
    return-void
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->O8ooOoo〇(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

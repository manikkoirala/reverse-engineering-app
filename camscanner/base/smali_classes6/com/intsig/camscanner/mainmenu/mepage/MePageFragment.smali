.class public final Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MePageFragment.kt"

# interfaces
.implements Lcom/chad/library/adapter/base/listener/OnItemClickListener;
.implements Lcom/chad/library/adapter/base/listener/OnItemChildClickListener;
.implements Lcom/chad/library/adapter/base/listener/OnItemChildLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇080OO8〇0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private OO:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

.field private 〇08O〇00〇o:I

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$2;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v1, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 35
    .line 36
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$3;

    .line 41
    .line 42
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$4;

    .line 46
    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$5;

    .line 52
    .line 53
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O08〇()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK KING KONG CLOUD SPACE"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    const/4 v1, 0x1

    .line 17
    const-string v2, "CSMyAccount"

    .line 18
    .line 19
    if-ne v0, v1, :cond_1

    .line 20
    .line 21
    const-string v0, "cloud"

    .line 22
    .line 23
    invoke-static {v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/backup/BackUpExp;->〇080()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "/me/setting_main"

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const-string v1, "extra_which_page"

    .line 43
    .line 44
    const/4 v2, 0x3

    .line 45
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇O8〇8000()V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    if-nez v0, :cond_2

    .line 58
    .line 59
    const-string v0, "login_register"

    .line 60
    .line 61
    invoke-static {v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onKKCloudSpaceClicked$1;

    .line 65
    .line 66
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onKKCloudSpaceClicked$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0O0O〇〇〇0(Lkotlin/jvm/functions/Function0;)V

    .line 70
    .line 71
    .line 72
    :cond_2
    :goto_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O0O0〇()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "onAreaFreeCardClicked"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "self_share"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇o〇()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "childFragmentManager"

    .line 26
    .line 27
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x2

    .line 31
    const/4 v2, 0x0

    .line 32
    invoke-static {v0, v2, v1, v2}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇O〇(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;ILjava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0〇()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK KING KONG TEAM"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "group"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->O8()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    new-instance v0, Landroid/os/Bundle;

    .line 22
    .line 23
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 24
    .line 25
    .line 26
    const-string v1, "path"

    .line 27
    .line 28
    const-string v2, "/cs/opennormalweb"

    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const v1, 0x7f131db4

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    const-string v2, "title"

    .line 41
    .line 42
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->〇08O8o〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const-string v2, "url"

    .line 52
    .line 53
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    const-string v2, "/web/allpage"

    .line 61
    .line 62
    invoke-virtual {v1, v2}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    instance-of v2, v1, Lcom/intsig/router/service/RouterWebService;

    .line 71
    .line 72
    if-eqz v2, :cond_0

    .line 73
    .line 74
    check-cast v1, Lcom/intsig/router/service/RouterWebService;

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_0
    const/4 v1, 0x0

    .line 78
    :goto_0
    if-eqz v1, :cond_2

    .line 79
    .line 80
    invoke-interface {v1, v0}, Lcom/intsig/router/service/RouterWebService;->startWeb(Landroid/os/Bundle;)V

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 85
    .line 86
    invoke-static {v0}, Lcom/intsig/camscanner/web/UrlUtil;->Oooo8o0〇(Landroid/content/Context;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v1

    .line 90
    invoke-static {v0, v1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    :cond_2
    :goto_1
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O88()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "startQrCodeMode"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o0ooO(Landroidx/fragment/app/Fragment;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sget-object v1, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇080:Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/capture/qrcode/manage/QRBarCodePreferenceHelper;->〇〇888()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->BARCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->QRCODE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 35
    .line 36
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_QR_CODE_ONLY:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const v1, 0x138d5

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00〇8(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O880O〇()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK SUBSCRIPTION MANAGEMENT"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "subscription_management"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->o8(Landroid/content/Context;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const/4 v2, 0x0

    .line 28
    invoke-static {v0, v2, v1}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->O08000(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O8O()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "refreshDataWhenLogInOrOut"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mepage/vip/VipLevelManager;->〇080:Lcom/intsig/camscanner/mainmenu/mepage/vip/VipLevelManager;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/vip/VipLevelManager;->O8()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;->〇080:Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;->〇o00〇〇Oo()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->o0ooO()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 48
    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_2

    .line 56
    .line 57
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->o8o〇〇0O()V

    .line 66
    .line 67
    .line 68
    :cond_2
    :goto_1
    return-void
.end method

.method private final O8〇8〇O80()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK ADD WIDGET"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "add_widget"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/utils/WebUrlUtils;->O8(Landroid/content/Context;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v0, v1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OO0O()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK FAQ HELP"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "usinghelp"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Landroid/os/Bundle;

    .line 16
    .line 17
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "path"

    .line 21
    .line 22
    const-string v2, "/cs/opennormalweb"

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->〇O00(Landroid/content/Context;)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const-string v2, "url"

    .line 34
    .line 35
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const-string v2, "/web/allpage"

    .line 43
    .line 44
    invoke-virtual {v1, v2}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    instance-of v2, v1, Lcom/intsig/router/service/RouterWebService;

    .line 53
    .line 54
    if-eqz v2, :cond_0

    .line 55
    .line 56
    check-cast v1, Lcom/intsig/router/service/RouterWebService;

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v1, 0x0

    .line 60
    :goto_0
    if-eqz v1, :cond_1

    .line 61
    .line 62
    invoke-interface {v1, v0}, Lcom/intsig/router/service/RouterWebService;->startWeb(Landroid/os/Bundle;)V

    .line 63
    .line 64
    .line 65
    :cond_1
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method private final OO〇〇o0oO()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK SETTINGS SYNC"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string v0, "CSSetting"

    .line 17
    .line 18
    const-string v1, "synchron"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇8O0880()V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onSettingSyncClicked$1;

    .line 28
    .line 29
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onSettingSyncClicked$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0O0O〇〇〇0(Lkotlin/jvm/functions/Function0;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OoO〇OOo8o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->MAIN_ME_WATERFALL:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshWaterfall$1;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshWaterfall$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->O8〇o(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final Ooo8o()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipManager;->〇〇808〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "MePageFragment"

    .line 8
    .line 9
    const-string v1, "buyVip, open cs x pdf web purchase page"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 17
    .line 18
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 21
    .line 22
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x4

    .line 26
    const/4 v3, 0x0

    .line 27
    const/4 v4, 0x0

    .line 28
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipManager;->OoO8(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;IILjava/lang/Object;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇OoO0o0()V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_1
    const-string v0, "CSMyAccount"

    .line 43
    .line 44
    const-string v1, "upgrade_premium"

    .line 45
    .line 46
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 50
    .line 51
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/mepage/EduInviteHelper;->〇o00〇〇Oo()Z

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    if-eqz v2, :cond_2

    .line 58
    .line 59
    const-string v2, "&isEdu=1"

    .line 60
    .line 61
    const-string v3, "app/premiumFeatureEdu?"

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    const-string v2, "&isEdu=0"

    .line 65
    .line 66
    const-string v3, ""

    .line 67
    .line 68
    :goto_0
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 69
    .line 70
    invoke-direct {v4, v1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 71
    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/mepage/EduInviteHelper;->〇o00〇〇Oo()Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-eqz v0, :cond_3

    .line 78
    .line 79
    const-string v0, "edu"

    .line 80
    .line 81
    iput-object v0, v4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;

    .line 82
    .line 83
    :cond_3
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 84
    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    const-string v5, "&coupon_type=0"

    .line 91
    .line 92
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    .line 94
    .line 95
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-static {v0, v4, v1, v3}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇08O8o〇0(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final OooO〇()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "onSettingStudentRightsClicked"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/edu/EduBenefitHelper;->〇080:Lcom/intsig/camscanner/edu/EduBenefitHelper;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduBenefitHelper;->o〇0()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const-string v1, "CSMyAccount"

    .line 15
    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOoOOO〇O(Z)V

    .line 20
    .line 21
    .line 22
    const-string v0, "edu_benefits"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const-string v3, "cs_my_account"

    .line 36
    .line 37
    invoke-static {v2, v0, v3}, Lcom/intsig/camscanner/web/UrlUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const-string v0, "student_rights"

    .line 46
    .line 47
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o〇0OOo〇0()Z

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    if-nez v1, :cond_1

    .line 55
    .line 56
    const/16 v0, 0x2715

    .line 57
    .line 58
    invoke-static {p0, v0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-static {v2, v0}, Lcom/intsig/camscanner/web/UrlUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-static {v1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇0O〇Oo〇o()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK MORE SETTINGS"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "more_setting"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/setting_main"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "extra_which_page"

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇8〇008()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "onEduInviteBannerClicked"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "invite_now"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    sget-object v0, Lcom/intsig/camscanner/edu/EduBenefitHelper;->〇080:Lcom/intsig/camscanner/edu/EduBenefitHelper;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduBenefitHelper;->〇〇888()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduBenefitHelper;->o〇0()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOoOOO〇O(Z)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v2, "edu_banner"

    .line 46
    .line 47
    const-string v3, "cs_my_account"

    .line 48
    .line 49
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/web/UrlUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-static {v0, v1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const-string v2, "me_banner"

    .line 66
    .line 67
    invoke-static {v1, v2}, Lcom/intsig/camscanner/web/UrlUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-static {v0, v1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    :goto_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O〇〇O80o8()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK ABOUT"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "feedback"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/about"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O〇〇o8O(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O00OoO〇(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)Lcom/intsig/camscanner/databinding/FragmentMePageBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0O0O〇〇〇0(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 2
    .line 3
    const-string v1, "cs_my_account"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇80〇808〇O(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    const-string v1, "mActivity"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$startLoginWithHalfScreen$1;

    .line 16
    .line 17
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$startLoginWithHalfScreen$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇〇808〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o0Oo()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK SETTINGS ACCOUNT"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o8oO〇()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x0

    .line 13
    const/4 v2, 0x1

    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/comm/account_data/AccountPreference;->〇〇0o(Landroid/content/Context;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 28
    :goto_1
    const-string v3, "CSMyAccount"

    .line 29
    .line 30
    if-ne v0, v2, :cond_3

    .line 31
    .line 32
    const-string v0, "account_setting"

    .line 33
    .line 34
    invoke-static {v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->〇〇808〇(Landroid/content/Context;)Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇088()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Ooo8O0(Z)V

    .line 54
    .line 55
    .line 56
    :cond_2
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const-string v1, "/me/setting_main"

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const-string v1, "extra_which_page"

    .line 67
    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    goto :goto_2

    .line 76
    :cond_3
    if-nez v0, :cond_4

    .line 77
    .line 78
    const-string v0, "login_register"

    .line 79
    .line 80
    invoke-static {v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onSettingAccountClicked$1;

    .line 84
    .line 85
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onSettingAccountClicked$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 86
    .line 87
    .line 88
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0O0O〇〇〇0(Lkotlin/jvm/functions/Function0;)V

    .line 89
    .line 90
    .line 91
    :cond_4
    :goto_2
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o0〇〇00()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "CSMyAccount"

    .line 6
    .line 7
    const-string v2, "MePageFragment"

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    const-string v0, "CLICK KING KONG POINTS"

    .line 12
    .line 13
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OO:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 17
    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    const-string v0, "mPurchaseClient"

    .line 21
    .line 22
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o〇O8〇〇o()V

    .line 27
    .line 28
    .line 29
    const-string v0, "left_points"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_1
    const-string v0, "CLICK KING KONG INVOICE"

    .line 36
    .line 37
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇0oO〇oo00()V

    .line 41
    .line 42
    .line 43
    const-string v0, "invoice"

    .line 44
    .line 45
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o808o8o08()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK DOC SCAN"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "document_scan"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/scansetting"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o88()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK CARD"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->Ooo8o()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oOoO8OO〇()V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->〇00()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$1;

    .line 10
    .line 11
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 12
    .line 13
    .line 14
    new-instance v2, L〇8〇oO〇〇8o/〇080;

    .line 15
    .line 16
    invoke-direct {v2, v1}, L〇8〇oO〇〇8o/〇080;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "viewLifecycleOwner"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x0

    .line 37
    new-instance v5, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$2;

    .line 38
    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$2;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;Lkotlin/coroutines/Continuation;)V

    .line 41
    .line 42
    .line 43
    const/4 v6, 0x3

    .line 44
    const/4 v7, 0x0

    .line 45
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-static {v2}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    const/4 v5, 0x0

    .line 60
    new-instance v6, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$3;

    .line 61
    .line 62
    invoke-direct {v6, p0, v0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$addObserver$3;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;Lkotlin/coroutines/Continuation;)V

    .line 63
    .line 64
    .line 65
    const/4 v7, 0x3

    .line 66
    const/4 v8, 0x0

    .line 67
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic oO〇oo(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇o08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇08oO80o()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK MY DEVICE"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "my_hardware"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/printer/home"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v1, "which_page_type"

    .line 26
    .line 27
    const/4 v2, 0x4

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇08O〇00〇o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇o08〇()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK RECOMMEND"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "recommend"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/like"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇oo()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "onBarMessageClicked"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "/me/message"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    const-string v0, "CSMyAccount"

    .line 25
    .line 26
    const-string v1, "message_center"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->o〇0OOo〇0()V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/adapter/MePageAdapter;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->〇00()Landroidx/lifecycle/MutableLiveData;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Ljava/util/List;

    .line 23
    .line 24
    invoke-direct {v0, v1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/MePageAdapter;-><init>(Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 28
    .line 29
    .line 30
    const/16 v1, 0x1f

    .line 31
    .line 32
    new-array v1, v1, [I

    .line 33
    .line 34
    fill-array-data v1, :array_0

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇〇8O0〇8([I)V

    .line 38
    .line 39
    .line 40
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇0〇O0088o()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    .line 46
    invoke-virtual {v0, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->ooo0〇O88O(Lcom/chad/library/adapter/base/listener/OnItemChildLongClickListener;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    const/4 v1, 0x1

    .line 50
    new-array v2, v1, [I

    .line 51
    .line 52
    const v3, 0x7f0a0987

    .line 53
    .line 54
    .line 55
    const/4 v4, 0x0

    .line 56
    aput v3, v2, v4

    .line 57
    .line 58
    invoke-virtual {v0, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇0〇O0088o([I)V

    .line 59
    .line 60
    .line 61
    new-array v2, v1, [I

    .line 62
    .line 63
    const v3, 0x7f0a0988

    .line 64
    .line 65
    .line 66
    aput v3, v2, v4

    .line 67
    .line 68
    invoke-virtual {v0, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇0〇O0088o([I)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->Oo(Lcom/chad/library/adapter/base/listener/OnItemChildClickListener;)V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    if-eqz v2, :cond_2

    .line 79
    .line 80
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 81
    .line 82
    if-eqz v2, :cond_2

    .line 83
    .line 84
    invoke-virtual {v0, v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O〇OO(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    instance-of v0, v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 95
    .line 96
    if-eqz v0, :cond_1

    .line 97
    .line 98
    invoke-virtual {v2}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    const-string v3, "null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator"

    .line 103
    .line 104
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    check-cast v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 108
    .line 109
    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 110
    .line 111
    .line 112
    :cond_1
    const/4 v0, 0x0

    .line 113
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v2, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 117
    .line 118
    .line 119
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 120
    .line 121
    const/16 v1, 0x17

    .line 122
    .line 123
    if-lt v0, v1, :cond_2

    .line 124
    .line 125
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$initRecycler$1$1;

    .line 126
    .line 127
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$initRecycler$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 131
    .line 132
    .line 133
    :cond_2
    return-void

    .line 134
    nop

    .line 135
    :array_0
    .array-data 4
        0x7f0a0988
        0x7f0a0987
        0x7f0a03e4
        0x7f0a15a2
        0x7f0a15a3
        0x7f0a03e2
        0x7f0a15a0
        0x7f0a15ba
        0x7f0a19c8
        0x7f0a0c51
        0x7f0a0c53
        0x7f0a0c54
        0x7f0a0c55
        0x7f0a0c52
        0x7f0a03e5
        0x7f0a15b9
        0x7f0a15b3
        0x7f0a15b2
        0x7f0a15b8
        0x7f0a15b1
        0x7f0a15b5
        0x7f0a15b6
        0x7f0a15b4
        0x7f0a15ae
        0x7f0a1365
        0x7f0a148a
        0x7f0a15ec
        0x7f0a039e
        0x7f0a0c57
        0x7f0a08d1
        0x7f0a0982
    .end array-data
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0o88Oo〇()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "CSMyAccount"

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v0, "page_type"

    .line 10
    .line 11
    const-string v2, "work_function_upgrade"

    .line 12
    .line 13
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    invoke-static {v1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->oO()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇80〇808〇O()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    const-string v0, "mall_entrance_show"

    .line 39
    .line 40
    invoke-static {v1, v0}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    :cond_1
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0oO〇oo00()V
    .locals 3

    .line 1
    new-instance v0, Landroid/os/Bundle;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "path"

    .line 7
    .line 8
    const-string v2, "/cs/opennormalweb"

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->〇oo〇(Landroid/content/Context;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const-string v2, "url"

    .line 20
    .line 21
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const-string v2, "/web/allpage"

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    instance-of v2, v1, Lcom/intsig/router/service/RouterWebService;

    .line 39
    .line 40
    if-eqz v2, :cond_0

    .line 41
    .line 42
    check-cast v1, Lcom/intsig/router/service/RouterWebService;

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/4 v1, 0x0

    .line 46
    :goto_0
    if-eqz v1, :cond_1

    .line 47
    .line 48
    invoke-interface {v1, v0}, Lcom/intsig/router/service/RouterWebService;->startWeb(Landroid/os/Bundle;)V

    .line 49
    .line 50
    .line 51
    :cond_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0ooOOo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;Landroidx/appcompat/widget/AppCompatImageView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8O0880()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "go2SyncStateAfterLogin"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "synchron"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/sync_state"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O0o〇〇o()V
    .locals 33

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    const-string v1, "MePageFragment"

    .line 4
    .line 5
    const-string v2, "onAddNicknameClicked"

    .line 6
    .line 7
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v1, Lcom/intsig/tsapp/account/model/LoginMainArgs;

    .line 11
    .line 12
    move-object v3, v1

    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    const/4 v6, 0x0

    .line 16
    const/4 v7, 0x0

    .line 17
    const/4 v8, 0x0

    .line 18
    const/4 v9, 0x0

    .line 19
    const/4 v10, 0x0

    .line 20
    const/4 v11, 0x0

    .line 21
    const/4 v12, 0x0

    .line 22
    const/4 v13, 0x0

    .line 23
    const/4 v14, 0x0

    .line 24
    const/4 v15, 0x0

    .line 25
    const/16 v16, 0x0

    .line 26
    .line 27
    const/16 v17, 0x0

    .line 28
    .line 29
    const/16 v18, 0x0

    .line 30
    .line 31
    const/16 v19, 0x0

    .line 32
    .line 33
    const/16 v20, 0x0

    .line 34
    .line 35
    const/16 v21, 0x0

    .line 36
    .line 37
    const/16 v22, 0x0

    .line 38
    .line 39
    const/16 v23, 0x0

    .line 40
    .line 41
    const/16 v24, 0x0

    .line 42
    .line 43
    const/16 v25, 0x0

    .line 44
    .line 45
    const/16 v26, 0x0

    .line 46
    .line 47
    const/16 v27, 0x0

    .line 48
    .line 49
    const/16 v28, 0x0

    .line 50
    .line 51
    const/16 v29, 0x0

    .line 52
    .line 53
    const/16 v30, 0x0

    .line 54
    .line 55
    const v31, 0x7ffffff

    .line 56
    .line 57
    .line 58
    const/16 v32, 0x0

    .line 59
    .line 60
    invoke-direct/range {v3 .. v32}, Lcom/intsig/tsapp/account/model/LoginMainArgs;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZZLcom/intsig/tsapp/account/fragment/VerifyCodeFragment$FromWhere;ZZZLjava/lang/String;Ljava/lang/String;ZZZZZZZZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 61
    .line 62
    .line 63
    const/4 v2, 0x1

    .line 64
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->o〇O(Z)V

    .line 65
    .line 66
    .line 67
    iget-object v2, v0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 68
    .line 69
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO〇(Landroid/content/Context;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->o0ooO(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o〇O0〇()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->o〇8(Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇8〇()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇80〇808〇O()Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    if-nez v2, :cond_0

    .line 99
    .line 100
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇80〇808〇O()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object v2

    .line 104
    const-string v3, "getDecryptNickName()"

    .line 105
    .line 106
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_0
    invoke-static {}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇80()Z

    .line 111
    .line 112
    .line 113
    move-result v2

    .line 114
    if-eqz v2, :cond_1

    .line 115
    .line 116
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇0000OOO()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    const-string v3, "getWeChatName()"

    .line 121
    .line 122
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    goto :goto_0

    .line 126
    :cond_1
    const-string v2, ""

    .line 127
    .line 128
    :goto_0
    invoke-virtual {v1, v2}, Lcom/intsig/tsapp/account/model/LoginMainArgs;->〇O〇80o08O(Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    iget-object v2, v0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 132
    .line 133
    invoke-static {v2, v1}, Lcom/intsig/tsapp/account/LoginMainActivity;->OO0O(Landroid/content/Context;Lcom/intsig/tsapp/account/model/LoginMainArgs;)Landroid/content/Intent;

    .line 134
    .line 135
    .line 136
    move-result-object v1

    .line 137
    const/16 v2, 0x2713

    .line 138
    .line 139
    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 140
    .line 141
    .line 142
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇0ooOOo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8〇8000()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "/me/setting_main"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "extra_which_page"

    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method private final 〇O8〇8O0oO()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const-string v1, "CSMyAccount"

    .line 8
    .line 9
    const-string v2, "MePageFragment"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const-string v0, "START LOGIN"

    .line 14
    .line 15
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string v0, "login_register"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/16 v0, 0x2711

    .line 24
    .line 25
    invoke-static {p0, v0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const-string v0, "SETTING MY ACCOUNT"

    .line 30
    .line 31
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const-string v0, "account_setting"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "/me/setting_main"

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "extra_which_page"

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    invoke-virtual {v0, v1, v2}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇OoO0o0()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "showOVipUpgradeDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "page_type"

    .line 9
    .line 10
    const-string v1, "work_function_upgrade"

    .line 11
    .line 12
    const-string v2, "CSMyAccount"

    .line 13
    .line 14
    const-string v3, "upgrade_preimum"

    .line 15
    .line 16
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;->〇080:Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/purchase/ovip/OVipPurchaseManager;->oO80(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇Oo〇O()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "onBarCaptureClicked"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "scan_to_login_web"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O88()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o08()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "checkAndQueryPoint>>> isGpMarket = "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "MePageFragment"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->〇〇888()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->O〇O〇oO()V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oO88o()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK DOC MANAGEMENT"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "document_management"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    const-string v1, "/me/doc_management"

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oOO80o()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK VIP LEVEL CARD"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 11
    .line 12
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    .line 14
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 15
    .line 16
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 17
    .line 18
    .line 19
    const-string v2, "&coupon_type=0&member_center=1"

    .line 20
    .line 21
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇〇0o(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oO〇08o()V
    .locals 3

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK KING KONG TEAM"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "task_center"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Landroid/os/Bundle;

    .line 16
    .line 17
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "path"

    .line 21
    .line 22
    const-string v2, "/me/taskcenter"

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const-string v2, "/web/allpage"

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    instance-of v2, v1, Lcom/intsig/router/service/RouterWebService;

    .line 42
    .line 43
    if-eqz v2, :cond_0

    .line 44
    .line 45
    check-cast v1, Lcom/intsig/router/service/RouterWebService;

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const/4 v1, 0x0

    .line 49
    :goto_0
    if-eqz v1, :cond_1

    .line 50
    .line 51
    invoke-interface {v1, v0}, Lcom/intsig/router/service/RouterWebService;->startWeb(Landroid/os/Bundle;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇ooO8Ooo〇()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v1, Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;->MAIN_ME_SKIN:Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshSkin$1;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshSkin$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->O8〇o(Lcom/intsig/camscanner/ads/csAd/bean/AdMarketingEnum;Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇ooO〇000()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->oO80()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshInterval$1;

    .line 10
    .line 11
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$refreshInterval$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->OO0o〇〇(Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager$TaskInterfaceCallBack;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OO〇〇o0oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇()V
    .locals 2

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK CARD START BUY"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->Ooo8o()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇08O〇00〇o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇〇00()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/comm/account_data/AccountPreference;->〇〇0o(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "checkAndQuerySpace>>> isLogin = "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    const-string v2, "MePageFragment"

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->o〇O()V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇〇O〇()V
    .locals 4

    .line 1
    const-string v0, "MePageFragment"

    .line 2
    .line 3
    const-string v1, "CLICK ENLARGE SPACE"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSMyAccount"

    .line 9
    .line 10
    const-string v1, "enlarge_cloud_space"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl;->Oooo8o0〇(Landroid/app/Activity;)Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onEnlargeSpaceClicked$1;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment$onEnlargeSpaceClicked$1;-><init>(Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl;->〇〇8O0〇8(Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl$IExchangeListener;)V

    .line 27
    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_0

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl;->〇80〇808〇O()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 40
    .line 41
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 42
    .line 43
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 44
    .line 45
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/business/cloud_over_limit/CloudStorageControl;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 49
    .line 50
    .line 51
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O00OoO〇(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_1

    .line 12
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 13
    .line 14
    const/16 v2, 0x17

    .line 15
    .line 16
    if-lt v1, v2, :cond_3

    .line 17
    .line 18
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    .line 23
    .line 24
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 28
    .line 29
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-nez p1, :cond_2

    .line 34
    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    const/4 p1, -0x1

    .line 38
    if-ne v0, p1, :cond_1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    const v1, 0x7f0601e0

    .line 50
    .line 51
    .line 52
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-static {p1, v0}, Lcom/intsig/utils/StatusBarUtil;->〇o〇(Landroid/view/Window;I)V

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 67
    .line 68
    const v1, 0x7f0602f5

    .line 69
    .line 70
    .line 71
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-static {p1, v0}, Lcom/intsig/utils/StatusBarUtil;->〇o〇(Landroid/view/Window;I)V

    .line 76
    .line 77
    .line 78
    :cond_3
    :goto_1
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public O0o〇O0〇(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 7
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .line 1
    const-string v0, "adapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "view"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    const-string v0, "MePageFragment"

    .line 20
    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    const-string p1, "click too fast."

    .line 24
    .line 25
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v1, "onItemChildClick >>> position = "

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    sparse-switch p1, :sswitch_data_0

    .line 54
    .line 55
    .line 56
    const-string p1, "NO ITEM CHILD CLICKED"

    .line 57
    .line 58
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    goto/16 :goto_0

    .line 62
    .line 63
    :sswitch_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇oOO80o()V

    .line 64
    .line 65
    .line 66
    goto/16 :goto_0

    .line 67
    .line 68
    :sswitch_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o〇08oO80o()V

    .line 69
    .line 70
    .line 71
    goto/16 :goto_0

    .line 72
    .line 73
    :sswitch_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇oOO80o()V

    .line 74
    .line 75
    .line 76
    goto/16 :goto_0

    .line 77
    .line 78
    :sswitch_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OO〇〇o0oO()V

    .line 79
    .line 80
    .line 81
    goto/16 :goto_0

    .line 82
    .line 83
    :sswitch_4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O880O〇()V

    .line 84
    .line 85
    .line 86
    goto/16 :goto_0

    .line 87
    .line 88
    :sswitch_5
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o〇o08〇()V

    .line 89
    .line 90
    .line 91
    goto/16 :goto_0

    .line 92
    .line 93
    :sswitch_6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O〇0O〇Oo〇o()V

    .line 94
    .line 95
    .line 96
    goto/16 :goto_0

    .line 97
    .line 98
    :sswitch_7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OO0O()V

    .line 99
    .line 100
    .line 101
    goto/16 :goto_0

    .line 102
    .line 103
    :sswitch_8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o808o8o08()V

    .line 104
    .line 105
    .line 106
    goto/16 :goto_0

    .line 107
    .line 108
    :sswitch_9
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇oO88o()V

    .line 109
    .line 110
    .line 111
    goto/16 :goto_0

    .line 112
    .line 113
    :sswitch_a
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8〇8〇O80()V

    .line 114
    .line 115
    .line 116
    goto/16 :goto_0

    .line 117
    .line 118
    :sswitch_b
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O〇〇O80o8()V

    .line 119
    .line 120
    .line 121
    goto/16 :goto_0

    .line 122
    .line 123
    :sswitch_c
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇O0o〇〇o()V

    .line 124
    .line 125
    .line 126
    goto/16 :goto_0

    .line 127
    .line 128
    :sswitch_d
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇O〇()V

    .line 129
    .line 130
    .line 131
    goto/16 :goto_0

    .line 132
    .line 133
    :sswitch_e
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇()V

    .line 134
    .line 135
    .line 136
    goto/16 :goto_0

    .line 137
    .line 138
    :sswitch_f
    new-instance p1, Landroid/content/Intent;

    .line 139
    .line 140
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    const-class p3, Lcom/intsig/camscanner/test/AppConfigVisualActivity;

    .line 145
    .line 146
    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 150
    .line 151
    .line 152
    goto/16 :goto_0

    .line 153
    .line 154
    :sswitch_10
    new-instance p1, Landroid/content/Intent;

    .line 155
    .line 156
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 157
    .line 158
    .line 159
    move-result-object p2

    .line 160
    const-class p3, Lcom/intsig/camscanner/test/docjson/DocJsonTestActivity;

    .line 161
    .line 162
    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 166
    .line 167
    .line 168
    goto :goto_0

    .line 169
    :sswitch_11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OooO〇()V

    .line 170
    .line 171
    .line 172
    goto :goto_0

    .line 173
    :sswitch_12
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇oO〇08o()V

    .line 174
    .line 175
    .line 176
    goto :goto_0

    .line 177
    :sswitch_13
    const-string p1, "CSMyAccount"

    .line 178
    .line 179
    const-string p2, "ocr"

    .line 180
    .line 181
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 189
    .line 190
    .line 191
    move-result-object p2

    .line 192
    invoke-static {p2}, Lcom/intsig/camscanner/web/UrlUtil;->O〇8O8〇008(Landroid/content/Context;)Ljava/lang/String;

    .line 193
    .line 194
    .line 195
    move-result-object p2

    .line 196
    invoke-static {p1, p2}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    goto :goto_0

    .line 200
    :sswitch_14
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O0〇()V

    .line 201
    .line 202
    .line 203
    goto :goto_0

    .line 204
    :sswitch_15
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0〇〇00()V

    .line 205
    .line 206
    .line 207
    goto :goto_0

    .line 208
    :sswitch_16
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O08〇()V

    .line 209
    .line 210
    .line 211
    goto :goto_0

    .line 212
    :sswitch_17
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o〇oo()V

    .line 213
    .line 214
    .line 215
    goto :goto_0

    .line 216
    :sswitch_18
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇Oo〇O()V

    .line 217
    .line 218
    .line 219
    goto :goto_0

    .line 220
    :sswitch_19
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O0O0〇()V

    .line 221
    .line 222
    .line 223
    goto :goto_0

    .line 224
    :sswitch_1a
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O〇8〇008()V

    .line 225
    .line 226
    .line 227
    goto :goto_0

    .line 228
    :sswitch_1b
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o0Oo()V

    .line 229
    .line 230
    .line 231
    goto :goto_0

    .line 232
    :sswitch_1c
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇O8〇8O0oO()V

    .line 233
    .line 234
    .line 235
    goto :goto_0

    .line 236
    :sswitch_1d
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->o88()V

    .line 237
    .line 238
    .line 239
    goto :goto_0

    .line 240
    :sswitch_1e
    const-string p1, "on click enterprise mall"

    .line 241
    .line 242
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    sget-object v1, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇080:Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;

    .line 246
    .line 247
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 248
    .line 249
    .line 250
    move-result-object v2

    .line 251
    const/4 v3, 0x0

    .line 252
    const/4 v4, 0x0

    .line 253
    const/4 v5, 0x6

    .line 254
    const/4 v6, 0x0

    .line 255
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;->〇O8o08O(Lcom/intsig/camscanner/gift/interval/IntervalTaskStateManager;Landroid/content/Context;Ljava/lang/String;Lcom/intsig/camscanner/ads/adinterface/OnOpenYouZanListener;ILjava/lang/Object;)V

    .line 256
    .line 257
    .line 258
    :goto_0
    return-void

    .line 259
    :sswitch_data_0
    .sparse-switch
        0x7f0a039e -> :sswitch_1e
        0x7f0a03e2 -> :sswitch_1d
        0x7f0a03e4 -> :sswitch_1c
        0x7f0a03e5 -> :sswitch_1b
        0x7f0a08d1 -> :sswitch_1a
        0x7f0a0982 -> :sswitch_19
        0x7f0a0987 -> :sswitch_18
        0x7f0a0988 -> :sswitch_17
        0x7f0a0c51 -> :sswitch_16
        0x7f0a0c52 -> :sswitch_15
        0x7f0a0c53 -> :sswitch_14
        0x7f0a0c54 -> :sswitch_13
        0x7f0a0c55 -> :sswitch_12
        0x7f0a0c57 -> :sswitch_11
        0x7f0a1365 -> :sswitch_10
        0x7f0a148a -> :sswitch_f
        0x7f0a15a0 -> :sswitch_e
        0x7f0a15a2 -> :sswitch_d
        0x7f0a15a3 -> :sswitch_c
        0x7f0a15ae -> :sswitch_b
        0x7f0a15b1 -> :sswitch_a
        0x7f0a15b2 -> :sswitch_9
        0x7f0a15b3 -> :sswitch_8
        0x7f0a15b4 -> :sswitch_7
        0x7f0a15b5 -> :sswitch_6
        0x7f0a15b6 -> :sswitch_5
        0x7f0a15b8 -> :sswitch_4
        0x7f0a15b9 -> :sswitch_3
        0x7f0a15ba -> :sswitch_2
        0x7f0a15ec -> :sswitch_1
        0x7f0a19c8 -> :sswitch_0
    .end sparse-switch
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final O〇080〇o0(II)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v1, "onPageChanged "

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string p1, " "

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const-string v0, "MePageFragment"

    .line 34
    .line 35
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const/4 p1, 0x3

    .line 39
    if-ne p2, p1, :cond_1

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->o〇O()V

    .line 46
    .line 47
    .line 48
    :cond_1
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public beforeInitialize()V
    .locals 4

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->beforeInitialize()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 12
    .line 13
    invoke-direct {v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 14
    .line 15
    .line 16
    sget-object v3, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSAccountSetting:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 17
    .line 18
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OO:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string p1, "MePageFragment"

    .line 2
    .line 3
    const-string v0, "initialize"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇08O()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->oOoO8OO〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v1, "onActivityResult requestCode:"

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v1, ",resultCode:"

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    const-string v1, ",data"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p3

    .line 37
    const-string v0, "MePageFragment"

    .line 38
    .line 39
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const-string p3, "STILL NOT LOGIN"

    .line 43
    .line 44
    packed-switch p1, :pswitch_data_0

    .line 45
    .line 46
    .line 47
    const-string p1, "NO operation to handle the req code."

    .line 48
    .line 49
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    goto/16 :goto_0

    .line 53
    .line 54
    :pswitch_0
    const-string p1, "REQ_STUDENT_RIGHTS"

    .line 55
    .line 56
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-eqz p1, :cond_0

    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OooO〇()V

    .line 70
    .line 71
    .line 72
    goto/16 :goto_0

    .line 73
    .line 74
    :cond_0
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_0

    .line 78
    .line 79
    :pswitch_1
    const-string p1, "REQ_SYNC_STATE"

    .line 80
    .line 81
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    if-eqz p1, :cond_1

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇8O0880()V

    .line 95
    .line 96
    .line 97
    goto/16 :goto_0

    .line 98
    .line 99
    :cond_1
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    goto/16 :goto_0

    .line 103
    .line 104
    :pswitch_2
    const-string p1, "REQ_CODE_ADD_NICKNAME"

    .line 105
    .line 106
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    if-eqz p1, :cond_5

    .line 114
    .line 115
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 116
    .line 117
    if-eqz p1, :cond_5

    .line 118
    .line 119
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    if-eqz p1, :cond_5

    .line 124
    .line 125
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 126
    .line 127
    .line 128
    goto :goto_0

    .line 129
    :pswitch_3
    const-string p1, "REQ_KK_CLOUD_SPACE"

    .line 130
    .line 131
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 139
    .line 140
    .line 141
    move-result p1

    .line 142
    if-eqz p1, :cond_2

    .line 143
    .line 144
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇O8〇8000()V

    .line 145
    .line 146
    .line 147
    goto :goto_0

    .line 148
    :cond_2
    invoke-static {v0, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    goto :goto_0

    .line 152
    :pswitch_4
    const-string p1, "REQ_HEADER_LOGIN"

    .line 153
    .line 154
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->O〇8O8〇008()Z

    .line 158
    .line 159
    .line 160
    move-result p1

    .line 161
    if-eqz p1, :cond_3

    .line 162
    .line 163
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 164
    .line 165
    .line 166
    move-result-object p1

    .line 167
    const/4 p2, 0x0

    .line 168
    const/4 p3, 0x0

    .line 169
    invoke-static {p1, p2, p3}, Lcom/intsig/advertisement/control/AdConfigManager;->OoO8(Landroid/content/Context;ZLcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 170
    .line 171
    .line 172
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇0()Lcom/intsig/camscanner/databinding/FragmentMePageBinding;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    if-eqz p1, :cond_5

    .line 177
    .line 178
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMePageBinding;->〇OOo8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 179
    .line 180
    if-eqz p1, :cond_5

    .line 181
    .line 182
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 183
    .line 184
    .line 185
    move-result-object p1

    .line 186
    if-eqz p1, :cond_5

    .line 187
    .line 188
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 189
    .line 190
    .line 191
    goto :goto_0

    .line 192
    :pswitch_5
    const/4 p1, -0x1

    .line 193
    if-ne p2, p1, :cond_4

    .line 194
    .line 195
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 196
    .line 197
    .line 198
    move-result-object p1

    .line 199
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 200
    .line 201
    .line 202
    move-result p1

    .line 203
    if-eqz p1, :cond_4

    .line 204
    .line 205
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O88()V

    .line 206
    .line 207
    .line 208
    goto :goto_0

    .line 209
    :cond_4
    const-string p1, "REQ_QR_CODE_LOGIN NOT RESULT_OK OR NOT LOGIN"

    .line 210
    .line 211
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .line 213
    .line 214
    :cond_5
    :goto_0
    return-void

    .line 215
    :pswitch_data_0
    .packed-switch 0x2710
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final onInviteRewardGiftReceived(Lcom/intsig/camscanner/attention/AppUpdatePropertyNotice;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "unused"
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "onInviteRewardGiftReceived"

    .line 2
    .line 3
    const-string v1, "MePageFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const-string p1, "onInviteRewardGiftReceived event null"

    .line 15
    .line 16
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
.end method

.method public final onReceiveLoginFinish(Lcom/intsig/tsapp/account/model/LoginFinishEvent;)V
    .locals 3
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "onReceiveLoginFinish"

    .line 2
    .line 3
    const-string v1, "MePageFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    instance-of v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.mainactivity.MainActivity"

    .line 15
    .line 16
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->〇oO88o()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooo008()V

    .line 28
    .line 29
    .line 30
    :cond_0
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const-string p1, "onReceiveLoginFinish event null"

    .line 37
    .line 38
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onReceiveLoginOut(Lcom/intsig/camscanner/tsapp/LogoutAccountDataTask$LoginOutEvent;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "unused"
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "onReceiveLoginOut"

    .line 2
    .line 3
    const-string v1, "MePageFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    instance-of v2, v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 11
    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const-string v2, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.mainactivity.MainActivity"

    .line 15
    .line 16
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->〇oO88o()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->ooo008()V

    .line 28
    .line 29
    .line 30
    :cond_0
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const-string p1, "onReceiveLoginOut event null"

    .line 37
    .line 38
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇0o88Oo〇()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->O8O()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇〇00()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇o08()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->〇8o〇〇8080()V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->Oo0oOo〇0()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->OoO〇OOo8o()V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇ooO8Ooo〇()V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇ooO〇000()V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final onSystemMsgReceived(Lcom/intsig/camscanner/mainmenu/mepage/entity/SystemMsgEvent;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "unused"
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "onSystemMsgReceived"

    .line 2
    .line 3
    const-string v1, "MePageFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mepage/MePageFragment;->〇〇O80〇0o()Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mepage/viewmode/MePageViewModel;->〇8o〇〇8080()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string p1, "onSystemMsgReceived event null"

    .line 19
    .line 20
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public ooO(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 2
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .line 1
    const-string v0, "adapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "view"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    const-string v0, "MePageFragment"

    .line 20
    .line 21
    if-nez p2, :cond_0

    .line 22
    .line 23
    const-string p1, "click too fast."

    .line 24
    .line 25
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string p2, "null cannot be cast to non-null type com.intsig.camscanner.mainmenu.mepage.entity.IMePageType"

    .line 38
    .line 39
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    check-cast p1, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;

    .line 43
    .line 44
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;->getType()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    new-instance p2, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v1, "onItemClick >>> position = "

    .line 54
    .line 55
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string p3, "  mePageType = "

    .line 62
    .line 63
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d030a

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)Z
    .locals 0
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)Z"
        }
    .end annotation

    .line 1
    const-string p3, "adapter"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "view"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    const p2, 0x7f0a0988

    .line 16
    .line 17
    .line 18
    if-ne p1, p2, :cond_0

    .line 19
    .line 20
    new-instance p1, Landroid/content/Intent;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    const-class p3, Lcom/intsig/camscanner/doc2officeactivity/Doc2OfficeActivity;

    .line 27
    .line 28
    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    const/4 p1, 0x1

    .line 35
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.class public final Lcom/intsig/camscanner/mainmenu/mepage/adapter/MePageAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "MePageAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/MePageAdapter;-><init>(Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;",
            ">;",
            "Landroidx/lifecycle/LifecycleOwner;",
            ")V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;)V

    .line 4
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;

    const/4 v0, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageLoginOptProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 5
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageBarProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageBarProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 6
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageHeaderProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageHeaderProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 7
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageCardProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageCardProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 8
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageHeaderBarProvider;

    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageHeaderBarProvider;-><init>()V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 9
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageVipCardNewProvider;

    invoke-direct {p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageVipCardNewProvider;-><init>(Landroidx/lifecycle/LifecycleOwner;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 10
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageOVipCardProvider;

    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageOVipCardProvider;-><init>()V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 11
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageKingKongProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageKingKongProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 12
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageIntervalProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 13
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageEnterpriseMallProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageEnterpriseMallProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 14
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsTopProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsTopProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 15
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsBottomProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsBottomProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 16
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsDebugProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsDebugProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 17
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsEduInviteBannerProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSettingsEduInviteBannerProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 18
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageAreaFreeCardProvider;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageAreaFreeCardProvider;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 19
    new-instance p1, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSlideBanner;

    invoke-direct {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/provider/MePageSlideBanner;-><init>(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mepage/adapter/MePageAdapter;-><init>(Ljava/util/List;Landroidx/lifecycle/LifecycleOwner;)V

    return-void
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/camscanner/mainmenu/mepage/entity/IMePageType;->getType()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

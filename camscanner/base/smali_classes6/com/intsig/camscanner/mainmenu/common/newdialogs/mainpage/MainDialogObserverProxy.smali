.class public final Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;
.super Ljava/lang/Object;
.source "MainDialogObserverProxy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->O8:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mainFragment"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->〇〇888()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08()V
    .locals 3

    .line 1
    const-string v0, "MainDialogObserverProxy"

    .line 2
    .line 3
    const-string v1, "create MainDialogObserver"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇080:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 11
    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 13
    .line 14
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public final o〇0(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->oO80(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇080()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->O8()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->Oo08(IILandroid/content/Intent;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final 〇o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserverProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->o〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

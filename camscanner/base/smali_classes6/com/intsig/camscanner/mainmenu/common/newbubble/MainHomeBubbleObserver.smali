.class public final Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;
.super Ljava/util/Observable;
.source "MainHomeBubbleObserver.kt"

# interfaces
.implements Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/EventLifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static 〇0O:Z


# instance fields
.field private final O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Z

.field private final o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$mObserverCallback$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇080OO8〇0:Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainHomeFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mainActivity"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$mObserverCallback$1;

    .line 19
    .line 20
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$mObserverCallback$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇00O:Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$mObserverCallback$1;

    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 26
    .line 27
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;)V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string v0, "MAIN_HOME_PAGE_OPEN_ALL_BUBBLE"

    .line 37
    .line 38
    const/4 v1, 0x0

    .line 39
    invoke-virtual {p1, v0, v1}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    sput-boolean p1, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇0O:Z

    .line 44
    .line 45
    invoke-virtual {p2}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O8()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NetWorkBubbleControl;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NetWorkBubbleControl;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/LoginTipBubbleControl;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/LoginTipBubbleControl;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/StoragePermissionBubbleControl;

    .line 18
    .line 19
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/StoragePermissionBubbleControl;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SubscriptionOnHoldBubbleControl;

    .line 26
    .line 27
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SubscriptionOnHoldBubbleControl;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/CloudStorageBubbleControl;

    .line 34
    .line 35
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/CloudStorageBubbleControl;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 39
    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/PointExpiredBubbleControl;

    .line 42
    .line 43
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/PointExpiredBubbleControl;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 47
    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/PointExpireRemindSoonBubbleControl;

    .line 50
    .line 51
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/PointExpireRemindSoonBubbleControl;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 55
    .line 56
    .line 57
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/TransferToOfficeResultBubbleControl;->Oo08:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/TransferToOfficeResultBubbleControl$Companion;

    .line 58
    .line 59
    new-instance v1, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$addOnResume$1;

    .line 60
    .line 61
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver$addOnResume$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/TransferToOfficeResultBubbleControl$Companion;->〇080(Lkotlin/jvm/functions/Function1;)V

    .line 65
    .line 66
    .line 67
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/VipBindBubbleControl;

    .line 68
    .line 69
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/VipBindBubbleControl;-><init>()V

    .line 70
    .line 71
    .line 72
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 73
    .line 74
    .line 75
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NewbieRegisterBubbleControl;

    .line 76
    .line 77
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NewbieRegisterBubbleControl;-><init>()V

    .line 78
    .line 79
    .line 80
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 81
    .line 82
    .line 83
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NewbieShareBubbleControl;

    .line 84
    .line 85
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/NewbieShareBubbleControl;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->oO80(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x0

    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->OO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final OO0o〇〇〇〇0()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 2
    .line 3
    const-class v1, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/StoragePermissionBubbleControl;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇〇808〇(Ljava/lang/Class;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final notifyUserTransferResult(Lcom/intsig/camscanner/eventbus/TransferToOfficeEvent;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            ""
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    const-string v0, "MainHomeBubbleObserver"

    .line 5
    .line 6
    const-string v1, "TransferToOfficeEvent notifyUserTransferResult() "

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/TransferToOfficeResultBubbleControl;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/TransferToOfficeResultBubbleControl;-><init>(Lcom/intsig/camscanner/eventbus/TransferToOfficeEvent;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final oO80()Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o0:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeFragment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/EventLifecycleObserver$DefaultImpls;->〇o00〇〇Oo(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/EventLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/EventLifecycleObserver$DefaultImpls;->〇o〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/EventLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const-string p1, "MainHomeBubbleObserver"

    .line 10
    .line 11
    const-string v0, "onDestroy"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o〇(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->OO:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final onReceiveSyncWechatFile(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFile;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFile;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            ""
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string v0, "event"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFile;->〇080()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    if-nez p1, :cond_0

    .line 11
    .line 12
    const-string p1, "MainHomeBubbleObserver"

    .line 13
    .line 14
    const-string v0, "onReceiveSyncWechatFile>>>IMAGE"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;

    .line 20
    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;

    .line 24
    .line 25
    new-instance v0, L〇Oo/O8;

    .line 26
    .line 27
    invoke-direct {v0, p0}, L〇Oo/O8;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;)V

    .line 28
    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    invoke-direct {p1, v1, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;-><init>(ILcom/intsig/callback/Callback;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 35
    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/SyncWechatFileBubbleControl;

    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->O8(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->OO:Z

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8()V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public synthetic onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->Oo08(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public synthetic onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->o〇0(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final onSyncResult(Lcom/intsig/camscanner/eventbus/SyncEvent;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            ""
        }
    .end annotation

    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    if-nez p1, :cond_1

    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/SyncEvent;->〇080()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_3

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/eventbus/SyncEvent;->〇o00〇〇Oo()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-nez p1, :cond_2

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_2
    const-string p1, "MainHomeBubbleObserver"

    .line 27
    .line 28
    const-string v0, "onSyncResult CloudStorageBubble"

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/CloudStorageBubbleControl;

    .line 34
    .line 35
    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/bubblecontrol/CloudStorageBubbleControl;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleControl;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->O8o08O8O:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 44
    .line 45
    .line 46
    :cond_3
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇O8o08O()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇OOo8〇0:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

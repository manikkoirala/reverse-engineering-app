.class public final enum Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;
.super Ljava/lang/Enum;
.source "ToolCellEnum.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum AD_TAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum BACKUP:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum BUY_DEVICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum COUNT_NUMBER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final enum IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum INVOICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum MORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_TO_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PDF_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum PRINTER_DOC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_BOOK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_ERASE_PAPER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_ID_CARD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_TOPIC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_TRANSLATE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SCAN_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SMART_ERASE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum SMART_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum WORK_FLOW:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum WhitePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

.field public static final enum WritePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;


# instance fields
.field private appName:Ljava/lang/String;

.field private cellType:I

.field private drawableRes:I

.field private dynamicCenterIcon:I

.field private dynamicColor:I

.field private dynamicColor2:I

.field private dynamicCornerIcon:I

.field private dynamicDescString:I

.field private kingDrawableRes:I

.field private kingStringRes:I

.field private stringRes:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;
    .locals 3

    .line 1
    const/16 v0, 0x2c

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->MORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SMART_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_CARD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TRANSLATE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    const/16 v1, 0x18

    .line 142
    .line 143
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TOPIC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 144
    .line 145
    aput-object v2, v0, v1

    .line 146
    .line 147
    const/16 v1, 0x19

    .line 148
    .line 149
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BOOK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 150
    .line 151
    aput-object v2, v0, v1

    .line 152
    .line 153
    const/16 v1, 0x1a

    .line 154
    .line 155
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 156
    .line 157
    aput-object v2, v0, v1

    .line 158
    .line 159
    const/16 v1, 0x1b

    .line 160
    .line 161
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WritePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 162
    .line 163
    aput-object v2, v0, v1

    .line 164
    .line 165
    const/16 v1, 0x1c

    .line 166
    .line 167
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WhitePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 168
    .line 169
    aput-object v2, v0, v1

    .line 170
    .line 171
    const/16 v1, 0x1d

    .line 172
    .line 173
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 174
    .line 175
    aput-object v2, v0, v1

    .line 176
    .line 177
    const/16 v1, 0x1e

    .line 178
    .line 179
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ERASE_PAPER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 180
    .line 181
    aput-object v2, v0, v1

    .line 182
    .line 183
    const/16 v1, 0x1f

    .line 184
    .line 185
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 186
    .line 187
    aput-object v2, v0, v1

    .line 188
    .line 189
    const/16 v1, 0x20

    .line 190
    .line 191
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 192
    .line 193
    aput-object v2, v0, v1

    .line 194
    .line 195
    const/16 v1, 0x21

    .line 196
    .line 197
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PRINTER_DOC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 198
    .line 199
    aput-object v2, v0, v1

    .line 200
    .line 201
    const/16 v1, 0x22

    .line 202
    .line 203
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BUY_DEVICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 204
    .line 205
    aput-object v2, v0, v1

    .line 206
    .line 207
    const/16 v1, 0x23

    .line 208
    .line 209
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 210
    .line 211
    aput-object v2, v0, v1

    .line 212
    .line 213
    const/16 v1, 0x24

    .line 214
    .line 215
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SMART_ERASE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 216
    .line 217
    aput-object v2, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x25

    .line 220
    .line 221
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 222
    .line 223
    aput-object v2, v0, v1

    .line 224
    .line 225
    const/16 v1, 0x26

    .line 226
    .line 227
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WORK_FLOW:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 228
    .line 229
    aput-object v2, v0, v1

    .line 230
    .line 231
    const/16 v1, 0x27

    .line 232
    .line 233
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->COUNT_NUMBER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 234
    .line 235
    aput-object v2, v0, v1

    .line 236
    .line 237
    const/16 v1, 0x28

    .line 238
    .line 239
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INVOICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 240
    .line 241
    aput-object v2, v0, v1

    .line 242
    .line 243
    const/16 v1, 0x29

    .line 244
    .line 245
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BACKUP:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 246
    .line 247
    aput-object v2, v0, v1

    .line 248
    .line 249
    const/16 v1, 0x2a

    .line 250
    .line 251
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->AD_TAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 252
    .line 253
    aput-object v2, v0, v1

    .line 254
    .line 255
    const/16 v1, 0x2b

    .line 256
    .line 257
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 258
    .line 259
    aput-object v2, v0, v1

    .line 260
    .line 261
    return-object v0
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static constructor <clinit>()V
    .locals 47

    .line 1
    new-instance v16, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v1, "MORE"

    const/4 v2, 0x0

    const/16 v3, 0x8

    const v4, 0x7f080921

    const v5, 0x7f080921

    const v6, 0x7f130c02

    const v7, 0x7f130c02

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x7e0

    const/4 v15, 0x0

    move-object/from16 v0, v16

    invoke-direct/range {v0 .. v15}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v16, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->MORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "SMART_SCAN"

    const/16 v19, 0x1

    const/16 v20, 0x1

    const v21, 0x7f08091b

    const v22, 0x7f08091b

    const v23, 0x7f131c74

    const v24, 0x7f131c74

    const-string v25, "cs_630_scan_doc"

    const v26, 0x7f08091a

    const v27, 0x7f08091c

    const v28, 0x7f06013a

    const v29, 0x7f081141

    const v30, 0x7f131375

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v30}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SMART_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 3
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_UTILS"

    const/4 v3, 0x2

    const/4 v4, 0x2

    const v5, 0x7f080924

    const v6, 0x7f080924

    const v7, 0x7f130806

    const v8, 0x7f130806

    const/4 v9, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7e0

    const/16 v16, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 4
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "SCAN_UTILS"

    const/16 v19, 0x3

    const/16 v20, 0x3

    const v21, 0x7f080926

    const v22, 0x7f080926

    const v23, 0x7f130afc

    const v24, 0x7f130afc

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x7e0

    const/16 v32, 0x0

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_UTILS:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 5
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_TO_WORD"

    const/4 v3, 0x4

    const/16 v4, 0x65

    const v5, 0x7f080d3b

    const v6, 0x7f08092b

    const v7, 0x7f130768

    const v8, 0x7f130768

    const-string v9, "cs_614_file_08"

    const v10, 0x7f080d3a

    const v11, 0x7f080d3c

    const v12, 0x7f06013a

    const v13, 0x7f081141

    const v14, 0x7f131387

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 6
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_TO_EXCEL"

    const/16 v17, 0x5

    const/16 v18, 0x66

    const v19, 0x7f080d2f

    const v20, 0x7f080d2f

    const v21, 0x7f1309a8

    const v22, 0x7f1309a8

    const-string v23, "cs_550_camelpdf_tools_014"

    const v24, 0x7f080d2e

    const v25, 0x7f080d30

    const v26, 0x7f060122

    const v27, 0x7f08113f

    const v28, 0x7f131383

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 7
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_TO_PPT"

    const/4 v3, 0x6

    const/16 v4, 0x67

    const v5, 0x7f080d38

    const v6, 0x7f080d38

    const v7, 0x7f1309a9

    const v8, 0x7f1309a9

    const-string v9, "cs_523_pdf_ppt"

    const v10, 0x7f080d37

    const v11, 0x7f080d39

    const v12, 0x7f0601ac

    const v13, 0x7f081145

    const v14, 0x7f131377

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_TO_IMAGE_ONE_BY_ONE"

    const/16 v17, 0x7

    const/16 v18, 0x68

    const v19, 0x7f080d32

    const v20, 0x7f080d32

    const v21, 0x7f1307ff

    const v22, 0x7f1307ff

    const-string v23, "cs_551_search_02"

    const v24, 0x7f080d31

    const v25, 0x7f080d33

    const v26, 0x7f060124

    const v28, 0x7f131385

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_IMAGE_ONE_BY_ONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_TO_LONG_PICTURE"

    const/16 v3, 0x8

    const/16 v4, 0x69

    const v5, 0x7f080d35

    const v6, 0x7f080d35

    const v7, 0x7f130800

    const v8, 0x7f130800

    const-string v9, "cs_518b_pdf_long_picture"

    const v10, 0x7f080d34

    const v11, 0x7f080d36

    const v12, 0x7f06013a

    const v13, 0x7f081141

    const v14, 0x7f131384

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_TO_LONG_PICTURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 10
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_IMPORT"

    const/16 v17, 0x9

    const/16 v18, 0xc9

    const v19, 0x7f080d19

    const v20, 0x7f080920

    const v21, 0x7f130bf8

    const v22, 0x7f130bf8

    const-string v23, "cs_542_renew_6"

    const v24, 0x7f080d18

    const v25, 0x7f080d1a

    const v26, 0x7f06013a

    const v27, 0x7f081141

    const v28, 0x7f13134c

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_IMPORT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 11
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_SIGNATURE"

    const/16 v3, 0xa

    const/16 v4, 0xca

    const v5, 0x7f080d10

    const v6, 0x7f080927

    const v7, 0x7f130805

    const v8, 0x7f130805

    const-string v9, "cs_522_web_03"

    const v10, 0x7f080d0f

    const v11, 0x7f080d11

    const v12, 0x7f060124

    const v13, 0x7f08113f

    const v14, 0x7f13137c

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_SIGNATURE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 12
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_ADD_WATERMARK"

    const/16 v17, 0xb

    const/16 v18, 0xcb

    const v19, 0x7f080d1f

    const v20, 0x7f08092a

    const v21, 0x7f130808

    const v22, 0x7f130808

    const-string v23, "cs_553_link_12"

    const v24, 0x7f080d1e

    const v25, 0x7f080d20

    const v28, 0x7f131389

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ADD_WATERMARK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 13
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_MERGE"

    const/16 v3, 0xc

    const/16 v4, 0xcc

    const v5, 0x7f080d1c

    const v6, 0x7f080d1c

    const v7, 0x7f130801

    const v8, 0x7f130801

    const-string v9, "cs_528_Application_title3"

    const v10, 0x7f080d1b

    const v11, 0x7f080d1d

    const v12, 0x7f06013a

    const v13, 0x7f081141

    const v14, 0x7f131361

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_MERGE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 14
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_EXTRACT"

    const/16 v17, 0xd

    const/16 v18, 0xcd

    const v19, 0x7f080d16

    const v20, 0x7f080d16

    const v21, 0x7f130a22

    const v22, 0x7f130a22

    const-string v23, "cs_518b_pdf_extract"

    const v24, 0x7f080d15

    const v25, 0x7f080d17

    const v28, 0x7f13133e

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_EXTRACT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 15
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PDF_PAGE_ADJUST"

    const/16 v3, 0xe

    const/16 v4, 0xce

    const v5, 0x7f080d0d

    const v6, 0x7f080d0d

    const v7, 0x7f130a23

    const v8, 0x7f130a23

    const-string v9, "cs_518b_pdf_move"

    const v10, 0x7f080d0c

    const v11, 0x7f080d0e

    const v12, 0x7f060124

    const v13, 0x7f08113f

    const v14, 0x7f131372

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_PAGE_ADJUST:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 16
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "PDF_ENCRYPTION"

    const/16 v17, 0xf

    const/16 v18, 0xcf

    const v19, 0x7f080d13

    const v20, 0x7f080d13

    const v21, 0x7f130c0d

    const v22, 0x7f130c0d

    const-string v23, "cs_511_pdf_password"

    const v24, 0x7f080d12

    const v25, 0x7f080d14

    const v26, 0x7f060122

    const v27, 0x7f08113f

    const v28, 0x7f13135f

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_ENCRYPTION:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 17
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v30, "PDF_APP_ENTRANCE"

    const/16 v31, 0x10

    const/16 v32, 0xd0

    const v33, 0x7f080cec

    const v34, 0x7f080cec

    const v35, 0x7f131148

    const v36, 0x7f131148

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x7e0

    const/16 v44, 0x0

    move-object/from16 v29, v0

    invoke-direct/range {v29 .. v44}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PDF_APP_ENTRANCE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 18
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_ID_CARD"

    const/16 v3, 0x11

    const/16 v4, 0x12d

    const v5, 0x7f080d03

    const v6, 0x7f08091e

    const v7, 0x7f130b8e

    const v8, 0x7f130b8e

    const-string v9, "cs_523_newtab_IDCard"

    const v10, 0x7f080d02

    const v11, 0x7f080d04

    const v14, 0x7f13134a

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_CARD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 19
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "SCAN_OCR"

    const/16 v17, 0x12

    const/16 v18, 0x12e

    const v19, 0x7f080d07

    const v20, 0x7f080922

    const v21, 0x7f130bdf

    const v22, 0x7f130b3d

    const-string v23, "cs_542_renew_3"

    const v24, 0x7f080d06

    const v25, 0x7f080d08

    const v26, 0x7f060124

    const v28, 0x7f131369

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_OCR:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 20
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_IMAGE_RESTORE"

    const/16 v3, 0x13

    const/16 v4, 0x12f

    const v5, 0x7f080d0a

    const v6, 0x7f080923

    const v7, 0x7f130fd5

    const v8, 0x7f130fd5

    const-string v9, "cs_614_title_enhance"

    const v10, 0x7f080d09

    const v11, 0x7f080d0b

    const v12, 0x7f0601ac

    const v13, 0x7f081145

    const v14, 0x7f13136f

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_IMAGE_RESTORE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 21
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "SCAN_ID_PHOTO"

    const/16 v17, 0x14

    const/16 v18, 0x130

    const v19, 0x7f080cff

    const v20, 0x7f080925

    const v21, 0x7f130bf3

    const v22, 0x7f130bf3

    const-string v23, "cs_512_id_photo_take_title"

    const v24, 0x7f080cfe

    const v25, 0x7f080d00

    const v26, 0x7f06013a

    const v27, 0x7f081141

    const v28, 0x7f13134b

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ID_PHOTO:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 22
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v30, "SCAN_EXCEL"

    const/16 v31, 0x15

    const/16 v32, 0x131

    const v33, 0x7f080d29

    const v34, 0x7f08091d

    const v35, 0x7f130622

    const v36, 0x7f130622

    move-object/from16 v29, v0

    invoke-direct/range {v29 .. v44}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_EXCEL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 23
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_WORD"

    const/16 v3, 0x16

    const/16 v4, 0x267

    const v5, 0x7f080d3b

    const v6, 0x7f08092b

    const v7, 0x7f131ce9

    const v8, 0x7f131ce9

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7e0

    const/16 v16, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_WORD:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "SCAN_TRANSLATE"

    const/16 v19, 0x17

    const/16 v20, 0x192

    const v21, 0x7f080d23

    const v22, 0x7f080d46

    const v23, 0x7f130c85

    const v24, 0x7f130c85

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x7e0

    const/16 v32, 0x0

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TRANSLATE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_TOPIC"

    const/16 v3, 0x18

    const/16 v4, 0x132

    const v5, 0x7f080d25

    const v6, 0x7f080d25

    const v7, 0x7f130b3a

    const v8, 0x7f130b3a

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_TOPIC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "SCAN_BOOK"

    const/16 v19, 0x19

    const/16 v20, 0x133

    const v21, 0x7f080d27

    const v22, 0x7f080d28

    const v23, 0x7f131c8b

    const v24, 0x7f131c8b

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BOOK:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 27
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_PPT"

    const/16 v3, 0x1a

    const/16 v4, 0x134

    const v5, 0x7f080d2b

    const v6, 0x7f080d2b

    const v7, 0x7f131c8c

    const v8, 0x7f131c8c

    const-string v9, "cs_620_ppt"

    const v10, 0x7f080d2a

    const v11, 0x7f080d2c

    const v12, 0x7f0601ac

    const v13, 0x7f081145

    const v14, 0x7f131377

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_PPT:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 28
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "WritePad"

    const/16 v17, 0x1b

    const/16 v18, 0x26c

    const v19, 0x7f080a71

    const v20, 0x7f080a71

    const v21, 0x7f13156b

    const v22, 0x7f13156b

    const-string v23, "cs_542_renew_4"

    const v24, 0x7f080cf9

    const v25, 0x7f080cfb

    const v26, 0x7f060124

    const v27, 0x7f08113f

    const v28, 0x7f13134e

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WritePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 29
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "WhitePad"

    const/16 v3, 0x1c

    const/16 v4, 0x26d

    const v5, 0x7f080dfe

    const v6, 0x7f080dfe

    const v7, 0x7f131806

    const v8, 0x7f131806

    const-string v9, "cs_542_renew_4"

    const v10, 0x7f080cf9

    const v11, 0x7f080cfb

    const v12, 0x7f060124

    const v13, 0x7f08113f

    const v14, 0x7f13134e

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WhitePad:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 30
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "IMPORT_FROM_GALLERY"

    const/16 v17, 0x1d

    const/16 v18, 0x135

    const v19, 0x7f080cfa

    const v20, 0x7f08091f

    const v21, 0x7f1301b5

    const v22, 0x7f1301b5

    const-string v23, "cs_542_renew_4"

    move-object v15, v0

    invoke-direct/range {v15 .. v28}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->IMPORT_FROM_GALLERY:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 31
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_ERASE_PAPER"

    const/16 v3, 0x1e

    const/16 v4, 0x136

    const v5, 0x7f080928

    const v6, 0x7f080929

    const v7, 0x7f130d24

    const v8, 0x7f130d24

    const-string v9, "cs_542_renew_107"

    const v10, 0x7f080d24

    const v11, 0x7f080d26

    const v12, 0x7f0601b0

    const v13, 0x7f081145

    const v14, 0x7f13136b

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_ERASE_PAPER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 32
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v16, "QRCODE"

    const/16 v17, 0x1f

    const/16 v18, 0x259

    const v19, 0x7f080d22

    const v20, 0x7f080d22

    const v21, 0x7f130b3b

    const v22, 0x7f130b3b

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x7e0

    const/16 v30, 0x0

    move-object v15, v0

    invoke-direct/range {v15 .. v30}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->QRCODE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 33
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v32, "INNOVATION_LAB"

    const/16 v33, 0x20

    const/16 v34, 0x261

    const v35, 0x7f08092e

    const v36, 0x7f08092e

    const v37, 0x7f13150d

    const v38, 0x7f13150d

    const/16 v39, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x7e0

    const/16 v46, 0x0

    move-object/from16 v31, v0

    invoke-direct/range {v31 .. v46}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INNOVATION_LAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 34
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "PRINTER_DOC"

    const/16 v3, 0x21

    const/16 v4, 0x25b

    const v5, 0x7f080d21

    const v6, 0x7f080d21

    const v7, 0x7f130d48

    const v8, 0x7f130d48

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7e0

    const/16 v16, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->PRINTER_DOC:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 35
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "BUY_DEVICE"

    const/16 v19, 0x22

    const/16 v20, 0x25c

    const v21, 0x7f080cf7

    const v22, 0x7f080cf7

    const v23, 0x7f130d49

    const v24, 0x7f130d49

    const/16 v25, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x7e0

    const/16 v32, 0x0

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BUY_DEVICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 36
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "BARCODE_SCAN"

    const/16 v3, 0x23

    const/16 v4, 0x260

    const v5, 0x7f080555

    const v6, 0x7f080555

    const v7, 0x7f13131b

    const v8, 0x7f13131b

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BARCODE_SCAN:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 37
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "SMART_ERASE"

    const/16 v19, 0x24

    const/16 v20, 0x262

    const v21, 0x7f080d2d

    const v22, 0x7f080d2d

    const v23, 0x7f1312d7

    const v24, 0x7f1312d7

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SMART_ERASE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 38
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "SCAN_BANK_CARD_JOURNAL"

    const/16 v3, 0x25

    const/16 v4, 0x137

    const v5, 0x7f080cf6

    const v6, 0x7f080cf6

    const v7, 0x7f131757

    const v8, 0x7f131757

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->SCAN_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 39
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "WORK_FLOW"

    const/16 v19, 0x26

    const/16 v20, 0x263

    const v21, 0x7f080d3e

    const v22, 0x7f080d3e

    const v23, 0x7f13180a

    const v24, 0x7f13180a

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->WORK_FLOW:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 40
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "COUNT_NUMBER"

    const/16 v3, 0x27

    const/16 v4, 0x264

    const v5, 0x7f080cf8

    const v6, 0x7f080cf8

    const v7, 0x7f131829

    const v8, 0x7f131829

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->COUNT_NUMBER:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "INVOICE"

    const/16 v19, 0x28

    const/16 v20, 0x265

    const v21, 0x7f080d05

    const v22, 0x7f080d05

    const v23, 0x7f131065

    const v24, 0x7f131065

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->INVOICE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 42
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "BACKUP"

    const/16 v3, 0x29

    const/16 v4, 0x266

    const v5, 0x7f080cf5

    const v6, 0x7f080cf5

    const v7, 0x7f131aa6

    const v8, 0x7f131aa6

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->BACKUP:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 43
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v18, "AD_TAB"

    const/16 v19, 0x2a

    const/16 v20, 0x25a

    const/16 v21, -0x1

    const/16 v22, -0x1

    const/16 v23, -0x1

    const/16 v24, -0x1

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v32}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->AD_TAB:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 44
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    const-string v2, "NONE"

    const/16 v3, 0x2b

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v16}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    invoke-static {}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->$values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    move-result-object v0

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->$VALUES:[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Landroidx/annotation/ColorRes;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII",
            "Ljava/lang/String;",
            "IIIII)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput p3, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->cellType:I

    .line 3
    iput p4, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->drawableRes:I

    .line 4
    iput p5, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingDrawableRes:I

    .line 5
    iput p6, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->stringRes:I

    .line 6
    iput p7, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingStringRes:I

    .line 7
    iput-object p8, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->appName:Ljava/lang/String;

    .line 8
    iput p9, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCenterIcon:I

    .line 9
    iput p10, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCornerIcon:I

    .line 10
    iput p11, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor:I

    .line 11
    iput p12, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor2:I

    .line 12
    iput p13, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicDescString:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 16

    move/from16 v0, p14

    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object v10, v1

    goto :goto_0

    :cond_0
    move-object/from16 v10, p8

    :goto_0
    and-int/lit8 v1, v0, 0x40

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    const/4 v11, 0x0

    goto :goto_1

    :cond_1
    move/from16 v11, p9

    :goto_1
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_2

    const/4 v12, 0x0

    goto :goto_2

    :cond_2
    move/from16 v12, p10

    :goto_2
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_3

    const/4 v13, 0x0

    goto :goto_3

    :cond_3
    move/from16 v13, p11

    :goto_3
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_4

    const/4 v14, 0x0

    goto :goto_4

    :cond_4
    move/from16 v14, p12

    :goto_4
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v15, 0x0

    goto :goto_5

    :cond_5
    move/from16 v15, p13

    :goto_5
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    .line 13
    invoke-direct/range {v2 .. v15}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;-><init>(Ljava/lang/String;IIIIIILjava/lang/String;IIIII)V

    return-void
.end method

.method public static final getDrawableRes(I)I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->〇080(I)I

    move-result p0

    return p0
.end method

.method public static final getKingKongDrawableRes(I)I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->〇o00〇〇Oo(I)I

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    return p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final getKingStringRes(I)I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->〇o〇(I)I

    move-result p0

    return p0
.end method

.method public static final getStringRes(I)I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->O8(I)I

    move-result p0

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->$VALUES:[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getAppName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->appName:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getCellType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->cellType:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDrawableRes()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->drawableRes:I

    return v0
.end method

.method public final getDynamicCenterIcon()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCenterIcon:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDynamicColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDynamicColor2()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor2:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDynamicCornerIcon()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCornerIcon:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getDynamicDescString()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicDescString:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getKingDrawableRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingDrawableRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getKingStringRes()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingStringRes:I

    return v0
.end method

.method public final getStringRes()I
    .locals 1

    .line 2
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->stringRes:I

    return v0
.end method

.method public final setAppName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->appName:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setCellType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->cellType:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDrawableRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->drawableRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDynamicCenterIcon(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCenterIcon:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDynamicColor(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDynamicColor2(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicColor2:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDynamicCornerIcon(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicCornerIcon:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setDynamicDescString(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->dynamicDescString:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setKingDrawableRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingDrawableRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setKingStringRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->kingStringRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setStringRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->stringRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

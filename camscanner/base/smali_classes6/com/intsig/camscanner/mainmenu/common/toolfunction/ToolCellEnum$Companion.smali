.class public final Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;
.super Ljava/lang/Object;
.source "ToolCellEnum.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final O8(I)I
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getCellType()I

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    if-ne v4, p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getStringRes()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getStringRes()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇080(I)I
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getCellType()I

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    if-ne v4, p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getDrawableRes()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getDrawableRes()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇o00〇〇Oo(I)I
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getCellType()I

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    if-ne v4, p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getKingDrawableRes()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getKingDrawableRes()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇o〇(I)I
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->values()[Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    array-length v1, v0

    .line 6
    const/4 v2, 0x0

    .line 7
    :goto_0
    if-ge v2, v1, :cond_1

    .line 8
    .line 9
    aget-object v3, v0, v2

    .line 10
    .line 11
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getCellType()I

    .line 12
    .line 13
    .line 14
    move-result v4

    .line 15
    if-ne v4, p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getKingStringRes()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    return p1

    .line 22
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->NONE:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->getKingStringRes()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    return p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public final Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;
.super Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;
.source "MainHomeBubbleAction.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;->Oo08:Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "observerCallback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "MainHomeBubbleAction"

    .line 7
    .line 8
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public Oooo8o0〇()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/mainmenu/common/newbubble/MainHomeBubbleObserver;->〇0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇8o8o〇(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->OO0o〇〇()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    new-instance v1, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string p1, " occur dismiss()"

    .line 30
    .line 31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/MainHomeBubbleAction;->Oooo8o0〇()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_0

    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V

    .line 48
    .line 49
    .line 50
    :cond_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

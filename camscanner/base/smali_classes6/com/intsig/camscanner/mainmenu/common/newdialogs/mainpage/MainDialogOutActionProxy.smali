.class public final Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;
.super Ljava/lang/Object;
.source "MainDialogOutActionProxy.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method


# virtual methods
.method public O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogAction;->〇〇808〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;->〇〇888(Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogControl;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;->oO80()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogAction;->OoO8()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

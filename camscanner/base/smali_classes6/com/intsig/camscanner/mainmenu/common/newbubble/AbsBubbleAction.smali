.class public Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;
.super Ljava/lang/Object;
.source "AbsBubbleAction.kt"

# interfaces
.implements Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleAction;


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction$mOnDismissListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "observerCallback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "TAG"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction$mOnDismissListener$1;

    .line 19
    .line 20
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction$mOnDismissListener$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction$mOnDismissListener$1;

    .line 24
    .line 25
    new-instance p1, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 31
    .line 32
    return-void
    .line 33
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇〇888(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v3, "PLEASE ADD "

    .line 17
    .line 18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-eqz v1, :cond_2

    .line 42
    .line 43
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    check-cast v1, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-static {v2, v3}, Lcom/intsig/utils/CommonUtil;->〇〇8O0〇8(FF)Z

    .line 58
    .line 59
    .line 60
    move-result v2

    .line 61
    if-nez v2, :cond_1

    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    if-eqz v2, :cond_0

    .line 76
    .line 77
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 83
    .line 84
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    .line 86
    .line 87
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 88
    .line 89
    new-instance v0, L〇Oo/〇080;

    .line 90
    .line 91
    invoke-direct {v0}, L〇Oo/〇080;-><init>()V

    .line 92
    .line 93
    .line 94
    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->〇oo〇(Ljava/util/List;Ljava/util/Comparator;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇O8o08O(Ljava/lang/Class;)Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;",
            ">;)",
            "Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    return-object v1

    .line 30
    :cond_1
    const/4 p1, 0x0

    .line 31
    return-object p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇〇888(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    invoke-static {p0, p1}, Ljava/lang/Float;->compare(FF)I

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    return p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->oO80()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "tryRemove setVisible false"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 20
    .line 21
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->〇o00〇〇Oo()Lcom/intsig/owlery/MessageView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    const/4 v1, 0x0

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final OO0o〇〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)Z
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oooo8o0〇()Z
    .locals 0

    const/4 p0, 0x0

    throw p0
.end method

.method public oO80(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->getCurrentActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇o〇(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleAction;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->o〇0()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_2

    .line 24
    .line 25
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleAction;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 30
    .line 31
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    new-instance v3, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v2, " isAsyncPass: "

    .line 48
    .line 49
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->Oooo8o0〇()Z

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    if-nez v1, :cond_1

    .line 67
    .line 68
    if-eqz v0, :cond_6

    .line 69
    .line 70
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 73
    .line 74
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->getCurrentActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->O8(Ljava/lang/ref/WeakReference;Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleAction;)V

    .line 82
    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->Oooo8o0〇()Z

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    if-nez v2, :cond_4

    .line 90
    .line 91
    if-eqz v1, :cond_3

    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 95
    .line 96
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    .line 105
    .line 106
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .line 108
    .line 109
    const-string v2, "Don\'t Add "

    .line 110
    .line 111
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    goto :goto_2

    .line 125
    :cond_4
    :goto_0
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->OO0o〇〇(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleAction;)Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    if-eqz v0, :cond_5

    .line 130
    .line 131
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V

    .line 135
    .line 136
    .line 137
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 138
    .line 139
    goto :goto_1

    .line 140
    :cond_5
    const/4 p1, 0x0

    .line 141
    :goto_1
    if-nez p1, :cond_6

    .line 142
    .line 143
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 144
    .line 145
    const-string v0, "add fail"

    .line 146
    .line 147
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    :cond_6
    :goto_2
    return-void
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇080()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇80〇808〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "checkShow"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    monitor-enter p0

    .line 9
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->getCurrentActivity()Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 18
    .line 19
    const-string v1, "activity null. Show Fail."

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    .line 23
    .line 24
    monitor-exit p0

    .line 25
    return-void

    .line 26
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_1

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 33
    .line 34
    const-string v1, "activity destroyed. Show Fail."

    .line 35
    .line 36
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 37
    .line 38
    .line 39
    monitor-exit p0

    .line 40
    return-void

    .line 41
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 42
    .line 43
    invoke-interface {v1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->〇o00〇〇Oo()Lcom/intsig/owlery/MessageView;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    if-nez v1, :cond_2

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 50
    .line 51
    const-string v1, "messageView is null"

    .line 52
    .line 53
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    .line 55
    .line 56
    monitor-exit p0

    .line 57
    return-void

    .line 58
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 59
    .line 60
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    if-nez v2, :cond_3

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 67
    .line 68
    const-string v1, "NO bubble in list. Show Fail."

    .line 69
    .line 70
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 71
    .line 72
    .line 73
    monitor-exit p0

    .line 74
    return-void

    .line 75
    :cond_3
    :try_start_4
    iget-object v2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 76
    .line 77
    invoke-interface {v2}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->〇080()Z

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    if-eqz v2, :cond_4

    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 84
    .line 85
    const-string v1, "Now, On Pausing. Show Fail."

    .line 86
    .line 87
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 88
    .line 89
    .line 90
    monitor-exit p0

    .line 91
    return-void

    .line 92
    :cond_4
    const/4 v2, 0x0

    .line 93
    :try_start_5
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 94
    .line 95
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 96
    .line 97
    .line 98
    move-result-object v3

    .line 99
    const-string v4, "mBubbleControlList.iterator()"

    .line 100
    .line 101
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    :cond_5
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    if-eqz v4, :cond_7

    .line 109
    .line 110
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    const-string v5, "itr.next()"

    .line 115
    .line 116
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    check-cast v4, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 120
    .line 121
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇〇888(Landroidx/fragment/app/FragmentActivity;)Z

    .line 122
    .line 123
    .line 124
    move-result v5

    .line 125
    if-eqz v5, :cond_5

    .line 126
    .line 127
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 128
    .line 129
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 130
    .line 131
    .line 132
    move-result-object v6

    .line 133
    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v6

    .line 137
    new-instance v7, Ljava/lang/StringBuilder;

    .line 138
    .line 139
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .line 141
    .line 142
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    const-string v6, " is removed now."

    .line 146
    .line 147
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v6

    .line 154
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇80〇808〇O()Z

    .line 158
    .line 159
    .line 160
    move-result v5

    .line 161
    if-eqz v5, :cond_6

    .line 162
    .line 163
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 164
    .line 165
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 166
    .line 167
    .line 168
    move-result-object v4

    .line 169
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v4

    .line 173
    new-instance v6, Ljava/lang/StringBuilder;

    .line 174
    .line 175
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 176
    .line 177
    .line 178
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    .line 180
    .line 181
    const-string v4, " hide view."

    .line 182
    .line 183
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    .line 185
    .line 186
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 187
    .line 188
    .line 189
    move-result-object v4

    .line 190
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 194
    .line 195
    .line 196
    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 197
    .line 198
    .line 199
    goto :goto_0

    .line 200
    :catch_0
    move-exception v3

    .line 201
    :try_start_6
    iget-object v4, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 202
    .line 203
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 204
    .line 205
    .line 206
    :cond_7
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 207
    .line 208
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 209
    .line 210
    .line 211
    move-result v3

    .line 212
    if-nez v3, :cond_8

    .line 213
    .line 214
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 215
    .line 216
    const-string v1, "NO bubble in list. Show Fail."

    .line 217
    .line 218
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 219
    .line 220
    .line 221
    monitor-exit p0

    .line 222
    return-void

    .line 223
    :cond_8
    :try_start_7
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 224
    .line 225
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 226
    .line 227
    .line 228
    move-result-object v3

    .line 229
    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 230
    .line 231
    .line 232
    move-result v4

    .line 233
    if-eqz v4, :cond_a

    .line 234
    .line 235
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 236
    .line 237
    .line 238
    move-result-object v4

    .line 239
    check-cast v4, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 240
    .line 241
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇80〇808〇O()Z

    .line 242
    .line 243
    .line 244
    move-result v5

    .line 245
    if-eqz v5, :cond_9

    .line 246
    .line 247
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 248
    .line 249
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 250
    .line 251
    .line 252
    move-result-object v1

    .line 253
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v1

    .line 257
    new-instance v2, Ljava/lang/StringBuilder;

    .line 258
    .line 259
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .line 261
    .line 262
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    .line 264
    .line 265
    const-string v1, " is showing now, Show Fail."

    .line 266
    .line 267
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    .line 269
    .line 270
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 271
    .line 272
    .line 273
    move-result-object v1

    .line 274
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 275
    .line 276
    .line 277
    monitor-exit p0

    .line 278
    return-void

    .line 279
    :cond_a
    :try_start_8
    iget-object v3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8:Ljava/util/ArrayList;

    .line 280
    .line 281
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 282
    .line 283
    .line 284
    move-result-object v3

    .line 285
    :cond_b
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 286
    .line 287
    .line 288
    move-result v4

    .line 289
    if-eqz v4, :cond_e

    .line 290
    .line 291
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 292
    .line 293
    .line 294
    move-result-object v4

    .line 295
    check-cast v4, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 296
    .line 297
    const-string v5, "control"

    .line 298
    .line 299
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    .line 301
    .line 302
    invoke-virtual {p0, v4}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)Z

    .line 303
    .line 304
    .line 305
    move-result v5

    .line 306
    const/4 v6, 0x1

    .line 307
    if-eqz v5, :cond_c

    .line 308
    .line 309
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇o00〇〇Oo(Landroid/content/Context;)Z

    .line 310
    .line 311
    .line 312
    move-result v5

    .line 313
    if-eqz v5, :cond_c

    .line 314
    .line 315
    const/4 v5, 0x1

    .line 316
    goto :goto_2

    .line 317
    :cond_c
    const/4 v5, 0x0

    .line 318
    :goto_2
    if-ne v5, v6, :cond_d

    .line 319
    .line 320
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction$mOnDismissListener$1;

    .line 321
    .line 322
    invoke-virtual {v4, v1, v5}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->〇8o8o〇(Lcom/intsig/owlery/MessageView;Lcom/intsig/camscanner/mainmenu/common/newbubble/mainhome/OnBubbleDismissListener;)Z

    .line 323
    .line 324
    .line 325
    move-result v5

    .line 326
    if-eqz v5, :cond_b

    .line 327
    .line 328
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 329
    .line 330
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 331
    .line 332
    .line 333
    move-result-object v1

    .line 334
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v1

    .line 338
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 339
    .line 340
    .line 341
    move-result v2

    .line 342
    new-instance v3, Ljava/lang/StringBuilder;

    .line 343
    .line 344
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .line 346
    .line 347
    const-string v4, "SHOW "

    .line 348
    .line 349
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    .line 351
    .line 352
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    .line 354
    .line 355
    const-string v1, " "

    .line 356
    .line 357
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    .line 359
    .line 360
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 361
    .line 362
    .line 363
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 364
    .line 365
    .line 366
    move-result-object v1

    .line 367
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .line 369
    .line 370
    goto :goto_3

    .line 371
    :cond_d
    if-nez v5, :cond_b

    .line 372
    .line 373
    iget-object v5, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 374
    .line 375
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 376
    .line 377
    .line 378
    move-result-object v6

    .line 379
    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 380
    .line 381
    .line 382
    move-result-object v6

    .line 383
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;->Oo08()F

    .line 384
    .line 385
    .line 386
    move-result v4

    .line 387
    new-instance v7, Ljava/lang/StringBuilder;

    .line 388
    .line 389
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 390
    .line 391
    .line 392
    const-string v8, "DON\'T SHOW "

    .line 393
    .line 394
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    .line 396
    .line 397
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    .line 399
    .line 400
    const-string v6, " "

    .line 401
    .line 402
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    .line 404
    .line 405
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 409
    .line 410
    .line 411
    move-result-object v4

    .line 412
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    .line 414
    .line 415
    goto/16 :goto_1

    .line 416
    .line 417
    :cond_e
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇o00〇〇Oo:Ljava/lang/String;

    .line 418
    .line 419
    const-string v1, "=================BUBBLE========================"

    .line 420
    .line 421
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 425
    .line 426
    monitor-exit p0

    .line 427
    return-void

    .line 428
    :catchall_0
    move-exception v0

    .line 429
    monitor-exit p0

    .line 430
    throw v0
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "newContent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080:Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/IBubbleObserverCallback;->〇o00〇〇Oo()Lcom/intsig/owlery/MessageView;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    new-instance v1, Landroid/text/SpannableString;

    .line 15
    .line 16
    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/owlery/MessageView;->setMessageContent(Landroid/text/SpannableString;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o〇(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "control"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->o〇0(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇808〇(Ljava/lang/Class;)V
    .locals 1
    .param p1    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "controlClazz"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    monitor-enter p0

    .line 7
    :try_start_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇O8o08O(Ljava/lang/Class;)Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->O8(Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleControl;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newbubble/AbsBubbleAction;->〇080()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 17
    .line 18
    .line 19
    :cond_0
    monitor-exit p0

    .line 20
    return-void

    .line 21
    :catchall_0
    move-exception p1

    .line 22
    monitor-exit p0

    .line 23
    throw p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

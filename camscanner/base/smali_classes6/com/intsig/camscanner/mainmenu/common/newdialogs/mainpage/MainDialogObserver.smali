.class public final Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;
.super Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;
.source "MainDialogObserver.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static OO〇00〇8oO:Z

.field public static oOo0:Z

.field public static final oOo〇8o008:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Z

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oOo〇8o008:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$Companion;

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    sput-boolean v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO〇00〇8oO:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mainActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mainFragment"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 17
    .line 18
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;

    .line 19
    .line 20
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇00O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;

    .line 24
    .line 25
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mDialogMainControl$2;

    .line 26
    .line 27
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mDialogMainControl$2;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)V

    .line 28
    .line 29
    .line 30
    invoke-static {p1}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->O8o08O8O:Lkotlin/Lazy;

    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string v0, "MAIN_PAGE_OPEN_ALL_BUBBLE_DIALOG"

    .line 41
    .line 42
    const/4 v1, 0x0

    .line 43
    invoke-virtual {p1, v0, v1}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    sput-boolean p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oOo0:Z

    .line 48
    .line 49
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O8ooOoo〇()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->advertise_style:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o8oO()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    if-nez v2, :cond_0

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->OO0o〇〇()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_0

    .line 26
    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdvertiseStyle;->is_show_scan_return:I

    .line 30
    .line 31
    const/4 v2, 0x1

    .line 32
    if-ne v0, v2, :cond_0

    .line 33
    .line 34
    if-nez v1, :cond_0

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v2, 0x0

    .line 38
    :goto_0
    return v2
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OoO8()V
    .locals 3

    .line 1
    const-string v0, "addOnResume"

    .line 2
    .line 3
    const-string v1, "MainDialogObserver"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O〇O〇oO()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-ne v0, v2, :cond_0

    .line 14
    .line 15
    const-string v0, "isTeamUser end addOnResume"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSecurityControl;

    .line 21
    .line 22
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSecurityControl;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    if-nez v0, :cond_1

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o800o8O()V

    .line 32
    .line 33
    .line 34
    :cond_1
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o800o8O()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/VipPopupControl;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/VipPopupControl;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseMarketingPopupControl;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseMarketingPopupControl;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/EduGroupControl;

    .line 18
    .line 19
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/EduGroupControl;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCNUnsubscribeRecallControl;

    .line 26
    .line 27
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCNUnsubscribeRecallControl;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckGetUnionMemberControl;

    .line 34
    .line 35
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckGetUnionMemberControl;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 39
    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GpWebPurchaseGiftControl;

    .line 42
    .line 43
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GpWebPurchaseGiftControl;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 47
    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/VipLevelUpgradeControl;

    .line 50
    .line 51
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/VipLevelUpgradeControl;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 55
    .line 56
    .line 57
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckGpDropCnlControl;

    .line 58
    .line 59
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckGpDropCnlControl;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 63
    .line 64
    .line 65
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/NewbieScanRewardControl;

    .line 66
    .line 67
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/NewbieScanRewardControl;-><init>()V

    .line 68
    .line 69
    .line 70
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 71
    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/LoginGuideControl;

    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 76
    .line 77
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇()Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/LoginGuideControl;-><init>(Z)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 85
    .line 86
    .line 87
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/PasswordIdentifyControl;

    .line 88
    .line 89
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇00O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;

    .line 90
    .line 91
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/PasswordIdentifyControl;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/IDialogMainObserverCallback;)V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 95
    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckRejoinBenefitControl;

    .line 98
    .line 99
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckRejoinBenefitControl;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 103
    .line 104
    .line 105
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckRejoinEightDaysControl;

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 108
    .line 109
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckRejoinEightDaysControl;-><init>(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;)V

    .line 110
    .line 111
    .line 112
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 113
    .line 114
    .line 115
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCNUnsubscribeScaffoldControl;

    .line 116
    .line 117
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCNUnsubscribeScaffoldControl;-><init>()V

    .line 118
    .line 119
    .line 120
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 121
    .line 122
    .line 123
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BindMobileControl;

    .line 124
    .line 125
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 126
    .line 127
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;->O8〇()Z

    .line 128
    .line 129
    .line 130
    move-result v1

    .line 131
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BindMobileControl;-><init>(Z)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 135
    .line 136
    .line 137
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GpFirstPremiumGiftControl;

    .line 138
    .line 139
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GpFirstPremiumGiftControl;-><init>()V

    .line 140
    .line 141
    .line 142
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 143
    .line 144
    .line 145
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/AnnualPremiumControl;

    .line 146
    .line 147
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/AnnualPremiumControl;-><init>()V

    .line 148
    .line 149
    .line 150
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 151
    .line 152
    .line 153
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CNRenewRecallControl;

    .line 154
    .line 155
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CNRenewRecallControl;-><init>()V

    .line 156
    .line 157
    .line 158
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 159
    .line 160
    .line 161
    return-void
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇O8〇〇o()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇080(Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogControl;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇O8〇〇o()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0〇O0088o()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseDocCaptureGuideControl;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseDocCaptureGuideControl;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CSProtocolsForRCNControl;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CSProtocolsForRCNControl;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/AndroidRPermissionErrorControl;

    .line 18
    .line 19
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/AndroidRPermissionErrorControl;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GoogleGdprControl;

    .line 26
    .line 27
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/GoogleGdprControl;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckEUAuthControl;

    .line 34
    .line 35
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckEUAuthControl;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 39
    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckAccountFreezeControl;

    .line 42
    .line 43
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckAccountFreezeControl;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 47
    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSecurityControl;

    .line 50
    .line 51
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSecurityControl;-><init>()V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 55
    .line 56
    .line 57
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckPirateAppControl;

    .line 58
    .line 59
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckPirateAppControl;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 63
    .line 64
    .line 65
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckReinstallTipsControl;

    .line 66
    .line 67
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckReinstallTipsControl;-><init>()V

    .line 68
    .line 69
    .line 70
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 71
    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNpsControl;

    .line 74
    .line 75
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNpsControl;-><init>()V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 79
    .line 80
    .line 81
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/SubscribeFailControl;

    .line 82
    .line 83
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/SubscribeFailControl;-><init>()V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 87
    .line 88
    .line 89
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCnPurchasePageControl;

    .line 90
    .line 91
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckCnPurchasePageControl;-><init>()V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 95
    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSeniorFolderGuideControl;

    .line 98
    .line 99
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckSeniorFolderGuideControl;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 103
    .line 104
    .line 105
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/EduBenefitControl;

    .line 106
    .line 107
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/EduBenefitControl;-><init>()V

    .line 108
    .line 109
    .line 110
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 111
    .line 112
    .line 113
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;

    .line 114
    .line 115
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 116
    .line 117
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;-><init>(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 118
    .line 119
    .line 120
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 121
    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇〇8O0〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇080OO8〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oOO8O8()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x3

    .line 8
    new-array v1, v1, [Landroid/util/Pair;

    .line 9
    .line 10
    new-instance v2, Landroid/util/Pair;

    .line 11
    .line 12
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SCANDONE_BACK:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 13
    .line 14
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v4

    .line 18
    const-string v5, "from"

    .line 19
    .line 20
    invoke-direct {v2, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    aput-object v2, v1, v4

    .line 25
    .line 26
    new-instance v2, Landroid/util/Pair;

    .line 27
    .line 28
    const-string v4, "from_part"

    .line 29
    .line 30
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v5

    .line 34
    invoke-direct {v2, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    const/4 v4, 0x1

    .line 38
    aput-object v2, v1, v4

    .line 39
    .line 40
    new-instance v2, Landroid/util/Pair;

    .line 41
    .line 42
    sget-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 43
    .line 44
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    const-string v6, "schema"

    .line 49
    .line 50
    invoke-direct {v2, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    const/4 v5, 0x2

    .line 54
    aput-object v2, v1, v5

    .line 55
    .line 56
    const-string v2, "cancel"

    .line 57
    .line 58
    invoke-static {v0, v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 59
    .line 60
    .line 61
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 62
    .line 63
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 64
    .line 65
    .line 66
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPage:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 67
    .line 68
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 69
    .line 70
    iput-object v3, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 71
    .line 72
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->SCANDONE_BACK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 73
    .line 74
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 75
    .line 76
    iput-object v4, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 79
    .line 80
    const v2, 0x1ed8ab

    .line 81
    .line 82
    .line 83
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o〇0OOo〇0(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇oo〇()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇O8〇〇o()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o00〇〇Oo()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇00O:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$mObserverCallback$1;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O〇O〇oO()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    const-string v0, "MainDialogObserver"

    .line 9
    .line 10
    const-string v1, "isTeamUser end addHomeDialogs"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseDocCaptureGuideControl;

    .line 16
    .line 17
    invoke-direct {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/BaseDocCaptureGuideControl;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    if-nez v0, :cond_1

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇0〇O0088o()V

    .line 27
    .line 28
    .line 29
    :cond_1
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇O8〇〇o()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->〇o〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(IILandroid/content/Intent;)V
    .locals 0

    .line 1
    const/16 p2, 0x8a

    .line 2
    .line 3
    if-eq p1, p2, :cond_1

    .line 4
    .line 5
    const/16 p2, 0xc8

    .line 6
    .line 7
    if-eq p1, p2, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇O8〇〇o()Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogOutActionProxy;->O8()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->O8ooOoo〇()Z

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    if-eqz p1, :cond_2

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇oOO8O8()V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x1

    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8o80(Z)V

    .line 29
    .line 30
    .line 31
    :cond_2
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final O〇8O8〇008()Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "dialogControl"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇oo〇()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 6
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->onCreate(Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 10
    .line 11
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    const/4 v2, 0x0

    .line 17
    new-instance v3, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$onCreate$1;

    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    invoke-direct {v3, p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$onCreate$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;Lkotlin/coroutines/Continuation;)V

    .line 21
    .line 22
    .line 23
    const/4 v4, 0x3

    .line 24
    const/4 v5, 0x0

    .line 25
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final onInsertFavorableEvent(Lcom/intsig/camscanner/purchase/FavorableManager$FavorableEvent;)V
    .locals 4
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    const-string p1, "MainDialogObserver"

    .line 2
    .line 3
    const-string v0, "onSyncMarkEvent"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->ooo0〇O88O()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const/4 v0, 0x3

    .line 13
    if-ne p1, v0, :cond_1

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇〇0〇88()J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    const-wide/16 v2, 0x0

    .line 20
    .line 21
    cmp-long p1, v0, v2

    .line 22
    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 26
    .line 27
    .line 28
    move-result-wide v0

    .line 29
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇O8O〇〇(J)V

    .line 30
    .line 31
    .line 32
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckShowDiscountPurchaseV2Control;

    .line 33
    .line 34
    invoke-direct {p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckShowDiscountPurchaseV2Control;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oo88o8O(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇oo〇()V

    .line 41
    .line 42
    .line 43
    :cond_1
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->onPause(Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇080OO8〇0:Z

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 6
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->onResume(Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const-string p1, "MainDialogObserver"

    .line 10
    .line 11
    const-string v0, "onResume"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 17
    .line 18
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v1, 0x0

    .line 23
    const/4 v2, 0x0

    .line 24
    new-instance v3, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$onResume$1;

    .line 25
    .line 26
    const/4 p1, 0x0

    .line 27
    invoke-direct {v3, p0, p1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$onResume$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;Lkotlin/coroutines/Continuation;)V

    .line 28
    .line 29
    .line 30
    const/4 v4, 0x3

    .line 31
    const/4 v5, 0x0

    .line 32
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 2
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇0〇〇8O(Landroid/content/Context;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->Oooo8o0〇()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 23
    .line 24
    const v1, 0x7f130234

    .line 25
    .line 26
    .line 27
    invoke-static {p1, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 28
    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇〇〇0〇〇0(Z)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 35
    .line 36
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o8〇o(Landroid/content/Context;)V

    .line 37
    .line 38
    .line 39
    invoke-static {}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->clearNormalPdfInThread()V

    .line 40
    .line 41
    .line 42
    :cond_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;->o808o8o08()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇080OO8〇0:Z

    .line 12
    .line 13
    sget-boolean v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->oOo0:Z

    .line 14
    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    sget-boolean v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO〇00〇8oO:Z

    .line 18
    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    sput-boolean v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO〇00〇8oO:Z

    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OoO8()V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OoO8()V

    .line 28
    .line 29
    .line 30
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇08O〇00〇o:Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;

    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$refreshDialogOnResume$1;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver$refreshDialogOnResume$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainInterfaceLifecycleObserver;->〇o〇(Lcom/intsig/camscanner/mainmenu/mainactivity/MainFragment;Lkotlin/jvm/functions/Function0;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇00()Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->OO:Lcom/intsig/camscanner/mainmenu/mainactivity/MainActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->〇〇8O0〇8()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/MainDialogObserver;->o〇0()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

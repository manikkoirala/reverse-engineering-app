.class public final Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;
.super Ljava/lang/Object;
.source "BubbleMsgItem.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Z

.field private OO0o〇〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field private OoO8:Z

.field private Oooo8o0〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇8O8〇008:Z

.field private o800o8O:Ljava/lang/String;

.field private oO80:Z

.field private oo88o8O:Landroid/text/SpannableString;

.field private o〇0:I

.field private o〇O8〇〇o:I

.field private 〇00:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0〇O0088o:Z

.field private 〇80〇808〇O:Z

.field private 〇8o8o〇:I

.field private 〇O00:I

.field private 〇O888o0o:J

.field private 〇O8o08O:Z

.field private 〇O〇:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇oo〇:Z

.field private 〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇808〇:Z

.field private 〇〇888:Ljava/lang/String;

.field private 〇〇8O0〇8:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "contentText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentColor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇080:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;

    const/4 p1, 0x1

    .line 5
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->O8:Z

    const/4 p2, -0x1

    .line 6
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->o〇0:I

    const-string p3, "#FFFFFF"

    .line 7
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 8
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇8o8o〇:I

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O8o08O:Z

    const-string p3, ""

    .line 10
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇:Ljava/lang/String;

    const-string p3, "#19BC9C"

    .line 11
    iput-object p3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->Oooo8o0〇:Ljava/lang/String;

    const p3, 0x7f0806a7

    .line 12
    iput p3, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O〇:I

    .line 13
    iput p2, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O00:I

    .line 14
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇0〇O0088o:Z

    .line 15
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OoO8:Z

    const-wide/16 p1, -0x1

    .line 16
    iput-wide p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O888o0o:J

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const-string p2, "#464646"

    .line 17
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;)V

    return-void
.end method


# virtual methods
.method public final O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8ooOoo〇(I)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O8o08O:Z

    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇8o8o〇:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O8〇o(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->Oooo8o0〇:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final OO0o〇〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇〇〇0()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O888o0o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->oO80:Z

    .line 3
    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇80〇808〇O:Z

    .line 5
    .line 6
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->Oo08:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O00:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OoO8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇80〇808〇O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oooo8o0〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "value"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O8o08O:Z

    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o0ooO(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OoO8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o800o8O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇〇808〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80()Landroid/text/SpannableString;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->oo88o8O:Landroid/text/SpannableString;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇0〇O0088o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->o〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇〇8O0〇8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇〇808〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇O8〇〇o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OoO8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O00:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00〇8(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->o800o8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0〇O0088o()Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇o〇:Lcom/intsig/camscanner/mainmenu/common/newbubble/OnBubbleClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇00:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O00()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O888o0o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O8o08O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->Oooo8o0〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->O〇8O8〇008:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o00〇〇Oo()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇0〇O0088o:Z

    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇O〇:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇oo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->oO80:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇8o8o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇808〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇oo〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇888()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇8O0〇8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/common/newbubble/data/BubbleMsgItem;->o〇O8〇〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;
.super Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;
.source "CheckNotificationPermissionControl.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic o〇0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final O8:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mHasRequest"

    .line 7
    .line 8
    const-string v3, "getMHasRequest()Z"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->Oo08(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->o〇0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->Oo08:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 7
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/AbsMainDialogControl;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->〇o〇:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    const-string v1, "KEY_HAS_REQUEST_NOTIFICATION_PERMISSION_646"

    .line 12
    .line 13
    const/4 v2, 0x0

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    const/4 v5, 0x6

    .line 17
    const/4 v6, 0x0

    .line 18
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/delegate/sp/SpDelegateUtilKt;->〇o〇(Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->O8:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final oo88o8O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->O8:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->o〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇o00〇〇Oo(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Ljava/lang/Boolean;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇oo〇(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->O8:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->o〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {v0, p0, v1, p1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇080(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public OO0o〇〇()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->〇oo〇(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getType()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o800o8O()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O00(Landroidx/appcompat/app/AppCompatActivity;Landroidx/lifecycle/DefaultLifecycleObserver;Lcom/intsig/camscanner/mainmenu/common/newdialogs/OnDialogDismissListener;)Z
    .locals 1
    .param p2    # Landroidx/lifecycle/DefaultLifecycleObserver;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/mainmenu/common/newdialogs/OnDialogDismissListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "dismissObserver"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "dismissListener"

    .line 7
    .line 8
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 14
    .line 15
    const/16 v0, 0x21

    .line 16
    .line 17
    if-lt p2, v0, :cond_0

    .line 18
    .line 19
    const-string p2, "android.permission.POST_NOTIFICATIONS"

    .line 20
    .line 21
    filled-new-array {p2}, [Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    new-instance v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$showInternal$1$1;

    .line 26
    .line 27
    invoke-direct {v0, p3, p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl$showInternal$1$1;-><init>(Lcom/intsig/camscanner/mainmenu/common/newdialogs/OnDialogDismissListener;Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;)V

    .line 28
    .line 29
    .line 30
    invoke-static {p1, p2, v0}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 31
    .line 32
    .line 33
    :cond_0
    const/4 p1, 0x1

    .line 34
    return p1

    .line 35
    :cond_1
    const/4 p1, 0x0

    .line 36
    return p1
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇o〇()Z
    .locals 4

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2
    .line 3
    const/16 v1, 0x21

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "CheckNotificationPermissionControl"

    .line 7
    .line 8
    if-ge v0, v1, :cond_0

    .line 9
    .line 10
    const-string v0, "sdk < 13"

    .line 11
    .line 12
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return v2

    .line 16
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇080:Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇o〇()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    const-string v0, "just installed within one day, don\'t show"

    .line 25
    .line 26
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return v2

    .line 30
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/AbsDialogControl;->o〇0()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    const/4 v1, 0x1

    .line 35
    if-ge v0, v1, :cond_2

    .line 36
    .line 37
    const-string v0, "must have one doc at least"

    .line 38
    .line 39
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return v2

    .line 43
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->oo88o8O()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_3

    .line 48
    .line 49
    const-string v0, "has request notification permission"

    .line 50
    .line 51
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    return v2

    .line 55
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialogcontrols/CheckNotificationPermissionControl;->〇o〇:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    const-string v2, "android.permission.POST_NOTIFICATIONS"

    .line 58
    .line 59
    filled-new-array {v2}, [Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-static {v0, v2}, Lcom/intsig/util/PermissionUtil;->〇oo〇(Landroid/content/Context;[Ljava/lang/String;)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    xor-int/2addr v0, v1

    .line 68
    return v0
.end method

.method public 〇〇888()F
    .locals 1

    .line 1
    const v0, 0x3f9ccccd    # 1.225f

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

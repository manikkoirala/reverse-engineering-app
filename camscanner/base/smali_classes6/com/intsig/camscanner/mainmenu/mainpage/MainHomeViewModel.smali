.class public final Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "MainHomeViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0O:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private final OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:[Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Lcom/intsig/callback/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/callback/Callback<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇0O:Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 3
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "application"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    const/16 p1, 0xe

    .line 10
    .line 11
    new-array p1, p1, [Ljava/lang/Integer;

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const/4 v2, 0x0

    .line 19
    aput-object v1, p1, v2

    .line 20
    .line 21
    const/16 v1, 0x135

    .line 22
    .line 23
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    aput-object v1, p1, v0

    .line 28
    .line 29
    const/16 v0, 0xc9

    .line 30
    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const/4 v1, 0x2

    .line 36
    aput-object v0, p1, v1

    .line 37
    .line 38
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const/4 v1, 0x3

    .line 43
    aput-object v0, p1, v1

    .line 44
    .line 45
    const/4 v0, 0x4

    .line 46
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    aput-object v1, p1, v0

    .line 51
    .line 52
    const/16 v0, 0x12d

    .line 53
    .line 54
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    const/4 v1, 0x5

    .line 59
    aput-object v0, p1, v1

    .line 60
    .line 61
    const/16 v0, 0x136

    .line 62
    .line 63
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    const/4 v1, 0x6

    .line 68
    aput-object v0, p1, v1

    .line 69
    .line 70
    const/16 v0, 0x12e

    .line 71
    .line 72
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    const/4 v1, 0x7

    .line 77
    aput-object v0, p1, v1

    .line 78
    .line 79
    const/16 v0, 0x12f

    .line 80
    .line 81
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    const/16 v1, 0x8

    .line 86
    .line 87
    aput-object v0, p1, v1

    .line 88
    .line 89
    const/16 v0, 0x65

    .line 90
    .line 91
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    const/16 v1, 0x9

    .line 96
    .line 97
    aput-object v0, p1, v1

    .line 98
    .line 99
    const/16 v0, 0x130

    .line 100
    .line 101
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    const/16 v1, 0xa

    .line 106
    .line 107
    aput-object v0, p1, v1

    .line 108
    .line 109
    const/16 v0, 0x131

    .line 110
    .line 111
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    const/16 v1, 0xb

    .line 116
    .line 117
    aput-object v0, p1, v1

    .line 118
    .line 119
    const/16 v0, 0xca

    .line 120
    .line 121
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    const/16 v1, 0xc

    .line 126
    .line 127
    aput-object v0, p1, v1

    .line 128
    .line 129
    const/16 v0, 0xcb

    .line 130
    .line 131
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    const/16 v1, 0xd

    .line 136
    .line 137
    aput-object v0, p1, v1

    .line 138
    .line 139
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o0:[Ljava/lang/Integer;

    .line 140
    .line 141
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 142
    .line 143
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 144
    .line 145
    .line 146
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 147
    .line 148
    new-instance p1, Landroidx/lifecycle/MutableLiveData;

    .line 149
    .line 150
    invoke-direct {p1}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 151
    .line 152
    .line 153
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 154
    .line 155
    new-instance p1, Ljava/util/ArrayList;

    .line 156
    .line 157
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .line 159
    .line 160
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 161
    .line 162
    new-instance p1, Ljava/util/ArrayList;

    .line 163
    .line 164
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .line 166
    .line 167
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o〇00O:Ljava/util/ArrayList;

    .line 168
    .line 169
    new-instance p1, L〇0O/OOO〇O0;

    .line 170
    .line 171
    invoke-direct {p1, p0}, L〇0O/OOO〇O0;-><init>(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;)V

    .line 172
    .line 173
    .line 174
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇080OO8〇0:Lcom/intsig/callback/Callback;

    .line 175
    .line 176
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 177
    .line 178
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇oOO8O8()Ljava/util/List;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    .line 186
    .line 187
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .line 189
    .line 190
    const-string v1, "recentDocCallbackList add "

    .line 191
    .line 192
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object p1

    .line 202
    const-string v0, "MainHomeViewModel"

    .line 203
    .line 204
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    return-void
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;Ljava/util/List;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇〇〇0〇〇0(Ljava/util/List;)V

    .line 9
    .line 10
    .line 11
    sget-object p0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p0, 0x0

    .line 15
    :goto_0
    if-nez p0, :cond_1

    .line 16
    .line 17
    const-string p0, "MainHomeViewModel"

    .line 18
    .line 19
    const-string p1, "receive RecentDocUpdate, but get null list!!!"

    .line 20
    .line 21
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;
    .locals 5

    .line 1
    const/4 v0, 0x3

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eq p1, v0, :cond_5

    .line 4
    .line 5
    const/16 v0, 0x65

    .line 6
    .line 7
    if-eq p1, v0, :cond_4

    .line 8
    .line 9
    const/16 v0, 0x136

    .line 10
    .line 11
    if-eq p1, v0, :cond_2

    .line 12
    .line 13
    const/16 v0, 0x267

    .line 14
    .line 15
    if-eq p1, v0, :cond_1

    .line 16
    .line 17
    packed-switch p1, :pswitch_data_0

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :pswitch_0
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oooo8o0〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-nez v0, :cond_6

    .line 26
    .line 27
    return-object v1

    .line 28
    :pswitch_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o〇〇Oo()Lcom/intsig/tsapp/sync/AppConfigJson$CertificateIdPhoto;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-nez v0, :cond_6

    .line 33
    .line 34
    return-object v1

    .line 35
    :pswitch_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->enableImageRestore()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-nez v0, :cond_0

    .line 44
    .line 45
    return-object v1

    .line 46
    :cond_0
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->Oo08()Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_6

    .line 51
    .line 52
    return-object v1

    .line 53
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇8()Z

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-nez v0, :cond_6

    .line 60
    .line 61
    return-object v1

    .line 62
    :cond_2
    const/4 v0, 0x0

    .line 63
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇o88(I)Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-nez v0, :cond_6

    .line 68
    .line 69
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 70
    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    if-nez v0, :cond_3

    .line 76
    .line 77
    return-object v1

    .line 78
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->showScanTools()Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-eqz v0, :cond_6

    .line 87
    .line 88
    return-object v1

    .line 89
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO80O0()Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-nez v0, :cond_6

    .line 94
    .line 95
    return-object v1

    .line 96
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->showScanTools()Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-nez v0, :cond_6

    .line 105
    .line 106
    return-object v1

    .line 107
    :cond_6
    :goto_0
    new-instance v0, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 108
    .line 109
    const/4 v2, 0x4

    .line 110
    invoke-direct {v0, v2, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;-><init>(II)V

    .line 111
    .line 112
    .line 113
    sget-object v2, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum;->Companion:Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;

    .line 114
    .line 115
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->〇o00〇〇Oo(I)I

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    invoke-virtual {p0}, Landroidx/lifecycle/AndroidViewModel;->getApplication()Landroid/app/Application;

    .line 120
    .line 121
    .line 122
    move-result-object v4

    .line 123
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/mainmenu/common/toolfunction/ToolCellEnum$Companion;->〇o〇(I)I

    .line 124
    .line 125
    .line 126
    move-result p1

    .line 127
    invoke-virtual {v4, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    const-string v2, "getApplication<Applicati\u2026num.getKingStringRes(it))"

    .line 132
    .line 133
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    if-lez v3, :cond_7

    .line 137
    .line 138
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 139
    .line 140
    .line 141
    move-result v2

    .line 142
    if-nez v2, :cond_7

    .line 143
    .line 144
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->OoO8(I)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇0〇O0088o(Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    return-object v0

    .line 151
    :cond_7
    return-object v1

    .line 152
    nop

    .line 153
    :pswitch_data_0
    .packed-switch 0x12f
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oo〇()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o0:[Ljava/lang/Integer;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->〇00(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O8〇o()V
    .locals 10

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇oo〇()Ljava/util/ArrayList;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/16 v3, 0xca

    .line 19
    .line 20
    const/16 v4, 0x136

    .line 21
    .line 22
    const/4 v5, 0x0

    .line 23
    const/4 v6, 0x6

    .line 24
    const/4 v7, 0x1

    .line 25
    if-eqz v2, :cond_a

    .line 26
    .line 27
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Ljava/lang/Integer;

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result v8

    .line 37
    const/4 v9, 0x7

    .line 38
    if-lt v8, v9, :cond_1

    .line 39
    .line 40
    goto/16 :goto_1

    .line 41
    .line 42
    :cond_1
    const-string v8, "itemType"

    .line 43
    .line 44
    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v8

    .line 51
    invoke-direct {p0, v8}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 52
    .line 53
    .line 54
    move-result-object v8

    .line 55
    invoke-static {v0, v8}, Lkotlin/collections/CollectionsKt;->o8(Ljava/lang/Iterable;Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result v9

    .line 59
    if-eqz v9, :cond_2

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O88()Z

    .line 63
    .line 64
    .line 65
    move-result v9

    .line 66
    if-eqz v9, :cond_3

    .line 67
    .line 68
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 69
    .line 70
    .line 71
    move-result v9

    .line 72
    if-ne v9, v6, :cond_3

    .line 73
    .line 74
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    if-eqz v2, :cond_0

    .line 79
    .line 80
    invoke-virtual {v2, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 81
    .line 82
    .line 83
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇O8o08O()Z

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    if-eqz v3, :cond_4

    .line 92
    .line 93
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    if-ne v3, v6, :cond_4

    .line 98
    .line 99
    const/16 v3, 0x192

    .line 100
    .line 101
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 102
    .line 103
    .line 104
    move-result-object v3

    .line 105
    if-eqz v3, :cond_4

    .line 106
    .line 107
    invoke-virtual {v3, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 108
    .line 109
    .line 110
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    .line 112
    .line 113
    goto :goto_0

    .line 114
    :cond_4
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 115
    .line 116
    .line 117
    move-result v3

    .line 118
    if-ne v3, v4, :cond_5

    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇O〇88O8O()Z

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    if-nez v3, :cond_5

    .line 125
    .line 126
    invoke-static {v5}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇o88(I)Z

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    if-eqz v3, :cond_5

    .line 131
    .line 132
    if-eqz v8, :cond_0

    .line 133
    .line 134
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 135
    .line 136
    .line 137
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    .line 139
    .line 140
    goto :goto_0

    .line 141
    :cond_5
    const/16 v3, 0x12f

    .line 142
    .line 143
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 144
    .line 145
    .line 146
    move-result v4

    .line 147
    if-ne v4, v3, :cond_6

    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇88()Z

    .line 150
    .line 151
    .line 152
    move-result v3

    .line 153
    if-nez v3, :cond_6

    .line 154
    .line 155
    if-eqz v8, :cond_0

    .line 156
    .line 157
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 158
    .line 159
    .line 160
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    .line 162
    .line 163
    goto/16 :goto_0

    .line 164
    .line 165
    :cond_6
    const/16 v3, 0x12d

    .line 166
    .line 167
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 168
    .line 169
    .line 170
    move-result v4

    .line 171
    if-ne v4, v3, :cond_7

    .line 172
    .line 173
    sget-object v3, Lcom/intsig/camscanner/ads/reward/function/FunctionType;->CERTIFICATE:Lcom/intsig/camscanner/ads/reward/function/FunctionType;

    .line 174
    .line 175
    invoke-static {v3}, Lcom/intsig/camscanner/ads/reward/function/FunctionRewardHelper;->O〇8O8〇008(Lcom/intsig/camscanner/ads/reward/function/FunctionType;)Z

    .line 176
    .line 177
    .line 178
    move-result v3

    .line 179
    if-eqz v3, :cond_7

    .line 180
    .line 181
    if-eqz v8, :cond_0

    .line 182
    .line 183
    invoke-virtual {v8, v7}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 184
    .line 185
    .line 186
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    .line 188
    .line 189
    goto/16 :goto_0

    .line 190
    .line 191
    :cond_7
    const/16 v3, 0xc9

    .line 192
    .line 193
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 194
    .line 195
    .line 196
    move-result v4

    .line 197
    if-ne v4, v3, :cond_8

    .line 198
    .line 199
    invoke-static {}, Lcom/intsig/camscanner/experiment/ImportDocOptExp;->〇080()Z

    .line 200
    .line 201
    .line 202
    move-result v3

    .line 203
    if-eqz v3, :cond_8

    .line 204
    .line 205
    if-eqz v8, :cond_0

    .line 206
    .line 207
    sget-object v2, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->〇080:Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;

    .line 208
    .line 209
    invoke-virtual {v2}, Lcom/intsig/camscanner/mainmenu/adapter/docrec/MainDocRecUtils;->Oo08()Z

    .line 210
    .line 211
    .line 212
    move-result v2

    .line 213
    invoke-virtual {v8, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 214
    .line 215
    .line 216
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    .line 218
    .line 219
    goto/16 :goto_0

    .line 220
    .line 221
    :cond_8
    if-eqz v8, :cond_0

    .line 222
    .line 223
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 224
    .line 225
    .line 226
    move-result v3

    .line 227
    if-eqz v3, :cond_9

    .line 228
    .line 229
    const/4 v3, 0x2

    .line 230
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 231
    .line 232
    .line 233
    move-result v2

    .line 234
    if-ne v2, v3, :cond_9

    .line 235
    .line 236
    invoke-interface {v0, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 237
    .line 238
    .line 239
    invoke-interface {v0, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 240
    .line 241
    .line 242
    goto/16 :goto_0

    .line 243
    .line 244
    :cond_9
    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    .line 246
    .line 247
    goto/16 :goto_0

    .line 248
    .line 249
    :cond_a
    :goto_1
    invoke-static {v7}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇o88(I)Z

    .line 250
    .line 251
    .line 252
    move-result v1

    .line 253
    const/4 v2, 0x5

    .line 254
    if-eqz v1, :cond_b

    .line 255
    .line 256
    const/16 v1, 0x133

    .line 257
    .line 258
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 259
    .line 260
    .line 261
    move-result-object v1

    .line 262
    if-eqz v1, :cond_b

    .line 263
    .line 264
    invoke-interface {v0, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 265
    .line 266
    .line 267
    :cond_b
    invoke-static {v5}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇o88(I)Z

    .line 268
    .line 269
    .line 270
    move-result v1

    .line 271
    if-eqz v1, :cond_c

    .line 272
    .line 273
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 274
    .line 275
    .line 276
    move-result-object v1

    .line 277
    if-eqz v1, :cond_c

    .line 278
    .line 279
    invoke-interface {v0, v6, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 280
    .line 281
    .line 282
    :cond_c
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 283
    .line 284
    .line 285
    move-result-object v1

    .line 286
    iget-object v1, v1, Lcom/intsig/tsapp/sync/AppConfigJson;->barcode_scan:Lcom/intsig/tsapp/sync/AppConfigJson$BarcodeScan;

    .line 287
    .line 288
    if-eqz v1, :cond_d

    .line 289
    .line 290
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson$BarcodeScan;->isInMainKinkKong()Z

    .line 291
    .line 292
    .line 293
    move-result v1

    .line 294
    if-ne v1, v7, :cond_d

    .line 295
    .line 296
    const/4 v5, 0x1

    .line 297
    :cond_d
    if-eqz v5, :cond_e

    .line 298
    .line 299
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 300
    .line 301
    .line 302
    move-result v1

    .line 303
    if-le v1, v2, :cond_e

    .line 304
    .line 305
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 306
    .line 307
    .line 308
    move-result-object v1

    .line 309
    check-cast v1, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 310
    .line 311
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->〇o00〇〇Oo()I

    .line 312
    .line 313
    .line 314
    move-result v1

    .line 315
    if-eq v1, v3, :cond_e

    .line 316
    .line 317
    const/16 v1, 0x260

    .line 318
    .line 319
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 320
    .line 321
    .line 322
    move-result-object v1

    .line 323
    if-eqz v1, :cond_e

    .line 324
    .line 325
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO00〇0o〇〇()Z

    .line 326
    .line 327
    .line 328
    move-result v3

    .line 329
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 330
    .line 331
    .line 332
    invoke-interface {v0, v2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 333
    .line 334
    .line 335
    :cond_e
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->Oooo8o0〇()Z

    .line 336
    .line 337
    .line 338
    move-result v1

    .line 339
    if-eqz v1, :cond_f

    .line 340
    .line 341
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->〇〇808〇()Z

    .line 342
    .line 343
    .line 344
    move-result v1

    .line 345
    if-eqz v1, :cond_f

    .line 346
    .line 347
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 348
    .line 349
    .line 350
    move-result v1

    .line 351
    if-le v1, v6, :cond_f

    .line 352
    .line 353
    const/16 v1, 0x262

    .line 354
    .line 355
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 356
    .line 357
    .line 358
    move-result-object v1

    .line 359
    if-eqz v1, :cond_f

    .line 360
    .line 361
    invoke-static {}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->〇o00〇〇Oo()Z

    .line 362
    .line 363
    .line 364
    move-result v2

    .line 365
    xor-int/2addr v2, v7

    .line 366
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 367
    .line 368
    .line 369
    invoke-interface {v0, v6, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 370
    .line 371
    .line 372
    :cond_f
    invoke-static {}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->OO0o〇〇()Z

    .line 373
    .line 374
    .line 375
    move-result v1

    .line 376
    if-eqz v1, :cond_10

    .line 377
    .line 378
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 379
    .line 380
    .line 381
    move-result v1

    .line 382
    if-le v1, v6, :cond_10

    .line 383
    .line 384
    const/16 v1, 0x264

    .line 385
    .line 386
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 387
    .line 388
    .line 389
    move-result-object v1

    .line 390
    if-eqz v1, :cond_10

    .line 391
    .line 392
    invoke-static {}, Lcom/intsig/camscanner/capture/count/CountNumberUtils;->oO80()Z

    .line 393
    .line 394
    .line 395
    move-result v2

    .line 396
    xor-int/2addr v2, v7

    .line 397
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;->oo88o8O(Z)V

    .line 398
    .line 399
    .line 400
    invoke-interface {v0, v6, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 401
    .line 402
    .line 403
    :cond_10
    sget-object v1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 404
    .line 405
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇8()Z

    .line 406
    .line 407
    .line 408
    move-result v1

    .line 409
    if-eqz v1, :cond_11

    .line 410
    .line 411
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 412
    .line 413
    .line 414
    move-result v1

    .line 415
    if-le v1, v6, :cond_11

    .line 416
    .line 417
    const/16 v1, 0x267

    .line 418
    .line 419
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 420
    .line 421
    .line 422
    move-result-object v1

    .line 423
    if-eqz v1, :cond_11

    .line 424
    .line 425
    invoke-interface {v0, v6, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 426
    .line 427
    .line 428
    :cond_11
    const/16 v1, 0x8

    .line 429
    .line 430
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇00(I)Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;

    .line 431
    .line 432
    .line 433
    move-result-object v1

    .line 434
    if-eqz v1, :cond_12

    .line 435
    .line 436
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    .line 438
    .line 439
    :cond_12
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 440
    .line 441
    invoke-virtual {v1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 442
    .line 443
    .line 444
    return-void
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public final Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/toolpage/entity/ToolPageItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇OOo8〇0:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o〇00O:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 6
    .line 7
    .line 8
    check-cast p1, Ljava/util/Collection;

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x1

    .line 15
    xor-int/2addr v0, v1

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/external_import/ExternalImportHelper;->〇080:Lcom/intsig/camscanner/external_import/ExternalImportHelper;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/external_import/ExternalImportHelper;->〇80〇808〇O(Z)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o〇00O:Ljava/util/ArrayList;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->oo〇()V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onCleared()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/lifecycle/ViewModel;->onCleared()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇oOO8O8()Ljava/util/List;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇080OO8〇0:Lcom/intsig/callback/Callback;

    .line 11
    .line 12
    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇080OO8〇0:Lcom/intsig/callback/Callback;

    .line 16
    .line 17
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v2, "onCleared remove "

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-string v1, "MainHomeViewModel"

    .line 35
    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final oo88o8O(Ljava/util/Set;)[Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;
    .locals 26
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)[",
            "Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    const-string v1, "selectedDocItems"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-interface/range {p1 .. p1}, Ljava/util/Set;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    sget-object v2, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇080:Lcom/intsig/camscanner/scenariodir/DocManualOperations;

    .line 13
    .line 14
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇8o8o〇(Ljava/util/Set;)Z

    .line 15
    .line 16
    .line 17
    move-result v3

    .line 18
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇0000OOO(Ljava/util/Set;)Z

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇oOO8O8(Ljava/util/Set;)Z

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    const/4 v5, 0x5

    .line 27
    new-array v14, v5, [Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 28
    .line 29
    new-instance v15, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 30
    .line 31
    const v6, 0x7f080c24

    .line 32
    .line 33
    .line 34
    const v7, 0x7f13053f

    .line 35
    .line 36
    .line 37
    const/4 v13, 0x1

    .line 38
    const/16 v16, 0x0

    .line 39
    .line 40
    if-nez v3, :cond_1

    .line 41
    .line 42
    if-eqz v2, :cond_0

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/4 v8, 0x0

    .line 46
    goto :goto_1

    .line 47
    :cond_1
    :goto_0
    const/4 v8, 0x1

    .line 48
    :goto_1
    const/4 v9, 0x0

    .line 49
    const/4 v10, 0x0

    .line 50
    const/4 v11, 0x0

    .line 51
    const/16 v12, 0x30

    .line 52
    .line 53
    const/4 v3, 0x0

    .line 54
    move-object v5, v15

    .line 55
    const/4 v0, 0x1

    .line 56
    move-object v13, v3

    .line 57
    invoke-direct/range {v5 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 58
    .line 59
    .line 60
    aput-object v15, v14, v16

    .line 61
    .line 62
    new-instance v3, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 63
    .line 64
    const v6, 0x7f0806b9

    .line 65
    .line 66
    .line 67
    const v7, 0x7f131c8a

    .line 68
    .line 69
    .line 70
    const/16 v12, 0x38

    .line 71
    .line 72
    const/4 v13, 0x0

    .line 73
    move-object v5, v3

    .line 74
    move v8, v2

    .line 75
    invoke-direct/range {v5 .. v13}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 76
    .line 77
    .line 78
    aput-object v3, v14, v0

    .line 79
    .line 80
    if-gt v1, v0, :cond_2

    .line 81
    .line 82
    const/4 v13, 0x1

    .line 83
    goto :goto_2

    .line 84
    :cond_2
    const/4 v13, 0x0

    .line 85
    :goto_2
    if-ne v13, v0, :cond_3

    .line 86
    .line 87
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 88
    .line 89
    const v18, 0x7f080b8a

    .line 90
    .line 91
    .line 92
    const v19, 0x7f131dba

    .line 93
    .line 94
    .line 95
    const/16 v20, 0x0

    .line 96
    .line 97
    const/16 v21, 0x0

    .line 98
    .line 99
    const/16 v22, 0x0

    .line 100
    .line 101
    const/16 v23, 0x0

    .line 102
    .line 103
    const/16 v24, 0x3c

    .line 104
    .line 105
    const/16 v25, 0x0

    .line 106
    .line 107
    move-object/from16 v17, v0

    .line 108
    .line 109
    invoke-direct/range {v17 .. v25}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 110
    .line 111
    .line 112
    goto :goto_6

    .line 113
    :cond_3
    if-nez v13, :cond_7

    .line 114
    .line 115
    const v3, 0x7f0809f4

    .line 116
    .line 117
    .line 118
    const v5, 0x7f1302e7

    .line 119
    .line 120
    .line 121
    if-nez v4, :cond_6

    .line 122
    .line 123
    if-nez v2, :cond_6

    .line 124
    .line 125
    sget-object v1, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->〇080:Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;

    .line 126
    .line 127
    move-object/from16 v2, p1

    .line 128
    .line 129
    check-cast v2, Ljava/lang/Iterable;

    .line 130
    .line 131
    new-instance v4, Ljava/util/ArrayList;

    .line 132
    .line 133
    const/16 v6, 0xa

    .line 134
    .line 135
    invoke-static {v2, v6}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 136
    .line 137
    .line 138
    move-result v6

    .line 139
    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 140
    .line 141
    .line 142
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 143
    .line 144
    .line 145
    move-result-object v2

    .line 146
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 147
    .line 148
    .line 149
    move-result v6

    .line 150
    if-eqz v6, :cond_4

    .line 151
    .line 152
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    move-result-object v6

    .line 156
    check-cast v6, Lcom/intsig/camscanner/datastruct/DocItem;

    .line 157
    .line 158
    invoke-virtual {v6}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 159
    .line 160
    .line 161
    move-result-wide v6

    .line 162
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 163
    .line 164
    .line 165
    move-result-object v6

    .line 166
    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 167
    .line 168
    .line 169
    goto :goto_3

    .line 170
    :cond_4
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/capture/certificates/CertificatePreviewHelper;->OoO8(Ljava/util/List;)Z

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    if-eqz v1, :cond_5

    .line 175
    .line 176
    goto :goto_4

    .line 177
    :cond_5
    const/4 v4, 0x0

    .line 178
    goto :goto_5

    .line 179
    :cond_6
    :goto_4
    const/4 v4, 0x1

    .line 180
    :goto_5
    const/4 v0, 0x0

    .line 181
    const/4 v6, 0x0

    .line 182
    const/4 v7, 0x0

    .line 183
    const/16 v8, 0x30

    .line 184
    .line 185
    const/4 v9, 0x0

    .line 186
    new-instance v10, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 187
    .line 188
    move-object v1, v10

    .line 189
    move v2, v3

    .line 190
    move v3, v5

    .line 191
    move v5, v0

    .line 192
    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 193
    .line 194
    .line 195
    move-object v0, v10

    .line 196
    :goto_6
    const/4 v1, 0x2

    .line 197
    aput-object v0, v14, v1

    .line 198
    .line 199
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 200
    .line 201
    const v3, 0x7f0806f1

    .line 202
    .line 203
    .line 204
    const v4, 0x7f1302e4

    .line 205
    .line 206
    .line 207
    const/4 v5, 0x0

    .line 208
    const/4 v6, 0x0

    .line 209
    const/4 v7, 0x0

    .line 210
    const/4 v8, 0x0

    .line 211
    const/16 v9, 0x3c

    .line 212
    .line 213
    const/4 v10, 0x0

    .line 214
    move-object v2, v0

    .line 215
    invoke-direct/range {v2 .. v10}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 216
    .line 217
    .line 218
    const/4 v1, 0x3

    .line 219
    aput-object v0, v14, v1

    .line 220
    .line 221
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;

    .line 222
    .line 223
    const v3, 0x7f080a09

    .line 224
    .line 225
    .line 226
    const v4, 0x7f130228

    .line 227
    .line 228
    .line 229
    move-object v2, v0

    .line 230
    invoke-direct/range {v2 .. v10}, Lcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabItem;-><init>(IIZIZLcom/intsig/camscanner/mainmenu/mainactivity/headfoot/BtmEditTabDotTextItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 231
    .line 232
    .line 233
    const/4 v1, 0x4

    .line 234
    aput-object v0, v14, v1

    .line 235
    .line 236
    return-object v14

    .line 237
    :cond_7
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 238
    .line 239
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 240
    .line 241
    .line 242
    throw v0
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final oo〇()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->O8o08O8O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->o〇00O:Ljava/util/ArrayList;

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    :goto_0
    return-void
    .line 21
.end method

.method public final 〇8o8o〇(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->O8o08O8O:Z

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->O8o08O8O:Z

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->oo〇()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O00()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/datastruct/DocItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 9
    .line 10
    check-cast p1, Ljava/util/Collection;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/MainHomeViewModel;->oo〇()V

    .line 16
    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camscanner/mainmenu/mainpage/SceneBannerAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "SceneBannerAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O〇08oOOO0:I

.field private final 〇00O0:Landroidx/lifecycle/LifecycleOwner;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroidx/lifecycle/LifecycleOwner;Ljava/util/List;I)V
    .locals 8
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/lifecycle/LifecycleOwner;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;",
            ">;I)V"
        }
    .end annotation

    const-string v1, "lifecycleOwner"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dataList"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p2}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;)V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/mainmenu/mainpage/SceneBannerAdapter;->〇00O0:Landroidx/lifecycle/LifecycleOwner;

    .line 4
    iput p3, p0, Lcom/intsig/camscanner/mainmenu/mainpage/SceneBannerAdapter;->O〇08oOOO0:I

    .line 5
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;

    const/16 v3, 0xe

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    move v1, p3

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v7}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 6
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerOneFunctionProvider;

    const/16 v3, 0xb

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerOneFunctionProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v7}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 7
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerTwoFunctionProvider;

    const/16 v3, 0xc

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerTwoFunctionProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v7}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 8
    new-instance v7, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;

    const/16 v3, 0xd

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v7}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroidx/lifecycle/LifecycleOwner;Ljava/util/List;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 1
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mainmenu/mainpage/SceneBannerAdapter;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/util/List;I)V

    return-void
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;->getLayout()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    const/16 p2, 0xe

    .line 17
    .line 18
    if-gt p1, p2, :cond_0

    .line 19
    .line 20
    const/16 v0, 0xb

    .line 21
    .line 22
    if-ge p1, v0, :cond_1

    .line 23
    .line 24
    :cond_0
    const/16 p1, 0xe

    .line 25
    .line 26
    :cond_1
    return p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

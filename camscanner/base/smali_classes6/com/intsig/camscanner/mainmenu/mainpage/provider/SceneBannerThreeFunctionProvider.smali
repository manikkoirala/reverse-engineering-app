.class public final Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;
.super Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;
.source "SceneBannerThreeFunctionProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8oOOo:Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:I

.field private final 〇〇08O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->o8oOOo:Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(ILandroidx/lifecycle/LifecycleOwner;II)V
    .locals 1
    .param p2    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "lifecycleOwner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;II)V

    .line 3
    iput p3, p0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->〇〇08O:I

    .line 4
    iput p4, p0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->O0O:I

    return-void
.end method

.method public synthetic constructor <init>(ILandroidx/lifecycle/LifecycleOwner;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const p4, 0x7f0d0415

    .line 1
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;-><init>(ILandroidx/lifecycle/LifecycleOwner;II)V

    return-void
.end method

.method private final o〇0OOo〇0(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V
    .locals 9

    .line 1
    const-string v0, "SceneBannerOneFunctionProvider"

    .line 2
    .line 3
    const-string v1, "refreshFunction3"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const v0, 0x7f0a03b4

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, v0}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getBg_color()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "#E5257258"

    .line 22
    .line 23
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    invoke-virtual {p0, v1, v2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->OOO〇O0(Ljava/lang/String;I)I

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    const/4 v4, 0x0

    .line 32
    const/4 v5, 0x4

    .line 33
    const/4 v6, 0x0

    .line 34
    move-object v1, p0

    .line 35
    move-object v2, v0

    .line 36
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->Oo8Oo00oo(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;Landroid/view/View;IIILjava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    const v1, 0x7f0a090f

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->getView(I)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    move-object v3, v1

    .line 47
    check-cast v3, Landroid/widget/ImageView;

    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getIcon_pic()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v4

    .line 53
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getIcon_pic_Local()I

    .line 54
    .line 55
    .line 56
    move-result v5

    .line 57
    const/4 v6, 0x0

    .line 58
    const/16 v7, 0x8

    .line 59
    .line 60
    const/4 v8, 0x0

    .line 61
    move-object v2, p0

    .line 62
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->o〇〇0〇(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;Landroid/widget/ImageView;Ljava/lang/String;IIILjava/lang/Object;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getTitle()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    const v2, 0x7f0a1488

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, v2, v1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setText(ILjava/lang/CharSequence;)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getTitle_color()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    const-string v3, "#FFFFFF"

    .line 80
    .line 81
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    invoke-virtual {p0, v1, v3}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->OOO〇O0(Ljava/lang/String;I)I

    .line 86
    .line 87
    .line 88
    move-result v1

    .line 89
    invoke-virtual {p1, v2, v1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setTextColor(II)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 90
    .line 91
    .line 92
    new-instance p1, LOO〇00〇8oO/O8;

    .line 93
    .line 94
    invoke-direct {p1, p0, p2}, LOO〇00〇8oO/O8;-><init>(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇〇0o(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$bean"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "SceneBannerOneFunctionProvider"

    .line 12
    .line 13
    const-string v1, "clFunction3 click deeplink"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    const-string v0, "it"

    .line 19
    .line 20
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;->getDeeplink_url()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p0, p2, p1}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->〇00(Landroid/view/View;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->O〇8O8〇008()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const/4 p2, 0x2

    .line 35
    new-array p2, p2, [Landroid/util/Pair;

    .line 36
    .line 37
    new-instance v0, Landroid/util/Pair;

    .line 38
    .line 39
    const-string v1, "type"

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->O8ooOoo〇()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p0

    .line 45
    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    const/4 p0, 0x0

    .line 49
    aput-object v0, p2, p0

    .line 50
    .line 51
    new-instance p0, Landroid/util/Pair;

    .line 52
    .line 53
    const-string v0, "function"

    .line 54
    .line 55
    const-string v1, "3"

    .line 56
    .line 57
    invoke-direct {p0, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    const/4 v0, 0x1

    .line 61
    aput-object p0, p2, v0

    .line 62
    .line 63
    const-string p0, "recommend_func_click"

    .line 64
    .line 65
    invoke-static {p1, p0, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 66
    .line 67
    .line 68
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->〇〇0o(Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->O0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;)V
    .locals 7
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-super {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;)V

    .line 12
    .line 13
    .line 14
    const-string v0, "load threeFunction"

    .line 15
    .line 16
    const-string v1, "SceneBannerOneFunctionProvider"

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;->getFunctions()Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Ljava/util/Collection;

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    const/4 v3, 0x1

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const/4 v0, 0x0

    .line 39
    goto :goto_1

    .line 40
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 41
    :goto_1
    const v4, 0x7f0a03b3

    .line 42
    .line 43
    .line 44
    const v5, 0x7f0a03b4

    .line 45
    .line 46
    .line 47
    const v6, 0x7f0a03b5

    .line 48
    .line 49
    .line 50
    if-eqz v0, :cond_2

    .line 51
    .line 52
    invoke-virtual {p1, v4, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v6, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v5, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 59
    .line 60
    .line 61
    const-string p1, "functions == null"

    .line 62
    .line 63
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void

    .line 67
    :cond_2
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;->getFunctions()Ljava/util/List;

    .line 68
    .line 69
    .line 70
    move-result-object p2

    .line 71
    if-eqz p2, :cond_5

    .line 72
    .line 73
    invoke-virtual {p1, v4, v3}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 74
    .line 75
    .line 76
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;

    .line 81
    .line 82
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->〇00〇8(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V

    .line 83
    .line 84
    .line 85
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eq v0, v3, :cond_4

    .line 90
    .line 91
    const/4 v4, 0x2

    .line 92
    if-eq v0, v4, :cond_3

    .line 93
    .line 94
    invoke-virtual {p1, v6, v3}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v5, v3}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 98
    .line 99
    .line 100
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    check-cast v0, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;

    .line 105
    .line 106
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->o0ooO(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V

    .line 107
    .line 108
    .line 109
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;

    .line 114
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->o〇0OOo〇0(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V

    .line 116
    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_3
    const-string v0, "functions size == 2"

    .line 120
    .line 121
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p1, v6, v3}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {p1, v5, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 128
    .line 129
    .line 130
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    move-result-object p2

    .line 134
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;

    .line 135
    .line 136
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerContentProvider;->o0ooO(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData$FunctionsBean;)V

    .line 137
    .line 138
    .line 139
    goto :goto_2

    .line 140
    :cond_4
    const-string p2, "functions size == 1"

    .line 141
    .line 142
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {p1, v6, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {p1, v5, v2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;->setVisible(IZ)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 149
    .line 150
    .line 151
    :cond_5
    :goto_2
    return-void
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/mainmenu/mainpage/entity/SceneSourceData;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mainmenu/mainpage/provider/SceneBannerThreeFunctionProvider;->〇〇08O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

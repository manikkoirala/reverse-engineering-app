.class public final Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "NewbieScanRewardDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;,
        Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$WhenMappings;,
        Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lcom/intsig/utils/ClickLimit;

.field private o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇080OO8〇0:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->O8o08O8O:Lcom/intsig/utils/ClickLimit;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->oOoO8OO〇(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo8o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$onClickAbandon$1;

    .line 6
    .line 7
    invoke-direct {v1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$onClickAbandon$1;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "main_file_pop_close"

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇80〇808〇O(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel$TakeRewardCallback;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇oo〇()V

    .line 20
    .line 21
    .line 22
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇0〇0(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇〇〇00(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oOoO8OO〇(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->O8o08O8O:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;->TOPIC:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;

    .line 16
    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇〇O80〇0o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇0oO〇oo00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇O8OO(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->O8o08O8O:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇o08()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇O8〇8000()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$takeTransferWord$1;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$takeTransferWord$1;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 8
    .line 9
    .line 10
    const-string v2, "main_file_pop_word"

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇80〇808〇O(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel$TakeRewardCallback;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇O00()V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0oO〇oo00()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f130d3d

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0ooOOo()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "binding"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 12
    .line 13
    new-instance v2, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 14
    .line 15
    invoke-direct {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v3

    .line 22
    const v4, 0x7f0602e0

    .line 23
    .line 24
    .line 25
    invoke-static {v3, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    invoke-virtual {v2, v3}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 34
    .line 35
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    const/16 v5, 0x10

    .line 40
    .line 41
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    int-to-float v4, v4

    .line 46
    invoke-virtual {v2, v4}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    const v5, 0x7f0602bb

    .line 55
    .line 56
    .line 57
    invoke-static {v4, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 58
    .line 59
    .line 60
    move-result v4

    .line 61
    invoke-virtual {v2, v4}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O〇8O8〇008(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    const/4 v4, 0x2

    .line 66
    invoke-virtual {v2, v4}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O8ooOoo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-virtual {v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 75
    .line 76
    .line 77
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 78
    .line 79
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    const v4, 0x7f060291

    .line 87
    .line 88
    .line 89
    invoke-static {v2, v4}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    const/16 v2, 0x12

    .line 98
    .line 99
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 100
    .line 101
    .line 102
    move-result-object v3

    .line 103
    invoke-static {v3, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 104
    .line 105
    .line 106
    move-result v2

    .line 107
    int-to-float v2, v2

    .line 108
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->oOo0:Landroid/widget/TextView;

    .line 117
    .line 118
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 119
    .line 120
    .line 121
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 124
    .line 125
    .line 126
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final 〇0〇0(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->O8o08O8O:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;->TRANSFER_WORD:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;

    .line 16
    .line 17
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇〇O80〇0o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8O0880()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$takeTopic$1;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$takeTopic$1;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 8
    .line 9
    .line 10
    const-string v2, "main_file_pop_patting"

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇80〇808〇O(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel$TakeRewardCallback;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->Oooo8o0〇()V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8〇80o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "binding"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 12
    .line 13
    new-instance v2, LoO〇oo/〇080;

    .line 14
    .line 15
    invoke-direct {v2, p0}, LoO〇oo/〇080;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    .line 20
    .line 21
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->oOo0:Landroid/widget/TextView;

    .line 22
    .line 23
    new-instance v2, LoO〇oo/〇o00〇〇Oo;

    .line 24
    .line 25
    invoke-direct {v2, p0}, LoO〇oo/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 32
    .line 33
    new-instance v1, LoO〇oo/〇o〇;

    .line 34
    .line 35
    invoke-direct {v1, p0}, LoO〇oo/〇o〇;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇O8OO(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O0o〇〇o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$WhenMappings;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    if-eq p1, v0, :cond_1

    .line 11
    .line 12
    const/4 v0, 0x2

    .line 13
    if-eq p1, v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇8O0880()V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇08O()V

    .line 21
    .line 22
    .line 23
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇O8〇8000()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f131749

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o08()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const v1, 0x7f1300a9

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const v1, 0x7f13174c

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, LoO〇oo/O8;

    .line 25
    .line 26
    invoke-direct {v1}, LoO〇oo/O8;-><init>()V

    .line 27
    .line 28
    .line 29
    const v2, 0x7f13174b

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const/16 v2, 0x14

    .line 43
    .line 44
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->oo88o8O(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    new-instance v1, LoO〇oo/Oo08;

    .line 53
    .line 54
    invoke-direct {v1, p0}, LoO〇oo/Oo08;-><init>(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;)V

    .line 55
    .line 56
    .line 57
    const v2, 0x7f13174a

    .line 58
    .line 59
    .line 60
    const v3, 0x7f060268

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    const-string v1, "NewbieScanRewardDialog"

    .line 77
    .line 78
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    .line 80
    .line 81
    :goto_0
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇o〇88〇8()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    .line 15
    .line 16
    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p0, v1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇O80〇0o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇O0o〇〇o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 19
    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;->TRANSFER_WORD:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;

    .line 23
    .line 24
    if-ne p1, v0, :cond_2

    .line 25
    .line 26
    const/16 p1, 0x90

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_2
    const/16 p1, 0x91

    .line 30
    .line 31
    :goto_0
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 32
    .line 33
    .line 34
    :goto_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇〇〇0(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->Ooo8o()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇〇〇00(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method protected init(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇o〇88〇8()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇0ooOOo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    new-instance p3, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v0, "\u6536\u5230\u767b\u5f55\u7ed3\u679c\uff1a requestCode = "

    .line 10
    .line 11
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    const-string v0, "  resultCode = "

    .line 18
    .line 19
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    const-string p3, "NewbieScanRewardDialog"

    .line 30
    .line 31
    invoke-static {p3, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    invoke-static {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    if-eqz p2, :cond_1

    .line 43
    .line 44
    const/16 p2, 0x90

    .line 45
    .line 46
    if-ne p1, p2, :cond_0

    .line 47
    .line 48
    sget-object p1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;->TRANSFER_WORD:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;

    .line 49
    .line 50
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇O0o〇〇o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;->TOPIC:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;

    .line 55
    .line 56
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇O0o〇〇o(Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog$RewardType;)V

    .line 57
    .line 58
    .line 59
    :cond_1
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d01a9

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    const-string p3, "bind(view)"

    .line 19
    .line 20
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-object p2, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/DialogDocNewbieScanRewardBinding;

    .line 24
    .line 25
    return-object p1
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onPause()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    const-string v0, "NewbieScanRewardDialog"

    .line 5
    .line 6
    const-string v1, "onPause"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    const-string v0, "NewbieScanRewardDialog"

    .line 5
    .line 6
    const-string v1, "onResume"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->oo88o8O()V

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;->〇00()V

    .line 23
    .line 24
    .line 25
    :cond_1
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/intsig/app/BaseDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->〇8〇80o()V

    .line 10
    .line 11
    .line 12
    new-instance p1, Landroidx/lifecycle/ViewModelProvider;

    .line 13
    .line 14
    invoke-direct {p1, p0}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 15
    .line 16
    .line 17
    const-class p2, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 18
    .line 19
    invoke-virtual {p1, p2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardDialog;->o〇00O:Lcom/intsig/camscanner/newbiescanreward/NewbieScanRewardViewModel;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

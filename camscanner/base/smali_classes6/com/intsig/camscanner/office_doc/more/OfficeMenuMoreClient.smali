.class public final Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;
.super Ljava/lang/Object;
.source "OfficeMenuMoreClient.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;,
        Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$WhenMappings;,
        Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;

.field private final Oo08:Z

.field private final oO80:Landroidx/lifecycle/LifecycleCoroutineScope;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:Lcom/intsig/app/BaseProgressDialog;

.field private final 〇o00〇〇Oo:J

.field private final 〇o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇8o8o〇:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/appcompat/app/AppCompatActivity;JLjava/lang/String;Ljava/util/List;ZLcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;)V
    .locals 1
    .param p1    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/appcompat/app/AppCompatActivity;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mFromPart"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "mTagList"

    .line 12
    .line 13
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "mListener"

    .line 17
    .line 18
    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    iput-wide p2, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o00〇〇Oo:J

    .line 27
    .line 28
    iput-object p4, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o〇:Ljava/lang/String;

    .line 29
    .line 30
    iput-object p5, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->O8:Ljava/util/List;

    .line 31
    .line 32
    iput-boolean p6, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->Oo08:Z

    .line 33
    .line 34
    iput-object p7, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;

    .line 35
    .line 36
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->oO80:Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f080a4b

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f1313af

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_631_newmore_05)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 22
    .line 23
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$ocrDoc$1;

    .line 24
    .line 25
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$ocrDoc$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 26
    .line 27
    .line 28
    const/16 v7, 0x8

    .line 29
    .line 30
    const/4 v8, 0x0

    .line 31
    move-object v0, v9

    .line 32
    move-object v3, p1

    .line 33
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 34
    .line 35
    .line 36
    return-object v9
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Ljava/lang/String;Lcom/intsig/camscanner/share/type/SendToPc$FileType;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->oo〇(Ljava/lang/String;Lcom/intsig/camscanner/share/type/SendToPc$FileType;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OOO〇O0(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/request/OfficeConvertManger;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/office_doc/request/OfficeConvertManger;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇0000OOO()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const/4 v3, 0x0

    .line 17
    if-nez v2, :cond_1

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO8oO0o〇()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v1, "printDoc: wait upload finish, isSyncing: "

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const-string v1, "OfficeMenuMoreClient"

    .line 49
    .line 50
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    if-nez p1, :cond_0

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->OOO(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_0
    return-void

    .line 63
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->oO80:Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 64
    .line 65
    const/4 v5, 0x0

    .line 66
    const/4 v6, 0x0

    .line 67
    new-instance v7, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printDocAction$1;

    .line 68
    .line 69
    invoke-direct {v7, p1, p0, v0, v3}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printDocAction$1;-><init>(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/request/OfficeConvertManger;Lkotlin/coroutines/Continuation;)V

    .line 70
    .line 71
    .line 72
    const/4 v8, 0x3

    .line 73
    const/4 v9, 0x0

    .line 74
    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o00〇〇Oo:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OoO8(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f0806b2

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f13084d

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_519b_pc_edit)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->TOP_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇O888o0o(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    .line 37
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->Oo08:Z

    .line 38
    .line 39
    if-eqz v0, :cond_0

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const/4 v0, 0x0

    .line 44
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 45
    .line 46
    .line 47
    move-result-object v5

    .line 48
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$editOnPC$1;

    .line 49
    .line 50
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$editOnPC$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 51
    .line 52
    .line 53
    const/16 v7, 0x8

    .line 54
    .line 55
    const/4 v8, 0x0

    .line 56
    move-object v0, v9

    .line 57
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 58
    .line 59
    .line 60
    return-object v9
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇8O8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f08092d

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f13133b

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.st\u2026630_detailed_information)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    const/4 v5, 0x0

    .line 24
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$information$1;

    .line 25
    .line 26
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$information$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 27
    .line 28
    .line 29
    const/16 v7, 0x18

    .line 30
    .line 31
    const/4 v8, 0x0

    .line 32
    move-object v0, v9

    .line 33
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 34
    .line 35
    .line 36
    return-object v9
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o800o8O(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f080996

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f1313b8

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_631_newmore_14)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇O888o0o(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    .line 37
    const/4 v0, 0x1

    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v0, 0x0

    .line 40
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 41
    .line 42
    .line 43
    move-result-object v5

    .line 44
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$emailToSelf$1;

    .line 45
    .line 46
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$emailToSelf$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 47
    .line 48
    .line 49
    const/16 v7, 0x8

    .line 50
    .line 51
    const/4 v8, 0x0

    .line 52
    move-object v0, v9

    .line 53
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 54
    .line 55
    .line 56
    return-object v9
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oo88o8O()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f080e4a

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f1307ff

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_518b_pdf_image)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 24
    .line 25
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$extractImages$1;

    .line 26
    .line 27
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$extractImages$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 28
    .line 29
    .line 30
    const/16 v7, 0x8

    .line 31
    .line 32
    const/4 v8, 0x0

    .line 33
    move-object v0, v9

    .line 34
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    .line 36
    .line 37
    return-object v9
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oo〇(Ljava/lang/String;Lcom/intsig/camscanner/share/type/SendToPc$FileType;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 12

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;

    .line 10
    .line 11
    const/16 p2, 0x2713

    .line 12
    .line 13
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;->〇o〇(I)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_PREVIEW_NEW_MORE_PC_EDIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 22
    .line 23
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD_PREVIEW_NEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 24
    .line 25
    invoke-virtual {v0, v1, p3, v2, v3}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_1

    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    invoke-virtual {p3}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇O888o0o()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v5

    .line 36
    iget-object p3, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 37
    .line 38
    invoke-static {p3}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 39
    .line 40
    .line 41
    move-result-object p3

    .line 42
    const/4 v7, 0x0

    .line 43
    const/4 v8, 0x0

    .line 44
    new-instance v9, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$sendToPcByWordEditMode$1;

    .line 45
    .line 46
    const/4 v6, 0x0

    .line 47
    move-object v1, v9

    .line 48
    move-object v2, p0

    .line 49
    move-object v3, p1

    .line 50
    move-object v4, p2

    .line 51
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$sendToPcByWordEditMode$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Ljava/lang/String;Lcom/intsig/camscanner/share/type/SendToPc$FileType;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 52
    .line 53
    .line 54
    const/4 v10, 0x3

    .line 55
    const/4 v11, 0x0

    .line 56
    move-object v6, p3

    .line 57
    invoke-static/range {v6 .. v11}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/ThumbData;
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    const v2, 0x7f080e23

    .line 12
    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    sget-object v3, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$WhenMappings;->〇080:[I

    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    aget v1, v3, v1

    .line 23
    .line 24
    packed-switch v1, :pswitch_data_0

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :pswitch_0
    const v2, 0x7f080e78

    .line 29
    .line 30
    .line 31
    const v7, 0x7f080e78

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :pswitch_1
    const v2, 0x7f080b13

    .line 36
    .line 37
    .line 38
    const v7, 0x7f080b13

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :pswitch_2
    const v2, 0x7f080783

    .line 43
    .line 44
    .line 45
    const v7, 0x7f080783

    .line 46
    .line 47
    .line 48
    goto :goto_1

    .line 49
    :cond_0
    :goto_0
    :pswitch_3
    const v7, 0x7f080e23

    .line 50
    .line 51
    .line 52
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇80〇808〇O()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-string v2, ""

    .line 57
    .line 58
    if-eqz v1, :cond_3

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 65
    .line 66
    .line 67
    move-result v3

    .line 68
    if-eqz v3, :cond_1

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    goto :goto_2

    .line 75
    :cond_1
    move-object v0, v2

    .line 76
    :goto_2
    if-nez v0, :cond_2

    .line 77
    .line 78
    goto :goto_3

    .line 79
    :cond_2
    move-object v4, v0

    .line 80
    goto :goto_4

    .line 81
    :cond_3
    :goto_3
    move-object v4, v2

    .line 82
    :goto_4
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/ThumbData;

    .line 83
    .line 84
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇O888o0o()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    if-nez v1, :cond_4

    .line 89
    .line 90
    move-object v5, v2

    .line 91
    goto :goto_5

    .line 92
    :cond_4
    move-object v5, v1

    .line 93
    :goto_5
    iget-object v6, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->O8:Ljava/util/List;

    .line 94
    .line 95
    const/4 v8, 0x0

    .line 96
    new-instance v9, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$getThumbData$thumbData$1;

    .line 97
    .line 98
    invoke-direct {v9, p1, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$getThumbData$thumbData$1;-><init>(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 99
    .line 100
    .line 101
    move-object v3, v0

    .line 102
    invoke-direct/range {v3 .. v9}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/ThumbData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZLkotlin/jvm/functions/Function0;)V

    .line 103
    .line 104
    .line 105
    return-object v0

    .line 106
    nop

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o〇〇0〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f080cf0

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f130d48

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_553_printer_02)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    const/4 v5, 0x0

    .line 24
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printDoc$1;

    .line 25
    .line 26
    invoke-direct {v6, p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printDoc$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 27
    .line 28
    .line 29
    const/16 v7, 0x18

    .line 30
    .line 31
    const/4 v8, 0x0

    .line 32
    move-object v0, v9

    .line 33
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 34
    .line 35
    .line 36
    return-object v9
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇00()V
    .locals 2

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇80〇808〇O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "OfficeMenuMoreClient"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0000OOO()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x0

    .line 9
    new-instance v4, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printByWordEditMode$1;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$printByWordEditMode$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lkotlin/coroutines/Continuation;)V

    .line 13
    .line 14
    .line 15
    const/4 v5, 0x3

    .line 16
    const/4 v6, 0x0

    .line 17
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private final 〇00〇8(Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇80〇808〇O:Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-static {p1, v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 24
    .line 25
    .line 26
    const v0, 0x7f1300a3

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-virtual {v1, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇80〇808〇O:Lcom/intsig/app/BaseProgressDialog;

    .line 37
    .line 38
    :cond_1
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇80〇808〇O:Lcom/intsig/app/BaseProgressDialog;

    .line 39
    .line 40
    if-eqz p1, :cond_2

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :catch_0
    move-exception p1

    .line 47
    const-string v0, "OfficeMenuMoreClient"

    .line 48
    .line 49
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇O〇(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0〇O0088o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇〇888:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇0000OOO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O00()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/IDocumentMoreType;",
            ">;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o00〇〇Oo:J

    .line 8
    .line 9
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const/4 v1, 0x0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    return-object v1

    .line 17
    :cond_0
    const/4 v2, 0x6

    .line 18
    new-array v2, v2, [Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/IDocumentMoreType;

    .line 19
    .line 20
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreThumbItem;

    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/ThumbData;

    .line 23
    .line 24
    .line 25
    move-result-object v4

    .line 26
    const/4 v5, 0x0

    .line 27
    const/4 v6, 0x2

    .line 28
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreThumbItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/ThumbData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 29
    .line 30
    .line 31
    aput-object v3, v2, v5

    .line 32
    .line 33
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 34
    .line 35
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OoO8(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 40
    .line 41
    .line 42
    const/4 v4, 0x1

    .line 43
    aput-object v3, v2, v4

    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 46
    .line 47
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o800o8O(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 52
    .line 53
    .line 54
    aput-object v3, v2, v6

    .line 55
    .line 56
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 57
    .line 58
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇〇0〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    invoke-direct {v3, v4, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 63
    .line 64
    .line 65
    const/4 v4, 0x3

    .line 66
    aput-object v3, v2, v4

    .line 67
    .line 68
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 69
    .line 70
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->O〇8O8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 71
    .line 72
    .line 73
    move-result-object v7

    .line 74
    invoke-direct {v3, v7, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 75
    .line 76
    .line 77
    const/4 v7, 0x4

    .line 78
    aput-object v3, v2, v7

    .line 79
    .line 80
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇〇8O0〇8()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 83
    .line 84
    .line 85
    move-result-object v7

    .line 86
    invoke-direct {v3, v7, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 87
    .line 88
    .line 89
    const/4 v7, 0x5

    .line 90
    aput-object v3, v2, v7

    .line 91
    .line 92
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->OOO〇O0(Ljava/lang/String;)Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-eqz v0, :cond_2

    .line 105
    .line 106
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    sub-int/2addr v0, v6

    .line 111
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 112
    .line 113
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇O888o0o()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 114
    .line 115
    .line 116
    move-result-object v7

    .line 117
    invoke-direct {v3, v7, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 121
    .line 122
    .line 123
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O〇8O8〇008()Z

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    if-eqz v0, :cond_2

    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O08000()Z

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    if-eqz v0, :cond_1

    .line 134
    .line 135
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 139
    .line 140
    :goto_0
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 141
    .line 142
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    invoke-direct {v3, v0, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v2, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 150
    .line 151
    .line 152
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O08000()Z

    .line 153
    .line 154
    .line 155
    move-result v0

    .line 156
    if-eqz v0, :cond_2

    .line 157
    .line 158
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;

    .line 159
    .line 160
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->oo88o8O()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 161
    .line 162
    .line 163
    move-result-object v3

    .line 164
    invoke-direct {v0, v3, v5, v6, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/item/DocumentMoreFuncItem;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 165
    .line 166
    .line 167
    invoke-virtual {v2, v4, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 168
    .line 169
    .line 170
    :cond_2
    return-object v2
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇O888o0o()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f080a8b

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f1314e1

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.st\u202632_newmore_file_password)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 24
    .line 25
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$encryptDoc$1;

    .line 26
    .line 27
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$encryptDoc$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 28
    .line 29
    .line 30
    const/16 v7, 0x8

    .line 31
    .line 32
    const/4 v8, 0x0

    .line 33
    move-object v0, v9

    .line 34
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    .line 36
    .line 37
    return-object v9
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OOO〇O0(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O〇(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->Oo08:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->o〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreListener;->〇80〇808〇O(Lkotlin/jvm/functions/Function0;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇0〇O0088o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/share/type/SendToPc$FileType;
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇080:Lcom/intsig/camscanner/office_doc/util/OfficeUtils;

    .line 6
    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const/4 p1, 0x0

    .line 14
    return-object p1

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$WhenMappings;->〇080:[I

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    aget p1, v0, p1

    .line 22
    .line 23
    packed-switch p1, :pswitch_data_0

    .line 24
    .line 25
    .line 26
    sget-object p1, Lcom/intsig/camscanner/share/type/SendToPc$FileType;->OFFICE_DOC_WORD:Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :pswitch_0
    sget-object p1, Lcom/intsig/camscanner/share/type/SendToPc$FileType;->OFFICE_DOC_PDF:Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/share/type/SendToPc$FileType;->OFFICE_DOC_PPT:Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :pswitch_2
    sget-object p1, Lcom/intsig/camscanner/share/type/SendToPc$FileType;->OFFICE_DOC_EXCEL:Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :pswitch_3
    sget-object p1, Lcom/intsig/camscanner/share/type/SendToPc$FileType;->OFFICE_DOC_WORD:Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 39
    .line 40
    :goto_0
    return-object p1

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/share/type/SendToPc$FileType;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇oo〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;Landroidx/appcompat/app/AppCompatActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇00〇8(Landroidx/appcompat/app/AppCompatActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8O0〇8()Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;
    .locals 10

    .line 1
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;

    .line 2
    .line 3
    const v1, 0x7f08073a

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const v2, 0x7f1311bf

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const-string v0, "mActivity.getString(R.string.cs_625_readexp_07)"

    .line 16
    .line 17
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sget-object v3, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ALL_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 23
    .line 24
    const/4 v5, 0x0

    .line 25
    new-instance v6, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$deleteDoc$1;

    .line 26
    .line 27
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$deleteDoc$1;-><init>(Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;)V

    .line 28
    .line 29
    .line 30
    const/16 v7, 0x10

    .line 31
    .line 32
    const/4 v8, 0x0

    .line 33
    move-object v0, v9

    .line 34
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/FuncData;-><init>(ILjava/lang/String;Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;Ljava/lang/Boolean;Ljava/lang/Boolean;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    .line 36
    .line 37
    return-object v9
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O8〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_4

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_0
    const-string v0, "from_part"

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o〇:Ljava/lang/String;

    .line 21
    .line 22
    const-string v2, "CSMore"

    .line 23
    .line 24
    invoke-static {v2, v0, v1}, Lcom/intsig/log/LogAgentHelper;->o〇〇0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇O00()Ljava/util/ArrayList;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v1, 0x0

    .line 32
    if-eqz v0, :cond_2

    .line 33
    .line 34
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const/4 v2, 0x0

    .line 42
    goto :goto_1

    .line 43
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 44
    :goto_1
    if-eqz v2, :cond_3

    .line 45
    .line 46
    const-string v0, "OfficeMenuMoreClient"

    .line 47
    .line 48
    const-string v1, "showDialog: data is empty"

    .line 49
    .line 50
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-void

    .line 54
    :cond_3
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew$Companion;

    .line 55
    .line 56
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew$Companion;->〇080(Ljava/util/ArrayList;Z)Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    const-string v2, "mActivity.supportFragmentManager"

    .line 67
    .line 68
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    const-string v2, "DocumentFragmentMoreDialogNew"

    .line 72
    .line 73
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇〇888:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/DocumentFragmentMoreDialogNew;

    .line 77
    .line 78
    :cond_4
    :goto_2
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇oOO8O8(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_8

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇080:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto/16 :goto_0

    .line 18
    .line 19
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v3, "onLoginFinish isLogin: "

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v3, ", requestCode: "

    .line 43
    .line 44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    const-string v3, "OfficeMenuMoreClient"

    .line 55
    .line 56
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    if-nez v1, :cond_1

    .line 60
    .line 61
    return-void

    .line 62
    :cond_1
    const/16 v1, 0x3e9

    .line 63
    .line 64
    if-eq p1, v1, :cond_7

    .line 65
    .line 66
    const/16 v1, 0x3ea

    .line 67
    .line 68
    if-eq p1, v1, :cond_5

    .line 69
    .line 70
    const/16 v1, 0x2713

    .line 71
    .line 72
    if-eq p1, v1, :cond_2

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    iget-wide v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o00〇〇Oo:J

    .line 80
    .line 81
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    if-nez p1, :cond_3

    .line 86
    .line 87
    return-void

    .line 88
    :cond_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇oo〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)Lcom/intsig/camscanner/share/type/SendToPc$FileType;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    if-nez v0, :cond_4

    .line 93
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    .line 95
    .line 96
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .line 98
    .line 99
    const-string v1, "onLoginFinish: fileType is null: "

    .line 100
    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .line 113
    .line 114
    return-void

    .line 115
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    invoke-direct {p0, v1, v0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->oo〇(Ljava/lang/String;Lcom/intsig/camscanner/share/type/SendToPc$FileType;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_5
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    iget-wide v0, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->〇o00〇〇Oo:J

    .line 128
    .line 129
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    if-nez p1, :cond_6

    .line 134
    .line 135
    return-void

    .line 136
    :cond_6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OOO〇O0(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 137
    .line 138
    .line 139
    goto :goto_0

    .line 140
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;

    .line 141
    .line 142
    if-eqz p1, :cond_8

    .line 143
    .line 144
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/more/OfficeMenuMoreClient$EmailHandler;->〇00()V

    .line 145
    .line 146
    .line 147
    :cond_8
    :goto_0
    return-void
.end method

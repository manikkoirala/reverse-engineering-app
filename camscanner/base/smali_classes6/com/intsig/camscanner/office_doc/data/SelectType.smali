.class public final enum Lcom/intsig/camscanner/office_doc/data/SelectType;
.super Ljava/lang/Enum;
.source "SelectType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/data/SelectType$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/office_doc/data/SelectType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum ALL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final Companion:Lcom/intsig/camscanner/office_doc/data/SelectType$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final enum EXCEL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum NO_OFFICE:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum PDF_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum PPT_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum SCAN_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field public static final enum WORD_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;


# instance fields
.field private des:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/office_doc/data/SelectType;
    .locals 3

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->ALL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->SCAN_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->PDF_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->WORD_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->EXCEL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    const/4 v1, 0x5

    .line 30
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->PPT_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 31
    .line 32
    aput-object v2, v0, v1

    .line 33
    .line 34
    const/4 v1, 0x6

    .line 35
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/SelectType;->NO_OFFICE:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 36
    .line 37
    aput-object v2, v0, v1

    .line 38
    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const v3, 0x7f130131

    .line 10
    .line 11
    .line 12
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const-string v3, "ApplicationHelper.sConte\u2026.a_label_drawer_menu_doc)"

    .line 17
    .line 18
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v3, "ALL_DOC"

    .line 22
    .line 23
    const/4 v4, 0x0

    .line 24
    invoke-direct {v0, v3, v4, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->ALL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const v2, 0x7f1313cb

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const-string v2, "ApplicationHelper.sConte\u2026R.string.cs_631_scan_doc)"

    .line 43
    .line 44
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    const-string v2, "SCAN_DOC"

    .line 48
    .line 49
    const/4 v3, 0x1

    .line 50
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 51
    .line 52
    .line 53
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->SCAN_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 54
    .line 55
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 56
    .line 57
    const/4 v1, 0x2

    .line 58
    const-string v2, "PDF"

    .line 59
    .line 60
    const-string v3, "PDF_DOC"

    .line 61
    .line 62
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 63
    .line 64
    .line 65
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->PDF_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 66
    .line 67
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 68
    .line 69
    const/4 v1, 0x3

    .line 70
    const-string v2, "Word"

    .line 71
    .line 72
    const-string v3, "WORD_DOC"

    .line 73
    .line 74
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 75
    .line 76
    .line 77
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->WORD_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 78
    .line 79
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 80
    .line 81
    const/4 v1, 0x4

    .line 82
    const-string v2, "Excel"

    .line 83
    .line 84
    const-string v3, "EXCEL_DOC"

    .line 85
    .line 86
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 87
    .line 88
    .line 89
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->EXCEL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 92
    .line 93
    const/4 v1, 0x5

    .line 94
    const-string v2, "PPT"

    .line 95
    .line 96
    const-string v3, "PPT_DOC"

    .line 97
    .line 98
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 99
    .line 100
    .line 101
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->PPT_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 102
    .line 103
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 104
    .line 105
    const/4 v1, 0x6

    .line 106
    const-string v2, ""

    .line 107
    .line 108
    const-string v3, "NO_OFFICE"

    .line 109
    .line 110
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/office_doc/data/SelectType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 111
    .line 112
    .line 113
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->NO_OFFICE:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 114
    .line 115
    invoke-static {}, Lcom/intsig/camscanner/office_doc/data/SelectType;->$values()[Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->$VALUES:[Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 120
    .line 121
    new-instance v0, Lcom/intsig/camscanner/office_doc/data/SelectType$Companion;

    .line 122
    .line 123
    const/4 v1, 0x0

    .line 124
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/data/SelectType$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 125
    .line 126
    .line 127
    sput-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->Companion:Lcom/intsig/camscanner/office_doc/data/SelectType$Companion;

    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/office_doc/data/SelectType;->des:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/office_doc/data/SelectType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/office_doc/data/SelectType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/data/SelectType;->$VALUES:[Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getDes()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/data/SelectType;->des:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setDes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/data/SelectType;->des:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

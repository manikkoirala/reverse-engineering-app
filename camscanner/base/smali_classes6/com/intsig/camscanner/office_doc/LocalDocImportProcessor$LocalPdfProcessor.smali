.class public Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;
.super Landroid/os/AsyncTask;
.source "LocalDocImportProcessor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LocalPdfProcessor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;",
        "Ljava/lang/Integer;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

.field private Oo08:J

.field final synthetic o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

.field private final 〇080:Z

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/office_doc/DocImportCell;

.field private 〇o〇:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;ZLcom/intsig/camscanner/office_doc/DocImportCell;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/intsig/camscanner/office_doc/DocImportCell;",
            ")V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 2
    .line 3
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-boolean p2, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇080:Z

    .line 7
    .line 8
    iput-object p3, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o00〇〇Oo:Lcom/intsig/camscanner/office_doc/DocImportCell;

    .line 9
    .line 10
    const-wide/16 p1, -0x1

    .line 11
    .line 12
    iput-wide p1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->Oo08:J

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Oo08(Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :catch_0
    move-exception p1

    .line 15
    const-string v0, "LocalDocImportProcessor"

    .line 16
    .line 17
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 18
    .line 19
    .line 20
    :cond_1
    :goto_0
    return-void
.end method

.method private final 〇080(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;
    .locals 9

    .line 1
    new-instance v1, Lcom/shockwave/pdfium/PdfiumCore;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 4
    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-direct {v1, v0}, Lcom/shockwave/pdfium/PdfiumCore;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 13
    .line 14
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 19
    .line 20
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPwd()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/source/FileSource;->createDocument(Landroid/content/Context;Lcom/shockwave/pdfium/PdfiumCore;Ljava/lang/String;)Lcom/shockwave/pdfium/PdfDocument;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const-string p1, "{\n                source\u2026Free!!.pwd)\n            }"

    .line 32
    .line 33
    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .line 35
    .line 36
    new-instance p1, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 37
    .line 38
    sget-object v3, Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;->BOTH:Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;

    .line 39
    .line 40
    const/4 v5, 0x0

    .line 41
    const/4 v6, 0x1

    .line 42
    const/4 v7, 0x0

    .line 43
    const/4 v8, 0x1

    .line 44
    move-object v0, p1

    .line 45
    move-object v4, p2

    .line 46
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;-><init>(Lcom/shockwave/pdfium/PdfiumCore;Lcom/shockwave/pdfium/PdfDocument;Lcom/intsig/camscanner/pdfengine/entity/FitPolicy;Lcom/shockwave/pdfium/util/Size;[IZIZ)V

    .line 47
    .line 48
    .line 49
    return-object p1

    .line 50
    :catch_0
    move-exception p1

    .line 51
    const-string p2, "LocalDocImportProcessor"

    .line 52
    .line 53
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    .line 55
    .line 56
    const/4 p1, 0x0

    .line 57
    return-object p1
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method protected varargs O8([Ljava/lang/Integer;)V
    .locals 2
    .param p1    # [Ljava/lang/Integer;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "values"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_4

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/4 v1, 0x0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v0, 0x0

    .line 32
    :goto_0
    if-eqz v0, :cond_1

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-nez v0, :cond_2

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_2
    aget-object p1, p1, v1

    .line 45
    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    :cond_3
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setProgress(I)V

    .line 53
    .line 54
    .line 55
    :cond_4
    :goto_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o00〇〇Oo([Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/net/Uri;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o〇(Landroid/net/Uri;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onPreExecute()V
    .locals 7

    .line 1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const/4 v0, 0x1

    .line 14
    const/4 v1, 0x0

    .line 15
    const/4 v2, 0x0

    .line 16
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/experiment/DocImportOptExp;->〇o00〇〇Oo(ZILjava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_3

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇00()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_3

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 29
    .line 30
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->O〇8O8〇008(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 37
    .line 38
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->OOO〇O0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇080:Z

    .line 45
    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 49
    .line 50
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    instance-of v0, v0, Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    if-eqz v0, :cond_2

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 59
    .line 60
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/office_doc/DocImportDialog;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    if-nez v0, :cond_1

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 67
    .line 68
    sget-object v1, Lcom/intsig/camscanner/office_doc/DocImportDialog;->oOo〇8o008:Lcom/intsig/camscanner/office_doc/DocImportDialog$Companion;

    .line 69
    .line 70
    const-wide/16 v2, 0x0

    .line 71
    .line 72
    const/4 v4, 0x0

    .line 73
    const/4 v5, 0x3

    .line 74
    const/4 v6, 0x0

    .line 75
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/office_doc/DocImportDialog$Companion;->〇o00〇〇Oo(Lcom/intsig/camscanner/office_doc/DocImportDialog$Companion;JZILjava/lang/Object;)Lcom/intsig/camscanner/office_doc/DocImportDialog;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-static {v0, v1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o0ooO(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;Lcom/intsig/camscanner/office_doc/DocImportDialog;)V

    .line 80
    .line 81
    .line 82
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 83
    .line 84
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/office_doc/DocImportDialog;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    if-eqz v0, :cond_2

    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 91
    .line 92
    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    check-cast v1, Landroidx/fragment/app/FragmentActivity;

    .line 97
    .line 98
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    const-string v2, "LocalDocImportProcessor"

    .line 103
    .line 104
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    :cond_2
    return-void

    .line 108
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 109
    .line 110
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    if-nez v0, :cond_4

    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 117
    .line 118
    new-instance v1, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 119
    .line 120
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 121
    .line 122
    invoke-static {v3}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 123
    .line 124
    .line 125
    move-result-object v3

    .line 126
    invoke-direct {v1, v3}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    .line 127
    .line 128
    .line 129
    invoke-static {v0, v1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇0OOo〇0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;)V

    .line 130
    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 133
    .line 134
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 139
    .line 140
    .line 141
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 142
    .line 143
    invoke-static {v1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 144
    .line 145
    .line 146
    move-result-object v1

    .line 147
    const v3, 0x7f131ec6

    .line 148
    .line 149
    .line 150
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 158
    .line 159
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->setProgress(I)V

    .line 167
    .line 168
    .line 169
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 170
    .line 171
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 179
    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 182
    .line 183
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->show()V

    .line 191
    .line 192
    .line 193
    goto :goto_0

    .line 194
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 195
    .line 196
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 204
    .line 205
    .line 206
    move-result v0

    .line 207
    if-nez v0, :cond_5

    .line 208
    .line 209
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 210
    .line 211
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oo88o8O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;

    .line 212
    .line 213
    .line 214
    move-result-object v0

    .line 215
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 216
    .line 217
    .line 218
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/dialog/CustomProgressDialog;->show()V

    .line 219
    .line 220
    .line 221
    :cond_5
    :goto_0
    return-void
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8([Ljava/lang/Integer;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o〇:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected varargs 〇o00〇〇Oo([Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;)Landroid/net/Uri;
    .locals 25
    .param p1    # [Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    const-string v2, ".jpg"

    .line 6
    .line 7
    const-string v3, "pdfFileDataFree"

    .line 8
    .line 9
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    array-length v3, v0

    .line 13
    const/4 v4, 0x1

    .line 14
    const/4 v5, 0x0

    .line 15
    if-nez v3, :cond_0

    .line 16
    .line 17
    const/4 v3, 0x1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v3, 0x0

    .line 20
    :goto_0
    const/4 v6, 0x0

    .line 21
    if-eqz v3, :cond_1

    .line 22
    .line 23
    return-object v6

    .line 24
    :cond_1
    aget-object v0, v0, v5

    .line 25
    .line 26
    iput-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 27
    .line 28
    const-string v3, "LocalDocImportProcessor"

    .line 29
    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    const-string v0, "LocalPdfProcessor:mPdfFree is not complete"

    .line 33
    .line 34
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    return-object v6

    .line 38
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 39
    .line 40
    .line 41
    move-result-wide v7

    .line 42
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 43
    .line 44
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPath()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    const-string v0, "LocalPdfProcessor:mPdfFree.path is empty"

    .line 58
    .line 59
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-object v6

    .line 63
    :cond_3
    new-instance v0, Ljava/io/File;

    .line 64
    .line 65
    iget-object v9, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 66
    .line 67
    invoke-static {v9}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {v9}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPath()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v9

    .line 74
    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    iget-object v9, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 78
    .line 79
    invoke-static {v9}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->OOO〇O0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Z

    .line 80
    .line 81
    .line 82
    move-result v9

    .line 83
    if-eqz v9, :cond_9

    .line 84
    .line 85
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v9

    .line 89
    invoke-static {v9}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o〇〇0〇(Ljava/lang/String;)Z

    .line 90
    .line 91
    .line 92
    move-result v9

    .line 93
    if-eqz v9, :cond_9

    .line 94
    .line 95
    new-instance v2, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 96
    .line 97
    invoke-direct {v2}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 98
    .line 99
    .line 100
    new-instance v4, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 101
    .line 102
    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 103
    .line 104
    .line 105
    iget-object v5, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 106
    .line 107
    if-eqz v5, :cond_4

    .line 108
    .line 109
    invoke-virtual {v5}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getTempDocTitle()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v5

    .line 113
    goto :goto_1

    .line 114
    :cond_4
    move-object v5, v6

    .line 115
    :goto_1
    iput-object v5, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 116
    .line 117
    new-instance v5, Lkotlin/jvm/internal/Ref$ObjectRef;

    .line 118
    .line 119
    invoke-direct {v5}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    .line 120
    .line 121
    .line 122
    const-string v9, "PDF"

    .line 123
    .line 124
    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 125
    .line 126
    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v9

    .line 130
    const-string v10, "this as java.lang.String).toLowerCase(Locale.ROOT)"

    .line 131
    .line 132
    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    iput-object v9, v5, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 136
    .line 137
    iget-object v11, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 138
    .line 139
    const/4 v12, 0x0

    .line 140
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v13

    .line 144
    new-instance v14, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor$doInBackground$savePath$1;

    .line 145
    .line 146
    invoke-direct {v14, v2, v5}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor$doInBackground$savePath$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lkotlin/jvm/internal/Ref$ObjectRef;)V

    .line 147
    .line 148
    .line 149
    new-instance v15, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor$doInBackground$savePath$2;

    .line 150
    .line 151
    invoke-direct {v15, v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor$doInBackground$savePath$2;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    .line 152
    .line 153
    .line 154
    const/16 v16, 0x1

    .line 155
    .line 156
    invoke-static/range {v11 .. v16}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇00(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;Landroid/net/Uri;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Z)Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v9

    .line 160
    iget-object v10, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 161
    .line 162
    if-eqz v10, :cond_5

    .line 163
    .line 164
    invoke-virtual {v10}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getTempDocTitle()Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v10

    .line 168
    goto :goto_2

    .line 169
    :cond_5
    move-object v10, v6

    .line 170
    :goto_2
    new-instance v11, Ljava/lang/StringBuilder;

    .line 171
    .line 172
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    .line 174
    .line 175
    const-string v12, "LocalPdfProcessor:file.name\uff1a"

    .line 176
    .line 177
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    const-string v12, ", savePath:"

    .line 184
    .line 185
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    .line 187
    .line 188
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    const-string v12, "\uff0c tempTitle\uff1a"

    .line 192
    .line 193
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v10

    .line 203
    invoke-static {v3, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v17

    .line 210
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 211
    .line 212
    if-eqz v0, :cond_6

    .line 213
    .line 214
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getPwd()Ljava/lang/String;

    .line 215
    .line 216
    .line 217
    move-result-object v0

    .line 218
    move-object/from16 v19, v0

    .line 219
    .line 220
    goto :goto_3

    .line 221
    :cond_6
    move-object/from16 v19, v6

    .line 222
    .line 223
    :goto_3
    const/16 v20, 0x0

    .line 224
    .line 225
    const/16 v21, 0x0

    .line 226
    .line 227
    const/16 v22, 0x10

    .line 228
    .line 229
    const/16 v23, 0x0

    .line 230
    .line 231
    move-object/from16 v18, v9

    .line 232
    .line 233
    invoke-static/range {v17 .. v23}, Lcom/intsig/camscanner/util/PdfUtils;->OO0o〇〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 234
    .line 235
    .line 236
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 237
    .line 238
    if-nez v0, :cond_7

    .line 239
    .line 240
    goto :goto_4

    .line 241
    :cond_7
    invoke-virtual {v0, v9}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->setPath(Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    :goto_4
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o00〇〇Oo:Lcom/intsig/camscanner/office_doc/DocImportCell;

    .line 245
    .line 246
    new-instance v10, Ljava/lang/StringBuilder;

    .line 247
    .line 248
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 249
    .line 250
    .line 251
    const-string v11, "cell is before "

    .line 252
    .line 253
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    .line 265
    .line 266
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 267
    .line 268
    iget-object v10, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o00〇〇Oo:Lcom/intsig/camscanner/office_doc/DocImportCell;

    .line 269
    .line 270
    iget-object v5, v5, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 271
    .line 272
    move-object/from16 v20, v5

    .line 273
    .line 274
    check-cast v20, Ljava/lang/String;

    .line 275
    .line 276
    iget-object v4, v4, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 277
    .line 278
    move-object/from16 v21, v4

    .line 279
    .line 280
    check-cast v21, Ljava/lang/String;

    .line 281
    .line 282
    iget-object v2, v2, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 283
    .line 284
    move-object/from16 v22, v2

    .line 285
    .line 286
    check-cast v22, Lcom/intsig/camscanner/office_doc/data/OfficeEnum;

    .line 287
    .line 288
    move-object/from16 v17, v0

    .line 289
    .line 290
    move-object/from16 v18, v10

    .line 291
    .line 292
    move-object/from16 v19, v9

    .line 293
    .line 294
    invoke-static/range {v17 .. v22}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->O8(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;Lcom/intsig/camscanner/office_doc/DocImportCell;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/office_doc/data/OfficeEnum;)J

    .line 295
    .line 296
    .line 297
    move-result-wide v4

    .line 298
    iput-wide v4, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->Oo08:J

    .line 299
    .line 300
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o00〇〇Oo:Lcom/intsig/camscanner/office_doc/DocImportCell;

    .line 301
    .line 302
    if-nez v0, :cond_8

    .line 303
    .line 304
    goto :goto_5

    .line 305
    :cond_8
    invoke-virtual {v0, v4, v5}, Lcom/intsig/camscanner/office_doc/DocImportCell;->setDocId(J)V

    .line 306
    .line 307
    .line 308
    :goto_5
    move-wide/from16 v19, v7

    .line 309
    .line 310
    goto/16 :goto_1e

    .line 311
    .line 312
    :cond_9
    new-instance v9, Ljava/io/File;

    .line 313
    .line 314
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v10

    .line 318
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    .line 322
    .line 323
    .line 324
    new-instance v10, Lcom/intsig/camscanner/pdfengine/source/FileSource;

    .line 325
    .line 326
    invoke-direct {v10, v0}, Lcom/intsig/camscanner/pdfengine/source/FileSource;-><init>(Ljava/io/File;)V

    .line 327
    .line 328
    .line 329
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 330
    .line 331
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->O8ooOoo〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/shockwave/pdfium/util/Size;

    .line 332
    .line 333
    .line 334
    move-result-object v11

    .line 335
    invoke-direct {v1, v10, v11}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 336
    .line 337
    .line 338
    move-result-object v0

    .line 339
    if-nez v0, :cond_a

    .line 340
    .line 341
    return-object v6

    .line 342
    :cond_a
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPagesCount()I

    .line 343
    .line 344
    .line 345
    move-result v12

    .line 346
    new-instance v15, Ljava/util/ArrayList;

    .line 347
    .line 348
    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .line 350
    .line 351
    new-instance v13, Ljava/lang/StringBuilder;

    .line 352
    .line 353
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 354
    .line 355
    .line 356
    const-string v14, "start import, estimated number = "

    .line 357
    .line 358
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    .line 360
    .line 361
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 362
    .line 363
    .line 364
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 365
    .line 366
    .line 367
    move-result-object v13

    .line 368
    invoke-static {v3, v13}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    .line 370
    .line 371
    const/4 v13, 0x0

    .line 372
    const/4 v14, 0x0

    .line 373
    :goto_6
    if-ge v13, v12, :cond_10

    .line 374
    .line 375
    add-int/2addr v14, v4

    .line 376
    if-nez v0, :cond_b

    .line 377
    .line 378
    invoke-direct {v1, v10, v11}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇080(Lcom/intsig/camscanner/pdfengine/source/FileSource;Lcom/shockwave/pdfium/util/Size;)Lcom/intsig/camscanner/pdfengine/entity/PdfFile;

    .line 379
    .line 380
    .line 381
    move-result-object v0

    .line 382
    if-nez v0, :cond_b

    .line 383
    .line 384
    const-string v0, "pdfFile == null"

    .line 385
    .line 386
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    .line 388
    .line 389
    return-object v6

    .line 390
    :cond_b
    move-object v6, v0

    .line 391
    :try_start_0
    invoke-virtual {v6, v13, v5}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->openPage(IZ)Z

    .line 392
    .line 393
    .line 394
    invoke-virtual {v6, v13}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->getPageSize(I)Lcom/shockwave/pdfium/util/SizeF;

    .line 395
    .line 396
    .line 397
    move-result-object v0

    .line 398
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇o00〇〇Oo()F

    .line 399
    .line 400
    .line 401
    move-result v4

    .line 402
    float-to-int v4, v4

    .line 403
    invoke-virtual {v0}, Lcom/shockwave/pdfium/util/SizeF;->〇080()F

    .line 404
    .line 405
    .line 406
    move-result v0

    .line 407
    float-to-int v0, v0

    .line 408
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 409
    .line 410
    invoke-static {v4, v0, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 411
    .line 412
    .line 413
    move-result-object v5
    :try_end_0
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_0 .. :try_end_0} :catch_1d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1b
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 414
    move-object/from16 p1, v10

    .line 415
    .line 416
    :try_start_1
    new-instance v10, Ljava/lang/StringBuilder;

    .line 417
    .line 418
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_1 .. :try_end_1} :catch_1a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_19
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_18
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 419
    .line 420
    .line 421
    move-object/from16 v24, v11

    .line 422
    .line 423
    :try_start_2
    const-string v11, "create Bitmap, width = "

    .line 424
    .line 425
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    .line 427
    .line 428
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    const-string v11, " height = "

    .line 432
    .line 433
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    .line 435
    .line 436
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 437
    .line 438
    .line 439
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 440
    .line 441
    .line 442
    move-result-object v10

    .line 443
    invoke-static {v3, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    .line 445
    .line 446
    const/16 v19, 0x0

    .line 447
    .line 448
    const/16 v20, 0x0

    .line 449
    .line 450
    const/16 v23, 0x1

    .line 451
    .line 452
    move-object/from16 v16, v6

    .line 453
    .line 454
    move-object/from16 v17, v5

    .line 455
    .line 456
    move/from16 v18, v13

    .line 457
    .line 458
    move/from16 v21, v4

    .line 459
    .line 460
    move/from16 v22, v0

    .line 461
    .line 462
    invoke-virtual/range {v16 .. v23}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->renderPageBitmap(Landroid/graphics/Bitmap;IIIIIZ)V

    .line 463
    .line 464
    .line 465
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 466
    .line 467
    .line 468
    move-result-object v0

    .line 469
    new-instance v4, Ljava/io/File;

    .line 470
    .line 471
    new-instance v10, Ljava/lang/StringBuilder;

    .line 472
    .line 473
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 474
    .line 475
    .line 476
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    .line 478
    .line 479
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    .line 481
    .line 482
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 483
    .line 484
    .line 485
    move-result-object v10

    .line 486
    invoke-direct {v4, v9, v10}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 487
    .line 488
    .line 489
    new-instance v10, Ljava/io/BufferedOutputStream;

    .line 490
    .line 491
    new-instance v11, Ljava/io/FileOutputStream;

    .line 492
    .line 493
    invoke-direct {v11, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 494
    .line 495
    .line 496
    invoke-direct {v10, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_2 .. :try_end_2} :catch_17
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_16
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_15
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497
    .line 498
    .line 499
    if-eqz v5, :cond_c

    .line 500
    .line 501
    :try_start_3
    sget-object v11, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;
    :try_end_3
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 502
    .line 503
    move-object/from16 v16, v9

    .line 504
    .line 505
    const/16 v9, 0x55

    .line 506
    .line 507
    :try_start_4
    invoke-virtual {v5, v11, v9, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 508
    .line 509
    .line 510
    invoke-virtual {v10}, Ljava/io/BufferedOutputStream;->flush()V

    .line 511
    .line 512
    .line 513
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V

    .line 514
    .line 515
    .line 516
    const/4 v10, 0x0

    .line 517
    goto :goto_7

    .line 518
    :catch_0
    move-exception v0

    .line 519
    move-object/from16 v16, v9

    .line 520
    .line 521
    goto/16 :goto_8

    .line 522
    .line 523
    :catch_1
    move-exception v0

    .line 524
    move-object/from16 v16, v9

    .line 525
    .line 526
    goto/16 :goto_b

    .line 527
    .line 528
    :catch_2
    move-exception v0

    .line 529
    move-object/from16 v16, v9

    .line 530
    .line 531
    goto/16 :goto_e

    .line 532
    .line 533
    :cond_c
    move-object/from16 v16, v9

    .line 534
    .line 535
    :goto_7
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O000()Ljava/lang/String;

    .line 536
    .line 537
    .line 538
    move-result-object v9

    .line 539
    new-instance v11, Ljava/lang/StringBuilder;

    .line 540
    .line 541
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 542
    .line 543
    .line 544
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 545
    .line 546
    .line 547
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    .line 549
    .line 550
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    .line 552
    .line 553
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 554
    .line 555
    .line 556
    move-result-object v9

    .line 557
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 558
    .line 559
    .line 560
    move-result-object v11

    .line 561
    invoke-static {v11, v9}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 562
    .line 563
    .line 564
    move-result v11
    :try_end_4
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_12
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 565
    move/from16 v17, v12

    .line 566
    .line 567
    :try_start_5
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->O0o〇〇Oo()Ljava/lang/String;

    .line 568
    .line 569
    .line 570
    move-result-object v12
    :try_end_5
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_5 .. :try_end_5} :catch_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_10
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_f
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 571
    move-wide/from16 v19, v7

    .line 572
    .line 573
    :try_start_6
    new-instance v7, Ljava/lang/StringBuilder;

    .line 574
    .line 575
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 576
    .line 577
    .line 578
    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    .line 580
    .line 581
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 582
    .line 583
    .line 584
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    .line 586
    .line 587
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 588
    .line 589
    .line 590
    move-result-object v7

    .line 591
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 592
    .line 593
    .line 594
    move-result-object v4

    .line 595
    invoke-static {v4, v7}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 596
    .line 597
    .line 598
    move-result v4

    .line 599
    sget-object v8, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 600
    .line 601
    const-string v8, "save image FileUtil.copyPath = %s,copyPicDir = %b,thumbPath = %s,copyThumbPath = %b"
    :try_end_6
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_c
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 602
    .line 603
    const/4 v12, 0x4

    .line 604
    move-object/from16 v18, v2

    .line 605
    .line 606
    :try_start_7
    new-array v2, v12, [Ljava/lang/Object;

    .line 607
    .line 608
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 609
    .line 610
    .line 611
    move-result-object v21

    .line 612
    const/16 v22, 0x0

    .line 613
    .line 614
    aput-object v21, v2, v22

    .line 615
    .line 616
    const/16 v21, 0x1

    .line 617
    .line 618
    aput-object v9, v2, v21

    .line 619
    .line 620
    const/4 v9, 0x2

    .line 621
    aput-object v7, v2, v9

    .line 622
    .line 623
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 624
    .line 625
    .line 626
    move-result-object v7

    .line 627
    const/4 v9, 0x3

    .line 628
    aput-object v7, v2, v9

    .line 629
    .line 630
    invoke-static {v2, v12}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 631
    .line 632
    .line 633
    move-result-object v2

    .line 634
    invoke-static {v8, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 635
    .line 636
    .line 637
    move-result-object v2

    .line 638
    const-string v7, "format(format, *args)"

    .line 639
    .line 640
    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 641
    .line 642
    .line 643
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    .line 645
    .line 646
    if-eqz v11, :cond_d

    .line 647
    .line 648
    if-eqz v4, :cond_d

    .line 649
    .line 650
    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    .line 652
    .line 653
    :cond_d
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 654
    .line 655
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 656
    .line 657
    .line 658
    move-result v0
    :try_end_7
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_7 .. :try_end_7} :catch_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 659
    if-lez v0, :cond_e

    .line 660
    .line 661
    const/4 v2, 0x1

    .line 662
    :try_start_8
    new-array v0, v2, [Ljava/lang/Integer;

    .line 663
    .line 664
    iget-object v4, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 665
    .line 666
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 667
    .line 668
    .line 669
    move-result v7
    :try_end_8
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 670
    add-int/2addr v7, v2

    .line 671
    :try_start_9
    invoke-static {v4, v7}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇8(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;I)V

    .line 672
    .line 673
    .line 674
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 675
    .line 676
    .line 677
    move-result v2

    .line 678
    mul-int/lit8 v2, v2, 0x64

    .line 679
    .line 680
    iget-object v4, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 681
    .line 682
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 683
    .line 684
    .line 685
    move-result v4

    .line 686
    div-int/2addr v2, v4

    .line 687
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 688
    .line 689
    .line 690
    move-result-object v2

    .line 691
    const/4 v4, 0x0

    .line 692
    aput-object v2, v0, v4

    .line 693
    .line 694
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_9
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_9 .. :try_end_9} :catch_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 695
    .line 696
    .line 697
    const/4 v2, 0x1

    .line 698
    const/4 v7, 0x0

    .line 699
    goto/16 :goto_1b

    .line 700
    .line 701
    :cond_e
    const/4 v2, 0x1

    .line 702
    :try_start_a
    new-array v0, v2, [Ljava/lang/Integer;

    .line 703
    .line 704
    iget-object v4, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 705
    .line 706
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 707
    .line 708
    .line 709
    move-result v7

    .line 710
    add-int/2addr v7, v2

    .line 711
    invoke-static {v4, v7}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇8(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;I)V

    .line 712
    .line 713
    .line 714
    invoke-static {v4}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇O8o08O(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)I

    .line 715
    .line 716
    .line 717
    move-result v4

    .line 718
    mul-int/lit8 v4, v4, 0x64

    .line 719
    .line 720
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 721
    .line 722
    .line 723
    move-result-object v4
    :try_end_a
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_a .. :try_end_a} :catch_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_7
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 724
    const/4 v7, 0x0

    .line 725
    :try_start_b
    aput-object v4, v0, v7

    .line 726
    .line 727
    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_b
    .catch Lcom/intsig/camscanner/pdfengine/utils/PageRenderingException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 728
    .line 729
    .line 730
    goto/16 :goto_1b

    .line 731
    .line 732
    :catch_3
    move-exception v0

    .line 733
    goto/16 :goto_16

    .line 734
    .line 735
    :catch_4
    move-exception v0

    .line 736
    goto/16 :goto_18

    .line 737
    .line 738
    :catch_5
    move-exception v0

    .line 739
    goto/16 :goto_1a

    .line 740
    .line 741
    :catch_6
    move-exception v0

    .line 742
    goto :goto_a

    .line 743
    :catch_7
    move-exception v0

    .line 744
    goto :goto_d

    .line 745
    :catch_8
    move-exception v0

    .line 746
    goto :goto_10

    .line 747
    :catch_9
    move-exception v0

    .line 748
    goto :goto_9

    .line 749
    :catch_a
    move-exception v0

    .line 750
    goto :goto_c

    .line 751
    :catch_b
    move-exception v0

    .line 752
    goto :goto_f

    .line 753
    :catch_c
    move-exception v0

    .line 754
    move-object/from16 v18, v2

    .line 755
    .line 756
    goto :goto_9

    .line 757
    :catch_d
    move-exception v0

    .line 758
    move-object/from16 v18, v2

    .line 759
    .line 760
    goto :goto_c

    .line 761
    :catch_e
    move-exception v0

    .line 762
    move-object/from16 v18, v2

    .line 763
    .line 764
    goto :goto_f

    .line 765
    :catch_f
    move-exception v0

    .line 766
    move-object/from16 v18, v2

    .line 767
    .line 768
    move-wide/from16 v19, v7

    .line 769
    .line 770
    goto :goto_9

    .line 771
    :catch_10
    move-exception v0

    .line 772
    move-object/from16 v18, v2

    .line 773
    .line 774
    move-wide/from16 v19, v7

    .line 775
    .line 776
    goto :goto_c

    .line 777
    :catch_11
    move-exception v0

    .line 778
    move-object/from16 v18, v2

    .line 779
    .line 780
    move-wide/from16 v19, v7

    .line 781
    .line 782
    goto :goto_f

    .line 783
    :catch_12
    move-exception v0

    .line 784
    :goto_8
    move-object/from16 v18, v2

    .line 785
    .line 786
    move-wide/from16 v19, v7

    .line 787
    .line 788
    move/from16 v17, v12

    .line 789
    .line 790
    :goto_9
    const/4 v2, 0x1

    .line 791
    :goto_a
    const/4 v7, 0x0

    .line 792
    goto/16 :goto_16

    .line 793
    .line 794
    :catch_13
    move-exception v0

    .line 795
    :goto_b
    move-object/from16 v18, v2

    .line 796
    .line 797
    move-wide/from16 v19, v7

    .line 798
    .line 799
    move/from16 v17, v12

    .line 800
    .line 801
    :goto_c
    const/4 v2, 0x1

    .line 802
    :goto_d
    const/4 v7, 0x0

    .line 803
    goto/16 :goto_18

    .line 804
    .line 805
    :catch_14
    move-exception v0

    .line 806
    :goto_e
    move-object/from16 v18, v2

    .line 807
    .line 808
    move-wide/from16 v19, v7

    .line 809
    .line 810
    move/from16 v17, v12

    .line 811
    .line 812
    :goto_f
    const/4 v2, 0x1

    .line 813
    :goto_10
    const/4 v7, 0x0

    .line 814
    goto/16 :goto_1a

    .line 815
    .line 816
    :catch_15
    move-exception v0

    .line 817
    move-object/from16 v18, v2

    .line 818
    .line 819
    move-wide/from16 v19, v7

    .line 820
    .line 821
    move-object/from16 v16, v9

    .line 822
    .line 823
    goto :goto_11

    .line 824
    :catch_16
    move-exception v0

    .line 825
    move-object/from16 v18, v2

    .line 826
    .line 827
    move-wide/from16 v19, v7

    .line 828
    .line 829
    move-object/from16 v16, v9

    .line 830
    .line 831
    goto :goto_12

    .line 832
    :catch_17
    move-exception v0

    .line 833
    move-object/from16 v18, v2

    .line 834
    .line 835
    move-wide/from16 v19, v7

    .line 836
    .line 837
    move-object/from16 v16, v9

    .line 838
    .line 839
    goto :goto_13

    .line 840
    :catchall_0
    move-exception v0

    .line 841
    move-object v6, v5

    .line 842
    goto :goto_14

    .line 843
    :catch_18
    move-exception v0

    .line 844
    move-object/from16 v18, v2

    .line 845
    .line 846
    move-wide/from16 v19, v7

    .line 847
    .line 848
    move-object/from16 v16, v9

    .line 849
    .line 850
    move-object/from16 v24, v11

    .line 851
    .line 852
    :goto_11
    move/from16 v17, v12

    .line 853
    .line 854
    const/4 v2, 0x1

    .line 855
    const/4 v7, 0x0

    .line 856
    goto :goto_15

    .line 857
    :catch_19
    move-exception v0

    .line 858
    move-object/from16 v18, v2

    .line 859
    .line 860
    move-wide/from16 v19, v7

    .line 861
    .line 862
    move-object/from16 v16, v9

    .line 863
    .line 864
    move-object/from16 v24, v11

    .line 865
    .line 866
    :goto_12
    move/from16 v17, v12

    .line 867
    .line 868
    const/4 v2, 0x1

    .line 869
    const/4 v7, 0x0

    .line 870
    goto :goto_17

    .line 871
    :catch_1a
    move-exception v0

    .line 872
    move-object/from16 v18, v2

    .line 873
    .line 874
    move-wide/from16 v19, v7

    .line 875
    .line 876
    move-object/from16 v16, v9

    .line 877
    .line 878
    move-object/from16 v24, v11

    .line 879
    .line 880
    :goto_13
    move/from16 v17, v12

    .line 881
    .line 882
    const/4 v2, 0x1

    .line 883
    const/4 v7, 0x0

    .line 884
    goto :goto_19

    .line 885
    :catchall_1
    move-exception v0

    .line 886
    const/4 v6, 0x0

    .line 887
    :goto_14
    const/4 v10, 0x0

    .line 888
    goto/16 :goto_1d

    .line 889
    .line 890
    :catch_1b
    move-exception v0

    .line 891
    move-object/from16 v18, v2

    .line 892
    .line 893
    move-wide/from16 v19, v7

    .line 894
    .line 895
    move-object/from16 v16, v9

    .line 896
    .line 897
    move-object/from16 p1, v10

    .line 898
    .line 899
    move-object/from16 v24, v11

    .line 900
    .line 901
    move/from16 v17, v12

    .line 902
    .line 903
    const/4 v2, 0x1

    .line 904
    const/4 v7, 0x0

    .line 905
    const/4 v5, 0x0

    .line 906
    :goto_15
    const/4 v10, 0x0

    .line 907
    :goto_16
    :try_start_c
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 908
    .line 909
    .line 910
    goto :goto_1b

    .line 911
    :catch_1c
    move-exception v0

    .line 912
    move-object/from16 v18, v2

    .line 913
    .line 914
    move-wide/from16 v19, v7

    .line 915
    .line 916
    move-object/from16 v16, v9

    .line 917
    .line 918
    move-object/from16 p1, v10

    .line 919
    .line 920
    move-object/from16 v24, v11

    .line 921
    .line 922
    move/from16 v17, v12

    .line 923
    .line 924
    const/4 v2, 0x1

    .line 925
    const/4 v7, 0x0

    .line 926
    const/4 v5, 0x0

    .line 927
    :goto_17
    const/4 v10, 0x0

    .line 928
    :goto_18
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 929
    .line 930
    .line 931
    goto :goto_1b

    .line 932
    :catch_1d
    move-exception v0

    .line 933
    move-object/from16 v18, v2

    .line 934
    .line 935
    move-wide/from16 v19, v7

    .line 936
    .line 937
    move-object/from16 v16, v9

    .line 938
    .line 939
    move-object/from16 p1, v10

    .line 940
    .line 941
    move-object/from16 v24, v11

    .line 942
    .line 943
    move/from16 v17, v12

    .line 944
    .line 945
    const/4 v2, 0x1

    .line 946
    const/4 v7, 0x0

    .line 947
    const/4 v5, 0x0

    .line 948
    :goto_19
    const/4 v10, 0x0

    .line 949
    :goto_1a
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 950
    .line 951
    .line 952
    :goto_1b
    invoke-direct {v1, v5}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->Oo08(Landroid/graphics/Bitmap;)V

    .line 953
    .line 954
    .line 955
    invoke-static {v10}, Lcom/intsig/utils/FileUtil;->〇o〇(Ljava/io/Closeable;)V

    .line 956
    .line 957
    .line 958
    invoke-virtual {v6, v13}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->closePage(I)V

    .line 959
    .line 960
    .line 961
    const/16 v0, 0x1e

    .line 962
    .line 963
    if-lt v14, v0, :cond_f

    .line 964
    .line 965
    invoke-virtual {v6}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->dispose()V

    .line 966
    .line 967
    .line 968
    const/4 v0, 0x0

    .line 969
    const/4 v14, 0x0

    .line 970
    goto :goto_1c

    .line 971
    :cond_f
    move-object v0, v6

    .line 972
    :goto_1c
    add-int/lit8 v13, v13, 0x1

    .line 973
    .line 974
    move-object/from16 v10, p1

    .line 975
    .line 976
    move-object/from16 v9, v16

    .line 977
    .line 978
    move/from16 v12, v17

    .line 979
    .line 980
    move-object/from16 v2, v18

    .line 981
    .line 982
    move-wide/from16 v7, v19

    .line 983
    .line 984
    move-object/from16 v11, v24

    .line 985
    .line 986
    const/4 v4, 0x1

    .line 987
    const/4 v5, 0x0

    .line 988
    const/4 v6, 0x0

    .line 989
    goto/16 :goto_6

    .line 990
    .line 991
    :catchall_2
    move-exception v0

    .line 992
    move-object v6, v5

    .line 993
    :goto_1d
    invoke-direct {v1, v6}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->Oo08(Landroid/graphics/Bitmap;)V

    .line 994
    .line 995
    .line 996
    invoke-static {v10}, Lcom/intsig/utils/FileUtil;->〇o〇(Ljava/io/Closeable;)V

    .line 997
    .line 998
    .line 999
    throw v0

    .line 1000
    :cond_10
    move-wide/from16 v19, v7

    .line 1001
    .line 1002
    if-eqz v0, :cond_11

    .line 1003
    .line 1004
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdfengine/entity/PdfFile;->dispose()V

    .line 1005
    .line 1006
    .line 1007
    :cond_11
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    .line 1008
    .line 1009
    .line 1010
    move-result v0

    .line 1011
    if-gtz v0, :cond_12

    .line 1012
    .line 1013
    const/4 v2, 0x0

    .line 1014
    return-object v2

    .line 1015
    :cond_12
    iget-object v13, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 1016
    .line 1017
    invoke-static {v13}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->〇8o8o〇(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Landroid/content/Context;

    .line 1018
    .line 1019
    .line 1020
    move-result-object v14

    .line 1021
    iget-object v0, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 1022
    .line 1023
    iget-object v2, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 1024
    .line 1025
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 1026
    .line 1027
    .line 1028
    invoke-virtual {v2}, Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;->getTitle()Ljava/lang/String;

    .line 1029
    .line 1030
    .line 1031
    move-result-object v2

    .line 1032
    const-string v4, "mPdfFree!!.title"

    .line 1033
    .line 1034
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1035
    .line 1036
    .line 1037
    invoke-static {v0, v2}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->Oo08(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;Ljava/lang/String;)Ljava/lang/String;

    .line 1038
    .line 1039
    .line 1040
    move-result-object v0

    .line 1041
    iget-object v2, v1, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 1042
    .line 1043
    invoke-static {v2}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->oO80(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)J

    .line 1044
    .line 1045
    .line 1046
    move-result-wide v17

    .line 1047
    move-object v2, v15

    .line 1048
    move-object v15, v0

    .line 1049
    move-object/from16 v16, v2

    .line 1050
    .line 1051
    invoke-virtual/range {v13 .. v18}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->o〇O(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;J)Landroid/net/Uri;

    .line 1052
    .line 1053
    .line 1054
    move-result-object v6

    .line 1055
    :goto_1e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 1056
    .line 1057
    .line 1058
    move-result-wide v4

    .line 1059
    sub-long v4, v4, v19

    .line 1060
    .line 1061
    new-instance v0, Ljava/lang/StringBuilder;

    .line 1062
    .line 1063
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1064
    .line 1065
    .line 1066
    const-string v2, "finish import, consume = "

    .line 1067
    .line 1068
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1069
    .line 1070
    .line 1071
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1072
    .line 1073
    .line 1074
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 1075
    .line 1076
    .line 1077
    move-result-object v0

    .line 1078
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    .line 1080
    .line 1081
    return-object v6
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method protected 〇o〇(Landroid/net/Uri;)V
    .locals 10

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "result docUri= "

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "LocalDocImportProcessor"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const-string v0, "import_pdf"

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/appsflyer/AppsFlyerHelper;->Oo08(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const-string v0, "pdfimport_ok"

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    const-string v2, "CSPdfimportPop"

    .line 34
    .line 35
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->oo88o8O(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 36
    .line 37
    .line 38
    const/4 v0, 0x0

    .line 39
    const/4 v3, 0x0

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 v0, 0x2

    .line 42
    const/4 v3, 0x2

    .line 43
    :goto_0
    iget-wide v8, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->Oo08:J

    .line 44
    .line 45
    const-wide/16 v0, -0x1

    .line 46
    .line 47
    cmp-long v2, v8, v0

    .line 48
    .line 49
    if-lez v2, :cond_1

    .line 50
    .line 51
    const/4 v6, 0x0

    .line 52
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o〇:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;

    .line 53
    .line 54
    if-eqz v4, :cond_2

    .line 55
    .line 56
    iget-object v7, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 57
    .line 58
    move-object v5, p1

    .line 59
    invoke-interface/range {v4 .. v9}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;->〇080(Landroid/net/Uri;ILcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;J)V

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->〇o〇:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;

    .line 64
    .line 65
    if-eqz v1, :cond_2

    .line 66
    .line 67
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->O8:Lcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;

    .line 68
    .line 69
    const-wide/16 v5, 0x0

    .line 70
    .line 71
    const/16 v7, 0x8

    .line 72
    .line 73
    const/4 v8, 0x0

    .line 74
    move-object v2, p1

    .line 75
    invoke-static/range {v1 .. v8}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener$DefaultImpls;->〇080(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$PdfProcessListener;Landroid/net/Uri;ILcom/intsig/camscanner/pdfengine/entity/PdfFileDataFree;JILjava/lang/Object;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor$LocalPdfProcessor;->o〇0:Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;

    .line 79
    .line 80
    invoke-static {p1}, Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/LocalDocImportProcessor;)Lcom/intsig/camscanner/office_doc/DocImportDialog;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    if-eqz p1, :cond_3

    .line 85
    .line 86
    invoke-virtual {p1}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 87
    .line 88
    .line 89
    :cond_3
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

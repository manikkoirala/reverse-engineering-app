.class public final Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "PdfEditFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$WhenMappings;,
        Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo0〇Ooo:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇〇0o〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/document/widget/PagesView$ElementData;

.field private final O88O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O8o08O8O:Ljava/lang/Boolean;

.field private OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

.field private OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo80:F

.field private final Ooo08:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇o88o08〇:I

.field private o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

.field private final o8o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

.field private final o8〇OO:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Ljava/lang/String;

.field private final oOO〇〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$onSeekBarChangeListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Ljava/lang/String;

.field private oOo〇8o008:Ljava/lang/String;

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooO:Lcom/intsig/document/widget/PagesView$ImageArgs;

.field private ooo0〇〇O:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

.field private o〇oO:Z

.field private 〇00O0:I

.field private 〇080OO8〇0:Landroid/net/Uri;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

.field private 〇08〇o0O:Lkotlinx/coroutines/Job;

.field private 〇0O:Ljava/lang/String;

.field private 〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

.field private final 〇OO8ooO8〇:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

.field private 〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

.field private 〇O〇〇O8:Z

.field private final 〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇〇o〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mISignatureEditView$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0〇Ooo:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "PdfEditFragment::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O8o08O8O:Ljava/lang/Boolean;

    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 11
    .line 12
    new-instance v0, Ljava/util/HashSet;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo0〇〇O:Ljava/util/HashSet;

    .line 18
    .line 19
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mPdfEncUtil$2;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mPdfEncUtil$2;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o0O:Lkotlin/Lazy;

    .line 31
    .line 32
    new-instance v1, L〇0oO〇oo00/〇O00;

    .line 33
    .line 34
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇O00;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O88O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 38
    .line 39
    new-instance v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$onSeekBarChangeListener$1;

    .line 40
    .line 41
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$onSeekBarChangeListener$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 42
    .line 43
    .line 44
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOO〇〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$onSeekBarChangeListener$1;

    .line 45
    .line 46
    new-instance v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mShareHelper$2;

    .line 47
    .line 48
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mShareHelper$2;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o:Lkotlin/Lazy;

    .line 56
    .line 57
    new-instance v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$loadingDialog$2;

    .line 58
    .line 59
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$loadingDialog$2;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 60
    .line 61
    .line 62
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 67
    .line 68
    new-instance v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mISignatureEditView$1;

    .line 69
    .line 70
    invoke-direct {v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mISignatureEditView$1;-><init>()V

    .line 71
    .line 72
    .line 73
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇o〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mISignatureEditView$1;

    .line 74
    .line 75
    const/4 v0, -0x1

    .line 76
    iput v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 77
    .line 78
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 79
    .line 80
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 81
    .line 82
    .line 83
    new-instance v1, L〇0oO〇oo00/〇〇8O0〇8;

    .line 84
    .line 85
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    const-string v1, "registerForActivityResul\u2026)\n            }\n        }"

    .line 93
    .line 94
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;

    .line 98
    .line 99
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 100
    .line 101
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 102
    .line 103
    .line 104
    new-instance v1, L〇0oO〇oo00/〇0〇O0088o;

    .line 105
    .line 106
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    const-string v1, "registerForActivityResul\u2026atureFile(path)\n        }"

    .line 114
    .line 115
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO:Landroidx/activity/result/ActivityResultLauncher;

    .line 119
    .line 120
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 121
    .line 122
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 123
    .line 124
    .line 125
    new-instance v2, L〇0oO〇oo00/OoO8;

    .line 126
    .line 127
    invoke-direct {v2, p0}, L〇0oO〇oo00/OoO8;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {p0, v0, v2}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    const-string v2, "registerForActivityResul\u2026n_handwriting\")\n        }"

    .line 135
    .line 136
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Ooo08:Landroidx/activity/result/ActivityResultLauncher;

    .line 140
    .line 141
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 142
    .line 143
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 144
    .line 145
    .line 146
    new-instance v2, L〇0oO〇oo00/o800o8O;

    .line 147
    .line 148
    invoke-direct {v2, p0}, L〇0oO〇oo00/o800o8O;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {p0, v0, v2}, Landroidx/fragment/app/Fragment;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO8ooO8〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 159
    .line 160
    return-void
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final O008oO0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;I)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->〇080:Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;

    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "childFragmentManager"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1, v2, p0}, Lcom/intsig/camscanner/office_doc/share/OfficeDocShareManager;->o〇8(Landroidx/fragment/app/FragmentManager;JLcom/intsig/document/widget/PagesView$WatermarkArgs;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O00OoO〇()V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8O0O80〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/util/PdfUtils;->〇080:Lcom/intsig/camscanner/util/PdfUtils;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/PdfUtils;->〇oOO8O8()Landroid/graphics/Bitmap;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    int-to-float v5, v1

    .line 21
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    int-to-float v6, v1

    .line 26
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const/16 v3, 0x8

    .line 33
    .line 34
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    int-to-float v2, v2

    .line 39
    neg-float v2, v2

    .line 40
    sub-float v4, v2, v5

    .line 41
    .line 42
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    int-to-float v1, v1

    .line 51
    neg-float v1, v1

    .line 52
    sub-float v7, v1, v6

    .line 53
    .line 54
    new-instance v8, Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;

    .line 55
    .line 56
    move-object v1, v8

    .line 57
    move-object v2, v0

    .line 58
    move v3, v4

    .line 59
    move v4, v7

    .line 60
    invoke-direct/range {v1 .. v6}, Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;-><init>(Landroid/graphics/Bitmap;FFFF)V

    .line 61
    .line 62
    .line 63
    sget-object v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    .line 70
    .line 71
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .line 73
    .line 74
    const-string v3, "addImageWaterMark bitmap size:"

    .line 75
    .line 76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    if-eqz v0, :cond_1

    .line 94
    .line 95
    invoke-virtual {v0, v8}, Lcom/intsig/document/widget/DocumentView;->beginImageWatermark(Lcom/intsig/document/widget/PagesView$ImageWatermarkArgs;)V

    .line 96
    .line 97
    .line 98
    :cond_1
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O088O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 7
    .line 8
    new-instance v2, L〇0oO〇oo00/oo88o8O;

    .line 9
    .line 10
    invoke-direct {v2}, L〇0oO〇oo00/oo88o8O;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setSignatureColors(Z)V

    .line 20
    .line 21
    .line 22
    new-instance v2, L〇0oO〇oo00/〇oo〇;

    .line 23
    .line 24
    invoke-direct {v2, p0}, L〇0oO〇oo00/〇oo〇;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;)V

    .line 28
    .line 29
    .line 30
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->ooo0〇〇O:Landroid/widget/SeekBar;

    .line 31
    .line 32
    new-instance v2, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureAdjustView$3;

    .line 33
    .line 34
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureAdjustView$3;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 38
    .line 39
    .line 40
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->o〇00O:Landroid/widget/FrameLayout;

    .line 41
    .line 42
    new-instance v2, L〇0oO〇oo00/〇o00〇〇Oo;

    .line 43
    .line 44
    invoke-direct {v2, p0}, L〇0oO〇oo00/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 51
    .line 52
    new-instance v1, L〇0oO〇oo00/〇o〇;

    .line 53
    .line 54
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇o〇;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O08o(I)V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "updateSignatureTabViewUiState : "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇00O0:I

    .line 24
    .line 25
    if-ne v1, p1, :cond_0

    .line 26
    .line 27
    const-string p1, "same state"

    .line 28
    .line 29
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 34
    .line 35
    if-eqz v0, :cond_c

    .line 36
    .line 37
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 38
    .line 39
    if-nez v1, :cond_1

    .line 40
    .line 41
    goto/16 :goto_3

    .line 42
    .line 43
    :cond_1
    if-eqz v0, :cond_c

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 46
    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    goto/16 :goto_3

    .line 50
    .line 51
    :cond_2
    const-string v2, "mBottomBarBinding"

    .line 52
    .line 53
    const-string v3, "mBottomBarBinding.root"

    .line 54
    .line 55
    const/4 v4, 0x0

    .line 56
    const/4 v5, 0x0

    .line 57
    const/16 v6, 0x8

    .line 58
    .line 59
    if-eqz p1, :cond_9

    .line 60
    .line 61
    const/4 v7, 0x2

    .line 62
    if-eq p1, v7, :cond_7

    .line 63
    .line 64
    const/4 v2, 0x3

    .line 65
    if-eq p1, v2, :cond_3

    .line 66
    .line 67
    goto/16 :goto_2

    .line 68
    .line 69
    :cond_3
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 76
    .line 77
    if-eqz v0, :cond_b

    .line 78
    .line 79
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    mul-int/lit8 v1, v1, 0x2

    .line 84
    .line 85
    add-int/lit8 v1, v1, 0x4

    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 88
    .line 89
    if-eqz v2, :cond_4

    .line 90
    .line 91
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 92
    .line 93
    if-eqz v2, :cond_4

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelect(Ljava/lang/Integer;)V

    .line 104
    .line 105
    .line 106
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 107
    .line 108
    if-eqz v0, :cond_5

    .line 109
    .line 110
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->ooo0〇〇O:Landroid/widget/SeekBar;

    .line 111
    .line 112
    :cond_5
    if-nez v4, :cond_6

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_6
    invoke-virtual {v4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 116
    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_7
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 126
    .line 127
    if-nez v0, :cond_8

    .line 128
    .line 129
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    goto :goto_0

    .line 133
    :cond_8
    move-object v4, v0

    .line 134
    :goto_0
    invoke-virtual {v4}, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 142
    .line 143
    .line 144
    goto :goto_2

    .line 145
    :cond_9
    sget-object v7, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 146
    .line 147
    iput-object v7, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 148
    .line 149
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 153
    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 156
    .line 157
    if-nez v0, :cond_a

    .line 158
    .line 159
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    goto :goto_1

    .line 163
    :cond_a
    move-object v4, v0

    .line 164
    :goto_1
    invoke-virtual {v4}, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 172
    .line 173
    .line 174
    :cond_b
    :goto_2
    iput p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇00O0:I

    .line 175
    .line 176
    :cond_c
    :goto_3
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic O08〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)J
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇O〇〇O8:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o088O8800()Lcom/intsig/document/widget/DocumentView;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o088O8800()Lcom/intsig/document/widget/DocumentView;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 17
    .line 18
    const-string v1, "pdfView is not prepare"

    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    const v1, 0x7f131a4c

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O0O0〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/camscanner/office_doc/data/EditMode;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O80OO(Lcom/intsig/camscanner/office_doc/data/EditMode;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O0o0(Ljava/lang/String;Z)V
    .locals 7

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "changeTextAnnotation content:"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, ", defaultInsert:"

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 34
    .line 35
    const-string v1, "mAnnotationStyleBinding"

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    if-nez v0, :cond_0

    .line 39
    .line 40
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    move-object v0, v2

    .line 44
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->getCurrentColor()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 51
    .line 52
    if-nez v3, :cond_1

    .line 53
    .line 54
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    move-object v3, v2

    .line 58
    :cond_1
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 59
    .line 60
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    int-to-float v1, v1

    .line 65
    const/high16 v3, 0x3f800000    # 1.0f

    .line 66
    .line 67
    mul-float v1, v1, v3

    .line 68
    .line 69
    const/16 v3, 0x8

    .line 70
    .line 71
    int-to-float v3, v3

    .line 72
    add-float/2addr v1, v3

    .line 73
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 74
    .line 75
    const/4 v4, 0x0

    .line 76
    const/4 v5, 0x1

    .line 77
    if-eqz v3, :cond_3

    .line 78
    .line 79
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 80
    .line 81
    .line 82
    move-result v3

    .line 83
    if-nez v3, :cond_2

    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_2
    const/4 v3, 0x0

    .line 87
    goto :goto_1

    .line 88
    :cond_3
    :goto_0
    const/4 v3, 0x1

    .line 89
    :goto_1
    if-eqz v3, :cond_4

    .line 90
    .line 91
    if-eqz p2, :cond_6

    .line 92
    .line 93
    :cond_4
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 94
    .line 95
    if-nez v3, :cond_5

    .line 96
    .line 97
    const-string v3, "mWaterMarkBarBinding"

    .line 98
    .line 99
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_5
    move-object v2, v3

    .line 104
    :goto_2
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 105
    .line 106
    const-string v3, "mWaterMarkBarBinding.tvClearMark"

    .line 107
    .line 108
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    invoke-direct {p0, v2, v5}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 112
    .line 113
    .line 114
    :cond_6
    if-eqz p1, :cond_8

    .line 115
    .line 116
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 117
    .line 118
    .line 119
    move-result v2

    .line 120
    if-nez v2, :cond_7

    .line 121
    .line 122
    goto :goto_3

    .line 123
    :cond_7
    const/4 v2, 0x0

    .line 124
    goto :goto_4

    .line 125
    :cond_8
    :goto_3
    const/4 v2, 0x1

    .line 126
    :goto_4
    if-eqz v2, :cond_9

    .line 127
    .line 128
    const-string p1, " "

    .line 129
    .line 130
    :cond_9
    new-instance v2, Lcom/intsig/document/widget/PagesView$TextArgs;

    .line 131
    .line 132
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo0:Ljava/lang/String;

    .line 133
    .line 134
    sget-object v6, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 135
    .line 136
    invoke-virtual {v6}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 137
    .line 138
    .line 139
    move-result-object v6

    .line 140
    invoke-static {v6, v1}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 141
    .line 142
    .line 143
    move-result v1

    .line 144
    float-to-int v1, v1

    .line 145
    int-to-float v1, v1

    .line 146
    invoke-direct {v2, p1, v3, v1, v0}, Lcom/intsig/document/widget/PagesView$TextArgs;-><init>(Ljava/lang/String;Ljava/lang/String;FI)V

    .line 147
    .line 148
    .line 149
    iput-boolean v4, v2, Lcom/intsig/document/widget/PagesView$BaseArgs;->deleteable:Z

    .line 150
    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 152
    .line 153
    .line 154
    move-result-object p1

    .line 155
    if-eqz p1, :cond_c

    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0O:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 158
    .line 159
    if-eqz v0, :cond_b

    .line 160
    .line 161
    if-nez p2, :cond_b

    .line 162
    .line 163
    if-nez v0, :cond_a

    .line 164
    .line 165
    goto :goto_5

    .line 166
    :cond_a
    iput-object v2, v0, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 167
    .line 168
    :goto_5
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->updateElement(Lcom/intsig/document/widget/PagesView$ElementData;)V

    .line 169
    .line 170
    .line 171
    goto :goto_6

    .line 172
    :cond_b
    invoke-virtual {p1, v2, v5}, Lcom/intsig/document/widget/DocumentView;->beginTextStampAnnot(Lcom/intsig/document/widget/PagesView$TextArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0O:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 177
    .line 178
    :cond_c
    :goto_6
    return-void
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private static final O0oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 p2, 0x0

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/document/widget/DocumentView;->onBackPressed()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    const/4 v0, 0x1

    .line 18
    if-ne p1, v0, :cond_0

    .line 19
    .line 20
    const/4 p2, 0x1

    .line 21
    :cond_0
    if-eqz p2, :cond_1

    .line 22
    .line 23
    sget-object p1, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->interceptBackPressed()Z

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O0〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo80:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oO8o〇08〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O0〇8〇()V
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 12
    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->getCurSignatureStrategy()Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    move-object v1, v2

    .line 21
    :goto_0
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-interface {v1}, Lcom/intsig/camscanner/pdf/signature/tab/ISignatureStrategy;->〇o00〇〇Oo()Ljava/util/List;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    if-eqz v3, :cond_1

    .line 28
    .line 29
    check-cast v3, Ljava/util/Collection;

    .line 30
    .line 31
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    goto :goto_1

    .line 36
    :cond_1
    move-object v3, v2

    .line 37
    :goto_1
    instance-of v1, v1, Lcom/intsig/camscanner/pdf/signature/tab/SignatureStrategy;

    .line 38
    .line 39
    if-eqz v1, :cond_2

    .line 40
    .line 41
    if-eqz v3, :cond_2

    .line 42
    .line 43
    const v4, 0x7f131427

    .line 44
    .line 45
    .line 46
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    .line 52
    .line 53
    :cond_2
    const/4 v4, 0x1

    .line 54
    const/4 v5, 0x0

    .line 55
    const/4 v6, 0x3

    .line 56
    const/4 v7, 0x2

    .line 57
    if-nez v3, :cond_3

    .line 58
    .line 59
    new-array v3, v6, [Ljava/lang/Integer;

    .line 60
    .line 61
    const v8, 0x7f13021d

    .line 62
    .line 63
    .line 64
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 65
    .line 66
    .line 67
    move-result-object v8

    .line 68
    aput-object v8, v3, v5

    .line 69
    .line 70
    const v8, 0x7f131e8c

    .line 71
    .line 72
    .line 73
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 74
    .line 75
    .line 76
    move-result-object v8

    .line 77
    aput-object v8, v3, v4

    .line 78
    .line 79
    const v8, 0x7f131e8b

    .line 80
    .line 81
    .line 82
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 83
    .line 84
    .line 85
    move-result-object v8

    .line 86
    aput-object v8, v3, v7

    .line 87
    .line 88
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 89
    .line 90
    .line 91
    move-result-object v3

    .line 92
    :cond_3
    if-eqz v1, :cond_4

    .line 93
    .line 94
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    if-le v1, v6, :cond_4

    .line 99
    .line 100
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 101
    .line 102
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 103
    .line 104
    .line 105
    move-result-object v6

    .line 106
    check-cast v6, Ljava/lang/Number;

    .line 107
    .line 108
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 109
    .line 110
    .line 111
    move-result v6

    .line 112
    invoke-virtual {p0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v6

    .line 116
    const v8, 0x7f0809e1

    .line 117
    .line 118
    .line 119
    invoke-direct {v1, v7, v6, v8}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    :cond_4
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 126
    .line 127
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 128
    .line 129
    .line 130
    move-result-object v6

    .line 131
    check-cast v6, Ljava/lang/Number;

    .line 132
    .line 133
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    invoke-virtual {p0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v6

    .line 141
    const v8, 0x7f08052e

    .line 142
    .line 143
    .line 144
    invoke-direct {v1, v5, v6, v8}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 145
    .line 146
    .line 147
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    .line 149
    .line 150
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 151
    .line 152
    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    move-result-object v6

    .line 156
    check-cast v6, Ljava/lang/Number;

    .line 157
    .line 158
    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    .line 159
    .line 160
    .line 161
    move-result v6

    .line 162
    invoke-virtual {p0, v6}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v6

    .line 166
    const v8, 0x7f08052c

    .line 167
    .line 168
    .line 169
    invoke-direct {v1, v4, v6, v8}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 170
    .line 171
    .line 172
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    .line 174
    .line 175
    new-instance v1, Lcom/intsig/app/AlertBottomDialog;

    .line 176
    .line 177
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 178
    .line 179
    const v6, 0x7f140004

    .line 180
    .line 181
    .line 182
    invoke-direct {v1, v4, v6}, Lcom/intsig/app/AlertBottomDialog;-><init>(Landroid/content/Context;I)V

    .line 183
    .line 184
    .line 185
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 186
    .line 187
    .line 188
    move-result-object v3

    .line 189
    check-cast v3, Ljava/lang/Number;

    .line 190
    .line 191
    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    .line 192
    .line 193
    .line 194
    move-result v3

    .line 195
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v3

    .line 199
    invoke-virtual {v1, v3, v0}, Lcom/intsig/app/AlertBottomDialog;->〇o00〇〇Oo(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 200
    .line 201
    .line 202
    new-instance v3, L〇0oO〇oo00/〇O888o0o;

    .line 203
    .line 204
    invoke-direct {v3, v0, p0}, L〇0oO〇oo00/〇O888o0o;-><init>(Ljava/util/ArrayList;Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {v1, v3}, Lcom/intsig/app/AlertBottomDialog;->O8(Landroid/content/DialogInterface$OnClickListener;)V

    .line 208
    .line 209
    .line 210
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 211
    .line 212
    const-string v3, "add_new_signature"

    .line 213
    .line 214
    invoke-static {v0, v3, v2, v7, v2}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->o〇0(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 215
    .line 216
    .line 217
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇〇888()V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v1}, Lcom/intsig/app/AlertBottomDialog;->show()V

    .line 221
    .line 222
    .line 223
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final O80OO(Lcom/intsig/camscanner/office_doc/data/EditMode;)V
    .locals 9

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 4
    .line 5
    const-string v1, "mBottomBarBinding"

    .line 6
    .line 7
    const-string v2, "mBottomBarBinding.root"

    .line 8
    .line 9
    const/4 v3, 0x1

    .line 10
    const/4 v4, 0x0

    .line 11
    const/4 v5, 0x0

    .line 12
    if-ne p1, v0, :cond_3

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 15
    .line 16
    if-nez p1, :cond_0

    .line 17
    .line 18
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    move-object v5, p1

    .line 23
    :goto_0
    invoke-virtual {v5}, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 38
    .line 39
    if-eqz p1, :cond_1

    .line 40
    .line 41
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 45
    .line 46
    if-eqz p1, :cond_2

    .line 47
    .line 48
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->O0O:Landroid/view/View;

    .line 49
    .line 50
    if-eqz p1, :cond_2

    .line 51
    .line 52
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 53
    .line 54
    .line 55
    :cond_2
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 56
    .line 57
    .line 58
    goto/16 :goto_4

    .line 59
    .line 60
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 61
    .line 62
    if-nez p1, :cond_4

    .line 63
    .line 64
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    move-object p1, v5

    .line 68
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 79
    .line 80
    if-eqz p1, :cond_5

    .line 81
    .line 82
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 83
    .line 84
    if-eqz p1, :cond_5

    .line 85
    .line 86
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 87
    .line 88
    .line 89
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 90
    .line 91
    if-eqz p1, :cond_6

    .line 92
    .line 93
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->O0O:Landroid/view/View;

    .line 94
    .line 95
    if-eqz p1, :cond_6

    .line 96
    .line 97
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 98
    .line 99
    .line 100
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 101
    .line 102
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$WhenMappings;->〇080:[I

    .line 103
    .line 104
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    aget p1, v0, p1

    .line 109
    .line 110
    const-string v0, "mSmudgeAnnotationBinding.root"

    .line 111
    .line 112
    const-string v1, "mWaterMarkBarBinding.root"

    .line 113
    .line 114
    const-string v2, "mWaterMarkBarBinding"

    .line 115
    .line 116
    const-string v6, "mSmudgeAnnotationBinding"

    .line 117
    .line 118
    if-eq p1, v3, :cond_11

    .line 119
    .line 120
    const/4 v7, 0x2

    .line 121
    const-string v8, "mAnnotationStyleBinding"

    .line 122
    .line 123
    if-eq p1, v7, :cond_b

    .line 124
    .line 125
    const/4 v7, 0x3

    .line 126
    if-eq p1, v7, :cond_7

    .line 127
    .line 128
    goto/16 :goto_4

    .line 129
    .line 130
    :cond_7
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 131
    .line 132
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->OO0o〇〇〇〇0()V

    .line 133
    .line 134
    .line 135
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 136
    .line 137
    if-nez p1, :cond_8

    .line 138
    .line 139
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    move-object p1, v5

    .line 143
    :cond_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 144
    .line 145
    .line 146
    move-result-object p1

    .line 147
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 151
    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 154
    .line 155
    if-nez p1, :cond_9

    .line 156
    .line 157
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    move-object p1, v5

    .line 161
    :cond_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 162
    .line 163
    .line 164
    move-result-object p1

    .line 165
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 169
    .line 170
    .line 171
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 172
    .line 173
    .line 174
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 175
    .line 176
    if-nez p1, :cond_a

    .line 177
    .line 178
    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 179
    .line 180
    .line 181
    goto :goto_1

    .line 182
    :cond_a
    move-object v5, p1

    .line 183
    :goto_1
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 184
    .line 185
    const v0, 0x7f130c03

    .line 186
    .line 187
    .line 188
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v0

    .line 192
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    .line 194
    .line 195
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 196
    .line 197
    const v0, 0x7f08080a

    .line 198
    .line 199
    .line 200
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 201
    .line 202
    .line 203
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 204
    .line 205
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 206
    .line 207
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o800o8O()I

    .line 208
    .line 209
    .line 210
    move-result v1

    .line 211
    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 212
    .line 213
    .line 214
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 215
    .line 216
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->OoO8()I

    .line 217
    .line 218
    .line 219
    move-result v0

    .line 220
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelectIndex(I)V

    .line 221
    .line 222
    .line 223
    goto/16 :goto_4

    .line 224
    .line 225
    :cond_b
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 226
    .line 227
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->O8()V

    .line 228
    .line 229
    .line 230
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 231
    .line 232
    if-nez p1, :cond_c

    .line 233
    .line 234
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 235
    .line 236
    .line 237
    move-object p1, v5

    .line 238
    :cond_c
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 239
    .line 240
    .line 241
    move-result-object p1

    .line 242
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 246
    .line 247
    .line 248
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 249
    .line 250
    if-nez p1, :cond_d

    .line 251
    .line 252
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    move-object p1, v5

    .line 256
    :cond_d
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 257
    .line 258
    .line 259
    move-result-object p1

    .line 260
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    .line 262
    .line 263
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 264
    .line 265
    .line 266
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 267
    .line 268
    if-nez p1, :cond_e

    .line 269
    .line 270
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    move-object p1, v5

    .line 274
    :cond_e
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 275
    .line 276
    const-string v0, "mSmudgeAnnotationBinding.tvRedo"

    .line 277
    .line 278
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    invoke-direct {p0, p1, v4}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 282
    .line 283
    .line 284
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 285
    .line 286
    if-nez p1, :cond_f

    .line 287
    .line 288
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 289
    .line 290
    .line 291
    move-object p1, v5

    .line 292
    :cond_f
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 293
    .line 294
    const-string v0, "mSmudgeAnnotationBinding.tvUndo"

    .line 295
    .line 296
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    .line 298
    .line 299
    invoke-direct {p0, p1, v4}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 300
    .line 301
    .line 302
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 303
    .line 304
    .line 305
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 306
    .line 307
    if-nez p1, :cond_10

    .line 308
    .line 309
    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 310
    .line 311
    .line 312
    goto :goto_2

    .line 313
    :cond_10
    move-object v5, p1

    .line 314
    :goto_2
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 315
    .line 316
    const v0, 0x7f1316c9

    .line 317
    .line 318
    .line 319
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 320
    .line 321
    .line 322
    move-result-object v0

    .line 323
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    .line 325
    .line 326
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 327
    .line 328
    const v0, 0x7f080c94

    .line 329
    .line 330
    .line 331
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 332
    .line 333
    .line 334
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 335
    .line 336
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 337
    .line 338
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->Oooo8o0〇()I

    .line 339
    .line 340
    .line 341
    move-result v1

    .line 342
    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 343
    .line 344
    .line 345
    iget-object p1, v5, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 346
    .line 347
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->OO0o〇〇()I

    .line 348
    .line 349
    .line 350
    move-result v0

    .line 351
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setCurrentSelectIndex(I)V

    .line 352
    .line 353
    .line 354
    goto :goto_4

    .line 355
    :cond_11
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 356
    .line 357
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->OO0o〇〇()V

    .line 358
    .line 359
    .line 360
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 361
    .line 362
    if-nez p1, :cond_12

    .line 363
    .line 364
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 365
    .line 366
    .line 367
    move-object p1, v5

    .line 368
    :cond_12
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 369
    .line 370
    .line 371
    move-result-object p1

    .line 372
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    .line 374
    .line 375
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 376
    .line 377
    .line 378
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 379
    .line 380
    .line 381
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 382
    .line 383
    if-nez p1, :cond_13

    .line 384
    .line 385
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 386
    .line 387
    .line 388
    move-object p1, v5

    .line 389
    :cond_13
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 390
    .line 391
    const-string v1, "mWaterMarkBarBinding.tvClearMark"

    .line 392
    .line 393
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 394
    .line 395
    .line 396
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 397
    .line 398
    .line 399
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 400
    .line 401
    if-nez p1, :cond_14

    .line 402
    .line 403
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 404
    .line 405
    .line 406
    goto :goto_3

    .line 407
    :cond_14
    move-object v5, p1

    .line 408
    :goto_3
    invoke-virtual {v5}, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080()Landroid/widget/LinearLayout;

    .line 409
    .line 410
    .line 411
    move-result-object p1

    .line 412
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 413
    .line 414
    .line 415
    invoke-static {p1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 416
    .line 417
    .line 418
    :goto_4
    return-void
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public static final synthetic O88(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oo0OOO(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lkotlinx/coroutines/Job;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08〇o0O:Lkotlinx/coroutines/Job;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O888Oo(Z)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08〇o0O:Lkotlinx/coroutines/Job;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-static {v0, v2, v1, v2}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 11
    .line 12
    iget-boolean v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO:Z

    .line 13
    .line 14
    new-instance v2, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "saveComplete: "

    .line 20
    .line 21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    const-string v3, " ,mOnShare:"

    .line 28
    .line 29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o0o8()Lcom/intsig/app/BaseProgressDialog;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 47
    .line 48
    .line 49
    if-nez p1, :cond_1

    .line 50
    .line 51
    return-void

    .line 52
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO:Z

    .line 53
    .line 54
    const-string v0, "mActivity"

    .line 55
    .line 56
    const/4 v1, 0x3

    .line 57
    if-eqz p1, :cond_3

    .line 58
    .line 59
    const/4 p1, 0x0

    .line 60
    iput-boolean p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO:Z

    .line 61
    .line 62
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇8o8o〇()Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_2

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 71
    .line 72
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 76
    .line 77
    .line 78
    move-result-wide v2

    .line 79
    invoke-static {p1, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->oo〇(Landroid/content/Context;JI)V

    .line 80
    .line 81
    .line 82
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 83
    .line 84
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 88
    .line 89
    .line 90
    move-result-wide v2

    .line 91
    invoke-static {p1, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇00〇8(Landroid/content/Context;JI)V

    .line 92
    .line 93
    .line 94
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo0〇〇O:Ljava/util/HashSet;

    .line 97
    .line 98
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->OOO〇O0(Ljava/util/HashSet;I)V

    .line 99
    .line 100
    .line 101
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 102
    .line 103
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 108
    .line 109
    .line 110
    move-result-wide v1

    .line 111
    const/4 v3, 0x3

    .line 112
    const/4 v4, 0x1

    .line 113
    const/4 v5, 0x0

    .line 114
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 115
    .line 116
    .line 117
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇o8〇〇O()V

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o〇88()V

    .line 122
    .line 123
    .line 124
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 125
    .line 126
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 130
    .line 131
    .line 132
    move-result-wide v2

    .line 133
    invoke-static {p1, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->oo〇(Landroid/content/Context;JI)V

    .line 134
    .line 135
    .line 136
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 137
    .line 138
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 142
    .line 143
    .line 144
    move-result-wide v2

    .line 145
    invoke-static {p1, v2, v3, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇00〇8(Landroid/content/Context;JI)V

    .line 146
    .line 147
    .line 148
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;

    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo0〇〇O:Ljava/util/HashSet;

    .line 151
    .line 152
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->OOO〇O0(Ljava/util/HashSet;I)V

    .line 153
    .line 154
    .line 155
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 156
    .line 157
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 162
    .line 163
    .line 164
    move-result-wide v1

    .line 165
    const/4 v3, 0x3

    .line 166
    const/4 v4, 0x1

    .line 167
    const/4 v5, 0x1

    .line 168
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 169
    .line 170
    .line 171
    :goto_0
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇O〇〇O8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    const/4 p2, 0x1

    .line 14
    invoke-static {p1, p2}, Lcom/intsig/camscanner/app/IntentUtil;->o〇0(Landroid/content/Context;Z)Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const-string v0, "has_max_count_limit"

    .line 19
    .line 20
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    const-string v0, "max_count"

    .line 24
    .line 25
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;

    .line 29
    .line 30
    invoke-virtual {p0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O8〇8〇O80(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0o0oO〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/camscanner/office_doc/data/EditMode;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 6
    .line 7
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O80OO(Lcom/intsig/camscanner/office_doc/data/EditMode;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0888(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static synthetic OO0o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇OoO0(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final OO0〇O(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-static {p2, v0, p0}, Lcom/intsig/camscanner/app/AppUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final OO8〇O8()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OOo00()Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇O〇〇O8:Z

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O8o08O8O:Ljava/lang/Boolean;

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo〇O0o〇()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OOo00()Ljava/lang/Boolean;
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v1, "default_add_signature"

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 20
    .line 21
    :goto_0
    return-object v0
.end method

.method private static final OO〇000(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇OOO〇〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final OO〇80oO〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x1

    .line 10
    const/4 v5, 0x4

    .line 11
    const/4 v6, 0x0

    .line 12
    move-object v0, p0

    .line 13
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo80:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo0O〇8800(Landroid/view/View;Z)V
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    const/high16 v0, 0x3f800000    # 1.0f

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const v0, 0x3e99999a    # 0.3f

    .line 7
    .line 8
    .line 9
    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OoO〇OOo8o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇8o0OOOo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇00O(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setColor(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0OO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Lcom/intsig/document/widget/DocumentView;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇0o8o8〇(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    float-to-int p1, p1

    .line 7
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->setStrokeSize(I)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0OO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇0o8〇()J
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-wide/16 v1, -0x1

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v3, "arg_doc_id"

    .line 10
    .line 11
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;J)J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    :cond_0
    return-wide v1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const-string v0, "extra_path"

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇〇o8O(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object p0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 24
    .line 25
    const-string p1, "create_signature_success"

    .line 26
    .line 27
    const-string v0, "scan_handwriting"

    .line 28
    .line 29
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
.end method

.method private final O〇8O0O80〇()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o8oO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇8〇008(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oO〇O0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇O800oo(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 7
    .line 8
    const-string v0, "share"

    .line 9
    .line 10
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOO8oo0()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇o8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "deleteCurSignature"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-virtual {v1, v0, v2}, Lcom/intsig/document/widget/DocumentView;->doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 18
    .line 19
    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 22
    .line 23
    const/4 v0, 0x2

    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08o(I)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O〇oo8O80(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 p1, 0x0

    .line 18
    :goto_0
    if-eqz p1, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-static {v0, p1, v1, v1}, Lcom/intsig/camscanner/signature/SignatureEditActivity;->o〇o08〇(Landroid/app/Activity;Landroid/net/Uri;FF)Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const-string v0, "extra_signature_filetype"

    .line 28
    .line 29
    iget v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 30
    .line 31
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO:Landroidx/activity/result/ActivityResultLauncher;

    .line 35
    .line 36
    invoke-virtual {p0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic O〇〇O80o8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇〇o8O(Ljava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-eqz p1, :cond_2

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    const/4 v1, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 18
    :goto_1
    if-eqz v1, :cond_3

    .line 19
    .line 20
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 21
    .line 22
    const-string v0, "path is empty"

    .line 23
    .line 24
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_3
    new-instance v1, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 29
    .line 30
    const/high16 v2, -0x1000000

    .line 31
    .line 32
    invoke-direct {v1, p1, v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;-><init>(Ljava/lang/String;I)V

    .line 33
    .line 34
    .line 35
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 36
    .line 37
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->Oooo8o0〇(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0OO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x3

    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08o(I)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇O800oo(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o088O8800()Lcom/intsig/document/widget/DocumentView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->o8〇OO0〇0o:Lcom/intsig/document/widget/DocumentView;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oooo800〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0OO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 10

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇O〇〇O8:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    const v0, 0x7f1318ec

    .line 8
    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 14
    .line 15
    const-string v0, "pdf not open, return"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    if-eqz p1, :cond_1

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    goto :goto_0

    .line 31
    :cond_1
    move-object v1, v0

    .line 32
    :goto_0
    const/4 v2, 0x1

    .line 33
    const/4 v3, 0x0

    .line 34
    if-eqz v1, :cond_d

    .line 35
    .line 36
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-nez v4, :cond_2

    .line 41
    .line 42
    goto/16 :goto_5

    .line 43
    .line 44
    :cond_2
    invoke-static {v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇0〇O0088o(Ljava/lang/String;)Lcom/intsig/camscanner/util/ParcelSize;

    .line 45
    .line 46
    .line 47
    move-result-object v4

    .line 48
    sget-object v5, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 49
    .line 50
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 51
    .line 52
    .line 53
    move-result v6

    .line 54
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 55
    .line 56
    .line 57
    move-result v7

    .line 58
    new-instance v8, Ljava/lang/StringBuilder;

    .line 59
    .line 60
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .line 62
    .line 63
    const-string v9, "addSignature bitmapSize.getWidth()="

    .line 64
    .line 65
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    const-string v6, " bitmapSize.getHeight()="

    .line 72
    .line 73
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 87
    .line 88
    .line 89
    move-result v6

    .line 90
    if-lez v6, :cond_c

    .line 91
    .line 92
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 93
    .line 94
    .line 95
    move-result v6

    .line 96
    if-gtz v6, :cond_3

    .line 97
    .line 98
    goto/16 :goto_4

    .line 99
    .line 100
    :cond_3
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getWidth()I

    .line 101
    .line 102
    .line 103
    move-result v6

    .line 104
    invoke-virtual {v4}, Lcom/intsig/camscanner/util/ParcelSize;->getHeight()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 109
    .line 110
    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    .line 111
    .line 112
    .line 113
    move-result-object v4

    .line 114
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇O〇0O0〇()Z

    .line 115
    .line 116
    .line 117
    move-result v6

    .line 118
    if-eqz v6, :cond_4

    .line 119
    .line 120
    invoke-static {v1, v4, v3, v3}, Lcom/intsig/nativelib/DraftEngine;->CleanImageKeepColor(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 121
    .line 122
    .line 123
    move-result v1

    .line 124
    goto :goto_1

    .line 125
    :cond_4
    invoke-static {v1, v4, v3, v3}, Lcom/intsig/nativelib/DraftEngine;->CleanImage(Ljava/lang/String;Landroid/graphics/Bitmap;II)I

    .line 126
    .line 127
    .line 128
    move-result v1

    .line 129
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 130
    .line 131
    .line 132
    move-result v6

    .line 133
    const/4 v7, -0x1

    .line 134
    if-le v1, v7, :cond_5

    .line 135
    .line 136
    if-eqz v6, :cond_5

    .line 137
    .line 138
    invoke-static {v1, v4, v6}, Lcom/intsig/nativelib/DraftEngine;->StrokeColor(ILandroid/graphics/Bitmap;I)V

    .line 139
    .line 140
    .line 141
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    if-le v1, v7, :cond_6

    .line 146
    .line 147
    if-lez p1, :cond_6

    .line 148
    .line 149
    if-eqz v6, :cond_6

    .line 150
    .line 151
    int-to-float p1, p1

    .line 152
    invoke-static {v1, v4, p1}, Lcom/intsig/nativelib/DraftEngine;->StrokeSize(ILandroid/graphics/Bitmap;F)V

    .line 153
    .line 154
    .line 155
    :cond_6
    if-le v1, v7, :cond_7

    .line 156
    .line 157
    new-instance p1, Ljava/lang/StringBuilder;

    .line 158
    .line 159
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .line 161
    .line 162
    const-string v6, "free = "

    .line 163
    .line 164
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    .line 166
    .line 167
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    .line 169
    .line 170
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    invoke-static {v1}, Lcom/intsig/nativelib/DraftEngine;->FreeContext(I)V

    .line 178
    .line 179
    .line 180
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooO:Lcom/intsig/document/widget/PagesView$ImageArgs;

    .line 181
    .line 182
    if-nez p1, :cond_8

    .line 183
    .line 184
    new-instance p1, Lcom/intsig/document/widget/PagesView$ImageArgs;

    .line 185
    .line 186
    invoke-direct {p1, v4}, Lcom/intsig/document/widget/PagesView$ImageArgs;-><init>(Landroid/graphics/Bitmap;)V

    .line 187
    .line 188
    .line 189
    iput-boolean v2, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->fixedRatio:Z

    .line 190
    .line 191
    iput-boolean v2, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->rotatable:Z

    .line 192
    .line 193
    const v1, 0x3e99999a    # 0.3f

    .line 194
    .line 195
    .line 196
    iput v1, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->initialSize:F

    .line 197
    .line 198
    iput-boolean v3, p1, Lcom/intsig/document/widget/PagesView$BaseArgs;->deleteable:Z

    .line 199
    .line 200
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooO:Lcom/intsig/document/widget/PagesView$ImageArgs;

    .line 201
    .line 202
    goto :goto_2

    .line 203
    :cond_8
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 204
    .line 205
    .line 206
    iput-object v4, p1, Lcom/intsig/document/widget/PagesView$ImageArgs;->bmp:Landroid/graphics/Bitmap;

    .line 207
    .line 208
    :goto_2
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InsertImage:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 209
    .line 210
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 211
    .line 212
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 213
    .line 214
    if-nez v1, :cond_a

    .line 215
    .line 216
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 217
    .line 218
    .line 219
    move-result-object v1

    .line 220
    if-eqz v1, :cond_9

    .line 221
    .line 222
    invoke-virtual {v1, p1, v2}, Lcom/intsig/document/widget/DocumentView;->beginInsertImage(Lcom/intsig/document/widget/PagesView$ImageArgs;Z)Lcom/intsig/document/widget/PagesView$ElementData;

    .line 223
    .line 224
    .line 225
    move-result-object v0

    .line 226
    :cond_9
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 227
    .line 228
    goto :goto_4

    .line 229
    :cond_a
    if-nez v1, :cond_b

    .line 230
    .line 231
    goto :goto_3

    .line 232
    :cond_b
    iput-object p1, v1, Lcom/intsig/document/widget/PagesView$ElementData;->args:Lcom/intsig/document/widget/PagesView$BaseArgs;

    .line 233
    .line 234
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 235
    .line 236
    .line 237
    move-result-object p1

    .line 238
    if-eqz p1, :cond_c

    .line 239
    .line 240
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 241
    .line 242
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->updateElement(Lcom/intsig/document/widget/PagesView$ElementData;)V

    .line 243
    .line 244
    .line 245
    :cond_c
    :goto_4
    return-void

    .line 246
    :cond_d
    :goto_5
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 247
    .line 248
    sget-object v0, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 249
    .line 250
    new-array v0, v2, [Ljava/lang/Object;

    .line 251
    .line 252
    aput-object v1, v0, v3

    .line 253
    .line 254
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    const-string v1, "%s : file is deleted"

    .line 259
    .line 260
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    const-string v1, "format(format, *args)"

    .line 265
    .line 266
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    return-void
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Lcom/intsig/document/widget/PagesView$WatermarkArgs;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0〇〇00(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Landroid/net/Uri;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇080OO8〇0:Landroid/net/Uri;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0〇〇00〇o()Lorg/json/JSONObject;
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "type"

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_0

    .line 13
    .line 14
    const-string v2, "premium"

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v2, "basic"

    .line 18
    .line 19
    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 20
    .line 21
    .line 22
    const-string v1, "login_status"

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    const-string v2, "logged_in"

    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    const-string v2, "no_logged_in"

    .line 36
    .line 37
    :goto_1
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    .line 39
    .line 40
    const-string v1, "from_part"

    .line 41
    .line 42
    const-string v2, "cs_pdf_preview"

    .line 43
    .line 44
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .line 46
    .line 47
    goto :goto_2

    .line 48
    :catch_0
    move-exception v1

    .line 49
    sget-object v2, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 50
    .line 51
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 52
    .line 53
    .line 54
    :goto_2
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o808o8o08()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o88(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o〇88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(ILcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o88o88(ILcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final o88o88(ILcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p2, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 7
    .line 8
    new-instance p3, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v0, "User Operation:  onclick continue ,lastSaveTime = "

    .line 14
    .line 15
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p3

    .line 25
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooo()V

    .line 29
    .line 30
    .line 31
    add-int/lit8 p0, p0, -0x1

    .line 32
    .line 33
    invoke-static {p0}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇00(I)V

    .line 34
    .line 35
    .line 36
    const-string p0, "CSFreeSignature"

    .line 37
    .line 38
    const-string p1, "continue"

    .line 39
    .line 40
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o88oo〇O()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oO8()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇〇808〇()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic o8O〇008(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p4, p3, 0x1

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    and-int/lit8 p3, p3, 0x2

    .line 7
    .line 8
    if-eqz p3, :cond_1

    .line 9
    .line 10
    const/4 p2, 0x0

    .line 11
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0o0(Ljava/lang/String;Z)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final o8o0()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "releasePdfView"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o088O8800()Lcom/intsig/document/widget/DocumentView;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->closeSearch()V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o088O8800()Lcom/intsig/document/widget/DocumentView;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->Close()V

    .line 24
    .line 25
    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o8o0o8()Lcom/intsig/app/BaseProgressDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o8oo0OOO(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "startAddSignatureFile menuId == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-eqz p1, :cond_2

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    if-eq p1, v1, :cond_1

    .line 27
    .line 28
    const/4 v1, 0x2

    .line 29
    if-eq p1, v1, :cond_0

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const-string p1, "User Operation:  hand write"

    .line 33
    .line 34
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    new-instance p1, Landroid/content/Intent;

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 40
    .line 41
    const-class v1, Lcom/intsig/camscanner/newsign/handwrite/HandWriteSignActivity;

    .line 42
    .line 43
    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Ooo08:Landroidx/activity/result/ActivityResultLauncher;

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const-string p1, "User Operation:  select from album"

    .line 53
    .line 54
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 58
    .line 59
    new-instance v0, L〇0oO〇oo00/〇80〇808〇O;

    .line 60
    .line 61
    invoke-direct {v0, p0}, L〇0oO〇oo00/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 62
    .line 63
    .line 64
    invoke-static {p1, v0}, Lcom/intsig/util/PermissionUtil;->〇8o8o〇(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    const-string p1, "User Operation:  take photo"

    .line 69
    .line 70
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 74
    .line 75
    new-instance v0, L〇0oO〇oo00/oO80;

    .line 76
    .line 77
    invoke-direct {v0, p0}, L〇0oO〇oo00/oO80;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 78
    .line 79
    .line 80
    invoke-static {p1, v0}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final oO8()Lcom/intsig/camscanner/util/PdfEncryptionUtil;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oO88〇0O8O(Ljava/io/File;Ljava/lang/String;ZZ)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "savePdf:  ,isShare:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iput-boolean p3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO:Z

    .line 24
    .line 25
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    const/4 v3, 0x0

    .line 30
    const/4 v4, 0x0

    .line 31
    new-instance p3, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$savePdf$1;

    .line 32
    .line 33
    const/4 v10, 0x0

    .line 34
    move-object v5, p3

    .line 35
    move v6, p4

    .line 36
    move-object v7, p0

    .line 37
    move-object v8, p1

    .line 38
    move-object v9, p2

    .line 39
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$savePdf$1;-><init>(ZLcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 40
    .line 41
    .line 42
    const/4 v6, 0x3

    .line 43
    const/4 v7, 0x0

    .line 44
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final oO8o〇08〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 7

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇80〇()V

    .line 7
    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    const/4 v4, 0x0

    .line 13
    const/16 v5, 0xc

    .line 14
    .line 15
    const/4 v6, 0x0

    .line 16
    move-object v0, p0

    .line 17
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oOO8oo0()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8O0O80〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 18
    .line 19
    const-string v1, "add anti Watermark"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0O:Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->〇O〇(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-eqz v1, :cond_1

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 37
    .line 38
    .line 39
    :cond_1
    if-eqz v0, :cond_2

    .line 40
    .line 41
    new-instance v1, Ljava/io/File;

    .line 42
    .line 43
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    const/4 v1, 0x0

    .line 48
    :goto_0
    move-object v3, v1

    .line 49
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇8o8o〇()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    const/4 v5, 0x0

    .line 58
    const/4 v6, 0x0

    .line 59
    const/4 v7, 0x1

    .line 60
    const/4 v8, 0x0

    .line 61
    const/16 v9, 0x8

    .line 62
    .line 63
    const/4 v10, 0x0

    .line 64
    move-object v4, p0

    .line 65
    invoke-static/range {v4 .. v10}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_3
    const/4 v4, 0x0

    .line 70
    const/4 v5, 0x1

    .line 71
    const/4 v6, 0x0

    .line 72
    const/16 v7, 0x8

    .line 73
    .line 74
    const/4 v8, 0x0

    .line 75
    move-object v2, p0

    .line 76
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V

    .line 77
    .line 78
    .line 79
    :cond_4
    :goto_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇80oO〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO〇O0O()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0〇8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO〇oo(Ljava/util/ArrayList;Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO08〇o0(Ljava/util/ArrayList;Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final oo0O(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-static {p2, v0, p0}, Lcom/intsig/camscanner/app/AppUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oo8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const-string v0, "extra_path"

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 21
    .line 22
    const-string v1, "create_signature_success"

    .line 23
    .line 24
    const-string v2, "album_import"

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇〇o8O(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
.end method

.method private final oo88()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o088O8800()Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    .line 4
    move-result-object v6

    .line 5
    if-eqz v6, :cond_0

    .line 6
    .line 7
    const v0, 0x7f0d0753

    .line 8
    .line 9
    .line 10
    invoke-virtual {v6, v0}, Lcom/intsig/document/widget/DocumentView;->setTextSelectionBar(I)Lcom/intsig/document/widget/DocumentView;

    .line 11
    .line 12
    .line 13
    const v0, 0x7f0d00f0

    .line 14
    .line 15
    .line 16
    invoke-virtual {v6, v0}, Lcom/intsig/document/widget/DocumentView;->setAnnotActionBar(I)Lcom/intsig/document/widget/DocumentView;

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    invoke-virtual {v6, v0}, Lcom/intsig/document/widget/DocumentView;->build(Landroid/content/Context;)V

    .line 22
    .line 23
    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    .line 25
    .line 26
    const/high16 v1, 0x40400000    # 3.0f

    .line 27
    .line 28
    invoke-virtual {v6, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setScaleRange(FF)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    const v1, 0x7f0601ee

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    sget-object v7, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 41
    .line 42
    invoke-virtual {v7}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const/4 v2, 0x2

    .line 47
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    int-to-float v2, v0

    .line 52
    const/4 v3, 0x0

    .line 53
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 54
    .line 55
    const v4, 0x7f080ba9

    .line 56
    .line 57
    .line 58
    invoke-static {v0, v4}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    const/4 v5, 0x0

    .line 63
    move-object v0, v6

    .line 64
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/document/widget/DocumentView;->setAnnotBoxStyle(IF[FLandroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 65
    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 68
    .line 69
    const v1, 0x7f080bda

    .line 70
    .line 71
    .line 72
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 77
    .line 78
    const v2, 0x7f060207

    .line 79
    .line 80
    .line 81
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    invoke-virtual {v6, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V

    .line 86
    .line 87
    .line 88
    const v0, 0x7f0601e5

    .line 89
    .line 90
    .line 91
    const v1, 0x3e4ccccd    # 0.2f

    .line 92
    .line 93
    .line 94
    invoke-static {v0, v1}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    const/4 v1, 0x3

    .line 99
    invoke-virtual {v7}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 100
    .line 101
    .line 102
    move-result-object v2

    .line 103
    invoke-static {v2, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    const v2, 0x66aaaaaa

    .line 108
    .line 109
    .line 110
    invoke-virtual {v6, v2, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setSideBarStyle(III)V

    .line 111
    .line 112
    .line 113
    const/4 v0, 0x0

    .line 114
    invoke-virtual {v6, v0}, Lcom/intsig/document/widget/DocumentView;->setPageBorderSize(I)V

    .line 115
    .line 116
    .line 117
    new-instance v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$loadPdfView$1$1;

    .line 118
    .line 119
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$loadPdfView$1$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v6, v0}, Lcom/intsig/document/widget/DocumentView;->SetActionListener(Lcom/intsig/document/widget/DocumentView$DocumentActionListener;)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo〇8o008:Ljava/lang/String;

    .line 126
    .line 127
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 128
    .line 129
    .line 130
    move-result v0

    .line 131
    if-eqz v0, :cond_0

    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo〇8o008:Ljava/lang/String;

    .line 134
    .line 135
    const/4 v1, 0x0

    .line 136
    invoke-virtual {v6, v0, v1}, Lcom/intsig/document/widget/DocumentView;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    :cond_0
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final oo8〇〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final ooo008(Ljava/lang/String;)V
    .locals 3

    .line 1
    const v0, 0x7f131d10

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v1, "getString(R.string.dlg_title)"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    new-instance v1, L〇0oO〇oo00/o〇0;

    .line 31
    .line 32
    invoke-direct {v1, p1}, L〇0oO〇oo00/o〇0;-><init>(Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    const p1, 0x7f1313c1

    .line 36
    .line 37
    .line 38
    const/4 v2, 0x0

    .line 39
    invoke-virtual {v0, p1, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇〇0〇(IZLandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const v0, 0x7f13057e

    .line 44
    .line 45
    .line 46
    const/4 v1, 0x0

    .line 47
    invoke-virtual {p1, v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p1, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0o〇o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final oooO8〇00()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8oo0oO0()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 5
    .line 6
    if-eqz v0, :cond_f

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const v2, 0x7f0a0b25

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const v3, 0x7f0a0d1e

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    const v4, 0x7f0a03e7

    .line 35
    .line 36
    .line 37
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    const v5, 0x7f0a0cec

    .line 46
    .line 47
    .line 48
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    invoke-static {v1}, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-string v5, "bind(bottomBar)"

    .line 57
    .line 58
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 62
    .line 63
    invoke-static {v2}, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    const-string v2, "bind(waterMarkView)"

    .line 68
    .line 69
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 73
    .line 74
    invoke-static {v3}, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    const-string v2, "bind(annotationStyle)"

    .line 79
    .line 80
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 84
    .line 85
    const-string v2, "mAnnotationStyleBinding"

    .line 86
    .line 87
    const/4 v3, 0x0

    .line 88
    if-nez v1, :cond_0

    .line 89
    .line 90
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    move-object v1, v3

    .line 94
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    const-string v5, "mAnnotationStyleBinding.root"

    .line 99
    .line 100
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 104
    .line 105
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    const/16 v6, 0xc

    .line 110
    .line 111
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 112
    .line 113
    .line 114
    move-result v5

    .line 115
    int-to-float v5, v5

    .line 116
    invoke-static {v1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O888o0o(Landroid/view/View;F)V

    .line 117
    .line 118
    .line 119
    invoke-static {v4}, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    const-string v4, "bind(smudgeAnnotation)"

    .line 124
    .line 125
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 129
    .line 130
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 131
    .line 132
    const-string v4, "mBottomBarBinding"

    .line 133
    .line 134
    if-nez v1, :cond_1

    .line 135
    .line 136
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    move-object v1, v3

    .line 140
    :cond_1
    const/16 v5, 0xd

    .line 141
    .line 142
    new-array v5, v5, [Landroid/view/View;

    .line 143
    .line 144
    iget-object v7, v1, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 145
    .line 146
    const/4 v8, 0x0

    .line 147
    aput-object v7, v5, v8

    .line 148
    .line 149
    iget-object v7, v1, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 150
    .line 151
    const/4 v9, 0x1

    .line 152
    aput-object v7, v5, v9

    .line 153
    .line 154
    const/4 v7, 0x2

    .line 155
    iget-object v10, v1, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 156
    .line 157
    aput-object v10, v5, v7

    .line 158
    .line 159
    const/4 v7, 0x3

    .line 160
    iget-object v10, v1, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 161
    .line 162
    aput-object v10, v5, v7

    .line 163
    .line 164
    const/4 v7, 0x4

    .line 165
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 166
    .line 167
    aput-object v1, v5, v7

    .line 168
    .line 169
    const/4 v1, 0x5

    .line 170
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 171
    .line 172
    aput-object v0, v5, v1

    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 175
    .line 176
    const-string v1, "mWaterMarkBarBinding"

    .line 177
    .line 178
    if-nez v0, :cond_2

    .line 179
    .line 180
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    move-object v0, v3

    .line 184
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 185
    .line 186
    const/4 v7, 0x6

    .line 187
    aput-object v0, v5, v7

    .line 188
    .line 189
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 190
    .line 191
    if-nez v0, :cond_3

    .line 192
    .line 193
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 194
    .line 195
    .line 196
    move-object v0, v3

    .line 197
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 198
    .line 199
    const/4 v7, 0x7

    .line 200
    aput-object v0, v5, v7

    .line 201
    .line 202
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 203
    .line 204
    if-nez v0, :cond_4

    .line 205
    .line 206
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 207
    .line 208
    .line 209
    move-object v0, v3

    .line 210
    :cond_4
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->OO:Landroid/widget/LinearLayout;

    .line 211
    .line 212
    const/16 v1, 0x8

    .line 213
    .line 214
    aput-object v0, v5, v1

    .line 215
    .line 216
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 217
    .line 218
    const-string v1, "mSmudgeAnnotationBinding"

    .line 219
    .line 220
    if-nez v0, :cond_5

    .line 221
    .line 222
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    move-object v0, v3

    .line 226
    :cond_5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇08O〇00〇o:Landroid/widget/LinearLayout;

    .line 227
    .line 228
    const/16 v7, 0x9

    .line 229
    .line 230
    aput-object v0, v5, v7

    .line 231
    .line 232
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 233
    .line 234
    if-nez v0, :cond_6

    .line 235
    .line 236
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    move-object v0, v3

    .line 240
    :cond_6
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 241
    .line 242
    const/16 v7, 0xa

    .line 243
    .line 244
    aput-object v0, v5, v7

    .line 245
    .line 246
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 247
    .line 248
    if-nez v0, :cond_7

    .line 249
    .line 250
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 251
    .line 252
    .line 253
    move-object v0, v3

    .line 254
    :cond_7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 255
    .line 256
    const/16 v7, 0xb

    .line 257
    .line 258
    aput-object v0, v5, v7

    .line 259
    .line 260
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 261
    .line 262
    if-nez v0, :cond_8

    .line 263
    .line 264
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    move-object v0, v3

    .line 268
    :cond_8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 269
    .line 270
    aput-object v0, v5, v6

    .line 271
    .line 272
    invoke-virtual {p0, v5}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 273
    .line 274
    .line 275
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 276
    .line 277
    if-nez v0, :cond_9

    .line 278
    .line 279
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    move-object v0, v3

    .line 283
    :cond_9
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 284
    .line 285
    invoke-virtual {v0, v8}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setSignatureColors(Z)V

    .line 286
    .line 287
    .line 288
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 289
    .line 290
    if-nez v0, :cond_a

    .line 291
    .line 292
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 293
    .line 294
    .line 295
    move-object v0, v3

    .line 296
    :cond_a
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 297
    .line 298
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOO〇〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$onSeekBarChangeListener$1;

    .line 299
    .line 300
    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 301
    .line 302
    .line 303
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 304
    .line 305
    if-nez v0, :cond_b

    .line 306
    .line 307
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 308
    .line 309
    .line 310
    move-object v0, v3

    .line 311
    :cond_b
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 312
    .line 313
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O88O:Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;

    .line 314
    .line 315
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ColorPickerView$OnColorSelectedListener;)V

    .line 316
    .line 317
    .line 318
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 319
    .line 320
    if-nez v0, :cond_c

    .line 321
    .line 322
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    move-object v0, v3

    .line 326
    :cond_c
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 327
    .line 328
    invoke-virtual {v0, v9}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 329
    .line 330
    .line 331
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 332
    .line 333
    if-nez v0, :cond_d

    .line 334
    .line 335
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 336
    .line 337
    .line 338
    move-object v0, v3

    .line 339
    :cond_d
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 340
    .line 341
    invoke-virtual {v0, v9}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 342
    .line 343
    .line 344
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;

    .line 345
    .line 346
    if-nez v0, :cond_e

    .line 347
    .line 348
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 349
    .line 350
    .line 351
    goto :goto_0

    .line 352
    :cond_e
    move-object v3, v0

    .line 353
    :goto_0
    iget-object v0, v3, Lcom/intsig/camscanner/databinding/PdfEditBottomBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 354
    .line 355
    invoke-virtual {v0, v9}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 356
    .line 357
    .line 358
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇0O〇()V

    .line 359
    .line 360
    .line 361
    :cond_f
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo88()V

    .line 362
    .line 363
    .line 364
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇80O()V

    .line 365
    .line 366
    .line 367
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O088O()V

    .line 368
    .line 369
    .line 370
    return-void
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final oooo800〇〇()V
    .locals 4

    .line 1
    const v0, 0x7f1316d9

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const-string v1, "getString(R.string.cs_640_file_conflict_intro)"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 18
    .line 19
    .line 20
    const v2, 0x7f1302e9

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const v1, 0x7f1316d8

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    new-instance v2, L〇0oO〇oo00/OO0o〇〇〇〇0;

    .line 43
    .line 44
    invoke-direct {v2, p0}, L〇0oO〇oo00/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 45
    .line 46
    .line 47
    const v3, 0x7f060207

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v1, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O888o0o(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 51
    .line 52
    .line 53
    const v1, 0x7f1316d7

    .line 54
    .line 55
    .line 56
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    new-instance v2, L〇0oO〇oo00/〇8o8o〇;

    .line 61
    .line 62
    invoke-direct {v2, p0}, L〇0oO〇oo00/〇8o8o〇;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final ooooo0O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    const-string v0, "extra_path"

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 21
    .line 22
    const-string v1, "create_signature_success"

    .line 23
    .line 24
    const-string v2, "scan_handwriting"

    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oo08(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇〇o8O(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
.end method

.method private final oo〇O0o〇()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->openNewESign()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 12
    .line 13
    const-string v1, "copy to use new esign"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/util/PdfUtils;->〇080:Lcom/intsig/camscanner/util/PdfUtils;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    const-string v2, "mActivity"

    .line 23
    .line 24
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 28
    .line 29
    .line 30
    move-result-wide v2

    .line 31
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    new-instance v3, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$startSignature$1;

    .line 36
    .line 37
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$startSignature$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/util/PdfUtils;->Oo08(Landroid/content/Context;Ljava/lang/Long;Lkotlin/jvm/functions/Function0;)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 45
    .line 46
    const-string v1, "use jesse pdfSign"

    .line 47
    .line 48
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 52
    .line 53
    const-string v1, "add_signature"

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const/4 v0, 0x2

    .line 59
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08o(I)V

    .line 60
    .line 61
    .line 62
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o8〇8(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo8〇〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇O8OO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇〇8〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇OoO0(Ljava/lang/String;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/dialog/AddTextAnnotationDialog;->OO:Lcom/intsig/camscanner/office_doc/dialog/AddTextAnnotationDialog$Companion;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$addTextAnnotation$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$addTextAnnotation$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 9
    .line 10
    .line 11
    move-result-object v2

    .line 12
    const-string v3, "childFragmentManager"

    .line 13
    .line 14
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/office_doc/dialog/AddTextAnnotationDialog$Companion;->〇080(Ljava/lang/String;Lcom/intsig/camscanner/office_doc/dialog/AddTextAnnotationDialog$Callback;Landroidx/fragment/app/FragmentManager;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O888Oo(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇o0oOO8(Landroid/view/View;)V
    .locals 1

    .line 1
    sget-object p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "do nothing"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇o8〇〇O()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-gtz v4, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "begin sharePdf"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇8o8o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    const/4 v1, 0x1

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 29
    .line 30
    new-array v1, v1, [Ljava/lang/Long;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 33
    .line 34
    .line 35
    move-result-wide v2

    .line 36
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    const/4 v3, 0x0

    .line 41
    aput-object v2, v1, v3

    .line 42
    .line 43
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    new-instance v2, L〇0oO〇oo00/OO0o〇〇;

    .line 48
    .line 49
    invoke-direct {v2, p0}, L〇0oO〇oo00/OO0o〇〇;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/util/PdfUtils;->〇080:Lcom/intsig/camscanner/util/PdfUtils;

    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 59
    .line 60
    const-string v3, "mActivity"

    .line 61
    .line 62
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 66
    .line 67
    .line 68
    move-result-wide v3

    .line 69
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/camscanner/util/PdfUtils;->〇00(Landroidx/fragment/app/FragmentActivity;J)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 74
    .line 75
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/share/type/SharePdf;->〇〇o0o(Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/SharePdf;->o〇(Z)V

    .line 79
    .line 80
    .line 81
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8oo8888()Lcom/intsig/camscanner/share/ShareHelper;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 86
    .line 87
    .line 88
    :goto_0
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final o〇oO08〇o0(Ljava/util/ArrayList;Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "$menuItems"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "this$0"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p0, p3}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    check-cast p0, Lcom/intsig/menu/MenuItem;

    .line 16
    .line 17
    if-eqz p0, :cond_0

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/menu/MenuItem;->〇〇888()I

    .line 20
    .line 21
    .line 22
    move-result p0

    .line 23
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oo0OOO(I)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic o〇oo(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0o0(Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇00o〇O8()V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 8
    .line 9
    .line 10
    move-result-wide v1

    .line 11
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeDbUtil;->〇O8o08O(Landroid/content/Context;J)Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object v2, v1

    .line 24
    :goto_0
    iput-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo〇8o008:Ljava/lang/String;

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇O888o0o()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    move-object v0, v1

    .line 34
    :goto_1
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0O:Ljava/lang/String;

    .line 35
    .line 36
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 39
    .line 40
    .line 41
    move-result-wide v2

    .line 42
    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇080OO8〇0:Landroid/net/Uri;

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 51
    .line 52
    .line 53
    move-result-wide v2

    .line 54
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇08O:Ljava/util/ArrayList;

    .line 59
    .line 60
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    const/4 v4, 0x0

    .line 69
    new-instance v5, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initData$1;

    .line 70
    .line 71
    invoke-direct {v5, p0, v1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initData$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lkotlin/coroutines/Continuation;)V

    .line 72
    .line 73
    .line 74
    const/4 v6, 0x2

    .line 75
    const/4 v7, 0x0

    .line 76
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇00〇〇〇o〇8()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f1319e8

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, L〇0oO〇oo00/〇080;

    .line 20
    .line 21
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇080;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 22
    .line 23
    .line 24
    const v2, 0x7f1312ff

    .line 25
    .line 26
    .line 27
    const v3, 0x7f060207

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0〇O0088o(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const v1, 0x7f1301df

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    new-instance v2, L〇0oO〇oo00/〇O8o08O;

    .line 42
    .line 43
    invoke-direct {v2, p0}, L〇0oO〇oo00/〇O8o08O;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 44
    .line 45
    .line 46
    const v3, 0x7f0601f2

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, v1, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->OOO〇O0(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0888(II)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "onTabChanged, last: "

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v1, ", position: "

    .line 19
    .line 20
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string p1, ", type: "

    .line 27
    .line 28
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iput p2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇〇8o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇08O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0OO(Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0O8Oo()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const v0, 0x7f1313d5

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const v0, 0x7f1313d4

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    goto :goto_0

    .line 29
    :cond_2
    const v0, 0x7f1313d3

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    :goto_0
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0o0oO〇〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o〇OO80oO()V

    .line 8
    .line 9
    .line 10
    goto :goto_2

    .line 11
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->TextAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 12
    .line 13
    if-ne v0, v1, :cond_3

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 v0, 0x0

    .line 28
    goto :goto_1

    .line 29
    :cond_2
    :goto_0
    const/4 v0, 0x1

    .line 30
    :goto_1
    if-nez v0, :cond_3

    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 35
    .line 36
    .line 37
    const/4 v2, 0x2

    .line 38
    const/4 v3, 0x0

    .line 39
    invoke-static {p0, v0, v1, v2, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8O〇008(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    :cond_3
    :goto_2
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0o88Oo〇()V
    .locals 9

    .line 1
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v3, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8〇()J

    .line 8
    .line 9
    .line 10
    move-result-wide v1

    .line 11
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oOo0:Ljava/lang/String;

    .line 12
    .line 13
    const/4 v5, 0x0

    .line 14
    new-instance v6, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$addAntiCounterfeit$1$1;

    .line 15
    .line 16
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$addAntiCounterfeit$1$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 20
    .line 21
    .line 22
    move-result-object v7

    .line 23
    const-string v8, "childFragmentManager"

    .line 24
    .line 25
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;->〇o00〇〇Oo(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;Landroidx/fragment/app/FragmentManager;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooooo0O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8o0o0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇0o〇o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p3, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "dialog"

    .line 7
    .line 8
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-static {p0, p1}, Lcom/intsig/camscanner/app/IntentUtil;->o800o8O(Landroid/content/Context;Ljava/lang/String;)Z

    .line 14
    .line 15
    .line 16
    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O8〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇0〇o8〇(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const-string v0, "mAnnotationStyleBinding"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v2, "mAnnotationStyleBinding.root"

    .line 17
    .line 18
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 22
    .line 23
    .line 24
    const v0, 0x7f0601ee

    .line 25
    .line 26
    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    const v2, 0x7f0601ee

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const v2, 0x7f060206

    .line 34
    .line 35
    .line 36
    :goto_0
    if-eqz p1, :cond_2

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_2
    const v0, 0x7f060208

    .line 40
    .line 41
    .line 42
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 43
    .line 44
    sget-object v3, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 45
    .line 46
    if-ne p1, v3, :cond_5

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 49
    .line 50
    const-string v3, "mSmudgeAnnotationBinding"

    .line 51
    .line 52
    if-nez p1, :cond_3

    .line 53
    .line 54
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    move-object p1, v1

    .line 58
    :cond_3
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 59
    .line 60
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    invoke-static {v4, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setImagIconColor(I)V

    .line 67
    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 70
    .line 71
    if-nez p1, :cond_4

    .line 72
    .line 73
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    goto :goto_2

    .line 77
    :cond_4
    move-object v1, p1

    .line 78
    :goto_2
    iget-object p1, v1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 81
    .line 82
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 87
    .line 88
    .line 89
    goto :goto_4

    .line 90
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 91
    .line 92
    const-string v3, "mWaterMarkBarBinding"

    .line 93
    .line 94
    if-nez p1, :cond_6

    .line 95
    .line 96
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    move-object p1, v1

    .line 100
    :cond_6
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 101
    .line 102
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 103
    .line 104
    invoke-static {v4, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setImagIconColor(I)V

    .line 109
    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 112
    .line 113
    if-nez p1, :cond_7

    .line 114
    .line 115
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_7
    move-object v1, p1

    .line 120
    :goto_3
    iget-object p1, v1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 123
    .line 124
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 129
    .line 130
    .line 131
    :goto_4
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇80O()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 7
    .line 8
    const-string v1, "binding.signatureTabView"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇o〇:Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$mISignatureEditView$1;

    .line 14
    .line 15
    new-instance v3, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$1;

    .line 16
    .line 17
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$1;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 18
    .line 19
    .line 20
    new-instance v4, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$2;

    .line 21
    .line 22
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$2;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 23
    .line 24
    .line 25
    new-instance v5, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$3;

    .line 26
    .line 27
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$3;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 28
    .line 29
    .line 30
    new-instance v6, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$4;

    .line 31
    .line 32
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$4;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 33
    .line 34
    .line 35
    new-instance v7, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$5;

    .line 36
    .line 37
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$5;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 38
    .line 39
    .line 40
    const-string v8, "cs_list_pdf"

    .line 41
    .line 42
    new-instance v9, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$6;

    .line 43
    .line 44
    invoke-direct {v9, p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment$initSignatureTabView$6;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 45
    .line 46
    .line 47
    move-object v1, v0

    .line 48
    invoke-virtual/range {v1 .. v9}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->OoO8(Lcom/intsig/camscanner/pdf/signature/tab/ISignatureEditView;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 49
    .line 50
    .line 51
    const/4 v1, 0x2

    .line 52
    const/4 v2, 0x0

    .line 53
    const/4 v3, 0x0

    .line 54
    invoke-static {v0, v3, v3, v1, v2}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->Oo8Oo00oo(Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;IIILjava/lang/Object;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇80〇()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "confirmAddSignatureOnPdf"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇08O:Ljava/util/ArrayList;

    .line 16
    .line 17
    if-nez v2, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    sget-object v3, Lcom/intsig/camscanner/office_doc/data/EditMode;->None:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 21
    .line 22
    iput-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8oOOo:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 25
    .line 26
    const/4 v4, 0x1

    .line 27
    if-eqz v3, :cond_2

    .line 28
    .line 29
    invoke-virtual {v1, v3, v4}, Lcom/intsig/document/widget/DocumentView;->doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 30
    .line 31
    .line 32
    move-result v4

    .line 33
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v3, "saveSignatureOnPdf pageNum == "

    .line 39
    .line 40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    if-ltz v4, :cond_6

    .line 54
    .line 55
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    if-gt v0, v4, :cond_3

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo0〇〇O:Ljava/util/HashSet;

    .line 63
    .line 64
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 72
    .line 73
    const/4 v1, 0x0

    .line 74
    if-eqz v0, :cond_4

    .line 75
    .line 76
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_4
    move-object v0, v1

    .line 80
    :goto_0
    if-eqz v0, :cond_5

    .line 81
    .line 82
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 83
    .line 84
    if-eqz v2, :cond_5

    .line 85
    .line 86
    invoke-virtual {v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getPath()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    invoke-virtual {v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getColor()I

    .line 91
    .line 92
    .line 93
    move-result v4

    .line 94
    invoke-virtual {v2}, Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;->getStrokeSize()I

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    invoke-virtual {v0, v3, v4, v2}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->〇o(Ljava/lang/String;II)V

    .line 99
    .line 100
    .line 101
    :cond_5
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO〇00〇0O:Lcom/intsig/camscanner/signature/SignatureAdapter$SignaturePath;

    .line 102
    .line 103
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooO:Lcom/intsig/document/widget/PagesView$ImageArgs;

    .line 104
    .line 105
    :cond_6
    :goto_1
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇8O(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "saveAnnotation pageIndex: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇08O:Ljava/util/ArrayList;

    .line 24
    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-lt p1, v1, :cond_1

    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo0〇〇O:Ljava/util/HashSet;

    .line 36
    .line 37
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇8O0880(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O00OoO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8o0o0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0o0oO〇〇0()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8oo0oO0()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v1, 0x3

    .line 9
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setTitleTextStyle(I)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0O:Ljava/lang/String;

    .line 13
    .line 14
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    new-instance v1, Landroid/widget/TextView;

    .line 18
    .line 19
    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    const v2, 0x7f081188

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 26
    .line 27
    .line 28
    const v2, 0x7f060204

    .line 29
    .line 30
    .line 31
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 36
    .line 37
    .line 38
    const v0, 0x7f130b4d

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 42
    .line 43
    .line 44
    const/high16 v0, 0x41600000    # 14.0f

    .line 45
    .line 46
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 47
    .line 48
    .line 49
    const/4 v0, 0x2

    .line 50
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 51
    .line 52
    .line 53
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 54
    .line 55
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 56
    .line 57
    .line 58
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    .line 59
    .line 60
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 61
    .line 62
    .line 63
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    const/16 v3, 0xc

    .line 70
    .line 71
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    const/4 v5, 0x6

    .line 80
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 85
    .line 86
    .line 87
    move-result-object v6

    .line 88
    invoke-static {v6, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 93
    .line 94
    .line 95
    move-result-object v6

    .line 96
    invoke-static {v6, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 97
    .line 98
    .line 99
    move-result v5

    .line 100
    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 101
    .line 102
    .line 103
    new-instance v2, L〇0oO〇oo00/〇〇888;

    .line 104
    .line 105
    invoke-direct {v2, p0}, L〇0oO〇oo00/〇〇888;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarMenu(Landroid/view/View;)V

    .line 112
    .line 113
    .line 114
    const/4 v3, 0x0

    .line 115
    const/4 v4, 0x0

    .line 116
    const/4 v5, 0x0

    .line 117
    const/4 v6, 0x0

    .line 118
    const/16 v7, 0xf

    .line 119
    .line 120
    const/4 v8, 0x0

    .line 121
    move-object v2, v1

    .line 122
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 126
    .line 127
    .line 128
    move-result-object v2

    .line 129
    instance-of v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 130
    .line 131
    if-eqz v3, :cond_1

    .line 132
    .line 133
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_1
    const/4 v2, 0x0

    .line 137
    :goto_0
    if-eqz v2, :cond_2

    .line 138
    .line 139
    const/16 v3, 0x20

    .line 140
    .line 141
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 142
    .line 143
    .line 144
    move-result-object v4

    .line 145
    invoke-static {v4, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 146
    .line 147
    .line 148
    move-result v3

    .line 149
    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 150
    .line 151
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 152
    .line 153
    .line 154
    move-result-object v3

    .line 155
    const/16 v4, 0x10

    .line 156
    .line 157
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 158
    .line 159
    .line 160
    move-result v3

    .line 161
    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 162
    .line 163
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    invoke-static {v0, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 172
    .line 173
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 174
    .line 175
    .line 176
    :cond_2
    return-void
    .line 177
.end method

.method private final 〇8oo8888()Lcom/intsig/camscanner/share/ShareHelper;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic 〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V
    .locals 1

    .line 1
    and-int/lit8 p6, p5, 0x4

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    if-eqz p6, :cond_0

    .line 5
    .line 6
    const/4 p3, 0x0

    .line 7
    :cond_0
    and-int/lit8 p5, p5, 0x8

    .line 8
    .line 9
    if-eqz p5, :cond_1

    .line 10
    .line 11
    const/4 p4, 0x0

    .line 12
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oO88〇0O8O(Ljava/io/File;Ljava/lang/String;ZZ)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
.end method

.method private final 〇8ooo()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 2
    .line 3
    const-string v1, "save"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x2

    .line 7
    invoke-static {v0, v1, v2, v3, v2}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->o〇0(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇80〇()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08o(I)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇8oo〇〇oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇00O(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8〇0O〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 6
    .line 7
    const-string v2, "llRemoveImageWaterMark"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8O0O80〇()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    const/4 v3, 0x1

    .line 17
    xor-int/2addr v2, v3

    .line 18
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8O0O80〇()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    const/4 v1, 0x0

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/document/widget/DocumentView;->doneImageWatermark(Z)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    const v1, 0x7f0601de

    .line 39
    .line 40
    .line 41
    const v2, 0x3e0f5c29    # 0.14f

    .line 42
    .line 43
    .line 44
    invoke-static {v1, v2}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇0O:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 49
    .line 50
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 51
    .line 52
    .line 53
    const v1, 0x7f131711

    .line 54
    .line 55
    .line 56
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    const-string v2, "getString(R.string.cs_64\u2026ve_watermark_prompt_bold)"

    .line 61
    .line 62
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    const v2, 0x7f131710

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 88
    .line 89
    const-string v5, "mActivity"

    .line 90
    .line 91
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const v5, 0x7f060208

    .line 95
    .line 96
    .line 97
    invoke-static {v4, v5}, Lcom/intsig/utils/ext/ContextExtKt;->Oo08(Landroid/content/Context;I)I

    .line 98
    .line 99
    .line 100
    move-result v4

    .line 101
    iget-object v5, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇〇08O:Landroid/widget/TextView;

    .line 102
    .line 103
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    .line 105
    .line 106
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇〇08O:Landroid/widget/TextView;

    .line 107
    .line 108
    invoke-static {v2, v1, v4, v3}, Lcom/intsig/utils/ext/StringExtKt;->〇o〇(Ljava/lang/String;Ljava/lang/String;II)Landroid/text/SpannableString;

    .line 109
    .line 110
    .line 111
    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    .line 114
    .line 115
    :cond_1
    :goto_0
    return-void
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8oo〇〇oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8〇OOoooo(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇o0oOO8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8〇o〇OoO8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Landroid/content/Intent;

    .line 7
    .line 8
    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string p2, "need_re_download_pdf"

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    iget-object p2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    const/4 v0, -0x1

    .line 22
    invoke-virtual {p2, v0, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    if-eqz p0, :cond_1

    .line 28
    .line 29
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 30
    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇8〇〇8o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 7
    .line 8
    const-string p2, "User Operation:  onclick upgrade now"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string p1, "CSFreeSignature"

    .line 14
    .line 15
    const-string p2, "upgrade_now"

    .line 16
    .line 17
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    invoke-direct {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 23
    .line 24
    .line 25
    sget-object p2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    sget-object p2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 32
    .line 33
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const/16 p2, 0x1f4

    .line 38
    .line 39
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇8〇〇8〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;[Ljava/lang/String;Z)V
    .locals 1

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->Oooo8o0〇(Landroid/content/Context;)Landroid/content/Intent;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const-string p2, "tipstext"

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0O8Oo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    const-string p2, "extra_signature_filetype"

    .line 27
    .line 28
    iget v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇o88o08〇:I

    .line 29
    .line 30
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 31
    .line 32
    .line 33
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇OO8ooO8〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 34
    .line 35
    invoke-virtual {p0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇OoO0(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8〇8000(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O008oO0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o0o8()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->ooo008(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇Oo〇O(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;F)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇0o8o8〇(F)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇000(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o8〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onDeleteSignature path == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇oOO80o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oO〇08o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Lcom/intsig/camscanner/office_doc/data/EditMode;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/view/View;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇oO〇〇8o:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇88()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o0o8()Lcom/intsig/app/BaseProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 13
    .line 14
    .line 15
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇oO:Z

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    return-void

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    const/4 v1, -0x1

    .line 23
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇oo8O80(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇OO80oO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mAnnotationStyleBinding"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->getCurrentColor()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 19
    .line 20
    if-nez v3, :cond_1

    .line 21
    .line 22
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    move-object v1, v3

    .line 27
    :goto_0
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 28
    .line 29
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    int-to-float v1, v1

    .line 34
    const/high16 v2, 0x3f800000    # 1.0f

    .line 35
    .line 36
    mul-float v1, v1, v2

    .line 37
    .line 38
    const/4 v2, 0x1

    .line 39
    int-to-float v2, v2

    .line 40
    add-float/2addr v1, v2

    .line 41
    new-instance v2, Lcom/intsig/document/widget/PagesView$InkArgs;

    .line 42
    .line 43
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 44
    .line 45
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    invoke-static {v3, v1}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    float-to-int v1, v1

    .line 54
    int-to-float v1, v1

    .line 55
    invoke-direct {v2, v0, v1}, Lcom/intsig/document/widget/PagesView$InkArgs;-><init>(IF)V

    .line 56
    .line 57
    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    .line 64
    invoke-virtual {v0, v2}, Lcom/intsig/document/widget/DocumentView;->beginInkAnnot(Lcom/intsig/document/widget/PagesView$InkArgs;)V

    .line 65
    .line 66
    .line 67
    :cond_2
    return-void
    .line 68
.end method

.method public static final synthetic 〇〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0(Z)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 5
    .line 6
    .line 7
    move-result-object v1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/document/widget/DocumentView;->getAnnotBox()Lcom/intsig/document/widget/PagesView$AnnotBox;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v1, v0

    .line 16
    :goto_0
    if-nez v1, :cond_1

    .line 17
    .line 18
    sget-object v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 19
    .line 20
    const-string v2, "annoBox == null"

    .line 21
    .line 22
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    iget v1, v1, Lcom/intsig/document/widget/PagesView$AnnotBox;->pageNum:I

    .line 27
    .line 28
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8O(I)V

    .line 29
    .line 30
    .line 31
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 32
    .line 33
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/EditMode;->Watermark:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 34
    .line 35
    const-string v3, "mWaterMarkBarBinding"

    .line 36
    .line 37
    const-string v4, "mWaterMarkBarBinding.tvClearMark"

    .line 38
    .line 39
    if-ne v1, v2, :cond_5

    .line 40
    .line 41
    if-nez p1, :cond_3

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    if-eqz v1, :cond_3

    .line 48
    .line 49
    const/4 v2, 0x0

    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/document/widget/DocumentView;->doneWatermark(Z)V

    .line 51
    .line 52
    .line 53
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 54
    .line 55
    if-nez v1, :cond_4

    .line 56
    .line 57
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    goto :goto_2

    .line 61
    :cond_4
    move-object v0, v1

    .line 62
    :goto_2
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 63
    .line 64
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_6

    .line 71
    .line 72
    :cond_5
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/EditMode;->TextAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 73
    .line 74
    const-string v5, "mAnnotationStyleBinding"

    .line 75
    .line 76
    if-ne v1, v2, :cond_a

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0O:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 79
    .line 80
    if-eqz v1, :cond_7

    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    if-eqz v2, :cond_6

    .line 87
    .line 88
    invoke-virtual {v2, v1, p1}, Lcom/intsig/document/widget/DocumentView;->doneElement(Lcom/intsig/document/widget/PagesView$ElementData;Z)I

    .line 89
    .line 90
    .line 91
    :cond_6
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0O:Lcom/intsig/document/widget/PagesView$ElementData;

    .line 92
    .line 93
    :cond_7
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 94
    .line 95
    if-nez v1, :cond_8

    .line 96
    .line 97
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    move-object v1, v0

    .line 101
    :cond_8
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 102
    .line 103
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    invoke-direct {p0, v1, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 107
    .line 108
    .line 109
    if-eqz p1, :cond_e

    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 112
    .line 113
    if-nez p1, :cond_9

    .line 114
    .line 115
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_3

    .line 119
    :cond_9
    move-object v0, p1

    .line 120
    :goto_3
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 121
    .line 122
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 123
    .line 124
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    .line 125
    .line 126
    .line 127
    move-result v1

    .line 128
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->oO(I)V

    .line 129
    .line 130
    .line 131
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->getSelectedColorIndex()I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇08O8o〇0(I)V

    .line 138
    .line 139
    .line 140
    goto :goto_6

    .line 141
    :cond_a
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 142
    .line 143
    if-ne v1, v2, :cond_e

    .line 144
    .line 145
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    const/4 v2, -0x1

    .line 150
    if-eqz v1, :cond_b

    .line 151
    .line 152
    invoke-virtual {v1, p1}, Lcom/intsig/document/widget/DocumentView;->doneInkAnnot(Z)I

    .line 153
    .line 154
    .line 155
    move-result p1

    .line 156
    goto :goto_4

    .line 157
    :cond_b
    const/4 p1, -0x1

    .line 158
    :goto_4
    if-le p1, v2, :cond_c

    .line 159
    .line 160
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8O(I)V

    .line 161
    .line 162
    .line 163
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 164
    .line 165
    if-nez p1, :cond_d

    .line 166
    .line 167
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    goto :goto_5

    .line 171
    :cond_d
    move-object v0, p1

    .line 172
    :goto_5
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 173
    .line 174
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080OO8〇0:Landroid/widget/SeekBar;

    .line 175
    .line 176
    invoke-virtual {v1}, Landroid/widget/ProgressBar;->getProgress()I

    .line 177
    .line 178
    .line 179
    move-result v1

    .line 180
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇〇〇0〇〇0(I)V

    .line 181
    .line 182
    .line 183
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->OO:Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;

    .line 184
    .line 185
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/signature/tab/CircleColorPickerView;->getSelectedColorIndex()I

    .line 186
    .line 187
    .line 188
    move-result v0

    .line 189
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->Oo8Oo00oo(I)V

    .line 190
    .line 191
    .line 192
    :cond_e
    :goto_6
    return-void
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇〇8o0OOOo(Ljava/lang/String;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-nez v2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v2, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 15
    :goto_1
    if-eqz v2, :cond_2

    .line 16
    .line 17
    return-void

    .line 18
    :cond_2
    new-instance v2, Lcom/intsig/app/AlertDialog$Builder;

    .line 19
    .line 20
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-direct {v2, v3}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->O8〇o(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const v2, 0x800005

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->o8(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const v2, 0x7f130023

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    new-instance v2, L〇0oO〇oo00/O8;

    .line 50
    .line 51
    invoke-direct {v2, p1}, L〇0oO〇oo00/O8;-><init>(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    const v3, 0x7f1313c1

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇〇0〇(IZLandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    new-instance v2, L〇0oO〇oo00/Oo08;

    .line 62
    .line 63
    invoke-direct {v2, p0, p1}, L〇0oO〇oo00/Oo08;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    const p1, 0x7f131d01

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇O8〇〇o(IZLandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const v0, 0x7f13057e

    .line 74
    .line 75
    .line 76
    const/4 v2, 0x0

    .line 77
    invoke-virtual {p1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    invoke-virtual {p1, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 90
    .line 91
    .line 92
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇o〇OoO8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇o0〇8(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo0O(Ljava/lang/String;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇〇0(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0oO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇〇00(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO0〇O(Ljava/lang/String;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇〇〇OOO〇〇()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_3

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto/16 :goto_0

    .line 14
    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;->SIGNATURE:Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇oo〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const/4 v3, 0x1

    .line 24
    if-eqz v2, :cond_1

    .line 25
    .line 26
    sget-object v2, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 27
    .line 28
    const-string v4, "has free point to save "

    .line 29
    .line 30
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1, v3}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->Oooo8o0〇(Lcom/intsig/camscanner/ads/reward/AdRewardedManager$RewardFunction;I)V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooo()V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->Oooo8o0〇()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-lez v0, :cond_2

    .line 45
    .line 46
    const-string v1, "CSFreeSignature"

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0〇〇00〇o()Lorg/json/JSONObject;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 53
    .line 54
    .line 55
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 58
    .line 59
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 60
    .line 61
    .line 62
    const v2, 0x7f131e57

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    new-array v2, v3, [Ljava/lang/Object;

    .line 70
    .line 71
    const/4 v3, 0x0

    .line 72
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 73
    .line 74
    .line 75
    move-result-object v4

    .line 76
    aput-object v4, v2, v3

    .line 77
    .line 78
    const v3, 0x7f13060c

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0, v3, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    new-instance v2, L〇0oO〇oo00/Oooo8o0〇;

    .line 90
    .line 91
    invoke-direct {v2, v0, p0}, L〇0oO〇oo00/Oooo8o0〇;-><init>(ILcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 92
    .line 93
    .line 94
    const v0, 0x7f130116

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    new-instance v1, L〇0oO〇oo00/〇〇808〇;

    .line 102
    .line 103
    invoke-direct {v1, p0}, L〇0oO〇oo00/〇〇808〇;-><init>(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V

    .line 104
    .line 105
    .line 106
    const v2, 0x7f130615

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 118
    .line 119
    .line 120
    return-void

    .line 121
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 122
    .line 123
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 124
    .line 125
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 126
    .line 127
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 128
    .line 129
    .line 130
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 131
    .line 132
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 133
    .line 134
    .line 135
    move-result-object v0

    .line 136
    const/16 v1, 0x1f4

    .line 137
    .line 138
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 139
    .line 140
    .line 141
    return-void

    .line 142
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 143
    .line 144
    const-string v1, "vip user or signature is free now "

    .line 145
    .line 146
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooo()V

    .line 150
    .line 151
    .line 152
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇〇〇O〇(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO8〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 14

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    const/16 v1, 0x65

    .line 15
    .line 16
    if-nez p1, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    const v3, 0x7f0a07b6

    .line 24
    .line 25
    .line 26
    if-ne v2, v3, :cond_3

    .line 27
    .line 28
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 29
    .line 30
    const-string v0, "click -> addAntiCounterfeit"

    .line 31
    .line 32
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 36
    .line 37
    const-string v0, "add_security_watermark"

    .line 38
    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O〇8O0O80〇()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-nez p1, :cond_2

    .line 47
    .line 48
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 49
    .line 50
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->ADD_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 51
    .line 52
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 53
    .line 54
    invoke-direct {p1, v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 55
    .line 56
    .line 57
    invoke-static {p0, p1, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0o88Oo〇()V

    .line 62
    .line 63
    .line 64
    goto/16 :goto_18

    .line 65
    .line 66
    :cond_3
    :goto_1
    if-nez p1, :cond_4

    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 70
    .line 71
    .line 72
    move-result v2

    .line 73
    const v3, 0x7f0a07f0

    .line 74
    .line 75
    .line 76
    if-ne v2, v3, :cond_5

    .line 77
    .line 78
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 79
    .line 80
    const-string v0, "click -> add pdfSignature"

    .line 81
    .line 82
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oo〇O0o〇()V

    .line 86
    .line 87
    .line 88
    goto/16 :goto_18

    .line 89
    .line 90
    :cond_5
    :goto_2
    if-nez p1, :cond_6

    .line 91
    .line 92
    goto :goto_3

    .line 93
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 94
    .line 95
    .line 96
    move-result v2

    .line 97
    const v3, 0x7f0a0817

    .line 98
    .line 99
    .line 100
    if-ne v2, v3, :cond_7

    .line 101
    .line 102
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 103
    .line 104
    const-string v0, "click -> addInkAnnotation"

    .line 105
    .line 106
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 110
    .line 111
    const-string v0, "annotate"

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    sget-object p1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 117
    .line 118
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O80OO(Lcom/intsig/camscanner/office_doc/data/EditMode;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇o〇OO80oO()V

    .line 122
    .line 123
    .line 124
    goto/16 :goto_18

    .line 125
    .line 126
    :cond_7
    :goto_3
    const/4 v2, 0x1

    .line 127
    if-nez p1, :cond_8

    .line 128
    .line 129
    goto :goto_4

    .line 130
    :cond_8
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 131
    .line 132
    .line 133
    move-result v3

    .line 134
    const v4, 0x7f0a07b3

    .line 135
    .line 136
    .line 137
    if-ne v3, v4, :cond_9

    .line 138
    .line 139
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 140
    .line 141
    const-string v1, "click -> addTextAnnotation"

    .line 142
    .line 143
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 147
    .line 148
    const-string v1, "insert_text"

    .line 149
    .line 150
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    invoke-static {p0, v0, v2, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO0o(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/lang/String;ILjava/lang/Object;)V

    .line 154
    .line 155
    .line 156
    goto/16 :goto_18

    .line 157
    .line 158
    :cond_9
    :goto_4
    const/4 v3, 0x0

    .line 159
    if-nez p1, :cond_a

    .line 160
    .line 161
    goto :goto_5

    .line 162
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 163
    .line 164
    .line 165
    move-result v4

    .line 166
    const v5, 0x7f0a07ef

    .line 167
    .line 168
    .line 169
    if-ne v4, v5, :cond_d

    .line 170
    .line 171
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 172
    .line 173
    const-string v0, "click -> pdf encrypt"

    .line 174
    .line 175
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 179
    .line 180
    const-string v0, "pdf_password"

    .line 181
    .line 182
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oO8()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 186
    .line 187
    .line 188
    move-result-object p1

    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o88oo〇O()Ljava/lang/String;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    if-eqz v0, :cond_b

    .line 194
    .line 195
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 196
    .line 197
    .line 198
    move-result v0

    .line 199
    if-nez v0, :cond_c

    .line 200
    .line 201
    :cond_b
    const/4 v3, 0x1

    .line 202
    :cond_c
    xor-int/lit8 v0, v3, 0x1

    .line 203
    .line 204
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->OO0o〇〇〇〇0(Z)V

    .line 205
    .line 206
    .line 207
    goto/16 :goto_18

    .line 208
    .line 209
    :cond_d
    :goto_5
    const-string v4, "mAnnotationStyleBinding"

    .line 210
    .line 211
    const-string v5, "mAnnotationStyleBinding.root"

    .line 212
    .line 213
    const-string v6, "write"

    .line 214
    .line 215
    const/4 v7, 0x2

    .line 216
    if-nez p1, :cond_e

    .line 217
    .line 218
    goto :goto_7

    .line 219
    :cond_e
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 220
    .line 221
    .line 222
    move-result v8

    .line 223
    const v9, 0x7f0a15dc

    .line 224
    .line 225
    .line 226
    if-ne v8, v9, :cond_12

    .line 227
    .line 228
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 229
    .line 230
    const-string v1, "click -> modify style"

    .line 231
    .line 232
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 236
    .line 237
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->Watermark:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 238
    .line 239
    if-ne p1, v1, :cond_f

    .line 240
    .line 241
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0o88Oo〇()V

    .line 242
    .line 243
    .line 244
    goto/16 :goto_18

    .line 245
    .line 246
    :cond_f
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->TextAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 247
    .line 248
    if-ne p1, v1, :cond_35

    .line 249
    .line 250
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 251
    .line 252
    invoke-static {p1, v6, v0, v7, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇o〇(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 253
    .line 254
    .line 255
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 256
    .line 257
    if-nez p1, :cond_10

    .line 258
    .line 259
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 260
    .line 261
    .line 262
    goto :goto_6

    .line 263
    :cond_10
    move-object v0, p1

    .line 264
    :goto_6
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 272
    .line 273
    .line 274
    move-result p1

    .line 275
    if-nez p1, :cond_11

    .line 276
    .line 277
    const/4 v3, 0x1

    .line 278
    :cond_11
    xor-int/lit8 p1, v3, 0x1

    .line 279
    .line 280
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 281
    .line 282
    .line 283
    goto/16 :goto_18

    .line 284
    .line 285
    :cond_12
    :goto_7
    const-string v8, "mWaterMarkBarBinding"

    .line 286
    .line 287
    const-string v9, "cs_list_pdf"

    .line 288
    .line 289
    const-string v10, "from_part"

    .line 290
    .line 291
    if-nez p1, :cond_13

    .line 292
    .line 293
    goto :goto_8

    .line 294
    :cond_13
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 295
    .line 296
    .line 297
    move-result v11

    .line 298
    const v12, 0x7f0a12f5

    .line 299
    .line 300
    .line 301
    if-ne v11, v12, :cond_15

    .line 302
    .line 303
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 304
    .line 305
    if-nez p1, :cond_14

    .line 306
    .line 307
    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 308
    .line 309
    .line 310
    move-object p1, v0

    .line 311
    :cond_14
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 312
    .line 313
    const-string v1, "mWaterMarkBarBinding.tvClearMark"

    .line 314
    .line 315
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    .line 317
    .line 318
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 319
    .line 320
    .line 321
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 322
    .line 323
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 324
    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    .line 326
    .line 327
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 328
    .line 329
    .line 330
    const-string v4, "click -> clear annotation editMode: "

    .line 331
    .line 332
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    .line 334
    .line 335
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 336
    .line 337
    .line 338
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 339
    .line 340
    .line 341
    move-result-object v1

    .line 342
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    .line 344
    .line 345
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇0(Z)V

    .line 346
    .line 347
    .line 348
    invoke-direct {p0, v0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O0o0(Ljava/lang/String;Z)V

    .line 349
    .line 350
    .line 351
    invoke-static {v10, v9}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 352
    .line 353
    .line 354
    move-result-object p1

    .line 355
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 356
    .line 357
    .line 358
    move-result-object p1

    .line 359
    const-string v0, "delete"

    .line 360
    .line 361
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 362
    .line 363
    .line 364
    move-result-object p1

    .line 365
    const-string v0, "CSInsertText"

    .line 366
    .line 367
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 368
    .line 369
    .line 370
    goto/16 :goto_18

    .line 371
    .line 372
    :cond_15
    :goto_8
    const-string v11, "save"

    .line 373
    .line 374
    if-nez p1, :cond_16

    .line 375
    .line 376
    goto :goto_d

    .line 377
    :cond_16
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 378
    .line 379
    .line 380
    move-result v12

    .line 381
    const v13, 0x7f0a0cf4

    .line 382
    .line 383
    .line 384
    if-ne v12, v13, :cond_1c

    .line 385
    .line 386
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO:Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;

    .line 387
    .line 388
    if-nez p1, :cond_17

    .line 389
    .line 390
    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 391
    .line 392
    .line 393
    move-object p1, v0

    .line 394
    :cond_17
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifyWaterMarkBarBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 395
    .line 396
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    .line 397
    .line 398
    .line 399
    move-result p1

    .line 400
    if-eqz p1, :cond_1b

    .line 401
    .line 402
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 403
    .line 404
    if-eqz p1, :cond_19

    .line 405
    .line 406
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 407
    .line 408
    .line 409
    move-result p1

    .line 410
    if-nez p1, :cond_18

    .line 411
    .line 412
    goto :goto_9

    .line 413
    :cond_18
    const/4 p1, 0x0

    .line 414
    goto :goto_a

    .line 415
    :cond_19
    :goto_9
    const/4 p1, 0x1

    .line 416
    :goto_a
    if-eqz p1, :cond_1a

    .line 417
    .line 418
    goto :goto_b

    .line 419
    :cond_1a
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 420
    .line 421
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 422
    .line 423
    new-instance v3, Ljava/lang/StringBuilder;

    .line 424
    .line 425
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 426
    .line 427
    .line 428
    const-string v4, "click -> save annotation editMode: "

    .line 429
    .line 430
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    .line 432
    .line 433
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 434
    .line 435
    .line 436
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 437
    .line 438
    .line 439
    move-result-object v1

    .line 440
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    .line 442
    .line 443
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇0(Z)V

    .line 444
    .line 445
    .line 446
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 447
    .line 448
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 449
    .line 450
    invoke-virtual {p1, v11, v1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 451
    .line 452
    .line 453
    goto :goto_c

    .line 454
    :cond_1b
    :goto_b
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇0(Z)V

    .line 455
    .line 456
    .line 457
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 458
    .line 459
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 460
    .line 461
    invoke-virtual {p1, v11, v1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 462
    .line 463
    .line 464
    :goto_c
    invoke-static {p0, v0, v2, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/camscanner/office_doc/data/EditMode;ILjava/lang/Object;)V

    .line 465
    .line 466
    .line 467
    goto/16 :goto_18

    .line 468
    .line 469
    :cond_1c
    :goto_d
    if-nez p1, :cond_1d

    .line 470
    .line 471
    goto :goto_f

    .line 472
    :cond_1d
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 473
    .line 474
    .line 475
    move-result v8

    .line 476
    const v12, 0x7f0a12ab

    .line 477
    .line 478
    .line 479
    if-ne v8, v12, :cond_20

    .line 480
    .line 481
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 482
    .line 483
    const-string v1, "click -> show or hide style setting layout"

    .line 484
    .line 485
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    .line 487
    .line 488
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;

    .line 489
    .line 490
    if-nez p1, :cond_1e

    .line 491
    .line 492
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 493
    .line 494
    .line 495
    goto :goto_e

    .line 496
    :cond_1e
    move-object v0, p1

    .line 497
    :goto_e
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ModifyAnnotationStyleBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 498
    .line 499
    .line 500
    move-result-object p1

    .line 501
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    .line 503
    .line 504
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 505
    .line 506
    .line 507
    move-result p1

    .line 508
    if-nez p1, :cond_1f

    .line 509
    .line 510
    const/4 v3, 0x1

    .line 511
    :cond_1f
    xor-int/lit8 p1, v3, 0x1

    .line 512
    .line 513
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇0〇o8〇(Z)V

    .line 514
    .line 515
    .line 516
    invoke-static {v10, v9}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 517
    .line 518
    .line 519
    move-result-object p1

    .line 520
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 521
    .line 522
    .line 523
    move-result-object p1

    .line 524
    invoke-static {v6, p1}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 525
    .line 526
    .line 527
    move-result-object p1

    .line 528
    const-string v0, "CSHandwrittenAnnotation"

    .line 529
    .line 530
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 531
    .line 532
    .line 533
    goto/16 :goto_18

    .line 534
    .line 535
    :cond_20
    :goto_f
    const-string v4, "mSmudgeAnnotationBinding.tvUndo"

    .line 536
    .line 537
    const-string v5, "mSmudgeAnnotationBinding.tvRedo"

    .line 538
    .line 539
    const-string v6, "mSmudgeAnnotationBinding"

    .line 540
    .line 541
    if-nez p1, :cond_21

    .line 542
    .line 543
    goto :goto_12

    .line 544
    :cond_21
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 545
    .line 546
    .line 547
    move-result v8

    .line 548
    const v9, 0x7f0a18d7

    .line 549
    .line 550
    .line 551
    if-ne v8, v9, :cond_26

    .line 552
    .line 553
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 554
    .line 555
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 556
    .line 557
    if-ne p1, v1, :cond_35

    .line 558
    .line 559
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 560
    .line 561
    .line 562
    move-result-object p1

    .line 563
    if-eqz p1, :cond_22

    .line 564
    .line 565
    invoke-virtual {p1}, Lcom/intsig/document/widget/DocumentView;->undoInk()I

    .line 566
    .line 567
    .line 568
    move-result p1

    .line 569
    goto :goto_10

    .line 570
    :cond_22
    const/4 p1, -0x1

    .line 571
    :goto_10
    sget-object v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 572
    .line 573
    new-instance v8, Ljava/lang/StringBuilder;

    .line 574
    .line 575
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 576
    .line 577
    .line 578
    const-string v9, "click -> ink undo: "

    .line 579
    .line 580
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    .line 582
    .line 583
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 584
    .line 585
    .line 586
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 587
    .line 588
    .line 589
    move-result-object v8

    .line 590
    invoke-static {v1, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    .line 592
    .line 593
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 594
    .line 595
    const-string v8, "revocation"

    .line 596
    .line 597
    invoke-static {v1, v8, v0, v7, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇o〇(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 598
    .line 599
    .line 600
    if-gez p1, :cond_24

    .line 601
    .line 602
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 603
    .line 604
    if-nez p1, :cond_23

    .line 605
    .line 606
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 607
    .line 608
    .line 609
    move-object p1, v0

    .line 610
    :cond_23
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 611
    .line 612
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 613
    .line 614
    .line 615
    invoke-direct {p0, p1, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 616
    .line 617
    .line 618
    :cond_24
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 619
    .line 620
    if-nez p1, :cond_25

    .line 621
    .line 622
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 623
    .line 624
    .line 625
    goto :goto_11

    .line 626
    :cond_25
    move-object v0, p1

    .line 627
    :goto_11
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 628
    .line 629
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 630
    .line 631
    .line 632
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 633
    .line 634
    .line 635
    goto/16 :goto_18

    .line 636
    .line 637
    :cond_26
    :goto_12
    if-nez p1, :cond_27

    .line 638
    .line 639
    goto/16 :goto_15

    .line 640
    .line 641
    :cond_27
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 642
    .line 643
    .line 644
    move-result v8

    .line 645
    const v9, 0x7f0a171c

    .line 646
    .line 647
    .line 648
    if-ne v8, v9, :cond_2e

    .line 649
    .line 650
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 651
    .line 652
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 653
    .line 654
    if-ne p1, v1, :cond_35

    .line 655
    .line 656
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 657
    .line 658
    .line 659
    move-result-object p1

    .line 660
    if-eqz p1, :cond_28

    .line 661
    .line 662
    invoke-virtual {p1}, Lcom/intsig/document/widget/DocumentView;->redoInk()I

    .line 663
    .line 664
    .line 665
    move-result p1

    .line 666
    goto :goto_13

    .line 667
    :cond_28
    const/4 p1, 0x0

    .line 668
    :goto_13
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 669
    .line 670
    .line 671
    move-result-object v1

    .line 672
    if-eqz v1, :cond_29

    .line 673
    .line 674
    invoke-virtual {v1}, Lcom/intsig/document/widget/DocumentView;->getInkCount()I

    .line 675
    .line 676
    .line 677
    move-result v1

    .line 678
    goto :goto_14

    .line 679
    :cond_29
    const/4 v1, 0x0

    .line 680
    :goto_14
    sget-object v8, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 681
    .line 682
    const-string v9, "redo"

    .line 683
    .line 684
    invoke-static {v8, v9, v0, v7, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇o〇(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 685
    .line 686
    .line 687
    if-ltz p1, :cond_2b

    .line 688
    .line 689
    sub-int/2addr v1, v2

    .line 690
    if-ne p1, v1, :cond_2b

    .line 691
    .line 692
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 693
    .line 694
    if-nez v1, :cond_2a

    .line 695
    .line 696
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 697
    .line 698
    .line 699
    move-object v1, v0

    .line 700
    :cond_2a
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 701
    .line 702
    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 703
    .line 704
    .line 705
    invoke-direct {p0, v1, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 706
    .line 707
    .line 708
    :cond_2b
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o〇00O:Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;

    .line 709
    .line 710
    if-nez v1, :cond_2c

    .line 711
    .line 712
    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 713
    .line 714
    .line 715
    move-object v1, v0

    .line 716
    :cond_2c
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ModifySmudgeAnnotationBarBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 717
    .line 718
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 719
    .line 720
    .line 721
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->Oo0O〇8800(Landroid/view/View;Z)V

    .line 722
    .line 723
    .line 724
    sget-object v1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 725
    .line 726
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 727
    .line 728
    .line 729
    move-result-object v2

    .line 730
    if-eqz v2, :cond_2d

    .line 731
    .line 732
    invoke-virtual {v2}, Lcom/intsig/document/widget/DocumentView;->getInkCount()I

    .line 733
    .line 734
    .line 735
    move-result v0

    .line 736
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 737
    .line 738
    .line 739
    move-result-object v0

    .line 740
    :cond_2d
    new-instance v2, Ljava/lang/StringBuilder;

    .line 741
    .line 742
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 743
    .line 744
    .line 745
    const-string v3, "click -> ink redo: "

    .line 746
    .line 747
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    .line 749
    .line 750
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 751
    .line 752
    .line 753
    const-string p1, ", linkCount:"

    .line 754
    .line 755
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 756
    .line 757
    .line 758
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 759
    .line 760
    .line 761
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 762
    .line 763
    .line 764
    move-result-object p1

    .line 765
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    .line 767
    .line 768
    goto :goto_18

    .line 769
    :cond_2e
    :goto_15
    if-nez p1, :cond_2f

    .line 770
    .line 771
    goto :goto_17

    .line 772
    :cond_2f
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 773
    .line 774
    .line 775
    move-result v4

    .line 776
    const v5, 0x7f0a0cf3

    .line 777
    .line 778
    .line 779
    if-ne v4, v5, :cond_33

    .line 780
    .line 781
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 782
    .line 783
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/EditMode;->InkAnnotation:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 784
    .line 785
    if-ne p1, v1, :cond_32

    .line 786
    .line 787
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 788
    .line 789
    const-string v1, "click -> save inkAnnot"

    .line 790
    .line 791
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    .line 793
    .line 794
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 795
    .line 796
    .line 797
    move-result-object p1

    .line 798
    if-eqz p1, :cond_30

    .line 799
    .line 800
    invoke-virtual {p1}, Lcom/intsig/document/widget/DocumentView;->getInkCount()I

    .line 801
    .line 802
    .line 803
    move-result p1

    .line 804
    goto :goto_16

    .line 805
    :cond_30
    const/4 p1, 0x0

    .line 806
    :goto_16
    if-lez p1, :cond_31

    .line 807
    .line 808
    const/4 v3, 0x1

    .line 809
    :cond_31
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 810
    .line 811
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 812
    .line 813
    .line 814
    move-result-object v1

    .line 815
    invoke-virtual {p1, v11, v1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 816
    .line 817
    .line 818
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇0(Z)V

    .line 819
    .line 820
    .line 821
    :cond_32
    invoke-static {p0, v0, v2, v0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Lcom/intsig/camscanner/office_doc/data/EditMode;ILjava/lang/Object;)V

    .line 822
    .line 823
    .line 824
    goto :goto_18

    .line 825
    :cond_33
    :goto_17
    if-nez p1, :cond_34

    .line 826
    .line 827
    goto :goto_18

    .line 828
    :cond_34
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 829
    .line 830
    .line 831
    move-result p1

    .line 832
    const v0, 0x7f0a0cb8

    .line 833
    .line 834
    .line 835
    if-ne p1, v0, :cond_35

    .line 836
    .line 837
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 838
    .line 839
    const-string v0, "remove_watermark"

    .line 840
    .line 841
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->Oooo8o0〇(Ljava/lang/String;)V

    .line 842
    .line 843
    .line 844
    new-instance p1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 845
    .line 846
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_WATERMARK_FREE_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 847
    .line 848
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 849
    .line 850
    invoke-direct {p1, v0, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 851
    .line 852
    .line 853
    invoke-static {p0, p1, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 854
    .line 855
    .line 856
    :cond_35
    :goto_18
    return-void
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public enableToolbar()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 6
    .line 7
    const v0, 0x7f0a0e66

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 19
    .line 20
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇〇808〇()V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇00o〇O8()V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->oooO8〇00()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public interceptBackPressed()Z
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->O08〇oO8〇()Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 8
    .line 9
    sget-object v2, Lcom/intsig/camscanner/office_doc/data/EditMode;->InsertImage:Lcom/intsig/camscanner/office_doc/data/EditMode;

    .line 10
    .line 11
    const/4 v3, 0x1

    .line 12
    if-ne v1, v2, :cond_0

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇00〇〇〇o〇8()V

    .line 15
    .line 16
    .line 17
    return v3

    .line 18
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->onBackPressed()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x0

    .line 26
    const/4 v7, 0x0

    .line 27
    const/4 v8, 0x0

    .line 28
    const/16 v9, 0xc

    .line 29
    .line 30
    const/4 v10, 0x0

    .line 31
    move-object v4, p0

    .line 32
    invoke-static/range {v4 .. v10}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooOO(Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;Ljava/io/File;Ljava/lang/String;ZZILjava/lang/Object;)V

    .line 33
    .line 34
    .line 35
    return v3

    .line 36
    :cond_1
    invoke-super {p0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->interceptBackPressed()Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    return v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/16 p2, 0x65

    .line 5
    .line 6
    if-eq p1, p2, :cond_2

    .line 7
    .line 8
    const/16 p2, 0x1f4

    .line 9
    .line 10
    if-eq p1, p2, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇O00()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-nez p1, :cond_1

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-eqz p1, :cond_3

    .line 24
    .line 25
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 26
    .line 27
    const-string p2, "onActivityResult, vip user or signature is free now "

    .line 28
    .line 29
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8ooo()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇8〇0O〇()V

    .line 37
    .line 38
    .line 39
    sget-object p1, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 40
    .line 41
    const-string p2, "onActivityResult -> requestCode: 101"

    .line 42
    .line 43
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_3
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroyView()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o8o0()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇08〇o0O:Lkotlinx/coroutines/Job;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-static {v0, v2, v1, v2}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onPause()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->〇〇〇0o〇〇0:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onPause"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/edit/PdfEditFragment;->o0:Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfEditBinding;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/newsign/esign/tabview/ESignTabView;->OOO〇O0()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d031d

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

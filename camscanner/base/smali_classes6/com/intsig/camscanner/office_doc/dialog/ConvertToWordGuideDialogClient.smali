.class public final Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;
.super Lcom/intsig/camscanner/tools/AbstractGuideClientContract;
.source "ConvertToWordGuideDialogClient.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80〇808〇O:Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇80〇808〇O:Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;-><init>(Landroid/app/Activity;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OoO8(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇oo〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇8O8〇008(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->o800o8O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o800o8O()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->o〇0()Lcom/intsig/camscanner/tools/AbstractGuideClientContract$GuidPopClientCallback;

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇80〇808〇O()Landroid/app/Dialog;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oo88o8O(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->oO80()Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇o00〇〇Oo()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    :cond_0
    if-nez v1, :cond_2

    .line 16
    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    const v0, 0x7f0a0c9f

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    check-cast v0, Lcom/intsig/camscanner/view/ArrowLinearLayout;

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    new-instance v1, L〇〇O80〇0o/Oo08;

    .line 31
    .line 32
    invoke-direct {v1}, L〇〇O80〇0o/Oo08;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    if-eqz p1, :cond_2

    .line 39
    .line 40
    new-instance v0, L〇〇O80〇0o/o〇0;

    .line 41
    .line 42
    invoke-direct {v0, p0}, L〇〇O80〇0o/o〇0;-><init>(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    .line 47
    .line 48
    :cond_2
    if-eqz p1, :cond_3

    .line 49
    .line 50
    const v0, 0x7f0a14a7

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    new-instance v1, L〇〇O80〇0o/〇〇888;

    .line 60
    .line 61
    invoke-direct {v1, p0}, L〇〇O80〇0o/〇〇888;-><init>(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    .line 66
    .line 67
    :cond_3
    if-eqz p1, :cond_4

    .line 68
    .line 69
    const v0, 0x7f0a087d

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    check-cast p1, Landroid/widget/ImageView;

    .line 77
    .line 78
    if-eqz p1, :cond_4

    .line 79
    .line 80
    new-instance v0, L〇〇O80〇0o/oO80;

    .line 81
    .line 82
    invoke-direct {v0, p0}, L〇〇O80〇0o/oO80;-><init>(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    .line 87
    .line 88
    :cond_4
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->o800o8O()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇00(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->Oo08()Lcom/intsig/callback/Callback0;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/callback/Callback0;->call()V

    .line 13
    .line 14
    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->o800o8O()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0000OOO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇〇888()Landroid/app/Activity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d07af

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    const v1, 0x7f0a0d5c

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    move-object v2, v0

    .line 27
    check-cast v2, Lcom/airbnb/lottie/LottieAnimationView;

    .line 28
    .line 29
    :cond_0
    if-eqz v2, :cond_1

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇00(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->O〇8O8〇008(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O888o0o(Landroid/view/View;)V
    .locals 2

    .line 1
    const v0, 0x7f0a0d5c

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Lcom/airbnb/lottie/LottieAnimationView;

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string v0, "lottie_word_list_guide_cn"

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const-string v0, "lottie_word_list_guide_eg"

    .line 20
    .line 21
    :goto_0
    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setImageAssetsFolder(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const/4 v0, -0x1

    .line 25
    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_1

    .line 33
    .line 34
    const v1, 0x7f120058

    .line 35
    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_1
    const v1, 0x7f120059

    .line 39
    .line 40
    .line 41
    :goto_1
    invoke-virtual {p1, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 52
    .line 53
    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇oOO8O8(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇oOO8O8(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇80〇808〇O()Landroid/app/Dialog;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    if-eqz p0, :cond_0

    .line 11
    .line 12
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇oo〇(Landroid/view/View;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8(Landroid/view/View;)Lcom/intsig/camscanner/view/IArrowViewContract;
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const v0, 0x7f0a0c9f

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/view/ArrowLinearLayout;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 p1, 0x0

    .line 14
    :goto_0
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O8ooOoo〇(Landroid/view/View;Lcom/intsig/callback/Callback0;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "targetView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "ConvertToWordGuideDialogClient"

    .line 7
    .line 8
    const-string v1, "showGuide"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇8o8o〇(Lcom/intsig/callback/Callback0;)V

    .line 14
    .line 15
    .line 16
    new-instance p2, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion$GuideDialogParams;

    .line 17
    .line 18
    invoke-direct {p2}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient$Companion$GuideDialogParams;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v0, Landroid/graphics/Rect;

    .line 22
    .line 23
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 24
    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    new-array v1, v1, [I

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->OoO8([I)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇O888o0o(Landroid/graphics/Rect;)V

    .line 39
    .line 40
    .line 41
    sget-object v0, Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;->TOP:Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 42
    .line 43
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇O〇(Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;)V

    .line 44
    .line 45
    .line 46
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const/16 v2, 0x8

    .line 53
    .line 54
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->o800o8O(I)V

    .line 59
    .line 60
    .line 61
    const/16 v1, 0x37

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇〇8O0〇8(I)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇O8o08O(Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;)V

    .line 75
    .line 76
    .line 77
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇0000OOO()V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇〇888()Landroid/app/Activity;

    .line 81
    .line 82
    .line 83
    move-result-object p2

    .line 84
    invoke-virtual {p0, p2, p1}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->OO0o〇〇(Landroid/content/Context;Landroid/view/View;)V

    .line 85
    .line 86
    .line 87
    new-instance p2, L〇〇O80〇0o/O8;

    .line 88
    .line 89
    invoke-direct {p2, p0}, L〇〇O80〇0o/O8;-><init>(Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;)V

    .line 90
    .line 91
    .line 92
    const-wide/16 v0, 0x1f40

    .line 93
    .line 94
    invoke-virtual {p1, p2, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method protected OO0o〇〇〇〇0()Landroid/view/View;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->〇〇888()Landroid/app/Activity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const v1, 0x7f0d07af

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    const v1, 0x7f0a0c9f

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/view/ArrowLinearLayout;

    .line 27
    .line 28
    if-eqz v1, :cond_2

    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract;->oO80()Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    invoke-virtual {v2}, Lcom/intsig/camscanner/tools/AbstractGuideClientContract$Companion$GuidParams;->〇080()Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    if-nez v2, :cond_1

    .line 41
    .line 42
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;->BOTTOM:Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;

    .line 43
    .line 44
    :cond_1
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/ArrowLinearLayout;->setArrowDirection(Lcom/intsig/camscanner/view/IArrowViewContract$ArrowDirection;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    const-string v1, "rootView"

    .line 48
    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->〇O888o0o(Landroid/view/View;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/dialog/ConvertToWordGuideDialogClient;->oo88o8O(Landroid/view/View;)V

    .line 56
    .line 57
    .line 58
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.class public final Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "WaterMarkSetDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Ljava/lang/String;

.field private final OO〇00〇8oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

.field private oOo〇8o008:Ljava/lang/String;

.field private o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

.field private 〇080OO8〇0:Ljava/lang/Long;

.field private final 〇08O〇00〇o:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/intsig/document/widget/PagesView$WatermarkArgs;",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "WaterMarkSetDialog::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .param p1    # Lkotlin/jvm/functions/Function2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/intsig/document/widget/PagesView$WatermarkArgs;",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "callBack"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function2;

    .line 10
    .line 11
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$mShareHelper$2;

    .line 14
    .line 15
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$mShareHelper$2;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)V

    .line 16
    .line 17
    .line 18
    invoke-static {p1, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->OO〇00〇8oO:Lkotlin/Lazy;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$loadingDialog$2;

    .line 25
    .line 26
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$loadingDialog$2;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)V

    .line 27
    .line 28
    .line 29
    invoke-static {p1, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o8〇OO0〇0o:Lkotlin/Lazy;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Ljava/lang/Long;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇080OO8〇0:Ljava/lang/Long;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final O8〇8〇O80(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;Landroidx/fragment/app/FragmentManager;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lkotlin/jvm/functions/Function2;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Landroidx/fragment/app/FragmentManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/intsig/document/widget/PagesView$WatermarkArgs;",
            "-",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;",
            "Landroidx/fragment/app/FragmentManager;",
            ")V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;

    .line 2
    .line 3
    move-wide v1, p0

    .line 4
    move-object v3, p2

    .line 5
    move-object v4, p3

    .line 6
    move-object v5, p4

    .line 7
    move-object v6, p5

    .line 8
    move-object v7, p6

    .line 9
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$Companion;->〇o00〇〇Oo(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function2;Landroidx/fragment/app/FragmentManager;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private final Ooo8o(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "binding"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 13
    .line 14
    const-string v3, "binding.tvAddMark"

    .line 15
    .line 16
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    xor-int/lit8 v3, p1, 0x1

    .line 20
    .line 21
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 25
    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    move-object v0, v1

    .line 32
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 33
    .line 34
    const-string v3, "binding.tvDelMark"

    .line 35
    .line 36
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 43
    .line 44
    if-nez v0, :cond_2

    .line 45
    .line 46
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    move-object v0, v1

    .line 50
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 51
    .line 52
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setEnableTouch(Z)V

    .line 53
    .line 54
    .line 55
    if-eqz p1, :cond_3

    .line 56
    .line 57
    const/high16 p1, 0x3f800000    # 1.0f

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_3
    const p1, 0x3e99999a    # 0.3f

    .line 61
    .line 62
    .line 63
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 64
    .line 65
    if-nez v0, :cond_4

    .line 66
    .line 67
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :cond_4
    move-object v1, v0

    .line 72
    :goto_1
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 73
    .line 74
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 75
    .line 76
    .line 77
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->Ooo8o(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOoO8OO〇(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const p1, 0x7f130bfc

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const p1, 0x7f130664

    .line 8
    .line 9
    .line 10
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$addAntiCounterfeit$1;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$addAntiCounterfeit$1;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)V

    .line 17
    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->〇O〇(Landroid/content/Context;Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog$SecurityMarkChangeListener;)Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const-string v1, "cs_list_pdf"

    .line 24
    .line 25
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->〇O888o0o(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Lcom/intsig/document/widget/PagesView$WatermarkArgs;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$savePdf$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$savePdf$1;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0oO〇oo00()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v1, "doc_id"

    .line 8
    .line 9
    const-wide/16 v2, -0x1

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;J)J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇080OO8〇0:Ljava/lang/Long;

    .line 20
    .line 21
    const-string v1, "pdf_path"

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->O8o08O8O:Ljava/lang/String;

    .line 28
    .line 29
    const-string v1, "pdf_dst_path"

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo〇8o008:Ljava/lang/String;

    .line 36
    .line 37
    const-string v1, "font_path"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0O:Ljava/lang/String;

    .line 44
    .line 45
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0O:Ljava/lang/String;

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-nez v0, :cond_1

    .line 52
    .line 53
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const/4 v2, 0x0

    .line 58
    const/4 v3, 0x0

    .line 59
    new-instance v4, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$initData$2;

    .line 60
    .line 61
    const/4 v0, 0x0

    .line 62
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$initData$2;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Lkotlin/coroutines/Continuation;)V

    .line 63
    .line 64
    .line 65
    const/4 v5, 0x3

    .line 66
    const/4 v6, 0x0

    .line 67
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇8O0880()V

    .line 72
    .line 73
    .line 74
    :goto_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0ooOOo()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->o8oO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    .line 9
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->ADD_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 10
    .line 11
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 12
    .line 13
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 14
    .line 15
    .line 16
    const/16 v1, 0x65

    .line 17
    .line 18
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇O0o〇〇o()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8O0880()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO〇oo()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/16 v1, 0x14

    .line 6
    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80O8o8O〇(I)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOo〇08〇()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    const/16 v3, 0x28

    .line 16
    .line 17
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O8oOo0(I)I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v4, :cond_0

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const v4, 0x7f130bfd

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :cond_0
    new-instance v4, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 41
    .line 42
    invoke-direct {v4, v0, v2, v3, v1}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 43
    .line 44
    .line 45
    sget-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 46
    .line 47
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->〇o00〇〇Oo()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->〇〇888()I

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->o〇0()I

    .line 56
    .line 57
    .line 58
    move-result v3

    .line 59
    new-instance v5, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v6, "default AntiCounterfeit alpha:"

    .line 65
    .line 66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string v1, " ,size:"

    .line 73
    .line 74
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    const-string v1, "level:"

    .line 81
    .line 82
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    new-instance v0, Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 96
    .line 97
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->oO80()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v1

    .line 101
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0O:Ljava/lang/String;

    .line 102
    .line 103
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->〇o〇()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object v3

    .line 107
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->〇o00〇〇Oo()I

    .line 108
    .line 109
    .line 110
    move-result v5

    .line 111
    int-to-float v5, v5

    .line 112
    const/high16 v6, 0x42c80000    # 100.0f

    .line 113
    .line 114
    div-float/2addr v5, v6

    .line 115
    invoke-static {v3, v5}, Lcom/intsig/utils/ColorUtil;->Oo08(Ljava/lang/String;F)I

    .line 116
    .line 117
    .line 118
    move-result v3

    .line 119
    invoke-virtual {v4}, Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;->〇〇888()I

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇〇〇00(I)I

    .line 124
    .line 125
    .line 126
    move-result v4

    .line 127
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/intsig/document/widget/PagesView$WatermarkArgs;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 128
    .line 129
    .line 130
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 131
    .line 132
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇8O0880()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O0o〇〇o()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇080OO8〇0:Ljava/lang/Long;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    const-wide/16 v3, 0x0

    .line 10
    .line 11
    cmp-long v5, v1, v3

    .line 12
    .line 13
    if-gtz v5, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 17
    .line 18
    const-string v2, "begin sharePdf"

    .line 19
    .line 20
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lcom/intsig/camscanner/util/PdfUtils;->〇080:Lcom/intsig/camscanner/util/PdfUtils;

    .line 24
    .line 25
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    const-string v3, "requireActivity()"

    .line 30
    .line 31
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 35
    .line 36
    .line 37
    move-result-wide v3

    .line 38
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/util/PdfUtils;->〇00(Landroidx/fragment/app/FragmentActivity;J)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/SharePdf;->〇〇o0o(Lcom/intsig/document/widget/PagesView$WatermarkArgs;)V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x1

    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/type/SharePdf;->o〇(Z)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇〇O80〇0o()Lcom/intsig/camscanner/share/ShareHelper;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 56
    .line 57
    .line 58
    :cond_1
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇O8〇8000()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo〇8o008:Ljava/lang/String;

    .line 11
    .line 12
    const/4 v1, 0x1

    .line 13
    const-string v2, "binding"

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    if-eqz v0, :cond_2

    .line 17
    .line 18
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->O8o08O8O:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v4, v0}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 24
    .line 25
    if-nez v0, :cond_1

    .line 26
    .line 27
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    move-object v0, v3

    .line 31
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 32
    .line 33
    const-string v4, "binding.tvShare"

    .line 34
    .line 35
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 39
    .line 40
    .line 41
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->OO0o〇〇()V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 47
    .line 48
    if-nez v0, :cond_3

    .line 49
    .line 50
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    move-object v0, v3

    .line 54
    :cond_3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->O8o08O8O:Lcom/intsig/document/widget/DocumentView;

    .line 55
    .line 56
    const/4 v4, 0x0

    .line 57
    invoke-virtual {v0, v4}, Lcom/intsig/document/widget/DocumentView;->enableTextSelection(Z)Lcom/intsig/document/widget/DocumentView;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v4}, Lcom/intsig/document/widget/DocumentView;->enableAnnotOperate(Z)Lcom/intsig/document/widget/DocumentView;

    .line 61
    .line 62
    .line 63
    const v5, 0x7f0601e5

    .line 64
    .line 65
    .line 66
    const v6, 0x3e4ccccd    # 0.2f

    .line 67
    .line 68
    .line 69
    invoke-static {v5, v6}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 70
    .line 71
    .line 72
    move-result v5

    .line 73
    sget-object v6, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 74
    .line 75
    invoke-virtual {v6}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    const/4 v7, 0x3

    .line 80
    invoke-static {v6, v7}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    const v8, 0x66aaaaaa

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v8, v5, v6}, Lcom/intsig/document/widget/DocumentView;->setSideBarStyle(III)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v4}, Lcom/intsig/document/widget/DocumentView;->setPageBorderSize(I)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    invoke-virtual {v0, v5}, Lcom/intsig/document/widget/DocumentView;->build(Landroid/content/Context;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 101
    .line 102
    .line 103
    move-result-object v5

    .line 104
    const v6, 0x7f080bda

    .line 105
    .line 106
    .line 107
    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 108
    .line 109
    .line 110
    move-result-object v5

    .line 111
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 112
    .line 113
    .line 114
    move-result-object v6

    .line 115
    const v8, 0x7f060207

    .line 116
    .line 117
    .line 118
    invoke-static {v6, v8}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 119
    .line 120
    .line 121
    move-result v6

    .line 122
    invoke-virtual {v0, v5, v6}, Lcom/intsig/document/widget/DocumentView;->setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V

    .line 123
    .line 124
    .line 125
    new-instance v5, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$initView$1$1;

    .line 126
    .line 127
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog$initView$1$1;-><init>(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;Lcom/intsig/document/widget/DocumentView;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v0, v5}, Lcom/intsig/document/widget/DocumentView;->SetActionListener(Lcom/intsig/document/widget/DocumentView$DocumentActionListener;)V

    .line 131
    .line 132
    .line 133
    iget-object v5, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->O8o08O8O:Ljava/lang/String;

    .line 134
    .line 135
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 136
    .line 137
    .line 138
    move-result v5

    .line 139
    if-eqz v5, :cond_4

    .line 140
    .line 141
    iget-object v5, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->O8o08O8O:Ljava/lang/String;

    .line 142
    .line 143
    invoke-virtual {v0, v5, v3}, Lcom/intsig/document/widget/DocumentView;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    :cond_4
    const/4 v0, 0x6

    .line 147
    new-array v0, v0, [Landroid/view/View;

    .line 148
    .line 149
    iget-object v5, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 150
    .line 151
    if-nez v5, :cond_5

    .line 152
    .line 153
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    move-object v5, v3

    .line 157
    :cond_5
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->OO:Landroid/widget/ImageView;

    .line 158
    .line 159
    aput-object v5, v0, v4

    .line 160
    .line 161
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 162
    .line 163
    if-nez v4, :cond_6

    .line 164
    .line 165
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    move-object v4, v3

    .line 169
    :cond_6
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 170
    .line 171
    aput-object v4, v0, v1

    .line 172
    .line 173
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 174
    .line 175
    if-nez v1, :cond_7

    .line 176
    .line 177
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    move-object v1, v3

    .line 181
    :cond_7
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 182
    .line 183
    const/4 v4, 0x2

    .line 184
    aput-object v1, v0, v4

    .line 185
    .line 186
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 187
    .line 188
    if-nez v1, :cond_8

    .line 189
    .line 190
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    move-object v1, v3

    .line 194
    :cond_8
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 195
    .line 196
    aput-object v1, v0, v7

    .line 197
    .line 198
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 199
    .line 200
    if-nez v1, :cond_9

    .line 201
    .line 202
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    move-object v1, v3

    .line 206
    :cond_9
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 207
    .line 208
    const/4 v4, 0x4

    .line 209
    aput-object v1, v0, v4

    .line 210
    .line 211
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 212
    .line 213
    if-nez v1, :cond_a

    .line 214
    .line 215
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    goto :goto_0

    .line 219
    :cond_a
    move-object v3, v1

    .line 220
    :goto_0
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 221
    .line 222
    const/4 v2, 0x5

    .line 223
    aput-object v1, v0, v2

    .line 224
    .line 225
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 226
    .line 227
    .line 228
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static final synthetic 〇o〇88〇8()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇O80〇0o()Lcom/intsig/camscanner/share/ShareHelper;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->OO〇00〇8oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;)Lcom/intsig/app/BaseProgressDialog;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇〇〇0()Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇0()Lcom/intsig/app/BaseProgressDialog;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o8〇OO0〇0o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/app/BaseProgressDialog;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇〇00(I)I
    .locals 1

    .line 1
    div-int/lit8 p1, p1, 0x18

    .line 2
    .line 3
    const/16 v0, 0xa

    .line 4
    .line 5
    rsub-int/lit8 p1, p1, 0xa

    .line 6
    .line 7
    invoke-static {p1, v0}, Lkotlin/ranges/RangesKt;->o〇0(II)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    if-nez p1, :cond_1

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const v2, 0x7f0a0847

    .line 22
    .line 23
    .line 24
    if-ne v1, v2, :cond_2

    .line 25
    .line 26
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 27
    .line 28
    .line 29
    goto/16 :goto_7

    .line 30
    .line 31
    :cond_2
    :goto_1
    const/4 v1, 0x1

    .line 32
    if-nez p1, :cond_3

    .line 33
    .line 34
    goto :goto_2

    .line 35
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    const v3, 0x7f0a1258

    .line 40
    .line 41
    .line 42
    if-ne v2, v3, :cond_4

    .line 43
    .line 44
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOoO8OO〇(Z)V

    .line 45
    .line 46
    .line 47
    goto/16 :goto_7

    .line 48
    .line 49
    :cond_4
    :goto_2
    const/4 v2, 0x2

    .line 50
    const/4 v3, 0x0

    .line 51
    if-nez p1, :cond_5

    .line 52
    .line 53
    goto :goto_3

    .line 54
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 55
    .line 56
    .line 57
    move-result v4

    .line 58
    const v5, 0x7f0a1368

    .line 59
    .line 60
    .line 61
    if-ne v4, v5, :cond_7

    .line 62
    .line 63
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 66
    .line 67
    if-nez p1, :cond_6

    .line 68
    .line 69
    const-string p1, "binding"

    .line 70
    .line 71
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    move-object p1, v0

    .line 75
    :cond_6
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->O8o08O8O:Lcom/intsig/document/widget/DocumentView;

    .line 76
    .line 77
    invoke-virtual {p1, v3}, Lcom/intsig/document/widget/DocumentView;->doneWatermark(Z)V

    .line 78
    .line 79
    .line 80
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->Ooo8o(Z)V

    .line 81
    .line 82
    .line 83
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 84
    .line 85
    const-string v1, "clear_security_water"

    .line 86
    .line 87
    invoke-static {p1, v1, v0, v2, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇O8o08O(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_7

    .line 91
    .line 92
    :cond_7
    :goto_3
    if-nez p1, :cond_8

    .line 93
    .line 94
    goto :goto_4

    .line 95
    :cond_8
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 96
    .line 97
    .line 98
    move-result v4

    .line 99
    const v5, 0x7f0a15db

    .line 100
    .line 101
    .line 102
    if-ne v4, v5, :cond_9

    .line 103
    .line 104
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 105
    .line 106
    const-string v1, "modify_security_water"

    .line 107
    .line 108
    invoke-static {p1, v1, v0, v2, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇O8o08O(Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;Ljava/lang/String;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 109
    .line 110
    .line 111
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOoO8OO〇(Z)V

    .line 112
    .line 113
    .line 114
    goto :goto_7

    .line 115
    :cond_9
    :goto_4
    if-nez p1, :cond_a

    .line 116
    .line 117
    goto :goto_6

    .line 118
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    const v2, 0x7f0a0cf2

    .line 123
    .line 124
    .line 125
    if-ne v0, v2, :cond_e

    .line 126
    .line 127
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 130
    .line 131
    if-eqz v0, :cond_b

    .line 132
    .line 133
    goto :goto_5

    .line 134
    :cond_b
    const/4 v1, 0x0

    .line 135
    :goto_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    const-string v1, "save"

    .line 140
    .line 141
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/office_doc/preview/PdfLogAgentHelper;->〇8o8o〇(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 142
    .line 143
    .line 144
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo〇8o008:Ljava/lang/String;

    .line 145
    .line 146
    if-nez p1, :cond_c

    .line 147
    .line 148
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function2;

    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 151
    .line 152
    const-wide/16 v1, -0x1

    .line 153
    .line 154
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    .line 160
    .line 161
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 162
    .line 163
    .line 164
    goto :goto_7

    .line 165
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->oOo0:Lcom/intsig/document/widget/PagesView$WatermarkArgs;

    .line 166
    .line 167
    if-nez p1, :cond_d

    .line 168
    .line 169
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 170
    .line 171
    .line 172
    goto :goto_7

    .line 173
    :cond_d
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇08O()V

    .line 174
    .line 175
    .line 176
    goto :goto_7

    .line 177
    :cond_e
    :goto_6
    if-nez p1, :cond_f

    .line 178
    .line 179
    goto :goto_7

    .line 180
    :cond_f
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 181
    .line 182
    .line 183
    move-result p1

    .line 184
    const v0, 0x7f0a17a6

    .line 185
    .line 186
    .line 187
    if-ne p1, v0, :cond_10

    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0ooOOo()V

    .line 190
    .line 191
    .line 192
    :cond_10
    :goto_7
    return-void
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇0oO〇oo00()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇O8〇8000()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance p1, Landroid/app/Dialog;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f140009

    .line 8
    .line 9
    .line 10
    invoke-direct {p1, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const v1, 0x7f0d0251

    .line 26
    .line 27
    .line 28
    const/4 v2, 0x0

    .line 29
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const-string v2, "bind(rootView)"

    .line 38
    .line 39
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 43
    .line 44
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    .line 57
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    const/16 v3, 0x50

    .line 62
    .line 63
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 64
    .line 65
    const/4 v3, -0x1

    .line 66
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 67
    .line 68
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 69
    .line 70
    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 71
    .line 72
    .line 73
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    .line 74
    .line 75
    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    .line 80
    .line 81
    :cond_0
    return-object p1
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onDestroyView()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "binding"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWaterMarkSetBinding;->O8o08O8O:Lcom/intsig/document/widget/DocumentView;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->Close()V

    .line 14
    .line 15
    .line 16
    invoke-super {p0}, Lcom/intsig/app/BaseDialogFragment;->onDestroyView()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public final 〇o08()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/intsig/document/widget/PagesView$WatermarkArgs;",
            "Ljava/lang/Long;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/WaterMarkSetDialog;->〇08O〇00〇o:Lkotlin/jvm/functions/Function2;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

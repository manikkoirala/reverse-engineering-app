.class public final Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "FileTypeAndTagFilterDialog.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;,
        Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$WhenMappings;,
        Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO〇00〇8oO:Ljava/lang/String;

.field public static final oOo0:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/office_doc/data/SelectType;

.field private OO:Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

.field private o0:Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;

.field private oOo〇8o008:Ljava/lang/String;

.field private o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

.field private 〇080OO8〇0:J

.field private 〇08O〇00〇o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo0:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 10
    .line 11
    const-wide/16 v0, -0x2

    .line 12
    .line 13
    iput-wide v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O0〇0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    instance-of v0, p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 8
    .line 9
    .line 10
    move-result-object p0

    .line 11
    const-string v0, "dialog.behavior"

    .line 12
    .line 13
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x3

    .line 17
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setDraggable(Z)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o00〇88〇08()V
    .locals 6

    .line 1
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->o0:Lkotlinx/coroutines/GlobalScope;

    .line 2
    .line 3
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    new-instance v3, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$getFileTypeData$1;

    .line 9
    .line 10
    const/4 v4, 0x0

    .line 11
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$getFileTypeData$1;-><init>(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lkotlin/coroutines/Continuation;)V

    .line 12
    .line 13
    .line 14
    const/4 v4, 0x2

    .line 15
    const/4 v5, 0x0

    .line 16
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇8〇OOoooo(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic oO〇oo(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;)Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO:Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇0〇o(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lkotlin/jvm/internal/Ref$IntRef;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 6

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$selectPos"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "<anonymous parameter 0>"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string p2, "view"

    .line 17
    .line 18
    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    sget-object p2, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 22
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v1, "item click position:"

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p3}, Landroid/view/View;->isSelected()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_5

    .line 48
    .line 49
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    instance-of v0, v0, Ljava/lang/Long;

    .line 54
    .line 55
    if-eqz v0, :cond_5

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-ge p4, v0, :cond_5

    .line 64
    .line 65
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const-string v1, "null cannot be cast to non-null type kotlin.Long"

    .line 70
    .line 71
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    check-cast v0, Ljava/lang/Long;

    .line 75
    .line 76
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 77
    .line 78
    .line 79
    move-result-wide v2

    .line 80
    iput-wide v2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 81
    .line 82
    iget v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 83
    .line 84
    const/4 v2, 0x2

    .line 85
    const/4 v3, 0x1

    .line 86
    if-ne v0, v2, :cond_3

    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 89
    .line 90
    invoke-virtual {p1, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    check-cast p1, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 95
    .line 96
    invoke-virtual {p1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇080()I

    .line 97
    .line 98
    .line 99
    move-result p1

    .line 100
    if-ne p1, v3, :cond_1

    .line 101
    .line 102
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 103
    .line 104
    .line 105
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 106
    .line 107
    if-eqz p0, :cond_0

    .line 108
    .line 109
    invoke-interface {p0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇o00〇〇Oo()V

    .line 110
    .line 111
    .line 112
    :cond_0
    const-string p0, "item click go2TagManage"

    .line 113
    .line 114
    invoke-static {p2, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    check-cast p1, Ljava/lang/Long;

    .line 126
    .line 127
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 128
    .line 129
    .line 130
    move-result-wide v4

    .line 131
    invoke-static {v4, v5}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O08o〇(J)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    check-cast p1, Ljava/lang/Long;

    .line 142
    .line 143
    const/4 p3, 0x0

    .line 144
    invoke-direct {p0, v3, p1, p3}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇088O(ZLjava/lang/Long;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 145
    .line 146
    .line 147
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 148
    .line 149
    if-eqz p1, :cond_2

    .line 150
    .line 151
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇080()V

    .line 152
    .line 153
    .line 154
    :cond_2
    const-string p1, "item click refreshFilterDoc"

    .line 155
    .line 156
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 160
    .line 161
    .line 162
    goto :goto_0

    .line 163
    :cond_3
    const-string p3, "item click select notify"

    .line 164
    .line 165
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 169
    .line 170
    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 171
    .line 172
    .line 173
    move-result-object p2

    .line 174
    check-cast p2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 175
    .line 176
    invoke-virtual {p2, v3}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇80〇808〇O(Z)V

    .line 177
    .line 178
    .line 179
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 180
    .line 181
    iget p3, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 182
    .line 183
    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object p2

    .line 187
    check-cast p2, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 188
    .line 189
    const/4 p3, 0x0

    .line 190
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇80〇808〇O(Z)V

    .line 191
    .line 192
    .line 193
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇0O:Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

    .line 194
    .line 195
    if-eqz p0, :cond_4

    .line 196
    .line 197
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 198
    .line 199
    .line 200
    :cond_4
    iput p4, p1, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 201
    .line 202
    :cond_5
    :goto_0
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final 〇088O(ZLjava/lang/Long;Lcom/intsig/camscanner/office_doc/data/SelectType;)V
    .locals 5

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "confirm"

    .line 7
    .line 8
    const-string v2, "CSDocClassifyAndLabel"

    .line 9
    .line 10
    const-string v3, "selected"

    .line 11
    .line 12
    if-eqz p1, :cond_7

    .line 13
    .line 14
    const-string p1, "1"

    .line 15
    .line 16
    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17
    .line 18
    .line 19
    if-eqz p2, :cond_0

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 28
    .line 29
    .line 30
    move-result-wide v3

    .line 31
    invoke-static {p1, v3, v4}, Lcom/intsig/camscanner/db/dao/TagDao;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const-string p2, "label"

    .line 36
    .line 37
    invoke-virtual {v0, p2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    .line 39
    .line 40
    :cond_0
    if-eqz p3, :cond_6

    .line 41
    .line 42
    sget-object p1, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$WhenMappings;->〇080:[I

    .line 43
    .line 44
    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    .line 45
    .line 46
    .line 47
    move-result p2

    .line 48
    aget p1, p1, p2

    .line 49
    .line 50
    const/4 p2, 0x1

    .line 51
    if-eq p1, p2, :cond_5

    .line 52
    .line 53
    const/4 p2, 0x2

    .line 54
    if-eq p1, p2, :cond_4

    .line 55
    .line 56
    const/4 p2, 0x3

    .line 57
    if-eq p1, p2, :cond_3

    .line 58
    .line 59
    const/4 p2, 0x4

    .line 60
    if-eq p1, p2, :cond_2

    .line 61
    .line 62
    const/4 p2, 0x5

    .line 63
    if-eq p1, p2, :cond_1

    .line 64
    .line 65
    const-string p1, "all_document"

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    const-string p1, "ppt"

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_2
    const-string p1, "excel"

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_3
    const-string p1, "word"

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_4
    const-string p1, "pdf"

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_5
    const-string p1, "scanned_document"

    .line 81
    .line 82
    :goto_0
    const-string p2, "type"

    .line 83
    .line 84
    invoke-virtual {v0, p2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 85
    .line 86
    .line 87
    :cond_6
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 88
    .line 89
    .line 90
    return-void

    .line 91
    :cond_7
    const-string p1, "0"

    .line 92
    .line 93
    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    .line 95
    .line 96
    invoke-static {v2, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic 〇80O8o8O〇(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->O0〇0(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8〇OOoooo(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "adapter"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "<anonymous parameter 1>"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    const-string p3, "null cannot be cast to non-null type com.intsig.camscanner.office_doc.data.FileFilterInfo"

    .line 25
    .line 26
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    check-cast p2, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;

    .line 30
    .line 31
    iget-object p3, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO:Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

    .line 32
    .line 33
    if-eqz p3, :cond_0

    .line 34
    .line 35
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;->〇o00〇〇Oo()Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;->ooO〇00O(Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 40
    .line 41
    .line 42
    :cond_0
    iget p3, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 43
    .line 44
    const/4 v0, 0x3

    .line 45
    if-ne p3, v0, :cond_1

    .line 46
    .line 47
    const-string p3, "type"

    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;->〇o〇()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const-string v2, "CSBackupList"

    .line 54
    .line 55
    const-string v3, "select_doc_type"

    .line 56
    .line 57
    invoke-static {v2, v3, p3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    :cond_1
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;->〇o00〇〇Oo()Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->O8o08O8O:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 68
    .line 69
    iget p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 70
    .line 71
    const/4 p3, 0x1

    .line 72
    if-eq p1, p3, :cond_2

    .line 73
    .line 74
    if-ne p1, v0, :cond_4

    .line 75
    .line 76
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 79
    .line 80
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;->〇o00〇〇Oo()Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇0OOo〇0(Ljava/lang/String;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 85
    .line 86
    .line 87
    const/4 p1, 0x0

    .line 88
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/data/FileFilterInfo;->〇o00〇〇Oo()Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    invoke-direct {p0, p3, p1, p2}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇088O(ZLjava/lang/Long;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 96
    .line 97
    if-eqz p1, :cond_3

    .line 98
    .line 99
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇080()V

    .line 100
    .line 101
    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 103
    .line 104
    .line 105
    :cond_4
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lkotlin/jvm/internal/Ref$IntRef;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇0〇o(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lkotlin/jvm/internal/Ref$IntRef;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final 〇o〇88〇8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o0:Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    const-string v3, "clFunctionBtn"

    .line 9
    .line 10
    const/4 v4, 0x0

    .line 11
    if-eq v1, v2, :cond_1

    .line 12
    .line 13
    const/4 v2, 0x2

    .line 14
    if-eq v1, v2, :cond_0

    .line 15
    .line 16
    const/4 v2, 0x3

    .line 17
    if-eq v1, v2, :cond_1

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇〇08O:Landroid/widget/TextView;

    .line 20
    .line 21
    const v1, 0x7f131442    # 1.955017E38f

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇〇08O:Landroid/widget/TextView;

    .line 33
    .line 34
    const v2, 0x7f1311bc

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 45
    .line 46
    const-string v2, "tvFileTag"

    .line 47
    .line 48
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 52
    .line 53
    .line 54
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 55
    .line 56
    const-string v2, "tvTagManage"

    .line 57
    .line 58
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 62
    .line 63
    .line 64
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 65
    .line 66
    const-string v2, "clFileClassification"

    .line 67
    .line 68
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 72
    .line 73
    .line 74
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 75
    .line 76
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-static {v0, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_1
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->oOo0:Landroid/widget/TextView;

    .line 84
    .line 85
    const-string v2, "tvFileClassification"

    .line 86
    .line 87
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 91
    .line 92
    .line 93
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 94
    .line 95
    const-string v2, "clFileTag"

    .line 96
    .line 97
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-static {v1, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 101
    .line 102
    .line 103
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 104
    .line 105
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-static {v0, v4}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 109
    .line 110
    .line 111
    :cond_2
    :goto_0
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇o0〇8()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const-string v1, "dialog_show_type"

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    iput v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 14
    .line 15
    const-string v1, "dialog_folder_sync_id"

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 22
    .line 23
    const-string v1, "tag_info_list"

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 34
    .line 35
    .line 36
    iget v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 37
    .line 38
    const/4 v1, 0x2

    .line 39
    if-ne v0, v1, :cond_0

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 42
    .line 43
    const-wide/16 v3, 0x0

    .line 44
    .line 45
    const-string v5, ""

    .line 46
    .line 47
    const/4 v6, 0x0

    .line 48
    const/4 v7, 0x4

    .line 49
    const/4 v8, 0x0

    .line 50
    move-object v2, v0

    .line 51
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;-><init>(JLjava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 52
    .line 53
    .line 54
    const/4 v1, 0x1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->o〇0(I)V

    .line 56
    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 59
    .line 60
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇o〇88〇8()V

    .line 64
    .line 65
    .line 66
    :cond_1
    return-void
    .line 67
    .line 68
.end method


# virtual methods
.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140192

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "dialog"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, p1, v0, v0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇088O(ZLjava/lang/Long;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const v1, 0x7f0a1836

    .line 21
    .line 22
    .line 23
    if-ne v0, v1, :cond_2

    .line 24
    .line 25
    sget-object p1, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 26
    .line 27
    const-string v0, "tv_tag_manage"

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 33
    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 36
    .line 37
    if-eqz p1, :cond_b

    .line 38
    .line 39
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇o00〇〇Oo()V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_4

    .line 43
    .line 44
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    const v1, 0x7f0a1733

    .line 52
    .line 53
    .line 54
    if-ne v0, v1, :cond_4

    .line 55
    .line 56
    sget-object p1, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 57
    .line 58
    const-string v0, "reset file type and tag"

    .line 59
    .line 60
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const-wide/16 v0, -0x2

    .line 64
    .line 65
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O08o〇(J)V

    .line 66
    .line 67
    .line 68
    sget-object p1, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 71
    .line 72
    sget-object v1, Lcom/intsig/camscanner/office_doc/data/SelectType;->ALL_DOC:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 73
    .line 74
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇0OOo〇0(Ljava/lang/String;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 78
    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 81
    .line 82
    if-eqz p1, :cond_b

    .line 83
    .line 84
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇080()V

    .line 85
    .line 86
    .line 87
    goto/16 :goto_4

    .line 88
    .line 89
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 90
    .line 91
    goto :goto_3

    .line 92
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    const v1, 0x7f0a181e

    .line 97
    .line 98
    .line 99
    if-ne v0, v1, :cond_9

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 102
    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->O8o08O8O:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 105
    .line 106
    if-eqz p1, :cond_6

    .line 107
    .line 108
    sget-object v0, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->〇080:Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;

    .line 109
    .line 110
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 111
    .line 112
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->o〇0OOo〇0(Ljava/lang/String;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 113
    .line 114
    .line 115
    :cond_6
    iget-wide v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 116
    .line 117
    const-wide/16 v2, 0x0

    .line 118
    .line 119
    cmp-long p1, v0, v2

    .line 120
    .line 121
    if-eqz p1, :cond_7

    .line 122
    .line 123
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O08o〇(J)V

    .line 124
    .line 125
    .line 126
    :cond_7
    sget-object p1, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 127
    .line 128
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->O8o08O8O:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 129
    .line 130
    iget-wide v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 131
    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    .line 133
    .line 134
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    .line 136
    .line 137
    const-string v4, "set file type = "

    .line 138
    .line 139
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    const-string v0, " and tag = "

    .line 146
    .line 147
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o〇00O:Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;

    .line 161
    .line 162
    if-eqz p1, :cond_8

    .line 163
    .line 164
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog$FileFilterCallback;->〇080()V

    .line 165
    .line 166
    .line 167
    :cond_8
    iget-wide v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 168
    .line 169
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->O8o08O8O:Lcom/intsig/camscanner/office_doc/data/SelectType;

    .line 174
    .line 175
    const/4 v1, 0x1

    .line 176
    invoke-direct {p0, v1, p1, v0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇088O(ZLjava/lang/Long;Lcom/intsig/camscanner/office_doc/data/SelectType;)V

    .line 177
    .line 178
    .line 179
    goto :goto_4

    .line 180
    :cond_9
    :goto_3
    if-nez p1, :cond_a

    .line 181
    .line 182
    goto :goto_4

    .line 183
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 184
    .line 185
    .line 186
    move-result p1

    .line 187
    const v0, 0x7f0a087d

    .line 188
    .line 189
    .line 190
    if-ne p1, v0, :cond_b

    .line 191
    .line 192
    sget-object p1, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO〇00〇8oO:Ljava/lang/String;

    .line 193
    .line 194
    const-string v0, "iv_close"

    .line 195
    .line 196
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 200
    .line 201
    .line 202
    :cond_b
    :goto_4
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d01c0

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o0:Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;

    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    new-instance p2, L〇〇O80〇0o/〇80〇808〇O;

    .line 16
    .line 17
    invoke-direct {p2}, L〇〇O80〇0o/〇80〇808〇O;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇〇o0〇8()V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o0:Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;

    .line 27
    .line 28
    if-eqz p1, :cond_7

    .line 29
    .line 30
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇080OO8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 31
    .line 32
    new-instance v0, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 33
    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const/4 v2, 0x3

    .line 39
    invoke-direct {v0, v1, v2}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 43
    .line 44
    .line 45
    new-instance p2, Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

    .line 46
    .line 47
    iget v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->oOo〇8o008:Ljava/lang/String;

    .line 50
    .line 51
    invoke-direct {p2, v0, v1}, Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;-><init>(ILjava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iput-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO:Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

    .line 55
    .line 56
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇080OO8〇0:Landroidx/recyclerview/widget/RecyclerView;

    .line 57
    .line 58
    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 59
    .line 60
    .line 61
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->OO:Lcom/intsig/camscanner/office_doc/dialog/FileFilterAdapter;

    .line 62
    .line 63
    if-eqz p2, :cond_1

    .line 64
    .line 65
    new-instance v0, L〇〇O80〇0o/OO0o〇〇〇〇0;

    .line 66
    .line 67
    invoke-direct {v0, p0}, L〇〇O80〇0o/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;)V

    .line 68
    .line 69
    .line 70
    invoke-virtual {p2, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 71
    .line 72
    .line 73
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->o00〇88〇08()V

    .line 74
    .line 75
    .line 76
    new-instance p2, Lcom/google/android/flexbox/FlexboxLayoutManager;

    .line 77
    .line 78
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    const/4 v1, 0x0

    .line 83
    const/4 v2, 0x1

    .line 84
    invoke-direct {p2, v0, v1, v2}, Lcom/google/android/flexbox/FlexboxLayoutManager;-><init>(Landroid/content/Context;II)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p2, v1}, Lcom/google/android/flexbox/FlexboxLayoutManager;->O0O8OO088(I)V

    .line 88
    .line 89
    .line 90
    new-instance v0, Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

    .line 91
    .line 92
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 93
    .line 94
    invoke-direct {v0, v3}, Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;-><init>(Ljava/util/List;)V

    .line 95
    .line 96
    .line 97
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇0O:Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

    .line 98
    .line 99
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇0O:Lcom/intsig/camscanner/view/MaxHeightRecyclerView;

    .line 100
    .line 101
    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 102
    .line 103
    .line 104
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇0O:Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

    .line 105
    .line 106
    invoke-virtual {v0, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 107
    .line 108
    .line 109
    iget p2, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇OOo8〇0:I

    .line 110
    .line 111
    if-nez p2, :cond_2

    .line 112
    .line 113
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇0O:Lcom/intsig/camscanner/view/MaxHeightRecyclerView;

    .line 114
    .line 115
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 116
    .line 117
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    const/16 v3, 0x104

    .line 122
    .line 123
    invoke-static {v0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/view/MaxHeightRecyclerView;->setMaxHeight(I)V

    .line 128
    .line 129
    .line 130
    :cond_2
    new-instance p2, Lkotlin/jvm/internal/Ref$IntRef;

    .line 131
    .line 132
    invoke-direct {p2}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 133
    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇0O:Lcom/intsig/camscanner/office_doc/dialog/TagFlexAdapter;

    .line 136
    .line 137
    if-eqz v0, :cond_3

    .line 138
    .line 139
    new-instance v3, L〇〇O80〇0o/〇8o8o〇;

    .line 140
    .line 141
    invoke-direct {v3, p0, p2}, L〇〇O80〇0o/〇8o8o〇;-><init>(Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;Lkotlin/jvm/internal/Ref$IntRef;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v3}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 145
    .line 146
    .line 147
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇08O〇00〇o:Ljava/util/ArrayList;

    .line 148
    .line 149
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    const/4 v3, 0x0

    .line 154
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 155
    .line 156
    .line 157
    move-result v4

    .line 158
    if-eqz v4, :cond_6

    .line 159
    .line 160
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 161
    .line 162
    .line 163
    move-result-object v4

    .line 164
    add-int/lit8 v5, v3, 0x1

    .line 165
    .line 166
    if-gez v3, :cond_4

    .line 167
    .line 168
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 169
    .line 170
    .line 171
    :cond_4
    check-cast v4, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;

    .line 172
    .line 173
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 174
    .line 175
    .line 176
    move-result-wide v6

    .line 177
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 178
    .line 179
    .line 180
    move-result-wide v8

    .line 181
    cmp-long v10, v6, v8

    .line 182
    .line 183
    if-nez v10, :cond_5

    .line 184
    .line 185
    invoke-virtual {v4}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇o〇()J

    .line 186
    .line 187
    .line 188
    move-result-wide v6

    .line 189
    iput-wide v6, p0, Lcom/intsig/camscanner/office_doc/dialog/FileTypeAndTagFilterDialog;->〇080OO8〇0:J

    .line 190
    .line 191
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇80〇808〇O(Z)V

    .line 192
    .line 193
    .line 194
    iput v3, p2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 195
    .line 196
    goto :goto_1

    .line 197
    :cond_5
    invoke-virtual {v4, v1}, Lcom/intsig/camscanner/mainmenu/docpage/tag/TagItem;->〇80〇808〇O(Z)V

    .line 198
    .line 199
    .line 200
    :goto_1
    move v3, v5

    .line 201
    goto :goto_0

    .line 202
    :cond_6
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 203
    .line 204
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    .line 206
    .line 207
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 208
    .line 209
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    .line 211
    .line 212
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 213
    .line 214
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    .line 216
    .line 217
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogFileTypeAndTagFilterBinding;->o〇00O:Landroid/widget/ImageView;

    .line 218
    .line 219
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    .line 221
    .line 222
    :cond_7
    return-void
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

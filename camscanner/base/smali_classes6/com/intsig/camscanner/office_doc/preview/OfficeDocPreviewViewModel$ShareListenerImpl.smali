.class public final Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;
.super Ljava/lang/Object;
.source "OfficeDocPreviewViewModel.kt"

# interfaces
.implements Landroidx/fragment/app/FragmentResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ShareListenerImpl"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic OO:Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;

.field private final o0:Landroidx/appcompat/app/AppCompatActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;Landroidx/appcompat/app/AppCompatActivity;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/appcompat/app/AppCompatActivity;",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "pageImageList"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->OO:Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;

    .line 12
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->o0:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->〇OOo8〇0:Ljava/util/List;

    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public onFragmentResult(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "requestKey"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "result"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p1, "select_type"

    .line 12
    .line 13
    invoke-virtual {p2, p1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    const-string v0, "select_component"

    .line 18
    .line 19
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/content/ComponentName;

    .line 24
    .line 25
    new-instance v1, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v2, "export listener dialog result: "

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    const-string v1, "OfficeDocPreviewViewModel"

    .line 43
    .line 44
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    if-eqz p1, :cond_2

    .line 48
    .line 49
    const/4 p2, 0x2

    .line 50
    if-eq p1, p2, :cond_1

    .line 51
    .line 52
    const/4 p2, 0x3

    .line 53
    if-eq p1, p2, :cond_0

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->OO:Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;

    .line 57
    .line 58
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->〇OOo8〇0:Ljava/util/List;

    .line 59
    .line 60
    invoke-static {p1, p2}, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;->oo88o8O(Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;Ljava/util/List;)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->OO:Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;

    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->〇OOo8〇0:Ljava/util/List;

    .line 67
    .line 68
    invoke-static {p1, v0, p2}, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;->〇oo〇(Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;Landroid/content/ComponentName;Ljava/util/List;)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->OO:Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;

    .line 73
    .line 74
    iget-object p2, p0, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel$ShareListenerImpl;->〇OOo8〇0:Ljava/util/List;

    .line 75
    .line 76
    invoke-static {p1, v0, p2}, Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;->〇00(Lcom/intsig/camscanner/office_doc/preview/OfficeDocPreviewViewModel;Landroid/content/ComponentName;Ljava/util/List;)V

    .line 77
    .line 78
    .line 79
    :goto_0
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

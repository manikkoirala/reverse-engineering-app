.class public final Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "PdfShareViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0O:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Ljava/lang/String;

.field private final OO:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Ljava/lang/Long;

.field private 〇080OO8〇0:Lcom/intsig/camscanner/share/ShareDataPresenter;

.field private final 〇08O〇00〇o:Landroidx/lifecycle/LiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/LiveData<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇0O:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 17
    .line 18
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇08O〇00〇o:Landroidx/lifecycle/LiveData;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇〇〇0〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇O〇oO(Landroidx/fragment/app/FragmentActivity;)V
    .locals 13

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇00(Landroidx/fragment/app/FragmentActivity;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_6

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇80〇808〇O()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto/16 :goto_3

    .line 16
    .line 17
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/share/channel/item/WxShareChannel;

    .line 18
    .line 19
    invoke-direct {v0}, Lcom/intsig/camscanner/share/channel/item/WxShareChannel;-><init>()V

    .line 20
    .line 21
    .line 22
    const v1, 0x7f080c52

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 26
    .line 27
    .line 28
    new-instance v1, Lcom/intsig/camscanner/share/channel/item/QqShareChannel;

    .line 29
    .line 30
    invoke-direct {v1}, Lcom/intsig/camscanner/share/channel/item/QqShareChannel;-><init>()V

    .line 31
    .line 32
    .line 33
    const v2, 0x7f080c3e

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 37
    .line 38
    .line 39
    new-instance v2, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;

    .line 40
    .line 41
    invoke-direct {v2}, Lcom/intsig/camscanner/share/channel/item/SendToPcShareChannel;-><init>()V

    .line 42
    .line 43
    .line 44
    const v3, 0x7f080c43

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 48
    .line 49
    .line 50
    new-instance v3, Lcom/intsig/camscanner/share/channel/item/EmailShareChannel;

    .line 51
    .line 52
    invoke-direct {v3}, Lcom/intsig/camscanner/share/channel/item/EmailShareChannel;-><init>()V

    .line 53
    .line 54
    .line 55
    const v4, 0x7f080c17

    .line 56
    .line 57
    .line 58
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 59
    .line 60
    .line 61
    new-instance v4, Lcom/intsig/camscanner/share/channel/item/MoreShareChannel;

    .line 62
    .line 63
    invoke-direct {v4}, Lcom/intsig/camscanner/share/channel/item/MoreShareChannel;-><init>()V

    .line 64
    .line 65
    .line 66
    const v5, 0x7f080c2f

    .line 67
    .line 68
    .line 69
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 70
    .line 71
    .line 72
    new-instance v5, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;

    .line 73
    .line 74
    invoke-direct {v5}, Lcom/intsig/camscanner/share/channel/item/LinkShareChannel;-><init>()V

    .line 75
    .line 76
    .line 77
    const v6, 0x7f080c07

    .line 78
    .line 79
    .line 80
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;->〇oOO8O8(I)V

    .line 81
    .line 82
    .line 83
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->o〇0()Z

    .line 84
    .line 85
    .line 86
    move-result v6

    .line 87
    const/4 v7, 0x5

    .line 88
    const/4 v8, 0x4

    .line 89
    const/4 v9, 0x3

    .line 90
    const/4 v10, 0x2

    .line 91
    const/4 v11, 0x0

    .line 92
    const/4 v12, 0x1

    .line 93
    if-eqz v6, :cond_1

    .line 94
    .line 95
    const/4 p1, 0x6

    .line 96
    new-array p1, p1, [Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 97
    .line 98
    aput-object v0, p1, v11

    .line 99
    .line 100
    aput-object v1, p1, v12

    .line 101
    .line 102
    aput-object v2, p1, v10

    .line 103
    .line 104
    aput-object v5, p1, v9

    .line 105
    .line 106
    aput-object v3, p1, v8

    .line 107
    .line 108
    aput-object v4, p1, v7

    .line 109
    .line 110
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    goto/16 :goto_2

    .line 115
    .line 116
    :cond_1
    new-array v0, v7, [Lcom/intsig/camscanner/share/channel/item/BaseShareChannel;

    .line 117
    .line 118
    sget-object v1, Lcom/intsig/camscanner/share/bean/ShareAppData;->o〇0:Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;->o〇0()Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 121
    .line 122
    .line 123
    move-result-object v6

    .line 124
    const v7, 0x7f080c58

    .line 125
    .line 126
    .line 127
    invoke-virtual {v6, v7}, Lcom/intsig/camscanner/share/bean/ShareAppData;->oO80(I)Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 128
    .line 129
    .line 130
    move-result-object v6

    .line 131
    invoke-virtual {v6}, Lcom/intsig/camscanner/share/bean/ShareAppData;->〇80〇808〇O()Lcom/intsig/camscanner/share/channel/item/DynamicShareChannel;

    .line 132
    .line 133
    .line 134
    move-result-object v6

    .line 135
    aput-object v6, v0, v11

    .line 136
    .line 137
    aput-object v2, v0, v12

    .line 138
    .line 139
    aput-object v5, v0, v10

    .line 140
    .line 141
    aput-object v3, v0, v9

    .line 142
    .line 143
    aput-object v4, v0, v8

    .line 144
    .line 145
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oooo8o0〇([Ljava/lang/Object;)Ljava/util/List;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    new-array v2, v8, [Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 150
    .line 151
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;->〇o00〇〇Oo()Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 152
    .line 153
    .line 154
    move-result-object v3

    .line 155
    const v4, 0x7f080c22

    .line 156
    .line 157
    .line 158
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/bean/ShareAppData;->oO80(I)Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    aput-object v3, v2, v11

    .line 163
    .line 164
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;->Oo08()Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 165
    .line 166
    .line 167
    move-result-object v3

    .line 168
    const v4, 0x7f080c50

    .line 169
    .line 170
    .line 171
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/bean/ShareAppData;->oO80(I)Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 172
    .line 173
    .line 174
    move-result-object v3

    .line 175
    aput-object v3, v2, v12

    .line 176
    .line 177
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;->〇080()Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 178
    .line 179
    .line 180
    move-result-object v3

    .line 181
    const v4, 0x7f080c1b

    .line 182
    .line 183
    .line 184
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/share/bean/ShareAppData;->oO80(I)Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 185
    .line 186
    .line 187
    move-result-object v3

    .line 188
    aput-object v3, v2, v10

    .line 189
    .line 190
    invoke-virtual {v1}, Lcom/intsig/camscanner/share/bean/ShareAppData$Companion;->O8()Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 191
    .line 192
    .line 193
    move-result-object v1

    .line 194
    const v3, 0x7f080c4c

    .line 195
    .line 196
    .line 197
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/share/bean/ShareAppData;->oO80(I)Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 198
    .line 199
    .line 200
    move-result-object v1

    .line 201
    aput-object v1, v2, v9

    .line 202
    .line 203
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 204
    .line 205
    .line 206
    move-result-object v1

    .line 207
    check-cast v1, Ljava/lang/Iterable;

    .line 208
    .line 209
    new-instance v2, Ljava/util/ArrayList;

    .line 210
    .line 211
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .line 213
    .line 214
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 215
    .line 216
    .line 217
    move-result-object v1

    .line 218
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 219
    .line 220
    .line 221
    move-result v3

    .line 222
    if-eqz v3, :cond_3

    .line 223
    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 225
    .line 226
    .line 227
    move-result-object v3

    .line 228
    move-object v4, v3

    .line 229
    check-cast v4, Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 230
    .line 231
    invoke-virtual {v4}, Lcom/intsig/camscanner/share/bean/ShareAppData;->〇〇888()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v4

    .line 235
    invoke-static {p1, v4}, Lcom/intsig/camscanner/app/AppUtil;->〇8〇0〇o〇O(Landroid/content/Context;Ljava/lang/String;)Z

    .line 236
    .line 237
    .line 238
    move-result v4

    .line 239
    if-eqz v4, :cond_2

    .line 240
    .line 241
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 242
    .line 243
    .line 244
    goto :goto_0

    .line 245
    :cond_3
    new-instance p1, Ljava/util/ArrayList;

    .line 246
    .line 247
    const/16 v1, 0xa

    .line 248
    .line 249
    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 250
    .line 251
    .line 252
    move-result v1

    .line 253
    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 254
    .line 255
    .line 256
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 257
    .line 258
    .line 259
    move-result-object v1

    .line 260
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 261
    .line 262
    .line 263
    move-result v2

    .line 264
    if-eqz v2, :cond_4

    .line 265
    .line 266
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 267
    .line 268
    .line 269
    move-result-object v2

    .line 270
    check-cast v2, Lcom/intsig/camscanner/share/bean/ShareAppData;

    .line 271
    .line 272
    invoke-virtual {v2}, Lcom/intsig/camscanner/share/bean/ShareAppData;->〇80〇808〇O()Lcom/intsig/camscanner/share/channel/item/DynamicShareChannel;

    .line 273
    .line 274
    .line 275
    move-result-object v2

    .line 276
    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 277
    .line 278
    .line 279
    goto :goto_1

    .line 280
    :cond_4
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 281
    .line 282
    .line 283
    move-result v1

    .line 284
    xor-int/2addr v1, v12

    .line 285
    if-eqz v1, :cond_5

    .line 286
    .line 287
    invoke-interface {v0, v12, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 288
    .line 289
    .line 290
    :cond_5
    move-object p1, v0

    .line 291
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 292
    .line 293
    new-instance v1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareLinkCardType;

    .line 294
    .line 295
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareLinkCardType;-><init>(Ljava/util/List;)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    .line 300
    .line 301
    :cond_6
    :goto_3
    return-void
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final oO(Landroidx/fragment/app/FragmentActivity;)V
    .locals 5

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇00(Landroidx/fragment/app/FragmentActivity;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->〇080:Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareLinkHelper;->O8()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇00O:Ljava/lang/Long;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareLinkType;

    .line 25
    .line 26
    const/4 v3, 0x1

    .line 27
    new-array v3, v3, [Ljava/lang/Long;

    .line 28
    .line 29
    const/4 v4, 0x0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    aput-object v0, v3, v4

    .line 35
    .line 36
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-static {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->O〇08(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const-string v0, "getLinkShareList(activity, arrayListOf(docId))"

    .line 45
    .line 46
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {v2, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareLinkType;-><init>(Ljava/util/List;)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 53
    .line 54
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    :cond_1
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic oo88o8O(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;Landroidx/fragment/app/FragmentActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->oO(Landroidx/fragment/app/FragmentActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo〇(Ljava/util/List;Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/type/ShareLongImage;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Landroidx/fragment/app/FragmentActivity;",
            ")",
            "Lcom/intsig/camscanner/share/type/ShareLongImage;"
        }
    .end annotation

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const-string v3, "page_num ASC"

    .line 10
    .line 11
    invoke-static {v2, p1, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->OOO(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 16
    .line 17
    .line 18
    move-result-wide v3

    .line 19
    sub-long/2addr v3, v0

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v1, "createShareLongImage costTime:"

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v1, "PdfShareViewModel"

    .line 38
    .line 39
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 43
    .line 44
    new-instance v5, Ljava/util/ArrayList;

    .line 45
    .line 46
    check-cast p1, Ljava/util/Collection;

    .line 47
    .line 48
    invoke-direct {v5, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 49
    .line 50
    .line 51
    const/4 v6, 0x0

    .line 52
    new-instance v7, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;

    .line 53
    .line 54
    invoke-direct {v7, p2, v2}, Lcom/intsig/camscanner/share/data_mode/LongImageShareData;-><init>(Landroid/app/Activity;Ljava/util/List;)V

    .line 55
    .line 56
    .line 57
    const/4 v8, 0x1

    .line 58
    move-object v3, v0

    .line 59
    move-object v4, p2

    .line 60
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/share/type/ShareLongImage;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/data_mode/LongImageShareData;Z)V

    .line 61
    .line 62
    .line 63
    return-object v0
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o〇0OOo〇0(Landroidx/fragment/app/FragmentActivity;)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    iget-object v2, v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇00O:Ljava/lang/Long;

    .line 6
    .line 7
    if-eqz v2, :cond_1

    .line 8
    .line 9
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    sget-object v4, Lcom/intsig/camscanner/util/PdfUtils;->〇080:Lcom/intsig/camscanner/util/PdfUtils;

    .line 14
    .line 15
    invoke-virtual {v4, v1, v2, v3}, Lcom/intsig/camscanner/util/PdfUtils;->〇00(Landroidx/fragment/app/FragmentActivity;J)Lcom/intsig/camscanner/share/type/SharePdf;

    .line 16
    .line 17
    .line 18
    move-result-object v6

    .line 19
    const v4, 0x7f080c36

    .line 20
    .line 21
    .line 22
    invoke-virtual {v6, v4}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 23
    .line 24
    .line 25
    iget-object v4, v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 26
    .line 27
    new-instance v11, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;

    .line 28
    .line 29
    const/4 v7, 0x1

    .line 30
    const/4 v8, 0x0

    .line 31
    const/4 v9, 0x4

    .line 32
    const/4 v10, 0x0

    .line 33
    move-object v5, v11

    .line 34
    invoke-direct/range {v5 .. v10}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;-><init>(Lcom/intsig/camscanner/share/type/BaseShare;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    new-instance v13, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 41
    .line 42
    const/4 v4, 0x1

    .line 43
    new-array v5, v4, [Ljava/lang/Long;

    .line 44
    .line 45
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 46
    .line 47
    .line 48
    move-result-object v6

    .line 49
    const/4 v7, 0x0

    .line 50
    aput-object v6, v5, v7

    .line 51
    .line 52
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    const/4 v6, 0x0

    .line 57
    invoke-direct {v13, v1, v5, v6}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 58
    .line 59
    .line 60
    iget-object v5, v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->O8o08O8O:Ljava/lang/String;

    .line 61
    .line 62
    if-eqz v5, :cond_0

    .line 63
    .line 64
    new-array v6, v4, [Ljava/lang/String;

    .line 65
    .line 66
    aput-object v5, v6, v7

    .line 67
    .line 68
    invoke-static {v6}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 69
    .line 70
    .line 71
    move-result-object v5

    .line 72
    invoke-virtual {v13, v5}, Lcom/intsig/camscanner/share/type/ShareToWord;->O〇Oo(Ljava/util/ArrayList;)V

    .line 73
    .line 74
    .line 75
    :cond_0
    const v5, 0x7f080c5e

    .line 76
    .line 77
    .line 78
    invoke-virtual {v13, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 79
    .line 80
    .line 81
    const v5, 0x7f080c5d

    .line 82
    .line 83
    .line 84
    invoke-virtual {v13, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 85
    .line 86
    .line 87
    const v5, 0x7f13085c

    .line 88
    .line 89
    .line 90
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 91
    .line 92
    .line 93
    move-result-object v5

    .line 94
    invoke-virtual {v13, v5}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    iget-object v5, v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 98
    .line 99
    new-instance v6, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;

    .line 100
    .line 101
    const/4 v14, 0x0

    .line 102
    const/4 v15, 0x0

    .line 103
    const/16 v16, 0x6

    .line 104
    .line 105
    const/16 v17, 0x0

    .line 106
    .line 107
    move-object v12, v6

    .line 108
    invoke-direct/range {v12 .. v17}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;-><init>(Lcom/intsig/camscanner/share/type/BaseShare;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    new-array v4, v4, [Ljava/lang/Long;

    .line 115
    .line 116
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 117
    .line 118
    .line 119
    move-result-object v2

    .line 120
    aput-object v2, v4, v7

    .line 121
    .line 122
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 123
    .line 124
    .line 125
    move-result-object v2

    .line 126
    invoke-direct {v0, v2, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->oo〇(Ljava/util/List;Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/type/ShareLongImage;

    .line 127
    .line 128
    .line 129
    move-result-object v4

    .line 130
    const v2, 0x7f080c2b

    .line 131
    .line 132
    .line 133
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O(I)V

    .line 134
    .line 135
    .line 136
    const v2, 0x7f080988

    .line 137
    .line 138
    .line 139
    invoke-virtual {v4, v2}, Lcom/intsig/camscanner/share/type/BaseShare;->oO00OOO(I)V

    .line 140
    .line 141
    .line 142
    const v2, 0x7f1307d1

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 146
    .line 147
    .line 148
    move-result-object v1

    .line 149
    invoke-virtual {v4, v1}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O〇80o08O(Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    iget-object v1, v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 153
    .line 154
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;

    .line 155
    .line 156
    const/4 v5, 0x0

    .line 157
    const/4 v6, 0x1

    .line 158
    const/4 v7, 0x2

    .line 159
    const/4 v8, 0x0

    .line 160
    move-object v3, v2

    .line 161
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;-><init>(Lcom/intsig/camscanner/share/type/BaseShare;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 162
    .line 163
    .line 164
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    .line 166
    .line 167
    :cond_1
    return-void
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;)Landroidx/lifecycle/MutableLiveData;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;Landroidx/fragment/app/FragmentActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇0OOo〇0(Landroidx/fragment/app/FragmentActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇oo〇(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;Landroidx/fragment/app/FragmentActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->O〇O〇oO(Landroidx/fragment/app/FragmentActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇0〇〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/share/ShareFreeWatermarkControl;->〇080:Lcom/intsig/camscanner/share/ShareFreeWatermarkControl;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareFreeWatermarkControl;->Oo08()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/ShareFreeWatermarkControl;->O8()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/CommonPdfShareType;

    .line 18
    .line 19
    const/4 v2, 0x2

    .line 20
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/CommonPdfShareType;-><init>(I)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O8ooOoo〇(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇OOo8〇0:Ljava/util/ArrayList;

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 7
    .line 8
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final O8〇o()Landroidx/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/LiveData<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇08O〇00〇o:Landroidx/lifecycle/LiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const-string v1, "docId"

    .line 5
    .line 6
    invoke-virtual {p1, v1}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;)J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    move-object v1, v0

    .line 16
    :goto_0
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇00O:Ljava/lang/Long;

    .line 17
    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    const-string v0, "docSyncId"

    .line 21
    .line 22
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :cond_1
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->O8o08O8O:Ljava/lang/String;

    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o〇8oOO88(Landroidx/fragment/app/FragmentActivity;)V
    .locals 7
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    new-instance v4, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$initShareTypes$1;

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    invoke-direct {v4, p0, p1, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel$initShareTypes$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;Landroidx/fragment/app/FragmentActivity;Lkotlin/coroutines/Continuation;)V

    .line 19
    .line 20
    .line 21
    const/4 v5, 0x2

    .line 22
    const/4 v6, 0x0

    .line 23
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o〇O()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v3

    .line 13
    const/4 v4, -0x1

    .line 14
    const/4 v5, 0x1

    .line 15
    if-eqz v3, :cond_2

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    check-cast v3, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;

    .line 22
    .line 23
    const/4 v6, 0x2

    .line 24
    invoke-interface {v3}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;->〇080()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    if-ne v6, v3, :cond_0

    .line 29
    .line 30
    const/4 v3, 0x1

    .line 31
    goto :goto_1

    .line 32
    :cond_0
    const/4 v3, 0x0

    .line 33
    :goto_1
    if-eqz v3, :cond_1

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    const/4 v2, -0x1

    .line 40
    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-eq v2, v4, :cond_3

    .line 49
    .line 50
    const/4 v1, 0x1

    .line 51
    :cond_3
    if-eqz v1, :cond_4

    .line 52
    .line 53
    goto :goto_3

    .line 54
    :cond_4
    const/4 v0, 0x0

    .line 55
    :goto_3
    if-eqz v0, :cond_5

    .line 56
    .line 57
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 58
    .line 59
    .line 60
    move-result v0

    .line 61
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 62
    .line 63
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->OO:Landroidx/lifecycle/MutableLiveData;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o0:Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 71
    .line 72
    .line 73
    :cond_5
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇00(Landroidx/fragment/app/FragmentActivity;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 6
    .line 7
    if-nez v1, :cond_1

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 10
    .line 11
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/share/ShareDataPresenter;-><init>(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 15
    .line 16
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇00O:Ljava/lang/Long;

    .line 17
    .line 18
    if-eqz p1, :cond_3

    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 21
    .line 22
    if-nez p1, :cond_2

    .line 23
    .line 24
    const-string p1, "mShareDataPresenter"

    .line 25
    .line 26
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/4 p1, 0x0

    .line 30
    :cond_2
    const/4 v1, 0x1

    .line 31
    new-array v2, v1, [Ljava/lang/Long;

    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareViewModel;->o〇00O:Ljava/lang/Long;

    .line 34
    .line 35
    aput-object v3, v2, v0

    .line 36
    .line 37
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/share/ShareDataPresenter;->O8(Ljava/util/ArrayList;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_3

    .line 46
    .line 47
    const/4 v0, 0x1

    .line 48
    :cond_3
    return v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

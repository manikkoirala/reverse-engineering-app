.class public final Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "PdfShareListProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o〇00O:Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;->o〇00O:Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;Lcom/intsig/camscanner/share/type/BaseShare;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;->〇oo〇(Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;Lcom/intsig/camscanner/share/type/BaseShare;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇oo〇(Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;Lcom/intsig/camscanner/share/type/BaseShare;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$share"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;->o〇00O:Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;

    .line 12
    .line 13
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;->O0oO0(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;

    .line 7
    .line 8
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string p2, "super.onCreateViewHolder\u2026arent, viewType).itemView"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d0795

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo88o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;)V
    .locals 6
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;

    .line 12
    .line 13
    if-eqz v0, :cond_18

    .line 14
    .line 15
    instance-of v0, p2, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto/16 :goto_c

    .line 20
    .line 21
    :cond_0
    check-cast p2, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;

    .line 22
    .line 23
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;->〇o00〇〇Oo()Lcom/intsig/camscanner/share/type/BaseShare;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast p1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O8ooOoo〇()Lcom/intsig/view/ImageViewDot;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-eqz v1, :cond_1

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇〇8O0〇8()I

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/view/SafeImageView;->setImageResource(I)V

    .line 40
    .line 41
    .line 42
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O8ooOoo〇()Lcom/intsig/view/ImageViewDot;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    if-eqz v1, :cond_2

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇0〇O0088o()Z

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {v1, v2}, Lcom/intsig/view/ImageViewDot;->Oo08(Z)V

    .line 53
    .line 54
    .line 55
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O8ooOoo〇()Lcom/intsig/view/ImageViewDot;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    if-nez v1, :cond_3

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_3
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 63
    .line 64
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    const v3, 0x7f060208

    .line 69
    .line 70
    .line 71
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 83
    .line 84
    .line 85
    move-result-object v1

    .line 86
    if-nez v1, :cond_4

    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_4
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->o〇O8〇〇o()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    .line 95
    .line 96
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇oo〇()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    const/4 v2, 0x1

    .line 101
    const/4 v3, 0x0

    .line 102
    if-eqz v1, :cond_6

    .line 103
    .line 104
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    if-nez v4, :cond_5

    .line 109
    .line 110
    goto :goto_2

    .line 111
    :cond_5
    const/4 v4, 0x0

    .line 112
    goto :goto_3

    .line 113
    :cond_6
    :goto_2
    const/4 v4, 0x1

    .line 114
    :goto_3
    if-nez v4, :cond_7

    .line 115
    .line 116
    instance-of v4, v0, Lcom/intsig/camscanner/share/type/SharePdf;

    .line 117
    .line 118
    if-eqz v4, :cond_7

    .line 119
    .line 120
    goto :goto_4

    .line 121
    :cond_7
    const/4 v2, 0x0

    .line 122
    :goto_4
    const/4 v4, 0x0

    .line 123
    if-eqz v2, :cond_8

    .line 124
    .line 125
    goto :goto_5

    .line 126
    :cond_8
    move-object v1, v4

    .line 127
    :goto_5
    if-eqz v1, :cond_b

    .line 128
    .line 129
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇0000OOO()Landroid/widget/TextView;

    .line 130
    .line 131
    .line 132
    move-result-object v2

    .line 133
    if-nez v2, :cond_9

    .line 134
    .line 135
    goto :goto_6

    .line 136
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    .line 137
    .line 138
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .line 140
    .line 141
    const-string v5, "("

    .line 142
    .line 143
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    const-string v1, ")"

    .line 150
    .line 151
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    .line 160
    .line 161
    :goto_6
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇0000OOO()Landroid/widget/TextView;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    if-nez v1, :cond_a

    .line 166
    .line 167
    goto :goto_7

    .line 168
    :cond_a
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .line 170
    .line 171
    :goto_7
    sget-object v4, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 172
    .line 173
    :cond_b
    const/16 v1, 0x8

    .line 174
    .line 175
    if-nez v4, :cond_d

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇0000OOO()Landroid/widget/TextView;

    .line 178
    .line 179
    .line 180
    move-result-object v2

    .line 181
    if-nez v2, :cond_c

    .line 182
    .line 183
    goto :goto_8

    .line 184
    :cond_c
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 185
    .line 186
    .line 187
    :cond_d
    :goto_8
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->o〇〇0〇()Landroid/widget/ImageView;

    .line 188
    .line 189
    .line 190
    move-result-object v2

    .line 191
    if-nez v2, :cond_e

    .line 192
    .line 193
    goto :goto_9

    .line 194
    :cond_e
    instance-of v4, v0, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 195
    .line 196
    if-eqz v4, :cond_f

    .line 197
    .line 198
    const/4 v1, 0x0

    .line 199
    :cond_f
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    .line 201
    .line 202
    :goto_9
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇00()Landroid/view/View;

    .line 203
    .line 204
    .line 205
    move-result-object v1

    .line 206
    if-eqz v1, :cond_11

    .line 207
    .line 208
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 209
    .line 210
    .line 211
    move-result-object v2

    .line 212
    if-eqz v2, :cond_10

    .line 213
    .line 214
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 215
    .line 216
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 217
    .line 218
    .line 219
    move-result-object v4

    .line 220
    const/16 v5, 0x3c

    .line 221
    .line 222
    invoke-static {v4, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 223
    .line 224
    .line 225
    move-result v4

    .line 226
    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 227
    .line 228
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 229
    .line 230
    .line 231
    goto :goto_a

    .line 232
    :cond_10
    new-instance p1, Ljava/lang/NullPointerException;

    .line 233
    .line 234
    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup.LayoutParams"

    .line 235
    .line 236
    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    throw p1

    .line 240
    :cond_11
    :goto_a
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;->〇o〇()Z

    .line 241
    .line 242
    .line 243
    move-result v1

    .line 244
    if-eqz v1, :cond_13

    .line 245
    .line 246
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇00()Landroid/view/View;

    .line 247
    .line 248
    .line 249
    move-result-object p2

    .line 250
    if-nez p2, :cond_12

    .line 251
    .line 252
    goto :goto_b

    .line 253
    :cond_12
    invoke-static {}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->oO80()Landroid/graphics/drawable/Drawable;

    .line 254
    .line 255
    .line 256
    move-result-object v1

    .line 257
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 258
    .line 259
    .line 260
    goto :goto_b

    .line 261
    :cond_13
    invoke-virtual {p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/PdfShareFileListType;->O8()Z

    .line 262
    .line 263
    .line 264
    move-result p2

    .line 265
    if-eqz p2, :cond_15

    .line 266
    .line 267
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇00()Landroid/view/View;

    .line 268
    .line 269
    .line 270
    move-result-object p2

    .line 271
    if-nez p2, :cond_14

    .line 272
    .line 273
    goto :goto_b

    .line 274
    :cond_14
    invoke-static {}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->〇o00〇〇Oo()Landroid/graphics/drawable/Drawable;

    .line 275
    .line 276
    .line 277
    move-result-object v1

    .line 278
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 279
    .line 280
    .line 281
    goto :goto_b

    .line 282
    :cond_15
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇00()Landroid/view/View;

    .line 283
    .line 284
    .line 285
    move-result-object p2

    .line 286
    if-nez p2, :cond_16

    .line 287
    .line 288
    goto :goto_b

    .line 289
    :cond_16
    invoke-static {}, Lcom/intsig/camscanner/share/ShareLinkOptimizationHelper;->Oo08()Landroid/graphics/drawable/Drawable;

    .line 290
    .line 291
    .line 292
    move-result-object v1

    .line 293
    invoke-virtual {p2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 294
    .line 295
    .line 296
    :goto_b
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->〇00()Landroid/view/View;

    .line 297
    .line 298
    .line 299
    move-result-object p2

    .line 300
    if-eqz p2, :cond_17

    .line 301
    .line 302
    new-instance v1, LO0O0〇/O8;

    .line 303
    .line 304
    invoke-direct {v1, p0, v0}, LO0O0〇/O8;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 305
    .line 306
    .line 307
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    .line 309
    .line 310
    :cond_17
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O888o0o()Ljava/lang/String;

    .line 311
    .line 312
    .line 313
    move-result-object p2

    .line 314
    if-eqz p2, :cond_18

    .line 315
    .line 316
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 317
    .line 318
    .line 319
    move-result-object p2

    .line 320
    if-eqz p2, :cond_18

    .line 321
    .line 322
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 323
    .line 324
    .line 325
    move-result-object p2

    .line 326
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 327
    .line 328
    .line 329
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 330
    .line 331
    .line 332
    move-result-object p2

    .line 333
    invoke-virtual {v0}, Lcom/intsig/camscanner/share/type/BaseShare;->〇O888o0o()Ljava/lang/String;

    .line 334
    .line 335
    .line 336
    move-result-object v0

    .line 337
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    .line 339
    .line 340
    invoke-static {}, Lcom/intsig/camscanner/share/view/ShareWatermarkUtil;->Oo08()I

    .line 341
    .line 342
    .line 343
    move-result p2

    .line 344
    const/4 v0, 0x3

    .line 345
    if-ne p2, v0, :cond_18

    .line 346
    .line 347
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider$ViewHolder;->O〇8O8〇008()Landroid/widget/TextView;

    .line 348
    .line 349
    .line 350
    move-result-object p1

    .line 351
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 352
    .line 353
    .line 354
    move-result-object p2

    .line 355
    const v0, 0x7f06027a

    .line 356
    .line 357
    .line 358
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 359
    .line 360
    .line 361
    move-result p2

    .line 362
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 363
    .line 364
    .line 365
    :cond_18
    :goto_c
    return-void
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;->oo88o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

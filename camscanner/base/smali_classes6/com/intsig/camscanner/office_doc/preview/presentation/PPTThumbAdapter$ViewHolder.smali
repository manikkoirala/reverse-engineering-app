.class public final Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "PPTThumbAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "bind(view)"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->〇OOo8〇0:Landroid/content/Context;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public final 〇00(Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;)V
    .locals 10
    .param p1    # Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    const-string v0, "item"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->o800o8O(Landroid/view/View;)Lcom/bumptech/glide/RequestManager;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;->〇o00〇〇Oo()Landroid/graphics/Bitmap;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Lcom/bumptech/glide/RequestBuilder;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 23
    .line 24
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 30
    .line 31
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->OO:Landroid/widget/TextView;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;->〇080()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    const/4 v2, 0x1

    .line 38
    add-int/2addr v1, v2

    .line 39
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    .line 45
    .line 46
    const/high16 v0, 0x40800000    # 4.0f

    .line 47
    .line 48
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;->〇o〇()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-eqz v1, :cond_0

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->〇OOo8〇0:Landroid/content/Context;

    .line 59
    .line 60
    const v3, 0x7f0601ee

    .line 61
    .line 62
    .line 63
    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    goto :goto_0

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->〇OOo8〇0:Landroid/content/Context;

    .line 69
    .line 70
    const v3, 0x7f060206

    .line 71
    .line 72
    .line 73
    invoke-static {v1, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 78
    .line 79
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->OO:Landroid/widget/TextView;

    .line 80
    .line 81
    const-string v4, "mBinding.tvIndex"

    .line 82
    .line 83
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    int-to-float v0, v0

    .line 87
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    .line 88
    .line 89
    invoke-direct {v4}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v4, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 93
    .line 94
    .line 95
    const/16 v5, 0x8

    .line 96
    .line 97
    new-array v6, v5, [Ljava/lang/Float;

    .line 98
    .line 99
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 100
    .line 101
    .line 102
    move-result-object v7

    .line 103
    const/4 v8, 0x0

    .line 104
    aput-object v7, v6, v8

    .line 105
    .line 106
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 107
    .line 108
    .line 109
    move-result-object v7

    .line 110
    aput-object v7, v6, v2

    .line 111
    .line 112
    const/4 v2, 0x2

    .line 113
    const/4 v7, 0x0

    .line 114
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 115
    .line 116
    .line 117
    move-result-object v9

    .line 118
    aput-object v9, v6, v2

    .line 119
    .line 120
    const/4 v2, 0x3

    .line 121
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 122
    .line 123
    .line 124
    move-result-object v9

    .line 125
    aput-object v9, v6, v2

    .line 126
    .line 127
    const/4 v2, 0x4

    .line 128
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 129
    .line 130
    .line 131
    move-result-object v9

    .line 132
    aput-object v9, v6, v2

    .line 133
    .line 134
    const/4 v2, 0x5

    .line 135
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    aput-object v0, v6, v2

    .line 140
    .line 141
    const/4 v0, 0x6

    .line 142
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 143
    .line 144
    .line 145
    move-result-object v2

    .line 146
    aput-object v2, v6, v0

    .line 147
    .line 148
    const/4 v0, 0x7

    .line 149
    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 150
    .line 151
    .line 152
    move-result-object v2

    .line 153
    aput-object v2, v6, v0

    .line 154
    .line 155
    invoke-static {v6}, Lkotlin/collections/ArraysKt;->o〇O([Ljava/lang/Float;)[F

    .line 156
    .line 157
    .line 158
    move-result-object v0

    .line 159
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 163
    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbData;->〇o〇()Z

    .line 166
    .line 167
    .line 168
    move-result p1

    .line 169
    if-eqz p1, :cond_1

    .line 170
    .line 171
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 172
    .line 173
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 174
    .line 175
    invoke-virtual {p1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 176
    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 179
    .line 180
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 181
    .line 182
    const-string v0, "mBinding.viewStroke"

    .line 183
    .line 184
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    const/high16 v0, 0x40000000    # 2.0f

    .line 188
    .line 189
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 190
    .line 191
    .line 192
    move-result v0

    .line 193
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    .line 194
    .line 195
    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 199
    .line 200
    .line 201
    const/high16 v0, 0x41700000    # 15.0f

    .line 202
    .line 203
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 207
    .line 208
    .line 209
    goto :goto_1

    .line 210
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/presentation/PPTThumbAdapter$ViewHolder;->o0:Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;

    .line 211
    .line 212
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemPptThumbBinding;->〇08O〇00〇o:Landroid/view/View;

    .line 213
    .line 214
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 215
    .line 216
    .line 217
    :goto_1
    return-void
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.class public final Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "PdfViewHostFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O0O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic ooo0〇〇O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

.field private final OO〇00〇8oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Z

.field private o〇00O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private final 〇080OO8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Z

.field private 〇0O:Z

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "PdfViewHostFragment::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 42
    .line 43
    const-string v0, "PDF"

    .line 44
    .line 45
    const-string v1, "Word"

    .line 46
    .line 47
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O:Ljava/util/ArrayList;

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$2;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 35
    .line 36
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$3;

    .line 41
    .line 42
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$4;

    .line 46
    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$5;

    .line 52
    .line 53
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 61
    .line 62
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$docType$2;

    .line 63
    .line 64
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$docType$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 65
    .line 66
    .line 67
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇080OO8〇0:Lkotlin/Lazy;

    .line 72
    .line 73
    const/4 v0, 0x1

    .line 74
    iput-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇0O:Z

    .line 75
    .line 76
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$isOpenBySystem$2;

    .line 77
    .line 78
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$isOpenBySystem$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 79
    .line 80
    .line 81
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->oOo0:Lkotlin/Lazy;

    .line 86
    .line 87
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mLoadingDialog$2;->o0:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mLoadingDialog$2;

    .line 88
    .line 89
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO〇00〇8oO:Lkotlin/Lazy;

    .line 94
    .line 95
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;

    .line 96
    .line 97
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 98
    .line 99
    .line 100
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;

    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O00OoO〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    .line 14
    .line 15
    :cond_1
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O0O0〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-eqz v0, :cond_2

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    const-wide/16 v2, 0x0

    .line 20
    .line 21
    cmp-long v4, v0, v2

    .line 22
    .line 23
    if-gtz v4, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->O8〇o()Lkotlinx/coroutines/flow/MutableStateFlow;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-interface {v0}, Lkotlinx/coroutines/flow/MutableStateFlow;->getValue()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Ljava/lang/Boolean;

    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-nez v0, :cond_3

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇O888o0o()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->oo88o8O(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    goto :goto_2

    .line 60
    :cond_2
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 61
    .line 62
    const-string v0, "docId == null || docId < 0"

    .line 63
    .line 64
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 68
    .line 69
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 70
    .line 71
    .line 72
    :cond_3
    :goto_2
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final O0〇()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇〇O〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "pdf_load_doc"

    .line 8
    .line 9
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-wide/16 v1, 0x5dc

    .line 13
    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 15
    .line 16
    .line 17
    new-instance v3, L〇O0o〇〇o/〇08O8o〇0;

    .line 18
    .line 19
    invoke-direct {v3, p0}, L〇O0o〇〇o/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇00O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇〇O〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;

    .line 30
    .line 31
    .line 32
    move-result-object v3

    .line 33
    const-string v4, "pdf_load_page"

    .line 34
    .line 35
    invoke-direct {v0, v3, v4}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 39
    .line 40
    .line 41
    new-instance v1, L〇O0o〇〇o/oO;

    .line 42
    .line 43
    invoke-direct {v1, p0}, L〇O0o〇〇o/oO;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 47
    .line 48
    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O8o08O8O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 50
    .line 51
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const-string v2, "getInstance()"

    .line 58
    .line 59
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 63
    .line 64
    .line 65
    const-class v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 66
    .line 67
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    check-cast v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initDatabaseCallbackViewModel$3;

    .line 82
    .line 83
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initDatabaseCallbackViewModel$3;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 84
    .line 85
    .line 86
    new-instance v3, L〇O0o〇〇o/〇8;

    .line 87
    .line 88
    invoke-direct {v3, v2}, L〇O0o〇〇o/〇8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 92
    .line 93
    .line 94
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO0O(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O88(Z)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 9
    .line 10
    const-string v2, "binding.toolbarTitle"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    const/4 v3, 0x0

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const/4 v3, 0x3

    .line 21
    :goto_0
    invoke-virtual {v1}, Landroidx/appcompat/widget/AppCompatTextView;->getText()Ljava/lang/CharSequence;

    .line 22
    .line 23
    .line 24
    move-result-object v4

    .line 25
    invoke-static {v1, v4, v3}, Lcom/intsig/utils/ToolbarUtils;->〇〇888(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 26
    .line 27
    .line 28
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 29
    .line 30
    const-string v3, "binding.tvSetTag"

    .line 31
    .line 32
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    xor-int/lit8 v3, p1, 0x1

    .line 36
    .line 37
    const/16 v4, 0x8

    .line 38
    .line 39
    if-eqz v3, :cond_2

    .line 40
    .line 41
    const/4 v3, 0x0

    .line 42
    goto :goto_1

    .line 43
    :cond_2
    const/16 v3, 0x8

    .line 44
    .line 45
    :goto_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    const-string v1, "binding.toolbarMenuNormal.root"

    .line 49
    .line 50
    const-string v3, "binding.toolbarMenuEdit.root"

    .line 51
    .line 52
    const-string v5, "binding.tabLayout"

    .line 53
    .line 54
    if-eqz p1, :cond_3

    .line 55
    .line 56
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 57
    .line 58
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->o〇00O:Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇080()Landroid/widget/TextView;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 74
    .line 75
    .line 76
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 77
    .line 78
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 86
    .line 87
    .line 88
    goto :goto_2

    .line 89
    :cond_3
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 90
    .line 91
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95
    .line 96
    .line 97
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->o〇00O:Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇080()Landroid/widget/TextView;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 107
    .line 108
    .line 109
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 110
    .line 111
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 119
    .line 120
    .line 121
    :goto_2
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final O880O〇(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/google/android/material/tabs/TabLayout;Lcom/google/android/material/tabs/TabLayout$Tab;I)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$tabLayout"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "tab"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const v1, 0x7f0d015b

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    const v0, 0x7f0a182a

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, Landroid/widget/TextView;

    .line 38
    .line 39
    const/16 v1, 0x11

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 42
    .line 43
    .line 44
    const/high16 v1, 0x41600000    # 14.0f

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 47
    .line 48
    .line 49
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 52
    .line 53
    .line 54
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O:Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object p3

    .line 60
    check-cast p3, Ljava/lang/CharSequence;

    .line 61
    .line 62
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    .line 64
    .line 65
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 66
    .line 67
    const p3, 0x7f060207

    .line 68
    .line 69
    .line 70
    invoke-static {p0, p3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 71
    .line 72
    .line 73
    move-result p0

    .line 74
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p2, p1}, Lcom/google/android/material/tabs/TabLayout$Tab;->setCustomView(Landroid/view/View;)Lcom/google/android/material/tabs/TabLayout$Tab;

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O8O(I)V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const-string v2, "viewLifecycleOwner"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x0

    .line 21
    new-instance v6, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$recordToRecentDocs$1;

    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    invoke-direct {v6, v0, p0, p1, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$recordToRecentDocs$1;-><init>(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;ILkotlin/coroutines/Continuation;)V

    .line 25
    .line 26
    .line 27
    const/4 v7, 0x3

    .line 28
    const/4 v8, 0x0

    .line 29
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O8〇8〇O80(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OO0O(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->〇oo()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO〇〇o0oO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->oOo0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic Ooo8o()Ljava/util/ArrayList;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OooO〇()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v8, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->OO〇00〇8oO:Landroidx/viewpager2/widget/ViewPager2;

    .line 9
    .line 10
    const-string v1, "binding.viewpager"

    .line 11
    .line 12
    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v9, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->OO:Lcom/google/android/material/tabs/TabLayout;

    .line 16
    .line 17
    const-string v1, "binding.tabLayout"

    .line 18
    .line 19
    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O〇8O8〇008()Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v10, 0x0

    .line 27
    if-eqz v1, :cond_1

    .line 28
    .line 29
    invoke-static {v9, v10}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 30
    .line 31
    .line 32
    const/4 v2, 0x0

    .line 33
    const/4 v3, 0x0

    .line 34
    const/4 v4, 0x0

    .line 35
    const/4 v5, 0x0

    .line 36
    const/16 v6, 0xd

    .line 37
    .line 38
    const/4 v7, 0x0

    .line 39
    move-object v1, v8

    .line 40
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O:Ljava/util/ArrayList;

    .line 44
    .line 45
    const-string v2, "Word"

    .line 46
    .line 47
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    if-eqz v3, :cond_1

    .line 52
    .line 53
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 54
    .line 55
    .line 56
    :cond_1
    invoke-virtual {v8, v10}, Landroidx/viewpager2/widget/ViewPager2;->setUserInputEnabled(Z)V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇〇O〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$1;

    .line 64
    .line 65
    invoke-direct {v2, p0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v8, v2}, Landroidx/viewpager2/widget/ViewPager2;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 69
    .line 70
    .line 71
    new-instance v1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$2;

    .line 72
    .line 73
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$2;-><init>(Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v8, v1}, Landroidx/viewpager2/widget/ViewPager2;->registerOnPageChangeCallback(Landroidx/viewpager2/widget/ViewPager2$OnPageChangeCallback;)V

    .line 77
    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/office_doc/util/CloudOfficeControl;->O〇8O8〇008()Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-eqz v0, :cond_2

    .line 84
    .line 85
    return-void

    .line 86
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$3;

    .line 87
    .line 88
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$initViewPager$3;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v9, v0}, Lcom/google/android/material/tabs/TabLayout;->addOnTabSelectedListener(Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;)V

    .line 92
    .line 93
    .line 94
    new-instance v0, Lcom/google/android/material/tabs/TabLayoutMediator;

    .line 95
    .line 96
    new-instance v1, L〇O0o〇〇o/O08000;

    .line 97
    .line 98
    invoke-direct {v1, p0, v9}, L〇O0o〇〇o/O08000;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/google/android/material/tabs/TabLayout;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {v0, v9, v8, v1}, Lcom/google/android/material/tabs/TabLayoutMediator;-><init>(Lcom/google/android/material/tabs/TabLayout;Landroidx/viewpager2/widget/ViewPager2;Lcom/google/android/material/tabs/TabLayoutMediator$TabConfigurationStrategy;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0}, Lcom/google/android/material/tabs/TabLayoutMediator;->attach()V

    .line 105
    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final O〇080〇o0(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o88()Ljava/lang/Long;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->〇8o〇〇8080(Ljava/lang/Long;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇0O〇Oo〇o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o88()Ljava/lang/Long;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->O〇8oOo8O(J)V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O〇8〇008()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇O888o0o()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O〇〇O80o8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇oO88o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0O0O〇〇〇0()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "viewLifecycleOwner"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    new-instance v5, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$subscribeUi$1;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$subscribeUi$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lkotlin/coroutines/Continuation;)V

    .line 20
    .line 21
    .line 22
    const/4 v6, 0x3

    .line 23
    const/4 v7, 0x0

    .line 24
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->〇o00〇〇Oo(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o0Oo()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->〇OOo8〇0:Landroidx/appcompat/widget/Toolbar;

    .line 9
    .line 10
    const-string v2, "binding.customToolbar"

    .line 11
    .line 12
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-virtual {v2, v1}, Landroidx/appcompat/app/AppCompatActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V

    .line 18
    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    invoke-virtual {v2}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    if-eqz v2, :cond_1

    .line 27
    .line 28
    const/4 v3, 0x1

    .line 29
    invoke-virtual {v2, v3}, Landroidx/appcompat/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 30
    .line 31
    .line 32
    :cond_1
    const v2, 0x7f080b93

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(I)V

    .line 36
    .line 37
    .line 38
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->〇0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 39
    .line 40
    new-instance v2, L〇O0o〇〇o/〇8〇0〇o〇O;

    .line 41
    .line 42
    invoke-direct {v2, p0}, L〇O0o〇〇o/〇8〇0〇o〇O;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    .line 47
    .line 48
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    const-string v2, "initToolbar$lambda$11"

    .line 55
    .line 56
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    const/4 v2, 0x0

    .line 60
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 61
    .line 62
    .line 63
    invoke-static {v1}, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 68
    .line 69
    new-instance v3, L〇O0o〇〇o/O〇O〇oO;

    .line 70
    .line 71
    invoke-direct {v3, p0}, L〇O0o〇〇o/O〇O〇oO;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    .line 76
    .line 77
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->OO:Landroid/widget/ImageView;

    .line 78
    .line 79
    new-instance v2, L〇O0o〇〇o/o8oO〇;

    .line 80
    .line 81
    invoke-direct {v2, p0}, L〇O0o〇〇o/o8oO〇;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->o〇00O:Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;

    .line 88
    .line 89
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇080()Landroid/widget/TextView;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    const-string v1, "initToolbar$lambda$14"

    .line 94
    .line 95
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    const/16 v1, 0x8

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    .line 102
    .line 103
    invoke-static {v0}, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 108
    .line 109
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 110
    .line 111
    const-string v3, "mActivity"

    .line 112
    .line 113
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    const v3, 0x7f0601ee

    .line 117
    .line 118
    .line 119
    invoke-static {v2, v3}, Lcom/intsig/utils/ext/ContextExtKt;->O8(Landroid/content/Context;I)I

    .line 120
    .line 121
    .line 122
    move-result v2

    .line 123
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    .line 125
    .line 126
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 127
    .line 128
    new-instance v1, L〇O0o〇〇o/o〇8oOO88;

    .line 129
    .line 130
    invoke-direct {v1, p0}, L〇O0o〇〇o/o〇8oOO88;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 131
    .line 132
    .line 133
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    .line 135
    .line 136
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o0〇〇00()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "extra_is_new_doc_first_view"

    .line 8
    .line 9
    const/4 v2, 0x0

    .line 10
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    xor-int/lit8 v0, v0, 0x1

    .line 15
    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->oOo〇8o008:Z

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private static final o808o8o08(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 v0, 0x1

    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->〇oo〇(I)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->Oooo8o0〇(Z)V

    .line 19
    .line 20
    .line 21
    const-string p0, "view_type"

    .line 22
    .line 23
    const-string p1, "pdf"

    .line 24
    .line 25
    invoke-static {p0, p1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object p0

    .line 33
    const-string p1, "edit"

    .line 34
    .line 35
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 36
    .line 37
    .line 38
    move-result-object p0

    .line 39
    const-string p1, "CSList"

    .line 40
    .line 41
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O〇0O〇Oo〇o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oOoO8OO〇(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O8o08O8O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇08oO80o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O8〇8〇O80(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇08oO80o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->O8〇o()Lkotlinx/coroutines/flow/MutableStateFlow;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-interface {p1}, Lkotlinx/coroutines/flow/MutableStateFlow;->getValue()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    check-cast p1, Ljava/lang/Boolean;

    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    sget-object p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 27
    .line 28
    const-string p1, "isEditMode, noClick"

    .line 29
    .line 30
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O08〇()V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O〇080〇o0(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇〇O〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇o08〇()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o0Oo()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OooO〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O0〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇08O()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o88()Ljava/lang/Long;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 12
    .line 13
    if-eqz v2, :cond_0

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v2, 0x0

    .line 21
    :goto_0
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 22
    .line 23
    .line 24
    move-result v3

    .line 25
    if-eqz v3, :cond_2

    .line 26
    .line 27
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const-string v5, "mActivity"

    .line 30
    .line 31
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-static {v4, v0, v1}, Lcom/intsig/camscanner/office_doc/util/OfficeUtils;->o0ooO(Landroid/content/Context;J)Z

    .line 35
    .line 36
    .line 37
    move-result v4

    .line 38
    if-eqz v4, :cond_1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    const/4 v4, 0x0

    .line 42
    goto :goto_2

    .line 43
    :cond_2
    :goto_1
    const/4 v4, 0x1

    .line 44
    :goto_2
    if-eqz v4, :cond_3

    .line 45
    .line 46
    sget-object v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 47
    .line 48
    new-instance v4, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v5, "needDownload pdfExist:"

    .line 54
    .line 55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇O0o〇〇o(J)V

    .line 69
    .line 70
    .line 71
    goto :goto_3

    .line 72
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->o8o〇〇0O(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :cond_4
    :goto_3
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0o88Oo〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->o〇00O:Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentEditActionbarPhoneBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇oOO80o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0ooOOo(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇O8〇8O0oO()Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8O0880(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O00OoO〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇00O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/google/android/material/tabs/TabLayout;Lcom/google/android/material/tabs/TabLayout$Tab;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O880O〇(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/google/android/material/tabs/TabLayout;Lcom/google/android/material/tabs/TabLayout$Tab;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O〇〇O80o8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O8〇8000(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇0o88Oo〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8〇8O0oO()Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO〇00〇8oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇OoO0o0(Ljava/lang/String;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_4

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz p1, :cond_1

    .line 13
    .line 14
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    if-nez v2, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v2, 0x0

    .line 22
    goto :goto_1

    .line 23
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 24
    :goto_1
    const/4 v3, 0x0

    .line 25
    if-eqz v2, :cond_3

    .line 26
    .line 27
    const p1, 0x7f130133

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 38
    .line 39
    const v2, 0x7f080e35

    .line 40
    .line 41
    .line 42
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    if-eqz p1, :cond_2

    .line 47
    .line 48
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    .line 53
    .line 54
    .line 55
    move-result v4

    .line 56
    invoke-virtual {p1, v1, v1, v2, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 57
    .line 58
    .line 59
    :cond_2
    const v1, 0x7f080227

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatTextView;->setBackgroundResource(I)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    const v2, 0x7f060206

    .line 70
    .line 71
    .line 72
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, v3, v3, p1, v3}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :cond_3
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v3, v3, v3, v3}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 87
    .line 88
    .line 89
    const p1, 0x7f0803fb

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setBackgroundResource(I)V

    .line 93
    .line 94
    .line 95
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    const v1, 0x7f060207

    .line 100
    .line 101
    .line 102
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 107
    .line 108
    .line 109
    :cond_4
    :goto_2
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇o08()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇oO88o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 4

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0O()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const/4 v0, 0x1

    .line 11
    const/4 v1, 0x0

    .line 12
    if-nez p1, :cond_0

    .line 13
    .line 14
    const-string p1, "grid"

    .line 15
    .line 16
    const/4 v2, 0x1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string p1, "big_image"

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->〇oo〇(I)V

    .line 26
    .line 27
    .line 28
    const/4 p0, 0x2

    .line 29
    new-array p0, p0, [Lkotlin/Pair;

    .line 30
    .line 31
    const-string v2, "view_type"

    .line 32
    .line 33
    const-string v3, "pdf"

    .line 34
    .line 35
    invoke-static {v2, v3}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    aput-object v2, p0, v1

    .line 40
    .line 41
    const-string v1, "type"

    .line 42
    .line 43
    invoke-static {v1, p1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    aput-object p1, p0, v0

    .line 48
    .line 49
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->〇O8o08O([Ljava/lang/Object;)Ljava/util/List;

    .line 50
    .line 51
    .line 52
    move-result-object p0

    .line 53
    const-string p1, "switch_display"

    .line 54
    .line 55
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    const-string p1, "CSList"

    .line 60
    .line 61
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇oOO80o(I)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 11
    .line 12
    const-string v1, "binding.toolbarMenuNormal.ivTopChangeMode"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    if-nez p1, :cond_1

    .line 18
    .line 19
    const p1, 0x7f080a81

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const p1, 0x7f080a80

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 30
    .line 31
    .line 32
    :goto_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇oO〇08o()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o0〇〇00()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const-string v1, "docItem"

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    check-cast v0, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0O0〇(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO〇〇o0oO()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v1, 0x1

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O8O(I)V

    .line 31
    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->〇080:Lcom/intsig/camscanner/transfer/CsTransferDocUtil;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->〇O〇(Z)V

    .line 37
    .line 38
    .line 39
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O0〇()V

    .line 40
    .line 41
    .line 42
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇08O()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇ooO8Ooo〇(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_1

    .line 6
    .line 7
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    goto :goto_1

    .line 16
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 17
    :goto_1
    if-nez v0, :cond_2

    .line 18
    .line 19
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇ooO〇000(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x3

    .line 23
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O8O(I)V

    .line 24
    .line 25
    .line 26
    :cond_2
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇ooO〇000(Ljava/lang/String;)V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->oo88o8O(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/launch/CsApplication;->oo〇()Lkotlinx/coroutines/CoroutineScope;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v4, 0x0

    .line 23
    new-instance v5, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$saveDocTitleToDbAsync$1;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    invoke-direct {v5, v0, p1, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$saveDocTitleToDbAsync$1;-><init>(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    const/4 v6, 0x3

    .line 30
    const/4 v7, 0x0

    .line 31
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O88(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o808o8o08(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇0(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇OoO0o0(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇00(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇ooO8Ooo〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇O〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O08〇()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->OO〇00〇8oO:Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 9
    .line 10
    .line 11
    move-result-wide v2

    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->O〇8〇008()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 17
    .line 18
    .line 19
    move-result-object v5

    .line 20
    const-string v6, "childFragmentManager"

    .line 21
    .line 22
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    sget-object v6, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 26
    .line 27
    new-instance v7, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$go2SetDocTitleOrTag$1;

    .line 28
    .line 29
    invoke-direct {v7, p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$go2SetDocTitleOrTag$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog$Companion;->〇080(JLjava/lang/String;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final OoO〇OOo8o(Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇oo()Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfViewHostBinding;->O8o08O8O:Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ActionbarMenuPageListBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    if-nez v0, :cond_1

    .line 18
    .line 19
    goto :goto_2

    .line 20
    :cond_1
    if-eqz p1, :cond_2

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_2
    const/16 v1, 0x8

    .line 25
    .line 26
    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    :goto_2
    xor-int/lit8 p1, p1, 0x1

    .line 30
    .line 31
    iput-boolean p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇08O〇00〇o:Z

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public enableToolbar()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇oO〇08o()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o〇o08〇()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o0O0O〇〇〇0()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public interceptBackPressed()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "interceptBackPressed"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇08O〇00〇o:Z

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    return v1

    .line 14
    :cond_0
    const-string v0, "view_type"

    .line 15
    .line 16
    const-string v2, "pdf"

    .line 17
    .line 18
    invoke-static {v0, v2}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v2, "back"

    .line 27
    .line 28
    invoke-static {v2, v0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v2, "CSList"

    .line 33
    .line 34
    invoke-static {v2, v0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->O8〇o()Lkotlinx/coroutines/flow/MutableStateFlow;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-interface {v0}, Lkotlinx/coroutines/flow/MutableStateFlow;->getValue()Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Ljava/lang/Boolean;

    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const/4 v2, 0x0

    .line 62
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->Oooo8o0〇(Z)V

    .line 63
    .line 64
    .line 65
    return v1

    .line 66
    :cond_1
    invoke-super {p0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->interceptBackPressed()Z

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    return v0
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final o88()Ljava/lang/Long;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->OO:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object p2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 5
    .line 6
    invoke-virtual {p2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-static {p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result p2

    .line 14
    sget-object p3, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇08O:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v1, "onActivityResult requestCode: "

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v1, ",  isLoginState: "

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-static {p3, v0}, Lcom/intsig/log/LogUtils;->〇〇888(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    const/16 p3, 0x4e21

    .line 45
    .line 46
    if-ne p1, p3, :cond_1

    .line 47
    .line 48
    if-eqz p2, :cond_0

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇08O()V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 55
    .line 56
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 57
    .line 58
    .line 59
    :cond_1
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment$mDownloadListener$1;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->〇80〇808〇O(Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncListener$DownloadListener;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇0O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->oOo〇8o008:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇0O:Z

    .line 14
    .line 15
    const-string v0, "0"

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const-string v0, ""

    .line 19
    .line 20
    :goto_0
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    const-string v2, "EXTRA_FROM_PART"

    .line 27
    .line 28
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    new-instance v2, Lorg/json/JSONObject;

    .line 33
    .line 34
    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v3, "newdoc"

    .line 38
    .line 39
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇Oo〇O()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v3, "type"

    .line 51
    .line 52
    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    .line 54
    .line 55
    const-string v0, "view_type"

    .line 56
    .line 57
    const-string v3, "pdf"

    .line 58
    .line 59
    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    .line 61
    .line 62
    if-eqz v1, :cond_1

    .line 63
    .line 64
    const-string v0, "from_part"

    .line 65
    .line 66
    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    .line 68
    .line 69
    :cond_1
    const-string v0, "CSList"

    .line 70
    .line 71
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/LogAgentExtKt;->Oo08(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->o88()Ljava/lang/Long;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->〇00(Ljava/lang/Long;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0321

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O0o〇〇o(J)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    const/16 p1, 0x4e21

    .line 14
    .line 15
    invoke-static {p0, p1}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->OO0o〇〇(Landroidx/fragment/app/Fragment;I)V

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/office/OfficeDocSyncManager;->〇o〇(Landroid/content/Context;J)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇O8〇8O0oO()Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    iget-object p2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    const/4 v0, 0x1

    .line 33
    new-array v0, v0, [I

    .line 34
    .line 35
    const/4 v1, 0x0

    .line 36
    const/16 v2, 0x10

    .line 37
    .line 38
    aput v2, v0, v1

    .line 39
    .line 40
    invoke-virtual {p1, p2, v0}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->O8(Landroid/content/Context;[I)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇O8〇8O0oO()Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    new-instance p2, L〇O0o〇〇o/〇〇0o;

    .line 48
    .line 49
    invoke-direct {p2, p0}, L〇O0o〇〇o/〇〇0o;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->oO80(Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$StatusListener;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇O8〇8O0oO()Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadingstyle/ProgressWithTipsFragment$TipsStrategy;->〇o00〇〇Oo()V

    .line 60
    .line 61
    .line 62
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇Oo〇O()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostFragment;->〇080OO8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

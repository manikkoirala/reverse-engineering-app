.class public abstract Lcom/intsig/camscanner/office_doc/preview/UIIntent;
.super Ljava/lang/Object;
.source "OfficeDocPreviewViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/UIIntent$SaveImage;,
        Lcom/intsig/camscanner/office_doc/preview/UIIntent$CovertPdf2Json;,
        Lcom/intsig/camscanner/office_doc/preview/UIIntent$StopCovertPdf2Json;,
        Lcom/intsig/camscanner/office_doc/preview/UIIntent$Export;,
        Lcom/intsig/camscanner/office_doc/preview/UIIntent$SaveImageByJson;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/UIIntent;-><init>()V

    return-void
.end method

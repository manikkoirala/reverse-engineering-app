.class public abstract Lcom/intsig/camscanner/office_doc/preview/UIState;
.super Ljava/lang/Object;
.source "OfficeDocPreviewViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/UIState$LoadingState;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$SaveImageState;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$LOADING;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$HIDE;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$CovertJsonLoadingState;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$CovertJsonResult;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$ConvertPdf;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$SaveAsOriginPdf;,
        Lcom/intsig/camscanner/office_doc/preview/UIState$ShareFile;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/UIState;-><init>()V

    return-void
.end method

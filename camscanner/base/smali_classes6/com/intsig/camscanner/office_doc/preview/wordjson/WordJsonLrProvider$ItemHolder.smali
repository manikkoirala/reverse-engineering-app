.class public final Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "WordJsonLrProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemHolder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO:Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇08O〇00〇o:I


# instance fields
.field private final o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/loadimage/PageImage;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->OO:Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder$Companion;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    div-int/lit8 v0, v0, 0x4

    .line 20
    .line 21
    sput v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇08O〇00〇o:I

    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "bind(itemView)"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 19
    .line 20
    invoke-virtual {p1, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 24
    .line 25
    const/4 v2, 0x0

    .line 26
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setScaleEnable(Z)V

    .line 27
    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 30
    .line 31
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setWordMarkVisible(Z)V

    .line 32
    .line 33
    .line 34
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 35
    .line 36
    const/4 v1, -0x1

    .line 37
    const/4 v2, -0x2

    .line 38
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    .line 43
    .line 44
    sget v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇08O〇00〇o:I

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇oOO8O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getMinimumHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const/4 v1, 0x1

    .line 16
    if-le v0, v1, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    sget v1, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇08O〇00〇o:I

    .line 25
    .line 26
    if-eq v0, v1, :cond_0

    .line 27
    .line 28
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getLayoutPosition()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v2, "reset item min height position: "

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    const-string v1, "WordJsonLrProvider"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 55
    .line 56
    const/4 v1, 0x0

    .line 57
    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumHeight(I)V

    .line 58
    .line 59
    .line 60
    :cond_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final O8ooOoo〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    const/16 v4, 0x8

    .line 8
    .line 9
    invoke-virtual {v1, v4, v2, v2, v3}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x2

    .line 13
    new-array v2, v1, [I

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 20
    .line 21
    .line 22
    const/4 v3, 0x1

    .line 23
    aget v2, v2, v3

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    div-int/2addr v3, v1

    .line 36
    if-le v2, v3, :cond_0

    .line 37
    .line 38
    return-void

    .line 39
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 40
    .line 41
    invoke-virtual {v1}, Landroid/view/View;->isFocused()Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-nez v1, :cond_1

    .line 46
    .line 47
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇oo〇()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    :cond_1
    const-string v1, "WordJsonLrProvider"

    .line 56
    .line 57
    const-string v2, "holder is focused hide soft input"

    .line 58
    .line 59
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    :cond_2
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 63
    .line 64
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇O8o08O()V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
.end method

.method public final O〇8O8〇008()Lcom/intsig/camscanner/loadimage/PageImage;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇OOo8〇0:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇(ZZ)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 4
    .line 5
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->OO:Landroid/widget/ImageView;

    .line 6
    .line 7
    const p2, 0x7f080e1a

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 16
    .line 17
    const p2, 0x7f130c61

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 25
    .line 26
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->OO:Landroid/widget/ImageView;

    .line 27
    .line 28
    const p2, 0x7f080e18

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 35
    .line 36
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 37
    .line 38
    const p2, 0x7f130af2

    .line 39
    .line 40
    .line 41
    invoke-static {p2}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->o0:Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇OOo8〇0:Lcom/intsig/camscanner/loadimage/PageImage;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonLrProvider$ItemHolder;->〇oOO8O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public final Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "Pdf2WordFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final OO〇00〇8oO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic oOo0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final oOo〇8o008:Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

.field private final 〇080OO8〇0:Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

.field private volatile 〇0O:Z

.field private 〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->oOo0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->oOo〇8o008:Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "Pdf2WordFragment::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$parentViewModel$2;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$parentViewModel$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$1;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v2, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 35
    .line 36
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    new-instance v3, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$2;

    .line 41
    .line 42
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v4, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$3;

    .line 46
    .line 47
    invoke-direct {v4, v5, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 48
    .line 49
    .line 50
    new-instance v6, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$4;

    .line 51
    .line 52
    invoke-direct {v6, p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$special$$inlined$viewModels$default$4;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 53
    .line 54
    .line 55
    invoke-static {p0, v2, v3, v4, v6}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO:Lkotlin/Lazy;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$shareHelper$2;

    .line 62
    .line 63
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$shareHelper$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V

    .line 64
    .line 65
    .line 66
    invoke-static {v1, v0}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->O8o08O8O:Lkotlin/Lazy;

    .line 71
    .line 72
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;

    .line 73
    .line 74
    const/4 v1, 0x1

    .line 75
    invoke-direct {v0, v5, v1, v5}, Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;-><init>(Lcom/intsig/camscanner/pagelist/contract/WordListContract$View;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 76
    .line 77
    .line 78
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;

    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic O0〇0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final Ooo8o()V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "hideScanAnim"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇0O:Landroid/view/View;

    .line 16
    .line 17
    const-string v2, "binding.vTouch"

    .line 18
    .line 19
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/16 v2, 0x8

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    const/4 v4, 0x0

    .line 31
    invoke-virtual {v1, v2, v4, v4, v3}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const-string v2, "viewLifecycleOwner"

    .line 39
    .line 40
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-static {v1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    const/4 v6, 0x0

    .line 48
    const/4 v7, 0x0

    .line 49
    new-instance v8, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$hideScanAnim$1;

    .line 50
    .line 51
    invoke-direct {v8, v0, v4}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$hideScanAnim$1;-><init>(Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;Lkotlin/coroutines/Continuation;)V

    .line 52
    .line 53
    .line 54
    const/4 v9, 0x3

    .line 55
    const/4 v10, 0x0

    .line 56
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)Lcom/intsig/document/widget/DocumentView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oOoO8OO〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇〇〇00(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇O8OO()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->oOo0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇08O(Landroid/view/View;)V
    .locals 1

    .line 1
    sget-object p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "cover onClick"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0oO〇oo00(Lcom/intsig/document/widget/DocumentView;J)Lcom/intsig/document/widget/DocumentView;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const v1, 0x7f080bda

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    const v2, 0x7f060207

    .line 13
    .line 14
    .line 15
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {p1, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setFastScrollBar(Landroid/graphics/drawable/Drawable;I)V

    .line 20
    .line 21
    .line 22
    const v0, 0x7f0d0753

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->setTextSelectionBar(I)Lcom/intsig/document/widget/DocumentView;

    .line 26
    .line 27
    .line 28
    const v0, 0x7f0601e5

    .line 29
    .line 30
    .line 31
    const v1, 0x3e4ccccd    # 0.2f

    .line 32
    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const/4 v2, 0x3

    .line 45
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    const v2, 0x66aaaaaa

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, v2, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setSideBarStyle(III)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->build(Landroid/content/Context;)V

    .line 58
    .line 59
    .line 60
    const/high16 v0, 0x3f800000    # 1.0f

    .line 61
    .line 62
    const/high16 v1, 0x40400000    # 3.0f

    .line 63
    .line 64
    invoke-virtual {p1, v0, v1}, Lcom/intsig/document/widget/DocumentView;->setScaleRange(FF)V

    .line 65
    .line 66
    .line 67
    const/4 v0, 0x0

    .line 68
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->enableAnnotOperate(Z)Lcom/intsig/document/widget/DocumentView;

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->setPageBorderSize(I)V

    .line 72
    .line 73
    .line 74
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$setUpPdfView$1$1;

    .line 75
    .line 76
    invoke-direct {v0, p2, p3, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$setUpPdfView$1$1;-><init>(JLcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1, v0}, Lcom/intsig/document/widget/DocumentView;->SetActionListener(Lcom/intsig/document/widget/DocumentView$DocumentActionListener;)V

    .line 80
    .line 81
    .line 82
    return-object p1
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇0ooOOo()Lcom/intsig/camscanner/share/ShareHelper;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/share/ShareHelper;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8O0880(Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Landroid/view/View;Ljava/lang/String;)V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showScanAnim docSyncId == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 25
    .line 26
    .line 27
    new-instance v0, L〇O0o〇〇o/〇o00〇〇Oo;

    .line 28
    .line 29
    invoke-direct {v0}, L〇O0o〇〇o/〇o00〇〇Oo;-><init>()V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    .line 34
    .line 35
    invoke-static {p1}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    .line 36
    .line 37
    .line 38
    move-result p2

    .line 39
    if-eqz p2, :cond_0

    .line 40
    .line 41
    invoke-virtual {p1}, Landroid/view/View;->isLayoutRequested()Z

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    if-nez p2, :cond_0

    .line 46
    .line 47
    new-instance p2, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$2$1;

    .line 48
    .line 49
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$2$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setAnimationEndListener(Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView$AnimationEndListener;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 56
    .line 57
    .line 58
    move-result-object p2

    .line 59
    const-string v0, "viewLifecycleOwner"

    .line 60
    .line 61
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-static {p2}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    const/4 v2, 0x0

    .line 69
    const/4 v3, 0x0

    .line 70
    new-instance v4, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$2$2;

    .line 71
    .line 72
    const/4 p2, 0x0

    .line 73
    invoke-direct {v4, p0, p3, p1, p2}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$2$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Ljava/lang/String;Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Lkotlin/coroutines/Continuation;)V

    .line 74
    .line 75
    .line 76
    const/4 v5, 0x3

    .line 77
    const/4 v6, 0x0

    .line 78
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$$inlined$doOnLayout$1;

    .line 83
    .line 84
    invoke-direct {p2, p1, p0, p3}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$showScanAnim$$inlined$doOnLayout$1;-><init>(Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p1, p2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 88
    .line 89
    .line 90
    :goto_0
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final 〇8〇80o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    instance-of v1, v0, Landroid/view/ViewGroup;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    check-cast v0, Landroid/view/ViewGroup;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    return-void

    .line 14
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇〇O80〇0o()V

    .line 15
    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 18
    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 22
    .line 23
    .line 24
    :cond_2
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O0o〇〇o()V
    .locals 8

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "viewLifecycleOwner"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x0

    .line 16
    new-instance v5, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$subscribeUi$1;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment$subscribeUi$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Lkotlin/coroutines/Continuation;)V

    .line 20
    .line 21
    .line 22
    const/4 v6, 0x3

    .line 23
    const/4 v7, 0x0

    .line 24
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇O8oOo0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O8〇8000(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    const/4 v2, 0x1

    .line 9
    new-array v3, v2, [Ljava/lang/Long;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 12
    .line 13
    .line 14
    move-result-wide v4

    .line 15
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    const/4 v5, 0x0

    .line 20
    aput-object v4, v3, v5

    .line 21
    .line 22
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    const/4 v4, 0x0

    .line 27
    invoke-direct {v0, v1, v3, v4}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 28
    .line 29
    .line 30
    new-array v1, v2, [Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    aput-object p1, v1, v5

    .line 37
    .line 38
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/share/type/ShareToWord;->O〇Oo(Ljava/util/ArrayList;)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇0ooOOo()Lcom/intsig/camscanner/share/ShareHelper;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o08()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 9
    .line 10
    new-instance v1, L〇O0o〇〇o/〇080;

    .line 11
    .line 12
    invoke-direct {v1, p0}, L〇O0o〇〇o/〇080;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o〇88〇8()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 9
    .line 10
    if-eqz v1, :cond_4

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->O8()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-nez v1, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 20
    .line 21
    const-string v3, "binding.galaxyRv"

    .line 22
    .line 23
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;

    .line 27
    .line 28
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/pagelist/presenter/WordListPresenter;->O0〇OO8(Ljava/lang/String;)Ljava/io/File;

    .line 29
    .line 30
    .line 31
    move-result-object v3

    .line 32
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_2

    .line 37
    .line 38
    const/16 v0, 0x8

    .line 39
    .line 40
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 44
    .line 45
    const-string v1, "has shown anim, return"

    .line 46
    .line 47
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return-void

    .line 51
    :cond_2
    iget-boolean v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇0O:Z

    .line 52
    .line 53
    if-eqz v3, :cond_3

    .line 54
    .line 55
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 56
    .line 57
    const-string v1, "animated before"

    .line 58
    .line 59
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    return-void

    .line 63
    :cond_3
    const/4 v3, 0x1

    .line 64
    iput-boolean v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇0O:Z

    .line 65
    .line 66
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇0O:Landroid/view/View;

    .line 67
    .line 68
    const-string v3, "binding.vTouch"

    .line 69
    .line 70
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-direct {p0, v2, v0, v1}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇8O0880(Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Landroid/view/View;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    :cond_4
    :goto_0
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇〇O80〇0o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 6
    .line 7
    const-string v2, "releasePdfView"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->closeSearch()V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/document/widget/DocumentView;->Close()V

    .line 16
    .line 17
    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 20
    .line 21
    return-void
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;)Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->oOoO8OO〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇0()V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "loadDocumentView"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇O8OO()Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const-string v3, "binding.root"

    .line 20
    .line 21
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 25
    .line 26
    const/4 v4, 0x0

    .line 27
    if-nez v3, :cond_1

    .line 28
    .line 29
    const-string v2, "fistLoad"

    .line 30
    .line 31
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPdfToWordBinding;->o〇00O:Lcom/intsig/document/widget/DocumentView;

    .line 35
    .line 36
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    .line 41
    .line 42
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 46
    .line 47
    iput-object v2, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    const-string v1, "reLoad"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇〇O80〇0o()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 59
    .line 60
    .line 61
    new-instance v1, Lcom/intsig/document/widget/DocumentView;

    .line 62
    .line 63
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 64
    .line 65
    invoke-direct {v1, v3}, Lcom/intsig/document/widget/DocumentView;-><init>(Landroid/content/Context;)V

    .line 66
    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 69
    .line 70
    invoke-virtual {v2, v1, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 71
    .line 72
    .line 73
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇0〇0()Ljava/lang/Long;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    const/4 v3, 0x0

    .line 78
    if-eqz v2, :cond_3

    .line 79
    .line 80
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    .line 81
    .line 82
    .line 83
    move-result-wide v5

    .line 84
    const-wide/16 v7, 0x0

    .line 85
    .line 86
    cmp-long v9, v5, v7

    .line 87
    .line 88
    if-ltz v9, :cond_2

    .line 89
    .line 90
    const/4 v4, 0x1

    .line 91
    :cond_2
    if-eqz v4, :cond_3

    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_3
    move-object v2, v3

    .line 95
    :goto_1
    iget-object v4, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 96
    .line 97
    if-eqz v4, :cond_4

    .line 98
    .line 99
    invoke-virtual {v4}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    if-eqz v4, :cond_4

    .line 104
    .line 105
    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    if-eqz v5, :cond_4

    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_4
    move-object v4, v3

    .line 113
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    const-string v6, "docId == "

    .line 119
    .line 120
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v6, ",filePath == "

    .line 127
    .line 128
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object v5

    .line 138
    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    if-eqz v2, :cond_6

    .line 142
    .line 143
    if-nez v4, :cond_5

    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_5
    new-instance v5, Ljava/io/File;

    .line 147
    .line 148
    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {v5}, Ljava/io/File;->length()J

    .line 152
    .line 153
    .line 154
    move-result-wide v5

    .line 155
    new-instance v7, Ljava/lang/StringBuilder;

    .line 156
    .line 157
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .line 159
    .line 160
    const-string v8, "fileSize == "

    .line 161
    .line 162
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v5

    .line 172
    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 176
    .line 177
    .line 178
    move-result-wide v5

    .line 179
    invoke-direct {p0, v1, v5, v6}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇0oO〇oo00(Lcom/intsig/document/widget/DocumentView;J)Lcom/intsig/document/widget/DocumentView;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    invoke-virtual {v0, v4, v3}, Lcom/intsig/document/widget/DocumentView;->Open(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 187
    .line 188
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇08O〇00〇o:Lcom/intsig/document/widget/DocumentView;

    .line 189
    .line 190
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇o〇88〇8()V

    .line 191
    .line 192
    .line 193
    :cond_6
    :goto_3
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 7
    .line 8
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇O8〇8000(Lcom/intsig/camscanner/office_doc/data/OfficeDocData;)V

    .line 9
    .line 10
    .line 11
    const-string p0, "view_type"

    .line 12
    .line 13
    const-string p1, "pdf"

    .line 14
    .line 15
    invoke-static {p0, p1}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    const-string p1, "word_export"

    .line 24
    .line 25
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇080(Ljava/lang/String;Ljava/util/List;)Lkotlin/Pair;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    const-string p1, "CSList"

    .line 30
    .line 31
    invoke-static {p1, p0}, Lcom/intsig/camscanner/util/LogAgentExtKt;->〇o〇(Ljava/lang/String;Lkotlin/Pair;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const-string v0, "docItem"

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    check-cast p1, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇o08()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇O0o〇〇o()V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onDestroyView()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇〇O80〇0o()V

    .line 2
    .line 3
    .line 4
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->oOoO8OO〇()Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/preview/pdf/PdfViewHostViewModel;->O〇O〇oO()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->OO〇00〇8oO:Ljava/lang/String;

    .line 15
    .line 16
    const-string v1, "clearPdfView"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇8〇80o()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0320

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0〇0()Ljava/lang/Long;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/pdf/Pdf2WordFragment;->〇OOo8〇0:Lcom/intsig/camscanner/office_doc/data/OfficeDocData;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/office_doc/data/OfficeDocData;->〇o〇()J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

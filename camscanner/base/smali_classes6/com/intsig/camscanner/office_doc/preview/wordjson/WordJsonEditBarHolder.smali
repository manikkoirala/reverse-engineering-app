.class public final Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;
.super Ljava/lang/Object;
.source "WordJsonEditBarHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇80〇808〇O:Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:I

.field private Oo08:Z

.field private oO80:I

.field private o〇0:Z

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

.field private final 〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binding"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o〇:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

    .line 19
    .line 20
    new-instance p2, Landroidx/lifecycle/ViewModelProvider;

    .line 21
    .line 22
    invoke-direct {p2, p1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 23
    .line 24
    .line 25
    const-class p1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 26
    .line 27
    invoke-virtual {p2, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 32
    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇O00()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇8O0〇8()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic O8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇0〇O0088o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8ooOoo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    xor-int/lit8 v0, v0, 0x1

    .line 12
    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->o〇0:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 16
    .line 17
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->oOo0:Landroid/widget/TextView;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/high16 v0, 0x3f800000    # 1.0f

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const v0, 0x3e99999a    # 0.3f

    .line 25
    .line 26
    .line 27
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OO0o〇〇()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->Oo08:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "WordEditBarHolder"

    .line 7
    .line 8
    const-string v1, "click text menu cut"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_CUT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 28
    .line 29
    .line 30
    const/16 v2, 0x3fb

    .line 31
    .line 32
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->CUT:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->O8ooOoo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o〇:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final OoO8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oooo8o0〇()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->o〇0:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "click text menu paste"

    .line 7
    .line 8
    const-string v1, "WordEditBarHolder"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string v0, "paste text is empty, return!!"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->PASTE:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇8O8〇008(Lcom/intsig/camscanner/pic2word/lr/LrElement;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 2
    .line 3
    const-string v1, "WordEditBarHolder"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    move-object v0, p1

    .line 8
    check-cast v0, Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->o800o8O()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string p1, "lrText focus"

    .line 17
    .line 18
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇00(I)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/pic2word/lr/LrTable;

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrTable;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrTable;->oO()Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    const-string p1, "lrTable focus"

    .line 39
    .line 40
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x2

    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇00(I)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    const-string p1, "layout default"

    .line 49
    .line 50
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const/4 p1, 0x0

    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇00(I)V

    .line 55
    .line 56
    .line 57
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final o800o8O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇00(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00(I)V
    .locals 3

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->oO80:I

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "layoutType:"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "WordEditBarHolder"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    const/4 v1, 0x0

    .line 27
    const/16 v2, 0x8

    .line 28
    .line 29
    if-eq p1, v0, :cond_1

    .line 30
    .line 31
    const/4 v0, 0x2

    .line 32
    if-eq p1, v0, :cond_0

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->getRoot()Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 49
    .line 50
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->getRoot()Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 64
    .line 65
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 69
    .line 70
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 75
    .line 76
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->getRoot()Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 86
    .line 87
    .line 88
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 89
    .line 90
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 91
    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->O8ooOoo〇()V

    .line 94
    .line 95
    .line 96
    :goto_0
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇080(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->o800o8O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0〇O0088o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;Lcom/intsig/camscanner/pic2word/lr/LrElement;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->O〇8O8〇008(Lcom/intsig/camscanner/pic2word/lr/LrElement;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇oOO8O8(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O00()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initKeyboardListener$listener$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initKeyboardListener$listener$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener;->〇o〇(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇O888o0o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8o08O()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->Oo08:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "WordEditBarHolder"

    .line 7
    .line 8
    const-string v1, "click text menu copy"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_COPY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 28
    .line 29
    .line 30
    const/16 v2, 0x3fb

    .line 31
    .line 32
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->COPY:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->OoO8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oOO8O8(Z)V
    .locals 2

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->Oo08:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    const/high16 v1, 0x3f800000    # 1.0f

    .line 10
    .line 11
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 12
    .line 13
    .line 14
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 15
    .line 16
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 21
    .line 22
    const v1, 0x3e99999a    # 0.3f

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 26
    .line 27
    .line 28
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 29
    .line 30
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇oo〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/pic2word/view/LrTableEditTextView;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 14
    .line 15
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇8oOO88(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇〇888(Landroid/view/View;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public static synthetic 〇o〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇O888o0o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇8O0〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$1;

    .line 10
    .line 11
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V

    .line 12
    .line 13
    .line 14
    new-instance v3, Lo88/O〇8O8〇008;

    .line 15
    .line 16
    invoke-direct {v3, v2}, Lo88/O〇8O8〇008;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 29
    .line 30
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$2;

    .line 31
    .line 32
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$2;-><init>(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V

    .line 33
    .line 34
    .line 35
    new-instance v3, Lo88/O8ooOoo〇;

    .line 36
    .line 37
    invoke-direct {v3, v2}, Lo88/O8ooOoo〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$3;

    .line 52
    .line 53
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$3;-><init>(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V

    .line 54
    .line 55
    .line 56
    new-instance v3, Lo88/〇oOO8O8;

    .line 57
    .line 58
    invoke-direct {v3, v2}, Lo88/〇oOO8O8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇〇888:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->oo88o8O()Landroidx/lifecycle/MutableLiveData;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 71
    .line 72
    new-instance v2, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$4;

    .line 73
    .line 74
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder$initViewModel$4;-><init>(Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;)V

    .line 75
    .line 76
    .line 77
    new-instance v3, Lo88/〇0000OOO;

    .line 78
    .line 79
    invoke-direct {v3, v2}, Lo88/〇0000OOO;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88o8O(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const-string v0, "type"

    .line 11
    .line 12
    const-string v1, "edit"

    .line 13
    .line 14
    const-string v2, "CSEditWord"

    .line 15
    .line 16
    sparse-switch p1, :sswitch_data_0

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :sswitch_0
    const-string p1, "paste"

    .line 21
    .line 22
    invoke-static {v2, v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->Oooo8o0〇()V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :sswitch_1
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇oo〇()V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :sswitch_2
    const-string p1, "cut"

    .line 34
    .line 35
    invoke-static {v2, v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->OO0o〇〇()V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :sswitch_3
    const-string p1, "copy"

    .line 43
    .line 44
    invoke-static {v2, v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇O8o08O()V

    .line 48
    .line 49
    .line 50
    :goto_0
    return-void

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x7f0a0386 -> :sswitch_3
        0x7f0a0389 -> :sswitch_2
        0x7f0a07da -> :sswitch_1
        0x7f0a1681 -> :sswitch_0
    .end sparse-switch
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o〇O8〇〇o(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 7
    .line 8
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 9
    .line 10
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    .line 12
    .line 13
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 14
    .line 15
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->oOo0:Landroid/widget/TextView;

    .line 19
    .line 20
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇O〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇808〇()Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/wordjson/WordJsonEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordJsonEditBarBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

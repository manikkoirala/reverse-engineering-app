.class public final Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;
.super Landroid/widget/LinearLayout;
.source "OfficePreviewBottomView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;,
        Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:I

.field private OO:Lcom/intsig/utils/ClickLimit;

.field private o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

.field private o〇00O:Landroid/animation/ValueAnimator;

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

.field private 〇0O:Z

.field private 〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->oOo〇8o008:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->OO:Lcom/intsig/utils/ClickLimit;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final OO0o〇〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$hideWordContentOpView$lambda$5$$inlined$addListener$default$1;

    .line 17
    .line 18
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$hideWordContentOpView$lambda$5$$inlined$addListener$default$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->Oooo8o0〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Landroid/animation/ValueAnimator;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast p1, Ljava/lang/Integer;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 31
    .line 32
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->OO0o〇〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OoO8(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    const/4 p3, 0x1

    .line 2
    and-int/2addr p2, p3

    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0〇O0088o(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final Oooo8o0〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0O:Z

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "hideWordContentOpView: isEditView: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "OfficePreviewBottomView"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0O:Z

    .line 26
    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 34
    .line 35
    iget v2, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇080OO8〇0:I

    .line 36
    .line 37
    sub-int/2addr v1, v2

    .line 38
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 39
    .line 40
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    .line 42
    .line 43
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 44
    .line 45
    if-eqz p0, :cond_1

    .line 46
    .line 47
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 48
    .line 49
    if-eqz p0, :cond_1

    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 53
    .line 54
    .line 55
    :cond_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O〇8O8〇008(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V
    .locals 7

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$binding"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 12
    .line 13
    iget-object v1, p1, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 14
    .line 15
    const-string v2, "binding.viewWordContentOp"

    .line 16
    .line 17
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const/4 v3, 0x1

    .line 25
    const/4 v4, 0x0

    .line 26
    if-nez v1, :cond_0

    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v1, 0x0

    .line 31
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v6, "switchPreviewMode: wordContentOpVisible: "

    .line 37
    .line 38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    const-string v5, "OfficePreviewBottomView"

    .line 49
    .line 50
    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 54
    .line 55
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    if-nez p1, :cond_1

    .line 63
    .line 64
    goto :goto_1

    .line 65
    :cond_1
    const/4 v3, 0x0

    .line 66
    :goto_1
    if-nez v3, :cond_2

    .line 67
    .line 68
    iget p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 69
    .line 70
    iget v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇080OO8〇0:I

    .line 71
    .line 72
    sub-int v0, p1, v0

    .line 73
    .line 74
    :cond_2
    const-wide/16 v1, 0x96

    .line 75
    .line 76
    invoke-direct {p0, v4, v0, v1, v2}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇80〇808〇O(IIJ)V

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic oO80(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->oo88o8O(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final oo88o8O(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 10
    .line 11
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 12
    .line 13
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 17
    .line 18
    if-eqz p0, :cond_1

    .line 19
    .line 20
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 21
    .line 22
    if-eqz p0, :cond_1

    .line 23
    .line 24
    const/4 v0, 0x1

    .line 25
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 26
    .line 27
    .line 28
    :cond_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O〇8O8〇008(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇O8〇〇o(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$binding"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "OfficePreviewBottomView"

    .line 12
    .line 13
    const-string v1, "switchEditMode: "

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 19
    .line 20
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 21
    .line 22
    const-string v1, "binding.viewWordContentOp"

    .line 23
    .line 24
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    const/4 v1, 0x0

    .line 32
    if-nez p1, :cond_0

    .line 33
    .line 34
    const/4 p1, 0x1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 p1, 0x0

    .line 37
    :goto_0
    if-nez p1, :cond_1

    .line 38
    .line 39
    iget p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 40
    .line 41
    iget v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇080OO8〇0:I

    .line 42
    .line 43
    sub-int v0, p1, v0

    .line 44
    .line 45
    :cond_1
    const-wide/16 v2, 0xfa

    .line 46
    .line 47
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇80〇808〇O(IIJ)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final varargs setSomeOnClickListeners([Landroid/view/View;)V
    .locals 3

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_1

    .line 4
    .line 5
    aget-object v2, p1, v1

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇O〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇80〇808〇O(IIJ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 6
    .line 7
    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 13
    .line 14
    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 16
    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 20
    .line 21
    .line 22
    :cond_2
    const/4 v0, 0x2

    .line 23
    new-array v0, v0, [I

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    aput p1, v0, v1

    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    aput p2, v0, p1

    .line 30
    .line 31
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    new-instance p2, Lo〇oo/〇00;

    .line 36
    .line 37
    invoke-direct {p2, p0}, Lo〇oo/〇00;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 47
    .line 48
    .line 49
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇O00(Landroid/content/Context;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2
    .line 3
    const/4 v1, -0x1

    .line 4
    const/4 v2, -0x2

    .line 5
    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f0d0620

    .line 9
    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    invoke-static {p1, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 29
    .line 30
    if-eqz p1, :cond_0

    .line 31
    .line 32
    const/4 v0, 0x4

    .line 33
    new-array v0, v0, [Landroid/view/View;

    .line 34
    .line 35
    const/4 v1, 0x0

    .line 36
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x1

    .line 41
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/4 v1, 0x2

    .line 46
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 47
    .line 48
    aput-object v2, v0, v1

    .line 49
    .line 50
    const/4 v1, 0x3

    .line 51
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 52
    .line 53
    aput-object p1, v0, v1

    .line 54
    .line 55
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 56
    .line 57
    .line 58
    :cond_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇O888o0o(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$showWordContentOpView$lambda$8$$inlined$addListener$default$1;

    .line 17
    .line 18
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$showWordContentOpView$lambda$8$$inlined$addListener$default$1;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->oo88o8O(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final 〇O〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->O8o08O8O:I

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    iput v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇080OO8〇0:I

    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    iget p0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇080OO8〇0:I

    .line 33
    .line 34
    new-instance v1, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v2, "initEditModeView: height: "

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v0, ", wordOpHeight: "

    .line 48
    .line 49
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    const-string v0, "OfficePreviewBottomView"

    .line 60
    .line 61
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇O888o0o(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇808〇(Landroid/content/Context;)V
    .locals 4

    .line 1
    const v0, 0x7f0d0623

    .line 2
    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 10
    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-static {v0}, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iput-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 19
    .line 20
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 21
    .line 22
    .line 23
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 24
    .line 25
    const/4 v2, -0x1

    .line 26
    const/4 v3, -0x2

    .line 27
    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 34
    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    return-void

    .line 38
    :cond_1
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 39
    .line 40
    const-string v2, "binding.llExport"

    .line 41
    .line 42
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const v2, 0x7f0601dc

    .line 46
    .line 47
    .line 48
    invoke-static {p1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    const/high16 v3, 0x40800000    # 4.0f

    .line 53
    .line 54
    invoke-static {p1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    .line 59
    .line 60
    invoke-direct {v3}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 70
    .line 71
    .line 72
    const/4 p1, 0x4

    .line 73
    new-array p1, p1, [Landroid/view/View;

    .line 74
    .line 75
    const/4 v1, 0x0

    .line 76
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 77
    .line 78
    aput-object v2, p1, v1

    .line 79
    .line 80
    const/4 v1, 0x1

    .line 81
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 82
    .line 83
    aput-object v2, p1, v1

    .line 84
    .line 85
    const/4 v1, 0x2

    .line 86
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 87
    .line 88
    aput-object v2, p1, v1

    .line 89
    .line 90
    const/4 v1, 0x3

    .line 91
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 92
    .line 93
    aput-object v0, p1, v1

    .line 94
    .line 95
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 96
    .line 97
    .line 98
    new-instance p1, Lo〇oo/O8ooOoo〇;

    .line 99
    .line 100
    invoke-direct {p1, p0}, Lo〇oo/O8ooOoo〇;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->Oooo8o0〇(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final getWordContentOpView()Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇0O:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o800o8O()V
    .locals 2

    .line 1
    const-string v0, "OfficePreviewBottomView"

    .line 2
    .line 3
    const-string v1, "showWordContentOpView "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lo〇oo/O〇8O8〇008;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lo〇oo/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->OO:Lcom/intsig/utils/ClickLimit;

    .line 5
    .line 6
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x0

    .line 18
    const/4 v2, 0x1

    .line 19
    sparse-switch v0, :sswitch_data_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :sswitch_0
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 24
    .line 25
    if-eqz p1, :cond_3

    .line 26
    .line 27
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->〇o00〇〇Oo(Z)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :sswitch_1
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 32
    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->〇o00〇〇Oo(Z)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :sswitch_2
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 40
    .line 41
    if-eqz p1, :cond_3

    .line 42
    .line 43
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->O8(Z)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :sswitch_3
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 48
    .line 49
    if-eqz p1, :cond_3

    .line 50
    .line 51
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->〇o〇()V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 56
    .line 57
    if-eqz p1, :cond_3

    .line 58
    .line 59
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->〇080()V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :sswitch_5
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 64
    .line 65
    if-eqz p1, :cond_3

    .line 66
    .line 67
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->O8(Z)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :sswitch_6
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 72
    .line 73
    if-eqz v0, :cond_3

    .line 74
    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    const v3, 0x7f0a027a

    .line 80
    .line 81
    .line 82
    if-ne p1, v3, :cond_2

    .line 83
    .line 84
    const/4 v1, 0x1

    .line 85
    :cond_2
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;->Oo08(Z)V

    .line 86
    .line 87
    .line 88
    :cond_3
    :goto_0
    return-void

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x7f0a027a -> :sswitch_6
        0x7f0a02a1 -> :sswitch_5
        0x7f0a02da -> :sswitch_4
        0x7f0a07de -> :sswitch_6
        0x7f0a07f2 -> :sswitch_3
        0x7f0a07f7 -> :sswitch_2
        0x7f0a07fc -> :sswitch_1
        0x7f0a0bf8 -> :sswitch_0
    .end sparse-switch
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 9
    .line 10
    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o〇00O:Landroid/animation/ValueAnimator;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 16
    .line 17
    .line 18
    :cond_1
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public final setDelegate(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "delegate"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇08O〇00〇o:Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView$BottomViewEventDelegate;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x0

    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0O:Z

    .line 8
    .line 9
    new-instance v1, Lo〇oo/o〇O8〇〇o;

    .line 10
    .line 11
    invoke-direct {v1, p0, v0}, Lo〇oo/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0〇O0088o(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 21
    .line 22
    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 28
    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setVipVisibility(Z)V

    .line 32
    .line 33
    .line 34
    :cond_2
    const/4 v0, 0x0

    .line 35
    if-eqz p1, :cond_4

    .line 36
    .line 37
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    const v1, 0x7f0812e6

    .line 42
    .line 43
    .line 44
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    .line 51
    .line 52
    .line 53
    move-result v1

    .line 54
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    const/4 v3, 0x0

    .line 59
    invoke-virtual {p1, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 60
    .line 61
    .line 62
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 63
    .line 64
    if-eqz v1, :cond_5

    .line 65
    .line 66
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 67
    .line 68
    if-eqz v1, :cond_5

    .line 69
    .line 70
    invoke-virtual {v1, v0, v0, p1, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 75
    .line 76
    if-eqz p1, :cond_5

    .line 77
    .line 78
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 79
    .line 80
    if-eqz p1, :cond_5

    .line 81
    .line 82
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 83
    .line 84
    .line 85
    :cond_5
    :goto_0
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇8o8o〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/OfficePreviewBottomBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()V
    .locals 2

    .line 1
    const-string v0, "OfficePreviewBottomView"

    .line 2
    .line 3
    const-string v1, "hideWordContentOpView: "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lo〇oo/oo88o8O;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lo〇oo/oo88o8O;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->o0:Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x1

    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇0O:Z

    .line 8
    .line 9
    new-instance v1, Lo〇oo/〇oo〇;

    .line 10
    .line 11
    invoke-direct {v1, p0, v0}, Lo〇oo/〇oo〇;-><init>(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;Lcom/intsig/camscanner/databinding/OfficePreviewWordEditBottomBinding;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇8O0〇8(ZZ)V
    .locals 1

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇〇808〇(Landroid/content/Context;)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->〇O00(Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    :goto_0
    if-eqz p2, :cond_1

    .line 27
    .line 28
    const/4 p1, 0x1

    .line 29
    const/4 p2, 0x0

    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-static {p0, v0, p1, p2}, Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;->OoO8(Lcom/intsig/camscanner/office_doc/preview/widget/OfficePreviewBottomView;ZILjava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public final Lcom/intsig/camscanner/office_doc/preview/pdf/share/PdfShareAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "PdfShareAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    const/4 v1, 0x1

    .line 8
    invoke-direct {p0, v0, v1, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 9
    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareRemoveWatermarkProvider;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareRemoveWatermarkProvider;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 17
    .line 18
    .line 19
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;

    .line 20
    .line 21
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareListProvider;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 25
    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareLinkCardProvider;

    .line 28
    .line 29
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareLinkCardProvider;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 33
    .line 34
    .line 35
    new-instance v0, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareLinkGridProvider;

    .line 36
    .line 37
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/provider/PdfShareLinkGridProvider;-><init>(Lcom/intsig/camscanner/office_doc/preview/pdf/share/IShareTypeClickCallback;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;

    .line 11
    .line 12
    invoke-interface {p1}, Lcom/intsig/camscanner/office_doc/preview/pdf/share/type/IPdfShareType;->〇080()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    return p1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

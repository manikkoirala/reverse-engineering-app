.class public Lcom/intsig/camscanner/openapi/OCROpenApiActivity;
.super Landroidx/fragment/app/FragmentActivity;
.source "OCROpenApiActivity.java"

# interfaces
.implements Lcom/intsig/camscanner/openapi/OpenApiPolicyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;,
        Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;,
        Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OnLineOcrOpenApiClient;
    }
.end annotation


# static fields
.field private static final o8o:Ljava/lang/String; = "OCROpenApiActivity"

.field private static final oo8ooo8O:[I


# instance fields
.field O0O:Landroid/net/Uri;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

.field private O8o08O8O:Z

.field private OO:Lcom/intsig/camscanner/openapi/ClientApp;

.field private OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/control/ProgressAnimHandler<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private o0:Ljava/lang/String;

.field o8oOOo:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

.field private oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

.field private oOo0:Landroid/widget/TextView;

.field private oOo〇8o008:Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;

.field private ooo0〇〇O:Z

.field private o〇00O:Ljava/lang/String;

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:J

.field private 〇0O:Z

.field private 〇8〇oO〇〇8o:I

.field private 〇OOo8〇0:Ljava/lang/String;

.field 〇O〇〇O8:Ljava/lang/String;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field 〇o0O:F

.field private 〇〇08O:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/4 v0, 0x5

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oo8ooo8O:[I

    .line 8
    .line 9
    return-void

    .line 10
    nop

    .line 11
    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/FragmentActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇OOo8〇0:Ljava/lang/String;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO:Lcom/intsig/camscanner/openapi/ClientApp;

    .line 8
    .line 9
    const-wide/16 v1, -0x1

    .line 10
    .line 11
    iput-wide v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 12
    .line 13
    const-string v1, "no word"

    .line 14
    .line 15
    iput-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o〇00O:Ljava/lang/String;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    iput-boolean v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O8o08O8O:Z

    .line 19
    .line 20
    const/4 v2, 0x1

    .line 21
    iput-boolean v2, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇080OO8〇0:Z

    .line 22
    .line 23
    iput-boolean v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇0O:Z

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo〇8o008:Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;

    .line 26
    .line 27
    iput v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇oO〇〇8o:I

    .line 28
    .line 29
    iput-boolean v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->ooo0〇〇O:Z

    .line 30
    .line 31
    new-instance v1, Landroid/os/Handler;

    .line 32
    .line 33
    new-instance v2, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$1;

    .line 34
    .line 35
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$1;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 39
    .line 40
    .line 41
    iput-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 42
    .line 43
    const/high16 v1, -0x40800000    # -1.0f

    .line 44
    .line 45
    iput v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇o0O:F

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$6;

    .line 50
    .line 51
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$6;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 52
    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O08〇()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/io/File;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, ".temp"

    .line 18
    .line 19
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :cond_0
    new-instance v1, Ljava/io/File;

    .line 27
    .line 28
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-nez v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 38
    .line 39
    .line 40
    :cond_1
    new-instance v0, Ljava/io/File;

    .line 41
    .line 42
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v2, "tempocr.txt"

    .line 47
    .line 48
    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    if-eqz v1, :cond_2

    .line 56
    .line 57
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 58
    .line 59
    .line 60
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇OOo8〇0:Ljava/lang/String;

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 67
    .line 68
    const/4 v1, 0x0

    .line 69
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 70
    .line 71
    .line 72
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O0O0〇(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    sget-object v1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O0〇0(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇8〇O80(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0O()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇080OO8〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0〇〇00()Lcom/intsig/app/AlertDialog;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8〇OO0〇0o:Lcom/intsig/app/AlertDialog;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    sget-object v1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic Ooo8o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇080〇o0(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "exitOnError errorCode="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0〇()V

    .line 24
    .line 25
    .line 26
    new-instance v0, Landroid/content/Intent;

    .line 27
    .line 28
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "RESPONSE_CODE"

    .line 32
    .line 33
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 34
    .line 35
    .line 36
    const/4 p1, 0x1

    .line 37
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇0O〇Oo〇o()V
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 6
    .line 7
    if-nez v1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇oO〇08o()Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iput-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 14
    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/ocrapi/LocalOcrClient;->o〇0(Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0:Ljava/lang/String;

    .line 25
    .line 26
    iget-wide v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 27
    .line 28
    const/4 v5, 0x0

    .line 29
    const/4 v6, 0x0

    .line 30
    const/4 v7, 0x0

    .line 31
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/ocrapi/LocalOcrClient;->〇00(Landroid/content/Context;Ljava/lang/String;J[Lcom/intsig/nativelib/Polygon;ZLcom/intsig/ocrapi/LocalOcrClient$MultimodalClassifyCallback;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O〇8〇008(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇oO88o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇〇O80o8()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "handleOcrLanguage mInputLanguage="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-wide v2, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 14
    .line 15
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v2, " mIsEnglisth="

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    iget-boolean v2, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O8o08O8O:Z

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    iget-wide v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 36
    .line 37
    const/4 v2, 0x1

    .line 38
    const-wide/16 v3, 0x0

    .line 39
    .line 40
    cmp-long v5, v0, v3

    .line 41
    .line 42
    if-lez v5, :cond_0

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/16 v5, 0x1f40

    .line 51
    .line 52
    cmp-long v6, v0, v3

    .line 53
    .line 54
    if-nez v6, :cond_2

    .line 55
    .line 56
    iget-boolean v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O8o08O8O:Z

    .line 57
    .line 58
    if-eqz v0, :cond_1

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 61
    .line 62
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_1
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇oO88o(I)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_2
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇oO88o(I)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o00〇88〇08(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0Oo()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0O:Landroid/net/Uri;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "initAfterAgreePolicy uri == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/16 v0, 0xfa6

    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇O888o0o(Landroid/app/Activity;)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v1, "api_ocr_hide"

    .line 31
    .line 32
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇8o8o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 36
    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v3, "callingPackage="

    .line 43
    .line 44
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string v3, " appKey="

    .line 51
    .line 52
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8oOOo:Ljava/lang/String;

    .line 56
    .line 57
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string v3, " mImagePath="

    .line 61
    .line 62
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    iget-object v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0:Ljava/lang/String;

    .line 66
    .line 67
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string v3, " mInputLanguage="

    .line 71
    .line 72
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    iget-wide v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 76
    .line 77
    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/ClientApp;->〇0000OOO(Landroid/content/Context;)V

    .line 92
    .line 93
    .line 94
    new-instance v0, Lcom/intsig/camscanner/openapi/ClientApp;

    .line 95
    .line 96
    iget-object v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8oOOo:Ljava/lang/String;

    .line 97
    .line 98
    iget-object v4, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 99
    .line 100
    iget v5, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇o0O:F

    .line 101
    .line 102
    const/4 v6, 0x1

    .line 103
    move-object v1, v0

    .line 104
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/openapi/ClientApp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FI)V

    .line 105
    .line 106
    .line 107
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO:Lcom/intsig/camscanner/openapi/ClientApp;

    .line 108
    .line 109
    invoke-virtual {v0}, Lcom/intsig/camscanner/openapi/ClientApp;->〇O00()Z

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    if-nez v0, :cond_1

    .line 114
    .line 115
    const/16 v0, 0xfa4

    .line 116
    .line 117
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 118
    .line 119
    .line 120
    return-void

    .line 121
    :cond_1
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    new-instance v1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$2;

    .line 126
    .line 127
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$2;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 128
    .line 129
    .line 130
    invoke-static {p0, v0, v1}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 131
    .line 132
    .line 133
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o0〇〇00()Lcom/intsig/app/AlertDialog;
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f0d061d

    .line 6
    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const v1, 0x7f0a1627

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Landroid/widget/TextView;

    .line 22
    .line 23
    iput-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo0:Landroid/widget/TextView;

    .line 24
    .line 25
    new-instance v1, Lcom/intsig/app/AlertDialog;

    .line 26
    .line 27
    invoke-direct {v1, p0}, Lcom/intsig/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog;->〇00〇8(Landroid/view/View;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo0:Landroid/widget/TextView;

    .line 40
    .line 41
    const/4 v2, 0x1

    .line 42
    new-array v2, v2, [Ljava/lang/Object;

    .line 43
    .line 44
    const-string v4, "0"

    .line 45
    .line 46
    aput-object v4, v2, v3

    .line 47
    .line 48
    const v3, 0x7f130168

    .line 49
    .line 50
    .line 51
    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    .line 57
    .line 58
    return-object v1
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o808o8o08()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f1300a9

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const v1, 0x7f130090

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    new-instance v1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$5;

    .line 26
    .line 27
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$5;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 28
    .line 29
    .line 30
    const v2, 0x7f13057e

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    new-instance v1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$4;

    .line 38
    .line 39
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$4;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 40
    .line 41
    .line 42
    const v2, 0x7f130074

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o88(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇〇O80o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o880(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/camscanner/openapi/ClientApp;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO:Lcom/intsig/camscanner/openapi/ClientApp;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oOoO8OO〇(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/camscanner/control/ProgressAnimHandler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oO〇oo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oooO888(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0〇o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O8o08O8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇O8OO(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo〇8o008:Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇oo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇088O(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇080OO8〇0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇08O(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo〇8o008:Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O8o08O8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0ooOOo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOo0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0〇0(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oOO〇〇:Lcom/intsig/camscanner/control/ProgressAnimHandler$ProgressAnimCallBack;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8O0880(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8〇80o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Lcom/intsig/camscanner/control/ProgressAnimHandler;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O8〇8000(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇Oo〇O(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇0O〇Oo〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o08(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oO88o(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x3

    .line 8
    iput v1, v0, Landroid/os/Message;->what:I

    .line 9
    .line 10
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oO〇08o()Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$3;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$3;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇o〇88〇8(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->ooo0〇〇O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇〇0(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇〇00(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->ooo0〇〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇〇O〇(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o808o8o08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public Oo0oO〇O〇O()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onPolicyDisagree"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/16 v0, 0xfab

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Ooo()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onPolicyAgree"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0Oo()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/4 p2, 0x1

    .line 5
    if-ne p2, p1, :cond_1

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO:Lcom/intsig/camscanner/openapi/ClientApp;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/openapi/ClientApp;->〇o()Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-static {p0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-nez p1, :cond_0

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o808o8o08()V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onBackPressed()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0〇()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇O8〇〇o()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇〇8O0〇8()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇00()V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/launch/CsApplication;->O8O〇(Z)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string v0, "mounted"

    .line 13
    .line 14
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 21
    .line 22
    new-instance v1, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v2, "MEDIA_MOUNTED state="

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/16 p1, 0xfa8

    .line 43
    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 45
    .line 46
    .line 47
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    if-nez v0, :cond_1

    .line 57
    .line 58
    return-void

    .line 59
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    const-string v1, "com.intsig.camscanner.ACTION_OCR"

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-nez v0, :cond_2

    .line 70
    .line 71
    return-void

    .line 72
    :cond_2
    const-string v0, "OCROpenApiActivity"

    .line 73
    .line 74
    invoke-static {v0}, Lcom/intsig/comm/util/AppLaunchSourceStatistic;->〇O8o08O(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const-string v0, "app_key"

    .line 78
    .line 79
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8oOOo:Ljava/lang/String;

    .line 84
    .line 85
    const-string v0, "sub_app_key"

    .line 86
    .line 87
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 92
    .line 93
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0O:Landroid/net/Uri;

    .line 98
    .line 99
    if-nez v0, :cond_3

    .line 100
    .line 101
    const-string v0, "android.intent.extra.STREAM"

    .line 102
    .line 103
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    check-cast v0, Landroid/net/Uri;

    .line 108
    .line 109
    iput-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0O:Landroid/net/Uri;

    .line 110
    .line 111
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O0O:Landroid/net/Uri;

    .line 112
    .line 113
    if-nez v0, :cond_4

    .line 114
    .line 115
    sget-object p1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 116
    .line 117
    const-string v0, "uri == null"

    .line 118
    .line 119
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    const/16 p1, 0xfa6

    .line 123
    .line 124
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇080〇o0(I)V

    .line 125
    .line 126
    .line 127
    return-void

    .line 128
    :cond_4
    const-string v0, "ocr_language"

    .line 129
    .line 130
    const/4 v1, -0x1

    .line 131
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 132
    .line 133
    .line 134
    move-result v0

    .line 135
    int-to-long v0, v0

    .line 136
    iput-wide v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇08O〇00〇o:J

    .line 137
    .line 138
    const-string v0, "ocr_show_progress"

    .line 139
    .line 140
    const/4 v1, 0x1

    .line 141
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    iput-boolean v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇080OO8〇0:Z

    .line 146
    .line 147
    const-string v0, "ocr_show_statusbar"

    .line 148
    .line 149
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 150
    .line 151
    .line 152
    move-result v0

    .line 153
    if-nez v0, :cond_5

    .line 154
    .line 155
    const/16 v0, 0x400

    .line 156
    .line 157
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 158
    .line 159
    .line 160
    move-result-object v1

    .line 161
    invoke-virtual {v1, v0, v0}, Landroid/view/Window;->setFlags(II)V

    .line 162
    .line 163
    .line 164
    :cond_5
    const-string v0, "api_version"

    .line 165
    .line 166
    const/high16 v1, -0x40800000    # -1.0f

    .line 167
    .line 168
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    .line 169
    .line 170
    .line 171
    move-result p1

    .line 172
    iput p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇o0O:F

    .line 173
    .line 174
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇〇8o()Z

    .line 175
    .line 176
    .line 177
    move-result p1

    .line 178
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 179
    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v2, "hasAgreedCSProtocolsForRCN="

    .line 186
    .line 187
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object p1

    .line 197
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    invoke-direct {p0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o0Oo()V

    .line 201
    .line 202
    .line 203
    return-void
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected onDestroy()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇08O:Landroid/os/Handler;

    .line 4
    .line 5
    sget-object v2, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->oo8ooo8O:[I

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/ocrapi/LocalOcrClient;->〇O8o08O()Lcom/intsig/ocrapi/LocalOcrClient;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/ocrapi/LocalOcrClient;->o〇O8〇〇o(Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;)V

    .line 22
    .line 23
    .line 24
    iput-object v3, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O88O:Lcom/intsig/ocrapi/LocalOcrClient$LocalOcrTaskCallback;

    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->o〇O8〇〇o()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇〇8O0〇8()V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->OO〇00〇8oO:Lcom/intsig/camscanner/control/ProgressAnimHandler;

    .line 42
    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ProgressAnimHandler;->〇00()V

    .line 44
    .line 45
    .line 46
    :cond_1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onDestroy()V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇0O:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    sget-object p1, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o8o:Ljava/lang/String;

    .line 15
    .line 16
    const-string p2, "onKeyDown mIsOcrProgressing=true"

    .line 17
    .line 18
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    return p1

    .line 23
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    return p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

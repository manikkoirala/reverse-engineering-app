.class Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;
.super Ljava/lang/Object;
.source "OCROpenApiActivity.java"

# interfaces
.implements Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OcrOpenApiClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/openapi/OCROpenApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OfflineOcrOpenApiClient"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;LO08〇/〇〇888;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;-><init>(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    return-void
.end method


# virtual methods
.method public 〇080()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "OfflineOcrOpenApiClient isOfflineKey"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o880(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)Lcom/intsig/camscanner/openapi/ClientApp;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 17
    .line 18
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/openapi/ClientApp;->oo〇(Landroid/content/Context;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_2

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 29
    .line 30
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)J

    .line 31
    .line 32
    .line 33
    move-result-wide v0

    .line 34
    const-wide/16 v2, -0x1

    .line 35
    .line 36
    const/4 v4, 0x1

    .line 37
    cmp-long v5, v0, v2

    .line 38
    .line 39
    if-nez v5, :cond_0

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 42
    .line 43
    invoke-static {v0, v4}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇0oO〇oo00(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Z)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 47
    .line 48
    invoke-static {}, Lcom/intsig/nativelib/OcrLanguage;->getAllLanguage()J

    .line 49
    .line 50
    .line 51
    move-result-wide v1

    .line 52
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇O80〇0o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;J)V

    .line 53
    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 57
    .line 58
    const-wide/16 v1, 0x1

    .line 59
    .line 60
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)J

    .line 61
    .line 62
    .line 63
    move-result-wide v5

    .line 64
    and-long/2addr v1, v5

    .line 65
    const-wide/16 v5, 0x0

    .line 66
    .line 67
    cmp-long v3, v1, v5

    .line 68
    .line 69
    if-lez v3, :cond_1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_1
    const/4 v4, 0x0

    .line 73
    :goto_0
    invoke-static {v0, v4}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇0oO〇oo00(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;Z)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 77
    .line 78
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)J

    .line 79
    .line 80
    .line 81
    move-result-wide v1

    .line 82
    const-wide/16 v3, 0x2

    .line 83
    .line 84
    div-long/2addr v1, v3

    .line 85
    invoke-static {v1, v2}, Lcom/intsig/nativelib/OcrLanguage;->filterLanguage(J)J

    .line 86
    .line 87
    .line 88
    move-result-wide v1

    .line 89
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇O80〇0o(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;J)V

    .line 90
    .line 91
    .line 92
    :goto_1
    invoke-static {}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇〇()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    const-string v2, "isLegalOfflineKey mInputLanguage="

    .line 102
    .line 103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    iget-object v2, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 107
    .line 108
    invoke-static {v2}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->〇8〇OOoooo(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)J

    .line 109
    .line 110
    .line 111
    move-result-wide v2

    .line 112
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v1

    .line 119
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 123
    .line 124
    invoke-static {v0}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->o88(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;)V

    .line 125
    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/openapi/OCROpenApiActivity$OfflineOcrOpenApiClient;->〇080:Lcom/intsig/camscanner/openapi/OCROpenApiActivity;

    .line 129
    .line 130
    const/16 v1, 0xfa3

    .line 131
    .line 132
    invoke-static {v0, v1}, Lcom/intsig/camscanner/openapi/OCROpenApiActivity;->O〇8〇008(Lcom/intsig/camscanner/openapi/OCROpenApiActivity;I)V

    .line 133
    .line 134
    .line 135
    :goto_2
    return-void
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

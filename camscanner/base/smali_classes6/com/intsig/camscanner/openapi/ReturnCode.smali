.class public Lcom/intsig/camscanner/openapi/ReturnCode;
.super Ljava/lang/Object;
.source "ReturnCode.java"


# direct methods
.method public static 〇080(I)Ljava/lang/String;
    .locals 1

    .line 1
    const/16 v0, 0x138b

    .line 2
    .line 3
    if-eq p0, v0, :cond_2

    .line 4
    .line 5
    const/16 v0, 0x1770

    .line 6
    .line 7
    if-eq p0, v0, :cond_1

    .line 8
    .line 9
    const/16 v0, 0x1f40

    .line 10
    .line 11
    if-eq p0, v0, :cond_0

    .line 12
    .line 13
    packed-switch p0, :pswitch_data_0

    .line 14
    .line 15
    .line 16
    packed-switch p0, :pswitch_data_1

    .line 17
    .line 18
    .line 19
    packed-switch p0, :pswitch_data_2

    .line 20
    .line 21
    .line 22
    const/4 p0, 0x0

    .line 23
    goto :goto_0

    .line 24
    :pswitch_0
    const-string p0, "CamScanner can\u2019t save the original image. The file is invalid, such as file not exists or no enough space to save the image."

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :pswitch_1
    const-string p0, "CamScanner can\u2019t save the pdf document. The file is invalid, such as file not exists or no enough space to save the image."

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :pswitch_2
    const-string p0, "Can\u2019t save the scanned image. The file is invalid, such as file not exists or no enough space to save the image."

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :pswitch_3
    const-string p0, "The call device is not supported."

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :pswitch_4
    const-string p0, "The request can be used only if CamScanner has an account logging in."

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :pswitch_5
    const-string p0, "The request open api_version is higher than that in CamScanner."

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :pswitch_6
    const-string p0, "The number of request devices reach the limitation."

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :pswitch_7
    const-string p0, "The request times has reached the limitation in one day."

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :pswitch_8
    const-string p0, "The enhance mode which user chooses is not authorized for the 3rd party App."

    .line 49
    .line 50
    goto :goto_0

    .line 51
    :pswitch_9
    const-string p0, "image too large"

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :pswitch_a
    const-string p0, "fail to get storage pemission"

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :pswitch_b
    const-string p0, "the storage is not mounted "

    .line 58
    .line 59
    goto :goto_0

    .line 60
    :pswitch_c
    const-string p0, "The User_ID is invalid."

    .line 61
    .line 62
    goto :goto_0

    .line 63
    :pswitch_d
    const-string p0, "The source image is invalid, such as the image file does not exist, broken, or not JPEG or PNG format."

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :pswitch_e
    const-string p0, "Invalid app signature, make sure that the signature of the app is the same to that when you apply OpenAPI."

    .line 67
    .line 68
    goto :goto_0

    .line 69
    :pswitch_f
    const-string p0, "Invalid App package name. Please call CamScanner OpenAPI by startActivity()."

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :pswitch_10
    const-string p0, "Authorization failed, maybe there is no network available."

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :pswitch_11
    const-string p0, "Authorization error, please input the legal APP_KEY"

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    const-string p0, "not config ocr lang in server"

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    const-string p0, "The returned result is OK."

    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_2
    const-string p0, "The 3rd party App certification is expired."

    .line 85
    .line 86
    :goto_0
    return-object p0

    .line 87
    :pswitch_data_0
    .packed-switch 0xfa2
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
    .end packed-switch

    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    :pswitch_data_1
    .packed-switch 0x138e
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    :pswitch_data_2
    .packed-switch 0x1b59
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.class public Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "MoveOrCopyDocActivity.java"

# interfaces
.implements Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;


# static fields
.field public static final O〇08oOOO0:I

.field public static final o8〇OO:I

.field public static final 〇00O0:I


# instance fields
.field private O0O:Lcom/intsig/view/ImageTextButton;

.field private O88O:Lcom/intsig/camscanner/datastruct/FolderItem;

.field Oo80:Landroid/widget/AdapterView$OnItemClickListener;

.field private O〇o88o08〇:Lcom/intsig/app/AlertDialog;

.field private o8o:Landroid/widget/ImageView;

.field private o8oOOo:Landroid/widget/TextView;

.field private oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

.field private oo8ooo8O:Landroid/view/View;

.field private ooo0〇〇O:Landroid/widget/AbsListView;

.field private o〇oO:Landroid/widget/TextView;

.field private 〇08〇o0O:Landroid/view/View;

.field private 〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

.field private 〇o0O:Z

.field private 〇〇08O:Landroid/widget/TextView;

.field private 〇〇o〇:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/util/CursorLoaderId;->O8:I

    .line 2
    .line 3
    sput v0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇00O0:I

    .line 4
    .line 5
    sget v0, Lcom/intsig/camscanner/util/CursorLoaderId;->oO80:I

    .line 6
    .line 7
    sput v0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇08oOOO0:I

    .line 8
    .line 9
    sget v0, Lcom/intsig/camscanner/util/CursorLoaderId;->〇o〇:I

    .line 10
    .line 11
    sput v0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8〇OO:I

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇o0O:Z

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O88O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 9
    .line 10
    new-instance v0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$1;

    .line 11
    .line 12
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$1;-><init>(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->Oo80:Landroid/widget/AdapterView$OnItemClickListener;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O0〇(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O880O〇()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f13041e

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const v2, 0x7f130359

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    new-instance v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$3;

    .line 21
    .line 22
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$3;-><init>(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)V

    .line 23
    .line 24
    .line 25
    const v3, 0x7f131d85

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v3, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const v2, 0x7f131cfc

    .line 33
    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    invoke-virtual {v1, v2, v3}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 37
    .line 38
    .line 39
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v2, "showLoginDlg "

    .line 50
    .line 51
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const-string v1, "MoveOrCopyDocActivity"

    .line 62
    .line 63
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void
    .line 67
    .line 68
.end method

.method private OO0O()V
    .locals 2

    .line 1
    const v0, 0x7f0a1147

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇08oO80o(I)V

    .line 5
    .line 6
    .line 7
    const v0, 0x7f0a0542

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    check-cast v0, Landroid/widget/AbsListView;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->ooo0〇〇O:Landroid/widget/AbsListView;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->Oo80:Landroid/widget/AdapterView$OnItemClickListener;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO8Oo0〇()Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->Oo08()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OooO〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇o88o08〇:Lcom/intsig/app/AlertDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const v1, 0x7f1300a9

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    .line 16
    const v1, 0x7f1302a7

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 20
    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    iput-object v1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇o88o08〇:Lcom/intsig/app/AlertDialog;

    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$2;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity$2;-><init>(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)V

    .line 35
    .line 36
    .line 37
    const v2, 0x7f130019

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    .line 43
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇o88o08〇:Lcom/intsig/app/AlertDialog;

    .line 44
    .line 45
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_1

    .line 50
    .line 51
    return-void

    .line 52
    :cond_1
    const-string v0, " the folder has be delete on other device"

    .line 53
    .line 54
    const-string v1, "MoveOrCopyDocActivity"

    .line 55
    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇o88o08〇:Lcom/intsig/app/AlertDialog;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67
    .line 68
    .line 69
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O〇080〇o0(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;Lcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O88O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇o0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇〇O80o8(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0Oo(Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O880O〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o808o8o08()V
    .locals 2

    .line 1
    const v0, 0x7f0a12c4

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    .line 10
    .line 11
    const v0, 0x7f0a15e7

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Landroid/widget/TextView;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 21
    .line 22
    const v0, 0x7f0a160c

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇o〇:Landroid/view/View;

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o88O〇8()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    if-nez v1, :cond_0

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 44
    .line 45
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;

    .line 55
    .line 56
    if-eqz v0, :cond_1

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇o00〇〇Oo()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    .line 72
    .line 73
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 74
    .line 75
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 79
    .line 80
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇080()V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o〇08oO80o(I)V
    .locals 1

    .line 1
    :try_start_0
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Landroid/view/ViewStub;

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :catch_0
    move-exception p1

    .line 14
    const-string v0, "MoveOrCopyDocActivity"

    .line 15
    .line 16
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    :goto_0
    return-void
    .line 20
.end method

.method private o〇o08〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0oOo〇0()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eq v1, v0, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0oOo〇0()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 v1, 0x0

    .line 20
    :cond_1
    :goto_0
    return v1
    .line 21
.end method

.method private 〇oO88o()V
    .locals 4

    .line 1
    const v0, 0x7f0a0f65

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 9
    .line 10
    const v1, 0x7f0a1889

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Landroid/widget/TextView;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-interface {v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->getTitle()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    .line 31
    .line 32
    const v2, 0x7f0a0918

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    check-cast v2, Landroid/widget/ImageView;

    .line 40
    .line 41
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8o:Landroid/widget/ImageView;

    .line 42
    .line 43
    const v2, 0x7f0a137b

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oo8ooo8O:Landroid/view/View;

    .line 51
    .line 52
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    .line 54
    .line 55
    const v2, 0x7f0a0ba2

    .line 56
    .line 57
    .line 58
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇08〇o0O:Landroid/view/View;

    .line 63
    .line 64
    const v2, 0x7f0a183d

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    check-cast v2, Landroid/widget/TextView;

    .line 72
    .line 73
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇08O:Landroid/widget/TextView;

    .line 74
    .line 75
    const v2, 0x7f0a07e1

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    check-cast v2, Lcom/intsig/view/ImageTextButton;

    .line 83
    .line 84
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 85
    .line 86
    const v2, 0x7f0a177f

    .line 87
    .line 88
    .line 89
    invoke-virtual {p0, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    check-cast v2, Landroid/widget/TextView;

    .line 94
    .line 95
    iput-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 96
    .line 97
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    .line 99
    .line 100
    iget-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8o:Landroid/widget/ImageView;

    .line 101
    .line 102
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    .line 104
    .line 105
    iget-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 106
    .line 107
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    .line 109
    .line 110
    iget-object v2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 111
    .line 112
    iget-object v3, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇08O:Landroid/widget/TextView;

    .line 113
    .line 114
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 115
    .line 116
    .line 117
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 118
    .line 119
    .line 120
    move-result v2

    .line 121
    if-eqz v2, :cond_0

    .line 122
    .line 123
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    const v3, 0x7f0602fa

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    .line 131
    .line 132
    .line 133
    move-result v2

    .line 134
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    const v2, 0x7f0600ef

    .line 142
    .line 143
    .line 144
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    .line 150
    .line 151
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇08O:Landroid/widget/TextView;

    .line 152
    .line 153
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 154
    .line 155
    .line 156
    move-result-object v1

    .line 157
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 158
    .line 159
    .line 160
    move-result v1

    .line 161
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 162
    .line 163
    .line 164
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 165
    .line 166
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 167
    .line 168
    .line 169
    move-result-object v1

    .line 170
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 171
    .line 172
    .line 173
    move-result v1

    .line 174
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 175
    .line 176
    .line 177
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8o:Landroid/widget/ImageView;

    .line 178
    .line 179
    const v1, 0x7f080b93

    .line 180
    .line 181
    .line 182
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    .line 184
    .line 185
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 186
    .line 187
    const v1, 0x7f080a20

    .line 188
    .line 189
    .line 190
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->setImageResource(I)V

    .line 191
    .line 192
    .line 193
    :cond_0
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method


# virtual methods
.method public O8888(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 4
    .line 5
    const v0, 0x7f060661

    .line 6
    .line 7
    .line 8
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 23
    .line 24
    const v0, 0x7f060527

    .line 25
    .line 26
    .line 27
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 38
    .line 39
    .line 40
    :goto_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public Oo〇(Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;)Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    const/4 v4, 0x0

    .line 8
    iget-object v5, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->ooo0〇〇O:Landroid/widget/AbsListView;

    .line 9
    .line 10
    move-object v0, p1

    .line 11
    move-object v1, p0

    .line 12
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/intsig/camscanner/adapter/QueryInterface;ILandroid/widget/AbsListView;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇〇〇0〇〇0(Z)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->OOO8o〇〇(Z)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x1

    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇O(Z)V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->ooo0〇〇O:Landroid/widget/AbsListView;

    .line 27
    .line 28
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 29
    .line 30
    .line 31
    return-object p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getContext()Landroidx/fragment/app/FragmentActivity;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string p1, "MoveOrCopyDocActivity"

    .line 2
    .line 3
    const-string v0, "onCreate"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->O0(Landroid/app/Activity;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;-><init>(Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇oO〇oo8o(Landroid/content/Intent;)V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇o〇8()V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->OO0O()V

    .line 34
    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇oO88o()V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o808o8o08()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o8O〇(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p3    # Landroid/content/Intent;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/16 p3, 0x12c

    .line 5
    .line 6
    if-ne p1, p3, :cond_0

    .line 7
    .line 8
    const/4 p1, -0x1

    .line 9
    if-ne p2, p1, :cond_0

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 12
    .line 13
    iget-object p2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O88O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 14
    .line 15
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8888(Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x1

    .line 19
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇o0O:Z

    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O88O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onBackPressed()V
    .locals 3

    .line 1
    const-string v0, "MoveOrCopyDocActivity"

    .line 2
    .line 3
    const-string v1, "onBackPressed"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇oo()Lorg/json/JSONObject;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "CSMoveCopy"

    .line 21
    .line 22
    const-string v2, "back"

    .line 23
    .line 24
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 25
    .line 26
    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o08〇〇0O()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a0918

    .line 6
    .line 7
    .line 8
    const-string v1, "CSMoveCopy"

    .line 9
    .line 10
    const-string v2, "MoveOrCopyDocActivity"

    .line 11
    .line 12
    if-eq p1, v0, :cond_7

    .line 13
    .line 14
    const v0, 0x7f0a137b

    .line 15
    .line 16
    .line 17
    if-ne p1, v0, :cond_0

    .line 18
    .line 19
    goto/16 :goto_1

    .line 20
    .line 21
    :cond_0
    const v0, 0x7f0a12c4

    .line 22
    .line 23
    .line 24
    if-ne p1, v0, :cond_3

    .line 25
    .line 26
    const-string p1, "cancel move"

    .line 27
    .line 28
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    instance-of p1, p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;

    .line 38
    .line 39
    const-string v0, "cancel"

    .line 40
    .line 41
    if-eqz p1, :cond_1

    .line 42
    .line 43
    const-string p1, "CSSelectPath"

    .line 44
    .line 45
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-eqz p1, :cond_2

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇oo()Lorg/json/JSONObject;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 65
    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_3
    const v0, 0x7f0a177f

    .line 69
    .line 70
    .line 71
    if-ne p1, v0, :cond_4

    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 74
    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    if-eqz p1, :cond_9

    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    instance-of p1, p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 88
    .line 89
    if-eqz p1, :cond_9

    .line 90
    .line 91
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 92
    .line 93
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 94
    .line 95
    .line 96
    move-result-object p1

    .line 97
    check-cast p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;->〇O8o08O()V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_4
    const v0, 0x7f0a07e1

    .line 104
    .line 105
    .line 106
    if-ne p1, v0, :cond_6

    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 109
    .line 110
    .line 111
    move-result p1

    .line 112
    if-eqz p1, :cond_5

    .line 113
    .line 114
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 115
    .line 116
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇oo()Lorg/json/JSONObject;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    const-string v0, "create_folder"

    .line 121
    .line 122
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 123
    .line 124
    .line 125
    :cond_5
    const/4 p1, 0x0

    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 127
    .line 128
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇00O〇0o(I)V

    .line 129
    .line 130
    .line 131
    goto :goto_2

    .line 132
    :cond_6
    const v0, 0x7f0a15e7

    .line 133
    .line 134
    .line 135
    if-ne p1, v0, :cond_9

    .line 136
    .line 137
    const-string p1, "click move doc"

    .line 138
    .line 139
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 143
    .line 144
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->execute()V

    .line 149
    .line 150
    .line 151
    goto :goto_2

    .line 152
    :cond_7
    :goto_1
    const-string p1, "click go back"

    .line 153
    .line 154
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 158
    .line 159
    .line 160
    move-result p1

    .line 161
    if-eqz p1, :cond_8

    .line 162
    .line 163
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 164
    .line 165
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇oo()Lorg/json/JSONObject;

    .line 166
    .line 167
    .line 168
    move-result-object p1

    .line 169
    const-string v0, "back"

    .line 170
    .line 171
    invoke-static {v1, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 172
    .line 173
    .line 174
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 175
    .line 176
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o08〇〇0O()V

    .line 177
    .line 178
    .line 179
    :cond_9
    :goto_2
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected onResume()V
    .locals 2

    .line 1
    const-string v0, "MoveOrCopyDocActivity"

    .line 2
    .line 3
    const-string v1, "onResume"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->onResume()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0oOo〇0()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x2

    .line 11
    if-ne v1, v0, :cond_1

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const-string v1, "CSSelectPath"

    .line 24
    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    invoke-static {v1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v2, "from_part"

    .line 38
    .line 39
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇o08〇()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-eqz v0, :cond_2

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇oo()Lorg/json/JSONObject;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    const-string v1, "CSMoveCopy"

    .line 56
    .line 57
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 58
    .line 59
    .line 60
    :cond_2
    :goto_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    .line 2
    .line 3
    .line 4
    const-string v0, "MoveOrCopyDocActivity"

    .line 5
    .line 6
    const-string v1, "onStop()"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->onStop()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oo08OO〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 8
    .line 9
    const v1, 0x7f1301b4

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇08〇o0O:Landroid/view/View;

    .line 16
    .line 17
    const/16 v1, 0x8

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O0O:Lcom/intsig/view/ImageTextButton;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oo0O〇0〇〇〇(Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;)Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;
    .locals 6

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x0

    .line 7
    const/4 v4, 0x0

    .line 8
    iget-object v5, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->ooo0〇〇O:Landroid/widget/AbsListView;

    .line 9
    .line 10
    move-object v0, p1

    .line 11
    move-object v1, p0

    .line 12
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;-><init>(Landroid/app/Activity;Landroid/database/Cursor;Lcom/intsig/camscanner/adapter/QueryInterface;ILandroid/widget/AbsListView;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇〇0〇88()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇0OOo〇0(I)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo8o〇o〇()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o8(Z)V

    .line 31
    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->O0〇OO8(Z)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->Oo08()Ljava/util/ArrayList;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->O〇OO(Ljava/util/ArrayList;)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oO8o()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->O〇08(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 60
    .line 61
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo0O〇0〇〇〇()Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->Oo〇O8o〇8(Z)V

    .line 66
    .line 67
    .line 68
    const-string v0, "dir_cardbag"

    .line 69
    .line 70
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->〇〇o8(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇o()Ljava/util/HashMap;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇080(Ljava/util/HashMap;)V

    .line 78
    .line 79
    .line 80
    const/4 v0, 0x1

    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->ooO〇00O(Z)V

    .line 82
    .line 83
    .line 84
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->ooo0〇〇O:Landroid/widget/AbsListView;

    .line 85
    .line 86
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    .line 88
    .line 89
    return-object p1
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public ooo8o〇o〇(IZ)V
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 4
    .line 5
    const v0, 0x7f1300f8

    .line 6
    .line 7
    .line 8
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o〇oO:Landroid/widget/TextView;

    .line 13
    .line 14
    const v0, 0x7f1301b4

    .line 15
    .line 16
    .line 17
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 18
    .line 19
    .line 20
    :goto_0
    const/4 p2, 0x0

    .line 21
    const/16 v0, 0x8

    .line 22
    .line 23
    if-lez p1, :cond_1

    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oo8ooo8O:Landroid/view/View;

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8o:Landroid/widget/ImageView;

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oo8ooo8O:Landroid/view/View;

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8o:Landroid/widget/ImageView;

    .line 42
    .line 43
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8oOOo:Landroid/widget/TextView;

    .line 47
    .line 48
    iget-object p2, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 49
    .line 50
    invoke-virtual {p2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    invoke-interface {p2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇o00〇〇Oo()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p2

    .line 58
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d008a

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇00()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇000〇〇08(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇o〇:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-eqz p1, :cond_1

    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_1
    const/16 p1, 0x8

    .line 14
    .line 15
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
.end method

.method public 〇0O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o0O(Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->OooO〇()V

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇08O:Landroid/widget/TextView;

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o8oO()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇〇08O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-static {p0, v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇〇0〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

    .line 11
    .line 12
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇O〇〇O8:Lcom/intsig/app/BaseProgressDialog;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 21
    .line 22
    .line 23
    :cond_1
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

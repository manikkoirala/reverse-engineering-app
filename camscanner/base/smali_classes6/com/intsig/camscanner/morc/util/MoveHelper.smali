.class public Lcom/intsig/camscanner/morc/util/MoveHelper;
.super Ljava/lang/Object;
.source "MoveHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/morc/util/MoveHelper$TeamModel;,
        Lcom/intsig/camscanner/morc/util/MoveHelper$PersonalModel;,
        Lcom/intsig/camscanner/morc/util/MoveHelper$IMoveFunc;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

.field private final Oo08:[J

.field private final o〇0:Lcom/intsig/camscanner/morc/util/MoveHelper$IMoveFunc;

.field private final 〇080:Ljava/lang/String;

.field private final 〇o00〇〇Oo:Landroid/content/Context;

.field private 〇o〇:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 1
    iget-wide v1, p2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    const/4 v3, 0x0

    aput-wide v1, v0, v3

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/morc/util/MoveHelper;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/datastruct/FolderDocInfo;[J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/datastruct/FolderDocInfo;[J)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "MoveHelper"

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇080:Ljava/lang/String;

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o〇:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 6
    iput-object p3, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 7
    iput-object p4, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->Oo08:[J

    .line 8
    iget-object p1, p3, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->OO:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    new-instance p1, Lcom/intsig/camscanner/morc/util/MoveHelper$PersonalModel;

    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/morc/util/MoveHelper$PersonalModel;-><init>(Lcom/intsig/camscanner/morc/util/MoveHelper;LOO〇OOo/〇080;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/intsig/camscanner/morc/util/MoveHelper$TeamModel;

    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/morc/util/MoveHelper$TeamModel;-><init>(Lcom/intsig/camscanner/morc/util/MoveHelper;LOO〇OOo/〇o00〇〇Oo;)V

    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->o〇0:Lcom/intsig/camscanner/morc/util/MoveHelper$IMoveFunc;

    return-void
.end method

.method private O8([J)Ljava/lang/String;
    .locals 7

    .line 1
    const-string v0, ""

    .line 2
    .line 3
    if-eqz p1, :cond_4

    .line 4
    .line 5
    array-length v1, p1

    .line 6
    if-gtz v1, :cond_0

    .line 7
    .line 8
    goto :goto_3

    .line 9
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    array-length v2, p1

    .line 15
    const/4 v3, 0x0

    .line 16
    :goto_0
    if-ge v3, v2, :cond_2

    .line 17
    .line 18
    aget-wide v4, p1, v3

    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 21
    .line 22
    .line 23
    move-result v6

    .line 24
    if-lez v6, :cond_1

    .line 25
    .line 26
    const-string v6, ","

    .line 27
    .line 28
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_1
    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-lez p1, :cond_3

    .line 49
    .line 50
    new-instance p1, Ljava/lang/StringBuilder;

    .line 51
    .line 52
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .line 54
    .line 55
    const-string v0, "("

    .line 56
    .line 57
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    const-string v0, ")"

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    goto :goto_2

    .line 77
    :cond_3
    const/4 p1, 0x0

    .line 78
    :goto_2
    return-object p1

    .line 79
    :cond_4
    :goto_3
    return-object v0
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/morc/util/MoveHelper;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/morc/util/MoveHelper;)Lcom/intsig/camscanner/datastruct/FolderDocInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o〇()I
    .locals 5

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 7
    .line 8
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->o0:Ljava/lang/String;

    .line 9
    .line 10
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    const-string v2, "sync_dir_id"

    .line 15
    .line 16
    if-eqz v1, :cond_0

    .line 17
    .line 18
    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 23
    .line 24
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->o0:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 30
    .line 31
    iget-boolean v1, v1, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->〇OOo8〇0:Z

    .line 32
    .line 33
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    const-string v2, "folder_type"

    .line 38
    .line 39
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 40
    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 43
    .line 44
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->o〇0:Lcom/intsig/camscanner/morc/util/MoveHelper$IMoveFunc;

    .line 49
    .line 50
    invoke-interface {v2, v0}, Lcom/intsig/camscanner/morc/util/MoveHelper$IMoveFunc;->〇080(Landroid/content/ContentValues;)V

    .line 51
    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 54
    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v4, "_id in "

    .line 61
    .line 62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    iget-object v4, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->Oo08:[J

    .line 66
    .line 67
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8([J)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v4

    .line 71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    const/4 v4, 0x0

    .line 79
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v2, "update num:"

    .line 89
    .line 90
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    const-string v2, "MoveHelper"

    .line 101
    .line 102
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 106
    .line 107
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->Oo08:[J

    .line 108
    .line 109
    iget-object v3, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 110
    .line 111
    iget-boolean v3, v3, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->〇OOo8〇0:Z

    .line 112
    .line 113
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/ImageDao;->O00(Landroid/content/Context;[JI)V

    .line 114
    .line 115
    .line 116
    return v0
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public Oo08()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o〇()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "updateNum: "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "MoveHelper"

    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    if-lez v0, :cond_0

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 36
    .line 37
    .line 38
    move-result-wide v0

    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 40
    .line 41
    iget-object v3, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o〇:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 42
    .line 43
    iget-object v3, v3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 44
    .line 45
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 46
    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 49
    .line 50
    iget-object v3, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 51
    .line 52
    iget-object v3, v3, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->o0:Ljava/lang/String;

    .line 53
    .line 54
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o00〇〇Oo:Landroid/content/Context;

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->Oo08:[J

    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 62
    .line 63
    iget-object v3, v2, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->o0:Ljava/lang/String;

    .line 64
    .line 65
    iget-object v2, v2, Lcom/intsig/camscanner/datastruct/FolderDocInfo;->OO:Ljava/lang/String;

    .line 66
    .line 67
    invoke-static {v0, v1, v3, v2}, Lcom/intsig/camscanner/app/DBUtil;->O〇OO(Landroid/content/Context;[JLjava/lang/String;Ljava/lang/String;)J

    .line 68
    .line 69
    .line 70
    :cond_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o〇0(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->〇o〇:Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/morc/util/MoveHelper;->O8:Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

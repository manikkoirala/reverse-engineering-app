.class public Lcom/intsig/camscanner/morc/util/CopyAsyncTask;
.super Landroid/os/AsyncTask;
.source "CopyAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/morc/util/CopyAsyncTask$CopyCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:[J

.field private Oo08:Ljava/lang/String;

.field private oO80:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

.field private o〇0:I

.field private 〇080:Lcom/intsig/app/BaseProgressDialog;

.field private 〇o00〇〇Oo:Landroid/content/Context;

.field private 〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

.field private 〇〇888:Z


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;[JZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->Oo08:Ljava/lang/String;

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇〇888:Z

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 11
    .line 12
    iput-object v1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->oO80:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 21
    .line 22
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    iput-object v1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 27
    .line 28
    iput-object p2, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->O8:[J

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    check-cast p1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 40
    .line 41
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    if-eqz p3, :cond_0

    .line 46
    .line 47
    const v2, 0x7f130044

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    const v2, 0x7f130043

    .line 52
    .line 53
    .line 54
    :goto_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string p1, " "

    .line 62
    .line 63
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    iput-object p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->Oo08:Ljava/lang/String;

    .line 71
    .line 72
    if-eqz p2, :cond_1

    .line 73
    .line 74
    array-length v0, p2

    .line 75
    :cond_1
    iput v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->o〇0:I

    .line 76
    .line 77
    iput-boolean p3, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇〇888:Z

    .line 78
    .line 79
    iput-object p4, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->oO80:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private O8(Landroid/content/Context;[JLcom/intsig/camscanner/morc/util/CopyAsyncTask$CopyCallback;ZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V
    .locals 33

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v9, p1

    .line 4
    .line 5
    move-object/from16 v10, p2

    .line 6
    .line 7
    move-object/from16 v11, p3

    .line 8
    .line 9
    move/from16 v12, p4

    .line 10
    .line 11
    move-object/from16 v13, p5

    .line 12
    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v2, "executeCopy deleteSrcDocs:"

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    const-string v14, "CopyAsyncTask"

    .line 31
    .line 32
    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    if-eqz v10, :cond_d

    .line 36
    .line 37
    array-length v1, v10

    .line 38
    if-nez v1, :cond_0

    .line 39
    .line 40
    goto/16 :goto_7

    .line 41
    .line 42
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->〇o00〇〇Oo([J)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    new-instance v2, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v3, "(_id in "

    .line 52
    .line 53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v3, ")"

    .line 60
    .line 61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v18

    .line 68
    const-string v2, "title"

    .line 69
    .line 70
    const-string v3, "_id"

    .line 71
    .line 72
    const-string v8, "type"

    .line 73
    .line 74
    const-string v7, "property"

    .line 75
    .line 76
    filled-new-array {v3, v2, v8, v7}, [Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v17

    .line 80
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 81
    .line 82
    .line 83
    move-result-object v15

    .line 84
    sget-object v16, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 85
    .line 86
    const/16 v19, 0x0

    .line 87
    .line 88
    const/16 v20, 0x0

    .line 89
    .line 90
    invoke-virtual/range {v15 .. v20}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 91
    .line 92
    .line 93
    move-result-object v15

    .line 94
    const/4 v6, 0x0

    .line 95
    if-eqz v15, :cond_a

    .line 96
    .line 97
    new-instance v5, Landroid/util/LongSparseArray;

    .line 98
    .line 99
    invoke-direct {v5}, Landroid/util/LongSparseArray;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-direct {v0, v9, v1, v5}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->Oo08(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V

    .line 103
    .line 104
    .line 105
    const-string v1, "_data"

    .line 106
    .line 107
    filled-new-array {v3, v1}, [Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v22

    .line 111
    const/4 v1, 0x0

    .line 112
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    .line 113
    .line 114
    .line 115
    move-result v2

    .line 116
    if-eqz v2, :cond_9

    .line 117
    .line 118
    const/4 v4, 0x1

    .line 119
    add-int/lit8 v3, v1, 0x1

    .line 120
    .line 121
    if-eqz v11, :cond_1

    .line 122
    .line 123
    invoke-interface {v11, v3}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask$CopyCallback;->〇080(I)V

    .line 124
    .line 125
    .line 126
    :cond_1
    invoke-interface {v15, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 127
    .line 128
    .line 129
    move-result v1

    .line 130
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getInt(I)I

    .line 131
    .line 132
    .line 133
    move-result v23

    .line 134
    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    move-object/from16 v24, v8

    .line 143
    .line 144
    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    .line 145
    .line 146
    .line 147
    move-result-wide v8

    .line 148
    invoke-interface {v15, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v16

    .line 152
    const/16 v17, 0x1

    .line 153
    .line 154
    iget-object v1, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 155
    .line 156
    invoke-virtual {v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇〇()Ljava/lang/String;

    .line 157
    .line 158
    .line 159
    move-result-object v18

    .line 160
    iget-object v1, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 161
    .line 162
    invoke-virtual {v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o08oOO()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v19

    .line 166
    const/16 v20, 0x1

    .line 167
    .line 168
    move-object/from16 v1, p1

    .line 169
    .line 170
    move-object/from16 v25, v2

    .line 171
    .line 172
    move-object/from16 v2, v16

    .line 173
    .line 174
    move/from16 v26, v3

    .line 175
    .line 176
    move/from16 v3, v17

    .line 177
    .line 178
    move-object/from16 v4, v18

    .line 179
    .line 180
    move-object/from16 v28, v5

    .line 181
    .line 182
    move-object/from16 v5, v19

    .line 183
    .line 184
    const/4 v11, 0x0

    .line 185
    move/from16 v6, v20

    .line 186
    .line 187
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/util/Util;->ooo〇8oO(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v1

    .line 191
    iget-object v2, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 192
    .line 193
    invoke-virtual {v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO8Oo0〇()Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 194
    .line 195
    .line 196
    move-result-object v2

    .line 197
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->O8(Ljava/lang/String;)Landroid/net/Uri;

    .line 198
    .line 199
    .line 200
    move-result-object v6

    .line 201
    if-nez v6, :cond_2

    .line 202
    .line 203
    const-string v1, "executeCopy newDocUri == null"

    .line 204
    .line 205
    invoke-static {v14, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    return-void

    .line 209
    :cond_2
    invoke-static {v6}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 210
    .line 211
    .line 212
    move-result-wide v4

    .line 213
    iget-object v1, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 214
    .line 215
    invoke-virtual {v1, v4, v5}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇(J)V

    .line 216
    .line 217
    .line 218
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 219
    .line 220
    .line 221
    move-result-object v16

    .line 222
    invoke-static {v8, v9}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 223
    .line 224
    .line 225
    move-result-object v17

    .line 226
    const/16 v19, 0x0

    .line 227
    .line 228
    const/16 v20, 0x0

    .line 229
    .line 230
    const-string v21, "page_num ASC"

    .line 231
    .line 232
    move-object/from16 v18, v22

    .line 233
    .line 234
    invoke-virtual/range {v16 .. v21}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 235
    .line 236
    .line 237
    move-result-object v2

    .line 238
    if-eqz v2, :cond_8

    .line 239
    .line 240
    new-instance v3, Landroid/content/ContentValues;

    .line 241
    .line 242
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 243
    .line 244
    .line 245
    const/4 v1, 0x0

    .line 246
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 247
    .line 248
    .line 249
    move-result v16

    .line 250
    if-eqz v16, :cond_7

    .line 251
    .line 252
    move-wide/from16 v16, v8

    .line 253
    .line 254
    const/4 v8, 0x1

    .line 255
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 256
    .line 257
    .line 258
    move-result-object v9

    .line 259
    invoke-static {v9}, Lcom/intsig/camscanner/util/Util;->〇〇o8(Ljava/lang/String;)Z

    .line 260
    .line 261
    .line 262
    move-result v9

    .line 263
    if-nez v9, :cond_4

    .line 264
    .line 265
    sget-object v9, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 266
    .line 267
    if-ne v13, v9, :cond_3

    .line 268
    .line 269
    goto :goto_2

    .line 270
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    .line 271
    .line 272
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 273
    .line 274
    .line 275
    const-string v11, "mergeDocuments file not exist path = "

    .line 276
    .line 277
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object v11

    .line 284
    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    .line 286
    .line 287
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 288
    .line 289
    .line 290
    move-result-object v9

    .line 291
    invoke-static {v14, v9}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    move-wide/from16 v8, v16

    .line 295
    .line 296
    const/4 v11, 0x0

    .line 297
    goto :goto_1

    .line 298
    :cond_4
    :goto_2
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 299
    .line 300
    .line 301
    const/4 v9, 0x0

    .line 302
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getLong(I)J

    .line 303
    .line 304
    .line 305
    move-result-wide v19

    .line 306
    add-int/lit8 v9, v1, 0x1

    .line 307
    .line 308
    const/4 v11, 0x1

    .line 309
    move-object/from16 v1, p1

    .line 310
    .line 311
    move-object/from16 v29, v2

    .line 312
    .line 313
    move-object/from16 v21, v3

    .line 314
    .line 315
    move-wide/from16 v2, v19

    .line 316
    .line 317
    move-wide/from16 v19, v4

    .line 318
    .line 319
    move-object/from16 v30, v6

    .line 320
    .line 321
    move v6, v9

    .line 322
    move/from16 v27, v9

    .line 323
    .line 324
    move-object v9, v7

    .line 325
    move-object/from16 v7, v21

    .line 326
    .line 327
    move-object/from16 v31, v14

    .line 328
    .line 329
    move-object/from16 v32, v15

    .line 330
    .line 331
    move-wide/from16 v14, v16

    .line 332
    .line 333
    move-object/from16 v10, v24

    .line 334
    .line 335
    const/16 v16, 0x1

    .line 336
    .line 337
    move v8, v11

    .line 338
    invoke-static/range {v1 .. v8}, Lcom/intsig/camscanner/app/DBUtil;->OO0o〇〇〇〇0(Landroid/content/Context;JJILandroid/content/ContentValues;Z)V

    .line 339
    .line 340
    .line 341
    sget-object v1, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 342
    .line 343
    const-string v2, "folder_type"

    .line 344
    .line 345
    if-ne v13, v1, :cond_5

    .line 346
    .line 347
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 348
    .line 349
    .line 350
    move-result-object v1

    .line 351
    move-object/from16 v3, v21

    .line 352
    .line 353
    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 354
    .line 355
    .line 356
    const/4 v4, 0x0

    .line 357
    goto :goto_3

    .line 358
    :cond_5
    move-object/from16 v3, v21

    .line 359
    .line 360
    sget-object v1, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 361
    .line 362
    const/4 v4, 0x0

    .line 363
    if-ne v13, v1, :cond_6

    .line 364
    .line 365
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 366
    .line 367
    .line 368
    move-result-object v1

    .line 369
    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    .line 371
    .line 372
    :cond_6
    :goto_3
    sget-object v1, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 373
    .line 374
    move-object/from16 v2, v29

    .line 375
    .line 376
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    .line 377
    .line 378
    .line 379
    move-result-wide v5

    .line 380
    invoke-virtual {v1, v3, v5, v6}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->oO80(Landroid/content/ContentValues;J)Landroid/net/Uri;

    .line 381
    .line 382
    .line 383
    move-object v7, v9

    .line 384
    move-object/from16 v24, v10

    .line 385
    .line 386
    move-wide v8, v14

    .line 387
    move-wide/from16 v4, v19

    .line 388
    .line 389
    move/from16 v1, v27

    .line 390
    .line 391
    move-object/from16 v6, v30

    .line 392
    .line 393
    move-object/from16 v14, v31

    .line 394
    .line 395
    move-object/from16 v15, v32

    .line 396
    .line 397
    const/4 v11, 0x0

    .line 398
    move-object/from16 v10, p2

    .line 399
    .line 400
    goto/16 :goto_1

    .line 401
    .line 402
    :cond_7
    move-wide/from16 v19, v4

    .line 403
    .line 404
    move-object/from16 v30, v6

    .line 405
    .line 406
    move-object/from16 v31, v14

    .line 407
    .line 408
    move-object/from16 v32, v15

    .line 409
    .line 410
    move-object/from16 v10, v24

    .line 411
    .line 412
    const/4 v4, 0x0

    .line 413
    move-wide v14, v8

    .line 414
    move-object v9, v7

    .line 415
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 416
    .line 417
    .line 418
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 419
    .line 420
    .line 421
    const-string v2, "pages"

    .line 422
    .line 423
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 424
    .line 425
    .line 426
    move-result-object v1

    .line 427
    invoke-virtual {v3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 428
    .line 429
    .line 430
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 431
    .line 432
    .line 433
    move-result-object v1

    .line 434
    invoke-virtual {v3, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 435
    .line 436
    .line 437
    move-object/from16 v1, v25

    .line 438
    .line 439
    invoke-virtual {v3, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    .line 441
    .line 442
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 443
    .line 444
    .line 445
    move-result-object v1

    .line 446
    const/4 v2, 0x0

    .line 447
    move-object/from16 v5, v30

    .line 448
    .line 449
    invoke-virtual {v1, v5, v3, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 450
    .line 451
    .line 452
    move-object/from16 v1, v28

    .line 453
    .line 454
    invoke-virtual {v1, v14, v15}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 455
    .line 456
    .line 457
    move-result-object v2

    .line 458
    check-cast v2, Ljava/util/ArrayList;

    .line 459
    .line 460
    move-object/from16 v3, p1

    .line 461
    .line 462
    move-wide/from16 v5, v19

    .line 463
    .line 464
    invoke-direct {v0, v3, v5, v6, v2}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇80〇808〇O(Landroid/content/Context;JLjava/util/ArrayList;)V

    .line 465
    .line 466
    .line 467
    goto :goto_4

    .line 468
    :cond_8
    move-object/from16 v3, p1

    .line 469
    .line 470
    move-object v9, v7

    .line 471
    move-object/from16 v31, v14

    .line 472
    .line 473
    move-object/from16 v32, v15

    .line 474
    .line 475
    move-object/from16 v10, v24

    .line 476
    .line 477
    move-object/from16 v1, v28

    .line 478
    .line 479
    const/4 v4, 0x0

    .line 480
    :goto_4
    move-object/from16 v11, p3

    .line 481
    .line 482
    move-object v5, v1

    .line 483
    move-object v7, v9

    .line 484
    move-object v8, v10

    .line 485
    move/from16 v1, v26

    .line 486
    .line 487
    move-object/from16 v14, v31

    .line 488
    .line 489
    move-object/from16 v15, v32

    .line 490
    .line 491
    const/4 v6, 0x0

    .line 492
    move-object/from16 v10, p2

    .line 493
    .line 494
    move-object v9, v3

    .line 495
    goto/16 :goto_0

    .line 496
    .line 497
    :cond_9
    move-object/from16 v32, v15

    .line 498
    .line 499
    const/4 v4, 0x0

    .line 500
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    .line 501
    .line 502
    .line 503
    goto :goto_5

    .line 504
    :cond_a
    const/4 v4, 0x0

    .line 505
    :goto_5
    if-eqz v12, :cond_c

    .line 506
    .line 507
    new-instance v1, Ljava/util/ArrayList;

    .line 508
    .line 509
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .line 511
    .line 512
    move-object/from16 v2, p2

    .line 513
    .line 514
    array-length v3, v2

    .line 515
    const/4 v6, 0x0

    .line 516
    :goto_6
    if-ge v6, v3, :cond_b

    .line 517
    .line 518
    aget-wide v4, v2, v6

    .line 519
    .line 520
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 521
    .line 522
    .line 523
    move-result-object v4

    .line 524
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    .line 526
    .line 527
    add-int/lit8 v6, v6, 0x1

    .line 528
    .line 529
    goto :goto_6

    .line 530
    :cond_b
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->oO80(Ljava/util/ArrayList;)V

    .line 531
    .line 532
    .line 533
    iget-object v2, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 534
    .line 535
    const/4 v3, 0x2

    .line 536
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 537
    .line 538
    .line 539
    iget-object v2, v0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 540
    .line 541
    invoke-static {v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 542
    .line 543
    .line 544
    :cond_c
    return-void

    .line 545
    :cond_d
    :goto_7
    move-object/from16 v31, v14

    .line 546
    .line 547
    const-string v1, "srcDocIds is empty"

    .line 548
    .line 549
    move-object/from16 v2, v31

    .line 550
    .line 551
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    .line 553
    .line 554
    return-void
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
.end method

.method private OO0o〇〇〇〇0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    new-instance v2, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->Oo08:Ljava/lang/String;

    .line 20
    .line 21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v3, "0/"

    .line 25
    .line 26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget v3, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->o〇0:I

    .line 30
    .line 31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 44
    .line 45
    .line 46
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 47
    .line 48
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-eqz v0, :cond_1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    const-string v1, "CopyAsyncTask"

    .line 63
    .line 64
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    :goto_0
    return-void
    .line 68
.end method

.method private Oo08(Landroid/content/Context;Ljava/lang/String;Landroid/util/LongSparseArray;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Landroid/util/LongSparseArray<",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "tag_id"

    .line 2
    .line 3
    const-string v1, "document_id"

    .line 4
    .line 5
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v4

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "document_id in "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v5

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 31
    .line 32
    const/4 v6, 0x0

    .line 33
    const/4 v7, 0x0

    .line 34
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    if-eqz p1, :cond_2

    .line 39
    .line 40
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 41
    .line 42
    .line 43
    move-result p2

    .line 44
    if-eqz p2, :cond_1

    .line 45
    .line 46
    const/4 p2, 0x1

    .line 47
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 48
    .line 49
    .line 50
    move-result-wide v0

    .line 51
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    if-ltz v0, :cond_0

    .line 56
    .line 57
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 58
    .line 59
    .line 60
    move-result-wide v0

    .line 61
    invoke-virtual {p3, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    check-cast p2, Ljava/util/ArrayList;

    .line 66
    .line 67
    goto :goto_1

    .line 68
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .line 72
    .line 73
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getLong(I)J

    .line 74
    .line 75
    .line 76
    move-result-wide v1

    .line 77
    invoke-virtual {p3, v1, v2, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 78
    .line 79
    .line 80
    move-object p2, v0

    .line 81
    :goto_1
    const/4 v0, 0x0

    .line 82
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    .line 83
    .line 84
    .line 85
    move-result-wide v0

    .line 86
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 95
    .line 96
    .line 97
    :cond_2
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private oO80(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-static {v0, p1, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇oo〇(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/ArrayList;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {v2, p1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    new-instance v2, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 17
    .line 18
    .line 19
    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 23
    .line 24
    invoke-static {p1, v2, v1}, Lcom/intsig/camscanner/app/DBUtil;->o〇0o〇〇(Landroid/content/Context;Ljava/util/List;I)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic 〇080(Lcom/intsig/camscanner/morc/util/CopyAsyncTask;[Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇80〇808〇O(Landroid/content/Context;JLjava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "CopyAsyncTask"

    .line 2
    .line 3
    if-eqz p4, :cond_4

    .line 4
    .line 5
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto :goto_2

    .line 12
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 13
    .line 14
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object p4

    .line 21
    :cond_1
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_2

    .line 26
    .line 27
    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Ljava/lang/Long;

    .line 32
    .line 33
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 34
    .line 35
    .line 36
    move-result-wide v2

    .line 37
    invoke-static {p1, v2, v3}, Lcom/intsig/camscanner/db/dao/TagDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 38
    .line 39
    .line 40
    move-result v4

    .line 41
    if-eqz v4, :cond_1

    .line 42
    .line 43
    new-instance v4, Landroid/content/ContentValues;

    .line 44
    .line 45
    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v5, "document_id"

    .line 49
    .line 50
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 51
    .line 52
    .line 53
    move-result-object v6

    .line 54
    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 55
    .line 56
    .line 57
    const-string v5, "tag_id"

    .line 58
    .line 59
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 64
    .line 65
    .line 66
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 67
    .line 68
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    invoke-virtual {v2, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 85
    .line 86
    .line 87
    move-result p2

    .line 88
    if-lez p2, :cond_3

    .line 89
    .line 90
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    sget-object p2, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 95
    .line 96
    invoke-virtual {p1, p2, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :catch_0
    move-exception p1

    .line 101
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    .line 103
    .line 104
    :cond_3
    :goto_1
    return-void

    .line 105
    :cond_4
    :goto_2
    const-string p1, "tagIds is empty"

    .line 106
    .line 107
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "CopyAsyncTask"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇([Ljava/lang/Void;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->o〇0(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onPreExecute()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇〇888([Ljava/lang/Integer;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected o〇0(Ljava/lang/Void;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08〇0〇o〇8(Z)V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->oO()Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 15
    .line 16
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/sync/DirSyncFromServer;->〇8(Landroid/content/Context;)J

    .line 17
    .line 18
    .line 19
    move-result-wide v0

    .line 20
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 25
    .line 26
    invoke-virtual {v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇〇()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-static {p1, v2, v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->〇008〇oo(Landroid/content/Context;Ljava/lang/String;J)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO8Oo0〇()Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->onFinish()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected varargs 〇o〇([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    .line 1
    iget-object v1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 2
    .line 3
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->O8:[J

    .line 4
    .line 5
    new-instance v3, Lcom/intsig/camscanner/morc/util/CopyAsyncTask$1;

    .line 6
    .line 7
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask$1;-><init>(Lcom/intsig/camscanner/morc/util/CopyAsyncTask;)V

    .line 8
    .line 9
    .line 10
    iget-boolean v4, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇〇888:Z

    .line 11
    .line 12
    iget-object v5, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->oO80:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 13
    .line 14
    move-object v0, p0

    .line 15
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->O8(Landroid/content/Context;[JLcom/intsig/camscanner/morc/util/CopyAsyncTask$CopyCallback;ZLcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;)V

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x0

    .line 19
    return-object p1
    .line 20
.end method

.method protected varargs 〇〇888([Ljava/lang/Integer;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->Oo08:Ljava/lang/String;

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    const/4 v2, 0x0

    .line 16
    aget-object p1, p1, v2

    .line 17
    .line 18
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string p1, "/"

    .line 22
    .line 23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    iget p1, p0, Lcom/intsig/camscanner/morc/util/CopyAsyncTask;->o〇0:I

    .line 27
    .line 28
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {v0, p1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

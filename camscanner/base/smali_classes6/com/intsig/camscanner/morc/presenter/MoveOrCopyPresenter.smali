.class public Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;
.super Lcom/intsig/mvp/presenter/BasePresenter;
.source "MoveOrCopyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$CopyAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OnlyGetPathAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$MoveAction;,
        Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/mvp/presenter/BasePresenter<",
        "Ljava/lang/Object;",
        "Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;",
        ">;"
    }
.end annotation


# instance fields
.field private O0O:I

.field private O88O:I

.field private O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

.field private OO〇00〇8oO:J

.field private Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private Oo80:Ljava/lang/String;

.field private Ooo08:Z

.field private O〇08oOOO0:Ljava/lang/String;

.field private O〇o88o08〇:Ljava/lang/String;

.field private o8o:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;

.field private o8oOOo:I

.field private o8〇OO:Z

.field private o8〇OO0〇0o:[J

.field private oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;

.field private oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

.field private oOo〇8o008:Z

.field private oO〇8O8oOo:Ljava/lang/String;

.field private oo8ooo8O:Z

.field private ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private ooo0〇〇O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

.field private o〇oO:Z

.field private 〇00O0:Z

.field private 〇080OO8〇0:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

.field private 〇08〇o0O:Z

.field private 〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

.field public 〇0O〇O00O:I

.field private 〇8〇oO〇〇8o:Ljava/lang/String;

.field private 〇OO8ooO8〇:I

.field private 〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private 〇O〇〇O8:I

.field private 〇o0O:Ljava/lang/String;

.field private 〇〇08O:Ljava/lang/String;

.field private 〇〇o〇:Ljava/lang/String;

.field private 〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/morc/model/MoveOrCopyModel;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/morc/model/MoveOrCopyModel;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0, p1}, Lcom/intsig/mvp/presenter/BasePresenter;-><init>(Lcom/intsig/mvp/model/IModel;Lcom/intsig/mvp/view/IView;)V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo〇8o008:Z

    .line 15
    .line 16
    const-wide/16 v1, -0x1

    .line 17
    .line 18
    iput-wide v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO〇00〇8oO:J

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 22
    .line 23
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 24
    .line 25
    new-instance v2, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 31
    .line 32
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇08O:Ljava/lang/String;

    .line 33
    .line 34
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O0O:I

    .line 35
    .line 36
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8oOOo:I

    .line 37
    .line 38
    const/4 v2, 0x1

    .line 39
    iput v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇O〇〇O8:I

    .line 40
    .line 41
    const-string v2, ""

    .line 42
    .line 43
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 44
    .line 45
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 46
    .line 47
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇oO:Z

    .line 48
    .line 49
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 50
    .line 51
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 52
    .line 53
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 54
    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇08oOOO0:Ljava/lang/String;

    .line 56
    .line 57
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO:Z

    .line 58
    .line 59
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Ooo08:Z

    .line 60
    .line 61
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O〇O00O:I

    .line 62
    .line 63
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    iput-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static synthetic O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O00(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O000(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0880()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O00O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08000(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o8OO0()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0O8OO088(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0OO8〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0oO008(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0oo0o0〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0o〇O0〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0o〇〇Oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O0〇OO8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O80〇O〇080()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇0O()V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇080()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic O880oOO08(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O88o〇()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->NON:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo〇8o008:Z

    .line 6
    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN_OFFLINE:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT_OFFLINE:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O88〇〇o0O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$2;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$2;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic O8O〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O8O〇88oO0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O80〇O〇080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OO0〇〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OO8oO0o〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO8〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->O〇Oooo〇〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic OOO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OOO8o〇〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OOO〇O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)[J
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OOo0O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OOo8o〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OOoo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇〇888()V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, LOo0O0o8/〇080;

    .line 15
    .line 16
    invoke-direct {v1, p0}, LOo0O0o8/〇080;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic OO〇0008O8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic Oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic Oo08OO8oO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic Oo0oO〇O〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo8Oo00oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇00O0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OoO8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OoOOo8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$4;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$4;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic OoO〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Ooo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o8oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Ooo8()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->OUT:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic Ooo8〇〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇0o〇〇(Ljava/lang/String;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private Oo〇()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 20
    .line 21
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    :cond_0
    const/4 v0, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_1
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic Oo〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic Oo〇O8o〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic Oo〇o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 18
    .line 19
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o88O8(Ljava/lang/String;)I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/teampermission/TeamPermissionUtil;->o〇0(I)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Ooo08:Z

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Ooo08:Z

    .line 32
    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 38
    .line 39
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, LOo0O0o8/〇o00〇〇Oo;

    .line 44
    .line 45
    invoke-direct {v1, p0}, LOo0O0o8/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic O〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O〇08(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O〇0〇o808〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇8oOo8O(I)V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O80〇oOo()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    invoke-static {v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇8o8O〇O(Landroid/content/Context;Z)I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇O〇()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    iget-object v3, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 17
    .line 18
    new-instance v4, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v5, "create new special folder ,maxDirNumber = "

    .line 24
    .line 25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    const-string v5, ",maxDirNumberCurrent = "

    .line 32
    .line 33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    const-string v5, ",maxDirLayer = "

    .line 40
    .line 41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    if-ge v1, v0, :cond_1

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 57
    .line 58
    if-eqz v0, :cond_0

    .line 59
    .line 60
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    if-ge v0, v2, :cond_0

    .line 65
    .line 66
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o8O〇O(I)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_0
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 71
    .line 72
    const-string v0, "checkCreateSpecialFolder --> layer num out limit"

    .line 73
    .line 74
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 79
    .line 80
    invoke-static {p1}, Lcom/intsig/camscanner/app/DialogUtils;->〇80(Landroid/content/Context;)V

    .line 81
    .line 82
    .line 83
    :goto_0
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static synthetic O〇OO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O〇Oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic O〇Oooo〇〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇O〇oO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o08O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$3;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$3;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic o0O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o0O〇8o0O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0ooO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o800o8O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o80ooO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o88O8(Ljava/lang/String;)I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-static {v0, v1, p1, v2}, Lcom/intsig/camscanner/app/DBUtil;->O〇Oooo〇〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    return p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o88o0O(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showCreateInviteDirTips duuId:$duuId"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    check-cast v1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 15
    .line 16
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 21
    .line 22
    .line 23
    const v1, 0x7f130ff4

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const v1, 0x7f130ff3

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    new-instance v1, LOo0O0o8/〇o〇;

    .line 38
    .line 39
    invoke-direct {v1, p0, p1}, LOo0O0o8/〇o〇;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const p1, 0x7f130ffc

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, p1, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 54
    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic o88〇OO08〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o8O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o8O〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8oO〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇080O0()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0〇O0088o()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 16
    .line 17
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->OO0o〇〇〇〇0()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    add-int/2addr v0, v1

    .line 22
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    check-cast v1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 27
    .line 28
    iget-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 29
    .line 30
    check-cast v2, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;->OO0o〇〇〇〇0()Z

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    invoke-interface {v1, v0, v2}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->ooo8o〇o〇(IZ)V

    .line 37
    .line 38
    .line 39
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 40
    .line 41
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇080()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic oO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇oO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oO00OOO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oOo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oO〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo88o8O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8oOOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic ooOO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic ooO〇00O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic ooo0〇O88O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooo〇8oO()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$8;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$8;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f1301e4

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇oo(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic ooo〇〇O〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/morc/contract/MoldInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0OOo〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇0o〇〇(Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p2, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p3, "showCreateInviteDirTips I know duuId:$duuId"

    .line 4
    .line 5
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance p2, LOo0O0o8/O8;

    .line 9
    .line 10
    invoke-direct {p2, p0, p1}, LOo0O0o8/O8;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic o〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇8oOO88(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇008〇o0〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇8〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Ooo8()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇O8〇〇o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO〇00〇8oO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇〇0〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0000OOO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO8ooO8〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇000O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇000〇〇08(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇008〇o0〇〇()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN_OFFLINE:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic 〇00O0O0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇080O0()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;->IN:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O:Lcom/intsig/camscanner/business/folders/OfflineFolder$OperatingDirection;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇08O8o〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇080OO8〇0:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇0O00oO()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "show upgrade vip dialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 9
    .line 10
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 15
    .line 16
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 17
    .line 18
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 19
    .line 20
    .line 21
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FOLDER_LIMIT_FOR_USER_CREATE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SELECT_PATH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 38
    .line 39
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 40
    .line 41
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 42
    .line 43
    .line 44
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FOLDER_LIMIT_FOR_USER_CREATE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 45
    .line 46
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 51
    .line 52
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇0OO8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇0O〇Oo(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0〇O0088o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/adapter/FolderAdapterInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo〇8oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇O8〇OO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇8o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8o8O〇O(I)V
    .locals 8

    .line 1
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 6
    .line 7
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 12
    .line 13
    const v3, 0x7f13021e

    .line 14
    .line 15
    .line 16
    const/4 v5, 0x1

    .line 17
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 22
    .line 23
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iget-object v4, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 28
    .line 29
    const/4 v6, 0x1

    .line 30
    invoke-static {v0, v4, v6, p1}, Lcom/intsig/camscanner/util/Util;->〇O888o0o(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v6

    .line 34
    new-instance v7, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$1;

    .line 35
    .line 36
    invoke-direct {v7, p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$1;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;I)V

    .line 37
    .line 38
    .line 39
    move v4, p1

    .line 40
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/app/DialogUtils;->o8(Landroid/app/Activity;Ljava/lang/String;IIZLjava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8o〇〇8080()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 6
    .line 7
    if-eqz v1, :cond_1

    .line 8
    .line 9
    instance-of v1, v1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 10
    .line 11
    if-nez v1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->OoO8()V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;->O0()V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇()V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->notifyDataSetChanged()V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o〇〇8080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O00(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Ooo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O8〇OO〇()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$5;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$5;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f1301e7

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇oo(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic 〇Oo〇o8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oO〇8O8oOo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O〇80o08O(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Lcom/intsig/camscanner/datastruct/FolderItem;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO8〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇O〇〇O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇o0O0O8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o8OO0()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o88O8(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/teampermission/TeamPermissionUtil;->O8(I)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    return v0

    .line 15
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO:Z

    .line 16
    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o8oO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/adapter/FolderAdapterInterface;->〇〇888()Landroid/database/Cursor;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 16
    .line 17
    invoke-interface {v1}, Lcom/intsig/camscanner/adapter/FolderAdapterInterface;->〇o〇()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 22
    .line 23
    invoke-interface {v2}, Lcom/intsig/camscanner/adapter/FolderAdapterInterface;->〇8o8o〇()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    add-int/2addr v1, v2

    .line 28
    if-nez v1, :cond_0

    .line 29
    .line 30
    const/4 v1, 0x1

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 v1, 0x0

    .line 33
    :goto_0
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇000〇〇08(Z)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oo(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f1300a9

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 15
    .line 16
    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$9;

    .line 24
    .line 25
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$9;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 26
    .line 27
    .line 28
    const v1, 0x7f13057e

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1, p1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 32
    .line 33
    .line 34
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$10;

    .line 35
    .line 36
    invoke-direct {p1, p0, p2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$10;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Landroid/content/DialogInterface$OnClickListener;)V

    .line 37
    .line 38
    .line 39
    const p2, 0x7f131e36

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, p2, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇oo〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇o〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇o〇Oo0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇〇00OO(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇0o(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇〇o8(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇〇(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇o0o(Ljava/lang/String;)J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/utils/DateTimeUtil;->〇8o8o〇(JJ)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "do not dirSendMsg same day duuId:$duuId"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    return-void

    .line 23
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/sharedir/ShareDirApiSync;->〇080:Lcom/intsig/camscanner/sharedir/ShareDirApiSync;

    .line 24
    .line 25
    const-string v1, "1"

    .line 26
    .line 27
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/sharedir/ShareDirApiSync;->o〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 34
    .line 35
    .line 36
    move-result-wide v0

    .line 37
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇Oo0〇O0(Ljava/lang/String;J)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇〇0880()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->〇o〇(Landroid/content/Context;[J)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$6;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$6;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 14
    .line 15
    .line 16
    const v1, 0x7f1301e6

    .line 17
    .line 18
    .line 19
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇oo(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 30
    .line 31
    invoke-static {v0, v1}, Lcom/intsig/camscanner/morc/util/MoveOrCopyUtils;->O8(Landroid/content/Context;[J)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$7;

    .line 38
    .line 39
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$7;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 40
    .line 41
    .line 42
    const v1, 0x7f1301e5

    .line 43
    .line 44
    .line 45
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇oo(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇8()V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 54
    .line 55
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->o〇0()V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;Lcom/intsig/camscanner/adapter/FolderAdapterInterface;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O08O0〇O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 9
    .line 10
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/adapter/FolderAdapterInterface;->OO0o〇〇(Landroid/database/Cursor;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88〇〇o0O()V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 26
    .line 27
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8〇OO:I

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 38
    .line 39
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->o8〇OO:I

    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo0〇Ooo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 60
    .line 61
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 67
    .line 68
    const-string v2, "updateLoader"

    .line 69
    .line 70
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public O0o8〇O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O0〇oo()[J
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8888(Lcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8O0O808〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8OO08o(Landroid/widget/TextView;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇00O0:Z

    .line 5
    .line 6
    const v1, 0x7f130131

    .line 7
    .line 8
    .line 9
    if-nez v0, :cond_2

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo〇()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-lez v0, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 33
    .line 34
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-nez v0, :cond_3

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 41
    .line 42
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_3
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(I)V

    .line 47
    .line 48
    .line 49
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 50
    .line 51
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->〇o00〇〇Oo()V

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 55
    .line 56
    if-eqz p1, :cond_4

    .line 57
    .line 58
    instance-of p1, p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 59
    .line 60
    if-eqz p1, :cond_4

    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    check-cast p1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 67
    .line 68
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->oo08OO〇0()V

    .line 69
    .line 70
    .line 71
    :cond_4
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public O8O〇8oo08(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇08oOOO0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8oOo80()Lcom/intsig/camscanner/adapter/FolderAdapterInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOo88OOo(Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;)V
    .locals 1

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o0O:Ljava/lang/String;

    .line 4
    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇08O:Ljava/lang/String;

    .line 6
    .line 7
    iget-object v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->〇o〇:Ljava/lang/String;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇o〇8()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->O8:Ljava/lang/String;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 17
    .line 18
    iget v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->Oo08:I

    .line 19
    .line 20
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O0O:I

    .line 21
    .line 22
    iget v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->o〇0:I

    .line 23
    .line 24
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8oOOo:I

    .line 25
    .line 26
    iget v0, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->〇〇888:I

    .line 27
    .line 28
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇O〇〇O8:I

    .line 29
    .line 30
    iget p1, p1, Lcom/intsig/camscanner/adapter/FolderAndDocAdapter$TeamEntryItem;->oO80:I

    .line 31
    .line 32
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/teampermission/TeamPermissionUtil;->〇080(I)Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO:Z

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 39
    .line 40
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->Oo08()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇OOo000()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08O8o8()V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O08O0〇O()V

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    check-cast p1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 57
    .line 58
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇o8oO()Landroid/widget/TextView;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 66
    .line 67
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 70
    .line 71
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇080()V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public Oo0oOo〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇oO〇oo8o(Landroid/content/Intent;)V
    .locals 12

    .line 1
    const-string v0, "gen_offline_folder"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->Oo0oOo〇0(Landroid/content/Context;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇oOo〇(Z)V

    .line 22
    .line 23
    .line 24
    const-string v0, "team_token_id"

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8O〇8oo08(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const-string v0, "extra_multi_doc_id"

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇o〇o([J)V

    .line 40
    .line 41
    .line 42
    const-string v0, "extra_folder_id"

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v7

    .line 48
    invoke-virtual {p0, v7}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o0o(Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const-string v0, "extra_offline_folder"

    .line 52
    .line 53
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇O00〇8(Z)V

    .line 58
    .line 59
    .line 60
    const-string v0, "action"

    .line 61
    .line 62
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88O:I

    .line 67
    .line 68
    const-string v0, "EXTRA_FROM_PART"

    .line 69
    .line 70
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oO〇8O8oOo:Ljava/lang/String;

    .line 75
    .line 76
    iget v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O88O:I

    .line 77
    .line 78
    const/4 v2, 0x0

    .line 79
    if-nez v0, :cond_1

    .line 80
    .line 81
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$MoveAction;

    .line 82
    .line 83
    invoke-direct {p1, p0, v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$MoveAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;LOo0O0o8/〇80〇808〇O;)V

    .line 84
    .line 85
    .line 86
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_1
    const/4 v3, 0x5

    .line 90
    if-ne v0, v3, :cond_2

    .line 91
    .line 92
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OnlyGetPathAction;

    .line 93
    .line 94
    invoke-direct {p1, p0, v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OnlyGetPathAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;LOo0O0o8/OO0o〇〇〇〇0;)V

    .line 95
    .line 96
    .line 97
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 98
    .line 99
    goto :goto_1

    .line 100
    :cond_2
    const/4 v3, 0x3

    .line 101
    const-wide/16 v4, -0x1

    .line 102
    .line 103
    if-ne v0, v3, :cond_3

    .line 104
    .line 105
    const-string v0, "extra_move_folder_id"

    .line 106
    .line 107
    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 108
    .line 109
    .line 110
    move-result-wide v8

    .line 111
    const-string v0, "extra_move_folder_title"

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v11

    .line 117
    const-string v0, "extra_move_folder_sync_id"

    .line 118
    .line 119
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v10

    .line 123
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;

    .line 124
    .line 125
    move-object v6, p1

    .line 126
    move-object v7, p0

    .line 127
    invoke-direct/range {v6 .. v11}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;JLjava/lang/String;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 131
    .line 132
    goto :goto_1

    .line 133
    :cond_3
    const/4 v3, 0x4

    .line 134
    if-ne v0, v3, :cond_4

    .line 135
    .line 136
    const-string v0, "extra_other_move_in_folder_id"

    .line 137
    .line 138
    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 139
    .line 140
    .line 141
    move-result-wide v4

    .line 142
    const-string v0, "extra_other_move_in_folder_sync_id"

    .line 143
    .line 144
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v6

    .line 148
    const-string v0, "extra_other_move_in_folder_layer_num"

    .line 149
    .line 150
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 151
    .line 152
    .line 153
    move-result v8

    .line 154
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 155
    .line 156
    move-object v2, p1

    .line 157
    move-object v3, p0

    .line 158
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;JLjava/lang/String;Ljava/lang/String;I)V

    .line 159
    .line 160
    .line 161
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 162
    .line 163
    goto :goto_1

    .line 164
    :cond_4
    const/4 p1, 0x2

    .line 165
    if-ne v0, p1, :cond_5

    .line 166
    .line 167
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;

    .line 168
    .line 169
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;)V

    .line 170
    .line 171
    .line 172
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 173
    .line 174
    goto :goto_1

    .line 175
    :cond_5
    new-instance p1, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$CopyAction;

    .line 176
    .line 177
    invoke-direct {p1, p0, v2}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$CopyAction;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;LOo0O0o8/〇〇888;)V

    .line 178
    .line 179
    .line 180
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 181
    .line 182
    :goto_1
    return-void
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public O〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o08oOO()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o08〇〇0O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->〇0〇O0088o()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 14
    .line 15
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->OO0o〇〇〇〇0()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    add-int/2addr v0, v1

    .line 20
    if-lez v0, :cond_0

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o〇〇8080()V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    const/4 v2, 0x0

    .line 30
    if-eqz v0, :cond_4

    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-lez v0, :cond_4

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    const/4 v3, 0x1

    .line 45
    if-ne v0, v3, :cond_2

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 48
    .line 49
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    sub-int/2addr v4, v3

    .line 54
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-nez v0, :cond_1

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 68
    .line 69
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 70
    .line 71
    :cond_1
    iput-boolean v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 72
    .line 73
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇00O0:Z

    .line 74
    .line 75
    if-eqz v0, :cond_3

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 78
    .line 79
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->Oo0oOo〇0(Landroid/content/Context;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8O0O808〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V

    .line 84
    .line 85
    .line 86
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 88
    .line 89
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    sub-int/2addr v1, v3

    .line 94
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 98
    .line 99
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    sub-int/2addr v1, v3

    .line 104
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 109
    .line 110
    if-eqz v0, :cond_3

    .line 111
    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    iput-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 117
    .line 118
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇〇〇0〇〇0()Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oo8ooo8O:Z

    .line 123
    .line 124
    :cond_3
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 125
    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    .line 127
    .line 128
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .line 130
    .line 131
    const-string v2, "goBack sParentSyncId"

    .line 132
    .line 133
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    iget-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 137
    .line 138
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇OOo000()V

    .line 149
    .line 150
    .line 151
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08O8o8()V

    .line 152
    .line 153
    .line 154
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O08O0〇O()V

    .line 155
    .line 156
    .line 157
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 162
    .line 163
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇o8oO()Landroid/widget/TextView;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 168
    .line 169
    .line 170
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO〇()V

    .line 171
    .line 172
    .line 173
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o〇〇8080()V

    .line 174
    .line 175
    .line 176
    goto :goto_0

    .line 177
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo〇()Z

    .line 178
    .line 179
    .line 180
    move-result v0

    .line 181
    if-eqz v0, :cond_5

    .line 182
    .line 183
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 184
    .line 185
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 186
    .line 187
    iput-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇o88o08〇:Ljava/lang/String;

    .line 188
    .line 189
    iput v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O0O:I

    .line 190
    .line 191
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇o〇8()V

    .line 192
    .line 193
    .line 194
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 195
    .line 196
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->Oo08()V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇OOo000()V

    .line 200
    .line 201
    .line 202
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08O8o8()V

    .line 203
    .line 204
    .line 205
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O08O0〇O()V

    .line 206
    .line 207
    .line 208
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 209
    .line 210
    .line 211
    move-result-object v0

    .line 212
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 213
    .line 214
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇o8oO()Landroid/widget/TextView;

    .line 215
    .line 216
    .line 217
    move-result-object v0

    .line 218
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 219
    .line 220
    .line 221
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 222
    .line 223
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇080()V

    .line 224
    .line 225
    .line 226
    goto :goto_0

    .line 227
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 228
    .line 229
    .line 230
    move-result-object v0

    .line 231
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 232
    .line 233
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇00()V

    .line 234
    .line 235
    .line 236
    :goto_0
    return-void
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public o0oO(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o88O〇8()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    check-cast v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$DirMoveAction;->oO80()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    add-int/2addr v2, v0

    .line 15
    goto :goto_1

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 17
    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    array-length v0, v0

    .line 21
    goto :goto_0

    .line 22
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 23
    .line 24
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoldInterface;->OO0o〇〇〇〇0()I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    add-int/2addr v2, v0

    .line 29
    if-lez v2, :cond_2

    .line 30
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 37
    .line 38
    invoke-interface {v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇o00〇〇Oo()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v1, "("

    .line 46
    .line 47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v1, ")"

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    return-object v0

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 64
    .line 65
    instance-of v1, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OnlyGetPathAction;

    .line 66
    .line 67
    if-eqz v1, :cond_3

    .line 68
    .line 69
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇o00〇〇Oo()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    return-object v0

    .line 74
    :cond_3
    const/4 v0, 0x0

    .line 75
    return-object v0
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o8o〇〇0O()Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO8008O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO8o()Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->〇o〇()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇OOo000()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08O8o8()V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O08O0〇O()V

    .line 8
    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇00O0:Z

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08〇o0O:Z

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->Oo0oOo〇0(Landroid/content/Context;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8O0O808〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V

    .line 26
    .line 27
    .line 28
    const/4 v0, 0x1

    .line 29
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08〇o0O:Z

    .line 30
    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onStop()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇oO:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 6
    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇oO:Z

    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo0O〇0〇〇〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooo8o〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO〇00〇8oO:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇OOo000()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 11
    .line 12
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 19
    .line 20
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$CopyAction;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 25
    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OoOOo8()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇08oOOO0:I

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 48
    .line 49
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 58
    .line 59
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->O〇08oOOO0:I

    .line 68
    .line 69
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooO:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 70
    .line 71
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 77
    .line 78
    const-string v2, "updateFolderInfo"

    .line 79
    .line 80
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇00O:Lcom/intsig/camscanner/adapter/FolderAdapterInterface;

    .line 85
    .line 86
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/adapter/FolderAdapterInterface;->oO80(Landroid/database/Cursor;)V

    .line 87
    .line 88
    .line 89
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o〇〇0〇88()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$OtherMoveInAction;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    return v0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇008〇oo()Lorg/json/JSONObject;
    .locals 3

    .line 1
    :try_start_0
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    const-string v1, "from"

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    .line 28
    :cond_0
    return-object v0

    .line 29
    :catch_0
    move-exception v0

    .line 30
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 31
    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    .line 34
    .line 35
    const/4 v0, 0x0

    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇08O8o8()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o08O()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇00O0:I

    .line 24
    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 26
    .line 27
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 36
    .line 37
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->getContext()Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sget v2, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;->〇00O0:I

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO〇00〇0O:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 48
    .line 49
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 55
    .line 56
    const-string v2, "updateLoader"

    .line 57
    .line 58
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    .line 60
    .line 61
    :goto_0
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇08〇0〇o〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇oO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8O0O808〇(Lcom/intsig/camscanner/datastruct/FolderItem;Z)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 4
    .line 5
    const-string p2, "open folder syncId == null"

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o0oO(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇〇〇0〇〇0()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O0o8〇O(Z)V

    .line 23
    .line 24
    .line 25
    if-eqz p2, :cond_1

    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 28
    .line 29
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    .line 31
    .line 32
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o〇OOo000()V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇08O8o8()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O08O0〇O()V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/mvp/presenter/BasePresenter;->O8()Lcom/intsig/mvp/view/IView;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    check-cast p1, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;

    .line 46
    .line 47
    invoke-interface {p1}, Lcom/intsig/camscanner/morc/contract/MoveOrCopyContract$View;->〇o8oO()Landroid/widget/TextView;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8OO08o(Landroid/widget/TextView;)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->OO〇()V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 58
    .line 59
    new-instance p2, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v0, "mParentSyncId="

    .line 65
    .line 66
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 70
    .line 71
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    :goto_0
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇8〇o〇8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->Oo80:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;

    .line 11
    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;

    .line 15
    .line 16
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;LOo0O0o8/〇O8o08O;)V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;

    .line 20
    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOO〇〇:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$PersonalMold;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;

    .line 27
    .line 28
    if-nez v0, :cond_2

    .line 29
    .line 30
    new-instance v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;

    .line 31
    .line 32
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;-><init>(Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;LOo0O0o8/OO0o〇〇;)V

    .line 33
    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;

    .line 36
    .line 37
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8o:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$TeamMold;

    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 40
    .line 41
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0OO8(Landroid/content/Context;)I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    iput v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇OO8ooO8〇:I

    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇OO8Oo0〇()Lcom/intsig/camscanner/morc/contract/MoldInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo0:Lcom/intsig/camscanner/morc/contract/MoldInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oO8O0〇〇O(Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_4

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇80()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_2

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->Oooo8o0〇(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    const/4 v2, 0x0

    .line 27
    if-eqz v1, :cond_2

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 43
    .line 44
    .line 45
    move-result-wide v3

    .line 46
    const/4 v1, 0x0

    .line 47
    invoke-static {v0, v3, v4, v2, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OoO〇(Landroid/content/Context;JLjava/lang/String;Z)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    goto :goto_1

    .line 52
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 55
    .line 56
    .line 57
    move-result-wide v3

    .line 58
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o8OO0(Landroid/content/Context;J)Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    :goto_1
    if-eqz v0, :cond_3

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 65
    .line 66
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->o〇8(Lcom/intsig/camscanner/datastruct/DocItem;)V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇()V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O8o08O8O:Lcom/intsig/camscanner/adapter/FolderAndDocAdapter;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/adapter/MultiChoiceCursorAdapter;->notifyDataSetChanged()V

    .line 75
    .line 76
    .line 77
    goto :goto_2

    .line 78
    :cond_3
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 81
    .line 82
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    .line 84
    .line 85
    const v0, 0x7f13041e

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 89
    .line 90
    .line 91
    move-result-object p1

    .line 92
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 93
    .line 94
    const v1, 0x7f130291

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    const v0, 0x7f131e36

    .line 106
    .line 107
    .line 108
    invoke-virtual {p1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 117
    .line 118
    .line 119
    :cond_4
    :goto_2
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇oOo〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇00O0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o〇o([J)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o8〇OO0〇0o:[J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇00O〇0o(I)V
    .locals 4

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O〇O00O:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->o0:Ljava/lang/String;

    .line 4
    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "User Operation: create new folder dirType:"

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 26
    .line 27
    instance-of v0, v0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$ChoseAction;

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    const-string v0, "CSSelectPath"

    .line 32
    .line 33
    const-string v1, "create_folder"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇o〇:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v0, v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    const/4 v2, 0x1

    .line 51
    if-ne v1, v2, :cond_3

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    if-nez v1, :cond_3

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-static {v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇〇8O0〇8(Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/data/SharedDirEntryData;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-static {v2}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->〇〇888(Ljava/lang/String;)I

    .line 76
    .line 77
    .line 78
    move-result v2

    .line 79
    if-eqz v1, :cond_2

    .line 80
    .line 81
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/SharedDirEntryData;->getDir_num()Ljava/lang/Integer;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    if-ge v2, v3, :cond_1

    .line 90
    .line 91
    iget-object v2, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 92
    .line 93
    if-eqz v2, :cond_2

    .line 94
    .line 95
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    invoke-virtual {v1}, Lcom/intsig/camscanner/sharedir/data/SharedDirEntryData;->getDir_layer()Ljava/lang/Integer;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    if-lt v2, v1, :cond_2

    .line 108
    .line 109
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->o88o0O(Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o8O〇O(I)V

    .line 118
    .line 119
    .line 120
    :goto_0
    return-void

    .line 121
    :cond_3
    if-le p1, v2, :cond_4

    .line 122
    .line 123
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->O〇8oOo8O(I)V

    .line 124
    .line 125
    .line 126
    return-void

    .line 127
    :cond_4
    iget-object v0, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 128
    .line 129
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇Oo(Landroid/content/Context;)I

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 134
    .line 135
    invoke-static {v1}, Lcom/intsig/camscanner/app/DBUtil;->〇oo(Landroid/content/Context;)I

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    if-ge v1, v0, :cond_9

    .line 140
    .line 141
    invoke-static {}, Lcom/intsig/huaweipaylib/HuaweiPayConfig;->〇o00〇〇Oo()Z

    .line 142
    .line 143
    .line 144
    move-result v0

    .line 145
    if-eqz v0, :cond_5

    .line 146
    .line 147
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o8O〇O(I)V

    .line 148
    .line 149
    .line 150
    goto :goto_1

    .line 151
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 152
    .line 153
    .line 154
    move-result v0

    .line 155
    if-eqz v0, :cond_6

    .line 156
    .line 157
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o8O〇O(I)V

    .line 158
    .line 159
    .line 160
    goto :goto_1

    .line 161
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->ooo0〇〇O:Ljava/util/ArrayList;

    .line 162
    .line 163
    if-eqz v0, :cond_7

    .line 164
    .line 165
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 166
    .line 167
    .line 168
    move-result v0

    .line 169
    iget-object v1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 170
    .line 171
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0O〇8o0O(Landroid/content/Context;)I

    .line 172
    .line 173
    .line 174
    move-result v1

    .line 175
    if-ge v0, v1, :cond_7

    .line 176
    .line 177
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8o8O〇O(I)V

    .line 178
    .line 179
    .line 180
    goto :goto_1

    .line 181
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 182
    .line 183
    .line 184
    move-result p1

    .line 185
    if-eqz p1, :cond_8

    .line 186
    .line 187
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 188
    .line 189
    invoke-static {p1}, Lcom/intsig/camscanner/app/DialogUtils;->〇80(Landroid/content/Context;)V

    .line 190
    .line 191
    .line 192
    goto :goto_1

    .line 193
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O00oO()V

    .line 194
    .line 195
    .line 196
    goto :goto_1

    .line 197
    :cond_9
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 198
    .line 199
    .line 200
    move-result p1

    .line 201
    if-eqz p1, :cond_a

    .line 202
    .line 203
    iget-object p1, p0, Lcom/intsig/mvp/presenter/BasePresenter;->〇08O〇00〇o:Landroid/content/Context;

    .line 204
    .line 205
    invoke-static {p1}, Lcom/intsig/camscanner/app/DialogUtils;->〇80(Landroid/content/Context;)V

    .line 206
    .line 207
    .line 208
    goto :goto_1

    .line 209
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇0O00oO()V

    .line 210
    .line 211
    .line 212
    :goto_1
    return-void
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇〇0o8O〇〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter$Action;->O8()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇0〇0o8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oO〇8O8oOo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇O00〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->oOo〇8o008:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇o0o(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/morc/presenter/MoveOrCopyPresenter;->〇8〇oO〇〇8o:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

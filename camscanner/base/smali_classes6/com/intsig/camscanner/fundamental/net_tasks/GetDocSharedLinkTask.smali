.class public Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;
.super Landroid/os/AsyncTask;
.source "GetDocSharedLinkTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/util/ArrayList<",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;

.field private Oo08:J

.field private oO80:I

.field private o〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080:Lcom/intsig/app/BaseProgressDialog;

.field private 〇o00〇〇Oo:Landroid/content/Context;

.field private 〇o〇:Ljava/lang/String;

.field private 〇〇888:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v8, p3

    .line 1
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;JILcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;JILcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "JI",
            "Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;",
            ")V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShareSecureLinkTask mExpiredTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->Oo08:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetDocSharedLinkTask"

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 5
    iput-object p2, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->o〇0:Ljava/util/ArrayList;

    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o〇:Ljava/lang/String;

    .line 7
    iput-wide p5, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->Oo08:J

    .line 8
    iput-object p8, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->O8:Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;

    .line 9
    iput-object p3, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇〇888:Ljava/util/ArrayList;

    .line 10
    iput p7, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->oO80:I

    return-void
.end method

.method private Oo08()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 20
    .line 21
    const v2, 0x7f13008a

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_1

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 42
    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇080()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :catch_0
    move-exception v0

    .line 10
    const-string v1, "GetDocSharedLinkTask"

    .line 11
    .line 12
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇〇888:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method protected O8(Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇080()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->O8:Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->O8()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->O8:Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;

    .line 21
    .line 22
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;->〇080(Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 27
    .line 28
    const v0, 0x7f13029d

    .line 29
    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 32
    .line 33
    .line 34
    const-string p1, "GetDocSharedLinkTask"

    .line 35
    .line 36
    const-string v0, "mListener or docShareLinkInfo is null"

    .line 37
    .line 38
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->O8:Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;

    .line 42
    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    invoke-interface {p1}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask$OnResultListener;->onFailure()V

    .line 46
    .line 47
    .line 48
    :cond_1
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo([Ljava/util/ArrayList;)Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->O8(Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onPreExecute()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->Oo08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected varargs 〇o00〇〇Oo([Ljava/util/ArrayList;)Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->o〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Ljava/lang/Long;

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const-string v1, "GetDocSharedLinkTask"

    .line 15
    .line 16
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->o〇0:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-static {p1, v2, v0}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇oo〇(Landroid/content/Context;Ljava/util/List;Z)Ljava/util/ArrayList;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    const/4 v2, 0x0

    .line 32
    if-lez p1, :cond_0

    .line 33
    .line 34
    new-instance p1, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v3, "docSyncIds="

    .line 40
    .line 41
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    check-cast v3, Ljava/lang/String;

    .line 49
    .line 50
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 61
    .line 62
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    check-cast v3, Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {p1, v3}, Lcom/intsig/camscanner/app/DBUtil;->OOo0O(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v5, " teamToken ="

    .line 78
    .line 79
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_0
    move-object p1, v2

    .line 94
    :goto_0
    const/4 v10, 0x0

    .line 95
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 96
    .line 97
    .line 98
    move-result v3

    .line 99
    if-eqz v3, :cond_2

    .line 100
    .line 101
    iget-object v3, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 102
    .line 103
    iget-object v5, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇〇888:Ljava/util/ArrayList;

    .line 104
    .line 105
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v6

    .line 109
    iget-object v8, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o〇:Ljava/lang/String;

    .line 110
    .line 111
    iget-wide v9, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->Oo08:J

    .line 112
    .line 113
    iget v11, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->oO80:I

    .line 114
    .line 115
    const/4 v7, 0x0

    .line 116
    invoke-static/range {v3 .. v11}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo〇O(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;ILjava/lang/String;JI)Ljava/util/ArrayList;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    invoke-static {v3}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 121
    .line 122
    .line 123
    move-result v4

    .line 124
    if-eqz v4, :cond_1

    .line 125
    .line 126
    return-object v2

    .line 127
    :cond_1
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    check-cast v0, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;

    .line 132
    .line 133
    goto :goto_1

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o〇()Z

    .line 135
    .line 136
    .line 137
    move-result v3

    .line 138
    if-eqz v3, :cond_3

    .line 139
    .line 140
    iget-object v4, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇〇888:Ljava/util/ArrayList;

    .line 141
    .line 142
    :cond_3
    move-object v6, v4

    .line 143
    iget-object v5, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 144
    .line 145
    invoke-direct {p0}, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o〇()Z

    .line 146
    .line 147
    .line 148
    move-result v7

    .line 149
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object v8

    .line 153
    iget-object v11, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o〇:Ljava/lang/String;

    .line 154
    .line 155
    iget-wide v12, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->Oo08:J

    .line 156
    .line 157
    move-object v9, p1

    .line 158
    invoke-static/range {v5 .. v13}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0(Landroid/content/Context;Ljava/util/ArrayList;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)Ljava/util/ArrayList;

    .line 159
    .line 160
    .line 161
    move-result-object v3

    .line 162
    invoke-static {v3}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 163
    .line 164
    .line 165
    move-result v4

    .line 166
    if-eqz v4, :cond_4

    .line 167
    .line 168
    return-object v2

    .line 169
    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    check-cast v0, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;

    .line 174
    .line 175
    :goto_1
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;->〇0000OOO(Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    sget-object p1, Lcom/intsig/camscanner/fundamental/net_tasks/DocSharedLinkTaskHelper;->〇080:Lcom/intsig/camscanner/fundamental/net_tasks/DocSharedLinkTaskHelper$Companion;

    .line 179
    .line 180
    iget-object v2, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 181
    .line 182
    iget-object v3, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->o〇0:Ljava/util/ArrayList;

    .line 183
    .line 184
    invoke-virtual {p1, v2, v0, v3}, Lcom/intsig/camscanner/fundamental/net_tasks/DocSharedLinkTaskHelper$Companion;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/fundamental/net_tasks/DocShareLinkInfo;Ljava/util/ArrayList;)V

    .line 185
    .line 186
    .line 187
    new-instance p1, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    const-string v2, "DocShareLinkInfo is ready now!, mType = "

    .line 193
    .line 194
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    iget v2, p0, Lcom/intsig/camscanner/fundamental/net_tasks/GetDocSharedLinkTask;->oO80:I

    .line 198
    .line 199
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object p1

    .line 206
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    .line 208
    .line 209
    return-object v0
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

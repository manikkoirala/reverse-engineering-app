.class public abstract Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;
.super Ljava/lang/Object;
.source "IMultiImageActionClient.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "actionInfo"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇080:Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public abstract O8()Z
.end method

.method public abstract OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method protected final Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->〇080:Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public abstract oO80()Z
.end method

.method public abstract o〇0(IILandroid/content/Intent;)Z
.end method

.method public abstract 〇080()Z
.end method

.method public abstract 〇80〇808〇O(Landroid/view/View;)Z
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇o00〇〇Oo(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z
    .param p1    # Landroidx/fragment/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇o〇()Z
.end method

.method public abstract 〇〇888()Z
.end method

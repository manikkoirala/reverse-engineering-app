.class public final Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;
.super Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;
.source "BookModeActionClient.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "actionInfo"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;-><init>(Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇()I
    .locals 7

    .line 1
    const-string v0, "deleteCurrentItem START!"

    .line 2
    .line 3
    const-string v1, "BookModeActionClient"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->getCurrentPosition()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->Oooo8o0〇()Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    invoke-virtual {v2}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇8o8o〇()Ljava/util/List;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 25
    .line 26
    .line 27
    move-result v4

    .line 28
    add-int/lit8 v4, v4, -0x1

    .line 29
    .line 30
    new-instance v5, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v6, "deleteCurrentItem currentItem:"

    .line 36
    .line 37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v6, " lastIndex="

    .line 44
    .line 45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v5

    .line 55
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    const/4 v1, -0x1

    .line 59
    if-ltz v4, :cond_3

    .line 60
    .line 61
    if-ltz v0, :cond_3

    .line 62
    .line 63
    if-gt v0, v4, :cond_3

    .line 64
    .line 65
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object v3

    .line 69
    check-cast v3, Ljava/lang/String;

    .line 70
    .line 71
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->OoO8(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    invoke-interface {v3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->〇o〇()Ljava/util/List;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 86
    .line 87
    .line 88
    if-ne v0, v4, :cond_0

    .line 89
    .line 90
    if-eqz v0, :cond_0

    .line 91
    .line 92
    add-int/lit8 v0, v0, -0x1

    .line 93
    .line 94
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇8o8o〇()Ljava/util/List;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    if-nez v2, :cond_2

    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    if-eqz v0, :cond_1

    .line 113
    .line 114
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 115
    .line 116
    .line 117
    :cond_1
    return v1

    .line 118
    :cond_2
    return v0

    .line 119
    :cond_3
    return v1
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;)V
    .locals 11
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    const-string v0, "editBookReplaceImage: START!"

    .line 2
    .line 3
    const-string v1, "BookModeActionClient"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    new-instance p1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v0, "editBookReplaceImage: ERROR newPath="

    .line 20
    .line 21
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇080:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 36
    .line 37
    if-eqz v0, :cond_c

    .line 38
    .line 39
    new-instance v2, Ljava/util/ArrayList;

    .line 40
    .line 41
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->Oo08()Ljava/util/List;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    const/4 v4, -0x1

    .line 53
    const/4 v5, 0x2

    .line 54
    const/4 v6, 0x0

    .line 55
    const/4 v7, 0x1

    .line 56
    if-lt v3, v5, :cond_1

    .line 57
    .line 58
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 59
    .line 60
    if-eqz v3, :cond_1

    .line 61
    .line 62
    const/4 v3, 0x0

    .line 63
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->Oo08()Ljava/util/List;

    .line 65
    .line 66
    .line 67
    move-result-object v3

    .line 68
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    if-lt v3, v5, :cond_2

    .line 73
    .line 74
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 75
    .line 76
    if-eqz v3, :cond_2

    .line 77
    .line 78
    const/4 v3, 0x1

    .line 79
    goto :goto_0

    .line 80
    :cond_2
    const/4 v3, -0x1

    .line 81
    :goto_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->Oo08()Ljava/util/List;

    .line 82
    .line 83
    .line 84
    move-result-object v5

    .line 85
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 86
    .line 87
    .line 88
    move-result v5

    .line 89
    iget-object v8, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 90
    .line 91
    if-eqz v8, :cond_3

    .line 92
    .line 93
    const/4 v8, 0x1

    .line 94
    goto :goto_1

    .line 95
    :cond_3
    const/4 v8, 0x0

    .line 96
    :goto_1
    iget-object v9, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 97
    .line 98
    if-eqz v9, :cond_4

    .line 99
    .line 100
    const/4 v6, 0x1

    .line 101
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    const-string v10, "turnLeft: currentPage="

    .line 107
    .line 108
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    const-string v10, ", size="

    .line 115
    .line 116
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    const-string v5, ", nextNonNull="

    .line 123
    .line 124
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    const-string v5, ", preNonNull="

    .line 131
    .line 132
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v5

    .line 142
    invoke-static {v1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    if-eq v3, v4, :cond_a

    .line 146
    .line 147
    const/4 v1, 0x0

    .line 148
    if-eqz v3, :cond_8

    .line 149
    .line 150
    if-eq v3, v7, :cond_5

    .line 151
    .line 152
    goto :goto_2

    .line 153
    :cond_5
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 154
    .line 155
    if-eqz p1, :cond_7

    .line 156
    .line 157
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 158
    .line 159
    if-eqz p1, :cond_7

    .line 160
    .line 161
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 162
    .line 163
    if-eqz p1, :cond_7

    .line 164
    .line 165
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 166
    .line 167
    .line 168
    move-result v3

    .line 169
    xor-int/2addr v3, v7

    .line 170
    if-eqz v3, :cond_6

    .line 171
    .line 172
    move-object v1, p1

    .line 173
    :cond_6
    if-eqz v1, :cond_7

    .line 174
    .line 175
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    :cond_7
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    .line 180
    .line 181
    goto :goto_2

    .line 182
    :cond_8
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    .line 184
    .line 185
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 186
    .line 187
    if-eqz p1, :cond_b

    .line 188
    .line 189
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 190
    .line 191
    if-eqz p1, :cond_b

    .line 192
    .line 193
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 194
    .line 195
    if-eqz p1, :cond_b

    .line 196
    .line 197
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 198
    .line 199
    .line 200
    move-result p2

    .line 201
    xor-int/2addr p2, v7

    .line 202
    if-eqz p2, :cond_9

    .line 203
    .line 204
    move-object v1, p1

    .line 205
    :cond_9
    if-eqz v1, :cond_b

    .line 206
    .line 207
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    goto :goto_2

    .line 211
    :cond_a
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    :cond_b
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->Oooo8o0〇()Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;

    .line 215
    .line 216
    .line 217
    move-result-object p1

    .line 218
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇80〇808〇O(Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;Ljava/util/List;)V

    .line 219
    .line 220
    .line 221
    :cond_c
    return-void
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;->OO0o〇〇()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;->Oooo8o0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O8()Z
    .locals 3

    .line 1
    const-string v0, "BookModeActionClient"

    .line 2
    .line 3
    const-string v1, "finishScan: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->〇〇888()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$finishScan$1;

    .line 20
    .line 21
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$finishScan$1;-><init>(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;Landroidx/appcompat/app/AppCompatActivity;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const/4 v2, 0x0

    .line 29
    new-array v2, v2, [Ljava/lang/Void;

    .line 30
    .line 31
    invoke-virtual {v1, v0, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public OO0o〇〇〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z
    .locals 4
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "page"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "turnLeft: START-"

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const-string v1, "BookModeActionClient"

    .line 28
    .line 29
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->〇80〇808〇O()V

    .line 33
    .line 34
    .line 35
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-interface {v1}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$turnLeft$task$1;

    .line 46
    .line 47
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$turnLeft$task$1;-><init>(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 48
    .line 49
    .line 50
    const-string p1, ""

    .line 51
    .line 52
    const/4 v3, 0x0

    .line 53
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 54
    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->〇8o8o〇()Ljava/util/concurrent/ExecutorService;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    new-array v1, v3, [Ljava/lang/Void;

    .line 61
    .line 62
    invoke-virtual {v0, p1, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    .line 64
    .line 65
    const/4 p1, 0x1

    .line 66
    return p1
    .line 67
.end method

.method public oO80()Z
    .locals 2

    .line 1
    const-string v0, "BookModeActionClient"

    .line 2
    .line 3
    const-string v1, "onEnterLogAgent: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->Oo08()V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(IILandroid/content/Intent;)Z
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "handleOnActivityResult: START, resultCode="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ", resultCode="

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "BookModeActionClient"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    const/16 v0, 0x72

    .line 32
    .line 33
    const/4 v2, 0x1

    .line 34
    const/4 v3, 0x0

    .line 35
    if-eq p1, v0, :cond_3

    .line 36
    .line 37
    const/16 p2, 0x73

    .line 38
    .line 39
    if-eq p1, p2, :cond_0

    .line 40
    .line 41
    const/4 v2, 0x0

    .line 42
    goto/16 :goto_3

    .line 43
    .line 44
    :cond_0
    const-string p1, "handleOnActivityResult: REQ_PAGE_SINGLE_BOOK_EDIT "

    .line 45
    .line 46
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    if-eqz p3, :cond_1

    .line 50
    .line 51
    const-string p1, "response_extra_key_book_model"

    .line 52
    .line 53
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    goto :goto_0

    .line 58
    :cond_1
    move-object p1, v3

    .line 59
    :goto_0
    instance-of p2, p1, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 60
    .line 61
    if-eqz p2, :cond_2

    .line 62
    .line 63
    move-object v3, p1

    .line 64
    check-cast v3, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 65
    .line 66
    :cond_2
    if-eqz v3, :cond_7

    .line 67
    .line 68
    :try_start_0
    invoke-static {v3}, Lcom/intsig/camscanner/booksplitter/Util/BookPreviewUtil;->〇o00〇〇Oo(Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)Ljava/util/List;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    invoke-interface {p2}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->〇o〇()Ljava/util/List;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 81
    .line 82
    .line 83
    move-result-object p3

    .line 84
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->getCurrentPosition()I

    .line 85
    .line 86
    .line 87
    move-result p3

    .line 88
    invoke-static {p2, p3}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    check-cast p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 93
    .line 94
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 95
    .line 96
    .line 97
    move-result-object p3

    .line 98
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->〇o〇()Ljava/util/List;

    .line 99
    .line 100
    .line 101
    move-result-object p3

    .line 102
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->getCurrentPosition()I

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    check-cast p1, Ljava/util/Collection;

    .line 111
    .line 112
    invoke-interface {p3, v0, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 113
    .line 114
    .line 115
    new-instance p1, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$handleOnActivityResult$1$removePage$1;

    .line 116
    .line 117
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$handleOnActivityResult$1$removePage$1;-><init>(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;)V

    .line 118
    .line 119
    .line 120
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .line 122
    .line 123
    goto :goto_3

    .line 124
    :cond_3
    const-string p1, "handleOnActivityResult: REQ_PAGE_DOUBLE_BOOK_EDIT "

    .line 125
    .line 126
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    if-eqz p3, :cond_7

    .line 130
    .line 131
    const/4 p1, -0x1

    .line 132
    if-ne p2, p1, :cond_7

    .line 133
    .line 134
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    if-eqz p1, :cond_4

    .line 139
    .line 140
    const-string p2, "extra_booksplittermodel"

    .line 141
    .line 142
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    check-cast p1, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 147
    .line 148
    goto :goto_1

    .line 149
    :cond_4
    move-object p1, v3

    .line 150
    :goto_1
    invoke-static {p1}, Lcom/intsig/camscanner/booksplitter/Util/BookPreviewUtil;->〇o00〇〇Oo(Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)Ljava/util/List;

    .line 151
    .line 152
    .line 153
    move-result-object p1

    .line 154
    :try_start_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 155
    .line 156
    .line 157
    move-result-object p2

    .line 158
    invoke-interface {p2}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->〇o〇()Ljava/util/List;

    .line 159
    .line 160
    .line 161
    move-result-object p2

    .line 162
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 163
    .line 164
    .line 165
    move-result-object p3

    .line 166
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->getCurrentPosition()I

    .line 167
    .line 168
    .line 169
    move-result p3

    .line 170
    invoke-static {p2, p3}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 171
    .line 172
    .line 173
    move-result-object p2

    .line 174
    check-cast p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 175
    .line 176
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 177
    .line 178
    .line 179
    move-result-object p3

    .line 180
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->〇o〇()Ljava/util/List;

    .line 181
    .line 182
    .line 183
    move-result-object p3

    .line 184
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->getCurrentPosition()I

    .line 189
    .line 190
    .line 191
    move-result v0

    .line 192
    check-cast p1, Ljava/util/Collection;

    .line 193
    .line 194
    invoke-interface {p3, v0, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 195
    .line 196
    .line 197
    new-instance p1, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$handleOnActivityResult$removePage$1;

    .line 198
    .line 199
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$handleOnActivityResult$removePage$1;-><init>(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;)V

    .line 200
    .line 201
    .line 202
    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    if-eqz p2, :cond_5

    .line 206
    .line 207
    iget-object p3, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 208
    .line 209
    goto :goto_2

    .line 210
    :cond_5
    move-object p3, v3

    .line 211
    :goto_2
    invoke-interface {p1, p3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    .line 213
    .line 214
    if-eqz p2, :cond_6

    .line 215
    .line 216
    iget-object v3, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 217
    .line 218
    :cond_6
    invoke-interface {p1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 219
    .line 220
    .line 221
    :catchall_0
    :cond_7
    :goto_3
    return v2
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public 〇080()Z
    .locals 2

    .line 1
    const-string v0, "BookModeActionClient"

    .line 2
    .line 3
    const-string v1, "addPage: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->oO80(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 23
    .line 24
    .line 25
    :cond_0
    const/4 v0, 0x1

    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇80〇808〇O(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "rootView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "BookModeActionClient"

    .line 7
    .line 8
    const-string v1, "provideBottomActionUi: START"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    new-array v1, v0, [Landroid/view/View;

    .line 15
    .line 16
    const v2, 0x7f0a01f5

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const/4 v2, 0x0

    .line 24
    aput-object p1, v1, v2

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 27
    .line 28
    .line 29
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o00〇〇Oo(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)Z
    .locals 10
    .param p1    # Landroidx/fragment/app/Fragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "fragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "docInfo"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "page"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "crop: START"

    .line 17
    .line 18
    const-string v1, "BookModeActionClient"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->O8()V

    .line 24
    .line 25
    .line 26
    iget-object v0, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->〇080:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->O8()I

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    new-instance v4, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v5, "crop doublePage="

    .line 41
    .line 42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->O8()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-eq v1, v2, :cond_2

    .line 60
    .line 61
    iget-object p3, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 62
    .line 63
    if-eqz p3, :cond_3

    .line 64
    .line 65
    new-instance v1, Ljava/util/ArrayList;

    .line 66
    .line 67
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .line 69
    .line 70
    iget-wide v3, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 71
    .line 72
    iget-object v5, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 73
    .line 74
    iget v6, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    .line 77
    .line 78
    .line 79
    move-result-object v7

    .line 80
    const/4 v8, 0x0

    .line 81
    if-eqz v7, :cond_0

    .line 82
    .line 83
    const-string v9, "arrayBorders"

    .line 84
    .line 85
    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    invoke-static {v7, v8}, Lkotlin/collections/ArraysKt;->〇0000OOO([Ljava/lang/Object;I)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    move-result-object v7

    .line 92
    check-cast v7, [I

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_0
    const/4 v7, 0x0

    .line 96
    :goto_0
    invoke-static {v3, v4, v5, v6, v7}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->O8(JLjava/lang/String;I[I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    const-string v4, "createPagePara(\n        \u2026                        )"

    .line 101
    .line 102
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    new-instance v3, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;

    .line 109
    .line 110
    invoke-direct {v3}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;-><init>()V

    .line 111
    .line 112
    .line 113
    invoke-virtual {v3, v8}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->〇O00(I)V

    .line 114
    .line 115
    .line 116
    iget-object v4, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 117
    .line 118
    iget v5, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 119
    .line 120
    invoke-virtual {v3, v4, v5}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->Oooo8o0〇(Ljava/lang/String;I)V

    .line 121
    .line 122
    .line 123
    iget-object v4, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 124
    .line 125
    if-eqz v4, :cond_1

    .line 126
    .line 127
    iget-object p3, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 128
    .line 129
    invoke-virtual {v3, p3, v4}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->OO0o〇〇〇〇0(Ljava/lang/String;[I)V

    .line 130
    .line 131
    .line 132
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 133
    .line 134
    .line 135
    move-result-object p3

    .line 136
    invoke-interface {p3}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 137
    .line 138
    .line 139
    move-result-object p3

    .line 140
    const/4 v4, 0x6

    .line 141
    invoke-static {p3, p2, v3, v4, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;->O0〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;ILjava/util/List;)Landroid/content/Intent;

    .line 142
    .line 143
    .line 144
    move-result-object p2

    .line 145
    if-eqz p2, :cond_3

    .line 146
    .line 147
    const-string p3, "request_extra_key_book_model"

    .line 148
    .line 149
    invoke-virtual {p2, p3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 150
    .line 151
    .line 152
    const/16 p3, 0x73

    .line 153
    .line 154
    invoke-static {p1, p2, p3}, Lcom/intsig/utils/TransitionUtil;->O8(Landroidx/fragment/app/Fragment;Landroid/content/Intent;I)V

    .line 155
    .line 156
    .line 157
    goto :goto_1

    .line 158
    :cond_2
    const/16 p2, 0x72

    .line 159
    .line 160
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/booksplitter/BookEditActivity;->O0〇(Landroidx/fragment/app/Fragment;ILcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V

    .line 161
    .line 162
    .line 163
    :cond_3
    :goto_1
    return v2
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public 〇o〇()Z
    .locals 7

    .line 1
    const-string v0, "BookModeActionClient"

    .line 2
    .line 3
    const-string v1, "deleteOnePage: START!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->〇080()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/action/IMultiImageActionClient;->Oo08()Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/multiimageedit/action/ICommonActionInfo;->O8()Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    const/4 v3, 0x0

    .line 32
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$deleteOnePage$1;

    .line 33
    .line 34
    const/4 v0, 0x0

    .line 35
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient$deleteOnePage$1;-><init>(Lcom/intsig/camscanner/multiimageedit/action/BookModeActionClient;Lkotlin/coroutines/Continuation;)V

    .line 36
    .line 37
    .line 38
    const/4 v5, 0x2

    .line 39
    const/4 v6, 0x0

    .line 40
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 41
    .line 42
    .line 43
    :cond_0
    const/4 v0, 0x1

    .line 44
    return v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->〇o〇()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/capture/logagent/CaptureLogAgent;->〇o00〇〇Oo()V

    .line 5
    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

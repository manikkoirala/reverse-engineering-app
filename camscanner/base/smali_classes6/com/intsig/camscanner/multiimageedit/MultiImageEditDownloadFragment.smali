.class public final Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MultiImageEditDownloadFragment.kt"

# interfaces
.implements Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$Companion;,
        Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$UpdateAdjustProgressCallback;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOO0880O:Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOoo80oO:Ljava/lang/String;


# instance fields
.field private O0O:Landroid/widget/CheckBox;

.field private volatile O88O:I

.field private O8o08O8O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

.field private OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

.field private Oo0〇Ooo:I

.field private final Oo80:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlinx/coroutines/Deferred<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Ooo08:I

.field private final O〇08oOOO0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇o88o08〇:Z

.field private final o0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0OoOOo0:Landroid/view/animation/Animation;

.field private final o8o:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:Ljava/lang/String;

.field private o8〇OO:Z

.field private o8〇OO0〇0o:Landroid/view/View;

.field private oO00〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

.field private oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

.field private oOo〇8o008:Landroid/widget/TextView;

.field private oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooO:Landroid/widget/EditText;

.field private ooo0〇〇O:Landroid/view/View;

.field private o〇00O:I

.field private final o〇oO:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇o〇Oo88:Landroid/view/View;

.field private 〇00O0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

.field private 〇08O〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

.field private 〇0O〇O00O:Landroid/view/animation/Animation;

.field private 〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

.field private 〇OO8ooO8〇:I

.field private final 〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

.field private 〇OO〇00〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O〇〇O8:I

.field private 〇o0O:Lcom/intsig/camscanner/Client/ProgressDialogClient;

.field private 〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

.field private final 〇〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇〇0o〇〇0:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO0880O:Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 7
    .line 8
    .line 9
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 10
    .line 11
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$2;

    .line 12
    .line 13
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-class v1, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;

    .line 21
    .line 22
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$3;

    .line 27
    .line 28
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 29
    .line 30
    .line 31
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$4;

    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 35
    .line 36
    .line 37
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$5;

    .line 38
    .line 39
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 40
    .line 41
    .line 42
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0:Lkotlin/Lazy;

    .line 47
    .line 48
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 53
    .line 54
    const-string v0, ""

    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 57
    .line 58
    const/4 v1, -0x1

    .line 59
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88O:I

    .line 60
    .line 61
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/〇080;

    .line 62
    .line 63
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/〇080;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 64
    .line 65
    .line 66
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 67
    .line 68
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadThumbFirst$2;->o0:Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadThumbFirst$2;

    .line 69
    .line 70
    invoke-static {v2}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 71
    .line 72
    .line 73
    move-result-object v2

    .line 74
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 75
    .line 76
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$getThreadNum$2;

    .line 77
    .line 78
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$getThreadNum$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 79
    .line 80
    .line 81
    invoke-static {v2}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇oO:Lkotlin/Lazy;

    .line 86
    .line 87
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadThreadPoolDispatcher$2;

    .line 88
    .line 89
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadThreadPoolDispatcher$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 90
    .line 91
    .line 92
    invoke-static {v2}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08〇o0O:Lkotlin/Lazy;

    .line 97
    .line 98
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadScope$2;

    .line 99
    .line 100
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$mDownloadScope$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 101
    .line 102
    .line 103
    invoke-static {v2}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 104
    .line 105
    .line 106
    move-result-object v2

    .line 107
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇o〇:Lkotlin/Lazy;

    .line 108
    .line 109
    new-instance v2, Ljava/util/ArrayList;

    .line 110
    .line 111
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .line 113
    .line 114
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo80:Ljava/util/List;

    .line 115
    .line 116
    new-instance v2, Ljava/util/ArrayList;

    .line 117
    .line 118
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .line 120
    .line 121
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 122
    .line 123
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$imageAdjustListener$1;

    .line 124
    .line 125
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$imageAdjustListener$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 126
    .line 127
    .line 128
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇08oOOO0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 129
    .line 130
    iput v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Ooo08:I

    .line 131
    .line 132
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 133
    .line 134
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/〇〇888;

    .line 135
    .line 136
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/〇〇888;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 137
    .line 138
    .line 139
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇〇0o〇〇0:Landroid/view/View$OnClickListener;

    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O008oO0()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_5

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-gtz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 21
    .line 22
    if-eqz v2, :cond_5

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v8

    .line 28
    if-eqz v8, :cond_5

    .line 29
    .line 30
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    const/16 v3, 0x4a

    .line 33
    .line 34
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    const v4, 0x7f070520

    .line 43
    .line 44
    .line 45
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 50
    .line 51
    .line 52
    move-result v4

    .line 53
    mul-int v4, v4, v2

    .line 54
    .line 55
    if-ge v4, v0, :cond_1

    .line 56
    .line 57
    const/high16 v2, 0x3f800000    # 1.0f

    .line 58
    .line 59
    int-to-float v0, v0

    .line 60
    mul-float v0, v0, v2

    .line 61
    .line 62
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 63
    .line 64
    .line 65
    move-result v2

    .line 66
    int-to-float v2, v2

    .line 67
    div-float/2addr v0, v2

    .line 68
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    :goto_0
    move v5, v0

    .line 73
    goto :goto_2

    .line 74
    :cond_1
    div-int v2, v0, v2

    .line 75
    .line 76
    const/4 v4, 0x5

    .line 77
    if-gt v2, v4, :cond_2

    .line 78
    .line 79
    const/high16 v2, 0x40900000    # 4.5f

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_2
    int-to-float v2, v2

    .line 83
    const/high16 v4, 0x3f000000    # 0.5f

    .line 84
    .line 85
    sub-float/2addr v2, v4

    .line 86
    :goto_1
    int-to-float v0, v0

    .line 87
    div-float/2addr v0, v2

    .line 88
    float-to-int v0, v0

    .line 89
    goto :goto_0

    .line 90
    :goto_2
    sub-int v0, v5, v3

    .line 91
    .line 92
    sub-int v6, v0, v3

    .line 93
    .line 94
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 95
    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    const-string v3, " oneItemWidth="

    .line 102
    .line 103
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 117
    .line 118
    if-eqz v0, :cond_3

    .line 119
    .line 120
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    if-eqz v0, :cond_3

    .line 125
    .line 126
    const v2, 0x7f0701be

    .line 127
    .line 128
    .line 129
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 130
    .line 131
    .line 132
    move-result v0

    .line 133
    move v7, v0

    .line 134
    goto :goto_3

    .line 135
    :cond_3
    const/4 v7, 0x0

    .line 136
    :goto_3
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 137
    .line 138
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 139
    .line 140
    move-object v3, v0

    .line 141
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;-><init>(Landroid/app/Activity;IIILjava/util/List;)V

    .line 142
    .line 143
    .line 144
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 145
    .line 146
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 147
    .line 148
    if-nez v2, :cond_4

    .line 149
    .line 150
    goto :goto_4

    .line 151
    :cond_4
    invoke-virtual {v2, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 152
    .line 153
    .line 154
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 155
    .line 156
    if-eqz v0, :cond_5

    .line 157
    .line 158
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/〇80〇808〇O;

    .line 159
    .line 160
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OOO〇O0(Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter$OnItemClickListener;)V

    .line 164
    .line 165
    .line 166
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 167
    .line 168
    if-nez v0, :cond_6

    .line 169
    .line 170
    goto :goto_5

    .line 171
    :cond_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    .line 173
    .line 174
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇00O()Landroid/view/animation/Animation;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    if-eqz v0, :cond_7

    .line 179
    .line 180
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$showEnhanceLayout$2;

    .line 181
    .line 182
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$showEnhanceLayout$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 186
    .line 187
    .line 188
    :cond_7
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 189
    .line 190
    if-eqz v1, :cond_8

    .line 191
    .line 192
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 193
    .line 194
    .line 195
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o0()V

    .line 196
    .line 197
    .line 198
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final O00OoO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "reTakePicture"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "CSBatchResultDelete"

    .line 9
    .line 10
    const-string p1, "retake"

    .line 11
    .line 12
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O088O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 9
    .line 10
    .line 11
    move-result-object v3

    .line 12
    const-string v4, "getInstance()"

    .line 13
    .line 14
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {v2, v0, v3}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 18
    .line 19
    .line 20
    const-class v3, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 21
    .line 22
    invoke-virtual {v2, v3}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 27
    .line 28
    iput-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 29
    .line 30
    if-eqz v2, :cond_0

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    if-eqz v2, :cond_0

    .line 37
    .line 38
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initImageAdjustViewMolder$1$1;

    .line 39
    .line 40
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initImageAdjustViewMolder$1$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 41
    .line 42
    .line 43
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/Oooo8o0〇;

    .line 44
    .line 45
    invoke-direct {v3, v1}, Lcom/intsig/camscanner/multiimageedit/Oooo8o0〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v2, v0, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 49
    .line 50
    .line 51
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 52
    .line 53
    move-object v1, v0

    .line 54
    :cond_0
    if-nez v1, :cond_1

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 57
    .line 58
    const-string v1, "initImageAdjustViewMolder BUT GET NULL ACTIVITY"

    .line 59
    .line 60
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O08〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8o08O8O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O08〇oO8〇()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/util/Util;->OOO(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const-string v1, "getPreferName(getActivit\u2026Util.BRACKET_SUFFIXSTYLE)"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 19
    .line 20
    if-eqz v0, :cond_0

    .line 21
    .line 22
    const/4 v1, 0x3

    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇〇(I)V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 27
    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O0O0〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->O〇O〇oO(Ljava/util/List;I)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 16
    .line 17
    return-object v0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    return-object v0
    .line 20
    .line 21
.end method

.method private static final O0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 7
    .line 8
    const-string p2, "discard"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 14
    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const/4 p2, 0x0

    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->oo88o8O(Z)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    if-eqz p0, :cond_1

    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :catchall_0
    move-exception p0

    .line 32
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 33
    .line 34
    new-instance p2, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v0, "showGiveUpImportImage discard, t="

    .line 40
    .line 41
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object p0

    .line 51
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :cond_1
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O0〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇〇o8O(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O0〇8〇()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇o8()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v1, 0x1

    .line 11
    add-int/2addr v0, v1

    .line 12
    iget v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 13
    .line 14
    if-lt v0, v2, :cond_1

    .line 15
    .line 16
    move v0, v2

    .line 17
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 18
    .line 19
    if-nez v2, :cond_2

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_2
    sget-object v3, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 23
    .line 24
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const/4 v4, 0x2

    .line 29
    new-array v5, v4, [Ljava/lang/Object;

    .line 30
    .line 31
    const/4 v6, 0x0

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    aput-object v0, v5, v6

    .line 37
    .line 38
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 39
    .line 40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    aput-object v0, v5, v1

    .line 45
    .line 46
    invoke-static {v5, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    const-string v1, "%d/%d"

    .line 51
    .line 52
    invoke-static {v3, v1, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const-string v1, "format(locale, format, *args)"

    .line 57
    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    .line 63
    .line 64
    :goto_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O80OO(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadThumbsAndHd$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadThumbsAndHd$2;-><init>(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    if-ne p1, p2, :cond_0

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O88(I)Landroid/view/animation/Animation;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/16 v0, 0x12c

    .line 8
    .line 9
    int-to-long v0, v0

    .line 10
    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 11
    .line 12
    .line 13
    const-string v0, "animation"

    .line 14
    .line 15
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-object p1
    .line 19
    .line 20
.end method

.method public static final synthetic O880O〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Ooo08:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O888Oo()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/utils/CommonLoadingTask;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$saveResult$1;

    .line 6
    .line 7
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$saveResult$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 8
    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O8O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooO:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic O8〇8〇O80(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8〇o0〇〇8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "enhanceThumbAdapter == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const/4 v0, 0x0

    .line 21
    :goto_0
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_5

    .line 36
    .line 37
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 42
    .line 43
    if-eqz v2, :cond_4

    .line 44
    .line 45
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 46
    .line 47
    if-eqz v2, :cond_4

    .line 48
    .line 49
    iget v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 50
    .line 51
    if-eq v0, v3, :cond_3

    .line 52
    .line 53
    iput v0, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 54
    .line 55
    iget v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 56
    .line 57
    sget v4, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇080:I

    .line 58
    .line 59
    if-ne v3, v4, :cond_3

    .line 60
    .line 61
    sget v3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 62
    .line 63
    iput v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 64
    .line 65
    :cond_3
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 66
    .line 67
    goto :goto_2

    .line 68
    :cond_4
    const/4 v2, 0x0

    .line 69
    :goto_2
    if-nez v2, :cond_2

    .line 70
    .line 71
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 72
    .line 73
    const-string v3, "enhanceApplyForPage multiImageEditPage null"

    .line 74
    .line 75
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_5
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic OO0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇8O0O80〇(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO0o(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;",
            ">;>;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    instance-of v2, v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;

    .line 6
    .line 7
    if-eqz v2, :cond_0

    .line 8
    .line 9
    move-object v2, v1

    .line 10
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;

    .line 11
    .line 12
    iget v3, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 13
    .line 14
    const/high16 v4, -0x80000000

    .line 15
    .line 16
    and-int v5, v3, v4

    .line 17
    .line 18
    if-eqz v5, :cond_0

    .line 19
    .line 20
    sub-int/2addr v3, v4

    .line 21
    iput v3, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;

    .line 25
    .line 26
    invoke-direct {v2, v0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iget-object v1, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->O8o08O8O:Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    iget v4, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 36
    .line 37
    const/4 v5, 0x3

    .line 38
    const/4 v6, 0x2

    .line 39
    const/4 v7, 0x1

    .line 40
    const/4 v8, 0x0

    .line 41
    if-eqz v4, :cond_4

    .line 42
    .line 43
    if-eq v4, v7, :cond_3

    .line 44
    .line 45
    if-eq v4, v6, :cond_2

    .line 46
    .line 47
    if-ne v4, v5, :cond_1

    .line 48
    .line 49
    invoke-static {v1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    goto/16 :goto_5

    .line 53
    .line 54
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 55
    .line 56
    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 57
    .line 58
    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    throw v1

    .line 62
    :cond_2
    iget-object v4, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o〇00O:Ljava/lang/Object;

    .line 63
    .line 64
    check-cast v4, Ljava/util/Iterator;

    .line 65
    .line 66
    iget-object v9, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 67
    .line 68
    check-cast v9, Lkotlin/jvm/internal/Ref$IntRef;

    .line 69
    .line 70
    iget-object v10, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->OO:Ljava/lang/Object;

    .line 71
    .line 72
    check-cast v10, Lkotlin/jvm/internal/Ref$IntRef;

    .line 73
    .line 74
    iget-object v11, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 75
    .line 76
    check-cast v11, Ljava/util/List;

    .line 77
    .line 78
    iget-object v12, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o0:Ljava/lang/Object;

    .line 79
    .line 80
    check-cast v12, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;

    .line 81
    .line 82
    invoke-static {v1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    goto/16 :goto_4

    .line 86
    .line 87
    :cond_3
    iget-object v4, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o〇00O:Ljava/lang/Object;

    .line 88
    .line 89
    check-cast v4, Ljava/util/Iterator;

    .line 90
    .line 91
    iget-object v9, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 92
    .line 93
    check-cast v9, Lkotlin/jvm/internal/Ref$IntRef;

    .line 94
    .line 95
    iget-object v10, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->OO:Ljava/lang/Object;

    .line 96
    .line 97
    check-cast v10, Lkotlin/jvm/internal/Ref$IntRef;

    .line 98
    .line 99
    iget-object v11, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 100
    .line 101
    check-cast v11, Ljava/util/List;

    .line 102
    .line 103
    iget-object v12, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o0:Ljava/lang/Object;

    .line 104
    .line 105
    check-cast v12, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;

    .line 106
    .line 107
    invoke-static {v1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 108
    .line 109
    .line 110
    goto/16 :goto_3

    .line 111
    .line 112
    :cond_4
    invoke-static {v1}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo80:Ljava/util/List;

    .line 116
    .line 117
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 118
    .line 119
    .line 120
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 125
    .line 126
    .line 127
    move-result v4

    .line 128
    if-eqz v4, :cond_5

    .line 129
    .line 130
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    move-result-object v4

    .line 134
    check-cast v4, Landroid/util/Pair;

    .line 135
    .line 136
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o88o88()Lkotlinx/coroutines/CoroutineScope;

    .line 137
    .line 138
    .line 139
    move-result-object v9

    .line 140
    const/4 v10, 0x0

    .line 141
    const/4 v11, 0x0

    .line 142
    new-instance v12, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$downloadJob$1;

    .line 143
    .line 144
    invoke-direct {v12, v4, v0, v8}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$downloadJob$1;-><init>(Landroid/util/Pair;Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 145
    .line 146
    .line 147
    const/4 v13, 0x3

    .line 148
    const/4 v14, 0x0

    .line 149
    invoke-static/range {v9 .. v14}, Lkotlinx/coroutines/BuildersKt;->〇o00〇〇Oo(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    .line 150
    .line 151
    .line 152
    move-result-object v4

    .line 153
    iget-object v9, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo80:Ljava/util/List;

    .line 154
    .line 155
    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_5
    new-instance v1, Lkotlin/jvm/internal/Ref$IntRef;

    .line 160
    .line 161
    invoke-direct {v1}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 162
    .line 163
    .line 164
    new-instance v4, Lkotlin/jvm/internal/Ref$IntRef;

    .line 165
    .line 166
    invoke-direct {v4}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    .line 167
    .line 168
    .line 169
    iget-object v9, v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo80:Ljava/util/List;

    .line 170
    .line 171
    check-cast v9, Ljava/lang/Iterable;

    .line 172
    .line 173
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 174
    .line 175
    .line 176
    move-result-object v9

    .line 177
    move-object v11, v0

    .line 178
    move-object v10, v9

    .line 179
    move-object v9, v4

    .line 180
    move-object v4, v3

    .line 181
    move-object v3, v2

    .line 182
    move-object v2, v1

    .line 183
    move-object/from16 v1, p1

    .line 184
    .line 185
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    .line 186
    .line 187
    .line 188
    move-result v12

    .line 189
    if-eqz v12, :cond_9

    .line 190
    .line 191
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 192
    .line 193
    .line 194
    move-result-object v12

    .line 195
    check-cast v12, Lkotlinx/coroutines/Deferred;

    .line 196
    .line 197
    iput-object v11, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o0:Ljava/lang/Object;

    .line 198
    .line 199
    iput-object v1, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 200
    .line 201
    iput-object v2, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->OO:Ljava/lang/Object;

    .line 202
    .line 203
    iput-object v9, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 204
    .line 205
    iput-object v10, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o〇00O:Ljava/lang/Object;

    .line 206
    .line 207
    iput v7, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 208
    .line 209
    invoke-interface {v12, v3}, Lkotlinx/coroutines/Deferred;->〇8o8o〇(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 210
    .line 211
    .line 212
    move-result-object v12

    .line 213
    if-ne v12, v4, :cond_6

    .line 214
    .line 215
    return-object v4

    .line 216
    :cond_6
    move-object v15, v11

    .line 217
    move-object v11, v1

    .line 218
    move-object v1, v12

    .line 219
    move-object v12, v15

    .line 220
    move-object/from16 v16, v10

    .line 221
    .line 222
    move-object v10, v2

    .line 223
    move-object v2, v3

    .line 224
    move-object v3, v4

    .line 225
    move-object/from16 v4, v16

    .line 226
    .line 227
    :goto_3
    check-cast v1, Ljava/lang/Boolean;

    .line 228
    .line 229
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 230
    .line 231
    .line 232
    move-result v1

    .line 233
    if-eqz v1, :cond_7

    .line 234
    .line 235
    iget v1, v10, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 236
    .line 237
    add-int/2addr v1, v7

    .line 238
    iput v1, v10, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 239
    .line 240
    :cond_7
    iget v1, v9, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 241
    .line 242
    if-nez v1, :cond_8

    .line 243
    .line 244
    invoke-direct {v12}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇O0O()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;

    .line 245
    .line 246
    .line 247
    move-result-object v1

    .line 248
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;->〇8o8o〇()V

    .line 249
    .line 250
    .line 251
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 252
    .line 253
    .line 254
    move-result-object v1

    .line 255
    new-instance v13, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$2$1;

    .line 256
    .line 257
    invoke-direct {v13, v12, v8}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$2$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 258
    .line 259
    .line 260
    iput-object v12, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o0:Ljava/lang/Object;

    .line 261
    .line 262
    iput-object v11, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 263
    .line 264
    iput-object v10, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->OO:Ljava/lang/Object;

    .line 265
    .line 266
    iput-object v9, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 267
    .line 268
    iput-object v4, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o〇00O:Ljava/lang/Object;

    .line 269
    .line 270
    iput v6, v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 271
    .line 272
    invoke-static {v1, v13, v2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 273
    .line 274
    .line 275
    move-result-object v1

    .line 276
    if-ne v1, v3, :cond_8

    .line 277
    .line 278
    return-object v3

    .line 279
    :cond_8
    :goto_4
    move-object v1, v11

    .line 280
    move-object v11, v12

    .line 281
    move-object v15, v3

    .line 282
    move-object v3, v2

    .line 283
    move-object v2, v10

    .line 284
    move-object v10, v4

    .line 285
    move-object v4, v15

    .line 286
    iget v12, v9, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 287
    .line 288
    add-int/2addr v12, v7

    .line 289
    iput v12, v9, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 290
    .line 291
    goto :goto_2

    .line 292
    :cond_9
    sget-object v6, Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;

    .line 293
    .line 294
    iget v2, v2, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 295
    .line 296
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 297
    .line 298
    .line 299
    move-result v1

    .line 300
    invoke-virtual {v6, v2, v1}, Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;->〇080(II)V

    .line 301
    .line 302
    .line 303
    invoke-direct {v11}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇oOO80o()V

    .line 304
    .line 305
    .line 306
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 307
    .line 308
    .line 309
    move-result-object v1

    .line 310
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$3;

    .line 311
    .line 312
    invoke-direct {v2, v11, v8}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$3;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 313
    .line 314
    .line 315
    iput-object v8, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o0:Ljava/lang/Object;

    .line 316
    .line 317
    iput-object v8, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 318
    .line 319
    iput-object v8, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->OO:Ljava/lang/Object;

    .line 320
    .line 321
    iput-object v8, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 322
    .line 323
    iput-object v8, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->o〇00O:Ljava/lang/Object;

    .line 324
    .line 325
    iput v5, v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadHdAndRaw$1;->〇0O:I

    .line 326
    .line 327
    invoke-static {v1, v2, v3}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 328
    .line 329
    .line 330
    move-result-object v1

    .line 331
    if-ne v1, v4, :cond_a

    .line 332
    .line 333
    return-object v4

    .line 334
    :cond_a
    :goto_5
    return-object v1
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final OO0〇O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$showProgressDialog$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$showProgressDialog$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO8〇O8()Lorg/json/JSONObject;
    .locals 5

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    const-string v1, "SCHEME"

    .line 7
    .line 8
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0O:Landroid/widget/CheckBox;

    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    if-eqz v2, :cond_0

    .line 12
    .line 13
    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    const/4 v4, 0x1

    .line 18
    if-ne v2, v4, :cond_0

    .line 19
    .line 20
    const/4 v3, 0x1

    .line 21
    :cond_0
    if-eqz v3, :cond_1

    .line 22
    .line 23
    const-string v2, "on"

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const-string v2, "off"

    .line 27
    .line 28
    :goto_0
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    .line 31
    goto :goto_1

    .line 32
    :catch_0
    move-exception v1

    .line 33
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 34
    .line 35
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 36
    .line 37
    .line 38
    :goto_1
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final OOo00()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇0o8o8〇()Landroid/view/animation/Animation;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const/16 v1, 0x8

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO〇000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSBatchResult"

    .line 7
    .line 8
    const-string v0, "filter_apply_all"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "isChecked="

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    if-eqz p2, :cond_1

    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8〇o0〇〇8()V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 47
    .line 48
    iget p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 49
    .line 50
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o00〇〇Oo:I

    .line 51
    .line 52
    const-string v1, "multiImageEditPage.modifyMultiImageEditModel"

    .line 53
    .line 54
    if-ne p2, v0, :cond_0

    .line 55
    .line 56
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 57
    .line 58
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0888(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    iget-object p2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 69
    .line 70
    iget p2, p2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 71
    .line 72
    sget v0, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o〇:I

    .line 73
    .line 74
    if-ne p2, v0, :cond_1

    .line 75
    .line 76
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 77
    .line 78
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o8〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 85
    .line 86
    .line 87
    :cond_1
    :goto_0
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final OO〇80oO〇(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a09e8

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 11
    .line 12
    const v2, 0x7f0a09a6

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const v2, 0x3e99999a    # 0.3f

    .line 20
    .line 21
    .line 22
    const/4 v3, 0x0

    .line 23
    const/high16 v4, 0x3f800000    # 1.0f

    .line 24
    .line 25
    const/4 v5, 0x1

    .line 26
    if-nez p1, :cond_0

    .line 27
    .line 28
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 39
    .line 40
    .line 41
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 42
    .line 43
    sub-int/2addr v0, v5

    .line 44
    if-ne p1, v0, :cond_1

    .line 45
    .line 46
    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 50
    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    invoke-virtual {v1, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 57
    .line 58
    .line 59
    :goto_1
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic OO〇〇o0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/Client/ProgressDialogClient;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o0O:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oo0O〇8800()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v0, v1, v2, v2}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 12
    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    const/4 v1, 0x1

    .line 24
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic OoO〇OOo8o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0〇8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Ooo8o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0OO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OooO〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇00O()Landroid/view/animation/Animation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0OoOOo0:Landroid/view/animation/Animation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const v0, 0x7f010042

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88(I)Landroid/view/animation/Animation;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0OoOOo0:Landroid/view/animation/Animation;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0OoOOo0:Landroid/view/animation/Animation;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O〇080〇o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Landroid/widget/CheckBox;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0O:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/Client/ProgressDialogClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o0O:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇0o8o8〇()Landroid/view/animation/Animation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O〇O00O:Landroid/view/animation/Animation;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const v0, 0x7f010043

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88(I)Landroid/view/animation/Animation;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O〇O00O:Landroid/view/animation/Animation;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O〇O00O:Landroid/view/animation/Animation;

    .line 15
    .line 16
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇0o8〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-static {p2, v0}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    if-eqz p3, :cond_0

    .line 9
    .line 10
    invoke-static {v1, p3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getScanBoundF([I[I)[I

    .line 11
    .line 12
    .line 13
    move-result-object p3

    .line 14
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo800〇〇()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 21
    .line 22
    iput-boolean v0, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8o:Z

    .line 23
    .line 24
    :cond_1
    iget-object v1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 25
    .line 26
    iput p4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇00O0:I

    .line 27
    .line 28
    invoke-static {p2}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 29
    .line 30
    .line 31
    move-result p4

    .line 32
    iput p4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O〇08oOOO0:I

    .line 33
    .line 34
    iget-object p4, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 35
    .line 36
    iput-object p2, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 37
    .line 38
    iput-object p3, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 39
    .line 40
    iput-boolean v0, p4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 41
    .line 42
    invoke-virtual {p4}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇8O0〇8()V

    .line 43
    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 46
    .line 47
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇o00〇〇Oo()V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static final O〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/view/View;)V
    .locals 10

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "rename"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const/4 v2, 0x0

    .line 18
    const v3, 0x7f130420

    .line 19
    .line 20
    .line 21
    const/4 v4, 0x0

    .line 22
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 23
    .line 24
    new-instance v6, Lcom/intsig/camscanner/multiimageedit/OO0o〇〇;

    .line 25
    .line 26
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/multiimageedit/OO0o〇〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 27
    .line 28
    .line 29
    new-instance v7, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$reNameClickListener$1$2;

    .line 30
    .line 31
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$reNameClickListener$1$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 32
    .line 33
    .line 34
    const-wide/16 v8, -0x1

    .line 35
    .line 36
    invoke-static/range {v1 .. v9}, Lcom/intsig/camscanner/app/DialogUtils;->〇0(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final O〇8O0O80〇(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$notifyAdapterUpdateInUI$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$notifyAdapterUpdateInUI$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O〇8〇008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇O800oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/o〇0;

    .line 13
    .line 14
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiimageedit/o〇0;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇o8()I
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8O〇008()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 6
    .line 7
    add-int/lit8 v2, v1, -0x1

    .line 8
    .line 9
    if-gt v0, v2, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8O〇008()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    add-int/lit8 v0, v1, -0x1

    .line 17
    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method private static final O〇oo8O80(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/lang/String;)V
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_3

    .line 11
    .line 12
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    xor-int/lit8 v1, v1, 0x1

    .line 17
    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v0, 0x0

    .line 22
    :goto_0
    if-eqz v0, :cond_3

    .line 23
    .line 24
    sget-object v1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 27
    .line 28
    const-wide/16 v3, -0x1

    .line 29
    .line 30
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    const-string v4, "MultiImageEditDownloadFragment.getReNameClickListener"

    .line 35
    .line 36
    invoke-virtual {v1, v4, p1, v2, v3}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->OoO8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 40
    .line 41
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-eqz p1, :cond_1

    .line 46
    .line 47
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->CUSTOM:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 51
    .line 52
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo0〇Ooo:I

    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 59
    .line 60
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 61
    .line 62
    if-nez p0, :cond_2

    .line 63
    .line 64
    goto :goto_2

    .line 65
    :cond_2
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    .line 67
    .line 68
    :cond_3
    :goto_2
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic O〇〇O80o8()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O〇〇o8O(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p0, "CSBatchResultDelete"

    .line 9
    .line 10
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final init()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8oo8888()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0〇〇00〇o()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O088O()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8oo〇〇oO()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0o〇o()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo0O()V

    .line 17
    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88O:I

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇O800oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o088O8800()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    if-nez v0, :cond_1

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_1
    const/16 v1, 0x8

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO00〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 15
    .line 16
    if-nez v0, :cond_2

    .line 17
    .line 18
    return-void

    .line 19
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    if-eqz v0, :cond_3

    .line 23
    .line 24
    const v2, 0x7f0a1212

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_3
    move-object v0, v1

    .line 35
    :goto_1
    if-eqz v0, :cond_4

    .line 36
    .line 37
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    goto :goto_2

    .line 42
    :cond_4
    move-object v2, v1

    .line 43
    :goto_2
    if-nez v2, :cond_5

    .line 44
    .line 45
    return-void

    .line 46
    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO00〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 51
    .line 52
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 53
    .line 54
    .line 55
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO00〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o0O0O〇〇〇0(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget v3, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-ne v3, v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇8o0OOOo()V

    .line 21
    .line 22
    .line 23
    goto/16 :goto_4

    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 26
    .line 27
    if-nez v0, :cond_2

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_2
    iget v3, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 31
    .line 32
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇〇0〇(I)V

    .line 33
    .line 34
    .line 35
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 36
    .line 37
    if-eqz v0, :cond_3

    .line 38
    .line 39
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 40
    .line 41
    .line 42
    :cond_3
    new-instance v0, Lorg/json/JSONObject;

    .line 43
    .line 44
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 45
    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 48
    .line 49
    if-eqz v3, :cond_4

    .line 50
    .line 51
    :try_start_0
    const-string v4, "scheme"

    .line 52
    .line 53
    invoke-virtual {v3}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o800o8O()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-static {v3}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇o00〇〇Oo(I)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v3

    .line 61
    invoke-virtual {v0, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    .line 64
    goto :goto_2

    .line 65
    :catch_0
    move-exception v3

    .line 66
    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V

    .line 67
    .line 68
    .line 69
    sget-object v3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 70
    .line 71
    :cond_4
    :goto_2
    const-string v3, "CSBatchResult"

    .line 72
    .line 73
    const-string v4, "filter_switch_filter"

    .line 74
    .line 75
    invoke-static {v3, v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0O:Landroid/widget/CheckBox;

    .line 79
    .line 80
    if-eqz v0, :cond_5

    .line 81
    .line 82
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    if-ne v0, v1, :cond_5

    .line 87
    .line 88
    goto :goto_3

    .line 89
    :cond_5
    const/4 v1, 0x0

    .line 90
    :goto_3
    if-eqz v1, :cond_6

    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8〇o0〇〇8()V

    .line 93
    .line 94
    .line 95
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    if-nez v0, :cond_7

    .line 100
    .line 101
    return-void

    .line 102
    :cond_7
    iget-object v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 103
    .line 104
    iget p1, p1, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->〇080:I

    .line 105
    .line 106
    invoke-static {p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceMode(I)I

    .line 107
    .line 108
    .line 109
    move-result p1

    .line 110
    iput p1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 111
    .line 112
    iget-object p1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 113
    .line 114
    const-string v0, "multiImageEditPage.modifyMultiImageEditModel"

    .line 115
    .line 116
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0888(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 120
    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 123
    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 126
    .line 127
    if-eqz p1, :cond_9

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 130
    .line 131
    if-eqz v0, :cond_8

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OoO8()I

    .line 134
    .line 135
    .line 136
    move-result v2

    .line 137
    :cond_8
    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 138
    .line 139
    .line 140
    :cond_9
    :goto_4
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o0OO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$dismissProgressDialog$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$dismissProgressDialog$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0Oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o0〇〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o0〇〇00〇o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    new-instance v1, Landroidx/lifecycle/ViewModelProvider;

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    const-string v3, "getInstance()"

    .line 12
    .line 13
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {v1, v0, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 17
    .line 18
    .line 19
    const-class v2, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 26
    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 28
    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->O〇O〇oO()Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    .line 37
    const-string v2, "multiEnhanceModelList"

    .line 38
    .line 39
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;->O8(Landroid/content/Context;Ljava/util/List;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 46
    .line 47
    if-eqz v1, :cond_1

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    if-eqz v1, :cond_1

    .line 54
    .line 55
    new-instance v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initEnhanceThumbViewModel$1$2;

    .line 56
    .line 57
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initEnhanceThumbViewModel$1$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 58
    .line 59
    .line 60
    new-instance v3, Lcom/intsig/camscanner/multiimageedit/〇o00〇〇Oo;

    .line 61
    .line 62
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/multiimageedit/〇o00〇〇Oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1, v0, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 66
    .line 67
    .line 68
    :cond_1
    return-void
.end method

.method public static final synthetic o808o8o08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇80O()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o88(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO8o〇08〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o88o88()Lkotlinx/coroutines/CoroutineScope;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇o〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o88oo〇O(Landroid/content/Intent;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    const-string v0, "extra_multi_capture_status"

    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    instance-of v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 15
    .line 16
    const-string v0, "parcelable is not MultiCaptureResultStatus"

    .line 17
    .line 18
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->〇80〇808〇O()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_2

    .line 29
    .line 30
    return-void

    .line 31
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureResultStatus;->o〇0()Ljava/util/List;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_3

    .line 40
    .line 41
    return-void

    .line 42
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 43
    .line 44
    if-nez v0, :cond_4

    .line 45
    .line 46
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 47
    .line 48
    const-string v0, "handleMultiAdjustResultIntent imageViewPager == null"

    .line 49
    .line 50
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    return-void

    .line 54
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 55
    .line 56
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-gtz v0, :cond_5

    .line 61
    .line 62
    return-void

    .line 63
    :cond_5
    new-instance v0, Ljava/util/HashMap;

    .line 64
    .line 65
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 73
    .line 74
    .line 75
    move-result v1

    .line 76
    if-eqz v1, :cond_6

    .line 77
    .line 78
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    check-cast v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 83
    .line 84
    iget-wide v2, v1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 85
    .line 86
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    const-string v3, "pagePara"

    .line 91
    .line 92
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 104
    .line 105
    check-cast v1, Ljava/lang/Iterable;

    .line 106
    .line 107
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    :cond_7
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 112
    .line 113
    .line 114
    move-result v2

    .line 115
    if-eqz v2, :cond_c

    .line 116
    .line 117
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    check-cast v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 122
    .line 123
    if-eqz v2, :cond_7

    .line 124
    .line 125
    iget-object v2, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 126
    .line 127
    if-eqz v2, :cond_7

    .line 128
    .line 129
    const-string v3, "modifyMultiImageEditModel"

    .line 130
    .line 131
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    iget-wide v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 135
    .line 136
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 137
    .line 138
    .line 139
    move-result-object v3

    .line 140
    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    .line 142
    .line 143
    move-result-object v3

    .line 144
    check-cast v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 145
    .line 146
    if-nez v3, :cond_8

    .line 147
    .line 148
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 149
    .line 150
    const-string v3, "pagePara == null"

    .line 151
    .line 152
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    goto :goto_1

    .line 156
    :cond_8
    iget v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 157
    .line 158
    iput v4, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 159
    .line 160
    iget-object v3, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 161
    .line 162
    iput-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 163
    .line 164
    if-eqz v3, :cond_9

    .line 165
    .line 166
    const/4 v3, 0x1

    .line 167
    goto :goto_2

    .line 168
    :cond_9
    const/4 v3, 0x0

    .line 169
    :goto_2
    iput-boolean v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO〇〇:Z

    .line 170
    .line 171
    sget v3, Lcom/intsig/camscanner/multiimageedit/model/ImageEditStatus;->〇o〇:I

    .line 172
    .line 173
    iput v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O88O:I

    .line 174
    .line 175
    if-eqz p1, :cond_a

    .line 176
    .line 177
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 178
    .line 179
    goto :goto_3

    .line 180
    :cond_a
    const/4 v3, 0x0

    .line 181
    :goto_3
    if-nez v3, :cond_b

    .line 182
    .line 183
    goto :goto_1

    .line 184
    :cond_b
    iget-object v3, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 185
    .line 186
    iget-object v3, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 187
    .line 188
    iget-object v4, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 189
    .line 190
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 191
    .line 192
    .line 193
    move-result v3

    .line 194
    if-eqz v3, :cond_7

    .line 195
    .line 196
    iget-object v3, v2, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO〇00〇8oO:Ljava/lang/String;

    .line 197
    .line 198
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 199
    .line 200
    .line 201
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o8〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 202
    .line 203
    .line 204
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 205
    .line 206
    .line 207
    sget-object v2, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 208
    .line 209
    const-string v3, "handleMultiAdjustResultIntent updateDataChange"

    .line 210
    .line 211
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .line 213
    .line 214
    goto :goto_1

    .line 215
    :cond_c
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final o8O〇008()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final o8o0()V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 6
    .line 7
    if-eqz v1, :cond_7

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v2, 0x0

    .line 15
    :goto_0
    if-nez v2, :cond_1

    .line 16
    .line 17
    goto :goto_4

    .line 18
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 19
    .line 20
    if-nez v1, :cond_2

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_2
    iget v2, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 24
    .line 25
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getEnhanceIndex(I)I

    .line 26
    .line 27
    .line 28
    move-result v2

    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇〇0〇(I)V

    .line 30
    .line 31
    .line 32
    :goto_1
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 33
    .line 34
    if-eqz v1, :cond_3

    .line 35
    .line 36
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 37
    .line 38
    .line 39
    :cond_3
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo0:Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;

    .line 40
    .line 41
    if-eqz v2, :cond_6

    .line 42
    .line 43
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 44
    .line 45
    iget-object v4, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇0O:Ljava/lang/String;

    .line 46
    .line 47
    const/4 v5, 0x0

    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 49
    .line 50
    const/4 v6, 0x0

    .line 51
    if-eqz v1, :cond_4

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->o〇O8〇〇o()I

    .line 54
    .line 55
    .line 56
    move-result v1

    .line 57
    goto :goto_2

    .line 58
    :cond_4
    const/4 v1, 0x0

    .line 59
    :goto_2
    iget-object v7, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 60
    .line 61
    if-eqz v7, :cond_5

    .line 62
    .line 63
    invoke-virtual {v7}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->〇oo〇()I

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    move v7, v6

    .line 68
    goto :goto_3

    .line 69
    :cond_5
    const/4 v7, 0x0

    .line 70
    :goto_3
    iget-object v8, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 71
    .line 72
    move v6, v1

    .line 73
    invoke-virtual/range {v2 .. v8}, Lcom/intsig/camscanner/multiimageedit/viewModel/EnhanceThumbViewModel;->〇8o8O〇O(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 74
    .line 75
    .line 76
    :cond_6
    return-void

    .line 77
    :cond_7
    :goto_4
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 78
    .line 79
    const-string v1, "requestEnhanceThumb == null"

    .line 80
    .line 81
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o8o0o8()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8O〇008()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8o0o0(I)Landroid/content/Intent;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/16 v1, 0x68

    .line 10
    .line 11
    invoke-static {p0, v0, v1}, Lcom/intsig/utils/TransitionUtil;->O8(Landroidx/fragment/app/Fragment;Landroid/content/Intent;I)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oO8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇0o8o8〇()Landroid/view/animation/Animation;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    const/16 v1, 0x8

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final varargs oO88〇0O8O([I)V
    .locals 4
    .param p1    # [I
        .annotation build Landroidx/annotation/IdRes;
        .end annotation
    .end param

    .line 1
    array-length v0, p1

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    if-ge v1, v0, :cond_1

    .line 4
    .line 5
    aget v2, p1, v1

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 8
    .line 9
    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    if-eqz v2, :cond_0

    .line 14
    .line 15
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final oO8o〇08〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOO8oo0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadImageIdsWithActionId$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, p1, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadImageIdsWithActionId$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooooo0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final oO〇O0O()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00o〇O8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo0O()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x0

    .line 13
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$startDownloadImages$1;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$startDownloadImages$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 17
    .line 18
    .line 19
    const/4 v5, 0x3

    .line 20
    const/4 v6, 0x0

    .line 21
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final oo8()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/oO80;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/oO80;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final oo8〇〇()Landroid/net/Uri;
    .locals 9

    .line 1
    new-instance v8, Lcom/intsig/camscanner/datastruct/DocProperty;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8oOOo:Ljava/lang/String;

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x0

    .line 9
    const/4 v5, 0x0

    .line 10
    const/4 v6, 0x0

    .line 11
    const/4 v7, 0x0

    .line 12
    move-object v0, v8

    .line 13
    invoke-direct/range {v0 .. v7}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo0〇Ooo:I

    .line 17
    .line 18
    iput v0, v8, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o00〇〇Oo:I

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-static {v0, v8}, Lcom/intsig/camscanner/util/Util;->O8O〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final ooo008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0OOoO(Z)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o088O8800()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oooO8〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final oooO8〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o8oO〇(Ljava/util/List;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 16
    .line 17
    if-eqz p0, :cond_1

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final oooo800〇〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "turnLeft multiImageEditPage == null"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 16
    .line 17
    const-string v1, "multiImageEditPage.modifyMultiImageEditModel"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final ooooo0O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "<anonymous parameter 0>"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {p1}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const/16 p2, 0x69

    .line 20
    .line 21
    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic o〇08oO80o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇O8OO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o0oOO8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇OoO0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 5
    .line 6
    if-eqz p1, :cond_1

    .line 7
    .line 8
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    goto :goto_0

    .line 13
    :cond_1
    const/4 p1, 0x0

    .line 14
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    sub-int/2addr v0, v1

    .line 18
    if-ne p1, v0, :cond_2

    .line 19
    .line 20
    add-int/lit8 p1, p1, -0x1

    .line 21
    .line 22
    :cond_2
    const/4 v0, -0x1

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Ooo08:I

    .line 24
    .line 25
    if-ltz p1, :cond_5

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 28
    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o8oO〇(Ljava/util/List;)V

    .line 34
    .line 35
    .line 36
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 37
    .line 38
    if-nez v0, :cond_4

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_4
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 42
    .line 43
    invoke-virtual {v0, v2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 44
    .line 45
    .line 46
    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8ooOO(IZ)V

    .line 47
    .line 48
    .line 49
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 50
    .line 51
    if-eqz p1, :cond_6

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 54
    .line 55
    .line 56
    :cond_6
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o〇o08〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO8ooO8〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇o0oOO8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇oO08〇o0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 2
    .line 3
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$updateThumbs$2;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$updateThumbs$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p1, v0, p2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇oo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Ooo08:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇00o〇O8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇00〇〇〇o〇8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showGiveUpImportImage"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    const v1, 0x7f131d10

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f130a8d

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/OO0o〇〇〇〇0;

    .line 32
    .line 33
    invoke-direct {v1}, Lcom/intsig/camscanner/multiimageedit/OO0o〇〇〇〇0;-><init>()V

    .line 34
    .line 35
    .line 36
    const v2, 0x7f13057e

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇8o8o〇;

    .line 44
    .line 45
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇8o8o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 46
    .line 47
    .line 48
    const v2, 0x7f130128

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0o88Oo〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇08O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0o0oO〇〇0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadImageWithFileId$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$downloadImageWithFileId$2;-><init>(Ljava/util/List;Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p2}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    if-ne p1, p2, :cond_0

    .line 20
    .line 21
    return-object p1

    .line 22
    :cond_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 23
    .line 24
    return-object p1
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0o88Oo〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$multiImageEditPage"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇OoO0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇80O()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o0()V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 24
    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 28
    .line 29
    if-eqz p0, :cond_0

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->OoO8()I

    .line 32
    .line 33
    .line 34
    move-result p0

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const/4 p0, 0x0

    .line 37
    :goto_0
    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 38
    .line 39
    .line 40
    :cond_1
    const-string p0, "CSBatchResultDelete"

    .line 41
    .line 42
    const-string p1, "delete"

    .line 43
    .line 44
    invoke-static {p0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o〇OO80oO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0ooOOo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇oOO80o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇0o〇o()V
    .locals 10

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80oo〇0〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/16 v1, 0x8

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    :goto_0
    return-void

    .line 18
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    if-nez v0, :cond_2

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 24
    .line 25
    const v2, 0x7f0a1a6a

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 36
    .line 37
    const v2, 0x7f0a0d10

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 45
    .line 46
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->〇080:Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;

    .line 47
    .line 48
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 49
    .line 50
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o〇Oo88:Landroid/view/View;

    .line 51
    .line 52
    new-instance v5, Lcom/intsig/camscanner/multiimageedit/〇O8o08O;

    .line 53
    .line 54
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/multiimageedit/〇O8o08O;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 55
    .line 56
    .line 57
    sget-object v6, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->BOTTOM:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 58
    .line 59
    const v0, 0x7f130a1d

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v7

    .line 66
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 67
    .line 68
    const v8, 0x7f0a07ca

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object v8

    .line 75
    const/4 v0, 0x1

    .line 76
    new-array v9, v0, [Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO00〇o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 79
    .line 80
    aput-object v0, v9, v1

    .line 81
    .line 82
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->Oo08(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/callback/Callback0;Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;Ljava/lang/String;Landroid/view/View;[Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)Z

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇0〇o8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiimageedit/adapter/EnhanceThumbAdapter;->oo88o8O(I)Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o0O0O〇〇〇0(Lcom/intsig/camscanner/capture/setting/model/MultiEnhanceModel;)V

    .line 19
    .line 20
    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇80O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    :cond_1
    return v1
.end method

.method public static final synthetic 〇8O0880(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8o0o0(I)Landroid/content/Intent;
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x0

    .line 13
    if-lez v1, :cond_4

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;

    .line 16
    .line 17
    invoke-direct {v1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;-><init>()V

    .line 18
    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v4

    .line 30
    if-eqz v4, :cond_3

    .line 31
    .line 32
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 37
    .line 38
    if-eqz v4, :cond_2

    .line 39
    .line 40
    iget-object v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 41
    .line 42
    if-eqz v4, :cond_2

    .line 43
    .line 44
    iget-object v5, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 45
    .line 46
    invoke-static {v5}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 47
    .line 48
    .line 49
    move-result v5

    .line 50
    if-nez v5, :cond_0

    .line 51
    .line 52
    sget-object v5, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 53
    .line 54
    new-instance v6, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v7, "getMultiCaptureResultIntent multiImageEditModel="

    .line 60
    .line 61
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v6

    .line 71
    invoke-static {v5, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    :cond_0
    iget-object v5, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 75
    .line 76
    const-string v6, "multiImageEditModel.bigRawImagePath"

    .line 77
    .line 78
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    iget v6, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 82
    .line 83
    invoke-virtual {v1, v5, v6}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->Oooo8o0〇(Ljava/lang/String;I)V

    .line 84
    .line 85
    .line 86
    iget-object v6, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 87
    .line 88
    if-eqz v6, :cond_1

    .line 89
    .line 90
    invoke-virtual {v1, v5, v6}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->OO0o〇〇〇〇0(Ljava/lang/String;[I)V

    .line 91
    .line 92
    .line 93
    goto :goto_1

    .line 94
    :cond_1
    move-object v6, v2

    .line 95
    :goto_1
    iget-wide v7, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o0:J

    .line 96
    .line 97
    iget v4, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 98
    .line 99
    invoke-static {v7, v8, v5, v4, v6}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->O8(JLjava/lang/String;I[I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 100
    .line 101
    .line 102
    move-result-object v4

    .line 103
    const-string v5, "createPagePara(\n        \u2026                        )"

    .line 104
    .line 105
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_2
    sget-object v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 113
    .line 114
    const-string v5, "getMultiCaptureResultIntent multiImageEditModel == null"

    .line 115
    .line 116
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_3
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;->〇O00(I)V

    .line 121
    .line 122
    .line 123
    move-object v2, v1

    .line 124
    :cond_4
    new-instance p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 125
    .line 126
    invoke-direct {p1}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 127
    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 130
    .line 131
    iput-object v1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 132
    .line 133
    iget v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo0〇Ooo:I

    .line 134
    .line 135
    iput v1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇8〇oO〇〇8o:I

    .line 136
    .line 137
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 138
    .line 139
    const/4 v3, 0x6

    .line 140
    invoke-static {v1, p1, v2, v3, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultActivity;->O0〇(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mutilcapture/mode/MultiCaptureStatus;ILjava/util/List;)Landroid/content/Intent;

    .line 141
    .line 142
    .line 143
    move-result-object p1

    .line 144
    const-string v0, "getIntent(\n            a\u2026W, pageParaList\n        )"

    .line 145
    .line 146
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    return-object p1
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇8oo0oO0(Landroid/content/Intent;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "extra_key_action_id"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "extra_key_action_batch_count"

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 21
    .line 22
    :cond_0
    if-eqz p1, :cond_1

    .line 23
    .line 24
    const-string v0, "extra_folder_id"

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8oOOo:Ljava/lang/String;

    .line 33
    .line 34
    :cond_1
    if-eqz p1, :cond_2

    .line 35
    .line 36
    const-string v0, "extra_key_action_file_id_list"

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    if-eqz p1, :cond_2

    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8o08O8O:Ljava/util/ArrayList;

    .line 45
    .line 46
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 51
    .line 52
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 53
    .line 54
    if-gtz p1, :cond_3

    .line 55
    .line 56
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 59
    .line 60
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8o08O8O:Ljava/util/ArrayList;

    .line 61
    .line 62
    new-instance v3, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    const-string v4, "try to download actionId="

    .line 68
    .line 69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    const-string v1, ", mFileIds="

    .line 76
    .line 77
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    const-string v1, ", but count is "

    .line 84
    .line 85
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    const-string p1, ", finish!"

    .line 92
    .line 93
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 104
    .line 105
    if-eqz p1, :cond_3

    .line 106
    .line 107
    invoke-virtual {p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 108
    .line 109
    .line 110
    :cond_3
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final 〇8oo8888()V
    .locals 2

    .line 1
    const-class v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/singleton/Singleton;->〇080(Ljava/lang/Class;)Lcom/intsig/singleton/Singleton;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    instance-of v1, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    check-cast v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 16
    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o800o8O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 22
    .line 23
    .line 24
    :cond_1
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇8ooOO(IZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v1, 0x0

    .line 7
    iput-boolean v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO:Z

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0, p1, p2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 12
    .line 13
    .line 14
    :cond_1
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8oo〇〇oO()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Landroid/view/View;

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 5
    .line 6
    const v2, 0x7f0a01f4

    .line 7
    .line 8
    .line 9
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    aput-object v1, v0, v2

    .line 15
    .line 16
    invoke-static {v2, v0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 20
    .line 21
    const v1, 0x7f0a1672    # 1.8355E38f

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    check-cast v0, Landroid/widget/TextView;

    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 31
    .line 32
    const/16 v0, 0xa

    .line 33
    .line 34
    new-array v0, v0, [I

    .line 35
    .line 36
    fill-array-data v0, :array_0

    .line 37
    .line 38
    .line 39
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO88〇0O8O([I)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 43
    .line 44
    const v1, 0x7f0a0774

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇o8〇〇O()V

    .line 54
    .line 55
    .line 56
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0〇8〇()V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇o8()I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇80oO〇(I)V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇o8()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    invoke-direct {p0, v0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8ooOO(IZ)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 74
    .line 75
    if-nez v0, :cond_0

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇8〇0〇o〇O(I)V

    .line 79
    .line 80
    .line 81
    :goto_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 82
    .line 83
    const v1, 0x7f0a0359

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    check-cast v0, Landroid/widget/CheckBox;

    .line 91
    .line 92
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0O:Landroid/widget/CheckBox;

    .line 93
    .line 94
    if-eqz v0, :cond_1

    .line 95
    .line 96
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇〇808〇;

    .line 97
    .line 98
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/〇〇808〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 102
    .line 103
    .line 104
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0O:Landroid/widget/CheckBox;

    .line 105
    .line 106
    if-eqz v0, :cond_2

    .line 107
    .line 108
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    if-eqz v0, :cond_2

    .line 113
    .line 114
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initView$2;

    .line 115
    .line 116
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$initView$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 117
    .line 118
    .line 119
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 120
    .line 121
    .line 122
    :cond_2
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 123
    .line 124
    const v1, 0x7f0a0586

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 132
    .line 133
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇oO〇〇8o:Landroidx/recyclerview/widget/RecyclerView;

    .line 134
    .line 135
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 136
    .line 137
    const v1, 0x7f0a0b1d

    .line 138
    .line 139
    .line 140
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 147
    .line 148
    const v1, 0x7f0a0736

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 152
    .line 153
    .line 154
    move-result-object v0

    .line 155
    check-cast v0, Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 156
    .line 157
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 158
    .line 159
    if-eqz v0, :cond_3

    .line 160
    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇08oOOO0:Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;

    .line 162
    .line 163
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->setImageAdjustListener(Lcom/intsig/camscanner/view/ImageAdjustLayout$ImageAdjustListener;)V

    .line 164
    .line 165
    .line 166
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->Oo0O〇8800()V

    .line 167
    .line 168
    .line 169
    return-void

    .line 170
    nop

    .line 171
    :array_0
    .array-data 4
        0x7f0a09e8
        0x7f0a09a6
        0x7f0a07f5
        0x7f0a0751
        0x7f0a07ca
        0x7f0a1a47
        0x7f0a05c8
        0x7f0a0774
        0x7f0a0b1d
        0x7f0a07d2
    .end array-data
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0〇o8〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo008(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8〇o〇OoO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8〇〇8o()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lkotlinx/coroutines/ExecutorCoroutineDispatcher;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇〇OOO〇〇()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇O8〇8000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lkotlinx/coroutines/ExecutorCoroutineDispatcher;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇〇8o()Lkotlinx/coroutines/ExecutorCoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇OoO0o0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇oO08〇o0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇Oo〇O(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Lcom/intsig/camscanner/view/MyViewPager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o08(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO0o(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇oO88o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final declared-synchronized 〇oOO80o()V
    .locals 4

    .line 1
    monitor-enter p0

    .line 2
    :try_start_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇o88o08〇:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3
    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    monitor-exit p0

    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x1

    .line 9
    :try_start_1
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇o88o08〇:Z

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O8o08O8O:Ljava/util/ArrayList;

    .line 12
    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const/4 v1, 0x0

    .line 23
    goto :goto_1

    .line 24
    :cond_2
    :goto_0
    const/4 v1, 0x1

    .line 25
    :goto_1
    if-nez v1, :cond_3

    .line 26
    .line 27
    sget-object v1, Lcom/intsig/camscanner/gallery/ImportWechatUtil;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatUtil;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/ImportWechatUtil;->〇o〇()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    goto :goto_2

    .line 34
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇08O〇00〇o:Ljava/lang/String;

    .line 35
    .line 36
    :goto_2
    if-eqz v1, :cond_5

    .line 37
    .line 38
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 39
    .line 40
    .line 41
    move-result v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    xor-int/2addr v0, v2

    .line 43
    if-eqz v0, :cond_4

    .line 44
    .line 45
    goto :goto_3

    .line 46
    :cond_4
    const/4 v1, 0x0

    .line 47
    :goto_3
    if-eqz v1, :cond_5

    .line 48
    .line 49
    :try_start_2
    invoke-static {v1}, Lcom/intsig/tianshu/TianShuAPI;->o〇0OOo〇0(Ljava/lang/String;)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 54
    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v3, "deleteWechatFile, res="

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/intsig/tianshu/exception/TianShuException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 73
    .line 74
    .line 75
    goto :goto_4

    .line 76
    :catch_0
    move-exception v0

    .line 77
    :try_start_3
    sget-object v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 80
    .line 81
    .line 82
    :cond_5
    :goto_4
    monitor-exit p0

    .line 83
    return-void

    .line 84
    :catchall_0
    move-exception v0

    .line 85
    monitor-exit p0

    .line 86
    throw v0
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇oO〇08o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇80oO〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇ooO〇000(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO0〇O(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇88()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇oo8O80(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇OO80oO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$generatePageList$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$generatePageList$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o〇88()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 3

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const-wide/16 v1, 0x0

    .line 12
    .line 13
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇o0O0O8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇8o0OOOo()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    if-lez v0, :cond_2

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 15
    .line 16
    if-nez v2, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 20
    .line 21
    .line 22
    :cond_2
    :goto_1
    const-string v0, "CSBatchResult"

    .line 23
    .line 24
    const-string v2, "modify"

    .line 25
    .line 26
    invoke-static {v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 38
    .line 39
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 40
    .line 41
    mul-int v2, v2, v0

    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;

    .line 44
    .line 45
    if-eqz v0, :cond_3

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/viewModel/ImageAdjustViewModel;->O8ooOoo〇(I)V

    .line 48
    .line 49
    .line 50
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const/16 v2, 0x32

    .line 55
    .line 56
    const/16 v3, 0x64

    .line 57
    .line 58
    if-nez v0, :cond_6

    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 61
    .line 62
    if-eqz v0, :cond_4

    .line 63
    .line 64
    invoke-virtual {v0, v2, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇8o8o〇(II)V

    .line 65
    .line 66
    .line 67
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 68
    .line 69
    if-eqz v0, :cond_5

    .line 70
    .line 71
    invoke-virtual {v0, v2, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO0o〇〇〇〇0(II)V

    .line 72
    .line 73
    .line 74
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 75
    .line 76
    if-eqz v0, :cond_9

    .line 77
    .line 78
    invoke-virtual {v0, v3, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇O8o08O(II)V

    .line 79
    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_6
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 83
    .line 84
    if-eqz v4, :cond_7

    .line 85
    .line 86
    iget-object v5, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 87
    .line 88
    iget v5, v5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇〇08O:I

    .line 89
    .line 90
    add-int/2addr v5, v2

    .line 91
    invoke-virtual {v4, v5, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇8o8o〇(II)V

    .line 92
    .line 93
    .line 94
    :cond_7
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 95
    .line 96
    if-eqz v4, :cond_8

    .line 97
    .line 98
    iget-object v5, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 99
    .line 100
    iget v5, v5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->O0O:I

    .line 101
    .line 102
    add-int/2addr v5, v2

    .line 103
    invoke-virtual {v4, v5, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->OO0o〇〇〇〇0(II)V

    .line 104
    .line 105
    .line 106
    :cond_8
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 107
    .line 108
    if-eqz v2, :cond_9

    .line 109
    .line 110
    iget-object v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 111
    .line 112
    iget v0, v0, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8oOOo:I

    .line 113
    .line 114
    invoke-virtual {v2, v0, v3}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇O8o08O(II)V

    .line 115
    .line 116
    .line 117
    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇08O:Lcom/intsig/camscanner/view/ImageAdjustLayout;

    .line 118
    .line 119
    if-eqz v0, :cond_a

    .line 120
    .line 121
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageAdjustLayout;->〇80〇808〇O()V

    .line 122
    .line 123
    .line 124
    :cond_a
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 125
    .line 126
    if-nez v0, :cond_b

    .line 127
    .line 128
    goto :goto_3

    .line 129
    :cond_b
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    .line 131
    .line 132
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 133
    .line 134
    if-eqz v0, :cond_c

    .line 135
    .line 136
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇00O()Landroid/view/animation/Animation;

    .line 137
    .line 138
    .line 139
    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 141
    .line 142
    .line 143
    :cond_c
    return-void
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O80OO(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇o0〇8(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O00OoO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇0(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0o0oO〇〇0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇〇〇00(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO8oo0(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇〇〇OOO〇〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇oO:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇〇O〇(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O0o8〇O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Z)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "changeCompareState: START, isDown="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    if-eqz p1, :cond_1

    .line 25
    .line 26
    iget-object p1, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    if-eqz p1, :cond_1

    .line 29
    .line 30
    sget-object v2, Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;->〇080:Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/gallery/ImportWechatLogAgentHelper;->〇o〇()V

    .line 33
    .line 34
    .line 35
    const/4 v2, 0x1

    .line 36
    if-ne p2, v2, :cond_0

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v2, 0x0

    .line 40
    :goto_0
    iput v2, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOO0880O:I

    .line 41
    .line 42
    const-string p1, "changeCompareState: START"

    .line 43
    .line 44
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 48
    .line 49
    if-eqz p1, :cond_1

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 52
    .line 53
    .line 54
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 55
    .line 56
    move-object v1, p1

    .line 57
    :cond_1
    if-nez v1, :cond_2

    .line 58
    .line 59
    const-string p1, "changeCompareState: BUT! modifyMultiImageEditModel is NULL"

    .line 60
    .line 61
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    :cond_2
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O08〇oO8〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->init()V

    .line 5
    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "onCreateView"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public interceptBackPressed()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO8()V

    .line 19
    .line 20
    .line 21
    return v2

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8〇OO0〇0o:Landroid/view/View;

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_2

    .line 31
    .line 32
    const/4 v1, 0x1

    .line 33
    :cond_2
    if-eqz v1, :cond_3

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OOo00()V

    .line 36
    .line 37
    .line 38
    return v2

    .line 39
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 40
    .line 41
    if-eqz v0, :cond_4

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8O〇008()I

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇O8〇〇o(I)Lcom/intsig/camscanner/view/ZoomImageView;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    goto :goto_1

    .line 52
    :cond_4
    const/4 v0, 0x0

    .line 53
    :goto_1
    if-eqz v0, :cond_5

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    goto :goto_2

    .line 60
    :cond_5
    const/4 v1, 0x0

    .line 61
    :goto_2
    const/high16 v3, 0x3f800000    # 1.0f

    .line 62
    .line 63
    cmpl-float v1, v1, v3

    .line 64
    .line 65
    if-lez v1, :cond_7

    .line 66
    .line 67
    if-eqz v0, :cond_6

    .line 68
    .line 69
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->〇80()V

    .line 70
    .line 71
    .line 72
    :cond_6
    return v2

    .line 73
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00〇〇〇o〇8()V

    .line 74
    .line 75
    .line 76
    return v2
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o08O()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "addItem, not supported yet!"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "retakeItem"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo8()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o88o0O(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "multiImageEditPage"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "deleteItem"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string v0, "CSBatchResult"

    .line 14
    .line 15
    const-string v1, "delete"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "CSBatchResultDelete"

    .line 21
    .line 22
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 32
    .line 33
    .line 34
    const v1, 0x7f130a04

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const v1, 0x7f130a03

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const/4 v1, 0x1

    .line 49
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->O8〇o(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const v1, 0x800005

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o8(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/〇o〇;

    .line 61
    .line 62
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/multiimageedit/〇o〇;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V

    .line 63
    .line 64
    .line 65
    const p1, 0x7f130533

    .line 66
    .line 67
    .line 68
    const v2, 0x7f0602f1

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, p1, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/O8;

    .line 76
    .line 77
    invoke-direct {v0}, Lcom/intsig/camscanner/multiimageedit/O8;-><init>()V

    .line 78
    .line 79
    .line 80
    const v1, 0x7f1317d7

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/Oo08;

    .line 88
    .line 89
    invoke-direct {v0}, Lcom/intsig/camscanner/multiimageedit/Oo08;-><init>()V

    .line 90
    .line 91
    .line 92
    const v1, 0x7f13057e

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "onActivityResult, request="

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, ", result="

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 v1, 0x67

    .line 35
    .line 36
    if-ne p1, v1, :cond_1

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->ooO:Landroid/widget/EditText;

    .line 39
    .line 40
    if-eqz p1, :cond_0

    .line 41
    .line 42
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    invoke-static {p2, p1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 47
    .line 48
    .line 49
    :cond_0
    return-void

    .line 50
    :cond_1
    const/16 v1, 0x68

    .line 51
    .line 52
    if-ne p1, v1, :cond_3

    .line 53
    .line 54
    if-eqz p3, :cond_2

    .line 55
    .line 56
    const/4 p1, -0x1

    .line 57
    if-ne p2, p1, :cond_2

    .line 58
    .line 59
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o88oo〇O(Landroid/content/Intent;)V

    .line 60
    .line 61
    .line 62
    :cond_2
    return-void

    .line 63
    :cond_3
    const/16 p2, 0x69

    .line 64
    .line 65
    if-ne p1, p2, :cond_7

    .line 66
    .line 67
    const/4 p1, 0x0

    .line 68
    if-eqz p3, :cond_5

    .line 69
    .line 70
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 71
    .line 72
    if-eqz p2, :cond_5

    .line 73
    .line 74
    invoke-static {}, Lcom/intsig/utils/DocumentUtil;->Oo08()Lcom/intsig/utils/DocumentUtil;

    .line 75
    .line 76
    .line 77
    move-result-object p2

    .line 78
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    invoke-virtual {p2, v1, v2}, Lcom/intsig/utils/DocumentUtil;->〇〇888(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    const-string v1, "extra_border"

    .line 91
    .line 92
    invoke-virtual {p3, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    const-string v2, "EXTRA_CAPTURE_SETTING_ROTATION"

    .line 97
    .line 98
    invoke-virtual {p3, v2, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O0o0()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 103
    .line 104
    .line 105
    move-result-object p3

    .line 106
    if-eqz p3, :cond_4

    .line 107
    .line 108
    invoke-direct {p0, p3, p2, v1, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O〇0o8〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;Ljava/lang/String;[II)V

    .line 109
    .line 110
    .line 111
    iget-object p1, p3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 112
    .line 113
    const-string p3, "multiImageEditPage.modifyMultiImageEditModel"

    .line 114
    .line 115
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o8〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 122
    .line 123
    .line 124
    :cond_4
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object p1

    .line 128
    new-instance p3, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v1, "imagePath="

    .line 134
    .line 135
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    const-string p2, ",border="

    .line 142
    .line 143
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    .line 145
    .line 146
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 150
    .line 151
    .line 152
    move-result-object p1

    .line 153
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    .line 155
    .line 156
    goto :goto_0

    .line 157
    :cond_5
    iget-object p2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 158
    .line 159
    if-nez p2, :cond_6

    .line 160
    .line 161
    const/4 p1, 0x1

    .line 162
    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    const-string p3, "imageViewPager == null is "

    .line 168
    .line 169
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 173
    .line 174
    .line 175
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    .line 181
    .line 182
    :cond_7
    :goto_0
    return-void
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "onAttach"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 14
    .line 15
    .line 16
    instance-of v0, p1, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    check-cast p1, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    move-object p1, v1

    .line 25
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 26
    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    :cond_1
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8oo0oO0(Landroid/content/Intent;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 45
    .line 46
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇O〇〇O8:I

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const v1, 0x7f0a09a6

    .line 11
    .line 12
    .line 13
    const-string v2, "last_next_photo"

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    const-string v4, "CSBatchResult"

    .line 17
    .line 18
    const/4 v5, 0x1

    .line 19
    if-eq v0, v1, :cond_4

    .line 20
    .line 21
    const v1, 0x7f0a09e8

    .line 22
    .line 23
    .line 24
    if-eq v0, v1, :cond_1

    .line 25
    .line 26
    const-wide/16 v1, 0xc8

    .line 27
    .line 28
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇OOo8〇0:Lcom/intsig/utils/ClickLimit;

    .line 29
    .line 30
    invoke-virtual {v5, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-nez p1, :cond_0

    .line 35
    .line 36
    return-void

    .line 37
    :cond_0
    sparse-switch v0, :sswitch_data_0

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :sswitch_0
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 42
    .line 43
    const-string v0, "scan_finish"

    .line 44
    .line 45
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O888Oo()V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :sswitch_1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo8()V

    .line 53
    .line 54
    .line 55
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 56
    .line 57
    const-string v0, "retake"

    .line 58
    .line 59
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :sswitch_2
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 64
    .line 65
    const-string v0, "filter"

    .line 66
    .line 67
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    invoke-static {v4, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O008oO0()V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :sswitch_3
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 78
    .line 79
    const-string v0, "adjust"

    .line 80
    .line 81
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0OOoO(Z)V

    .line 85
    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o088O8800()V

    .line 88
    .line 89
    .line 90
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o0o8()V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :sswitch_4
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 95
    .line 96
    const-string v0, "turn left"

    .line 97
    .line 98
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oooo800〇〇()V

    .line 102
    .line 103
    .line 104
    goto :goto_0

    .line 105
    :sswitch_5
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 106
    .line 107
    const-string v0, "exit_enhance"

    .line 108
    .line 109
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    const-string p1, "filter_save"

    .line 113
    .line 114
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO8〇O8()Lorg/json/JSONObject;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    invoke-static {v4, p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OOo00()V

    .line 122
    .line 123
    .line 124
    :goto_0
    return-void

    .line 125
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 126
    .line 127
    const-string v0, "pre page"

    .line 128
    .line 129
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    invoke-static {v4, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 136
    .line 137
    if-eqz p1, :cond_2

    .line 138
    .line 139
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 140
    .line 141
    .line 142
    move-result v3

    .line 143
    :cond_2
    if-lez v3, :cond_3

    .line 144
    .line 145
    sub-int/2addr v3, v5

    .line 146
    invoke-direct {p0, v3, v5}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8ooOO(IZ)V

    .line 147
    .line 148
    .line 149
    :cond_3
    return-void

    .line 150
    :cond_4
    sget-object p1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 151
    .line 152
    const-string v0, "next page"

    .line 153
    .line 154
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-static {v4, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    .line 159
    .line 160
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 161
    .line 162
    if-eqz p1, :cond_5

    .line 163
    .line 164
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 165
    .line 166
    .line 167
    move-result v3

    .line 168
    :cond_5
    iget p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o〇00O:I

    .line 169
    .line 170
    sub-int/2addr p1, v5

    .line 171
    if-ge v3, p1, :cond_6

    .line 172
    .line 173
    add-int/2addr v3, v5

    .line 174
    invoke-direct {p0, v3, v5}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8ooOO(IZ)V

    .line 175
    .line 176
    .line 177
    :cond_6
    return-void

    .line 178
    nop

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x7f0a05c8 -> :sswitch_5
        0x7f0a0751 -> :sswitch_4
        0x7f0a07ca -> :sswitch_3
        0x7f0a07d2 -> :sswitch_2
        0x7f0a07f5 -> :sswitch_1
        0x7f0a1a47 -> :sswitch_0
    .end sparse-switch
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "newConfig"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 7
    .line 8
    .line 9
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇O〇〇O8:I

    .line 10
    .line 11
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 12
    .line 13
    if-eq v0, p1, :cond_1

    .line 14
    .line 15
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇O〇〇O8:I

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 18
    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->notifyDataSetChanged()V

    .line 22
    .line 23
    .line 24
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0o〇o()V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onDestroy()V
    .locals 8

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o〇88()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    if-eqz v2, :cond_0

    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v4, 0x0

    .line 23
    new-instance v5, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$onDestroy$1;

    .line 24
    .line 25
    invoke-direct {v5, p0, v1}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$onDestroy$1;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 26
    .line 27
    .line 28
    const/4 v6, 0x3

    .line 29
    const/4 v7, 0x0

    .line 30
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 31
    .line 32
    .line 33
    :cond_0
    iput-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8o:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->o0O0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager$MultiImageEditPageChangeLister;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "from_part"

    .line 5
    .line 6
    const-string v1, "wechat"

    .line 7
    .line 8
    const-string v2, "CSBatchResult"

    .line 9
    .line 10
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo88()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onGetWindowFocus: START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oO〇O0O()Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/viewModel/MultiImageEditDownloadViewModel;->〇80〇808〇O()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "multiImageEditPage"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇8〇o〇OoO8()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇o8〇〇O()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a075e

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/camscanner/view/MyViewPager;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const/4 v1, 0x2

    .line 17
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 26
    .line 27
    .line 28
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 29
    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 33
    .line 34
    .line 35
    :cond_2
    new-instance v0, Lcom/intsig/view/viewpager/AlphaScaleTransformer;

    .line 36
    .line 37
    invoke-direct {v0}, Lcom/intsig/view/viewpager/AlphaScaleTransformer;-><init>()V

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 41
    .line 42
    if-eqz v2, :cond_3

    .line 43
    .line 44
    invoke-virtual {v2, v1, v0}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 45
    .line 46
    .line 47
    :cond_3
    new-instance v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 48
    .line 49
    iget-object v4, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 50
    .line 51
    iget-object v5, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 52
    .line 53
    const/4 v6, 0x0

    .line 54
    const/4 v7, -0x1

    .line 55
    const/4 v8, 0x1

    .line 56
    move-object v3, v0

    .line 57
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;ZIZ)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇o〇88()Z

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    const/4 v2, 0x1

    .line 65
    if-eqz v1, :cond_4

    .line 66
    .line 67
    iput-boolean v2, v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->OoO8:Z

    .line 68
    .line 69
    iput-boolean v2, v0, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o800o8O:Z

    .line 70
    .line 71
    :cond_4
    iput-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 72
    .line 73
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O08000(Z)V

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 77
    .line 78
    if-eqz v0, :cond_5

    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->o8O〇008()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O000(I)V

    .line 85
    .line 86
    .line 87
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 88
    .line 89
    if-eqz v0, :cond_6

    .line 90
    .line 91
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->O〇O〇oO(Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter$ImageEditItemListener;)V

    .line 92
    .line 93
    .line 94
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 95
    .line 96
    if-eqz v0, :cond_7

    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->o〇8oOO88(Lcom/intsig/camscanner/view/MyViewPager;)V

    .line 101
    .line 102
    .line 103
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 104
    .line 105
    if-eqz v0, :cond_8

    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 108
    .line 109
    const v2, 0x7f0a0c7a

    .line 110
    .line 111
    .line 112
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;->〇80(Landroid/view/View;)V

    .line 117
    .line 118
    .line 119
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 120
    .line 121
    if-eqz v0, :cond_9

    .line 122
    .line 123
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$setupViewPager$2;

    .line 124
    .line 125
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$setupViewPager$2;-><init>(Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;)V

    .line 126
    .line 127
    .line 128
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 129
    .line 130
    .line 131
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇080OO8〇0:Lcom/intsig/camscanner/view/MyViewPager;

    .line 132
    .line 133
    if-eqz v0, :cond_a

    .line 134
    .line 135
    iget-object v1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇0O:Lcom/intsig/camscanner/multiimageedit/adapter/MultiImageEditAdapter;

    .line 136
    .line 137
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 138
    .line 139
    .line 140
    :cond_a
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d030e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0888(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 3
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "multiImageEditModel"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    iput-wide v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-wide/16 v1, 0x0

    .line 17
    .line 18
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇0O8Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇()Landroid/view/View$OnClickListener;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇〇〇0o〇〇0:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8O(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->O88O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇8ooo()Landroid/net/Uri;
    .locals 28

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oo8〇〇()Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v2

    .line 7
    if-eqz v2, :cond_9

    .line 8
    .line 9
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 10
    .line 11
    .line 12
    move-result-wide v14

    .line 13
    const v0, 0x7f131113

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    invoke-static {v0, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    const-string v6, "withAppendedId(Documents\u2026ument.CONTENT_URI, docId)"

    .line 31
    .line 32
    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object v6

    .line 39
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    invoke-static {v4}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    invoke-static {v6, v4, v0}, Lcom/intsig/camscanner/app/DBUtil;->O0〇OO8(Landroid/content/Context;Ljava/util/List;Landroid/net/Uri;)V

    .line 48
    .line 49
    .line 50
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 51
    .line 52
    const-string v4, "adding tags, "

    .line 53
    .line 54
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 58
    .line 59
    const/4 v10, 0x0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    .line 62
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    .line 68
    const-string v4, "EXTRA_KEY_IMPORT_ENTRANCE_SOURCE"

    .line 69
    .line 70
    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    goto :goto_0

    .line 75
    :cond_0
    const/4 v0, 0x0

    .line 76
    :goto_0
    const/4 v13, 0x1

    .line 77
    if-ne v0, v13, :cond_1

    .line 78
    .line 79
    const-string v6, "ENTRANCE_ESIGN_HOME_IMPORT_WECHAT_PIC_MULTI"

    .line 80
    .line 81
    const/4 v7, 0x0

    .line 82
    const/4 v8, 0x4

    .line 83
    const/4 v9, 0x0

    .line 84
    move-wide v4, v14

    .line 85
    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/newsign/data/dao/ESignDbDao;->〇O00(JLjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    .line 89
    .line 90
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .line 92
    .line 93
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 94
    .line 95
    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object v4

    .line 99
    invoke-static {v4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->getCurrentEnhanceMode(Landroid/content/Context;)I

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    invoke-static {v4}, Lcom/intsig/camscanner/app/DBUtil;->oo〇(I)I

    .line 104
    .line 105
    .line 106
    move-result v8

    .line 107
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 108
    .line 109
    check-cast v4, Ljava/lang/Iterable;

    .line 110
    .line 111
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 112
    .line 113
    .line 114
    move-result-object v22

    .line 115
    const/4 v5, 0x0

    .line 116
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    .line 117
    .line 118
    .line 119
    move-result v4

    .line 120
    if-eqz v4, :cond_6

    .line 121
    .line 122
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 123
    .line 124
    .line 125
    move-result-object v4

    .line 126
    add-int/lit8 v23, v5, 0x1

    .line 127
    .line 128
    if-gez v5, :cond_2

    .line 129
    .line 130
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 131
    .line 132
    .line 133
    :cond_2
    check-cast v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;

    .line 134
    .line 135
    if-eqz v4, :cond_5

    .line 136
    .line 137
    iget-object v6, v4, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPage;->Oo08:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 138
    .line 139
    if-eqz v6, :cond_5

    .line 140
    .line 141
    const-string v4, "modifyMultiImageEditModel"

    .line 142
    .line 143
    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 147
    .line 148
    .line 149
    move-result-object v4

    .line 150
    iget-object v7, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 151
    .line 152
    new-instance v9, Ljava/lang/StringBuilder;

    .line 153
    .line 154
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    .line 162
    .line 163
    const-string v4, ".jpg"

    .line 164
    .line 165
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v4

    .line 172
    iget-object v7, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 173
    .line 174
    invoke-static {v7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 175
    .line 176
    .line 177
    move-result v7

    .line 178
    if-nez v7, :cond_3

    .line 179
    .line 180
    iget-object v7, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 181
    .line 182
    invoke-static {v7, v4}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 183
    .line 184
    .line 185
    :cond_3
    iput-object v4, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 186
    .line 187
    invoke-static {v4}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 188
    .line 189
    .line 190
    move-result-object v9

    .line 191
    iget-object v4, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 192
    .line 193
    invoke-static {v4, v9}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 194
    .line 195
    .line 196
    iput-object v9, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oOo〇8o008:Ljava/lang/String;

    .line 197
    .line 198
    sget-object v4, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 199
    .line 200
    iget-object v7, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 201
    .line 202
    invoke-static {v9}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 203
    .line 204
    .line 205
    move-result v9

    .line 206
    iget-object v10, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 207
    .line 208
    const/4 v11, 0x1

    .line 209
    iget v12, v6, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 210
    .line 211
    const/16 v16, 0x0

    .line 212
    .line 213
    const/4 v3, 0x1

    .line 214
    move/from16 v13, v16

    .line 215
    .line 216
    const/16 v16, 0x1

    .line 217
    .line 218
    move-wide/from16 v24, v14

    .line 219
    .line 220
    move/from16 v14, v16

    .line 221
    .line 222
    const/4 v15, 0x0

    .line 223
    iget-object v3, v1, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->〇00O0:Ljava/util/List;

    .line 224
    .line 225
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 226
    .line 227
    .line 228
    move-result v17

    .line 229
    const/16 v19, 0x0

    .line 230
    .line 231
    const/16 v20, 0x2000

    .line 232
    .line 233
    const/16 v21, 0x0

    .line 234
    .line 235
    move v3, v5

    .line 236
    move-object/from16 v26, v6

    .line 237
    .line 238
    move-wide/from16 v5, v24

    .line 239
    .line 240
    move/from16 v27, v8

    .line 241
    .line 242
    move/from16 v8, v23

    .line 243
    .line 244
    move/from16 v18, v3

    .line 245
    .line 246
    invoke-static/range {v4 .. v21}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇〇8O0〇8(Lcom/intsig/camscanner/app/DBInsertPageUtil;JLjava/lang/String;IZ[IIIZZZZIIIILjava/lang/Object;)Landroid/net/Uri;

    .line 247
    .line 248
    .line 249
    move-result-object v4

    .line 250
    move-object/from16 v5, v26

    .line 251
    .line 252
    iget v6, v5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 253
    .line 254
    move/from16 v7, v27

    .line 255
    .line 256
    if-eq v6, v7, :cond_4

    .line 257
    .line 258
    if-eqz v4, :cond_4

    .line 259
    .line 260
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 261
    .line 262
    .line 263
    move-result-object v4

    .line 264
    iget v6, v5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->o8〇OO0〇0o:I

    .line 265
    .line 266
    invoke-static {v6}, Lcom/intsig/camscanner/app/DBUtil;->oo〇(I)I

    .line 267
    .line 268
    .line 269
    move-result v6

    .line 270
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 271
    .line 272
    .line 273
    move-result-object v6

    .line 274
    const-string v8, "enhance_mode"

    .line 275
    .line 276
    invoke-virtual {v4, v8, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 277
    .line 278
    .line 279
    move-result-object v4

    .line 280
    const/4 v6, 0x1

    .line 281
    invoke-virtual {v4, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    .line 282
    .line 283
    .line 284
    move-result-object v4

    .line 285
    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 286
    .line 287
    .line 288
    move-result-object v4

    .line 289
    const-string v6, "newUpdate(nonNullUri)\n  \u2026                 .build()"

    .line 290
    .line 291
    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    .line 293
    .line 294
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    .line 296
    .line 297
    :cond_4
    sget-object v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 298
    .line 299
    iget-object v5, v5, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇OOo8〇0:Ljava/lang/String;

    .line 300
    .line 301
    new-instance v6, Ljava/lang/StringBuilder;

    .line 302
    .line 303
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 304
    .line 305
    .line 306
    const-string v8, "F-saveResultToDb UUID="

    .line 307
    .line 308
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    .line 313
    .line 314
    const-string v5, " index="

    .line 315
    .line 316
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    .line 318
    .line 319
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 320
    .line 321
    .line 322
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 323
    .line 324
    .line 325
    move-result-object v3

    .line 326
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    .line 328
    .line 329
    goto :goto_2

    .line 330
    :cond_5
    move v7, v8

    .line 331
    move-wide/from16 v24, v14

    .line 332
    .line 333
    :goto_2
    move v8, v7

    .line 334
    move/from16 v5, v23

    .line 335
    .line 336
    move-wide/from16 v14, v24

    .line 337
    .line 338
    const/4 v13, 0x1

    .line 339
    goto/16 :goto_1

    .line 340
    .line 341
    :cond_6
    move-wide/from16 v24, v14

    .line 342
    .line 343
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 344
    .line 345
    .line 346
    move-result v3

    .line 347
    const/4 v4, 0x1

    .line 348
    xor-int/2addr v3, v4

    .line 349
    if-eqz v3, :cond_7

    .line 350
    .line 351
    goto :goto_3

    .line 352
    :cond_7
    const/4 v0, 0x0

    .line 353
    :goto_3
    if-eqz v0, :cond_8

    .line 354
    .line 355
    :try_start_0
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 356
    .line 357
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 358
    .line 359
    .line 360
    move-result-object v3

    .line 361
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 362
    .line 363
    .line 364
    move-result-object v3

    .line 365
    sget-object v4, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 366
    .line 367
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 368
    .line 369
    .line 370
    move-result-object v0

    .line 371
    const-string v3, "{\n                    Ap\u2026Y, ops)\n                }"

    .line 372
    .line 373
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    .line 375
    .line 376
    goto :goto_4

    .line 377
    :catch_0
    move-exception v0

    .line 378
    sget-object v3, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 379
    .line 380
    const-string v4, "saveResultToDb Exception: "

    .line 381
    .line 382
    invoke-static {v3, v4, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 383
    .line 384
    .line 385
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 386
    .line 387
    :cond_8
    :goto_4
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 388
    .line 389
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 390
    .line 391
    .line 392
    move-result-object v3

    .line 393
    move-wide/from16 v10, v24

    .line 394
    .line 395
    invoke-static {v3, v10, v11}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)V

    .line 396
    .line 397
    .line 398
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 399
    .line 400
    .line 401
    move-result-object v3

    .line 402
    const/4 v4, 0x0

    .line 403
    invoke-static {v3, v10, v11, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o0O0O8(Landroid/content/Context;JLjava/lang/String;)V

    .line 404
    .line 405
    .line 406
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 407
    .line 408
    .line 409
    move-result-object v4

    .line 410
    const/4 v7, 0x1

    .line 411
    const/4 v8, 0x1

    .line 412
    const/4 v9, 0x1

    .line 413
    move-wide v5, v10

    .line 414
    invoke-static/range {v4 .. v9}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 415
    .line 416
    .line 417
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 418
    .line 419
    .line 420
    move-result-object v3

    .line 421
    invoke-static {v3, v10, v11}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 422
    .line 423
    .line 424
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 425
    .line 426
    .line 427
    move-result-object v3

    .line 428
    invoke-virtual {v3, v10, v11}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 429
    .line 430
    .line 431
    move-result-object v3

    .line 432
    invoke-virtual {v3}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0OOo〇0()V

    .line 433
    .line 434
    .line 435
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 436
    .line 437
    .line 438
    move-result-object v0

    .line 439
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 440
    .line 441
    .line 442
    sget-object v3, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 443
    .line 444
    goto :goto_5

    .line 445
    :cond_9
    const/4 v4, 0x0

    .line 446
    move-object v3, v4

    .line 447
    :goto_5
    if-nez v3, :cond_a

    .line 448
    .line 449
    sget-object v0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOoo80oO:Ljava/lang/String;

    .line 450
    .line 451
    const-string v3, "saveResultToDb, insertEmptyDoc failed!"

    .line 452
    .line 453
    invoke-static {v0, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    .line 455
    .line 456
    :cond_a
    return-object v2
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public final 〇8〇0O〇(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "multiImageEditModel"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    iput-wide v0, p1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇O〇〇O8:J

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->oOO〇〇:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditPageManager;->〇0(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;J)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇o8〇8(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 7
    .param p1    # Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "multiImageEditModel"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;->OO:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    const/4 v3, 0x0

    .line 21
    new-instance v4, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$requestHandleAll$1;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-direct {v4, p1, p0, v0}, Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment$requestHandleAll$1;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/MultiImageEditDownloadFragment;Lkotlin/coroutines/Continuation;)V

    .line 25
    .line 26
    .line 27
    const/4 v5, 0x2

    .line 28
    const/4 v6, 0x0

    .line 29
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method
